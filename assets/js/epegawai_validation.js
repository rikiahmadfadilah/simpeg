$(function() {
    $("form.need_validation").each(function(){
        var form = this;
        $(form).validate({
            focusInvalid: true,
            ignore: ':hidden:not(.isnothidden), .select2-search__field, hidden',
            // ignore: 'input[type=hidden], .select2-search__field',
            errorClass: 'validation-error-label',
            successClass: 'validation-valid-label',
            highlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },

            errorPlacement: function(error, element) {

             // Styled checkboxes, radios, bootstrap switch
             if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent().parent().parent() );
                }
                else {
                    error.appendTo( element.parent().parent().parent().parent().parent() );
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo( element.parent().parent().parent() );
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent() );
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }

            else {
                error.insertAfter(element);
            }

        },
        validClass: "validation-valid-label",
        rules: {
            minimum_characters: {
                required: true,
                minlength: 3
            },
            maximum_characters: {
                required: true,
                maxlength: 6
            },
            minimum_number: {
                required: true,
                min: 3
            },
            maximum_number: {
                required: true,
                max: 6
            },
            range: {
                required: true,
                range: [6, 16]
            },
            email_field: {
                required: true,
                email: true
            },
            url_field: {
                required: true,
                url: true
            },
            date_field: {
                required: true,
                date: true
            },
            digits_only: {
                required: true,
                digits: true
            },
            group_styled: {
                required: true,
                minlength: 2
            },
            group_unstyled: {
                required: true,
                minlength: 2
            },
            agree: "required",
        },
        messages: {
            agree: "Please accept our policy",
            accept: "Harap Masukan Extension yang di ijinkan",
        },
        success: function(label) {
            //label.addClass("validation-valid-label").text("Success.")
        },
        submitHandler: function (form) {
            $('#konfirmasipenyimpanan').modal('show');
            $('#setujukonfirmasibutton').unbind();
            $('#setujukonfirmasibutton').on('click', function () {
                $('#konfirmasipenyimpanan').modal('hide');
                $('.angkadoank').inputmask('remove');
                form.submit();
                $('#text_konfirmasi').html('Anda yakin dengan data ini ?');
            });
        }
    });

$('select.select-size-xs', $(form)).on("change",function () {
    $(form).validate().element($(this));
});
$('select.select2-wizards', $(form)).on("change", function () {
    $(form).validate().element($(this));
});
$.validator.addMethod('filesize', function (value, element, param) {
    return this.optional(element) || (element.files[0].size <= param)
}, 'Ukuran File Harus <= Maks Ukuran');

$.validator.addClassRules({
    wajib: {
        required: true
    },
    email: {
        required: true,
        email: true
    },
    wajibcheckbox: {
        required: true
    },
    wajibfile: {
        required: true,
        extension: "pdf|doc|docx|odt",
        filesize: 10240000,
    },
    FileDocument: {
        required: false,
        extension: "pdf|doc|docx|odt",
        filesize: 10240000,
    }
});


$.extend($.validator.messages, {
    required: "Wajib diisi.",
    remote: "Please fix this field.",
    email: "Harap Gunakan Format Email.",
    url: "Please enter a valid URL.",
    date: "Harap Gunakan Format Tanggal",
    dateISO: "Please enter a valid date (ISO).",
    number: "Hanya Angka yang di perbolehkan",
    digits: "Hanya Angka yang di perbolehkan",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Tidak Sama.",
    accept: "Harap Masukan Extension yang di ijinkan.",
    maxlength: $.validator.format("Kurang dari {0} characters."),
    minlength: $.validator.format("Lebih {0} characters."),
    rangelength: $.validator.format("Please enter a value between {0} and {1} characters long."),
    range: $.validator.format("Please enter a value between {0} and {1}."),
    max: $.validator.format("Maksimal {0}"),
    min: $.validator.format("Minimal {0}"),
});
});


});