<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

function getAccessToken($_this,$param){
	
	$_this->load->model('api/init_api_model','',TRUE);
	$hashtime = md5(time());
	$generated_token = $hashtime.md5($hashtime);
	$app_code = $param['app_code'];

	$device_data = $_this->init_api_model->check_app_code($app_code)->row_array();

	$data = array('status'=>$_this->status[0],'message'=>'Your application code is not exist!');
	if(count($device_data)>0) {
		$data = array('status'=>$_this->status[0],'message'=>'Your application code is not active right now!');
		if($device_data['api_device_status']==1) {

			//update device last access date
			$_this->init_api_model->update_api_device_date($device_data['api_device_id']);
			$new_access = $_this->init_api_model->add_access_token($device_data['api_device_id'], $generated_token, $_this->access_idle);

			$data = array('status'=>$_this->status[0],'message'=>'Something error on our server');
			if($new_access) {
				$data = array('status'=>$_this->status[1],'message'=>'');
				$data['results'] = array(
					'access_token'	=> $generated_token,
					'info'			=> 'Your access token will be invalid if system haven\'t accept any request for '.$_this->access_idle.' seconds ('.date('Y-m-d H:i:s', time()+$_this->access_idle).' WIT)'
				);
			}
		}
	}
	return  $data;
}

function check_access_token($_this, $param) {

	$_this->load->model('api/init_api_model','',TRUE);
	$data = false;
	$access_token = $_this->init_api_model->check_token($param['access_token'])->row_array(); // get array result
	/*echo "<pre>";
	print_r($access_token);
	die();*/
	if(count($access_token)>0) {
		if(strtotime($access_token['date'])>=strtotime(date('Y-m-d H:i:s', time()))) {
			$data = true;
			$getUser = explode(':', base64_decode($param['access_token']));
			if(isset($getUser[2]) && $getUser[2]==1) {
				$update_token_date = $_this->init_api_model->update_valid_token_stay($param['access_token'], $_this->access_idle);
			} else {
				$update_token_date = $_this->init_api_model->update_valid_token($param['access_token'], $_this->access_idle);
			}
		}
	}
	
	// falldown to generator
	return $data;
}

function generated_time($start) {
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	return $total_time;
}