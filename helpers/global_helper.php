<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

function dateEnToId($string='', $format='') { 
	$LongMonth = array(
		'en' => array('January','February','March','April','May','June','July','August','September','October','November','December'),
		'id' => array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember')
	);

	$shortMonthId = array(
		'en' => array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'),
		'id' => array('Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agu','Sept','Okt','Nov','Des')
	);
	$newDate = strtotime($string);
	$newDate = date($format, $newDate);
	$newDate = str_replace($LongMonth['en'], $LongMonth['id'], $newDate);
	$newDate = str_replace($shortMonthId['en'], $shortMonthId['id'], $newDate);

	return $newDate;
}
function dateEnToIdFullDay($string='', $format='') { 
	
	$LongDay = array(
		'en' => array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'),
		'id' => array('Senin','Selasa','Rabu','Kamis','Jum\'at','Sabtu','Minggu')
	);

	$shortDayId = array(
		'en' => array('Mon','Tue','Wed','Thu','Fri','Sat','Sun'),
		'id' => array('Sen','Sel','Rab','Kam','Jum','Sab','Min')
	);

	$LongMonth = array(
		'en' => array('January','February','March','April','May','June','July','August','September','October','November','December'),
		'id' => array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember')
	);

	$shortMonthId = array(
		'en' => array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'),
		'id' => array('Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agu','Sept','Okt','Nov','Des')
	);
	$newDate = strtotime($string);
	$newDate = date($format, $newDate);
	$newDate = str_replace($LongMonth['en'], $LongMonth['id'], $newDate);
	$newDate = str_replace($shortMonthId['en'], $shortMonthId['id'], $newDate);
	$newDate = str_replace($LongDay['en'], $LongDay['id'], $newDate);
	$newDate = str_replace($shortDayId['en'], $shortDayId['id'], $newDate);

	return $newDate;
}
function make_url($string){
	return str_replace(' ', '-', preg_replace('/\s\s+/', ' ',strtolower(preg_replace('/[^a-z_\-0-9 ]/i', '', preg_replace('/\s\s+/', ' ', strip_tags($string))))));
}
function parseCurrency($value) {
	if ( intval($value) == $value ) {
		$return = number_format($value, 0, ",", ".");
	}
	else {
		$return = number_format($value, 2, ",", ".");
		$return = rtrim($return, 0);
	}

	return $return;
}
function toNum($num) {
	$list=array('A' => 0,
		'B' => 1,
		'C' => 2,
		'D' => 3,
		'E' => 4,
		'F' => 5,
		'G' => 6,
		'H' => 7,
		'I' => 8,
		'J' => 9);
	$temp='';
	$arr_num=str_split ($num);
	foreach($arr_num as $data)
	{
		$temp.=array_search($data,$list);
	}
	$num=$temp;
	return $num;
}
function timeToText($string) {
	$result = '';
	$newDate = strtotime($string);
	if(date('Y-m-d', time())==date('Y-m-d', $newDate)) {
		$rem = time()-$newDate;
		if($rem<300) {
			$result = 'Beberapa menit yang lalu';
		} else {
			$hasil = '';
			$time = floor($rem/60);
			$jam = floor($time/60);
			$menit = $time-($jam*60);
			if($jam>0) {
				$hasil = $jam.' jam';
			}
			if($menit>0) {
				$hasil .= ($hasil!=''?' ':'').$menit.' menit';
			}
			if($hasil!='') {
				$result = $hasil.' yang lalu';
			} else {
				$result = '---';
			}
		}
	} else {
		if(date('Y', time())==date('Y', $newDate)) {
			$result = dateEnToId($string, 'd F').' jam '.dateEnToId($string, 'H:i');
		} else {
			$result = dateEnToId($string, 'd F Y').' jam '.dateEnToId($string, 'H:i');
		}
	}
	return $result;
}
function timeToTextNotif($string) {
	$result = '';
	$newDate = strtotime($string);
	if(date('Y-m-d', time())==date('Y-m-d', $newDate)) {
		$rem = time()-$newDate;
		if($rem<300) {
			$result = '0 Menit';
		} else {
			$hasil = '';
			$time = floor($rem/60);
			$jam = floor($time/60);
			$menit = $time-($jam*60);
			if($jam>0) {
				$hasil = $jam.' Jam';
			}
			if($menit>0) {
				$hasil .= ($hasil!=''?' ':''.$menit.' Menit');
			}
			if($hasil!='') {
				$result = $hasil.'';
			} else {
				$result = '---';
			}
		}
	} else {
		if(date('Y', time())==date('Y', $newDate)) {
			$result = dateEnToId($string, 'd F').'';
		} else {
			$result = dateEnToId($string, 'd F Y').'';
		}
	}
	return $result;
}
function passwordEncoder($text) {
	$first = md5($text);
	$second = '';
	for($i=0;$i<strlen($first);++$i) {
		$second .= ($i%2==0)?substr($first, $i,1):'';
	}
	$third = base64_encode($second);
	return $first;
}
function converToRupiahWithDot($rupiah) {
	$newRupiah = $rupiah*1;
	$jumlahDecimal = strlen(substr(strrchr($newRupiah, "."), 1));
	$result = number_format($newRupiah,$jumlahDecimal,'.',',');
	return $result;
}
?>