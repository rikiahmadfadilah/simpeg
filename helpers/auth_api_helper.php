<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

function getAuth($_this, $param){
	$_this->load->model('auth/auth_api_model','',TRUE);
	$data = array('status'=>$_this->status[0],'message'=>'Username atau Password tidak boleh kosong!');
	$remember = (isset($param['remember']) && $param['remember']==1)?$param['remember']:0;
	$username = isset($param['username'])?$param['username']:'';
	$password = isset($param['password'])?$param['password']:'';
	$password = ($password!='')?$password:'';
	if($username!='' && $password!='') {
		$auth = $_this->auth_api_model->getAuth($username,$password)->row_array();
		
		if(count($auth)>0){ 
			if(0==0) {
				//$dataUpdate = array('pengguna_online' => 1, 'pengguna_terakhir_login' => date('Y-m-d H:i:s'));
				//$_this->auth_api_model->updateOnline($auth['userid'], $dataUpdate);

				

				// if(LDAPChecking($_this)) {
					// if(LDAPAuth($_this, $username, $password)) {
						$data = array('status'=>$_this->status[1],'message'=>'');
						$new_token = str_replace('=', '', base64_encode($auth['user_id'].':'.$param['access_token'].':'.$remember));
						$_this->auth_api_model->updateToken($param['access_token'], $new_token);
						$data['results'] = array(
								'user_id'			=> $auth['user_id'],
								'user_name'			=> $auth['username'],
								'user_nip'			=> $auth['user_nip'],
								'new_token'			=> $new_token
							);
					// } else {
					// 	$data = array('status'=>$_this->status[0],'message'=>'Username atau Password salah!');
					// }
				// } else {
				// 	$data = array('status'=>$_this->status[0],'message'=>'Tidak dapat berkomunikasi dengan LDAP Server!');
				// }
			}else{
				$data = array('status'=>$_this->status[0],'message'=>'User sedang login ke aplikasi');
			} 
		}else {
			$data = array('status'=>$_this->status[0],'message'=>'Username / Password Salah');
		}
	}else{
		$data = array('status'=>$_this->status[0],'message'=>'Username / Password tidak boleh kosong');
	}
	return $data;
}
function LDAPChecking($_this) {
	if($_this->config->config['use_LDAP']) {
		$ldap = ldap_connect($_this->config->config['LDAP_server'], $_this->config->config['LDAP_port']);
		ldap_close($ldap);
		return $ldap?true:false;
	} else {
		return true;
	}
}

function LDAPAuth($_this, $username, $password) {
	if($_this->config->config['use_LDAP']) {
		$ldap = ldap_connect($_this->config->config['LDAP_server']);
		$bind = @ldap_bind($ldap, $_this->config->config['LDAP_domain']. "\\" . $username, $password);
		ldap_close($ldap);
		return $bind?true:false;
	} else {
		return $_this->auth_api_model->doLogin($username, $password)>0?true:false;
	}
}

function logout($_this, $param){
	$_this->load->model('auth_api_model','',TRUE);

	$data = array();
	$data['status'] 	= $_this->status[0];
	if($param['user_id']!='' && count($param)==2){
		$data['status'] = $_this->status[1];
		$dataUpdate = array('pengguna_online' => 0);
		$dataToken  = array('API_TOKEN_VALID' => date('Y-m-d H:i:s'));
		$_this->auth_api_model->updateOnline($param['user_id'], $dataUpdate);
		$_this->auth_api_model->destroyToken($param['access_token'], $dataToken);
	}else{
		$data = array('status'=>0,'message'=>'Error, Parameter ada yg kurang / user id tidak diketahui, silahkan coba lagi!');
	}

	return $data;
}
