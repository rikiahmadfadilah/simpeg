<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

function colorStatus($status) {
	$color = '969696';
	$colorStatus = array(
		'DRAFT'				=> '969696',
		'SUBMITTED'			=> '9303a4',
		'IN PROGRESS'		=> 'F4B202',
		'APPROVED'			=> '43CC27',
		'REJECTED'			=> 'd6031b',
		'READY TO RELEASE'	=> 'CF680d',
		'RELEASED'			=> '038E09',
		'SUCCESSFUL'		=> '20B89D',
		'PAID'				=> '0086C6'
	);
	if(array_key_exists($status, $colorStatus)) {
		$color = $colorStatus[$status];
	}
	return $color;
}
function getDataAbsensi($_this, $param){
	$_this->load->model('presensi/presensi_model','',TRUE);
	$_this->load->helper('get_api_helper');


	$data = array();
	$data['status'] = $_this->status[1];
	$pegawaiId = $param['pegawai_id'];
	$pegawai = $_this->presensi_model->getDataPegawai($pegawaiId)->row_array();

	$datefilter = $_this->db->query("SELECT YEAR(MAX(time_shift_start)) year FROM abs_checkinout_proses WHERE userid = '".$pegawai['pegawai_abs_id']."' AND is_holiday = 'T'")->row_array();

	$absensi = $_this->presensi_model->getDataAbsensi($pegawai['pegawai_abs_id'], $datefilter["year"])->result_array();

	if(count($absensi)>0){
		$data['absensi'] = $absensi;
	}else{
		$data = array('status'=>$_this->status[0],'message'=>'Error Result!');
	}
	$dataabsen = array();
	$idx = 0;
	foreach ($absensi as $value) {
		if($value['shift_lintas_hari']=='Y'){
			$ci_str = ($value['time_cin'] != null)?date('d-m-Y H:i:s', strtotime($value['time_cin'])):"";
			$co_str = ($value['time_cout'] != null)?date('d-m-Y H:i:s', strtotime($value['time_cout'])):"";
			$shift_i_str = ($value['time_shift_start'] != null)?date('d-m-Y H:i:s', strtotime($value['time_shift_start'])):"";
			$shift_o_str = ($value['time_shift_end'] != null)?date('d-m-Y H:i:s', strtotime($value['time_shift_end'])):"";
		}
		else{
			$ci_str = ($value['time_cin'] != null)?date('H:i:s', strtotime($value['time_cin'])):"";
			$co_str = ($value['time_cout'] != null)?date('H:i:s', strtotime($value['time_cout'])):"";
			$shift_i_str = ($value['time_shift_start'] != null)?date('H:i:s', strtotime($value['time_shift_start'])):"";
			$shift_o_str = ($value['time_shift_end'] != null)?date('H:i:s', strtotime($value['time_shift_end'])):"";
		}
		$dataabsen[$idx]["hari"] = dateEnToIdFullDay($value['time_shift_start'], 'l, d M Y');
		$dataabsen[$idx]["masuk"] = $ci_str;
		$dataabsen[$idx]["keluar"] = $co_str;
		$idx++;
	}
	$data = array('status'=>$_this->status[1],'message'=>'');
	$data['results'] = array(
		'dataabsen'	=> $dataabsen,
		'absensi'	=> $absensi,
	);
	return $data;

}
function getDataPegawai($_this, $param){

	$data = array();
	$data['status'] = $_this->status[1];
	$wew = $_this->db->query("SELECT * FROM view_ms_pegawai WHERE pegawai_nip = '".$param['pegawai_nip']."'")->row_array();
	if(count($wew)>0){
		$data['pegawai'] = $wew;
	}else{
		$data = array('status'=>$_this->status[0],'message'=>'Error Result!');
	}
	$data = array('status'=>$_this->status[1],'message'=>'');
	$data['results'] = array(
		'pegawai'	=> $wew,
	);
	return $data;

}
function getDataKgb($_this, $param){

	$data = array();
	$data['pendidikan'] = array();
	$data['status'] = $_this->status[1];
	$wew = $_this->db->query("SELECT * FROM view_trx_kgb WHERE kgb_pegawai_nip = '".$param['pegawai_nip']."' ORDER BY kgb_tanggal DESC")->result_array();
	if(count($wew)>0){
		$data['kgb'] = $wew;
	}else{
		$data = array('status'=>$_this->status[0],'message'=>'Error Result!');
	}
	$data = array('status'=>$_this->status[1],'message'=>'');
	$data['results'] = array(
		'kgb'	=> $wew,
	);
	return $data;

}
function updateFirebasePegawai($_this, $param){

	$data = array();
	$data['pendidikan'] = array();
	$data['status'] = $_this->status[1];
	$_this->db->query("UPDATE ms_pegawai SET pegawai_firebase_id = '".$param['firebase_id']."' WHERE  pegawai_nip = '".$param['pegawai_nip']."'");
	$data = array('status'=>$_this->status[1],'message'=>'');
	return $data;

}

function getDataPendidikanFormal($_this, $param){

	$data = array();
	$data['pendidikan'] = array();
	$data['status'] = $_this->status[1];
	$wew = $_this->db->query("SELECT * FROM view_trx_pendidikan_formal WHERE pendidikan_formal_pegawai_nip = '".$param['pegawai_nip']."' AND pendidikan_formal_status = 1")->result_array();
	if(count($wew)>0){
		$data['pendidikan'] = $wew;
	}else{
		$data = array('status'=>$_this->status[0],'message'=>'Error Result!');
	}
	$data = array('status'=>$_this->status[1],'message'=>'');
	$data['results'] = array(
		'pendidikan'	=> $wew,
	);
	return $data;

}
function getDataPendidikanInFormal($_this, $param){

	$data = array();
	$data['pendidikan_informal'] = array();
	$data['status'] = $_this->status[1];
	$wew = $_this->db->query("SELECT * FROM view_trx_pendidikan_nonformal WHERE pendidikan_nonformal_pegawai_nip = '".$param['pegawai_nip']."' AND pendidikan_nonformal_status = 1")->result_array();
	if(count($wew)>0){
		$data['pendidikan_informal'] = $wew;
	}else{
		$data = array('status'=>$_this->status[0],'message'=>'Error Result!');
	}
	$data = array('status'=>$_this->status[1],'message'=>'');
	$data['results'] = array(
		'pendidikan_informal'	=> $wew,
	);
	return $data;

}
function getDataPenjenjangan($_this, $param){

	$data = array();
	$data['diklat'] = array();
	$data['status'] = $_this->status[1];
	$wew = $_this->db->query("SELECT * FROM view_trx_diklat_perjenjangan WHERE diklat_pegawai_nip = '".$param['pegawai_nip']."' AND diklat_status = 1")->result_array();
	if(count($wew)>0){
		$data['diklat'] = $wew;
	}else{
		$data = array('status'=>$_this->status[0],'message'=>'Error Result!');
	}
	$data = array('status'=>$_this->status[1],'message'=>'');
	$data['results'] = array(
		'diklat'	=> $wew,
	);
	return $data;

}
function getDataSuamiIstri($_this, $param){

	$data = array();
	$data['suami_istri'] = array();
	$data['status'] = $_this->status[1];
	$wew = $_this->db->query("SELECT * FROM view_trx_suami_istri WHERE suami_istri_pegawai_nip = '".$param['pegawai_nip']."' AND suami_istri_status = 1")->result_array();
	if(count($wew)>0){
		$data['suami_istri'] = $wew;
	}else{
		$data = array('status'=>$_this->status[0],'message'=>'Error Result!');
	}
	$data = array('status'=>$_this->status[1],'message'=>'');
	$data['results'] = array(
		'suami_istri'	=> $wew,
	);
	return $data;

}
function getDataAnak($_this, $param){

	$data = array();
	$data['anak'] = array();
	$data['status'] = $_this->status[1];
	$wew = $_this->db->query("SELECT * FROM view_trx_anak WHERE anak_pegawai_nip = '".$param['pegawai_nip']."' AND anak_status = 1")->result_array();
	if(count($wew)>0){
		$data['anak'] = $wew;
	}else{
		$data = array('status'=>$_this->status[0],'message'=>'Error Result!');
	}
	$data = array('status'=>$_this->status[1],'message'=>'');
	$data['results'] = array(
		'anak'	=> $wew,
	);
	return $data;

}
function getDataKeluarga($_this, $param){

	$data = array();
	$data['keluarga'] = array();
	$data['status'] = $_this->status[1];
	$wew = $_this->db->query("SELECT * FROM view_trx_keluarga WHERE keluarga_pegawai_nip = '".$param['pegawai_nip']."' AND keluarga_status = 1")->result_array();
	if(count($wew)>0){
		$data['keluarga'] = $wew;
	}else{
		$data = array('status'=>$_this->status[0],'message'=>'Error Result!');
	}
	$data = array('status'=>$_this->status[1],'message'=>'');
	$data['results'] = array(
		'keluarga'	=> $wew,
	);
	return $data;

}
function getDataRiwayatKepangkatan($_this, $param){

	$data = array();
	$data['kepangkatan'] = array();
	$data['status'] = $_this->status[1];
	$wew = $_this->db->query("SELECT * FROM view_trx_kepangkatan WHERE kepangkatan_pegawai_nip = '".$param['pegawai_nip']."' AND kepangkatan_status = 1")->result_array();
	if(count($wew)>0){
		$data['kepangkatan'] = $wew;
	}else{
		$data = array('status'=>$_this->status[0],'message'=>'Error Result!');
	}
	$data = array('status'=>$_this->status[1],'message'=>'');
	$data['results'] = array(
		'kepangkatan'	=> $wew,
	);
	return $data;

}
function getDataRiwayatJabatan($_this, $param){

	$data = array();
	$data['jabatan'] = array();
	$data['status'] = $_this->status[1];
	$wew = $_this->db->query("SELECT * FROM view_trx_jenjang_jabatan WHERE jenjang_jabatan_pegawai_nip = '".$param['pegawai_nip']."' AND jenjang_jabatan_status = 1")->result_array();
	if(count($wew)>0){
		$data['jabatan'] = $wew;
	}else{
		$data = array('status'=>$_this->status[0],'message'=>'Error Result!');
	}
	$data = array('status'=>$_this->status[1],'message'=>'');
	$data['results'] = array(
		'jabatan'	=> $wew,
	);
	return $data;

}
function getDataSeminar($_this, $param){

	$data = array();
	$data['seminar'] = array();
	$data['status'] = $_this->status[1];
	$wew = $_this->db->query("SELECT * FROM view_trx_seminar WHERE seminar_pegawai_nip = '".$param['pegawai_nip']."' AND seminar_status = 1")->result_array();
	if(count($wew)>0){
		$data['seminar'] = $wew;
	}else{
		$data = array('status'=>$_this->status[0],'message'=>'Error Result!');
	}
	$data = array('status'=>$_this->status[1],'message'=>'');
	$data['results'] = array(
		'seminar'	=> $wew,
	);
	return $data;

}
function getDataPenghargaan($_this, $param){

	$data = array();
	$data['penghargaaan'] = array();
	$data['status'] = $_this->status[1];
	$wew = $_this->db->query("SELECT * FROM view_trx_penghargaan WHERE penghargaan_pegawai_nip = '".$param['pegawai_nip']."' AND penghargaan_status = 1")->result_array();
	if(count($wew)>0){
		$data['penghargaaan'] = $wew;
	}else{
		$data = array('status'=>$_this->status[0],'message'=>'Error Result!');
	}
	$data = array('status'=>$_this->status[1],'message'=>'');
	$data['results'] = array(
		'penghargaaan'	=> $wew,
	);
	return $data;

}

function getDataUnitKerja($_this, $param){

	$data = array();
	$data['kecamatan_data'] = array();
	$data['status'] = $_this->status[1];
	$wew = $_this->db->query("SELECT * FROM view_ms_unit_kerja WHERE unit_kerja_kode = '".$param['unit_kerja_kode']."'")->result_array();
	if(count($wew)>0){
		$data['unit_kerja'] = $wew;
	}else{
		$data = array('status'=>$_this->status[0],'message'=>'Error Result!');
	}
	$data = array('status'=>$_this->status[1],'message'=>'');
	$data['results'] = array(
		'unit_kerja'	=> $wew,
	);
	return $data;

}
function getListKuisoner($_this, $param){
	$_this->load->model('responden_model', '', TRUE);
	$_this->load->model('auth_api_model','',TRUE);

	$data = array();
	$data['status'] = $_this->status[1];
	$data['kuisoners'] = array();
	$groups = $_this->responden_model->get_kuisoner_group()->result_array();

	if(count($groups)>0){
		foreach($groups as $gk => $gv) {
			$kuisoners = $_this->responden_model->get_kuisoner($gv['kuisoner_group_id'])->result_array();

			$pertanyaan = array();
			foreach($kuisoners as $kk => $kv) {
				$pilihans = $_this->responden_model->get_kuisoner_pilihan($kv['kuisoner_id'])->result_array();
				$jawaban = array();

				foreach($pilihans as $pk => $pv) {
					$anakpilihans = $_this->responden_model->get_kuisoner_pilihan($kv['kuisoner_id'], $pv['kuisoner_pilihan_id'])->result_array();
					$anakjawaban = array();

					foreach($anakpilihans as $ak => $av) {
						$anakjawaban[] = array(
							'id_pilihan'		=> $av['kuisoner_pilihan_id'],
							'is_esay'			=> $av['kuisoner_pilihan_is_esay'],
							'nomor_pilihan'		=> $av['kuisoner_pilihan_nomor'],
							'nama_pilihan'		=> $av['kuisoner_pilihan_teks'],
							'tambahan_pilihan'	=> $av['kuisoner_pilihan_tambahan']
						);
					}

					$jawaban[] = array(
						'id_pilihan'		=> $pv['kuisoner_pilihan_id'],
						'is_esay'			=> $pv['kuisoner_pilihan_is_esay'],
						'nomor_pilihan'		=> $pv['kuisoner_pilihan_nomor'],
						'nama_pilihan'		=> $pv['kuisoner_pilihan_teks'],
						'tambahan_pilihan'	=> $pv['kuisoner_pilihan_tambahan'],
						'anak_pilihan'		=> $anakjawaban
					);

				}

				if(count($jawaban)>0) {
					$pertanyaan[] = array(
						'id_pertanyaan' => $kv['kuisoner_id'],
						'pertanyaan'	=> $kv['kuisoner_pertanyaan'],
						'pilihan'		=> $jawaban
					);
				}
			}
			if(count($pertanyaan)>0) {
				$data['kuisoners'][] = array(
					'id_group'			=> $gv['kuisoner_group_id'],
					'nama_group'		=> $gv['kuisoner_group_nama'],
					'pertanyaan_group'	=> $pertanyaan
				);
			}
		}
	}else{
		$data = array('status'=>$_this->status[0],'message'=>'Error Result!');
	}

	return $data;
}

function getListResponden($_this, $param){

	$_this->load->model('responden_model', '', TRUE);
	$_this->load->model('auth_api_model','',TRUE);

	$data = array();
	$data['status'] = $_this->status[1];
	$data['respondenData'] = array();
	$totalData = $_this->responden_model->getTotalData()->result_array();

	if(isset($param['limit']) !="" && isset($param['page']) !=""){
		$data['curpage']       = $param['page'];
		$data['totalpage']     = ceil($totalData[0]['totaldata']/$param['limit']);
		$data['respondenData'] = $_this->responden_model->get_limit($param['limit'], ($param['page']-1))->result(); 
	}else{
		$data['respondenData'] = $_this->responden_model->get_all()->result(); 
	}

	if(count($data['respondenData'])<1){
		$data = array('status'=>$_this->status[0],'message'=>'Error Result!');
	}    

	return $data;
}

function getSearchResponden($_this, $param){
	$_this->load->model('responden_model', '', TRUE);
	$_this->load->model('auth_api_model','',TRUE);

	$data = array();
	$data['$searchResponden'] = array();
	$data['status'] = $_this->status[1];

	if($param['paramkey'] !=""){
		$data['$searchResponden'] = $_this->responden_model->getRespondenByParam($param['paramkey'])->result();
	}else{
		$data = array('status'=>$_this->status[0],'message'=>'Error Result!');
	}

	return $data;
}

function getDetilResponden($_this, $param){
	$_this->load->model('responden_model', '', TRUE);
	$_this->load->model('auth_api_model','',TRUE);

	$data = array();
	$data['detilResponden'] = array();
	$data['status'] = $_this->status[1];

	if($param['RespondenId'] !=""){
		$data['detilResponden'] = $_this->responden_model->get_responden($param['RespondenId'])->result();
	}else{
		$data = array('status'=>$_this->status[0],'message'=>'Error Result!');
	}

	return $data;
}

function getlistKecamatan($_this, $param){
	$_this->load->model('responden_model', '', TRUE);
	$_this->load->model('auth_api_model','',TRUE);
	$_this->load->model('map_model','',TRUE);

	$data = array();
	$data['kecamatan_data'] = array();
	$data['status'] = $_this->status[1];

	if($param['kotaKabId'] !=""){
		$data['kecamatan_data'] = $_this->map_model->get_kecamatanByKab($param['kotaKabId'])->result_array();
	}else{
		$data = array('status'=>$_this->status[0],'message'=>'Error Result!');
	}

	return $data;

}

function getlistKelurahan($_this, $param){
	$_this->load->model('responden_model', '', TRUE);
	$_this->load->model('auth_api_model','',TRUE);
	$_this->load->model('map_model','',TRUE);

	$data = array();
	$data['kelurahan_data'] = array();
	$data['status'] = $_this->status[1];

	if($param['kotaKabId'] !="" && $param['kecamatanId']!=""){
		$data['kelurahan_data'] = $_this->map_model->get_kelurahan($param['kecamatanId'])->result_array();
	}else{
		$data = array('status'=>$_this->status[0],'message'=>'Error Result!');
	}

	return $data;

}

function getLastIdResponden($_this, $param){
	$_this->load->model('responden_model', '', TRUE);
	$_this->load->model('auth_api_model','',TRUE);

	$data = array();
// $data['kecamatan_data'] = array();
// $data['kelurahan_data'] = array();
	$data['status'] = $_this->status[1];

	if(count($param==2)){
		$query = $_this->responden_model->getLastIdData()->result_array();
		$data['lastId'] = $query[0]['lastIdResponden'];
	}else{
		$data = array('status'=>$_this->status[0],'message'=>'Error Result!');
	}

	return $data;

}

function getCheckQrCode($_this, $param){
	$_this->load->model('responden_model', '', TRUE);
	$_this->load->model('auth_api_model','',TRUE);
	$data = array();
	$data['status'] = $_this->status[1];

	if(count($param)>2){
		$jmlqrcode = $_this->responden_model->getCountQRCode($param['qrcode'])->row_array();
		if($jmlqrcode['jmlqrcode']==0){
			$data = array('status'=>$_this->status[1],'message'=>'QRCODE '.$param['qrcode'].'Belum ada.');
		}else{
			$data = array('status'=>$_this->status[0],'message'=>'Data Responden dengan QRCODE '.$param['qrcode'].'  sudah ada, silahkan coba lagi!.');
		}
	}else{
		$data = array('status'=>$_this->status[0],'message'=>'Parameter yang dikirim kurang!');
	}

	return $data;	
}
