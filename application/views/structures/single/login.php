<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ropeg</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/core.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="<?php echo base_url(); ?>assets/js/plugins/loaders/pace.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/core/libraries/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/core/libraries/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->


    <!-- Theme JS files -->
    <script src="<?php echo base_url(); ?>assets/js/app.js"></script>
    <!-- /theme JS files -->
    <style type="text/css">
    #bg {
      position: fixed; 
      top: -50%; 
      left: -50%; 
      width: 200%; 
      height: 200%;
  }
  #bg img {
      position: absolute; 
      top: 0; 
      left: 0; 
      right: 0; 
      bottom: 0; 
      margin: auto; 
      min-width: 50%;
      min-height: 50%;
  }
</style>

</head>
<script type="text/javascript">
    function showPassword(){
      var flag = $('#flagShowPass').val();
      if(flag.toString()=='0'){
        $('#password').attr('type', 'text');       
        $('#flagShowPass').val(1);
      }else{
        $('#password').attr('type', 'password');
        $('#flagShowPass').val(0);
      }
    }
</script>
<body class="login-container" >
<div id="bg">
  <img src="<?php echo base_url($bg_login); ?>" alt="">
</div>
    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Content area -->
                <div class="content">
                    <!-- Simple login form -->
                    <form action="<?php echo base_url(); ?>home/auth/checklogin" role="form" method="post" enctype="multipart/form-data">
                        <div class="panel panel-body login-form">
                            <div class="text-center">
                                <img src="<?php echo base_url($logo_login); ?>" style="height:80px;clear:both;">
                                <!-- <div class="icon-object border-slate-300 text-slate-300">
                                    <i class="icon-reading"></i>
                                </div> -->
                                <h5 class="content-group"><?php echo $app_name; ?><small class="display-block"><?php echo $company_name; ?></small></h5>
                            </div>
              							<?php $message = $this->session->flashdata('message'); if($message!=""){?>
              							<div class="alert alert-danger" style="background-color: transparent;color:black;border-color:#ddd;border-radius:0;">
              								<span style="color:#bd2025">Terdapat kesalahan.</span><br><?php echo $message;?>.
              							</div>
              							<?php } ?>
                            <div class="form-group has-feedback has-feedback-left">
                                <input type="text" class="form-control" required="required" placeholder="Username" name="username" id="username">
                                <div class="form-control-feedback">
                                    <i class="icon-user text-muted"></i>
                                </div>
                            </div>
                            <div class="input-group has-feedback has-feedback-left">
                                <input type="hidden" name="flagShowPass" id="flagShowPass" value="0">
                                <input type="password" class="form-control" required="required" placeholder="Password" name="password" id="password">
                                <div class="form-control-feedback">
                                    <i class="icon-lock2 text-muted"></i>
                                </div>
                              <span class="input-group-addon" id="lihat"><i class="icon-eye" style="position: right" onclick="showPassword();"></i></span>
                            </div>
                            <br/>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Login<i class="icon-circle-right2 position-right"></i></button>
                            </div>

                            <!-- <div class="text-center">
                              <div class="col-md-6"><a href="login_password_recover.html" style="position:left">Lupa Password?</a></div>
                            </div> -->
                        </div>
                    </form>
                    <!-- /simple login form -->


                    <!-- Footer -->
                    <div class="footer text-muted text-center">
                        &copy; 2019. KEMARITIMAN
                    </div>
                    <!-- /footer -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->

</body>
</html>
