<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $this->config->config['app_name']; ?> | <?php echo $this->config->config['app_company']; ?></title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->
	<script type="text/javascript">
		var base_url = '<?php echo base_url();?>';
	</script>
	<!-- Core JS files -->
	<script src="<?php echo base_url();?>assets/js/plugins/loaders/pace.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/core/libraries/jquery.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/core/libraries/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/loaders/blockui.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/core/libraries/jquery_ui/full.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="<?php echo base_url();?>assets/js/plugins/visualization/d3/d3.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/ui/moment/moment.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/pickers/daterangepicker.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/ui/nicescroll.min.js"></script>


	<script src="<?php echo base_url();?>assets/js/plugins/forms/validation/validate.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/forms/selects/select2.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/ui/moment/moment.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/ui/moment/moment_locales.min.js"></script>

	<script src="<?php echo base_url();?>assets/js/plugins/pickers/daterangepicker.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/pickers/anytime.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/pickers/pickadate/legacy.js"></script>
	<script src="<?php echo base_url();?>assets/js/demo_pages/form_layouts.js"></script>
	<script src="<?php echo base_url();?>assets/js/demo_pages/picker_date.js"></script>
	<script src="<?php echo base_url();?>assets/js/demo_pages/form_select2.js"></script>
	<script src="<?php echo base_url();?>assets/js/demo_pages/datatables_responsive.js"></script>
	<script src="<?php echo base_url();?>assets/js/demo_pages/form_inputs.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/forms/inputs/autosize.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/forms/inputs/formatter.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/forms/inputs/passy.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/forms/inputs/maxlength.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/forms/inputs/typeahead/handlebars.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/demo_pages/form_controls_extended.js"></script>
	<script src="<?php echo base_url();?>assets/js/core/libraries/jasny_bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/ui/ripple.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/notifications/bootbox.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/notifications/sweet_alert.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/forms/tags/tokenfield.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/forms/tags/tagsinput.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/demo_pages/form_tags_input.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/ui/prism.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/demo_pages/components_popups.js"></script>
	<script src="<?php echo base_url();?>assets/js/app.js"></script>
	<script src="<?php echo base_url();?>assets/js/demo_pages/layout_fixed_custom.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/notifications/pnotify.min.js"></script>


	<script src="<?php echo base_url();?>assets/js/epegawai_validation.js"></script>

	<script src="<?php echo base_url();?>assets/js/plugins/visualization/echarts/echarts.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/demo_pages/charts/echarts/columns_waterfalls.js"></script>
	<script src="<?php echo base_url();?>assets/js/demo_pages/charts/echarts/pies_donuts.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/visualization/echarts/echarts.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/demo_pages/charts/echarts/columns_waterfalls.js"></script>
	<script src="<?php echo base_url();?>assets/js/demo_pages/charts/echarts/pies_donuts.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/ui/ripple.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/visualization/d3/d3.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/visualization/c3/c3.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/demo_pages/charts/c3/c3_bars_pies.js"></script>
	<!-- /theme JS files -->

</head>

<body class="navbar-top">

	<!-- Main navbar -->
	<div class="navbar navbar-default navbar-fixed-top header-highlight">
		<div class="navbar-header">
			<a class="navbar-brand" href="javascript:void(0);"><img src="<?php echo base_url($logo_sidebar);?>" width="100"></a>

			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>

			<div class="navbar-right">
				<p class="navbar-text"><b>Login Sebagai : <?php echo $this->session->userdata('user_akses_nama');?></b></p>
				<p class="navbar-text"><span class="label bg-success">Online</span></p>
				
				<ul class="nav navbar-nav">				
					<li class="dropdown">
						<div class="dropdown-menu dropdown-content">
							<div class="dropdown-content-heading">
								Activity
								<ul class="icons-list">
									<li><a href="#"><i class="icon-menu7"></i></a></li>
								</ul>
							</div>

							<ul class="media-list dropdown-content-body width-350">
								<li class="media">
									<div class="media-left">
										<a href="#" class="btn bg-success-400 btn-rounded btn-icon btn-xs"><i class="icon-mention"></i></a>
									</div>

									<div class="media-body">
										<a href="#">Taylor Swift</a> mentioned you in a post "Angular JS. Tips and tricks"
										<div class="media-annotation">4 minutes ago</div>
									</div>
								</li>

								<li class="media">
									<div class="media-left">
										<a href="#" class="btn bg-pink-400 btn-rounded btn-icon btn-xs"><i class="icon-paperplane"></i></a>
									</div>
									
									<div class="media-body">
										Special offers have been sent to subscribed users by <a href="#">Donna Gordon</a>
										<div class="media-annotation">36 minutes ago</div>
									</div>
								</li>

								<li class="media">
									<div class="media-left">
										<a href="#" class="btn bg-blue btn-rounded btn-icon btn-xs"><i class="icon-plus3"></i></a>
									</div>
									
									<div class="media-body">
										<a href="#">Chris Arney</a> created a new <span class="text-semibold">Design</span> branch in <span class="text-semibold">Limitless</span> repository
										<div class="media-annotation">2 hours ago</div>
									</div>
								</li>

								<li class="media">
									<div class="media-left">
										<a href="#" class="btn bg-purple-300 btn-rounded btn-icon btn-xs"><i class="icon-truck"></i></a>
									</div>
									
									<div class="media-body">
										Shipping cost to the Netherlands has been reduced, database updated
										<div class="media-annotation">Feb 8, 11:30</div>
									</div>
								</li>

								<li class="media">
									<div class="media-left">
										<a href="#" class="btn bg-warning-400 btn-rounded btn-icon btn-xs"><i class="icon-bubble8"></i></a>
									</div>
									
									<div class="media-body">
										New review received on <a href="#">Server side integration</a> services
										<div class="media-annotation">Feb 2, 10:20</div>
									</div>
								</li>

								<li class="media">
									<div class="media-left">
										<a href="#" class="btn bg-teal-400 btn-rounded btn-icon btn-xs"><i class="icon-spinner11"></i></a>
									</div>
									
									<div class="media-body">
										<strong>January, 2016</strong> - 1320 new users, 3284 orders, $49,390 revenue
										<div class="media-annotation">Feb 1, 05:46</div>
									</div>
								</li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main sidebar-fixed">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user-material">
						<div class="category-content">
							<div class="sidebar-user-material-content">
								<a href="#">
									<?php if($this->session->userdata('PHOTO') != '') { ?>
										<img src="<?php echo base_url().'assets/images/pegawai/'.$this->session->userdata('PHOTO');?>" class="img-circle img-responsive">
									<?php }else{ ?>
										<img src="<?php echo base_url();?>assets/images/default_user.jpg" class="img-circle img-responsive">
									<?php } ?>
								</a>
								<h6><?php echo $this->session->userdata('NAMA');?></h6>
								<span class="text-size-small"><?php echo $this->session->userdata('unit_kerja_nama');?></span>
							</div>
														
							<div class="sidebar-user-material-menu">
								<a href="#user-nav" data-toggle="collapse"><span>My account</span> <i class="caret"></i></a>
							</div>
						</div>
						
						<div class="navigation-wrapper collapse" id="user-nav">
							<ul class="navigation">
								<li><a href="javascript:void(0)" onclick="GantiAkses()"><i class="icon-arrow-resize7 position-left"></i> <span>Ganti Akses</span></a></li>
								<li class="divider"></li>
								<li><a href="<?php echo base_url('password/ganti_password/ubah/'.$this->session->userdata('ID'));?>"><i class="icon-cog5"></i> <span>Ubah Password</span></a></li>
								<li><a href="<?php echo base_url();?>home/auth/logout"><i class="icon-switch2"></i> <span>Logout</span></a></li>
							</ul>
						</div>
					</div>
					<!-- /user menu -->


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<?php echo $menu;?>
								<!-- /main -->
							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header page-header-default">
					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url();?>"><i class="icon-home2 position-left"></i>Home</a></li>
							<li class="active">Dashboard</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- <div class="panel panel-flat">
						<div class="panel-body" style="padding: 10px">
							<form action="<?php echo base_url('/home/dashboard/cari_pegawai'); ?>" class="main-search" method="post" enctype="multipart/form-data" target="_blank">
								<div class="input-group content-group" style="margin-bottom: 0 !important">
									<div class="has-feedback has-feedback-left">
										<input type="text" name="nip_nama" id="nip_nama" class="form-control input-xlg" placeholder="NIP / Nama">
										<div class="form-control-feedback">
											<i class="icon-search4"></i>
										</div>
									</div>

									<div class="input-group-btn">
										<button type="submit" class="btn btn-primary btn-xs legitRipple">Search</button>
									</div>
								</div>
							</form>
						</div>
					</div> -->

					<?php echo $_content; ?>

					<div id="konfirmasipenyimpanan" class="modal fade" data-backdrop="true" data-keyboard="false" tabindex="-1">
						<div class="modal-dialog modal-xs">
							<div class="modal-content">
								<div class="modal-header bg-primary">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h6 class="modal-title">Konfirmasi</h6>
								</div>

								<div class="modal-body">
									<span id="text_konfirmasi">Anda yakin dengan data ini ?</span>

								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-xs btn-primary" id="setujukonfirmasibutton">Yakin</button>
									<button type="button" class="btn btn-xs btn-danger" data-dismiss="modal">Batal</button>
								</div>
							</div>
						</div>
					</div>
					<div id="ModalChangeRole" class="modal fade" tabindex="-1">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header bg-primary">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h3 class="modal-title">Ganti Akses Role</h3>
								</div>

								<div class="modal-body">
									<div class="form-group">
										<label for="" class="col-sm-4 control-label" style="margin-right: -25px;">Pilih Akses Role</label>
										<div class="col-sm-8">
											<select class="select-size-xs" id="newRoles" name="newRoles">
												<option></option>
												<?php foreach ($data_Akses as $akses) {?>
													<option value="<?php echo $akses["akses_id"];?>" <?php echo (($akses["akses_id"]==$current_akses_id)?"selected":"");?>><?php echo $akses["akses_nama"];?></option>	
												<?php }?>
											</select>
										</div>
									</div>

								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-xs btn-danger" data-dismiss="modal">Batal</button>
									<button type="button" class="btn btn-xs btn-primary" id="yaChangeRole" data-name="yaChangeRole">OK</button>
								</div>
							</div>
						</div>
					</div>
					<div id="modal_confirm_error" class="modal fade" data-backdrop="false" data-keyboard="false" tabindex="-1">
						<div class="modal-dialog modal-xs">
							<div class="modal-content">
								<div class="modal-header bg-danger">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h6 class="modal-title">Konfirmasi</h6>
								</div>

								<div class="modal-body">
									<h6 class="text-semibold">Terjadi Kesalahan</h6>
									<p>Data yang anda masukan ..</p>

								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-danger">Save changes</button>
								</div>
							</div>
						</div>
					</div>
					<!-- /default modal -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
	<?php 
	$notif = $this->session->flashdata('notify');
	$notif_isShow = 0;
	$notif_tipe = 0;
	$notif_message = '';
	$notif_url = '';
	if(isset($notif)) {
		$notif_isShow = ((isset($notif))?1:0);
		$notif_tipe = (($notif["status"]=="success")?1:0);
		$notif_message = (($notif["message"]=="")?"":$notif["message"]);
		$notif_url = (($notif)?"":$notif["url"]);
	}


	?>
	<script type="text/javascript">
		$(function() {
			var notif_isShow 	= '<?php echo $notif_isShow;?>';
			var notif_tipe 	= '<?php echo $notif_tipe;?>';
			var notif_message = '<?php echo $notif_message;?>';
			var notif_url 		= '<?php echo $notif_url;?>';
			if(parseInt(notif_isShow)==1){
				set_notif_apps(parseInt(notif_tipe));
					//set_notif_desktop(parseInt(notif_tipe));
				}
			});



		function set_notif_apps(tipe = 1){
			if(tipe == 1){
				new PNotify({
					title: 'Notifikasi Ropeg',
					text: 'Selamat data anda Berhasil disimpan.',
					addclass: 'bg-success'
				});
			}else{
				new PNotify({
					title: 'Notifikasi Ropeg',
					text: 'Maaf data anda Gagal tersimpan.',
					addclass: 'bg-danger'
				});
			}

		}
		function set_notif_desktop(tipe = 1, isClick = false, url = base_url){
			if(tipe == 1){
				PNotify.desktop.permission();
				(new PNotify({
					title: 'Notifikasi Ropeg',
					type: 'success',
					text: 'Selamat data anda Berhasil disimpan.',
					desktop: {
						desktop: true,
						icon: base_url+'assets/images/pnotify/success.png'
					}
				})
				).get().on('click', function(e) {
					if(isClick==true){
						if ($('.ui-pnotify-closer, .ui-pnotify-sticker, .ui-pnotify-closer *, .ui-pnotify-sticker *').is(e.target)) return;
						window.location.href = url;
					}
				});
			}else{
				PNotify.desktop.permission();
				(new PNotify({
					title: 'Notifikasi Ropeg',
					type: 'danger',
					text: 'Maaf data anda Gagal tersimpan.',
					desktop: {
						desktop: true,
						icon: base_url+'assets/images/pnotify/danger.png'
					}
				})
				).get().on('click', function(e) {
					if(isClick==true){
						if ($('.ui-pnotify-closer, .ui-pnotify-sticker, .ui-pnotify-closer *, .ui-pnotify-sticker *').is(e.target)) return;
						window.location.href = url;
					}
				});
			}

		}

		function GetMonthIndInt(month) {
			switch (month) {
				case 1:
				return "Januari"
				break;
				case 2:
				return "Februari"
				break;
				case 3:
				return "Maret"
				break;
				case 4:
				return "April"
				break;
				case 5:
				return "Mei"
				break;
				case 6:
				return "Juni"
				break;
				case 7:
				return "Juli"
				break;
				case 8:
				return "Agustus"
				break;
				case 9:
				return "September"
				break;
				case 10:
				return "Oktober"
				break;
				case 11:
				return "November"
				break;
				case 12:
				return "Desember"
				break;
				default:
				return "Undefined"
			}
		}

		function GantiAkses() {
			$("#ModalChangeRole").modal("show");
			$('#modalChangeRole').modal('show');
			$('#yaChangeRole').click(function () {
				var newRoles = $("#newRoles").val();
				var newRolesName = $("#newRoles option:selected").text();
				$.post(base_url + 'home/auth/changeroles', { newRoles: newRoles, newRolesName: newRolesName  }).done(function (data) {
					$('#ModalChangeRole').modal('hide');
					var menu_url = data.menu_url;
					// alert(menu_url);
					// window.open(base_url+""+menu_url, "_self");
					window.open(base_url+"home/dashboard");

				});
			});
		}

	</script>
</body>
</html>
