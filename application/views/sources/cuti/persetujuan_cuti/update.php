
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Persetujuan Cuti</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<legend class="text-bold">Data Pegawai</legend>
					<div class="form-group form-group-xs" style="margin-bottom: 0px;">
						<label class="col-md-3 control-label">NIP</label>
						<label class="control-label col-xs-9"><?php echo $cuti['pegawai_nip']; ?></label>
					</div>
					<div class="form-group form-group-xs" style="margin-bottom: 0px;">
						<label class="col-md-3 control-label">Nama</label>
						<label class="control-label col-xs-9"><?php echo $cuti['pegawai_nama']; ?></label>
					</div>
					<div class="form-group form-group-xs" style="margin-bottom: 0px;">
						<label class="col-md-3 control-label">Alasan Cuti</label>
						<label class="control-label col-xs-9"><?php echo $cuti['cuti_detail_alasan']; ?></label>
					</div>
					<div class="form-group form-group-xs" style="margin-bottom: 0px;">
						<label class="col-md-3 control-label">Tanggal Mulai Cuti</label>
						<label class="control-label col-xs-9"><?php echo $cuti['cuti_detail_tanggal_mulai']; ?></label>
					</div>
					<div class="form-group form-group-xs" style="margin-bottom: 0px;">
						<label class="col-md-3 control-label">Tanggal Selesai Cuti</label>
						<label class="control-label col-xs-9"><?php echo $cuti['cuti_detail_tanggal_selesai']; ?></label>
					</div>
					<div class="form-group form-group-xs" style="margin-bottom: 0px;">
						<label class="col-md-3 control-label">Tahun Cuti</label>
						<label class="control-label col-xs-9"><?php echo $cuti['cuti_detail_tahun']; ?></label>
					</div>
					<div class="form-group form-group-xs" style="margin-bottom: 0px;">
						<label class="col-md-3 control-label">Alamat Selama Menjalankan Cuti</label>
						<label class="control-label col-xs-9"><?php echo $cuti['cuti_detail_alamat']; ?></label>
					</div>
					<div class="form-group form-group-xs" style="margin-bottom: 0px;">
						<label class="col-md-3 control-label">No Telepon Selama Menjalankan Cuti</label>
						<label class="control-label col-xs-9"><?php echo $cuti['cuti_detail_telp']; ?></label>
					</div>
					</br>
				</div>
				<div class="col-md-12">
					<legend class="text-bold">Keputusan Atasan</legend>
					<!-- <div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Pilih Atasan</label>
						<div class="col-md-9">
							<select name="cuti_detail_atasan_nip" id="cuti_detail_atasan_nip" data-placeholder="Pilih Atasan" class="select-size-xs wajib" onchange="setAtasan(this)">
								<option></option>
								<?php foreach ($pegawai as $i) {

									echo '<option value="'.$i["pegawai_nip"].'" data-nip="'.$i["pegawai_nip"].'" data-nama="'.$i["pegawai_nama_lengkap"].'" data-pangkat="-" data-eselon="'.$i["pegawai_esel_kini"].'" data-jabatan="'.$i["pegawai_nama_jabatan"].'"  data-unit="'.$i["pegawai_nama_unit_kerja"].'">'.$i["pegawai_nip"]." - ".$i["pegawai_nama_lengkap"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs info-atasan" style="margin-bottom: 0px;">
						<label class="control-label col-xs-3">NIP</label>
						<label class="control-label col-xs-9" id="nip_atasan">-</label>
					</div>
					<div class="form-group form-group-xs info-atasan" style="margin-bottom: 0px;">
						<label class="control-label col-xs-3">Nama</label>
						<label class="control-label col-xs-9" id="nama_atasan">-</label>
					</div>
					<div class="form-group form-group-xs info-atasan" style="margin-bottom: 0px;">
						<label class="control-label col-xs-3">Pangkat</label>
						<label class="control-label col-xs-9" id="angkat_atasan">-</label>
					</div>
					<div class="form-group form-group-xs info-atasan" style="margin-bottom: 0px;">
						<label class="control-label col-xs-3">Jabatan</label>
						<label class="control-label col-xs-9" id="jabatan_atasan"><-/label>
					</div>
					<div class="form-group form-group-xs info-atasan" style="margin-bottom: 0px;">
						<label class="control-label col-xs-3">Unit Kerja</label>
						<label class="control-label col-xs-9" id="unit_kerja_atasan">-</label>
					</div>
					<div class="form-group form-group-xs info-atasan" style="margin-bottom: 0px;">
						<label class="control-label col-xs-3">Eselon</label>
						<label class="control-label col-xs-9" id="eselon_atasan">-</label>
					</div> -->
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Status Atasan</label>
						<div class="col-lg-9">
							<select data-placeholder="Pilih Status" class="select-size-xs" name="cuti_detail_status_atasan" id="cuti_detail_status_atasan">
								<option></option>
								<option value="1">DISETUJUI</option>
								<option value="2">PERUBAHAN</option>
								<option value="3">DITANGGUHKAN</option>
								<option value="4">TIDAK DISETUJUI</option>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Keterangan Atasan</label>
						<div class="col-lg-9">
							<textarea class="form-control input-xs wajib" name="cuti_detail_keterangan_atasan" id="cuti_detail_keterangan_atasan" placeholder="Keterangan atasan" style="width:750px;height:50px;"></textarea>
						</div>
					</div>
				</div>
				<!-- <div class="col-md-6">
					<legend class="text-bold">Keputusan Pejabat</legend>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Pilih Pejabat</label>
						<div class="col-md-9">
							<select name="cuti_detail_pejabat_nip" id="cuti_detail_pejabat_nip" data-placeholder="Pilih Pejabat" class="select-size-xs wajib" onchange="setPejabat(this)">
								<option value=""></option>
								<?php foreach ($pegawai as $i) {

									echo '<option value="'.$i["pegawai_nip"].'" data-nip="'.$i["pegawai_nip"].'" data-nama="'.$i["pegawai_nama_lengkap"].'" data-pangkat="-" data-eselon="'.$i["pegawai_esel_kini"].'" data-jabatan="'.$i["pegawai_nama_jabatan"].'"  data-unit="'.$i["pegawai_nama_unit_kerja"].'">'.$i["pegawai_nip"]." - ".$i["pegawai_nama_lengkap"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs info-pejabat" style="margin-bottom: 0px;">
						<label class="control-label col-xs-3">NIP</label>
						<label class="control-label col-xs-9" id="nip_pejabat">-</label>
					</div>
					<div class="form-group form-group-xs info-pejabat" style="margin-bottom: 0px;">
						<label class="control-label col-xs-3">Nama</label>
						<label class="control-label col-xs-9" id="nama_pejabat">-</label>
					</div>
					<div class="form-group form-group-xs info-pejabat" style="margin-bottom: 0px;">
						<label class="control-label col-xs-3">Pangkat</label>
						<label class="control-label col-xs-9" id="angkat_pejabat">-</label>
					</div>
					<div class="form-group form-group-xs info-pejabat" style="margin-bottom: 0px;">
						<label class="control-label col-xs-3">Jabatan</label>
						<label class="control-label col-xs-9" id="jabatan_pejabat">-</label>
					</div>
					<div class="form-group form-group-xs info-pejabat" style="margin-bottom: 0px;">
						<label class="control-label col-xs-3">Unit Kerja</label>
						<label class="control-label col-xs-9" id="unit_kerja_pejabat">-</label>
					</div>
					<div class="form-group form-group-xs info-pejabat" style="margin-bottom: 0px;">
						<label class="control-label col-xs-3">Eselon</label>
						<label class="control-label col-xs-9" id="eselon_pejabat">-</label>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Status Pejabat</label>
						<div class="col-lg-9">
							<select data-placeholder="Pilih Status" class="select-size-xs" name="cuti_detail_status_pejabat" id="cuti_detail_status_pejabat">
								<option value=""></option>
								<option value="1">DISETUJUI</option>
								<option value="2">PERUBAHAN</option>
								<option value="3">DITANGGUHKAN</option>
								<option value="4">TIDAK DISETUJUI</option>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Keterangan Pejabat</label>
						<div class="col-lg-9">
							<textarea rows="5" cols="5" class="form-control input-xs wajib" name="cuti_detail_keterangan_pejabat" id="cuti_detail_keterangan_pejabat" placeholder="Keterangan pejabat"></textarea>
						</div>
					</div>
				</div> -->
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
					<a href="<?php echo base_url();?>cuti/persetujuan_cuti" class="btn btn-xs btn-warning">Kembali</a>
					<input type="hidden" name="cuti_detail_id" id="cuti_detail_id"  class="form-control input-xs wajib" type="text" value="<?php echo $cuti["cuti_detail_id"];?>" >
					
				</div>
			</div>

		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$(".info-atasan").hide();
		$(".info-pejabat").hide();
	});

	function setAtasan(ini) {
		var TenderRUPId = $('option:selected', ini).attr('data-TenderRUPId');
		var nip = $('option:selected', ini).attr('data-nip');
		var nama = $('option:selected', ini).attr('data-nama');
		var pangkat = $('option:selected', ini).attr('data-pangkat');
		var eselon = $('option:selected', ini).attr('data-eselon');
		var jabatan = $('option:selected', ini).attr('data-jabatan');
		var unit = $('option:selected', ini).attr('data-unit');

		$('#nip_atasan').html("").html(nip);
		$('#nama_atasan').html("").html(nama);
		$('#pangkat_atasan').html("").html(pangkat);
		$('#eselon_atasan').html("").html(eselon);
		$('#jabatan_atasan').html("").html(jabatan);
		$('#unit_atasan').html("").html(unit);
		$(".info-atasan").show();
	}

	function setPejabat(ini) {
		var TenderRUPId = $('option:selected', ini).attr('data-TenderRUPId');
		var nip = $('option:selected', ini).attr('data-nip');
		var nama = $('option:selected', ini).attr('data-nama');
		var pangkat = $('option:selected', ini).attr('data-pangkat');
		var eselon = $('option:selected', ini).attr('data-eselon');
		var jabatan = $('option:selected', ini).attr('data-jabatan');
		var unit = $('option:selected', ini).attr('data-unit');

		$('#nip_pejabat').html("").html(nip);
		$('#nama_pejabat').html("").html(nama);
		$('#pangkat_pejabat').html("").html(pangkat);
		$('#eselon_pejabat').html("").html(eselon);
		$('#jabatan_pejabat').html("").html(jabatan);
		$('#unit_pejabat').html("").html(unit);
		$(".info-pejabat").show();
	}
</script>
