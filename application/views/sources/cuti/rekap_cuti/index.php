<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
    <div class="panel-heading">
        <h6 class="panel-title">Data Rekap Cuti</h6>
        <!-- <div class="heading-elements">
            <div class="heading-btn">
                <a href="<?php echo base_url().'cuti/rekap_cuti/create';?>" class="btn btn-primary btn-labeled btn-xs"><b><i class=" icon-plus3"></i></b> Tambah Data rekap Cuti</a>
            </div>
        </div> -->
    </div>

    <div class="panel-body">
        <table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive ropeg_table" id="list_data">
            <thead>
                <tr>
                    <th class="text-center">Nama Pegawai</th>
                    <th class="text-center">Jenis Cuti</th>
                    <th class="text-center">Alasan Cuti</th>
                    <th class="text-center">Tanggal Cuti</th>
                    <!-- <th class="text-center" style="width: 150px;min-width: 150px;">Status</th> -->
                    <th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
                </tr>
            </thead>
            
        </table>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#list_data').dataTable( {
            "processing": true,
            "serverSide": true,
            "bServerSide": true,
            "sAjaxSource": base_url+"cuti/rekap_cuti/list_data",
            "aaSorting": [],
            "order": [],
            "iDisplayLength": 10,
            "aoColumns": [
            { "bSortable": false, "sClass": "text-left" },
            { "bSortable": false, "sClass": "text-left" },
            { "bSortable": false, "sClass": "text-left" },
            { "bSortable": false, "sClass": "text-center" },
            // { "bSortable": false, "sClass": "text-center" },
            { "bSortable": false, "sClass": "text-center" },
            ],
            "fnDrawCallback": function () {
                set_default_datatable();
            },
        });
    });
</script>
