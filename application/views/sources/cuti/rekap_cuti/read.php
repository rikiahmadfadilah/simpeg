
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Detail Rekap Cuti</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">NIP Pegawai</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $cuti["NIP"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Nama Pegawai</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $cuti["NAMA"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Jenis Cuti</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $cuti["jenis_cuti_name"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Alasan Cuti</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $cuti["cuti_detail_alasan"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Tanggal Mulai Cuti</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo dateEnToId($cuti["cuti_detail_tanggal_mulai"], 'd F Y');?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Tanggal Selesai Cuti</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo dateEnToId($cuti["cuti_detail_tanggal_selesai"], 'd F Y');?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Lama Cuti</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php if($thn > 0){echo $thn; echo ' Tahun ';}else{} if($bln > 0){echo $bln; echo ' Bulan ';}else{} if($mng > 0){echo $mng; echo ' Minggu ';}else{} if($hr > 0){echo $hr; echo ' Hari ';}else{} ?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Tahun Cuti</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $cuti["cuti_detail_tahun"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Alamat Selama Menjalankan Cuti</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $cuti["cuti_detail_alamat"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">No Telepon Selama Menjalankan Cuti</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $cuti["cuti_detail_telp"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">NIP Atasan</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $cuti["cuti_detail_atasan_nip"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Nama Atasan</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $cuti["nama_atasan"];?>" readonly="readonly">
						</div>
					</div>
					<!-- <div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Status Atasan</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $cuti["cuti_detail_status_atasan_name"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Keterangan Atasan</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $cuti["cuti_detail_keterangan_atasan"];?>" readonly="readonly">
						</div>
					</div> -->
					<!-- <div class="form-group form-group-xs">
						<label class="col-md-3 control-label">NIP Pejabat</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $cuti["cuti_detail_pejabat_nip"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Nama Pejabat</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $cuti["nama_pejabat"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Status Pejabat</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $cuti["cuti_detail_status_pejabat_name"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Keterangan Pejabat</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $cuti["cuti_detail_keterangan_pejabat"];?>" readonly="readonly">
						</div>
					</div> -->
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<a href="<?php echo base_url();?>cuti/rekap_cuti" class="btn btn-xs btn-warning">Kembali</a>
					<input type="hidden" name="cuti_detail_id" id="cuti_detail_id"  class="form-control input-xs wajib" type="text" value="<?php echo $cuti["cuti_detail_id"];?>" >
					
				</div>
			</div>

		</form>
	</div>
</div>
<script type="text/javascript">
	
	$(document).ready(function() {
		

	});
</script>
