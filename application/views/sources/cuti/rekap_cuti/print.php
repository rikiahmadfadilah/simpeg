<?php
 	header("Content-Type: application/vnd.ms-word");
    header("Expires: 0");
    header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
    $pegawai_nip = $cuti["pegawai_nip"];
    $jenis_cuti = $cuti["jenis_cuti_name"];
    header("Content-disposition: attachment; filename=$pegawai_nip-$jenis_cuti.doc");
?>
<html>
	<style>
		.besar{
			text-transform: uppercase;
		}
		.kecil{
			text-transform: lowercase;
		}
		.awal_besar{
			text-transform: capitalize;
		}
	</style>
	<center><font style="font-size: 18.0pt; font-weight: bold; font-family: Arial,sans-serif;">KEMENTERIAN KELAUTAN DAN PERIKANAN</font></center>
	<center><font style="font-size: 15.0pt; font-weight: bold; font-family: Times New Roman,serif;"><?php echo $cuti["unit_kerja_nama"] ?></font></center>
	<hr width="100%">
	<br>
	<p style="text-align: right; font-size: 12.0pt; font-family: Arial,sans-serif;"><?php echo dateEnToID($cuti["cuti_detail_creete_date"], 'd M Y'); ?></p>
	<center><font style="font-size: 12.0pt; font-weight: bold; font-family: Arial,sans-serif;" class="besar"><u>SURAT IZIN <?php echo $cuti["jenis_cuti_name"] ?></u></font></center>
	<center><font style="font-size: 12.0pt; font-family: Arial,sans-serif;" class="besar">Nomor : 03/MEN.3/KPTS/KP.616/XII/2019</font></center>
	<br>
	<br>
	<font style="font-size: 12.0pt; text-align: justify; text-indent: -18.0pt;">1.	Diberikan <?php echo $cuti["jenis_cuti_name"] ?> Kepada Pegawai Negeri Sipil :</font>
	<br>
	<br>
	<table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="597" style="width:447.95pt;margin-left:23.4pt;border-collapse:collapse;mso-padding-alt:
 0mm 5.4pt 0mm 5.4pt">
 <tbody><tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes;
  height:106.45pt">
  <td width="28" valign="top" style="width:21.1pt;padding:0mm 5.4pt 0mm 5.4pt;
  height:106.45pt">
  <p class="MsoNormal" style="text-align:justify"><span lang="SV" style="font-family:
  &quot;Arial&quot;,&quot;sans-serif&quot;;mso-ansi-language:SV">a.<o:p></o:p></span></p>
  <p class="MsoNormal" style="text-align:justify"><span lang="SV" style="font-family:
  &quot;Arial&quot;,&quot;sans-serif&quot;;mso-ansi-language:SV">b.<o:p></o:p></span></p>
  <p class="MsoNormal" style="text-align:justify"><span lang="SV" style="font-family:
  &quot;Arial&quot;,&quot;sans-serif&quot;;mso-ansi-language:SV">c.<o:p></o:p></span></p>
  <p class="MsoNormal" style="text-align:justify"><span lang="SV" style="font-family:
  &quot;Arial&quot;,&quot;sans-serif&quot;;mso-ansi-language:SV">d.<o:p></o:p></span></p>
  <p class="MsoNormal" style="text-align:justify"><span lang="SV" style="font-family:
  &quot;Arial&quot;,&quot;sans-serif&quot;;mso-ansi-language:SV">e.<o:p></o:p></span></p>
  </td>
  <td width="167" valign="top" style="width:125.45pt;padding:0mm 5.4pt 0mm 5.4pt;
  height:106.45pt">
  <p class="MsoNormal" style="text-align:justify"><span lang="PT-BR" style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;mso-ansi-language:PT-BR">N a m a <o:p></o:p></span></p>
  <p class="MsoNormal" style="text-align:justify"><span lang="PT-BR" style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;mso-ansi-language:PT-BR">N I P<o:p></o:p></span></p>
  <p class="MsoNormal" style="text-align:justify"><span lang="PT-BR" style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;mso-ansi-language:PT-BR">Pangkat,
  Gol/ Ruang<o:p></o:p></span></p>
  <p class="MsoNormal" style="text-align:justify"><span lang="SV" style="font-family:
  &quot;Arial&quot;,&quot;sans-serif&quot;;mso-ansi-language:SV">Jabatan <o:p></o:p></span></p>
  <p class="MsoNormal" style="text-align:justify"><span lang="SV" style="font-family:
  &quot;Arial&quot;,&quot;sans-serif&quot;;mso-ansi-language:SV">Unit Kerja<o:p></o:p></span></p>
  </td>
  <td width="402" valign="top" style="width:301.4pt;padding:0mm 5.4pt 0mm 5.4pt;
  height:106.45pt">
  <p class="MsoNormal" style="text-align:justify;tab-stops:12.6pt"><span lang="SV" style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;mso-ansi-language:SV">:<span style="mso-tab-count:1">&nbsp;&nbsp; </span></span><strong><span lang="IN" style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;mso-ansi-language:IN;font-weight:
  normal;mso-bidi-font-weight:bold"><?php echo $cuti["pegawai_nama_lengkap"] ?></span></strong><span lang="IN" style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;mso-ansi-language:IN"><o:p></o:p></span></p>
  <p class="MsoNormal" style="text-align:justify;tab-stops:12.6pt"><span lang="SV" style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;mso-ansi-language:SV">:<span style="mso-tab-count:1">&nbsp;&nbsp; </span></span><strong><span lang="IN" style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;mso-ansi-language:IN;font-weight:
  normal;mso-bidi-font-weight:bold"><?php echo $cuti["pegawai_nip"] ?></span></strong><span lang="IN" style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;mso-ansi-language:IN"><o:p></o:p></span></p>
  <p class="MsoNormal" style="text-align:justify;tab-stops:12.6pt"><span lang="SV" style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;mso-ansi-language:SV">:<span style="mso-tab-count:1">&nbsp;&nbsp; </span></span><span lang="IN" style="font-family:
  &quot;Arial&quot;,&quot;sans-serif&quot;;mso-ansi-language:IN"><?php echo $cuti["golongan_pangkat"] ?> ( <?php echo $cuti["golongan_nama"] ?> )<o:p></o:p></span></p>
  <p class="MsoNormal" style="margin-top:0mm;margin-right:-5.4pt;margin-bottom:
  0mm;margin-left:14.05pt;margin-bottom:.0001pt;text-indent:-14.05pt;
  tab-stops:14.05pt"><span lang="SV" style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
  mso-ansi-language:SV">:<span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp; </span></span><span lang="IN" style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;mso-ansi-language:IN"><?php echo $cuti["pegawai_nama_jabatan"] ?><o:p></o:p></span></p>
  <p class="MsoNormal" style="margin-left:14.05pt;text-indent:-14.05pt;
  tab-stops:14.05pt"><span lang="SV" style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
  mso-ansi-language:SV">: <span style="mso-tab-count:1">&nbsp;&nbsp; </span></span><span lang="IN" style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;mso-ansi-language:IN"><?php echo $cuti["unit_kerja_nama"] ?><o:p></o:p></span></p>
  </td>
 </tr>
</tbody></table>
<br>
<br>
<p class=MsoBodyText style='margin-left:18.0pt;text-indent:-18.0pt;tab-stops:
18.0pt'><span lang=SV style='mso-ansi-language:SV'><span style='mso-tab-count:
1'></span>Selama </span><span lang=IN style='mso-ansi-language:IN'>1
(satu) hari</span><span lang=SV style='mso-ansi-language:SV'> kerja, terhitung
mulai tanggal </span><span lang=IN style='mso-ansi-language:IN'><?php echo dateEnToID($cuti["cuti_detail_tanggal_mulai"], 'd M Y') ?>
 s.d. <?php echo dateEnToID($cuti["cuti_detail_tanggal_selesai"], 'd M Y') ?></span><span lang=SV style='mso-ansi-language:SV'> dengan
ketentuan sebagai berikut :<o:p></o:p></span></p>

<ol style='margin-top:0mm' start=1 type=a>
 <li class=MsoNormal style='text-align:justify;mso-list:l26 level1 lfo20'><span
     lang=SV style='font-family:"Arial","sans-serif";mso-ansi-language:SV'>Sebelum
     menjalankan </span><span lang=IN style='font-family:"Arial","sans-serif";
     mso-ansi-language:IN'><?php echo $cuti["jenis_cuti_name"]; ?></span><span lang=SV
     style='font-family:"Arial","sans-serif";mso-ansi-language:SV'> wajib
     menyerahkan pekerjaannya kepada atasan langsungnya atau pejabat lain yang
     telah ditentukan.<o:p></o:p></span></li>
</ol>

<ol style='margin-top:0mm' start=2 type=a>
 <li class=MsoNormal style='text-align:justify;mso-list:l26 level1 lfo20'><span
     lang=SV style='font-family:"Arial","sans-serif";mso-ansi-language:SV'>Setelah
     selesai menjalankan <?php echo $cuti["jenis_cuti_name"]; ?> </span><span lang=IN style='font-family:"Arial","sans-serif";
     mso-ansi-language:IN'></span><span lang=SV style='font-family:"Arial","sans-serif";
     mso-ansi-language:SV'> wajib melaporkan diri kepada atasan langsungnya dan
     bekerja kembali sebagaimana biasa.<o:p></o:p></span></li>
</ol>

<p class=MsoNormal style='margin-left:18.0pt;text-align:justify;text-indent:
-18.0pt;tab-stops:18.0pt'><span lang=SV style='font-family:"Arial","sans-serif";
mso-ansi-language:SV'>2.<span style='mso-tab-count:1'>   </span>Demikian surat
izin </span><span lang=IN style='font-family:"Arial","sans-serif";
mso-ansi-language:IN'><?php echo $cuti["jenis_cuti_name"]; ?></span><span lang=SV style='font-family:"Arial","sans-serif";
mso-ansi-language:SV'> ini dibuat untuk dapat dipergunakan sebagaimana
mestinya.<o:p></o:p></span></p>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='margin-left:339.3pt;border-collapse:collapse;mso-yfti-tbllook:1184;
 mso-padding-alt:0mm 5.4pt 0mm 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td width=311 valign=top style='width:233.6pt;padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=IN
  style='font-family:"Arial","sans-serif";mso-ansi-language:IN'><?php echo $cuti["jabatan_atasan"]; ?><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1'>
  <td width=311 valign=top style='width:233.6pt;padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoNormal><span lang=IN style='font-family:"Tahoma","sans-serif";
  mso-ansi-language:IN'><o:p>&nbsp;</o:p></span></p>
  <p class=MsoNormal><span lang=IN style='font-family:"Tahoma","sans-serif";
  mso-ansi-language:IN'><o:p>&nbsp;</o:p></span></p>
  <p class=MsoNormal><span lang=IN style='font-family:"Tahoma","sans-serif";
  mso-ansi-language:IN'><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2;mso-yfti-lastrow:yes'>
  <td width=311 valign=top style='width:233.6pt;padding:0mm 5.4pt 0mm 5.4pt'>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><u><span lang=IN
  style='font-family:"Arial","sans-serif";mso-ansi-language:IN'><?php echo $cuti["nama_atasan"] ?></span></u></b><span lang=IN style='font-family:"Arial","sans-serif";
  mso-ansi-language:IN'><o:p></o:p></span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal style='text-align:justify'><span lang=PT-BR
style='font-family:"Arial","sans-serif";mso-ansi-language:PT-BR'>Tembusan Yth :
</span></p>
</html>