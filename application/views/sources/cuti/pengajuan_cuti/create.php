<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Tambah Data Pengajuan Cuti</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Pilih Pegawai</label>
						<div class="col-md-9">
							<select name="cuti_detail_pegawai_nip_header" id="cuti_detail_pegawai_nip_header" data-placeholder="Pilih Pegawai" class="form-control" onchange="set_other_properies(this);">
								<option></option>
							</select>
							<input type="hidden" name="cuti_detail_pegawai_nip" id="cuti_detail_pegawai_nip">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Pilih Jenis Cuti</label>
						<div class="col-md-9">
							<select name="cuti_detail_jenis_id" id="cuti_detail_jenis_id" data-placeholder="Pilih Jenis Cuti" class="select-size-xs wajib">
								<option></option>
								<?php foreach ($jenis_cuti as $i) {
									echo '<option value="'.$i["jenis_cuti_id"].'">'.$i["jenis_cuti_name"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs" id="anak_ke" style="display: none;">
						<label class="col-md-3 control-label">Anak Ke-</label>
						<div class="col-md-9">
							<select name="cuti_detail_anak_ke" id="cuti_detail_anak_ke" data-placeholder="Anak Ke-" class="select-size-xs wajib">
								<option value="1">Ke- 1</option>
								<option value="2">Ke- 2</option>
								<option value="3">Ke- 3</option>
								<option value="4">Ke- 4</option>
								<option value="5">Ke- 5</option>
								<option value="6">Ke- 6</option>
								<option value="7">Ke- 7</option>
								<option value="8">Ke- 8</option>
								<option value="9">Ke- 9</option>
								<option value="10">Ke- 10</option>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Alasan Cuti</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" placeholder="" name="cuti_detail_alasan" id="cuti_detail_alasan">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Lama Cuti</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" placeholder="" name="cuti_detail_waktu" id="cuti_detail_waktu">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Satuan Waktu</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" placeholder="" name="cuti_detail_satuan_waktu" id="cuti_detail_satuan_waktu">
						</div>
					</div>
					<div class="form-group form-group-xs" id="cuti_tahunan_tgl_mulai">
						<label class="col-lg-3 control-label">Tanggal Mulai Cuti</label>
						<div class="col-lg-6">
							<input type="text" class="form-control input-xs wajib pickmulaicuti" name="cuti_detail_tanggal_mulai" id="cuti_detail_tanggal_mulai">
						</div>
					</div>
					<div class="form-group form-group-xs" id="cuti_tahunan_tgl_selesai">
						<label class="col-lg-3 control-label">Tanggal Selesai Cuti</label>
						<div class="col-lg-6">
							<input type="text" class="form-control input-xs wajib pickselesaicuti" name="cuti_detail_tanggal_selesai" id="cuti_detail_tanggal_selesai">
						</div>
					</div>
					<div class="form-group form-group-xs" id="selain_cuti_tahunan" style="display: none;">
						<label class="col-lg-3 control-label">Tanggal Cuti</label>
						<div class="col-lg-9"
						<div id="input_cuti">
							<input type="text" class="form-control input-xs wajib" placeholder="Masukkan Banyaknya Cuti Terlebihdahulu" name="" id="data_kosong" disabled>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Alamat Selama Menjalankan Cuti</label>
						<div class="col-lg-9">
							<textarea rows="5" cols="5" class="form-control input-xs wajib" name="cuti_detail_alamat" id="cuti_detail_alamat" placeholder="" style="height: 50px"></textarea>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">No Telepon Selama Menjalankan Cuti</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib number" placeholder="" name="cuti_detail_telp" id="cuti_detail_telp">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Pilih Atasan</label>
						<div class="col-md-9">
							<select name="cuti_detail_atasan_nip_header" id="cuti_detail_atasan_nip_header" data-placeholder="Pilih Atasan" class="form-control" onchange="set_other_properies_atasan(this);">
							</select>
							<input type="hidden" class="form-control input-xs wajib" placeholder="" name="cuti_detail_atasan_nip" id="cuti_detail_atasan_nip">
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-xs btn-primary">Simpan</button>
					<a href="<?php echo base_url();?>cuti/pengajuan_cuti" class="btn btn-xs btn-warning">Kembali</a>
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {

		$('.pickmulaicuti').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			min: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
	});
	$(document).ready(function() {

		$('.pickselesaicuti').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			min: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
	});

	$('#banyak_cuti').keyup(function(event) {

		$('#data_kosong').hide();
		$('#input_cuti').html("");
		for(var i= 0; i < parseInt($('#banyak_cuti').val()); i++){
			$('#input_cuti').append('<input type="text" class="form-control input-xs wajib date" name="tgl_cuti[]" id="cuti_detail_tanggal_mulai_'+i+'"><br>');
		}

		$('.date').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			min: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmm yyyy'
		});
	});

	// $('#cuti_detail_pegawai_nip_header').change(function(event) {
	// 	var unit_parrent = $('option:selected', this).attr('unit_parrent');
	// 	$.ajax({
	// 		url: '<?php echo base_url();?>cuti/pengajuan_cuti/get_atasan',
	// 		type: 'POST',
	// 		dataType: 'json',
	// 		data: {unit_parrent: unit_parrent},
	// 		success: function (data) {
	// 			$.each(data, function(index, val) {
	// 				console.log(val.pegawai_nama);
	// 				$('#cuti_detail_atasan_nip').append('<option value="'+val.pegawai_nip+'">'+val.pegawai_nama+'</option>');
	// 			});
	// 		}
	// 	});
	// });
	$('#cuti_detail_jenis_id').change(function() {
		var jc = $('#cuti_detail_jenis_id').val();
		if(jc == 1){
			$('#anak_ke').hide();
			$('#cuti_detail_satuan_waktu').val("Hari");
			$('#cuti_detail_satuan_waktu').hide();
		}else if(jc == 4){
			$('#anak_ke').show();
			$('#cuti_detail_satuan_waktu').show();
			$('#cuti_detail_satuan_waktu').val("");
			$('#cuti_detail_satuan_waktu').prop( "readonly", false );
		}else{
			$('#anak_ke').hide();
			$('#cuti_detail_satuan_waktu').show();
			$('#cuti_detail_satuan_waktu').val("");
			$('#cuti_detail_satuan_waktu').prop( "readonly", false );
			// $('#cuti_detail_satuan_waktu').prop("readonly", true );
		}
	});

	$("#cuti_detail_pegawai_nip_header,#cuti_detail_atasan_nip_header").select2({
		ajax: {
			url: base_url+"cuti/pengajuan_cuti/getDataListPegawai",
			dataType: 'json',
			delay: 250,
			data: function (params) {
				return {
					q: params.term, 
					page: params.page,
					
				};
			},
			processResults: function (data, params) {
				params.page = params.page || 1;

				return {
					results: data.items,
					pagination: {
						more: (params.page * 5) < data.total_count
					}
				};
			},
			cache: true
		},
		containerCssClass: 'select-xs',
		placeholder: 'Pencarian Pegawai',
		escapeMarkup: function (markup) { return markup; }, 
		minimumInputLength: 2,
		templateResult: formatRepo,
		templateSelection: formatRepoSelection,
	});

	function formatRepo (repo) {
		if (repo.loading) {
			return repo.text;
		}

		var markup = "<div class='select2-result-repository clearfix'>" +
		"<div class='select2-result-repository__avatar'><img src='<?php echo base_url().'assets/images/pegawai/'?>"+repo.pegawai_image_path + "' /></div>" +
		"<div class='select2-result-repository__meta'>" +
		"<div class='select2-result-repository__title'>" + repo.pegawai_nama + "</div>"+
		"<div class='select2-result-repository__description'>NIP : " + repo.pegawai_nip + "<br>Jabatan : " + repo.pegawai_nama_jabatan + "<br>Unit Kerja : " + repo.unit_kerja_nama + "</div>"+
		"</div>";
		
		return markup;
	}

	function formatRepoSelection (repo) {
		return repo.pegawai_nama || repo.text;
	}

	function set_other_properies(ini){
		var pegawai_id = $(ini).val();
		var update1 = $('#isUpdate').val();
		$.ajax({
			url: base_url+'cuti/pengajuan_cuti/getDataPegawai',
			type: "post",
			dataType: 'json',
			data: { pegawai_id: pegawai_id},
			success: function (data) {
					if(data.pegawai!=null){
						$('#cuti_detail_pegawai_nip').val(data.pegawai.NIP);
					}else{
						$('#cuti_detail_pegawai_nip').val('');
					}
			}
		});
	}

	function set_other_properies_atasan(ini){
		var pegawai_id = $(ini).val();
		var update1 = $('#isUpdate').val();
		$.ajax({
			url: base_url+'cuti/pengajuan_cuti/getDataPegawai',
			type: "post",
			dataType: 'json',
			data: { pegawai_id: pegawai_id},
			success: function (data) {
					if(data.pegawai!=null){
						$('#cuti_detail_atasan_nip').val(data.pegawai.NIP);
					}else{
						$('#cuti_detail_atasan_nip').val('');
					}
			}
		});
	}

</script>
