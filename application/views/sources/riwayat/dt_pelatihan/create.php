<style type="text/css">
.control-label{
	font-weight: bold;
}
</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Data Pelatihan</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["NIP"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["NAMA"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tanggal Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo dateEnToID($pegawai["TGL_LAHIR"], 'd F Y');?></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan Data Pelatihan</span>
					<div class="col-md-12">
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Tipe Pelatihan</label>
							<div class="col-md-9">
								<select name="KATEGORI_DIKLAT" id="KATEGORI_DIKLAT" data-placeholder="Tipe Pelatihan" class="select-size-xs wajib" >
									<option value=""></option>
									<?php foreach ($kategori_pel as $i) {
										echo '<option value="'.$i["ID"].'" >'.$i["KATEGORI_PEL"].'</option>';
									}?>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Nama Pelatihan</label>
							<div class="col-md-9">
								<input type="text" name="NAMA_DIKLAT" id="NAMA_DIKLAT" class="form-control input-xs wajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Angkatan</label>
							<div class="col-md-9">
								<input type="text" name="ANGKATAN" id="ANGKATAN" class="form-control input-xs gakwajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Tanggal Mulai</label>
							<div class="col-md-9">
								<input type="text" name="TGL_MULAI" id="TGL_MULAI" class="form-control input-xs wajib pickttl">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Tanggal Selesai</label>
							<div class="col-md-9">
								<input type="text" name="TGL_SELESAI" id="TGL_SELESAI" class="form-control input-xs wajib pickttl">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Penyelenggara</label>
							<div class="col-md-9">
								<input type="text" name="PENYELENGGARA" id="PENYELENGGARA" class="form-control input-xs gakwajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Predikat</label>
							<div class="col-md-9">
								<select name="PREDIKAT" id="PREDIKAT" data-placeholder="Predikat" class="select-size-xs gakwajib" >
									<option value=""></option>
									<?php foreach ($kategori_predikat as $i) {
										echo '<option value="'.$i["ID"].'" >'.$i["KATEGORI_PREDIKAT"].'</option>';
									}?>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Lokasi</label>
							<div class="col-md-9">
								<select name="LOKASI" id="LOKASI" data-placeholder="Lokasi" class="select-size-xs gakwajib" >
									<option value=""></option>
									<?php foreach ($kategori_lokasi as $i) {
										echo '<option value="'.$i["ID"].'" >'.$i["KATEGORI_LOKASI"].'</option>';
									}?>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Jumlah Jam</label>
							<div class="col-md-9">
								<input type="text" name="JUMLAH_JAM" id="JUMLAH_JAM" class="form-control input-xs gakwajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">No Sertifikat</label>
							<div class="col-md-9">
								<input type="text" name="NO_SERTIFIKAT" id="NO_SERTIFIKAT" class="form-control input-xs gakwajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Tanggal Sertifikat</label>
							<div class="col-md-9">
								<input type="text" name="TGL_SERTIFIKAT" id="TGL_SERTIFIKAT" class="form-control input-xs gakwajib pickttl">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">e-File</label>
							<div class="col-md-9">
								<input type="file" name="E_DOC" id="E_DOC" class="form-control input-xs" style="padding-top: 0px !important;padding-bottom: 0px !important;" accept="*">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
								<a href="<?php echo base_url().'dt_dasar/dt_dasar/proses/'.$pegawai['ID'] ?>" class="btn btn-xs btn-danger">Batal</a>
								<input type="hidden" name="ID" id="ID" class="form-control input-xs wajib" value="<?php echo $pegawai["ID"];?>">
								<input type="hidden" name="NIP" id="NIP" class="form-control input-xs wajib" value="<?php echo $pegawai["NIP"];?>">
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.pickttl').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
	});
</script>