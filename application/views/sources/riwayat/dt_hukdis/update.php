<style type="text/css">
.control-label{
	font-weight: bold;
}
</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Data Hukuman Disiplin</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["NIP"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["NAMA"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tanggal Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo dateEnToID($pegawai["TGL_LAHIR"], 'd F Y');?></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan Data Hukuman Disiplin</span>
					<div class="col-md-12">
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Kategori Hukuman Disiplin</label>
							<div class="col-md-9">
								<select name="KATEGORI_HUKDIS" id="KATEGORI_HUKDIS" data-placeholder="Kategori Hukuman Disiplin" class="select-size-xs wajib" >
									<option value=""></option>
									<?php foreach ($kategori_hukdis as $i) {
										echo '<option value="'.$i["ID"].'" '.(($dt_hukdis["KATEGORI_HUKDIS"]==$i["ID"])?"selected":"").'>'.$i["HUKUMAN_DISIPLIN"].'</option>';
									}?>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Tanggal Monev</label>
							<div class="col-md-9">
								<input type="text" name="TGL_MONEV" id="TGL_MONEV" value="<?php echo dateEnToID($dt_hukdis['TGL_MONEV'], 'd F Y') ?>" class="form-control input-xs wajib pickttl">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Tanggal Mulai</label>
							<div class="col-md-9">
								<input type="text" name="TGL_MULAI" id="TGL_MULAI" value="<?php echo dateEnToID($dt_hukdis['TGL_MULAI'], 'd F Y') ?>" class="form-control input-xs wajib pickttl">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Tanggal Selesai</label>
							<div class="col-md-9">
								<input type="text" name="TGL_SELESAI" id="TGL_SELESAI" value="<?php echo dateEnToID($dt_hukdis['TGL_SELESAI'], 'd F Y') ?>" class="form-control input-xs wajib pickttl">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Total Akumulasi Ketidakhadiran</label>
							<div class="col-md-9">
								<input type="text" name="TOTAL_AKUMULASI" id="TOTAL_AKUMULASI" value="<?php echo $dt_hukdis['TOTAL_AKUMULASI'] ?>" class="form-control input-xs gakwajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
								<a href="<?php echo base_url().'dt_dasar/dt_dasar/proses/'.$pegawai['ID'] ?>" class="btn btn-xs btn-danger">Batal</a>
								<input type="hidden" name="ID" id="ID" class="form-control input-xs wajib" value="<?php echo $pegawai["ID"];?>">
								<input type="hidden" name="NIP" id="NIP" class="form-control input-xs wajib" value="<?php echo $pegawai["NIP"];?>">
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.pickttl').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
	});
</script>