<style type="text/css">
.control-label{
	font-weight: bold;
}
</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Data Pendidikan</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["NIP"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["NAMA"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tanggal Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo dateEnToID($pegawai["TGL_LAHIR"], 'd F Y');?></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan Data Pendidikan</span>
					<div class="col-md-12">
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Pendidikan</label>
							<div class="col-md-9">
								<select name="TINGKAT" id="TINGKAT" data-placeholder="Pendidikan" class="select-size-xs wajib" onchange="getProdi(this)">
									<option value=""></option>
									<?php foreach ($pendidikan as $i) {
										echo '<option value="'.$i["KODE_PEND"].'" '.(($dt_pendidikan["TINGKAT"]==$i["KODE_PEND"])?"selected":"").'>'.$i["PENDIDIKAN"].'</option>';
									}?>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Nama Sekolah/PT</label>
							<div class="col-md-9">
								<input type="text" name="NAMA_SEK_PT" id="NAMA_SEK_PT" value="<?php echo $dt_pendidikan["NAMA_SEK_PT"] ?>" class="form-control input-xs wajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs pro_studi" style="display: none;">
							<label class="col-md-3 control-label">Program Studi</label>
							<div class="col-md-9">
								<input type="text" name="PRO_STUDI" id="PRO_STUDI" value="<?php echo $dt_pendidikan["PRO_STUDI"] ?>" class="form-control input-xs gakwajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Tahun Masuk</label>
							<div class="col-md-9">
								<select name="THN_MASUK" id="THN_MASUK" data-placeholder="Tahun Masuk" class="select-size-xs wajib">
									<option value=""></option>
									<?PHP
										$yearstart = date("Y");
										$yearend = $yearstart-68;
										for($i=$yearstart;$i>=$yearend;$i--) {
											echo '<option value="'.$i.'" '.(($i==$dt_pendidikan["THN_MASUK"])?"selected":"").'>'.$i.'</option>';
										}
									?>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Tahun Lulus</label>
							<div class="col-md-9">
								<select name="THN_LULUS" id="THN_LULUS" data-placeholder="Tahun Lulus" class="select-size-xs wajib">
									<option value=""></option>
									<?PHP
										$yearstart = date("Y");
										$yearend = $yearstart-68;
										for($i=$yearstart;$i>=$yearend;$i--) {
											echo '<option value="'.$i.'" '.(($i==$dt_pendidikan["THN_LULUS"])?"selected":"").'>'.$i.'</option>';
										}
									?>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Tempat Belajar</label>
							<div class="col-md-9">
								<select name="TMP_BLJ" id="TMP_BLJ" data-placeholder="Tempat Belajar" class="select-size-xs wajib">
									<option value=""></option>
									<?php foreach ($kategori_lokasi as $i) {
										echo '<option value="'.$i["ID"].'" '.(($dt_pendidikan["TMP_BLJ"]==$i["ID"])?"selected":"").'>'.$i["KATEGORI_LOKASI"].'</option>';
									}?>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Lokasi</label>
							<div class="col-md-9">
								<input type="text" name="LOKASI" id="LOKASI" value="<?php echo $dt_pendidikan["LOKASI"] ?>" class="form-control input-xs gakwajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">No Ijazah</label>
							<div class="col-md-9">
								<input type="text" name="NO_IJASAH" id="NO_IJASAH" value="<?php echo $dt_pendidikan["NO_IJASAH"] ?>" class="form-control input-xs gakwajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Nama Rektor/Kepala Sekolah</label>
							<div class="col-md-9">
								<input type="text" name="NAMA_KEPSEK_REKTOR" id="NAMA_KEPSEK_REKTOR" value="<?php echo $dt_pendidikan["NAMA_KEPSEK_REKTOR"] ?>" class="form-control input-xs gakwajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">e-File</label>
							<div class="col-md-9">
								<input type="file" name="E_DOCS" id="E_DOCS" class="form-control input-xs" style="padding-top: 0px !important;padding-bottom: 0px !important;" accept="*">
								<input type="hidden" name="E_DOC" id="E_DOC" value="<?php echo $dt_pendidikan["E_DOC"] ?>">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
								<a href="<?php echo base_url().'dt_dasar/dt_dasar/proses/'.$pegawai['ID'] ?>" class="btn btn-xs btn-danger">Batal</a>
								<input type="hidden" name="ID" id="ID" class="form-control input-xs wajib" value="<?php echo $pegawai["ID"];?>">
								<input type="hidden" name="NIP" id="NIP" class="form-control input-xs wajib" value="<?php echo $pegawai["NIP"];?>">
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		getProdi().change();
	});

	function getProdi(ini){
		var kode  = $('option:selected', ini).attr('value');
		if(kode<="08"){
			$('.pro_studi').slideDown('fast');
		}else{
			$('.pro_studi').slideUp('fast');
		}
	}
</script>