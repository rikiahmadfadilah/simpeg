<style type="text/css">
.control-label{
	font-weight: bold;
}
</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Data Jabatan</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["NIP"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["NAMA"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tanggal Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo dateEnToID($pegawai["TGL_LAHIR"], 'd F Y');?></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan Data Jabatan</span>
					<div class="col-md-12">
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Unit Kerja</label>
							<div class="col-md-9">
								<select name="UNIT_KERJA" id="UNIT_KERJA" data-placeholder="Unit Kerja" class="select-size-xs wajib" >
									<option value=""></option>
									<?php foreach ($unit_kerja as $i) {
										echo '<option value="'.$i["idunker"].'" '.(($dt_jabatan["UNIT_KERJA_KODE"]==$i["idunker"])?"selected":"").'>'.$i["nama_unker"].'</option>';
									}?>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Jenis Pegawai</label>
							<div class="col-md-9">
								<select name="JENIS_PEGAWAI" id="JENIS_PEGAWAI" data-placeholder="Jenis Pegawai" class="select-size-xs wajib">
									<option value=""></option>
									<?php foreach ($jenis_peg as $i) {
										echo '<option value="'.$i["ID"].'" '.(($dt_jabatan["JENIS_PEGAWAI"]==$i["ID"])?"selected":"").'>'.$i["JENIS_PEGAWAI"].'</option>';
									}?>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Tipe Jabatan</label>
							<div class="col-md-9">
								<select name="KATEGORI_JAB" id="KATEGORI_JAB" data-placeholder="Tipe Jabatan" class="select-size-xs wajib" >
									<option value=""></option>
									<?php foreach ($kategori_jab as $i) {
										echo '<option value="'.$i["ID"].'" '.(($dt_jabatan["KATEGORI_JAB"]==$i["ID"])?"selected":"").'>'.$i["KATEGORI_JAB"].'</option>';
									}?>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Eselon</label>
							<div class="col-md-9">
								<select name="ESELON" id="ESELON" data-placeholder="Eselon" class="select-size-xs wajib" >
									<option value=""></option>
									<?php foreach ($eselon as $i) {
										echo '<option value="'.$i["KODE_ESL"].'" '.(($dt_jabatan["ESELON"]==$i["KODE_ESL"])?"selected":"").'>'.$i["ESELON"].'</option>';
									}?>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Nama Jabatan</label>
							<div class="col-md-9">
								<input type="text" name="NAMA_JAB" id="NAMA_JAB" value="<?php echo $dt_jabatan['NAMA_JAB'] ?>" class="form-control input-xs gakwajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">TMT Golongan</label>
							<div class="col-md-9">
								<input type="text" name="TGL_MULAI" id="TGL_MULAI" value="<?php echo dateEnToID($dt_jabatan['TGL_MULAI'], 'd F Y') ?>" class="form-control input-xs wajib pickttl">
							</div>
						</div>
						<div class="form-group form-group-xs" style="display: none;">
							<label class="col-md-3 control-label">Unit Kerja Lama</label>
							<div class="col-md-9">
								<input type="text" name="UNIT_KERJA_LAMA" value="<?php echo $dt_jabatan['UNIT_KERJA_LAMA'] ?>" id="UNIT_KERJA_LAMA" class="form-control input-xs gakwajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Nomor SK</label>
							<div class="col-md-9">
								<input type="text" name="NOMOR_SK" id="NOMOR_SK" value="<?php echo $dt_jabatan['NOMOR_SK'] ?>" class="form-control input-xs wajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Tanggal SK</label>
							<div class="col-md-9">
								<input type="text" name="TANGGAL_SK" id="TANGGAL_SK" value="<?php echo dateEnToID($dt_jabatan['TANGGAL_SK'], 'd F Y') ?>" class="form-control input-xs wajib pickttl">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">e-File</label>
							<div class="col-md-9">
								<input type="file" name="E_DOCS" id="E_DOCS" class="form-control input-xs" style="padding-top: 0px !important;padding-bottom: 0px !important;" accept="*">
								<input type="hidden" name="E_DOC" id="E_DOC" value="<?php echo $dt_jabatan['E_DOC'] ?>">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
								<a href="<?php echo base_url().'dt_dasar/dt_dasar/proses/'.$pegawai['ID'] ?>" class="btn btn-xs btn-danger">Batal</a>
								<input type="hidden" name="ID" id="ID" class="form-control input-xs wajib" value="<?php echo $pegawai["ID"];?>">
								<input type="hidden" name="NIP" id="NIP" class="form-control input-xs wajib" value="<?php echo $pegawai["NIP"];?>">
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.pickttl').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
	});
</script>