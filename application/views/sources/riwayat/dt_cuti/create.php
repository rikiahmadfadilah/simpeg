	<style type="text/css">
.control-label{
	font-weight: bold;
}
</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Data Cuti</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["NIP"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["NAMA"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tanggal Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo dateEnToID($pegawai["TGL_LAHIR"], 'd F Y');?></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan Data Cuti</span>
					<div class="col-md-12">
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Jenis Cuti</label>
							<div class="col-md-9">
								<select name="jns_cuti" id="jns_cuti" data-placeholder="Jenis Cuti" class="select-size-xs wajib" >
									<option value=""></option>
									<?php foreach ($jenis_cuti as $i) {
										echo '<option value="'.$i["ID"].'" >'.$i["JENIS_CUTI"].'</option>';
									}?>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Nomor Surat</label>
							<div class="col-md-9">
								<input type="text" name="no_surat" id="no_surat" class="form-control input-xs wajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Tanggal Surat</label>
							<div class="col-md-9">
								<input type="text" name="tgl_surat" id="tgl_surat" class="form-control input-xs wajib pickttl">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Kota Surat</label>
							<div class="col-md-9">
								<input type="text" name="kota_surat" id="kota_surat" class="form-control input-xs gakwajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Cuti Mulai Tanggal</label>
							<div class="col-md-9">
								<input type="text" name="tgl_start" id="tgl_start" class="form-control input-xs wajib pickttl">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Sampai Tanggal</label>
							<div class="col-md-9">
								<input type="text" name="tgl_end" id="tgl_end" class="form-control input-xs wajib pickttl">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Jumlah Hari/Bulan</label>
							<div class="col-md-2">
								<select name="jml_hari" id="jml_hari" data-placeholder="Jenis Cuti" class="select-size-xs wajib" >
									<option value="1">1 (satu)</option>
									<option value="2">2 (dua)</option>
									<option value="3">3 (tiga)</option>
									<option value="4">4 (empat)</option>
									<option value="5">5 (lima)</option>
									<option value="6">6 (enam)</option>
									<option value="7">7 (tujuh)</option>
									<option value="8">8 (delapan)</option>
									<option value="9">9 (sembilan)</option>
									<option value="10">10 (sepuluh)</option>
									<option value="11">11 (sebelas)</option>
									<option value="12">12 (dua belas)</option>
								</select>
							</div>
							<div class="col-md-2">
								<select name="satuan_jml" id="jenis_jumlah" data-placeholder="Jenis Cuti" class="select-size-xs wajib" >
									<option value="HR">Hari</option>
									<option value="BL">Buan</option>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">NIP Pejabat</label>
							<div class="col-md-9">
								<select name="iddatadasar_kepala" id="iddatadasar_kepala" data-placeholder="Pilih Pejabat" class="select-size-xs wajib">
									<option value=""></option>
									<?php foreach ($pejabat as $i) {
										echo '<option value="'.$i["ID"].'" data-id="'.$i["ID"].'"  data-nip="'.$i["NIP"].'" data-nama="'.$i["NAMA"].'" data-jabatan="'.$i["NAMA_JAB"].' - '.$i['UNIT_KERJA'].'">'.$i["NIP"].'</option>';
									}?>
								<input type="hidden" name="nipp_kepala" id="nipp_kepala" class="form-control input-xs wajib" placeholder="">
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Nama Pejabat</label>
							<div class="col-md-9">
								<input type="text" name="nama_kepala" id="nama_kepala" class="form-control input-xs wajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Jabatan & Unit Kerja</label>
							<div class="col-md-9">
								<input type="text" name="nama_unker_kepala" id="nama_unker_kepala" rows="5" class="form-control input-xs wajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
								<a href="<?php echo base_url().'dt_dasar/dt_dasar/proses/'.$pegawai['ID'] ?>" class="btn btn-xs btn-danger">Batal</a>
								<input type="hidden" name="ID" id="ID" class="form-control input-xs wajib" value="<?php echo $pegawai["ID"];?>">
								<input type="hidden" name="NIP" id="NIP" class="form-control input-xs wajib" value="<?php echo $pegawai["NIP"];?>">
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.pickttl').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
	});

	$('#iddatadasar_kepala').change(function() {
		var id = $(this).find(':selected').attr('data-id');
		var nip = $(this).find(':selected').attr('data-nip');
		var nama = $(this).find(':selected').attr('data-nama');
		var jabatan = $(this).find(':selected').attr('data-jabatan');
		$('#nipp_kepala').val(nip);
		$('#nama_kepala').val(nama);
		$('#nama_unker_kepala').val(jabatan);
    });
	
</script>