<style type="text/css">
.control-label{
	font-weight: bold;
}
</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Data SKP</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["NIP"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["NAMA"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tanggal Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo dateEnToID($pegawai["TGL_LAHIR"], 'd F Y');?></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Edit Data SKP</span>
					<div class="col-md-12">
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Tahun SKP</label>
							<div class="col-md-9">
								<select name="TH_NILAI" id="TH_NILAI" data-placeholder="Tahun SKP" class="select-size-xs wajib">
									<option value=""></option>
									<?PHP
										$yearstart = date("Y");
										$yearend = $yearstart-68;
										for($i=$yearstart;$i>=$yearend;$i--) {
											echo '<option value="'.$i.'" '.(($i==$dt_skp["TH_NILAI"])?"selected":"").'>'.$i.'</option>';
										}
									?>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Nilai SKP</label>
							<div class="col-md-9">
								<input type="text" name="SKP" id="SKP" value="<?php echo $dt_skp["SKP"] ?>" class="form-control input-xs wajib angka" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Orientasi Pelyanan</label>
							<div class="col-md-9">
								<input type="text" name="ORIENTASI_PELAYANAN" id="ORIENTASI_PELAYANAN" value="<?php echo $dt_skp["ORIENTASI_PELAYANAN"] ?>" class="form-control input-xs gakwajib angka" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Integritas</label>
							<div class="col-md-9">
								<input type="text" name="INTEGRITAS" id="INTEGRITAS" value="<?php echo $dt_skp["INTEGRITAS"] ?>" class="form-control input-xs gakwajib angka" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Komitmen</label>
							<div class="col-md-9">
								<input type="text" name="KOMITMEN" id="KOMITMEN" value="<?php echo $dt_skp["KOMITMEN"] ?>" class="form-control input-xs gakwajib angka" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Disiplin</label>
							<div class="col-md-9">
								<input type="text" name="DISIPLIN" id="DISIPLIN" value="<?php echo $dt_skp["DISIPLIN"] ?>" class="form-control input-xs gakwajib angka" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Kerjasama</label>
							<div class="col-md-9">
								<input type="text" name="KERJASAMA" id="KERJASAMA" value="<?php echo $dt_skp["KERJASAMA"] ?>" class="form-control input-xs gakwajib angka" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Kepemimpinan</label>
							<div class="col-md-9">
								<input type="text" name="KEPEMIMPINAN" id="KEPEMIMPINAN" value="<?php echo $dt_skp["KEPEMIMPINAN"] ?>" class="form-control input-xs gakwajib angka" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">e-File</label>
							<div class="col-md-9">
								<input type="file" name="E_DOCS" id="E_DOCS" class="form-control input-xs" style="padding-top: 0px !important;padding-bottom: 0px !important;" accept="*">
								<input type="hidden" name="E_DOC" id="E_DOC" value="<?php echo $dt_skp["E_DOC"] ?>">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
								<a href="<?php echo base_url().'dt_dasar/dt_dasar/proses/'.$pegawai['ID'] ?>" class="btn btn-xs btn-danger">Batal</a>
								<input type="hidden" name="ID" id="ID" class="form-control input-xs wajib" value="<?php echo $pegawai["ID"];?>">
								<input type="hidden" name="NIP" id="NIP" class="form-control input-xs wajib" value="<?php echo $pegawai["NIP"];?>">
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.pickttl').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});

		$('.angka').formatter({
			pattern: '{{99}}.{{99}}'
		});
	});
</script>