<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title text-center">DATA RIWAYAT HIDUP</h6>
		
	</div>
	<div class="panel-body">
		<form class="need_validation form-horizontal" action="<?php echo base_url('kepegawaian/pegawai_jfu/update') ?>" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px;">I. DATA PRIBADI</span>
                        <table border="0" cellpadding="0" cellspacing="0" class="style2" width="100%">
                        <tbody>
                        <tr>
                          <td width="18%">N A M A</td>
                          <td width="1%">:</td>
                          <td width="60%">
                            <strong><?php echo $pegawai["NAMA"]; ?></strong>
                          </td>
                          <td rowspan="9">
                            <img src="<?php echo base_url();?>assets/images/pegawai/<?php echo $pegawai["PHOTO"] ?>"  alt="PHOTO" name="photo" width="120" height="160" align="left" id="photo">
                          </td>
                        </tr>
                        <tr>
                          <td>NIP</td>
                          <td>:</td>
                          <td>
                            <strong><?php echo $pegawai["NIP"]; ?></strong>
                          </td>
                        </tr>
                        <tr>
                          <td>KARPEG/KARIS-KARSU/NPWP</td>
                          <td>:</td>
                          <td><?php echo $pegawai["NO_KARPEG"]; ?>/<?php echo $pegawai["NO_KARIS"]; ?>/<?php echo $pegawai["NO_NPWP"]; ?></td>
                        </tr>
                        <tr>
                          <td>TEMPAT/TANGGAL LAHIR</td>
                          <td>:</td>
                          <td><?php echo $pegawai["TMP_TGL_LAHIR"]; ?></td>
                        </tr>
                        <tr>
                          <td>JENIS KELAMIN</td>
                          <td>:</td>
                          <td><?php echo $pegawai["JENIS_KELAMIN"]; ?></td>
                        </tr>
                        <tr>
                          <td>AGAMA</td>
                          <td>:</td>
                          <td><?php echo $pegawai["AGAMA_NAMA"]; ?></td>
                        </tr>
                        <tr>
                          <td>STATUS KELUARGA</td>
                          <td>:</td>
                          <td><?php echo $pegawai["STATUS_KAWIN"]; ?></td>
                        </tr>
                        <tr>
                          <td>PENDIDIKAN AKHIR</td>
                          <td>:</td>
                          <td><?php echo $pegawai["NAMA_PENDIDIKAN"]; ?></td>
                        </tr>
                        <tr>
                          <td>PROGRAM STUDI</td>
                          <td>:</td>
                          <td><?php echo $pegawai["PRO_STUDI"]; ?></td>
                        </tr>
                        <tr>
                          <td valign="top">UNIT KERJA</td>
                          <td valign="top">:</td>
                          <td>
                            <strong><?php echo $pegawai["UNIT_KERJA"]; ?></strong>
                          </td>
                        </tr>
                        <tr>
                          <td>DIKLAT PENJENJANGAN</td>
                          <td>:</td>
                            <td><?php echo $pegawai["NAMA_DIKLAT"]; ?>/<?php echo dateEnToID($pegawai["TGL_DIKLAT"], 'd-m-Y'); ?></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>PANGKAT,GOLONGAN,TMT GOL/TMT CPNS</td>
                          <td>:</td>
                          <td><?php echo $pegawai["PANGKAT"]; ?>, <?php echo $pegawai["GOLONGAN_NAMA"]; ?>, <?php echo dateEnToID($pegawai["TMT_GOL"], 'd-m-Y'); ?> / <?php echo $pegawai["TMT_CPNS"]; ?></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>MASA KERJA GOLONGAN</td>
                          <td>:</td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>MASA KERJA KESELURUHAN</td>
                          <td>:</td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td valign="top">JABATAN STRUKTURAL/TMT</td>
                          <td valign="top">:</td>
                          <td>
                            <strong><?php echo $pegawai["NAMA_JAB"]; ?> / <?php echo dateEnToID($pegawai["TMT_JAB"], 'd-m-Y'); ?></strong>
                          </td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>JABATAN FUNGSIONAL</td>
                          <td>:</td>
                          <td><?php echo $pegawai["NAMA_JABFUNG"]; ?> / <?php echo dateEnToID($pegawai["TMT_JABFUNG"], 'd-m-Y'); ?></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>BIDANG KEAHLIAN</td>
                          <td>:</td>
                          <td>
                            / </td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>ALAMAT RUMAH</td>
                          <td>:</td>
                          <td><?php echo $pegawai["ALAMAT"]; ?></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>KOTA/KODE POS</td>
                          <td>:</td>
                          <td> / <?php echo $pegawai["KODE_POS"]; ?></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>PROPINSI/NEGARA</td>
                          <td>:</td>
                          <td>/INDONESIA</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>TELEPHONE RUMAH/HP</td>
                          <td>:</td>
                          <td><?php echo $pegawai["TELPON"]; ?> / <?php echo $pegawai["NO_HP_SMS"]; ?></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>ALAMAT KANTOR</td>
                          <td>:</td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>TELPHONE/EXT</td>
                          <td>:</td>
                          <td>/</td>
                          <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <br/>
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px">II. DATA PENDIDIKAN FORMAL</span>
                    <table class="table table-xxs table-bordered table-striped table-hover datatable-responsive ropeg_table" >
                        <thead>
                            <tr>
                                <th class="text-center">TINGKAT</th>
                                <th class="text-center">NAMA SEKOLAH</th>                                  
                                <th class="text-center">FAKULTAS/JURUSAN/PRODI</th>
                                <th class="text-center">LULUS</th>
                                <th class="text-center">LOKASI</th>
                                <th class="text-center">KEPALA/DEKAN</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($dt_pendidikan)>0){?>
                            <?php foreach ($dt_pendidikan as $dp) {  ?>
                            <tr>
                                <td><?php echo $dp["NAMA_PENDIDIKAN"];?></td>
                                <td><?php echo $dp["NAMA_SEK_PT"];?></td>
                                <td><?php echo $dp["PRO_STUDI"];?></td>
                                <td><?php echo $dp["THN_LULUS"];?></td>
                                <td><?php echo $dp["LOKASI"];?></td>
                                <td><?php echo $dp["NAMA_KEPSEK_REKTOR"];?></td>                                
                            </tr>
                            <?php } ?>
                            <?php }else{ ?>
                            <tr>
                                <td class="text-center" colspan="9">Data Belum Tersedia</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px">III. DATA RIWAYAT KEPANGKATAN</span>
                    <table class="table table-xxs table-bordered table-striped table-hover datatable-responsive ropeg_table" >
                        <thead>
                            <tr>
                                <th class="text-center">GOLONGAN</th>
                                <th class="text-center">TMT GOL</th>                                  
                                <th class="text-center">PEJABAT</th>
                                <th class="text-center">NOMOR SK</th>
                                <th class="text-center">TGL SK</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($dt_kepangkatan)>0){?>
                            <?php foreach ($dt_kepangkatan as $dk) {  ?>
                            <tr>
                                <td><?php echo $dk["GOL"];?></td>
                                <td><?php echo dateEnToID($dk["TMT_GOL"], 'd-m-Y');?></td>
                                <td><?php echo $dk["PEJABAT"];?></td>
                                <td><?php echo $dk["NOMOR_SK"];?></td>
                                <td><?php echo dateEnToID($dk["TGL_SK"], 'd-m-Y');?></td>                          
                            </tr>
                            <?php } ?>
                            <?php }else{ ?>
                            <tr>
                                <td class="text-center" colspan="9">Data Belum Tersedia</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px">IV. DATA RIWAYAT JABATAN</span>
                    <table class="table table-xxs table-bordered table-striped table-hover datatable-responsive ropeg_table" >
                        <thead>
                            <tr>
                                <th class="text-center">ESELON</th>
                                <th class="text-center">NAMA JABATAN</th>                                  
                                <th class="text-center">TMT JABATAN</th>
                                <th class="text-center">NOMOR SK</th>
                                <th class="text-center">TGL SK</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($dt_jabatan)>0){?>
                            <?php foreach ($dt_jabatan as $dj) {  ?>
                            <tr>
                                <td><?php echo $dj["NAMA_ESELON"];?></td>
                                <td><?php echo $dj["NAMA_JAB"];?></td>
                                <td><?php echo dateEnToID($dj["TGL_MULAI"], 'd-m-Y');?></td>
                                <td><?php echo $dj["NOMOR_SK"];?></td>
                                <td><?php echo dateEnToID($dj["TANGGAL_SK"], 'd-m-Y');?></td>                            
                            </tr>
                            <?php } ?>
                            <?php }else{ ?>
                            <tr>
                                <td class="text-center" colspan="9">Data Belum Tersedia</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px">V. DATA DIKLAT PENJENJANGAN/LEMHANAS</span>
                    <table class="table table-xxs table-bordered table-striped table-hover datatable-responsive ropeg_table" >
                        <thead>
                            <tr>
                                <th class="text-center">NAMA DIKLAT</th>
                                <th class="text-center">LOKASI</th>                                  
                                <th class="text-center">TGL MULAI</th>
                                <th class="text-center">TGL SELESAI</th>
                                <th class="text-center">PREDIKAT</th>
                                <th class="text-center">PENYELENGGARA</th>
                                <th class="text-center">JML JAM</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($dt_diklat)>0){?>
                            <?php foreach ($dt_diklat as $dd) {  ?>
                            <tr>
                                <td><?php echo $dd["NAMA_DIKLAT"];?></td>
                                <td><?php echo $dd["LOKASI_TEXT"];?></td>
                                <td><?php echo dateEnToID($dd["TGL_MULAI"], 'd-m-Y');?></td>
                                <td><?php echo dateEnToID($dd["TGL_SELESAI"], 'd-m-Y');?></td>
                                <td><?php echo $dd["PREDIKAT_TEXT"];?></td>
                                <td><?php echo $dd["PENYELENGGARA"];?></td>
                                <td><?php echo $dd["JUMLAH_JAM"];?></td>                         
                            </tr>
                            <?php } ?>
                            <?php }else{ ?>
                            <tr>
                                <td class="text-center" colspan="9">Data Belum Tersedia</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px">VI. DATA ISTRI/SUAMI</span>
                    <table class="table table-xxs table-bordered table-striped table-hover datatable-responsive ropeg_table" >
                        <thead>
                            <tr>
                                <th class="text-center">NAMA ISTRI/SUAMI</th>
                                <th class="text-center">NOMOR KARIS/KARSU</th>                                  
                                <th class="text-center">TGL LAHIR</th>
                                <th class="text-center">TGL NIKAH</th>
                                <!-- <th class="text-center">PENDIDIKAN</th> -->
                                <th class="text-center">PEKERJAAN</th>
                                <th class="text-center">KETERANGAN</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($dt_keluarga)>0){?>
                            <?php foreach ($dt_keluarga as $dk) {  ?>
                              <?php if($dk["HUBUNGAN"] == 1){ ?>
                                <tr>
                                    <td><?php echo $dk["NAMA_KEL"];?></td>
                                    <td><?php echo $dk["NO_KARIS_KARSU"];?></td>
                                    <td><?php echo $dk["TGL_LAHIR"];?></td>
                                    <td><?php echo $dk["TGL_NIKAH"];?></td>
                                    <!-- <td><?php echo $dk["pendidikan_nama"];?></td> -->
                                    <td><?php echo $dk["PEKERJAAN"];?></td>
                                    <td>-</td>                          
                                </tr>
                              <?php } ?>
                            <?php } ?>
                            <?php }else{ ?>
                            <tr>
                                <td class="text-center" colspan="9">Data Belum Tersedia</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px">VIII. DATA ANAK</span>
                    <table class="table table-xxs table-bordered table-striped table-hover datatable-responsive ropeg_table" >
                        <thead>
                            <tr>
                                <th class="text-center">NAMA ANAK</th>
                                <th class="text-center">JNS KELAMIN</th>                                  
                                <th class="text-center">TMP LAHIR</th>
                                <th class="text-center">TGL LAHIR</th>
                                <!-- <th class="text-center">PENDIDIKAN</th> -->
                                <!-- <th class="text-center">KONDISI</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($dt_keluarga)>0){?>
                            <?php foreach ($dt_keluarga as $dk) {  ?>
                              <?php if($dk["HUBUNGAN"] == 2){ ?>
                                <tr>
                                    <td><?php echo $dk["NAMA_KEL"];?></td>
                                    <td><?php echo $dk["JENIS_KELAMIN_TEXT"];?></td>
                                    <td><?php echo $dk["TMP_LAHIR"];?></td>
                                    <td><?php echo $dk["TGL_LAHIR"];?></td>
                                    <!-- <td><?php echo $dk["pendidikan_nama"];?></td> -->
                                    <!-- <td><?php echo $dk["PEKERJAAN"];?></td> -->
                                    <!-- <td>-</td>                           -->
                                </tr>
                              <?php } ?>
                            <?php } ?>
                            <?php }else{ ?>
                            <tr>
                                <td class="text-center" colspan="9">Data Belum Tersedia</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px">IX. DATA KELUARGA</span>
                    <table class="table table-xxs table-bordered table-striped table-hover datatable-responsive ropeg_table" >
                        <thead>
                            <tr>
                                <th class="text-center">NAMA KELUARGA</th>
                                <th class="text-center">JNS KELAMIN</th>                                  
                                <th class="text-center">TGL LAHIR</th>
                                <th class="text-center">HUBUNGAN</th>
                                <th class="text-center">PEKERJAAN</th>
                                <!-- <th class="text-center">KONDISI</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($dt_keluarga)>0){?>
                            <?php foreach ($dt_keluarga as $dk) {  ?>
                              <?php if($dk["HUBUNGAN"] != 1 AND $dk["HUBUNGAN"] != 2){ ?>
                              <tr>
                                <td><?php echo $dk["NAMA_KEL"];?></td>
                                <td><?php echo $dk["JENIS_KELAMIN_TEXT"];?></td>
                                <td><?php echo $dk["TGL_LAHIR"];?></td>
                                <td><?php echo $dk["HUBUNGAN_KELUARGA"];?></td>
                                <td><?php echo $dk["PEKERJAAN"];?></td>
                                <!-- <td><?php echo $dk["keluarga_kondisi_nama"];?></td> -->                      
                              </tr>
                            <?php } ?>
                            <?php } ?>
                            <?php }else{ ?>
                            <tr>
                                <td class="text-center" colspan="9">Data Belum Tersedia</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px">X. DATA SEMINAR/LOKAKARYA</span>
                    <table class="table table-xxs table-bordered table-striped table-hover datatable-responsive ropeg_table" >
                        <thead>
                            <tr>
                                <th class="text-center">NAMA SEMINAR/LOKAKARYA</th>
                                <th class="text-center">LOKASI</th>                                  
                                <th class="text-center">PENYELENGGARA</th>
                                <th class="text-center">TAHUN</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($seminar)>0){?>
                            <?php foreach ($seminar as $s) {  ?>
                            <tr>
                                <td><?php echo $s["seminar_nama_kegiatan"];?></td>
                                <td><?php echo $s["seminar_lokasi"];?></td>
                                <td><?php echo $s["seminar_penyelenggara"];?></td>
                                <td><?php echo $s["seminar_tahun"];?></td>                            
                            </tr>
                            <?php } ?>
                            <?php }else{ ?>
                            <tr>
                                <td class="text-center" colspan="9">Data Belum Tersedia</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div> -->
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px">X. DATA TANDA JASA</span>
                    <table class="table table-xxs table-bordered table-striped table-hover datatable-responsive ropeg_table" >
                        <thead>
                            <tr>
                                <th class="text-center">NAMA TANDA JASA</th>
                                <th class="text-center">TGL PEROLEH</th>                                  
                                <th class="text-center">NOMOR SK</th>
                                <th class="text-center">PEMBERI</th>
                                <th class="text-center">JABATAN</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($dt_penghargaan)>0){?>
                            <?php foreach ($dt_penghargaan as $dp) {  ?>
                            <tr>
                                <td><?php echo $dp["NAMA_TANDA"];?></td>
                                <td><?php echo $dp["TGL_OLEH"];?></td>
                                <td><?php echo $dp["NOMOR_SK"];?></td>
                                <td><?php echo $dp["PEMBERI"];?></td>
                                <td><?php echo $dp["JABATAN"];?></td>                          
                            </tr>
                            <?php } ?>
                            <?php }else{ ?>
                            <tr>
                                <td class="text-center" colspan="9">Data Belum Tersedia</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px">XII. HUKUMAN DISIPLIN</span>
                    <table class="table table-xxs table-bordered table-striped table-hover datatable-responsive ropeg_table" >
                        <thead>
                            <tr>
                                <th class="text-center">KODE HUKUM</th>
                                <!-- <th class="text-center">NOMOR SK</th>                                   -->
                                <th class="text-center">TGL SK</th>
                                <th class="text-center">TGL BERLAKU</th>
                                <!-- <th class="text-center">PEJABAT</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($dt_hukdis)>0){?>
                            <?php foreach ($dt_hukdis as $hd) {  ?>
                            <tr>
                                <td><?php echo $hd["KODE_HUKDIS"];?></td>
                                <td><?php echo dateEnToId($hd["TGL_MONEV"], 'd-m-Y');?></td>
                                <td><?php echo dateEnToId($hd["TGL_SELESAI"], 'd-m-Y');?></td>
                            </tr>
                            <?php } ?>
                            <?php }else{ ?>
                            <tr>
                                <td class="text-center" colspan="9">Data Belum Tersedia</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px">XIV. PENILAIAN KINERJA PEGAWAI</span>
                    <table class="table table-xxs table-bordered table-striped table-hover datatable-responsive ropeg_table" >
                        <thead>
                            <tr>
                                <th class="text-center" rowspan="2">TAHUN</th>
                                <th class="text-center" rowspan="2">SKP 60%</th>                                  
                                <th class="text-center" colspan="7">PENILAIAN PERILAKU</th>
                                <th class="text-center" colspan="3">NILAI KINERJA (SKP 60% + PERILAKU 40%)</th>
                            </tr>
                            <tr>
                                <th class="text-center">ORIENTASI PELAYANAN</th>
                                <th class="text-center">INTEGRITAS</th>
                                <th class="text-center">KOMITMEN</th>
                                <th class="text-center">DISIPLIN</th>
                                <th class="text-center">KERJASAMA</th>
                                <th class="text-center">KEPEMIMPINAN</th>
                                <th class="text-center">RATA-RATA</th>
                                <th class="text-center">SKP 60%</th>
                                <th class="text-center">PERILAKU 40%</th>
                                <th class="text-center">TOTAL NILAI</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center" colspan="12">Data Belum Tersedia</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
	</div>
</div>