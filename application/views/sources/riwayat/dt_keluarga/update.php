<style type="text/css">
.control-label{
	font-weight: bold;
}
</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Data Pangkat</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["NIP"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["NAMA"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tanggal Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo dateEnToID($pegawai["TGL_LAHIR"], 'd F Y');?></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan Data Pangkat</span>
					<div class="col-md-12">
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Hubungan Keluarga</label>
							<div class="col-md-9">
								<select name="HUBUNGAN" id="HUBUNGAN" data-placeholder="Hubungan Keluarga" class="select-size-xs wajib" onchange="getHubungan(this)">
									<option value=""></option>
									<?php foreach ($hubungan_keluarga as $i) {
										echo '<option value="'.$i["ID"].'" '.(($dt_keluarga["HUBUNGAN"]==$i["ID"])?"selected":"").'>'.$i["HUBUNGAN_KELUARGA"].'</option>';
									}?>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Nama Keluarga</label>
							<div class="col-md-9">
								<input type="text" name="NAMA_KEL" id="NAMA_KEL" value="<?php echo $dt_keluarga['NAMA_KEL'] ?>" class="form-control input-xs wajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Jenis Kelamin</label>
							<div class="col-md-9">
								<select name="JENIS_KEL" id="JENIS_KEL" data-placeholder="Jenis Kelamin" class="select-size-xs wajib" >
									<option value=""></option>
									<?php foreach ($jenis_kelamin as $i) {
										echo '<option value="'.$i["ID"].'" '.(($dt_keluarga["JENIS_KEL"]==$i["ID"])?"selected":"").'>'.$i["JENIS_KELAMIN"].'</option>';
									}?>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Tempat Lahir</label>
							<div class="col-md-9">
								<input type="text" name="TMP_LAHIR" id="TMP_LAHIR" value="<?php echo $dt_keluarga['TMP_LAHIR'] ?>" class="form-control input-xs gakwajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Tanggal Lahir</label>
							<div class="col-md-9">
								<input type="text" name="TGL_LAHIR" id="TGL_LAHIR" value="<?php echo dateEnToID($dt_keluarga['TGL_LAHIR'], 'd F Y') ?>" class="form-control input-xs wajib pickttl">
							</div>
						</div>
						<div class="form-group form-group-xs anak" style="display: none;">
							<label class="col-md-3 control-label">Status Anak</label>
							<div class="col-md-9">
								<select name="STATUS_ANAK" id="STATUS_ANAK" data-placeholder="Status Anak" class="select-size-xs wajib">
									<option value=""></option>
									<option value="1" <?php if($dt_keluarga['STATUS_ANAK'] == 1){echo 'selected';}else{} ?>>Anak Kandung</option>
									<option value="2" <?php if($dt_keluarga['STATUS_ANAK'] == 2){echo 'selected';}else{} ?>>Anak Tiri</option>
									<option value="3" <?php if($dt_keluarga['STATUS_ANAK'] == 3){echo 'selected';}else{} ?>>Anak Angkat</option>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs suami_istri" style="display: none;">
							<label class="col-md-3 control-label">Tanggal Nikah</label>
							<div class="col-md-9">
								<input type="text" name="TGL_NIKAH" id="TGL_NIKAH" value="<?php echo dateEnToID($dt_keluarga['TGL_NIKAH'], 'd F Y') ?>" class="form-control input-xs wajib pickttl">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Pekerjaan</label>
							<div class="col-md-9">
								<input type="text" name="PEKERJAAN" id="PEKERJAAN" value="<?php echo $dt_keluarga['PEKERJAAN'] ?>" class="form-control input-xs gakwajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs anak" style="display: none;">
							<label class="col-md-3 control-label">Masuk KP4</label>
							<div class="col-md-9">
								<select name="MASUK_KP4" id="MASUK_KP4" data-placeholder="Masuk KP4" class="select-size-xs wajib">
									<option value=""></option>
									<option value="1" <?php if($dt_keluarga['MASUK_KP4'] == 1){echo 'selected';}else{} ?>>Masuk KP4</option>
									<option value="2" <?php if($dt_keluarga['MASUK_KP4'] == 2){echo 'selected';}else{} ?>>Tidak Masuk KP4</option>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs suami_istri" style="display: none;">
							<label class="col-md-3 control-label">Status Istri/Suami</label>
							<div class="col-md-9">
								<select name="STATUS_ISTRI_SUAMI" id="STATUS_ISTRI_SUAMI" data-placeholder="Status Istri/Suami" class="select-size-xs wajib" onchange="getStatus(this)">
									<option value=""></option>
									<option value="1" <?php if($dt_keluarga['STATUS_ISTRI_SUAMI'] == 1){echo 'selected';}else{} ?>>Istri/Suami Saat Ini</option>
									<option value="2" <?php if($dt_keluarga['STATUS_ISTRI_SUAMI'] == 2){echo 'selected';}else{} ?>>Meninggal</option>
									<option value="3" <?php if($dt_keluarga['STATUS_ISTRI_SUAMI'] == 3){echo 'selected';}else{} ?>>Cerai</option>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs cerai_wafat" style="display: none;">
							<label class="col-md-3 control-label">Tanggal Cerai/Wafat</label>
							<div class="col-md-9">
								<input type="text" name="TGL_CERAIWAFAT" id="TGL_CERAIWAFAT" value="<?php echo dateEnToID($dt_keluarga['TGL_CERAIWAFAT'], 'd F Y') ?>" class="form-control input-xs gakwajib pickttl">
							</div>
						</div>
						<div class="form-group form-group-xs cerai_wafat" style="display: none;">
							<label class="col-md-3 control-label">SK Cerai/Wafat</label>
							<div class="col-md-9">
								<input type="text" name="SK_CERAIWAFAT" id="SK_CERAIWAFAT" value="<?php echo $dt_keluarga['SK_CERAIWAFAT'] ?>" class="form-control input-xs gakwajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs kondisi">
							<label class="col-md-3 control-label">Kondisi</label>
							<div class="col-md-9">
								<select name="KONDISI" id="KONDISI" data-placeholder="Kondisi" class="select-size-xs gakwajib">
									<option value=""></option>
									<option value="1" <?php if($dt_keluarga['KONDISI'] == 1){echo 'selected';}else{} ?>>Masih Hidup</option>
									<option value="2" <?php if($dt_keluarga['KONDISI'] == 2){echo 'selected';}else{} ?>>Sudah Meninggal</option>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
								<a href="<?php echo base_url().'dt_dasar/dt_dasar/proses/'.$pegawai['ID'] ?>" class="btn btn-xs btn-danger">Batal</a>
								<input type="hidden" name="ID" id="ID" class="form-control input-xs wajib" value="<?php echo $pegawai["ID"];?>">
								<input type="hidden" name="NIP" id="NIP" class="form-control input-xs wajib" value="<?php echo $pegawai["NIP"];?>">
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.pickttl').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
		getHubungan().change();
	});

	function getHubungan(ini){
		var kode  = $('option:selected', ini).attr('value');
		if(kode=="1"){
			$('.suami_istri').slideDown('fast');
			$('.kondisi').slideUp('fast');
			$('.anak').slideUp('fast');
		}else if(kode=="2"){
			$('.anak').slideDown('fast');
			$('.suami_istri').slideUp('fast');
			$('.cerai_wafat').slideUp('fast');
			$('.kondisi').slideDown('fast');
		}else{
			$('.anak').slideUp('fast');
			$('.suami_istri').slideUp('fast');
			$('.cerai_wafat').slideUp('fast');
			$('.kondisi').slideDown('fast');
		}
	}
	function getStatus(ini){
		var kode = $('option:selected', ini).attr('value');
		if(kode == 1){
			$('.cerai_wafat').slideUp('fast');
		}else{
			$('.cerai_wafat').slideDown('fast');
		}
	}
</script>