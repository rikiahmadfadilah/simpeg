<style type="text/css">
.control-label{
	font-weight: bold;
}
</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Data Pangkat</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["NIP"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["NAMA"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tanggal Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo dateEnToID($pegawai["TGL_LAHIR"], 'd F Y');?></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan Data Pangkat</span>
					<div class="col-md-12">
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Golongan</label>
							<div class="col-md-9">
								<select name="GOLONGAN" id="GOLONGAN" data-placeholder="Golongan" class="select-size-xs wajib" >
									<option value=""></option>
									<?php foreach ($golongan as $i) {
										echo '<option value="'.$i["KODE_GOL"].'" '.(($dt_kepangkatan["GOLONGAN"]==$i["KODE_GOL"])?"selected":"").'>'.$i["KODE_GOL"].'</option>';
									}?>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">TMT Golongan</label>
							<div class="col-md-9">
								<input type="text" name="TMT_GOL" id="TMT_GOL" value="<?php echo dateEnToID($dt_kepangkatan['TMT_GOL'], 'd F Y') ?>" class="form-control input-xs wajib pickttl">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Pejabat Penandatangan SK</label>
							<div class="col-md-9">
								<input type="text" name="PEJABAT" id="PEJABAT" value="<?php echo $dt_kepangkatan['PEJABAT'] ?>" class="form-control input-xs gakwajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Nomor SK</label>
							<div class="col-md-9">
								<input type="text" name="NOMOR_SK" id="NOMOR_SK" value="<?php echo $dt_kepangkatan['NOMOR_SK'] ?>" class="form-control input-xs wajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Tanggal SK</label>
							<div class="col-md-9">
								<input type="text" name="TGL_SK" id="TGL_SK" value="<?php echo dateEnToID($dt_kepangkatan['TGL_SK'], 'd F Y') ?>"  class="form-control input-xs wajib pickttl">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Kategori SK</label>
							<div class="col-md-9">
								<select name="KATEGORI_SK" id="KATEGORI_SK" data-placeholder="Kategori SK" class="select-size-xs wajib">
									<option value=""></option>
									<?php foreach ($kategori_sk as $i) {
										echo '<option value="'.$i["ID"].'" '.(($dt_kepangkatan["KATEGORI_SK"]==$i["ID"])?"selected":"").'>'.$i["KATEGORI_SK"].'</option>';
									}?>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Masa Kerja Tahun</label>
							<div class="col-md-9">
								<input type="text" name="MASA_KERJA_THN" id="MASA_KERJA_THN" value="<?php echo $dt_kepangkatan['MASA_KERJA_THN'] ?>" class="form-control input-xs wajib angka" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Masa Kerja Bulan</label>
							<div class="col-md-9">
								<input type="text" name="MASA_KERJA_BLN" id="MASA_KERJA_BLN" value="<?php echo $dt_kepangkatan['MASA_KERJA_BLN'] ?>" class="form-control input-xs wajib angka" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">e-File</label>
							<div class="col-md-9">
								<input type="file" name="E_DOCS" id="E_DOCS" class="form-control input-xs" style="padding-top: 0px !important;padding-bottom: 0px !important;" accept="*">
								<input type="hidden" name="E_DOC" id="E_DOC">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
								<a href="<?php echo base_url().'dt_dasar/dt_dasar/proses/'.$pegawai['ID'] ?>" class="btn btn-xs btn-danger">Batal</a>
								<input type="hidden" name="ID" id="ID" class="form-control input-xs wajib" value="<?php echo $pegawai["ID"];?>">
								<input type="hidden" name="NIP" id="NIP" class="form-control input-xs wajib" value="<?php echo $pegawai["NIP"];?>">
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.pickttl').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});

		$('.angka').formatter({
			pattern: '{{99}}'
		});
	});
</script>