<style type="text/css">
.control-label{
	font-weight: bold;
}
</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Data Penghargaan</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["NIP"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["NAMA"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tanggal Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo dateEnToID($pegawai["TGL_LAHIR"], 'd F Y');?></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan Data Penghargaan</span>
					<div class="col-md-12">
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Nama Penghargaan</label>
							<div class="col-md-9">
								<input type="text" name="NAMA_TANDA" id="NAMA_TANDA" value="<?php echo $dt_penghargaan['NAMA_TANDA'] ?>" class="form-control input-xs wajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Jenis Penghargaan</label>
							<div class="col-md-9">
								<select name="JENIS_PENGHARGAAN" id="JENIS_PENGHARGAAN" data-placeholder="Jenis Penghargaan" class="select-size-xs gakwajib" >
									<option value=""></option>
									<?php foreach ($kategori_slks as $i) {
										echo '<option value="'.$i["ID"].'" '.(($dt_penghargaan["JENIS_PENGHARGAAN"]==$i["ID"])?"selected":"").'>'.$i["SLKS"].'</option>';
									}?>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">No SK</label>
							<div class="col-md-9">
								<input type="text" name="NOMOR_SK" id="NOMOR_SK" value="<?php echo $dt_penghargaan['NOMOR_SK'] ?>" class="form-control input-xs gakwajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Tanggal SK</label>
							<div class="col-md-9">
								<input type="text" name="TGL_OLEH" id="TGL_OLEH" value="<?php echo dateEnToID($dt_penghargaan['TGL_OLEH'], 'd F Y') ?>" class="form-control input-xs wajib pickttl">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Tahun</label>
							<div class="col-md-9">
								<select name="THN_OLEH" id="THN_OLEH" data-placeholder="Tahun" class="select-size-xs gakwajib">
									<option value=""></option>
									<?PHP
										$yearstart = date("Y");
										$yearend = $yearstart-68;
										for($i=$yearstart;$i>=$yearend;$i--) {
											echo '<option value="'.$i.'" '.(($i==$dt_penghargaan["THN_OLEH"])?"selected":"").'>'.$i.'</option>';
										}
									?>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Pemberi</label>
							<div class="col-md-9">
								<input type="text" name="PEMBERI" id="PEMBERI" value="<?php echo $dt_penghargaan['PEMBERI'] ?>" class="form-control input-xs gakwajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Jabatan</label>
							<div class="col-md-9">
								<input type="text" name="JABATAN" id="JABATAN" value="<?php echo $dt_penghargaan['JABATAN'] ?>" class="form-control input-xs gakwajib" placeholder="">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">e-File</label>
							<div class="col-md-9">
								<input type="file" name="E_DOCS" id="E_DOCS" class="form-control input-xs" style="padding-top: 0px !important;padding-bottom: 0px !important;" accept="*">
								<input type="hidden" name="E_DOC" id="E_DOC" value="<?php echo $dt_penghargaan['E_DOC'] ?>">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
								<a href="<?php echo base_url().'dt_dasar/dt_dasar/proses/'.$pegawai['ID'] ?>" class="btn btn-xs btn-danger">Batal</a>
								<input type="hidden" name="ID" id="ID" class="form-control input-xs wajib" value="<?php echo $pegawai["ID"];?>">
								<input type="hidden" name="NIP" id="NIP" class="form-control input-xs wajib" value="<?php echo $pegawai["NIP"];?>">
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.pickttl').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
	});
</script>