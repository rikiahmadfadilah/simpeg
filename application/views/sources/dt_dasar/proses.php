<style type="text/css">
.nav-tabs > li > a{
	text-transform: none;
}
.tab-content > .active{
	padding: 15px 10px;
}
.label{
	border-radius:0;
}
.label-success{
	background-color: #5cb85c;
	border-color: #5cb85c;
}
.label-warning{
	background-color: #f0ad4e;
	border-color: #f0ad4e;
}
.label-danger{
	background-color: #d9534f;
	border-color: #d9534f;
}
</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Data Pegawai</h6>
	
	</div>

	<div class="panel-body">
		<div class="tabbable tab-content-bordered">
			<ul class="nav nav-tabs nav-tabs-highlight nav-justified nav-tabs-sm">
				<li class="active"><a href="#dt_dasar" data-toggle="tab">Data Dasar</a></li>
				<li><a href="#dt_pendidikan" data-toggle="tab">Pendidikan</a></li>
				<li><a href="#dt_pangkat" data-toggle="tab">Pangkat</a></li>
				<li><a href="#dt_jabatan" data-toggle="tab">Jabatan</a></li>
				<li><a href="#dt_pelatihan" data-toggle="tab">Pelatihan</a></li>
				<li><a href="#dt_skp" data-toggle="tab">SKP</a></li>
				<li><a href="#dt_penghargaan" data-toggle="tab">Penghargaan</a></li>
				<li><a href="#dt_kgb" data-toggle="tab">KGB</a></li>
				<li><a href="#dt_cuti" data-toggle="tab">Cuti</a></li>
				<li><a href="#dt_keluarga" data-toggle="tab">Keluarga</a></li>
				<li><a href="#dt_assessment" data-toggle="tab">Assesment</a></li>
				<li><a href="#dt_hukdis" data-toggle="tab">Ketidakdisiplinan</a></li>
			</ul>
			<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="tab-content">
				<div class="tab-pane active" id="dt_dasar">
					<div class="row">
						<div class="col-md-12">
							<legend class="text-bold" style="padding-bottom:5px;border-bottom:solid 1px #00bcd4;">Data Pokok</legend>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">Status Pegawai</label>
										<div class="col-lg-9">
											<select name="IS_PNS" id="IS_PNS" data-placeholder="Pilih Status Pegawai" class="select-size-xs">
												<option></option>
												<?php 
												if($pegawai['IS_PNS'] == 1){
													$selected1 = 'selected';
													$selected2 = '';
												}else{
													$selected1 = '';
													$selected2 = 'selected';
												} 
												?>
												<option value="1" <?php echo $selected1; ?>>PNS</option>
												<option value="2" <?php echo $selected2; ?>>Non PNS</option>
											</select>
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">NIP</label>
										<div class="col-lg-9">
											<input type="text" name="NIP" id="NIP" value="<?php echo $pegawai['NIP'] ?>" class="form-control input-xs ga_wajibzz"  type="text">
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">NIK</label>
										<div class="col-lg-9">
											<div class="row">
												<div class="col-md-7">
													<input type="text" name="NO_KTP" id="NO_KTP" value="<?php echo $pegawai['NO_KTP'] ?>" class="form-control input-xs ga_wajibzz"  type="text">
												</div>

												<div class="col-md-5">
													<input type="file" name="upload_nip" id="upload_nip" class="form-control input-xs" style="padding-top: 0px !important;padding-bottom: 0px !important;" accept="image/*">
												</div>
											</div>
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">Nama Lengkap</label>
										<div class="col-lg-9">
											<input type="text" name="NAMA" id="NAMA" value="<?php echo $pegawai['NAMA'] ?>" class="form-control input-xs ga_wajibzz">
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">Gelar</label>
										<div class="col-lg-9">
											<div class="row">
												<div class="col-md-6">
													<input type="text" name="GELAR_DPN" id="GELAR_DPN" value="<?php echo $pegawai['GELAR_DPN'] ?>" class="form-control input-xs ga_wajibzz"  type="text">
													<span class="help-block">Depan</span>
												</div>

												<div class="col-md-6">
													<input type="text" name="GELAR_BLK" id="GELAR_BLK" value="<?php echo $pegawai['GELAR_BLK'] ?>" class="form-control input-xs ga_wajibzz"  type="text">
													<span class="help-block">Belakang</span>
												</div>
											</div>
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">Tempat Lahir</label>
										<div class="col-lg-9">
											<input type="text" name="TMP_LAHIR" id="TMP_LAHIR" value="<?php echo $pegawai['TMP_LAHIR'] ?>" class="form-control input-xs ga_wajibzz">
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">Tanggal Lahir</label>
										<div class="col-lg-9">
											<input type="text" name="TGL_LAHIR" id="TGL_LAHIR" value="<?php echo dateEnToID($pegawai['TGL_LAHIR'], 'd F Y') ?>" class="form-control input-xs ga_wajibzz pickttl">
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">Jenis Kelamin</label>
										<div class="col-lg-9">
											<?php foreach ($jenis_kelamin as $i) {
												echo '<label class="radio-inline">';
												echo '<input name="JENIS_KEL" '.(($pegawai["JENIS_KEL"]==$i["ID"])?"checked":"").' id="JENIS_KEL'.$i["ID"].'" value="'.$i["ID"].'" type="radio">';
												echo ''.$i["JENIS_KELAMIN"].'';
												echo '</label>';
											}?>

										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">Status Kawin</label>
										<div class="col-lg-9">
											<select name="STAT_KAWIN" id="STAT_KAWIN" data-placeholder="Pilih Status Kawin" class="select-size-xs">
												<option></option>
												<?php foreach ($status_kawin as $i) {
													echo '<option value="'.$i["ID"].'" '.(($pegawai["STAT_KAWIN"]==$i["ID"])?"selected":"").'>'.$i["STATUS_KAWIN"].'</option>';
												}?>
											</select>
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">Agama</label>
										<div class="col-lg-9">
											<select name="AGAMA" id="AGAMA" data-placeholder="Pilih Agama" class="select-size-xs">
												<option></option>
												<?php foreach ($agama as $i) {
													echo '<option value="'.$i["ID"].'" '.(($pegawai["AGAMA"]==$i["ID"])?"selected":"").'>'.$i["AGAMA"].'</option>';
												}?>
											</select>
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">Golongan Darah</label>
										<div class="col-lg-9">
											<select name="GOL_DARAH" id="GOL_DARAH" data-placeholder="Pilih Golongan Darah" class="select-size-xs">
												<option></option>
												<?php foreach ($gol_darah as $i) {
													echo '<option value="'.$i["ID"].'" '.(($pegawai["GOL_DARAH"]==$i["ID"])?"selected":"").'>'.$i["GOL_DARAH"].'</option>';
												}?>
											</select>
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">No BPJS</label>
										<div class="col-lg-9">
											<div class="row">
												<div class="col-md-6">
													<input type="text" name="NO_BPJS" id="NO_BPJS" value="<?php echo $pegawai['NO_BPJS'] ?>" class="form-control input-xs ga_wajibzz"  type="text">
												</div>

												<div class="col-md-6">
													<input type="file" name="upload_bpjs" id="upload_bpjs" class="form-control input-xs" style="padding-top: 0px !important;padding-bottom: 0px !important;" accept="image/*">
												</div>
											</div>
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">NPWP</label>
										<div class="col-lg-9">
											<div class="row">
												<div class="col-md-6">
													<input type="text" name="NO_NPWP" id="NO_NPWP" value="<?php echo $pegawai['NO_NPWP'] ?>" class="form-control input-xs ga_wajibzz"  type="text">
												</div>

												<div class="col-md-6">
													<input type="file" name="upload_npwp" id="upload_npwp" class="form-control input-xs" style="padding-top: 0px !important;padding-bottom: 0px !important;" accept="image/*">
												</div>
											</div>
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">Karpeg</label>
										<div class="col-lg-9">
											<div class="row">
												<div class="col-md-6">
													<input type="text" name="NO_KARPEG" id="NO_KARPEG" value="<?php echo $pegawai['NO_KARPEG'] ?>" class="form-control input-xs ga_wajibzz"  type="text">
												</div>

												<div class="col-md-6">
													<input type="file" name="upload_karpeg" id="upload_karpeg" class="form-control input-xs" style="padding-top: 0px !important;padding-bottom: 0px !important;" accept="image/*">
												</div>
											</div>
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">Karis/Karsu</label>
										<div class="col-lg-9">
											<div class="row">
												<div class="col-md-6">
													<input type="text" name="NO_KARIS" id="NO_KARIS" value="<?php echo $pegawai['NO_KARIS'] ?>" class="form-control input-xs ga_wajibzz"  type="text">
												</div>

												<div class="col-md-6">
													<input type="file" name="upload_karis_karsu" id="upload_karis_karsu" class="form-control input-xs" style="padding-top: 0px !important;padding-bottom: 0px !important;" accept="image/*">
												</div>
											</div>
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">Taspen</label>
										<div class="col-lg-9">
											<div class="row">
												<div class="col-md-6">
													<input type="text" name="KD_TASPEN" id="KD_TASPEN" value="<?php echo $pegawai['KD_TASPEN'] ?>" class="form-control input-xs ga_wajibzz"  type="text">
												</div>

												<div class="col-md-6">
													<input type="file" name="upload_taspen" id="upload_taspen" class="form-control input-xs" style="padding-top: 0px !important;padding-bottom: 0px !important;" accept="image/*">
												</div>
											</div>
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">Email</label>
										<div class="col-lg-9">
											<input type="text" name="ALMT_EMAIL" id="ALMT_EMAIL" value="<?php echo $pegawai['ALMT_EMAIL'] ?>" class="form-control input-xs ga_wajibzz ktp">
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">No Telepon</label>
										<div class="col-lg-9">
											<input type="text" name="TELPON" id="TELPON" value="<?php echo $pegawai['TELPON'] ?>" class="form-control input-xs ga_wajibzz ktp">
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">No HP</label>
										<div class="col-lg-9">
											<input type="text" name="NO_HP_SMS" id="NO_HP_SMS" value="<?php echo $pegawai['NO_HP_SMS'] ?>" class="form-control input-xs ga_wajibzz ktp">
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-group-xs">
										<div class="col-lg-9">
											<div class="clearfix"></div>
											<?php if($pegawai["PHOTO"] != ''){ ?>
												<img src="<?php echo base_url().'assets/images/pegawai/'.$pegawai["PHOTO"];?>" height="100px;" id="image_pegawai">
												<?php }else{ ?>	
												<img src="<?php echo base_url().'assets/images/default_user.jpg';?>" height="100px;" id="image_pegawai">
												<?php } ?>
												<div class="clearfix"></div>
												<input type="file" name="PHOTOS" id="PHOTOS" class="form-control input-xs" style="padding-top: 0px !important;padding-bottom: 0px !important;" accept="image/*">
												<input type="hidden" name="PHOTO" id="PHOTO" value="<?php echo $pegawai["PHOTO"]; ?>">
										</div>
									</div>

								</div>
								<div class="col-md-6">
									<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Alamat KTP</span>
									<!-- <div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">Negara</label>
										<div class="col-lg-9">
											<select name="pegawai_domisili_negara_id" id="pegawai_domisili_negara_id" data-placeholder="Pilih Negara" class="select-size-xs" onchange="get_provinsi(this)">
												<option></option>
												<?php foreach ($negara as $i) {

													echo '<option value="'.$i["negara_id"].'" '.(($i["negara_type"]==1)?"selected":"").' data-negara-type="'.$i["negara_type"].'">'.$i["negara_nama"].'</option>';
												}?>
											</select>
										</div>
									</div> -->
									<div class="form-group form-group-xs dalam_negeri">
										<label class="col-lg-3 control-label">Provinsi</label>
										<div class="col-lg-9">
											<select name="KODE_PROVINSI" id="KODE_PROVINSI" data-oldvalue="<?php echo $pegawai["KODE_PROVINSI"];?>" data-placeholder="Pilih Provinsi" class="select-size-xs" onchange="get_kota(this,'KODE_KOTA')">
												<option></option>
												<?php foreach ($provinsi as $i) {
													echo '<option value="'.$i["KODE"].'" '.(($pegawai["KODE_PROVINSI"]==$i["KODE"])?"selected":"").'>'.$i["PROVINSI"].'</option>';
												}?>
											</select>
										</div>
									</div>
									<div class="form-group form-group-xs dalam_negeri">
										<label class="col-lg-3 control-label">Kota</label>
										<div class="col-lg-9">
											<select name="KODE_KOTA" id="KODE_KOTA" data-placeholder="Pilih Kota" class="select-size-xs" onchange="get_kecamatan(this,'KODE_KECAMATAN')">
												<option></option>
											</select>
										</div>
									</div>
									<div class="form-group form-group-xs dalam_negeri">
										<label class="col-lg-3 control-label">Kecamatan</label>
										<div class="col-lg-9">
											<select name="KODE_KECAMATAN" id="KODE_KECAMATAN" data-placeholder="Pilih Kecamatan" class="select-size-xs" onchange="get_kelurahan(this,'KODE_KELURAHAN')">
												<option></option>
											</select>
										</div>
									</div>
									<div class="form-group form-group-xs dalam_negeri">
										<label class="col-lg-3 control-label">Kelurahan</label>
										<div class="col-lg-9">
											<select name="KODE_KELURAHAN" id="KODE_KELURAHAN" data-placeholder="Pilih Kelurahan" class="select-size-xs">
												<option></option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Alamat</label>
										<div class="col-lg-9">
											<textarea name="ALAMAT" id="ALAMAT" rows="3" cols="3" class="form-control elastic ga_wajibzz" ><?php echo $pegawai['ALAMAT'] ?></textarea>
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">Kode Pos</label>
										<div class="col-lg-9">
											<input type="text" name="KODE_POS" id="KODE_POS" value="<?php echo $pegawai['KODE_POS'] ?>" class="form-control input-xs ga_wajibzz kodepos">
										</div>
									</div>
									<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Alamat Domisili</span>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">Provinsi</label>
										<div class="col-lg-9">
											<select name="KODE_PROVINSI2" id="KODE_PROVINSI2" data-placeholder="Pilih Provinsi" class="select-size-xs" onchange="get_kota(this,'KODE_KOTA2')">
												<option></option>
												<?php foreach ($provinsi as $i) {
													echo '<option value="'.$i["provinsi_kode"].'">'.$i["provinsi_nama"].'</option>';
												}?>
											</select>
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">Kota</label>
										<div class="col-lg-9">
											<select name="KODE_KOTA2" id="KODE_KOTA2" data-placeholder="Pilih Kota" class="select-size-xs" onchange="get_kecamatan(this,'KODE_KECAMATAN2')">
												<option></option>
											</select>
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">Kecamatan</label>
										<div class="col-lg-9">
											<select name="KODE_KECAMATAN2" id="KODE_KECAMATAN2" data-placeholder="Pilih Kecamatan" class="select-size-xs" onchange="get_kelurahan(this,'KODE_KELURAHAN2')">
												<option></option>
											</select>
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">Kelurahan</label>
										<div class="col-lg-9">
											<select name="KODE_KELURAHAN2" id="KODE_KELURAHAN2" data-placeholder="Pilih Kelurahan" class="select-size-xs">
												<option></option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Alamat</label>
										<div class="col-lg-9">
											<textarea name="ALAMAT2" id="ALAMAT2" rows="3" cols="3" class="form-control elastic ga_wajibzz" ><?php echo $pegawai['ALAMAT2'] ?></textarea>
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">Kode Pos</label>
										<div class="col-lg-9">
											<input type="text" name="KODE_POS2" id="KODE_POS2" value="<?php echo $pegawai['KODE_POS2'] ?>"  class="form-control input-xs ga_wajibzz kodepos">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-9 col-md-offset-3">
									<button type="submit" class="btn btn-xs btn-primary">Simpan</button>
									<input type="hidden" name="ID" id="ID" value="<?php echo $pegawai['ID'] ?>" class="form-control input-xs wajibz"  type="text">
									<a href="<?php echo base_url();?>dt_dasar/dt_dasar" class="btn btn-xs btn-warning">Kembali</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="tab-pane" id="dt_pendidikan">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Daftar Data Pendidikan</span>
					<div class="text-right">
						<a href="<?php echo base_url().'dt_dasar/dt_dasar/create_dt_pendidikan/'.$pegawai['ID'] ?>" class="btn btn-primary btn-labeled btn-xs"><b><i class=" icon-plus3"></i></b> Tambah Data</a>
					</div>
					<br>
					<div class="table-responsive">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive ropeg_table" id="list_dt_pendidikan">
							<thead>
								<tr>
									<th class="text-center">No</th>
									<th class="text-center">Pendidikan</th>
									<th class="text-center">Tingkat Pendidikan</th>									
									<th class="text-left">Nama Sekolah/PT</th>
									<th class="text-center">Tahun Masuk</th>
									<th class="text-center">Tahun Lulus</th>
									<th class="text-left">Nomor Ijazah</th>
									<th class="text-left">Nama Rektor/Kepala Sekolah</th>
									<th class="text-left">e-File</th>
									<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($dt_pendidikan)>0){?>
								<?php $nodp = 1; foreach ($dt_pendidikan as $dp) {  ?>
								<tr>
									<td><?php echo $nodp;?></td>
									<td><?php echo $dp["NAMA_PENDIDIKAN"];?></td>
									<td><?php echo $dp["NAMA_PENDIDIKAN"];?></td>
									<td><?php echo $dp["NAMA_SEK_PT"];?></td>
									<td><?php echo $dp["THN_MASUK"];?></td>
									<td><?php echo $dp["THN_LULUS"];?></td>
									<td><?php echo $dp["NO_IJASAH"];?></td>
									<td><?php echo $dp["NAMA_KEPSEK_REKTOR"];?></td>
									<td><a href="<?php echo base_url().'assets/images/pendidikan/'.$dp["E_DOC"];?>" target="_blank"><?php echo $dp["E_DOC"];?></a></td>									
									<td>
										<ul class="icons-list">
											<li><a href="<?php echo base_url().'dt_dasar/dt_dasar/update_dt_pendidikan/'.$dp['ID'] ?>" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
											<!-- <li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($dp),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li> -->
											<li><a onclick="delete_dt_pendidikan(<?php echo $dp["ID"];?>,<?php echo $pegawai["ID"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
										</ul>
									</td>
								</tr>
								<?php $nodp++;} ?>
								<?php }else{ ?>
								<tr>
									<td class="text-center" colspan="9">Data Belum Tersedia</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>

				<div class="tab-pane" id="dt_pangkat">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Daftar Data Kepangkatan</span>
					<div class="text-right">
						<a href="<?php echo base_url().'dt_dasar/dt_dasar/create_dt_pangkat/'.$pegawai['ID'] ?>" class="btn btn-primary btn-labeled btn-xs"><b><i class=" icon-plus3"></i></b> Tambah Data</a>
					</div>
					<br>
					<div class="table-responsive">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive ropeg_table" id="list_dt_pangkat">
							<thead>
								<tr>
									<th class="text-center">No</th>
									<th class="text-center">Gol</th>									
									<th class="text-center">TMT Gol</th>
									<th class="text-left">Pejabat Penandatangan SK</th>
									<th class="text-left">No SK</th>
									<th class="text-center">Tanggal SK</th>
									<th class="text-center">Kategori SK</th>
									<th class="text-left">e-File</th>
									<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($dt_kepangkatan)>0){?>
								<?php $nodk = 1; foreach ($dt_kepangkatan as $dk) {  ?>
								<tr>
									<td><?php echo $nodk;?></td>
									<td><?php echo $dk["GOLONGAN"];?></td>
									<td><?php echo dateEnToID($dk["TMT_GOL"], 'd-m-Y');?></td>
									<td><?php echo $dk["PEJABAT"];?></td>
									<td><?php echo $dk["NOMOR_SK"];?></td>
									<td><?php echo dateEnToID($dk["TGL_SK"], 'd-m-Y');?></td>
									<td><?php echo $dk["KATEGORI_SK_TEXT"];?></td>
									<td><a href="<?php echo base_url().'assets/doc/kepangkatan/'.$dk["E_DOC"];?>" target="_blank"><?php echo $dk["E_DOC"];?></a></td>									
									<td>
										<ul class="icons-list">
											<li><a href="<?php echo base_url().'dt_dasar/dt_dasar/update_dt_pangkat/'.$dk['ID'] ?>" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
											<!-- <li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($dk),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li> -->
											<li><a onclick="delete_dt_pangkat(<?php echo $dk["ID"];?>,<?php echo $pegawai["ID"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
										</ul>
									</td>
								</tr>
								<?php $nodk++; } ?>
								<?php }else{ ?>
								<tr>
									<td class="text-center" colspan="9">Data Belum Tersedia</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>

				<div class="tab-pane" id="dt_jabatan">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Daftar Data Jabatan</span>
					<div class="text-right">
						<a href="<?php echo base_url().'dt_dasar/dt_dasar/create_dt_jabatan/'.$pegawai['ID'] ?>" class="btn btn-primary btn-labeled btn-xs"><b><i class=" icon-plus3"></i></b> Tambah Data</a>
					</div>
					<br>
					<div class="table-responsive">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive ropeg_table" id="list_dt_jabatan">
							<thead>
								<tr>
									<th class="text-center">No</th>
									<th class="text-left">Unit Kerja</th>									
									<th class="text-center">Tipe Jabatan</th>
									<th class="text-center">Eselon</th>
									<th class="text-left">Nama Jabatan</th>
									<th class="text-center">TMT Jabatan</th>
									<th class="text-left">Unit Kerja Lama</th>
									<th class="text-left">No SK</th>
									<th class="text-center">Tanggal SK</th>
									<th class="text-left">e-File</th>
									<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($dt_jabatan)>0){?>
								<?php $nodj = 1; foreach ($dt_jabatan as $dj) {  ?>
								<tr>
									<td><?php echo $nodj;?></td>
									<td><?php echo $dj["UNIT_KERJA"];?></td>
									<td><?php echo $dj["KATEGORI_JAB_TEXT"];?></td>
									<td><?php echo $dj["ESELON"];?></td>
									<td><?php echo $dj["NAMA_JAB"];?></td>
									<td><?php echo dateEnToID($dj["TGL_MULAI"], 'd-m-Y');?></td>
									<td><?php echo $dj["UNIT_KERJA_LAMA"];?></td>
									<td><?php echo $dj["NOMOR_SK"];?></td>
									<td><?php echo dateEnToID($dj["TANGGAL_SK"], 'd-m-Y');?></td>
									<td><a href="<?php echo base_url().'assets/doc/pendidikan/'.$dj["E_DOC"];?>" target="_blank"><?php echo $dj["E_DOC"];?></a></td>									
									<td>
										<ul class="icons-list">
											<li><a href="<?php echo base_url().'dt_dasar/dt_dasar/update_dt_jabatan/'.$dj['ID'] ?>" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
											<!-- <li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($dj),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li> -->
											<li><a onclick="delete_dt_jabatan(<?php echo $dj["ID"];?>,<?php echo $pegawai["ID"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
										</ul>
									</td>
								</tr>
								<?php $nodj++; } ?>
								<?php }else{ ?>
								<tr>
									<td class="text-center" colspan="11">Data Belum Tersedia</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="tab-pane" id="dt_pelatihan">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Daftar Data Pelatihan</span>
					<div class="text-right">
						<a href="<?php echo base_url().'dt_dasar/dt_dasar/create_dt_pelatihan/'.$pegawai['ID'] ?>" class="btn btn-primary btn-labeled btn-xs"><b><i class=" icon-plus3"></i></b> Tambah Data</a>
					</div>
					<br>
					<div class="table-responsive">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive ropeg_table" id="list_dt_diklat">
							<thead>
								<tr>
									<th class="text-center">No</th>
									<th class="text-left">Tipe Pelatihan</th>									
									<th class="text-left">Nama Pelatihan</th>
									<th class="text-center">Tanggal Mulai</th>
									<th class="text-center">Tanggal Selesai</th>
									<th class="text-left">Penyelenggara</th>
									<th class="text-center">Jumlah Jam</th>
									<th class="text-left">Lokasi</th>
									<th class="text-left">e-File</th>
									<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($dt_diklat)>0){?>
								<?php $nodd = 1; foreach ($dt_diklat as $dd) {  ?>
								<tr>
									<td><?php echo $nodd;?></td>
									<td><?php echo $dd["KATEGORI_DIKLAT_TEXT"];?></td>
									<td><?php echo $dd["NAMA_DIKLAT"];?></td>
									<td><?php echo dateEnToID($dd["TGL_MULAI"], 'd-m-Y');?></td>
									<td><?php echo dateEnToID($dd["TGL_SELESAI"], 'd-m-Y');?></td>
									<td><?php echo $dd["PENYELENGGARA"];?></td>
									<td><?php echo $dd["JUMLAH_JAM"];?></td>
									<td><?php echo $dd["LOKASI_TEXT"];?></td>
									<td><a href="<?php echo base_url().'assets/doc/pelatihan/'.$dd["E_DOC"];?>" target="_blank"><?php echo $dd["E_DOC"];?></a></td>									
									<td>
										<ul class="icons-list">
											<li><a href="<?php echo base_url().'dt_dasar/dt_dasar/update_dt_pelatihan/'.$dd['ID'] ?>" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
											<!-- <li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($dd),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li> -->
											<li><a onclick="delete_dt_pelatihan(<?php echo $dd["ID"];?>,<?php echo $pegawai["ID"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
										</ul>
									</td>
								</tr>
								<?php $nodd++; } ?>
								<?php }else{ ?>
								<tr>
									<td class="text-center" colspan="10">Data Belum Tersedia</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="tab-pane" id="dt_skp">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data SKP</span>
					<div class="text-right">
						<a href="<?php echo base_url().'dt_dasar/dt_dasar/create_dt_skp/'.$pegawai['ID'] ?>" class="btn btn-primary btn-labeled btn-xs"><b><i class=" icon-plus3"></i></b> Tambah Data</a>
					</div>
					<br>
					<div class="table-responsive">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive ropeg_table" id="list_dt_skp">
							<thead>
								<tr>
									<th class="text-center" rowspan="2">No</th>
									<th class="text-left" rowspan="2">Tahun SKP</th>									
									<th class="text-left" rowspan="2">Nilai SKP</th>
									<th class="text-center" colspan="10">Nilai Perilaku</th>
									<th class="text-left" rowspan="2">e-File</th>
									<th class="text-center" style="width: 80px;min-width: 80px;" rowspan="2">Aksi</th>
								</tr>
								<tr>
									<th class="text-center">Orientasi Pelayanan</th>
									<th class="text-center">Integritas</th>
									<th class="text-center">Komitmen</th>
									<th class="text-center">Disiplin</th>
									<th class="text-center">Kerjasama</th>
									<th class="text-center">Kepemimpinan</th>
									<th class="text-center">Rata-rata</th>
									<th class="text-center">SKP(60%)</th>
									<th class="text-center">Perilaku(40%)</th>
									<th class="text-center">Total</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($dt_skp)>0){?>
									<?php $nods = 1; foreach ($dt_skp as $ds) { ?>
								<tr>
									<td><?php echo $nods; ?></td>
									<td><?php echo $ds['TH_NILAI'] ?></td>
									<td><?php echo $ds['SKP'] ?></td>
									<td><?php echo $ds['ORIENTASI_PELAYANAN'] ?></td>
									<td><?php echo $ds['INTEGRITAS'] ?></td>
									<td><?php echo $ds['KOMITMEN'] ?></td>
									<td><?php echo $ds['DISIPLIN'] ?></td>
									<td><?php echo $ds['KERJASAMA'] ?></td>
									<td><?php echo $ds['KEPEMIMPINAN'] ?></td>
									<?php
										if($ds['KEPEMIMPINAN'] > 0){
											$rata = ($ds['ORIENTASI_PELAYANAN']+$ds['INTEGRITAS']+$ds['KOMITMEN']+$ds['DISIPLIN']+$ds['KERJASAMA']+$ds['KEPEMIMPINAN'])/6;
										}else{
											$rata = ($ds['ORIENTASI_PELAYANAN']+$ds['INTEGRITAS']+$ds['KOMITMEN']+$ds['DISIPLIN']+$ds['KERJASAMA'])/5;
										}
										$skp60 = round(($ds['SKP']*0.6), 2);
										$perilaku40 = round(($rata*0.4), 2);
										$total = round($skp60+$perilaku40, 2);
									?>
									<td><?php echo round($rata, 2) ?></td>
									<td><?php echo $skp60 ?></td>
									<td><?php echo $perilaku40 ?></td>
									<td><?php echo $total ?></td>
									<td><a href="<?php echo base_url().'assets/doc/skp/'.$ds["E_DOC"];?>" target="_blank"><?php echo $ds["E_DOC"];?></a></td>
									<td>
										<ul class="icons-list">
											<li><a href="<?php echo base_url().'dt_dasar/dt_dasar/update_dt_skp/'.$ds['ID'] ?>" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
											<!-- <li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($ds),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li> -->
											<li><a onclick="delete_dt_skp(<?php echo $ds["ID"];?>,<?php echo $pegawai["ID"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
										</ul>
									</td>
								</tr>
								<?php $nods++; }}else{ ?>
									<td class="text-center" colspan="15">Data Belum Tersedia</td>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="tab-pane" id="dt_penghargaan">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Penghargaan</span>
					<div class="text-right">
						<a href="<?php echo base_url().'dt_dasar/dt_dasar/create_dt_penghargaan/'.$pegawai['ID'] ?>" class="btn btn-primary btn-labeled btn-xs"><b><i class=" icon-plus3"></i></b> Tambah Data</a>
					</div>
					<br>
					<div class="table-responsive">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive ropeg_table" id="list_dt_penghargaan">
							<thead>
								<tr>
									<th class="text-center">No</th>
									<th class="text-left">Tipe Penghargaan</th>									
									<th class="text-left">No SK</th>
									<th class="text-center">Tanggal SK</th>
									<th class="text-left">e-File</th>
									<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($dt_penghargaan)>0){?>
								<?php $nodp = 1; foreach ($dt_penghargaan as $dp) {  ?>
								<tr>
									<td><?php echo $nodp;?></td>
									<td><?php echo $dp["NAMA_TANDA"];?></td>
									<td><?php echo $dp["NOMOR_SK"];?></td>
									<td><?php echo dateEnToID($dp["TGL_OLEH"], 'd-m-Y');?></td>
									<td><a href="<?php echo base_url().'assets/doc/penghargaan/'.$dp["E_DOC"];?>" target="_blank"><?php echo $dp["E_DOC"];?></a></td>									
									<td>
										<ul class="icons-list">
											<li><a href="<?php echo base_url().'dt_dasar/dt_dasar/update_dt_penghargaan/'.$dp['ID'] ?>" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
											<!-- <li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($dp),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li> -->
											<li><a onclick="delete_dt_penghargaan(<?php echo $dp["ID"];?>,<?php echo $pegawai["ID"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
										</ul>
									</td>
								</tr>
								<?php $nodp++; } ?>
								<?php }else{ ?>
								<tr>
									<td class="text-center" colspan="6">Data Belum Tersedia</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="tab-pane" id="dt_kgb">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Kenaikan Gaji Berkala</span>
					<div class="text-right">
						<a href="<?php echo base_url().'dt_dasar/dt_dasar/create_dt_kgb/'.$pegawai['ID'] ?>" class="btn btn-primary btn-labeled btn-xs"><b><i class=" icon-plus3"></i></b> Tambah Data</a>
					</div>
					<br>
					<div class="table-responsive">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive ropeg_table" id="list_dt_kgb">
							<thead>
								<tr>
									<th class="text-center">No</th>
									<th class="text-left">Tanggal Kenaikan Gaji Berkala</th>									
									<th class="text-left">Kenaikan Gaji Berkala Sebelum</th>
									<th class="text-left">Kenaikan Gaji Berkala Sesudah</th>
									<th class="text-left">e-File</th>
									<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($dt_kgb)>0){?>
								<?php $nodk = 1; foreach ($dt_kgb as $dk) {  ?>
								<tr>
									<td class="text-center"><?php echo $nodk;?></td>
									<td><?php echo $dk["TANGGAL_KGB"];?></td>
									<td><?php echo $dk["KGB_SEBELUM"];?></td>
									<td><?php echo $dk["KGB_SESUDAH"];?></td>
									<td><a href="<?php echo base_url().'assets/doc/kgb/'.$dk["E_DOC"];?>" target="_blank"><?php echo $dk["E_DOC"];?></a></td>									
									<td>
										<ul class="icons-list">
											<li><a href="<?php echo base_url().'dt_dasar/dt_dasar/update_dt_kgb/'.$dk['ID'] ?>" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
											<!-- <li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($dk),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li> -->
											<li><a onclick="delete_dt_kgb(<?php echo $dk["ID"];?>,<?php echo $pegawai["ID"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
										</ul>
									</td>
								</tr>
								<?php $nodk++; } ?>
								<?php }else{ ?>
								<tr>
									<td class="text-center" colspan="6">Data Belum Tersedia</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="tab-pane" id="dt_cuti">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Cuti</span>
					<div class="text-right">
						<a href="<?php echo base_url().'dt_dasar/dt_dasar/create_dt_cuti/'.$pegawai['ID'] ?>" class="btn btn-primary btn-labeled btn-xs"><b><i class=" icon-plus3"></i></b> Tambah Data</a>
					</div>
					<br>
					<div class="table-responsive">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive ropeg_table" id="list_dt_cuti">
							<thead>
								<tr>
									<th class="text-center" rowspan="2">No</th>
									<th class="text-left" rowspan="2">Jenis Cuti</th>									
									<th class="text-left" rowspan="2">No Surat</th>
									<th class="text-center" colspan="2">Tanggal Cuti</th>
									<th class="text-center" style="width: 80px;min-width: 80px;" rowspan="2">Aksi</th>
								</tr>
								<tr>
									<th class="text-center">Mulai</th>
									<th class="text-center">Selesai</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($dt_cuti)>0){?>
								<?php $nodc = 1; foreach ($dt_cuti as $dc) {  ?>
								<tr>
									<td class="text-center"><?php echo $nodc;?></td>
									<td><?php echo $dc["JENIS_CUTI"];?></td>
									<td><?php echo $dc["no_surat"];?></td>
									<td class="text-center"><?php echo dateEnToID($dc["tgl_start"], 'd-m-Y');?></td>
									<td class="text-center"><?php echo dateEnToID($dc["tgl_end"], 'd-m-Y');?></td>
									<td>
										<ul class="icons-list">
											<li><a href="<?php echo base_url().'dt_dasar/dt_dasar/update_dt_cuti/'.$dc['idcuti'] ?>" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
											<!-- <li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($dc),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li> -->
											<li><a onclick="delete_dt_cuti(<?php echo $dc["idcuti"];?>,<?php echo $pegawai["ID"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
										</ul>
									</td>
								</tr>
								<?php $nodc++; } ?>
								<?php }else{ ?>
								<tr>
									<td class="text-center" colspan="6">Data Belum Tersedia</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="tab-pane" id="dt_keluarga">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Keluarga</span>
					<div class="text-right">
						<a href="<?php echo base_url().'dt_dasar/dt_dasar/create_dt_keluarga/'.$pegawai['ID'] ?>" class="btn btn-primary btn-labeled btn-xs"><b><i class=" icon-plus3"></i></b> Tambah Data</a>
					</div>
					<br>
					<div class="table-responsive">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive ropeg_table" id="list_dt_keluarga">
							<thead>
								<tr>
									<th class="text-center">No</th>
									<th class="text-left">Nama Keluarga</th>									
									<th class="text-left">Jenis Kelamin</th>
									<th class="text-left">Hubungan</th>
									<th class="text-left">Pekerjaan</th>
									<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($dt_keluarga)>0){?>
								<?php $nodk = 1; foreach ($dt_keluarga as $dk) {  ?>
								<tr>
									<td class="text-center"><?php echo $nodk;?></td>
									<td><?php echo $dk["NAMA_KEL"];?></td>
									<td><?php echo $dk["JENIS_KELAMIN_TEXT"];?></td>
									<td><?php echo $dk["HUBUNGAN_KELUARGA"];?></td>
									<td><?php echo $dk["PEKERJAAN"];?></td>									
									<td>
										<ul class="icons-list">
											<li><a href="<?php echo base_url().'dt_dasar/dt_dasar/update_dt_keluarga/'.$dk['ID'] ?>" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
											<!-- <li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($dk),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li> -->
											<li><a onclick="delete_dt_keluarga(<?php echo $dk["ID"];?>,<?php echo $pegawai["ID"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
										</ul>
									</td>
								</tr>
								<?php $nodk++; } ?>
								<?php }else{ ?>
								<tr>
									<td class="text-center" colspan="6">Data Belum Tersedia</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="tab-pane" id="dt_assessment">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Assessment</span>
					<div class="text-right">
						<a href="<?php echo base_url().'dt_dasar/dt_dasar/create_dt_assessment/'.$pegawai['ID'] ?>" class="btn btn-primary btn-labeled btn-xs"><b><i class=" icon-plus3"></i></b> Tambah Data</a>
					</div>
					<br>
					<div class="table-responsive">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive ropeg_table" id="list_dt_assessment">
							<thead>
								<tr>
									<th class="text-center">No</th>
									<th class="text-left">Tanggal Assessment</th>									
									<th class="text-left">Nilai Job Fit(%)</th>
									<th class="text-left">Rekomendasi</th>
									<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($dt_assessment)>0){?>
								<?php $noda = 1; foreach ($dt_assessment as $da) {  ?>
								<tr>
									<td class="text-center"><?php echo $noda;?></td>
									<td><?php echo dateEnToID($da["TANGGAL_ASSESSMENT"], 'd F Y');?></td>
									<td><?php echo $da["NILAI_JOB_FIT"];?></td>
									<td><?php echo $da["REKOMENDASI_TEXTT"];?></td>
									<td>
										<ul class="icons-list">
											<li><a href="<?php echo base_url().'dt_dasar/dt_dasar/update_dt_assessment/'.$da['ID'] ?>" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
											<!-- <li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($da),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li> -->
											<li><a onclick="delete_dt_assessment(<?php echo $da["ID"];?>,<?php echo $pegawai["ID"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
										</ul>
									</td>
								</tr>
								<?php $noda++; } ?>
								<?php }else{ ?>
								<tr>
									<td class="text-center" colspan="5">Data Belum Tersedia</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="tab-pane" id="dt_hukdis">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Hukuman Disiplin</span>
					<div class="text-right">
						<a href="<?php echo base_url().'dt_dasar/dt_dasar/create_dt_hukdis/'.$pegawai['ID'] ?>" class="btn btn-primary btn-labeled btn-xs"><b><i class=" icon-plus3"></i></b> Tambah Data</a>
					</div>
					<br>
					<div class="table-responsive">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive ropeg_table" id="list_dt_hukdis">
							<thead>
								<tr>
									<th class="text-center" rowspan="2">No</th>
									<th class="text-left" rowspan="2">Jenis Hukuman Disipln</th>									
									<th class="text-center" colspan="2">Tanggal</th>
									<th class="text-center" rowspan="2">Total Akumulasi</th>
									<th class="text-center" style="width: 80px;min-width: 80px;" rowspan="2">Aksi</th>
								</tr>
								<tr>
									<td class="text-center">Mulai</td>
									<td class="text-center">Selesai</td>
								</tr>
							</thead>
							<tbody>
								<?php if(count($dt_hukdis)>0){?>
								<?php $nodh = 1; foreach ($dt_hukdis as $dh) {  ?>
								<tr>
									<td class="text-center"><?php echo $nodh;?></td>
									<td><?php echo $dh["HUKUMAN_DISIPLIN"];?></td>
									<td><?php echo dateEnToID($dh["TGL_MULAI"], 'd-m-Y');?></td>
									<td><?php echo dateEnToID($dh["TGL_SELESAI"], 'd-m-Y');?></td>
									<td><?php echo $dh["TOTAL_AKUMULASI"];?></td>
									<td>
										<ul class="icons-list">
											<li><a href="<?php echo base_url().'dt_dasar/dt_dasar/update_dt_hukdis/'.$dh['ID'] ?>" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
											<!-- <li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($dh),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li> -->
											<li><a onclick="delete_dt_hukdis(<?php echo $dh["ID"];?>,<?php echo $pegawai["ID"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
										</ul>
									</td>
								</tr>
								<?php $nodh++; } ?>
								<?php }else{ ?>
								<tr>
									<td class="text-center" colspan="6">Data Belum Tersedia</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#KODE_PROVINSI').change();
		$("#PHOTOS").change(function() {
			readURL(this);
		});
		show_dt_pendidikan();
		show_dt_pangkat();
		show_dt_jabatan();
		show_dt_diklat();
		show_dt_skp();
		show_dt_penghargaan();
		show_dt_kgb();
		show_dt_cuti();
		show_dt_keluarga();
		show_dt_assessment();
		show_dt_hukdis();
	});
	function show_dt_pendidikan(){
		$('#list_dt_pendidikan').dataTable( {
			"processing": true,
			"serverSide": false,
			"bServerSide": false,
			"iDisplayLength": 10,
			"destroy": true,
			"aoColumns": [
			{ "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-center" },
			],
			"fnDrawCallback": function () {
				$('#list_dt_pendidikan').find('tfoot').find('td').removeClass('text-center').removeClass('text-left');
				set_default_datatable();
			},
		});
	}
	function show_dt_pangkat(){
		$('#list_dt_pangkat').dataTable( {
			"processing": true,
			"serverSide": false,
			"bServerSide": false,
			"iDisplayLength": 10,
			"destroy": true,
			"aoColumns": [
			{ "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-center" },
			],
			"fnDrawCallback": function () {
				$('#list_dt_pangkat').find('tfoot').find('td').removeClass('text-center').removeClass('text-left');
				set_default_datatable();
			},
		});
	}
	function show_dt_jabatan(){
		$('#list_dt_jabatan').dataTable( {
			"processing": true,
			"serverSide": false,
			"bServerSide": false,
			"iDisplayLength": 10,
			"destroy": true,
			"aoColumns": [
			{ "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-center" },
			],
			"fnDrawCallback": function () {
				$('#list_dt_jabatan').find('tfoot').find('td').removeClass('text-center').removeClass('text-left');
				set_default_datatable();
			},
		});
	}
	function show_dt_diklat(){
		$('#list_dt_diklat').dataTable( {
			"processing": true,
			"serverSide": false,
			"bServerSide": false,
			"iDisplayLength": 10,
			"destroy": true,
			"aoColumns": [
			{ "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-center" },
			],
			"fnDrawCallback": function () {
				$('#list_dt_diklat').find('tfoot').find('td').removeClass('text-center').removeClass('text-left');
				set_default_datatable();
			},
		});
	}
	function show_dt_skp(){
		$('#list_dt_skp').dataTable( {
			"processing": true,
			"serverSide": false,
			"bServerSide": false,
			"iDisplayLength": 10,
			"destroy": true,
			"aoColumns": [
			{ "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			],
			"fnDrawCallback": function () {
				$('#list_dt_skp').find('tfoot').find('td').removeClass('text-center').removeClass('text-left');
				set_default_datatable();
			},
		});
	}
	function show_dt_penghargaan(){
		$('#list_dt_penghargaan').dataTable( {
			"processing": true,
			"serverSide": false,
			"bServerSide": false,
			"iDisplayLength": 10,
			"destroy": true,
			"aoColumns": [
			{ "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-center" },
			],
			"fnDrawCallback": function () {
				$('#list_dt_penghargaan').find('tfoot').find('td').removeClass('text-center').removeClass('text-left');
				set_default_datatable();
			},
		});
	}
	function show_dt_kgb(){
		$('#list_dt_kgb').dataTable( {
			"processing": true,
			"serverSide": false,
			"bServerSide": false,
			"iDisplayLength": 10,
			"destroy": true,
			"aoColumns": [
			{ "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-center" },
			],
			"fnDrawCallback": function () {
				$('#list_dt_kgb').find('tfoot').find('td').removeClass('text-center').removeClass('text-left');
				set_default_datatable();
			},
		});
	}
	function show_dt_cuti(){
		$('#list_dt_cuti').dataTable( {
			"processing": true,
			"serverSide": false,
			"bServerSide": false,
			"iDisplayLength": 10,
			"destroy": true,
			"aoColumns": [
			{ "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			],
			"fnDrawCallback": function () {
				$('#list_dt_cuti').find('tfoot').find('td').removeClass('text-center').removeClass('text-left');
				set_default_datatable();
			},
		});
	}
	function show_dt_keluarga(){
		$('#list_dt_keluarga').dataTable( {
			"processing": true,
			"serverSide": false,
			"bServerSide": false,
			"iDisplayLength": 10,
			"destroy": true,
			"aoColumns": [
			{ "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-center" },
			],
			"fnDrawCallback": function () {
				$('#list_dt_keluarga').find('tfoot').find('td').removeClass('text-center').removeClass('text-left');
				set_default_datatable();
			},
		});
	}
	function show_dt_assessment(){
		$('#list_dt_assessment').dataTable( {
			"processing": true,
			"serverSide": false,
			"bServerSide": false,
			"iDisplayLength": 10,
			"destroy": true,
			"aoColumns": [
			{ "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			],
			"fnDrawCallback": function () {
				$('#list_dt_assessment').find('tfoot').find('td').removeClass('text-center').removeClass('text-left');
				set_default_datatable();
			},
		});
	}
	function show_dt_hukdis(){
		$('#list_dt_hukdis').dataTable( {
			"processing": true,
			"serverSide": false,
			"bServerSide": false,
			"iDisplayLength": 10,
			"destroy": true,
			"aoColumns": [
			{ "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			],
			"fnDrawCallback": function () {
				$('#list_dt_hukdis').find('tfoot').find('td').removeClass('text-center').removeClass('text-left');
				set_default_datatable();
			},
		});
	}
	$('.pickttl').pickadate({
		monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
		weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
		selectMonths: true,
		selectYears: 80,
		max: true,
		today:'Hari ini',
		clear: 'Hapus',
		close: 'Keluar',
		format: 'dd mmmm yyyy',
		formatSubmit: 'yyyy-mm-dd'
	});

	function get_kota(ini,nextopsi) {
		var KODE_PROVINSI = $(ini).val();
		var oldvalue = $(ini).attr('data-oldvalue');
		$.ajax({
			url: base_url+'dt_dasar/dt_dasar/get_kota',
			type: "post",
			dataType: 'json',
			data: { KODE_PROVINSI: KODE_PROVINSI},
			success: function (data) {
				var kota = data.kota;
				var newoption = "<option></option>";
				for(var i = 0; i < kota.length; i ++){
					newoption+='<option value="'+kota[i].KODE+'"  '+((oldvalue==kota[i].KODE_PROVINSI)?"selected":"")+'>'+kota[i].KOTA+'</option>';
				}
				$('#'+nextopsi).html(newoption);
				$('#'+nextopsi).change();
			}
		});
	}
	function get_kecamatan(ini,nextopsi) {
		var kota_kode = $(ini).val();
		var oldvalue = $(ini).attr('data-oldvalue');
		$.ajax({
			url: base_url+'kepegawaian/pns/get_kecamatan',
			type: "post",
			dataType: 'json',
			data: { kota_kode: kota_kode},
			success: function (data) {
				var kecamatan = data.kecamatan;
				var newoption = "<option></option>";
				for(var i = 0; i < kecamatan.length; i ++){
					newoption+='<option value="'+kecamatan[i].kecamatan_kode+'" '+((oldvalue==kecamatan[i].kecamatan_kode)?"selected":"")+'>'+kecamatan[i].kecamatan_nama+'</option>';
				}
				$('#'+nextopsi).html(newoption);
				$('#'+nextopsi).change();
			}
		});
	}
	function get_kelurahan(ini,nextopsi) {
		var kecamatan_kode = $(ini).val();
		var oldvalue = $(ini).attr('data-oldvalue');
		$.ajax({
			url: base_url+'kepegawaian/pns/get_kelurahan',
			type: "post",
			dataType: 'json',
			data: { kecamatan_kode: kecamatan_kode},
			success: function (data) {
				var kelurahan = data.kelurahan;
				var newoption = "<option></option>";
				for(var i = 0; i < kelurahan.length; i ++){
					newoption+='<option value="'+kelurahan[i].kelurahan_kode+'" '+((oldvalue==kelurahan[i].kelurahan_kode)?"selected":"")+'>'+kelurahan[i].kelurahan_nama+'</option>';
				}
				$('#'+nextopsi).html(newoption);
			}
		});
	}

	function delete_dt_pendidikan(id,pegawai_id){
		$('#text_konfirmasi').html('Anda yakin nonaktifkan data ini ?');
		$('#konfirmasipenyimpanan').modal('show');
		$('#setujukonfirmasibutton').unbind();
		$('#setujukonfirmasibutton').on('click', function () {
			$('#konfirmasipenyimpanan').modal('hide');
			$('.angkadoank').inputmask('remove');
			$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
			window.location.href = base_url+'dt_dasar/dt_dasar/delete_dt_pendidikan/'+id+'/'+pegawai_id;
		});
	}

	function delete_dt_pangkat(id,pegawai_id){
		$('#text_konfirmasi').html('Anda yakin nonaktifkan data ini ?');
		$('#konfirmasipenyimpanan').modal('show');
		$('#setujukonfirmasibutton').unbind();
		$('#setujukonfirmasibutton').on('click', function () {
			$('#konfirmasipenyimpanan').modal('hide');
			$('.angkadoank').inputmask('remove');
			$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
			window.location.href = base_url+'dt_dasar/dt_dasar/delete_dt_pangkat/'+id+'/'+pegawai_id;
		});
	}

	function delete_dt_jabatan(id,pegawai_id){
		$('#text_konfirmasi').html('Anda yakin nonaktifkan data ini ?');
		$('#konfirmasipenyimpanan').modal('show');
		$('#setujukonfirmasibutton').unbind();
		$('#setujukonfirmasibutton').on('click', function () {
			$('#konfirmasipenyimpanan').modal('hide');
			$('.angkadoank').inputmask('remove');
			$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
			window.location.href = base_url+'dt_dasar/dt_dasar/delete_dt_jabatan/'+id+'/'+pegawai_id;
		});
	}

	function delete_dt_pelatihan(id,pegawai_id){
		$('#text_konfirmasi').html('Anda yakin nonaktifkan data ini ?');
		$('#konfirmasipenyimpanan').modal('show');
		$('#setujukonfirmasibutton').unbind();
		$('#setujukonfirmasibutton').on('click', function () {
			$('#konfirmasipenyimpanan').modal('hide');
			$('.angkadoank').inputmask('remove');
			$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
			window.location.href = base_url+'dt_dasar/dt_dasar/delete_dt_pelatihan/'+id+'/'+pegawai_id;
		});
	}

	function delete_dt_skp(id,pegawai_id){
		$('#text_konfirmasi').html('Anda yakin nonaktifkan data ini ?');
		$('#konfirmasipenyimpanan').modal('show');
		$('#setujukonfirmasibutton').unbind();
		$('#setujukonfirmasibutton').on('click', function () {
			$('#konfirmasipenyimpanan').modal('hide');
			$('.angkadoank').inputmask('remove');
			$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
			window.location.href = base_url+'dt_dasar/dt_dasar/delete_dt_skp/'+id+'/'+pegawai_id;
		});
	}

	function delete_dt_penghargaan(id,pegawai_id){
		$('#text_konfirmasi').html('Anda yakin nonaktifkan data ini ?');
		$('#konfirmasipenyimpanan').modal('show');
		$('#setujukonfirmasibutton').unbind();
		$('#setujukonfirmasibutton').on('click', function () {
			$('#konfirmasipenyimpanan').modal('hide');
			$('.angkadoank').inputmask('remove');
			$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
			window.location.href = base_url+'dt_dasar/dt_dasar/delete_dt_penghargaan/'+id+'/'+pegawai_id;
		});
	}

	function delete_dt_kgb(id,pegawai_id){
		$('#text_konfirmasi').html('Anda yakin nonaktifkan data ini ?');
		$('#konfirmasipenyimpanan').modal('show');
		$('#setujukonfirmasibutton').unbind();
		$('#setujukonfirmasibutton').on('click', function () {
			$('#konfirmasipenyimpanan').modal('hide');
			$('.angkadoank').inputmask('remove');
			$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
			window.location.href = base_url+'dt_dasar/dt_dasar/delete_dt_kgb/'+id+'/'+pegawai_id;
		});
	}

	function delete_dt_cuti(id,pegawai_id){
		$('#text_konfirmasi').html('Anda yakin nonaktifkan data ini ?');
		$('#konfirmasipenyimpanan').modal('show');
		$('#setujukonfirmasibutton').unbind();
		$('#setujukonfirmasibutton').on('click', function () {
			$('#konfirmasipenyimpanan').modal('hide');
			$('.angkadoank').inputmask('remove');
			$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
			window.location.href = base_url+'dt_dasar/dt_dasar/delete_dt_cuti/'+id+'/'+pegawai_id;
		});
	}

	function delete_dt_keluarga(id,pegawai_id){
		$('#text_konfirmasi').html('Anda yakin nonaktifkan data ini ?');
		$('#konfirmasipenyimpanan').modal('show');
		$('#setujukonfirmasibutton').unbind();
		$('#setujukonfirmasibutton').on('click', function () {
			$('#konfirmasipenyimpanan').modal('hide');
			$('.angkadoank').inputmask('remove');
			$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
			window.location.href = base_url+'dt_dasar/dt_dasar/delete_dt_keluarga/'+id+'/'+pegawai_id;
		});
	}

	function delete_dt_assessment(id,pegawai_id){
		$('#text_konfirmasi').html('Anda yakin nonaktifkan data ini ?');
		$('#konfirmasipenyimpanan').modal('show');
		$('#setujukonfirmasibutton').unbind();
		$('#setujukonfirmasibutton').on('click', function () {
			$('#konfirmasipenyimpanan').modal('hide');
			$('.angkadoank').inputmask('remove');
			$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
			window.location.href = base_url+'dt_dasar/dt_dasar/delete_dt_assessment/'+id+'/'+pegawai_id;
		});
	}

	function delete_dt_hukdis(id,pegawai_id){
		$('#text_konfirmasi').html('Anda yakin nonaktifkan data ini ?');
		$('#konfirmasipenyimpanan').modal('show');
		$('#setujukonfirmasibutton').unbind();
		$('#setujukonfirmasibutton').on('click', function () {
			$('#konfirmasipenyimpanan').modal('hide');
			$('.angkadoank').inputmask('remove');
			$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
			window.location.href = base_url+'dt_dasar/dt_dasar/delete_dt_hukdis/'+id+'/'+pegawai_id;
		});
	}
	
</script>
