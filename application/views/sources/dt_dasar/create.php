<!-- <script src="<?php echo base_url();?>assets/js/plugins/forms/wizards/steps.min.js"></script> -->
<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Tambah Data Pegawai PNS / Non PNS</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<legend class="text-bold" style="padding-bottom:5px;border-bottom:solid 1px #00bcd4;">Data Pokok</legend>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">Status Pegawai</label>
								<div class="col-lg-9">
									<select name="IS_PNS" id="IS_PNS" data-placeholder="Pilih Status Pegawai" class="select-size-xs wajib">
										<option></option>
										<option value="1">PNS</option>
										<option value="0">Non PNS</option>
									</select>
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">NIP</label>
								<div class="col-lg-9">
									<input type="text" name="NIP" id="NIP" class="form-control input-xs wajib"  type="text">
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">NIK</label>
								<div class="col-lg-9">
									<div class="row">
										<div class="col-md-7">
											<input type="text" name="NO_KTP" id="NO_KTP" class="form-control input-xs wajib nik"  type="text">
										</div>

										<div class="col-md-5">
											<input type="file" name="upload_nip" id="upload_nip" class="form-control input-xs" style="padding-top: 0px !important;padding-bottom: 0px !important;" accept="image/*">
										</div>
									</div>
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">Nama Lengkap</label>
								<div class="col-lg-9">
									<input type="text" name="NAMA" id="NAMA" class="form-control input-xs ga_wajibzz">
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">Gelar</label>
								<div class="col-lg-9">
									<div class="row">
										<div class="col-md-6">
											<input type="text" name="GELAR_DPN" id="GELAR_DPN" class="form-control input-xs ga_wajibzz"  type="text">
											<span class="help-block">Depan</span>
										</div>

										<div class="col-md-6">
											<input type="text" name="GELAR_BLK" id="GELAR_BLK" class="form-control input-xs ga_wajibzz"  type="text">
											<span class="help-block">Belakang</span>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">Tempat Lahir</label>
								<div class="col-lg-9">
									<input type="text" name="TMP_LAHIR" id="TMP_LAHIR" class="form-control input-xs ga_wajibzz">
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">Tanggal Lahir</label>
								<div class="col-lg-9">
									<input type="text" name="TGL_LAHIR" id="TGL_LAHIR" class="form-control input-xs ga_wajibzz pickttl">
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">Jenis Kelamin</label>
								<div class="col-lg-9">
									<?php foreach ($jenis_kelamin as $i) {
										echo '<label class="radio-inline">';
										echo '<input name="JENIS_KEL" id="JENIS_KEL'.$i["ID"].'" value="'.$i["ID"].'" type="radio">';
										echo ''.$i["JENIS_KELAMIN"].'';
										echo '</label>';
									}?>
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">Status Kawin</label>
								<div class="col-lg-9">
									<select name="STAT_KAWIN" id="STAT_KAWIN" data-placeholder="Pilih Status Kawin" class="select-size-xs">
										<option></option>
										<?php foreach ($status_kawin as $i) {
											echo '<option value="'.$i["ID"].'">'.$i["STATUS_KAWIN"].'</option>';
										}?>
									</select>
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">Agama</label>
								<div class="col-lg-9">
									<select name="AGAMA" id="AGAMA" data-placeholder="Pilih Agama" class="select-size-xs">
										<option></option>
										<?php foreach ($agama as $i) {
											echo '<option value="'.$i["ID"].'">'.$i["AGAMA"].'</option>';
										}?>
									</select>
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">Golongan Darah</label>
								<div class="col-lg-9">
									<select name="GOL_DARAH" id="GOL_DARAH" data-placeholder="Pilih Golongan Darah" class="select-size-xs">
										<option></option>
										<?php foreach ($golongan_darah as $i) {
											echo '<option value="'.$i["ID"].'">'.$i["GOL_DARAH"].'</option>';
										}?>
									</select>
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">No BPJS</label>
								<div class="col-lg-9">
									<div class="row">
										<div class="col-md-6">
											<input type="text" name="NO_BPJS" id="NO_BPJS" class="form-control input-xs ga_wajibzz"  type="text">
										</div>

										<div class="col-md-6">
											<input type="file" name="upload_bpjs" id="upload_bpjs" class="form-control input-xs" style="padding-top: 0px !important;padding-bottom: 0px !important;" accept="image/*">
										</div>
									</div>
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">NPWP</label>
								<div class="col-lg-9">
									<div class="row">
										<div class="col-md-6">
											<input type="text" name="NO_NPWP" id="NO_NPWP" class="form-control input-xs ga_wajibzz npwp"  type="text">
										</div>

										<div class="col-md-6">
											<input type="file" name="upload_npwp" id="upload_npwp" class="form-control input-xs" style="padding-top: 0px !important;padding-bottom: 0px !important;" accept="image/*">
										</div>
									</div>
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">Karpeg</label>
								<div class="col-lg-9">
									<div class="row">
										<div class="col-md-6">
											<input type="text" name="NO_KARPEG" id="NO_KARPEG" class="form-control input-xs ga_wajibzz"  type="text">
										</div>

										<div class="col-md-6">
											<input type="file" name="upload_karpeg" id="upload_karpeg" class="form-control input-xs" style="padding-top: 0px !important;padding-bottom: 0px !important;" accept="image/*">
										</div>
									</div>
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">Karis/Karsu</label>
								<div class="col-lg-9">
									<div class="row">
										<div class="col-md-6">
											<input type="text" name="NO_KARIS" id="NO_KARIS" class="form-control input-xs ga_wajibzz"  type="text">
										</div>

										<div class="col-md-6">
											<input type="file" name="upload_karis_karsu" id="upload_karis_karsu" class="form-control input-xs" style="padding-top: 0px !important;padding-bottom: 0px !important;" accept="image/*">
										</div>
									</div>
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">Taspen</label>
								<div class="col-lg-9">
									<div class="row">
										<div class="col-md-6">
											<input type="text" name="KD_TASPEN" id="KD_TASPEN" class="form-control input-xs ga_wajibzz"  type="text">
										</div>

										<div class="col-md-6">
											<input type="file" name="upload_taspen" id="upload_taspen" class="form-control input-xs" style="padding-top: 0px !important;padding-bottom: 0px !important;" accept="image/*">
										</div>
									</div>
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">Email</label>
								<div class="col-lg-9">
									<input type="text" name="ALMT_EMAIL" id="ALMT_EMAIL" class="form-control input-xs ga_wajibzz ktp">
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">No Telepon</label>
								<div class="col-lg-9">
									<input type="text" name="TELPON" id="TELPON" class="form-control input-xs ga_wajibzz ktp">
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">No HP</label>
								<div class="col-lg-9">
									<input type="text" name="NO_HP_SMS" id="NO_HP_SMS" class="form-control input-xs ga_wajibzz ktp">
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group form-group-xs">
								<div class="col-lg-9">
									<img src="<?php echo base_url();?>assets/images/default_user.jpg" height="100px;" id="image_pegawai">
									<div class="clearfix"></div>
									<input type="file" name="PHOTO" id="PHOTO" class="form-control input-xs" style="padding-top: 0px !important;padding-bottom: 0px !important;" accept="image/*">
								</div>
							</div>

						</div>
						<div class="col-md-6">
							<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Alamat KTP</span>
							<!-- <div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">Negara</label>
								<div class="col-lg-9">
									<select name="pegawai_domisili_negara_id" id="pegawai_domisili_negara_id" data-placeholder="Pilih Negara" class="select-size-xs" onchange="get_provinsi(this)">
										<option></option>
										<?php foreach ($negara as $i) {

											echo '<option value="'.$i["negara_id"].'" '.(($i["negara_type"]==1)?"selected":"").' data-negara-type="'.$i["negara_type"].'">'.$i["negara_nama"].'</option>';
										}?>
									</select>
								</div>
							</div> -->
							<div class="form-group form-group-xs dalam_negeri">
								<label class="col-lg-3 control-label">Provinsi</label>
								<div class="col-lg-9">
									<select name="KODE_PROVINSI" id="KODE_PROVINSI" data-placeholder="Pilih Provinsi" class="select-size-xs" onchange="get_kota(this,'KODE_KOTA')">
										<option></option>
										<?php foreach ($provinsi as $i) {
											echo '<option value="'.$i["KODE"].'">'.$i["PROVINSI"].'</option>';
										}?>
									</select>
								</div>
							</div>
							<div class="form-group form-group-xs dalam_negeri">
								<label class="col-lg-3 control-label">Kota</label>
								<div class="col-lg-9">
									<select name="KODE_KOTA" id="KODE_KOTA" data-placeholder="Pilih Kota" class="select-size-xs" onchange="get_kecamatan(this,'KODE_KECAMATAN')">
										<option></option>
									</select>
								</div>
							</div>
							<div class="form-group form-group-xs dalam_negeri">
								<label class="col-lg-3 control-label">Kecamatan</label>
								<div class="col-lg-9">
									<select name="KODE_KECAMATAN" id="KODE_KECAMATAN" data-placeholder="Pilih Kecamatan" class="select-size-xs" onchange="get_kelurahan(this,'KODE_KELURAHAN')">
										<option></option>
									</select>
								</div>
							</div>
							<div class="form-group form-group-xs dalam_negeri">
								<label class="col-lg-3 control-label">Kelurahan</label>
								<div class="col-lg-9">
									<select name="KODE_KELURAHAN" id="KODE_KELURAHAN" data-placeholder="Pilih Kelurahan" class="select-size-xs">
										<option></option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">Alamat</label>
								<div class="col-lg-9">
									<textarea name="ALAMAT" id="ALAMAT" rows="3" cols="3" class="form-control elastic ga_wajibzz" ></textarea>
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">Kode Pos</label>
								<div class="col-lg-9">
									<input type="text" name="KODE_POS" id="KODE_POS" class="form-control input-xs ga_wajibzz kodepos">
								</div>
							</div>
							<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Alamat Domisili</span>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">Provinsi</label>
								<div class="col-lg-9">
									<select name="KODE_PROVINSI2" id="KODE_PROVINSI2" data-placeholder="Pilih Provinsi" class="select-size-xs" onchange="get_kota(this,'KODE_KOTA2')">
										<option></option>
										<?php foreach ($provinsi as $i) {
											echo '<option value="'.$i["KODE"].'">'.$i["PROVINSI"].'</option>';
										}?>
									</select>
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">Kota</label>
								<div class="col-lg-9">
									<select name="KODE_KOTA2" id="KODE_KOTA2" data-placeholder="Pilih Kota" class="select-size-xs" onchange="get_kecamatan(this,'KODE_KECAMATAN2')">
										<option></option>
									</select>
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">Kecamatan</label>
								<div class="col-lg-9">
									<select name="KODE_KECAMATAN2" id="KODE_KECAMATAN2" data-placeholder="Pilih Kecamatan" class="select-size-xs" onchange="get_kelurahan(this,'KODE_KELURAHAN2')">
										<option></option>
									</select>
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">Kelurahan</label>
								<div class="col-lg-9">
									<select name="KODE_KELURAHAN2" id="KODE_KELURAHAN2" data-placeholder="Pilih Kelurahan" class="select-size-xs">
										<option></option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">Alamat</label>
								<div class="col-lg-9">
									<textarea name="ALAMAT2" id="ALAMAT2" rows="3" cols="3" class="form-control elastic ga_wajibzz" ></textarea>
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">Kode Pos</label>
								<div class="col-lg-9">
									<input type="text" name="KODE_POS2" id="KODE_POS2" class="form-control input-xs ga_wajibzz kodepos">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-xs btn-primary">Simpan</button>
					<a href="<?php echo base_url();?>dt_dasar/dt_dasar" class="btn btn-xs btn-warning">Kembali</a>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	function set_tahun_akhir(ini,nextopsi){
		var tahunmulai = parseInt($(ini).val());
		var d = new Date();
		var tahunakhir = d.getFullYear();
		var newoption = "<option value=''></option>";
		for(var i=tahunakhir; i >=tahunmulai; i--) {
			newoption += '<option value="'+i+'">'+i+'</option>';
		}
		$('#'+nextopsi).html(newoption);
	}
	function getFungsionalTertentuByJabatan(ini){
		var kode  = $('option:selected', ini).attr('kode');
		if(kode=="88"){
			$('.fungsional_tertentu_pegawai').slideDown('fast');
		}else{
			$('.fungsional_tertentu_pegawai').slideUp('fast');
		}
	}
	function getFungsionalTertentuByJabatanCPNS(ini){
		var vals  = $(ini).val();
		if(vals=="2"){
			$('.fungsional_tertentu_cpns').slideDown('fast');
		}else{
			$('.fungsional_tertentu_cpns').slideUp('fast');
		}
	}
	function  get_unit_kerja_hirarki(ini,lvl) {
		if(lvl==1){
			$('.pegawai_unit_kerja_id_5').slideUp('fast',function(){
				$('#pegawai_unit_kerja_id_5').select2('val', '');
				$('.pegawai_unit_kerja_id_4').slideUp('fast',function(){
					$('#pegawai_unit_kerja_id_4').select2('val', '');
					$('.pegawai_unit_kerja_id_3').slideUp('fast',function(){
						$('#pegawai_unit_kerja_id_3').select2('val', '');
						$('.pegawai_unit_kerja_id_2').slideUp('fast',function(){
							$('#pegawai_unit_kerja_id_2').select2('val', '');
						});
					});
				});
			});
		}else if(lvl==2){
			$('.pegawai_unit_kerja_id_5').slideUp('fast',function(){
				$('#pegawai_unit_kerja_id_5').select2('val', '');
				$('.pegawai_unit_kerja_id_4').slideUp('fast',function(){
					$('#pegawai_unit_kerja_id_4').select2('val', '');
					$('.pegawai_unit_kerja_id_3').slideUp('fast',function(){
						$('#pegawai_unit_kerja_id_3').select2('val', '');
					});
				});
			});
		}else if(lvl==3){
			$('.pegawai_unit_kerja_id_5').slideUp('fast',function(){
				$('#pegawai_unit_kerja_id_5').select2('val', '');
				$('.pegawai_unit_kerja_id_4').slideUp('fast',function(){
					$('#pegawai_unit_kerja_id_4').select2('val', '');
				});
			});
		}else if(lvl==4){
			$('.pegawai_unit_kerja_id_5').slideUp('fast',function(){
				$('#pegawai_unit_kerja_id_5').select2('val', '');
			});
		}
		var parent_id_kode  = $('option:selected', ini).attr('parent_id_kode');
		var jabatan_id  = $('option:selected', ini).attr('jabatan_id');

		$('#pegawai_jabatan_id').select2('val', '');
		$('#pegawai_jabatan_id').val(jabatan_id);
		$('#pegawai_jabatan_id').trigger('change');
		$.ajax({
			url: base_url+'kepegawaian/pns/get_unit_kerja_hirarki',
			type: "post",
			dataType: 'json',
			data: { unit_kerja_parent_id_kode: parent_id_kode, unit_kerja_level: (lvl+2)},
			success: function (data) {

				var unit_kerja = data.unit_kerja;
				var newoption = "<option></option>";

				for(var i = 0; i < unit_kerja.length; i ++){
					newoption+='<option value="'+unit_kerja[i].unit_kerja_kode+'" parent_id_kode="'+unit_kerja[i].unit_kerja_id_kode+'"  jabatan_id="'+unit_kerja[i].jabatan_id+'">'+unit_kerja[i].unit_kerja_nama+'</option>';
				}
				$('#pegawai_unit_kerja_id_'+(lvl+1)).html(newoption);
				if(unit_kerja.length>0){
					$('.pegawai_unit_kerja_id_'+(lvl+1)).slideDown('fast');
				}else{
					$('.pegawai_unit_kerja_id_'+(lvl+1)).slideUp('fast');

				}
			}
		});
	}
	function get_provinsi(ini){
		var IsDalamNegeri = 2;
		if(IsDalamNegeri!=1){
			$('.dalam_negeri').slideUp(function(){
				$('#pegawai_domisili_provinsi_id').select2('val', ' ');
				$('#pegawai_domisili_provinsi_id').trigger('change');
				$('#pegawai_domisili_kota_id').html('<option></option>');
				$('#pegawai_domisili_kec_id').html('<option></option>');
				$('#pegawai_domisili_kel_id').html('<option></option>');
			});
		}else{
			$('.dalam_negeri').slideDown(function(){
			});
		}
		
	}
	function get_kota(ini,nextopsi) {
		var KODE_PROVINSI = $(ini).val();
		$.ajax({
			url: base_url+'dt_dasar/dt_dasar/get_kota',
			type: "post",
			dataType: 'json',
			data: { KODE_PROVINSI: KODE_PROVINSI},
			success: function (data) {
				var kota = data.kota;
				var newoption = "<option></option>";
				for(var i = 0; i < kota.length; i ++){
					newoption+='<option value="'+kota[i].KODE+'">'+kota[i].KOTA+'</option>';
				}
				$('#'+nextopsi).html(newoption);
			}
		});
	}
	function get_kecamatan(ini,nextopsi) {
		var KODE_KOTA = $(ini).val();
		$.ajax({
			url: base_url+'dt_dasar/dt_dasar/get_kecamatan',
			type: "post",
			dataType: 'json',
			data: { KODE_KOTA: KODE_KOTA},
			success: function (data) {
				var kecamatan = data.kecamatan;
				var newoption = "<option></option>";
				for(var i = 0; i < kecamatan.length; i ++){
					newoption+='<option value="'+kecamatan[i].KODE+'">'+kecamatan[i].KECAMATAN+'</option>';
				}
				$('#'+nextopsi).html(newoption);
			}
		});
	}
	function get_kelurahan(ini,nextopsi) {
		var KODE_KECAMATAN = $(ini).val();
		$.ajax({
			url: base_url+'dt_dasar/dt_dasar/get_kelurahan',
			type: "post",
			dataType: 'json',
			data: { KODE_KECAMATAN: KODE_KECAMATAN},
			success: function (data) {
				var kelurahan = data.kelurahan;
				var newoption = "<option></option>";
				for(var i = 0; i < kelurahan.length; i ++){
					newoption+='<option value="'+kelurahan[i].KODE+'">'+kelurahan[i].KELURAHAN+'</option>';
				}
				$('#'+nextopsi).html(newoption);
			}
		});
	}
	
	function  get_tingkat_pendidikan(ini,nextopsi) {
		var pendidikan_kode = $('option:selected', ini).attr('data-kode');
		$.ajax({
			url: base_url+'kepegawaian/pendidikan_formal/get_tingkat_pendidikan',
			type: "post",
			dataType: 'json',
			data: { pendidikan_kode: pendidikan_kode},
			success: function (data) {
				var isUpdate = parseInt($('#isUpdate').val());
				var pendidikan_formal_jurusan_code_tingkat_temp = $('#pendidikan_formal_jurusan_code_tingkat_temp').val();
				var tingkat_pendidikan = data.tingkat_pendidikan;
				var newoption = "<option value=''></option>";
				for(var i = 0; i < tingkat_pendidikan.length; i ++){
					newoption+='<option value="'+tingkat_pendidikan[i].jurusan_kode+'" '+((tingkat_pendidikan.length==1 || (isUpdate==1 && pendidikan_formal_jurusan_code_tingkat_temp == tingkat_pendidikan[i].jurusan_kode))?"selected":"")+'>'+tingkat_pendidikan[i].jurusan_grup+'</option>';
				}
				$('#'+nextopsi).html(newoption);

				get_fakultas($('#pegawai_pendidikan_tingkat_id'),'pegawai_pendidikan_fakultas_id');
			}
		});
	}
	function  get_fakultas(ini,nextopsi) {
		var jurusan_kode = $(ini).val();
		$.ajax({
			url: base_url+'kepegawaian/pendidikan_formal/get_fakultas',
			type: "post",
			dataType: 'json',
			data: { jurusan_kode: jurusan_kode},
			success: function (data) {
				var isUpdate = parseInt($('#isUpdate').val());
				var pendidikan_formal_jurusan_code_fakultas_temp = $('#pendidikan_formal_jurusan_code_fakultas_temp').val();

				var jurusan = data.jurusan;
				var newoption = "<option value=''></option>";

				for(var i = 0; i < jurusan.length; i ++){
					newoption+='<option value="'+jurusan[i].jurusan_kode+'" '+(((isUpdate==1 && pendidikan_formal_jurusan_code_fakultas_temp == jurusan[i].jurusan_kode))?"selected":"")+'>'+jurusan[i].jurusan_nama+'</option>';
				}
				$('#'+nextopsi).html(newoption);
				get_jurusan($('#pegawai_pendidikan_fakultas_id'),'pegawai_pendidikan_jurusan_id');
			}
		});
	}
	function  get_jurusan(ini,nextopsi) {
		var jurusan_kode = $(ini).val();
		$.ajax({
			url: base_url+'kepegawaian/pendidikan_formal/get_jurusan',
			type: "post",
			dataType: 'json',
			data: { jurusan_kode: jurusan_kode},
			success: function (data) {
				var isUpdate = parseInt($('#isUpdate').val());
				var pendidikan_formal_jurusan_code_temp = $('#pendidikan_formal_jurusan_code_temp').val();

				var jurusan = data.jurusan;
				var newoption = "<option value=''></option>";

				for(var i = 0; i < jurusan.length; i ++){
					if(jurusan[i].jurusan_grup != jurusan[i].jurusan_nama){
						newoption+='<option value="'+jurusan[i].jurusan_kode+'" '+(((isUpdate==1 && pendidikan_formal_jurusan_code_temp == jurusan[i].jurusan_kode))?"selected":"")+'>'+jurusan[i].jurusan_nama+'</option>';
					}
				}
				$('#'+nextopsi).html(newoption);
			}
		});
	}
	

	function readURL(input) {

		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
				$('#image_pegawai').attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}


	$(document).ready(function() {
		var form = $('form.need_validation').show();
		$("#PHOTO").change(function() {
			readURL(this);
		});

		$('[name="NIP"]').formatter({
			pattern: '{{99999999}}  {{999999}}  {{9}}  {{999}}'
		});
		$('.kodepos').formatter({
			pattern: '{{99999}}'
		});
		$('.nik').formatter({
			pattern: '{{9999999999999999}}'
		});
		$('.npwp').formatter({
			pattern: '{{99}}  {{999}}  {{999}}  {{9999}} {{999}}'
		});
		$("#pegawai_ktp_negara_id").prop("disabled", true);
		var d = new Date();

		$('.pickttl').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
		$('.pickttlstart').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});

		$('.pickttlend').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
		$( "#pegawai_nip" ).rules( "add", {
			required: true,
			remote: {
				url: base_url+'kepegawaian/pns/checkNipExist',
				type: "post",
				data: {
					pegawai_nip: function() {
						return $( "#pegawai_nip" ).val();
					}
				}
			},
			messages: {
				required: "Wajib Diisi",
				remote: "NIP Telah Digunakan"
			}
		});
		$( "#pegawai_nomor_ktp" ).rules( "add", {
			required: true,
			remote: {
				url: base_url+'kepegawaian/pns/checkKTPExist',
				type: "post",
				data: {
					pegawai_nomor_ktp: function() {
						return $( "#pegawai_nomor_ktp" ).val();
					}
				}
			},
			messages: {
				required: "Wajib Diisi",
				remote: "NO KTP Telah Digunakan"
			}
		});
		$( "#pegawai_nomor_npwp" ).rules( "add", {
			required: true,
			remote: {
				url: base_url+'kepegawaian/pns/checkNPWPExist',
				type: "post",
				data: {
					pegawai_nomor_npwp: function() {
						return $( "#pegawai_nomor_npwp" ).val();
					}
				}
			},
			messages: {
				required: "Wajib Diisi",
				remote: "NO NPWP Telah Digunakan"
			}
		});
	});
</script>
