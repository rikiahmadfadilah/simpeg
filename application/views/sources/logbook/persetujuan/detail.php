<style type="text/css">
.control-label{
	font-weight: bold;
}
</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Permintaan Verifikasi</h6>
		<div class="heading-elements">
			<div class="heading-btn">
				<a href="<?php echo base_url('logbook/persetujuan');?>" class="btn btn-xs btn-danger">Kembali</a>
			</div>
		</div>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">

				<div class="col-md-12 form_tambah_data">
					<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Verifikasi</span>
					<div class="col-md-12">
						<div class="col-md-12">
							<div class="form-group form-group-xs">
								<div class="text-center">
									<button type="submit" class="btn btn-xs btn-primary">Verifikasi</button> 
									<input type="hidden" name="aduhai" id="aduhai">
									<!-- <a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a> -->
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Daftar kegiatan</span>
					<div class="col-md-12">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive ropeg_table" >
							<thead>
								<tr>
									<th rowspan="2" class="text-center" style="width: 50px;min-width: 50px;">No</th>
									<th colspan="2" class="text-center">Waktu</th>									
									<th rowspan="2" class="text-center">Kegiatan</th>
									<th rowspan="2" class="text-center">Keterangan</th>
								</tr>
								<tr>
									<th class="text-center">Mulai</th>
									<th class="text-center">Selesai</th>
								</tr>
							</thead>
							<tbody>
								<?php $nomor = 1; ?>
								<?php if(count($history)>0){?>
								<?php foreach ($history as $h) {  ?>
								<tr>
									<td><?php echo $nomor;?></td>									
									<td><?php echo substr($h["kegiatan_waktu_mulai"], 0, 5);?></td>
									<td><?php echo substr($h["kegiatan_waktu_akhir"], 0, 5);?></td>
									<td><?php echo $h["kegiatan_harian_nama"];?></td>
									<td><?php echo $h["kegiatan_harian_keterangan"];?></td>
								</tr>
								<?php $nomor++; ?>
								<?php } ?>
								<?php }else{ ?>
								<tr>
									<td class="text-center" colspan="6">Data Belum Tersedia</td>
								</tr>
								<?php } ?>
							</tbody>
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>
<script type="text/javascript">
function set_non_aktif(id,pegawai_id){
	$('#text_konfirmasi').html('Anda yakin menghapus data ini ?');
	$('#konfirmasipenyimpanan').modal('show');
	$('#setujukonfirmasibutton').unbind();
	$('#setujukonfirmasibutton').on('click', function () {
		$('#konfirmasipenyimpanan').modal('hide');
		$('.angkadoank').inputmask('remove');
		$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
		window.location.href = base_url+'kepegawaian/kepangkatan/hapus/'+id+'/'+pegawai_id;
	});
}
function lihat_data(data){
	var decoded = $("<div/>").html(data).text();
	var datajson = $.parseJSON(decoded);

	var jabatan_nama = datajson.jabatan_nama;
	var jenjang_jabatan_jabatan_nama = datajson.jenjang_jabatan_jabatan_nama;
	var unit_kerja_nama = datajson.unit_kerja_nama;
	var jenjang_jabatan_tanggal_mulai = datajson.jenjang_jabatan_tanggal_mulai;
	var jenjang_jabatan_tanggal_selesai = datajson.jenjang_jabatan_tanggal_selesai;
	var jenjang_jabatan_nomor_sk = datajson.jenjang_jabatan_nomor_sk;
	var jenjang_jabatan_tanggal_sk = datajson.jenjang_jabatan_tanggal_sk;

	$(".jabatan_nama").html(jabatan_nama);
	$(".jenjang_jabatan_jabatan_nama").html(jenjang_jabatan_jabatan_nama);
	$(".unit_kerja_nama").html(unit_kerja_nama);
	$(".jenjang_jabatan_tanggal_mulai").html(jenjang_jabatan_tanggal_mulai);
	$(".jenjang_jabatan_tanggal_selesai").html(jenjang_jabatan_tanggal_selesai);
	$(".jenjang_jabatan_nomor_sk").html(jenjang_jabatan_nomor_sk);
	$(".jenjang_jabatan_tanggal_sk").html(jenjang_jabatan_tanggal_sk);

	$('.form_lihat_data').show();
}
function tambah_data(){
	$('.form_lihat_data').hide();
	reset_form();
	$('.label_aksi_form').html('Form Penambahan Data Pendidikan');
	if($('.form_tambah_data').is(":visible")){
		$('.form_tambah_data').slideUp('slow',function(){
			$('.form_tambah_data').slideDown('fast',function(){
				$('html, body').animate({
					scrollTop: $(".form_tambah_data").offset().top-60
				}, 500);

			});
		});
	}else{
		$('.form_tambah_data').slideDown('fast',function(){
			$('html, body').animate({
				scrollTop: $(".form_tambah_data").offset().top-60
			}, 500);
		});
	}
}
function batal_data(){
	if($('.form_tambah_data').is(":visible")){
		$('.form_tambah_data').slideUp('slow',function(){
			reset_form();
		});
	}
	if($('.form_lihat_data').is(":visible")){
		$('.form_lihat_data').slideUp('slow',function(){
			reset_form();
		});
	}
}
function reset_form(){
			$('.validation-error-label').css('display','none');
		}
		
		$(document).ready(function() {
			$('.select-size-xs').select2();
			var d = new Date();

			$('.pickttl').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});
			$('.pickttlstart').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});

			$('.pickttlend').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});

		//On value change
		$(".pickttlstart").change(function(e) {

			var mulai1 = $("#diklat_tanggal_mulai").val().split(" ");
			var tgl = mulai1[0];
			var bulan = mulai1[1];
			var tahun = mulai1[2];

			$('.pickttlend').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				min: new Date(tahun, 5, 20),
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});

			var yesterday  = new Date((new Date()).valueOf()-1000*60*60*24);
			var neededDates = datas.prdetail.PRD_DATE_NEED;
			var neededDate = neededDates.split('-');

			var tglneed = $('#tanggal_dibutuhkan').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agt', 'Sep', 'Okt', 'Nov', 'Des'],
				weekdaysFull: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu'],
				weekdaysShort: ['Min', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectYears: true,
				selectMonths: true,
				format: 'd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd',
				today: 'Hari ini',
				clear: 'Reset',
				close: 'Batal',
				disable: [
				{ from: [0,0,0], to: yesterday }
				]
			});

			var dpicker = tglneed.pickadate('picker');
			dpicker.set('select', [neededDate[0], (neededDate[1]-1), neededDate[2]]);

		});

	});

		function ToJavaScriptDateViewDoank(value) {
        //var pattern = /Date\(([^)]+)\)/;
        //var results = pattern.exec(value);
        //var dt = new Date(parseFloat(results[1]));
        var MonthInd = GetMonthIndInt((value.getMonth() + 1));
        return value.getDate() + " " + MonthInd + " " + value.getFullYear();
    }

    function GetMonthIndInt(month) {
    	switch (month) {
    		case 1:
    		return "Januari"
    		break;
    		case 2:
    		return "Februari"
    		break;
    		case 3:
    		return "Maret"
    		break;
    		case 4:
    		return "April"
    		break;
    		case 5:
    		return "Mei"
    		break;
    		case 6:
    		return "Juni"
    		break;
    		case 7:
    		return "Juli"
    		break;
    		case 8:
    		return "Agustus"
    		break;
    		case 9:
    		return "September"
    		break;
    		case 10:
    		return "Oktober"
    		break;
    		case 11:
    		return "November"
    		break;
    		case 12:
    		return "Desember"
    		break;
    		default:
    		return "Undefined"
    	}
    }

    function  get_unit_kerja_hirarki(ini,lvl) {
    	var attrid = $(ini).attr('data-id');
    	//alert(attrid);
    	if(lvl==1){
    		$('.jenjang_jabatan_unit_kerja_id_5').slideUp('fast',function(){
    			$('#jenjang_jabatan_unit_kerja_id_5').select2('val', '');
    			$('.jenjang_jabatan_unit_kerja_id_4').slideUp('fast',function(){
    				$('#jenjang_jabatan_unit_kerja_id_4').select2('val', '');
    				$('.jenjang_jabatan_unit_kerja_id_3').slideUp('fast',function(){
    					$('#jenjang_jabatan_unit_kerja_id_3').select2('val', '');
    					$('.jenjang_jabatan_unit_kerja_id_2').slideUp('fast',function(){
    						$('#jenjang_jabatan_unit_kerja_id_2').select2('val', '');
    					});
    				});
    			});
    		});
    	}else if(lvl==2){
    		$('.jenjang_jabatan_unit_kerja_id_5').slideUp('fast',function(){
    			$('#jenjang_jabatan_unit_kerja_id_5').select2('val', '');
    			$('.jenjang_jabatan_unit_kerja_id_4').slideUp('fast',function(){
    				$('#jenjang_jabatan_unit_kerja_id_4').select2('val', '');
    				$('.jenjang_jabatan_unit_kerja_id_3').slideUp('fast',function(){
    					$('#jenjang_jabatan_unit_kerja_id_3').select2('val', '');
    				});
    			});
    		});
    	}else if(lvl==3){
    		$('.jenjang_jabatan_unit_kerja_id_5').slideUp('fast',function(){
    			$('#jenjang_jabatan_unit_kerja_id_5').select2('val', '');
    			$('.jenjang_jabatan_unit_kerja_id_4').slideUp('fast',function(){
    				$('#jenjang_jabatan_unit_kerja_id_4').select2('val', '');
    			});
    		});
    	}else if(lvl==4){
    		$('.jenjang_jabatan_unit_kerja_id_5').slideUp('fast',function(){
    			$('#jenjang_jabatan_unit_kerja_id_5').select2('val', '');
    		});
    	}
    	var parent_id_kode  = $('option:selected', ini).attr('parent_id_kode');
    	var jabatan_id  = $('option:selected', ini).attr('jabatan_id');

    	$('#jenjang_jabatan_jabatan_id').select2('val', '');
    	$('#jenjang_jabatan_jabatan_id').val(jabatan_id);
    	$('#jenjang_jabatan_jabatan_id').trigger('change');

    	$.ajax({
    		url: base_url+'kepegawaian/pns/get_unit_kerja_hirarki',
    		type: "post",
    		dataType: 'json',
    		data: { unit_kerja_parent_id_kode: parent_id_kode, unit_kerja_level: (lvl+2)},
    		success: function (data) {

    			var unit_kerja = data.unit_kerja;
    			var newoption = "<option></option>";

    			for(var i = 0; i < unit_kerja.length; i ++){
    				newoption+='<option value="'+unit_kerja[i].unit_kerja_id+'" parent_id_kode="'+unit_kerja[i].unit_kerja_id_kode+'"  jabatan_id="'+unit_kerja[i].jabatan_id+'" '+((unit_kerja[i].unit_kerja_id == attrid)?"selected":"")+'>'+unit_kerja[i].unit_kerja_nama+'</option>';
    			}
    			$('#jenjang_jabatan_unit_kerja_id_'+(lvl+1)).html(newoption);
    			if(unit_kerja.length>0){
    				$('.jenjang_jabatan_unit_kerja_id_'+(lvl+1)).slideDown('fast');
    			}else{
    				$('.jenjang_jabatan_unit_kerja_id_'+(lvl+1)).slideUp('fast');
    			}
    			if(attrid > 1){
    				$('#jenjang_jabatan_unit_kerja_id_'+(lvl+1)).trigger('change');
    			}
    		}
    	});
    }
</script>
