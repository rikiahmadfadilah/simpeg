<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Data Laporan Kegiatan Harian</h6>
		<div class="heading-elements">
			<div class="heading-btn">
				<a href="javascript:void(0)" onclick="tambah_jadwal();" class="btn btn-xs btn-primary btn-tfoot-table">Tambah Jadwal</a>
			</div>
		</div>
		
	</div>

	<div class="panel-body">
		<table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive ropeg_table" id="list_data">
			<thead>
				<tr>
					<th class="text-center">No</th>
					<th class="text-left">Pemilik</th>
					<th class="text-center">Tanggal Kegiatan</th>
					<th class="text-center">Total Efektif Jam Kerja</th>
					<th class="text-center">Jumlah Kegiatan</th>
					<th class="text-center">Status</th>
					<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
				</tr>
			</thead>
			
		</table>
	</div>
</div>

<!-- Info modal -->
<div id="modal_create_jadwal" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title">Tambah Jadwal</h6>
			</div>

			<div class="modal-body">
				<form action="#" class="form-horizontal">
					<div class="modal-body">
						<div class="form-group">
							<label class="control-label col-sm-3">Unit Kerja</label>
							<div class="col-sm-9">
								<input type="text" placeholder="Unit Kerja" class="form-control input-xs" value="<?php echo $UNIT_KERJA ?>" readonly>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-3">Jabatan</label>
							<div class="col-sm-9">
								<input type="text" placeholder="Jabatan" class="form-control input-xs" value="<?php echo $NAMA_JAB ?>" readonly>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-3">Tanggal Kegiatan</label>
							<div class="col-sm-9">
								<input type="text" placeholder="" class="form-control input-xs pickttlend" id="jadwal_kegiatan_tanggal" name="jadwal_kegiatan_tanggal">
								<input type="hidden" name="jadwal_kegiatan_id" id="jadwal_kegiatan_id" value="0">
							</div>
						</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-xs btn-link" data-dismiss="modal">Keluar</button>
				<button type="button" class="btn btn-xs btn-info" onclick="simpanjadwal()">Simpan</button>
			</div>
		</div>
	</div>
</div>
<!-- /info modal -->

<script type="text/javascript">
	$(document).ready(function() {
		$('#list_data').dataTable( {
			"processing": true,
			"serverSide": true,
			"bServerSide": true,
			"sAjaxSource": base_url+"logbook/elogbook/list_data_aktif",
			"aaSorting": [],
			"order": [],
			"iDisplayLength": 10,
			"aoColumns": [
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			],
			"fnDrawCallback": function () {
				set_default_datatable();
			},
		});

		$('.pickttlend').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				min: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});
	});
	function set_aktif(id){
		$('#text_konfirmasi').html('Anda yakin mengaktifkan data ini ?');
		$('#konfirmasipenyimpanan').modal('show');
		$('#setujukonfirmasibutton').unbind();
		$('#setujukonfirmasibutton').on('click', function () {
			$('#konfirmasipenyimpanan').modal('hide');
			$('.angkadoank').inputmask('remove');
			$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
			window.location.href = base_url+'logbook/elogbook/aktif/'+id;
		});
	}
	function set_non_aktif(id){
		$('#text_konfirmasi').html('Anda yakin nonaktifkan data ini ?');
		$('#konfirmasipenyimpanan').modal('show');
		$('#setujukonfirmasibutton').unbind();
		$('#setujukonfirmasibutton').on('click', function () {
			$('#konfirmasipenyimpanan').modal('hide');
			$('.angkadoank').inputmask('remove');
			$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
			window.location.href = base_url+'logbook/elogbook/non_aktif/'+id;
		});
	}

	function tambah_jadwal(){
		$(".modal-title").html("Tambah Jadwal");
		$("#modal_create_jadwal").modal('show');
	}

	function ubah_jadwal(ini){
		$(".modal-title").html("Ubah Jadwal");		
		var dateshow = $(ini).attr("data-tanggal");
		var dataid = $(ini).attr("data-id");
		var dateneed =  dateshow.split("-");

		var datenewshow = dateneed[2]+" "+GetMonthIndInt(parseInt(dateneed[1]))+" "+dateneed[0];
		
		$("#jadwal_kegiatan_id").val(dataid);
		$("input[name=jadwal_kegiatan_tanggal_submit]").val(dateshow);
		$("#jadwal_kegiatan_tanggal").val(datenewshow);
		$("#modal_create_jadwal").modal('show');
	}

	function simpanjadwal(){
		var jadwal = $("input[name=jadwal_kegiatan_tanggal_submit]").val();
		if(jadwal != ""){
			var id = $("#jadwal_kegiatan_id").val(); 
			window.location.href = base_url+'logbook/elogbook/insertjadwal/'+id+'/'+jadwal;
		}
		else{
			alert("jadwal harus diisi");
		}
		
	}

</script>
