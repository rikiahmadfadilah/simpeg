<!-- <script src="<?php echo base_url();?>assets/js/plugins/editors/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url();?>assets/js/demo_pages/editor_ckeditor.js"></script> -->
<style type="text/css">
.control-label{
	font-weight: bold;
}
</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Detail kegiatan</h6>
		<div class="heading-elements">
			<div class="heading-btn">
				<a href="<?php echo base_url('logbook/elogbook');?>" class="btn btn-xs btn-danger">Kembali</a>
			</div>
		</div>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data" onSubmit="validasi()">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Daftar kegiatan</span>
					<div class="col-md-12">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive ropeg_table">
							<thead>
								<tr>
									<th rowspan="2" class="text-center">No</th>
									<th colspan="2" class="text-center">Waktu</th>									
									<th rowspan="2" class="text-center">kegiatan</th>
									<th rowspan="2" class="text-center">Keterangan</th>
									<th rowspan="2" class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
								<tr>
									<th class="text-center">Mulai</th>
									<th class="text-center">Selesai</th>
								</tr>
							</thead>
							<tbody>
								<?php $nomor = 1; ?>
								<?php if(count($history)>0){?>
								<?php foreach ($history as $h) {  ?>
								<tr>
									<td><?php echo $nomor;?></td>									
									<td><?php echo substr($h["kegiatan_waktu_mulai"], 0, 5);?></td>
									<td><?php echo substr($h["kegiatan_waktu_akhir"], 0, 5);?></td>
									<td><?php echo $h["kegiatan_harian_nama"];?></td>
									<td><?php echo $h["kegiatan_harian_keterangan"];?></td>
									<td>
										<ul class="icons-list">
											<!-- <li><a href="javascript:void(0)" onclick="update_data_kegiatan('<?php echo $h["kegiatan_id"];?>')" class="lihat_data" data-popup="tooltip" title="Ubah Data Kegiatan" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li> -->
											<li><a href="javascript:void(0)" onclick="update_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
											<li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>
											<li><a onclick="set_non_aktif(<?php echo $h["kegiatan_id"];?>,<?php echo $h["kegiatan_jadwal_kegiatan_id"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
										</ul>
									</td>
								</tr>
								<?php $nomor++; ?>
								<?php } ?>
								<?php }else{ ?>
								<tr>
									<td class="text-center" colspan="6">Data Belum Tersedia</td>
								</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<td class="text-right" colspan="6"><a href="javascript:void(0)" onclick="tambah_data();" class="btn btn-xs btn-primary btn-tfoot-table">Tambah Data</a>
								</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="col-md-12 form_tambah_data" style="display: none;">
						<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan kegiatan</span>
						<div class="col-md-12">

							<div class="col-md-12">
								
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Waktu Mulai</label>
									<div class="col-md-9">
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-watch2"></i></span>
											<input type="text" class="form-control" name="kegiatan_waktu_mulai" id="kegiatan_waktu_mulai" value="12:34">
										</div>
										<!-- <input type="text" name="kegiatan_waktu_mulai" id="kegiatan_waktu_mulai" class="form-control input-xs wajib pickttl"> -->
									</div>
								</div>

								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Waktu Selesai</label>
									<div class="col-md-9">
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-watch2"></i></span>
											<input type="text" class="form-control" id="kegiatan_waktu_akhir" name="kegiatan_waktu_akhir" value="12:34">
										</div>
										<!-- <input type="text" name="kegiatan_waktu_akhir" id="kegiatan_waktu_akhir" class="form-control input-xs wajib pickttl"> -->
									</div>
								</div>

								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Pilih Sasaran Kerja Pegawai (SKP)</label>
									<div class="col-lg-9">
										<select name="kegiatan_skp" id="kegiatan_skp" data-placeholder="Pilih Sasaran Kerja Pegawai (SKP)" data-id="0" class="select-size-xs ga_wajib">
											<option></option>
											<?php foreach ($getSKP as $i) {
												echo '<option value="'.$i["jenis_skp_id"].'" kode="'.$i["jenis_skp_kode"].'">'.$i["jenis_skp_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>

								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Kegiatan Harian</label>
									<div class="col-md-9">
										<textarea class="form-control input-xs wajib" name="kegiatan_harian_nama" id="kegiatan_harian_nama" style="width:720px;height:40px;"></textarea>
										<!-- <input type="text" name="jenjang_jabatan_jabatan_nama" id="jenjang_jabatan_jabatan_nama" class="form-control input-xs wajib"> -->
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Keterangan kegiatan Harian</label>
									<div class="col-md-9">
										<!-- <textarea class="form-control wajib" name="kegiatan_harian_keterangan" id="editor-fulls" rows="4" cols="4"></textarea> -->
										<textarea class="form-control wajib" name="kegiatan_harian_keterangan" id="kegiatan_harian_keterangan" style="width:720px;height:300px;"></textarea>
										<!-- <textarea class="form-control wajib" name="kegiatan_harian_keterangan" id="kegiatan_harian_keterangan" cols="3" rows="5"></textarea> -->
										<!-- <input type="text" name="jenjang_jabatan_jabatan_nama" id="jenjang_jabatan_jabatan_nama" class="form-control wajib"> -->
									</div>
								</div>

								<div class="form-group form-group-xs">
									<div class="col-md-9 col-md-offset-3">
										<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
										<input type="hidden" name="jenjang_jabatan_pegawai_id" id="jenjang_jabatan_pegawai_id" class="form-control input-xs wajib" value="">
										<input type="hidden" name="jenjang_jabatan_pegawai_nip" id="jenjang_jabatan_pegawai_nip" class="form-control input-xs wajib" value="">
										<input type="hidden" name="isUpdate" id="isUpdate" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="jenjang_jabatan_id" id="jenjang_jabatan_id" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="kegiatan_id" id="kegiatan_id" class="form-control input-xs wajib" value="0">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 form_lihat_data" style="display: none;">
						<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Lihat Data kegiatan </span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Waktu Mulai</label>
									<div class="col-md-9">
										<div class="form-control-static kegiatan_waktu_mulai">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Waktu Selesai</label>
									<div class="col-md-9">
										<div class="form-control-static kegiatan_waktu_akhir">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-lg-3 control-label">Sasaran Kerja Pegawai (SKP)</label>
									<div class="col-lg-9">
										<div class="form-control-static kegiatan_skp_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">kegiatan harian</label>
									<div class="col-md-9">
										<div class="form-control-static kegiatan_harian_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Keterangan kegiatan Harian</label>
									<div class="col-md-9">
										<div class="form-control-static kegiatan_harian_keterangan">-</div>
									</div>
								</div>
								
								<div class="form-group form-group-xs lihat_data_statik">
									<div class="col-md-9 col-md-offset-3">
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>
	<script type="text/javascript">
		function set_non_aktif(id,kegiatan_jadwal_kegiatan_id){
			$('#text_konfirmasi').html('Anda yakin menghapus data ini ?');
			$('#konfirmasipenyimpanan').modal('show');
			$('#setujukonfirmasibutton').unbind();
			$('#setujukonfirmasibutton').on('click', function () {
				$('#konfirmasipenyimpanan').modal('hide');
				$('.angkadoank').inputmask('remove');
				$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
				window.location.href = base_url+'logbook/elogbook/hapus/'+id+'/'+kegiatan_jadwal_kegiatan_id;
			});
		}

		function lihat_data(data){
			$('.form_tambah_data').hide();
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);			
			reset_form();
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					lihat_data(data);
				});
			}else{
				$('.form_lihat_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_lihat_data").offset().top-60
					}, 500);
					var kegiatan_waktu_mulai = datajson.kegiatan_waktu_mulai;
					var kegiatan_waktu_akhir = datajson.kegiatan_waktu_akhir;
					var kegiatan_skp_nama = datajson.kegiatan_skp_nama;
					var kegiatan_harian_nama = datajson.kegiatan_harian_nama;
					var kegiatan_harian_keterangan = datajson.kegiatan_harian_keterangan;

					// $(".kegiatan_waktu_mulai").html(substr(kegiatan_waktu_mulai, 0, 5));
					// $(".kegiatan_waktu_akhir").html(substr(kegiatan_waktu_akhir, 0, 5));
					$(".kegiatan_waktu_mulai").html(kegiatan_waktu_mulai);
					$(".kegiatan_waktu_akhir").html(kegiatan_waktu_akhir);
					$(".kegiatan_skp_nama").html(kegiatan_skp_nama);
					$(".kegiatan_harian_nama").html(kegiatan_harian_nama);
					$(".kegiatan_harian_keterangan").html(kegiatan_harian_keterangan);
					
				});
			}
		}
		function update_data(data){
			var decoded = $("<div/>").html(data).text();
			// var decoded = '[{ "Display": "string1", "Sell": "string2" }, { "Display": "string1", "Sell": "string2" }]';
			// var datajson = $.parseJSON(decoded);
			var datajson = $.parseJSON(decoded);
			// alert(datajson);
			reset_form();
			$('.form_lihat_data').hide();
			$('.label_aksi_form').html('Form Perubahan Data Kegiatan');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					update_data(data);
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
					$('#isUpdate').val(1);
					var kegiatan_id = datajson.kegiatan_id;
					var kegiatan_waktu_mulai = datajson.kegiatan_waktu_mulai;
					var kegiatan_waktu_akhir = datajson.kegiatan_waktu_akhir;
					var kegiatan_skp = datajson.kegiatan_skp;
					var kegiatan_harian_nama = datajson.kegiatan_harian_nama;
					var kegiatan_harian_keterangan = datajson.kegiatan_harian_keterangan;

					$('#kegiatan_id').val(kegiatan_id);
					$('#kegiatan_waktu_mulai').val(kegiatan_waktu_mulai);
					$('#kegiatan_waktu_akhir').val(kegiatan_waktu_akhir);
					$('#kegiatan_harian_nama').val(kegiatan_harian_nama);
					$('#kegiatan_harian_keterangan').val(kegiatan_harian_keterangan);
					$('#kegiatan_id').val(datajson.kegiatan_id);

					$('#kegiatan_skp').select2('val', '');
					$('#kegiatan_skp').val(kegiatan_skp);
					$('#kegiatan_skp').trigger('change');

				});
			}
		}
// 		function update_data(data){
// 			var decoded = $("<div/>").html(data).text();
// 			var datajson = $.parseJSON(decoded);
// 			reset_form();
			
// 			$('.form_lihat_data').hide();
// 			$('.label_aksi_form').html('Form Perubahan Data Riwayat Jabatan');
// 			if($('.form_tambah_data').is(":visible")){
// 				$('.form_tambah_data').slideUp('slow',function(){
// 					update_data(data);
// 				});
// 			}else{
// 				$('.form_tambah_data').slideDown('fast',function(){
// 					$('html, body').animate({
// 						scrollTop: $(".form_tambah_data").offset().top-60
// 					}, 500);
// 					$('#isUpdate').val(1);

// 					// var unit_kerja_level = datajson.jenjang_jabatan_id;
// 					// var hirarki = datajson.unit_kerja_hirarki.split('|');
// 					// alert(hirarki[2]);
// 					// alert(datajson.unit_kerja_id);
// 					var unit_kerja_id_5 = datajson.unit_kerja_id_5;
// 					var unit_kerja_id_4 = datajson.unit_kerja_id_4;
// 					var unit_kerja_id_3 = datajson.unit_kerja_id_3;
// 					var unit_kerja_id_2 = datajson.unit_kerja_id_2;
// 					var unit_kerja_id = datajson.unit_kerja_id;
// 					if(unit_kerja_id_5 != null && unit_kerja_id_5 != 1){
// 						$('#jenjang_jabatan_unit_kerja_id_1').attr('data-id', unit_kerja_id_4);
// 						$('#jenjang_jabatan_unit_kerja_id_2').attr('data-id', unit_kerja_id_3);
// 						$('#jenjang_jabatan_unit_kerja_id_3').attr('data-id', unit_kerja_id_2);
// 						$('#jenjang_jabatan_unit_kerja_id_4').attr('data-id', unit_kerja_id);

// 						$("#jenjang_jabatan_unit_kerja_id_1").val(unit_kerja_id_5);
// 						$('#jenjang_jabatan_unit_kerja_id_1').trigger('change');

						
// 						// $('#jenjang_jabatan_unit_kerja_id_5').attr('data-id', unit_kerja_id);

// 						// $("#jenjang_jabatan_unit_kerja_id_2").val(unit_kerja_id_4);
// 						// $('#jenjang_jabatan_unit_kerja_id_2').trigger('change');

// 						// $("#jenjang_jabatan_unit_kerja_id_3").val(unit_kerja_id_3);
// 						// $('#jenjang_jabatan_unit_kerja_id_3').trigger('change');

// 						// $("#jenjang_jabatan_unit_kerja_id_4").val(unit_kerja_id_2);
// 						// $('#jenjang_jabatan_unit_kerja_id_4').trigger('change');

// 						// $("#jenjang_jabatan_unit_kerja_id_5").val(unit_kerja_id);
// 						// $('#jenjang_jabatan_unit_kerja_id_5').trigger('change');
// 					}

// 					else{
// 						if(unit_kerja_id_4 != null && unit_kerja_id_4 != 1){		
// 							$('#jenjang_jabatan_unit_kerja_id_1').attr('data-id', unit_kerja_id_3);
// 							$('#jenjang_jabatan_unit_kerja_id_2').attr('data-id', unit_kerja_id_2);
// 							$('#jenjang_jabatan_unit_kerja_id_3').attr('data-id', unit_kerja_id);

// 							$("#jenjang_jabatan_unit_kerja_id_1").val(unit_kerja_id_4);
// 							$('#jenjang_jabatan_unit_kerja_id_1').trigger('change');
							
// 							//$('#jenjang_jabatan_unit_kerja_id_4').attr('data-id', unit_kerja_id);

// 							// $("#jenjang_jabatan_unit_kerja_id_2").val(unit_kerja_id_3);
// 							// $('#jenjang_jabatan_unit_kerja_id_2').trigger('change');

// 							// $("#jenjang_jabatan_unit_kerja_id_3").val(unit_kerja_id_2);
// 							// $('#jenjang_jabatan_unit_kerja_id_3').trigger('change');

// 							// $("#jenjang_jabatan_unit_kerja_id_4").val(unit_kerja_id);
// 							// $('#jenjang_jabatan_unit_kerja_id_4').trigger('change');
// 						}
// 						else{
// 							if(unit_kerja_id_3 != null && unit_kerja_id_3 != 1){
// 								$('#jenjang_jabatan_unit_kerja_id_1').attr('data-id', unit_kerja_id_2);
// 								$('#jenjang_jabatan_unit_kerja_id_2').attr('data-id', unit_kerja_id);

// 								$("#jenjang_jabatan_unit_kerja_id_1").val(unit_kerja_id_3);
// 								$('#jenjang_jabatan_unit_kerja_id_1').trigger('change');
								
// 								//$('#jenjang_jabatan_unit_kerja_id_3').attr('data-id', unit_kerja_id);

// 								// $("#jenjang_jabatan_unit_kerja_id_2").val(unit_kerja_id_2);
// 								// $('#jenjang_jabatan_unit_kerja_id_2').trigger('change');

// 								// $("#jenjang_jabatan_unit_kerja_id_3").val(unit_kerja_id);
// 								// $('#jenjang_jabatan_unit_kerja_id_3').trigger('change');
// 							}
// 							else{
// 								if(unit_kerja_id_2 != null && unit_kerja_id_2 != 1){
// 									$('#jenjang_jabatan_unit_kerja_id_1').attr('data-id', unit_kerja_id);

// 									$("#jenjang_jabatan_unit_kerja_id_1").val(unit_kerja_id_2);
// 									$('#jenjang_jabatan_unit_kerja_id_1').trigger('change');
									
// 									//$('#jenjang_jabatan_unit_kerja_id_2').attr('data-id', unit_kerja_id);

// 									// $("#jenjang_jabatan_unit_kerja_id_2").val(unit_kerja_id);
// 									// $('#jenjang_jabatan_unit_kerja_id_2').trigger('change');
// 								}
// 								else{
// 									if(unit_kerja_id != null && unit_kerja_id != 1){
// 										$("#jenjang_jabatan_unit_kerja_id_1").val(unit_kerja_id);
// 										$('#jenjang_jabatan_unit_kerja_id_1').trigger('change');

// 										//$('#jenjang_jabatan_unit_kerja_id_1').attr('data-id', unit_kerja_id);
// 									}
// 								}
// 							}
// 						}
// 					}
					
// 					var jenjang_jabatan_jabatan_nama = datajson.jenjang_jabatan_jabatan_nama;
// 					var jenjang_jabatan_unit_kerja_id = datajson.jenjang_jabatan_unit_kerja_id;
// 					var kegiatan_waktu_mulai = datajson.kegiatan_waktu_mulai;
// 					var kegiatan_waktu_akhir = datajson.kegiatan_waktu_akhir;
// 					var jenjang_jabatan_nomor_sk = datajson.jenjang_jabatan_nomor_sk;
// 					var jenjang_jabatan_tanggal_sk = datajson.jenjang_jabatan_tanggal_sk;

// 					$("#jenjang_jabatan_jabatan_nama").val(jenjang_jabatan_jabatan_nama);
// 					$("#jenjang_jabatan_unit_kerja_id").val(jenjang_jabatan_unit_kerja_id);
// 					$("#kegiatan_waktu_mulai").val(kegiatan_waktu_mulai);
// 					$("#kegiatan_waktu_akhir").val(kegiatan_waktu_akhir);
// 					$("#jenjang_jabatan_nomor_sk").val(jenjang_jabatan_nomor_sk);
// 					$("#jenjang_jabatan_tanggal_sk").val(jenjang_jabatan_tanggal_sk);
// 				});
// }
// }

function tambah_data(){
	$('.form_lihat_data').hide();
	reset_form();
	$('.label_aksi_form').html('Form Penambahan Data Pendidikan');
	if($('.form_tambah_data').is(":visible")){
		$('.form_tambah_data').slideUp('slow',function(){
			$('.form_tambah_data').slideDown('fast',function(){
				$('html, body').animate({
					scrollTop: $(".form_tambah_data").offset().top-60
				}, 500);

			});
		});
	}else{
		$('.form_tambah_data').slideDown('fast',function(){
			$('html, body').animate({
				scrollTop: $(".form_tambah_data").offset().top-60
			}, 500);
		});
	}
}
function batal_data(){
	if($('.form_tambah_data').is(":visible")){
		$('.form_tambah_data').slideUp('slow',function(){
			reset_form();
		});
	}
	if($('.form_lihat_data').is(":visible")){
		$('.form_lihat_data').slideUp('slow',function(){
			reset_form();
		});
	}
}
function reset_form(){
			$('#kegiatan_id').val(0);
			$('#kegiatan_waktu_mulai').val('');
			$('#kegiatan_waktu_akhir').val('');
			$('#kegiatan_skp').select2('val', ' ');
			$('#kegiatan_harian_nama').val('');
			$('#kegiatan_harian_keterangan').val('');
			$('#kegiatan_id').val(0);
			// $("#jenjang_jabatan_id").val('');
			// $("#jenjang_jabatan_pegawai_id").val('');
			//$("#jenjang_jabatan_pegawai_nip").val('');
			
			$('.validation-error-label').css('display','none');
		}
		
		function validasi(){
			var mulai = document.getElementById("kegiatan_waktu_mulai").value;
			var akhir = document.getElementById("kegiatan_waktu_akhir").value;

			if (mulai < akhir) {
				return true;
			}else{
				alert('Waktu mulai tidak boleh lebih dari waktu akhir !');
				refresh();
			}
		}
		function refresh() {

		    setTimeout(function () {
		        location.reload()
		    }, 100);
		}

		$(document).ready(function() {
			$("#kegiatan_waktu_mulai").AnyTime_picker({
				format: "%H:%i"
			});
			$("#kegiatan_waktu_akhir").AnyTime_picker({
				format: "%H:%i"
			});
			
			// CKEDITOR.replace('editor-fulls', {
			// 	height: 100,
			// 	extraPlugins: 'forms'
			// });
			$('.select-size-xs').select2();
			var d = new Date();

			$('.pickttl').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});
			$('.pickttlstart').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});

			$('.pickttlend').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});

		//On value change
		$(".pickttlstart").change(function(e) {

			var mulai1 = $("#diklat_tanggal_mulai").val().split(" ");
			var tgl = mulai1[0];
			var bulan = mulai1[1];
			var tahun = mulai1[2];

			$('.pickttlend').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				min: new Date(tahun, 5, 20),
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});

			var yesterday  = new Date((new Date()).valueOf()-1000*60*60*24);
			var neededDates = datas.prdetail.PRD_DATE_NEED;
			var neededDate = neededDates.split('-');

			var tglneed = $('#tanggal_dibutuhkan').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agt', 'Sep', 'Okt', 'Nov', 'Des'],
				weekdaysFull: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu'],
				weekdaysShort: ['Min', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectYears: true,
				selectMonths: true,
				format: 'd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd',
				today: 'Hari ini',
				clear: 'Reset',
				close: 'Batal',
				disable: [
				{ from: [0,0,0], to: yesterday }
				]
			});

			var dpicker = tglneed.pickadate('picker');
			dpicker.set('select', [neededDate[0], (neededDate[1]-1), neededDate[2]]);

		});

	});

		function ToJavaScriptDateViewDoank(value) {
        //var pattern = /Date\(([^)]+)\)/;
        //var results = pattern.exec(value);
        //var dt = new Date(parseFloat(results[1]));
        var MonthInd = GetMonthIndInt((value.getMonth() + 1));
        return value.getDate() + " " + MonthInd + " " + value.getFullYear();
    }

    function GetMonthIndInt(month) {
    	switch (month) {
    		case 1:
    		return "Januari"
    		break;
    		case 2:
    		return "Februari"
    		break;
    		case 3:
    		return "Maret"
    		break;
    		case 4:
    		return "April"
    		break;
    		case 5:
    		return "Mei"
    		break;
    		case 6:
    		return "Juni"
    		break;
    		case 7:
    		return "Juli"
    		break;
    		case 8:
    		return "Agustus"
    		break;
    		case 9:
    		return "September"
    		break;
    		case 10:
    		return "Oktober"
    		break;
    		case 11:
    		return "November"
    		break;
    		case 12:
    		return "Desember"
    		break;
    		default:
    		return "Undefined"
    	}
    }

    function  get_unit_kerja_hirarki(ini,lvl) {
    	var attrid = $(ini).attr('data-id');
    	//alert(attrid);
    	if(lvl==1){
    		$('.jenjang_jabatan_unit_kerja_id_5').slideUp('fast',function(){
    			$('#jenjang_jabatan_unit_kerja_id_5').select2('val', '');
    			$('.jenjang_jabatan_unit_kerja_id_4').slideUp('fast',function(){
    				$('#jenjang_jabatan_unit_kerja_id_4').select2('val', '');
    				$('.jenjang_jabatan_unit_kerja_id_3').slideUp('fast',function(){
    					$('#jenjang_jabatan_unit_kerja_id_3').select2('val', '');
    					$('.jenjang_jabatan_unit_kerja_id_2').slideUp('fast',function(){
    						$('#jenjang_jabatan_unit_kerja_id_2').select2('val', '');
    					});
    				});
    			});
    		});
    	}else if(lvl==2){
    		$('.jenjang_jabatan_unit_kerja_id_5').slideUp('fast',function(){
    			$('#jenjang_jabatan_unit_kerja_id_5').select2('val', '');
    			$('.jenjang_jabatan_unit_kerja_id_4').slideUp('fast',function(){
    				$('#jenjang_jabatan_unit_kerja_id_4').select2('val', '');
    				$('.jenjang_jabatan_unit_kerja_id_3').slideUp('fast',function(){
    					$('#jenjang_jabatan_unit_kerja_id_3').select2('val', '');
    				});
    			});
    		});
    	}else if(lvl==3){
    		$('.jenjang_jabatan_unit_kerja_id_5').slideUp('fast',function(){
    			$('#jenjang_jabatan_unit_kerja_id_5').select2('val', '');
    			$('.jenjang_jabatan_unit_kerja_id_4').slideUp('fast',function(){
    				$('#jenjang_jabatan_unit_kerja_id_4').select2('val', '');
    			});
    		});
    	}else if(lvl==4){
    		$('.jenjang_jabatan_unit_kerja_id_5').slideUp('fast',function(){
    			$('#jenjang_jabatan_unit_kerja_id_5').select2('val', '');
    		});
    	}
    	var parent_id_kode  = $('option:selected', ini).attr('parent_id_kode');
    	var jabatan_id  = $('option:selected', ini).attr('jabatan_id');

    	$('#jenjang_jabatan_jabatan_id').select2('val', '');
    	$('#jenjang_jabatan_jabatan_id').val(jabatan_id);
    	$('#jenjang_jabatan_jabatan_id').trigger('change');

    	$.ajax({
    		url: base_url+'kepegawaian/pns/get_unit_kerja_hirarki',
    		type: "post",
    		dataType: 'json',
    		data: { unit_kerja_parent_id_kode: parent_id_kode, unit_kerja_level: (lvl+2)},
    		success: function (data) {

    			var unit_kerja = data.unit_kerja;
    			var newoption = "<option></option>";

    			for(var i = 0; i < unit_kerja.length; i ++){
    				newoption+='<option value="'+unit_kerja[i].unit_kerja_id+'" parent_id_kode="'+unit_kerja[i].unit_kerja_id_kode+'"  jabatan_id="'+unit_kerja[i].jabatan_id+'" '+((unit_kerja[i].unit_kerja_id == attrid)?"selected":"")+'>'+unit_kerja[i].unit_kerja_nama+'</option>';
    			}
    			$('#jenjang_jabatan_unit_kerja_id_'+(lvl+1)).html(newoption);
    			if(unit_kerja.length>0){
    				$('.jenjang_jabatan_unit_kerja_id_'+(lvl+1)).slideDown('fast');
    			}else{
    				$('.jenjang_jabatan_unit_kerja_id_'+(lvl+1)).slideUp('fast');
    			}
    			if(attrid > 1){
    				$('#jenjang_jabatan_unit_kerja_id_'+(lvl+1)).trigger('change');
    			}
    		}
    	});
    }
</script>
