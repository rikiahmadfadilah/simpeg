
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Form Ubah Pengaturan Aplikasi</h6>
	</div>

	<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<form class="need_validation form-horizontal" action="<?php echo base_url('pengaturan/aplikasi/update'); ?>" method="post" enctype="multipart/form-data" onsubmit="return validasi_input(this)">
		                <?php 
		                	foreach ($getConfig as $config) {
		                  	if(strlen($config['config_value'])>50){
		                    echo '<div class="form-group form-group-xs">
		                          <label class="col-md-3 control-label">'.$config['config_name'].'</label>
		                          <div class="col-md-9">
		                            <textarea rows="3" cols="12" class="form-control input-xs wajib" name="value[]">'.$config['config_value'].'</textarea>
		                            <input type="hidden" class="form-control input-xs wajib" name="kode[]" value="'.$config['config_code'].'" />
		                          </div>
		                        </div>';
		                  }else{
		                    echo '<div class="form-group form-group-xs">
		                        <label class="col-md-3 control-label">'.$config['config_name'].'</label>
		                        <div class="col-md-9">
		                          <input type="text" class="form-control input-xs wajib" name="value[]" value="'.$config['config_value'].'"/>
		                         <input type="hidden" name="kode[]" value="'.$config['config_code'].'" />
		                        </div>
		                      </div>';
		                  }  
		                }
		               ?>
				</div>
			</div>
			<div class="row">
				<!-- <div class="form-group">
                  	<label class="control-label col-lg-2"></label>
                  	<div class="col-lg-10">
	                    <label class="checkbox-inline">
                        	<input type="checkbox" id="agreement" class="styled" required="true">
	                        Saya mengerti dan bertanggung jawab atas perubahan data diatas.
	                    </label>
              		</div>
                </div> -->
                <br>
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
					<a href="<?php echo base_url();?>pengaturan/aplikasi" class="btn btn-xs btn-warning">Batal</a>
					<input type="hidden" name="config_id" id="config_id"  class="form-control input-xs wajib" type="text" value="<?php echo $config["config_id"];?>" >
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
  function validatingForm(){

    var parent_add_agreement  = $('#agreement').parents('p');
    var add_agreement         = $('#agreement:checked').val();

      parent_add_agreement.removeClass('md-input-danger md-color-red-A700');
    
      var isvalidated = true;   

      if(!add_agreement) {
        isvalidated = false;
        parent_add_agreement.addClass('md-input-danger md-color-red-A700');
      }
      return isvalidated;
    }
</script>
