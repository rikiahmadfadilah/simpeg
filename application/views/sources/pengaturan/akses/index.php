<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
    <div class="panel-heading">
        <h6 class="panel-title">Data Hak Akses</h6>
        <div class="heading-elements">
            <div class="heading-btn">
                <a href="<?php echo base_url().'pengaturan/akses/create';?>" class="btn btn-primary btn-labeled btn-xs"><b><i class=" icon-plus3"></i></b> Tambah Data Hak Akses</a>
            </div>
        </div>
    </div>

    <div class="panel-body">
        <table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive epagawai_table" id="list_data">
            <thead>
                <tr>
                    <th class="text-center">Nama Hak Akses</th>
                    <th class="text-center">Total User</th>
                    <th class="text-center">Total Menu</th>
                    <th class="text-center" style="width: 150px;min-width: 150px;">Status</th>
                    <th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
                </tr>
            </thead>
            
        </table>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#list_data').dataTable( {
            "processing": true,
            "serverSide": true,
            "bServerSide": true,
            "sAjaxSource": base_url+"pengaturan/akses/list_data",
            "aaSorting": [],
            "order": [],
            "iDisplayLength": 10,
            "aoColumns": [
            { "sClass": "text-left" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "bSortable": false, "sClass": "text-center" },
            ],
            "fnDrawCallback": function () {
                set_default_datatable();
            },
        });
    });
    function set_aktif(id){
        $('#text_konfirmasi').html('Anda yakin mengaktifkan data ini ?');
        $('#konfirmasipenyimpanan').modal('show');
        $('#setujukonfirmasibutton').unbind();
        $('#setujukonfirmasibutton').on('click', function () {
            $('#konfirmasipenyimpanan').modal('hide');
            $('.angkadoank').inputmask('remove');
            $('#text_konfirmasi').html('Anda yakin dengan data ini ?');
            window.location.href = base_url+'pengaturan/akses/aktif/'+id;
        });
    }
    function set_non_aktif(id){
        $('#text_konfirmasi').html('Anda yakin nonaktifkan data ini ?');
        $('#konfirmasipenyimpanan').modal('show');
        $('#setujukonfirmasibutton').unbind();
        $('#setujukonfirmasibutton').on('click', function () {
            $('#konfirmasipenyimpanan').modal('hide');
            $('.angkadoank').inputmask('remove');
            $('#text_konfirmasi').html('Anda yakin dengan data ini ?');
            window.location.href = base_url+'pengaturan/akses/non_aktif/'+id;
        });
    }
</script>
