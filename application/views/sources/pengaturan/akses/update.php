
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Perubahan Data Hak Akses</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-xs">
	                    <label class="col-md-2 control-label">Nama Hak Akses</label>
	                    <div class="col-md-2">
	                      	<input type="text" name="akses_namas" id="akses_namas" class="form-control input-xs wajib" type="text" value="<?php echo $getDetail["akses_nama"];?>">
	                    </div>
	                </div>
					<table id="tbl_akses" class="table table-striped table-bordered table-hover epagawai_table" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th style="text-align:center;" width="30px">Def<br>Menu</th>
                            <th width="*">Modul &amp; Menu</th>
                            <th style="text-align:center;" width="30px">Read</th>
                            <th style="text-align:center;" width="30px">Create</th>
                            <th style="text-align:center;" width="30px">Update</th>
                            <th style="text-align:center;" width="30px">Delete</th>
                            <th style="text-align:center;" width="30px">Approve</th>
                            <th style="text-align:center;" width="30px">Print</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($datamenus as $key => $menu) {

                            $padding = ($menu["menu_status"] == 1)?'':'padding-left:30px;';
                            $cekedread = '';
                            $cekedcreate = '';
                            $cekedupdate = '';
                            $cekeddelete = '';
                            $cekedapprove = '';
                            $cekedprint = '';
                            $cekedall = "";
                            for ($i=0; $i < count($dataakses); $i++) {

                                if($dataakses[$i]["akses_detail_menu_id"] == $menu["menu_id"] && $dataakses[$i]["akses_detail_can_read"] == 1){
                                    $cekedread = "checked";
                                }
                                if($dataakses[$i]["akses_detail_menu_id"] == $menu["menu_id"] && $dataakses[$i]["akses_detail_can_create"] == 1){
                                    $cekedcreate = "checked";
                                }
                                if($dataakses[$i]["akses_detail_menu_id"] == $menu["menu_id"] && $dataakses[$i]["akses_detail_can_update"] == 1){
                                    $cekedupdate = "checked";
                                }
                                if($dataakses[$i]["akses_detail_menu_id"] == $menu["menu_id"] && $dataakses[$i]["akses_detail_can_delete"] == 1){
                                    $cekeddelete = "checked";
                                }
                                if($dataakses[$i]["akses_detail_menu_id"] == $menu["menu_id"] && $dataakses[$i]["akses_detail_can_approve"] == 1){
                                    $cekedapprove = "checked";
                                }
                                if($dataakses[$i]["akses_detail_menu_id"] == $menu["menu_id"] && $dataakses[$i]["akses_detail_can_print"] == 1){
                                    $cekedprint = "checked";
                                }
                                if($cekedread == "checked" && $cekedcreate == "checked" && $cekedupdate == "checked" && $cekeddelete == "checked" && $cekedapprove == "checked"  && $cekedprint == "checked")
                                {
                                    $cekedall = "checked";
                                }
                            }
                            ?>
                            <tr>
                                 <td align="center">
                                    <input type="radio" name="default_menu" value="<?php echo $menu['menu_id']; ?>" <?php echo (($getDetail['akses_default_menu_id']==$menu['menu_id'])?"checked":""); ?> id="default_menu_<?php echo $menu['menu_id']; ?>">
                                </td>
                                <td style="padding-left: <?=$menu['MenuLevel']*2?>em;">
                                    <div class="checkbox" style="<?php echo $padding; ?>padding-top:0px;padding-bottom:0px;min-height:auto;">
                                        <label style="height:18px;">
                                            <input type="checkbox" <?php echo $cekedall;?> name="all[]" data-hirarki="<?php echo $menu['MenuHirarki']; ?>" class="all child<?php echo $menu['menu_parent_id']; ?>" id="all_<?php echo $menu['menu_id']; ?>" data-parent="<?php echo $menu['menu_parent_id']; ?>" value="<?php echo $menu['menu_id']; ?>" >
                                            <i class="input-helper"></i><?php echo $menu["menu_nama"]; ?>
                                        </label>
                                    </div>
                                </td>
                                <td align="center">
                                    <input type="checkbox" name="read[]" data-hirarki="<?php echo $menu['MenuHirarki']; ?>" class="child<?php echo $menu['menu_parent_id']; ?> read<?php echo $menu['menu_parent_id']; ?> ck<?php echo $menu['menu_id']; ?> cekbox" data-type="read" id="read_<?php echo $menu['menu_id']; ?>" data-parent="<?php echo $menu['menu_parent_id']; ?>" value="<?php echo $menu['menu_id']; ?>" <?php echo $cekedread;?>>
                                </td>
                                <td align="center">
                                    <input type="checkbox" name="create[]" data-hirarki="<?php echo $menu['MenuHirarki']; ?>" class="child<?php echo $menu['menu_parent_id']; ?> create<?php echo $menu['menu_parent_id']; ?> ck<?php echo $menu['menu_id']; ?> cekbox" data-type="create" id="create_<?php echo $menu['menu_id']; ?>" data-parent="<?php echo $menu['menu_parent_id']; ?>" value="<?php echo $menu['menu_id']; ?>" <?php echo $cekedcreate;?>>
                                </td>
                                <td align="center">
                                    <input type="checkbox" name="update[]" data-hirarki="<?php echo $menu['MenuHirarki']; ?>" class="child<?php echo $menu['menu_parent_id']; ?> update<?php echo $menu['menu_parent_id']; ?> ck<?php echo $menu['menu_id']; ?> cekbox" data-type="update" id="update_<?php echo $menu['menu_id']; ?>" data-parent="<?php echo $menu['menu_parent_id']; ?>" value="<?php echo $menu['menu_id']; ?>" <?php echo $cekedupdate;?>>
                                </td>
                                <td align="center">
                                    <input type="checkbox" name="delete[]" data-hirarki="<?php echo $menu['MenuHirarki']; ?>" class="child<?php echo $menu['menu_parent_id']; ?> delete<?php echo $menu['menu_parent_id']; ?> ck<?php echo $menu['menu_id']; ?> cekbox" data-type="delete" id="delete_<?php echo $menu['menu_id']; ?>" data-parent="<?php echo $menu['menu_parent_id']; ?>" value="<?php echo $menu['menu_id']; ?>" <?php echo $cekeddelete;?>>
                                </td>
                                <td align="center">
                                    <input type="checkbox" name="approve[]" data-hirarki="<?php echo $menu['MenuHirarki']; ?>" class="child<?php echo $menu['menu_parent_id']; ?> approve<?php echo $menu['menu_parent_id']; ?> ck<?php echo $menu['menu_id']; ?> cekbox" data-type="approve" id="approve_<?php echo $menu['menu_id']; ?>" data-parent="<?php echo $menu['menu_parent_id']; ?>" value="<?php echo $menu['menu_id']; ?>" <?php echo $cekedapprove;?>>
                                </td>
                                <td align="center">
                                    <input type="checkbox" name="print[]" data-hirarki="<?php echo $menu['MenuHirarki']; ?>" class="child<?php echo $menu['menu_parent_id']; ?> print<?php echo $menu['menu_parent_id']; ?> ck<?php echo $menu['menu_id']; ?> cekbox" data-type="print" id="print_<?php echo $menu['menu_id']; ?>" data-parent="<?php echo $menu['menu_parent_id']; ?>" value="<?php echo $menu['menu_id']; ?>" <?php echo $cekedprint;?>>
                                </td>
                            </tr>
                        <?php } ?>
                    	</tbody>
                  	</table><br>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
					<a href="<?php echo base_url();?>pengaturan/akses" class="btn btn-xs btn-warning">Kembali</a>
					<input type="hidden" name="akses_id" id="akses_id"  class="form-control input-xs wajib" type="text" value="<?php echo $getDetail["akses_id"];?>" >
					
				</div>
			</div>

		</form>
	</div>
</div>

<script type="text/javascript">
     var $ = jQuery;
     $(document).ready(function(){
        $('.all').each(function(){
            var val = $(this).val();
            var cek = (($('.all[data-parent="'+val+'"]').length == 0)?1:0);
            if(cek==1){
                $('#default_menu_'+val).show();
            }else{
                 $('#default_menu_'+val).hide();
            }
        })
     });
     function allclick(){
        
     }
     $(".all").click(function(){
         var ref = $(this).val();
         var dataparent = $(this).attr('data-parent');
         var hirarki = $(this).attr('data-hirarki');

         var action = false;
         if(this.checked){
             action = true;
         }
         $(".child"+ref).each(function(){
             if($(this).attr('data-parent') == ref){
                 $(this).prop('checked', action);
             }
         });
         $('#read_'+ref).prop('checked', action);
         $('#create_'+ref).prop('checked', action);
         $('#update_'+ref).prop('checked', action);
         $('#delete_'+ref).prop('checked', action);
         $('#approve_'+ref).prop('checked', action);
         $('#print_'+ref).prop('checked', action);
         $("input[data-hirarki^='"+hirarki+"']").prop('checked', action);
         var count = $(".child"+ref).length;
         if(count == 0){
             var countcek = 0;
             var countall = $(".child"+dataparent).length;
             $(".child"+dataparent).each(function(){
                 if(this.checked){
                     countcek++;
                 }
             });
             if(countcek>0){
                 $('#all_'+dataparent).prop('checked',true);
                 $('#read_'+dataparent).prop('checked', true);
                 $('#create_'+dataparent).prop('checked', true);
                 $('#update_'+dataparent).prop('checked', true);
                 $('#delete_'+dataparent).prop('checked', true);
                 $('#approve_'+dataparent).prop('checked', true);
                 $('#print_'+dataparent).prop('checked', true);
                 

             }else{
                 $('#all_'+dataparent).prop('checked',false);
                 $('#read_'+dataparent).prop('checked', false);
                 $('#create_'+dataparent).prop('checked', false);
                 $('#update_'+dataparent).prop('checked', false);
                 $('#delete_'+dataparent).prop('checked', false);
                 $('#approve_'+dataparent).prop('checked', false);
                 $('#print_'+dataparent).prop('checked', false);
             }
             //alert("input[data-hirarki=^='"+hirarki+"']")
         }
     });

     $('.cekbox').click(function(){
         var ref = $(this).val();
         var dataparent = $(this).attr('data-parent');
         var type = $(this).attr('data-type');
         var action = false;
         if(this.checked){
             action = true;
         }
         $("."+type+ref).each(function(){
             if($(this).attr('data-parent') == ref){
                 $(this).prop('checked', action);
             }
         });
         if(this.checked){
             action = true;
             $("#"+type+'_'+dataparent).prop('checked', true);
             if(type!='read'){
                 $("#read_"+ref).prop('checked', true);
                 $("#read_"+dataparent).prop('checked', true);
                 $(".read"+ref).each(function(){
                     if($(this).attr('data-parent') == ref){
                         $(this).prop('checked', action);
                     }
                 });
             }
         }else{
             var n = 0;
             $("."+type+dataparent).each(function(){
                 if(this.checked){
                     n++;
                 }
             });
             if(n==0){
                 $("#"+type+'_'+dataparent).prop('checked', false);
             }
         }
         cekuncekheader(ref,dataparent);
     });


     function cekuncekheader(ref,dataparent){
         var n = 0;
         $(".ck"+ref).each(function(){
             if(this.checked){
                 n++;
             }
         });
         if(n<6){
             $('#all_'+ref).prop('checked',false);
             $(".all").each(function(){
                 if($(this).attr('data-parent') == ref){
                     $(this).prop('checked', false);
                 }
             });
         }else{
             $('#all_'+ref).prop('checked',true);
             $(".all").each(function(){
                 if($(this).attr('data-parent') == ref){
                     $(this).prop('checked', true);
                 }
             });

         }
         var m = 0;
         $(".ck"+dataparent).each(function(){
             if(this.checked){
                 m++;
             }
         });
         if(m<6){
             $('#all_'+dataparent).prop('checked',false);
         }else{
             $('#all_'+dataparent).prop('checked',true);
         }
     }

 </script>
