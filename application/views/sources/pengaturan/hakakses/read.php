
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Lihat Data Hak Akses</h6>
	</div>

	<div class="panel-body1">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<!--div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Kode Jabatan</label>
						<div class="col-md-1">
							<input type="text" name="jabatan_kode" id="jabatan_kode"  class="form-control input-xs wajib" type="text">
						</div>
					</div-->
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama Hak Akses</label>
						<div class="col-md-9">
							<input disabled type="text" name="akses_nama" id="akses_nama"  value="<?=$akses['akses_nama']?>" class="form-control input-xs wajib" type="text">
						    <input type="hidden" name="akses_id" id="akses_id"  value="<?=$akses['akses_id']?>" class="form-control input-xs wajib">
						</div>
					</div>
				</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<!--button type="submit" class="btn btn-xs btn-primary">Simpan</button--> 
					<a href="<?php echo base_url();?>pengaturan/hakakses" class="btn btn-xs btn-warning">Kembali</a>
					
				</div>
			</div>
				
		
    <div class="panel-body">
        <table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive epagawai_table" id="list_data">
            <thead>
                <tr>
                    <th class="text-center" width="5%" rowspan=2>No</th>
                    <th class="text-center" width="50%" rowspan=2>Menu</th>
                    <th class="text-center" width="50%" colspan=7>Hak Akses</th>
                </tr>
                <tr>
                    <th class="text-center" width="9%">Read</th>
                    <th class="text-center" width="9%">Create</th>
                    <th class="text-center" width="9%">Update</th>
                    <th class="text-center" width="9%">Delete</th>
                    <th class="text-center" width="9%">Approve</th>
                    <th class="text-center" width="9%">Print</th>
              </tr>
            </thead>
			<tbody>
				<?
				//print_r($data);				
				$no=1;
				foreach ($data as $row):?>
				<tr>
					<td style="width:10px;text-align:left;font-size:12px;"><?=$no;?></td>
					<td style="width:80px;text-align:left;font-size:12px;"><?=$row['menu_nama']; ?>
					<input checked type="hidden" name="menu_id[]" value="<?=$row['menu_id']; ?>">
					<input type="hidden" name="akses_detail_id[]" value="<?=$row['akses_detail_id']; ?>">
					</td>

					<td style="width:80px;text-align:center;font-size:12px;">
					<input disabled type="checkbox" <?=($row['akses_detail_can_read'] == 1 ? 'checked':'')?> name="re_<?=$row['menu_id']?>" 
					value="<?=($row['akses_detail_can_read'] == 1 ? '1':'0')?>"></td>
					
					<td style="width:80px;text-align:center;font-size:12px;">
					<input disabled type="checkbox" <?=($row['akses_detail_can_create'] == 1 ? 'checked':'')?> name="cr_<?=$row['menu_id']?>" 
					value="<?=($row['akses_detail_can_create'] == 1 ? '1':'0')?>"></td>
					
					<td style="width:80px;text-align:center;font-size:12px;">
					<input disabled type="checkbox" <?=($row['akses_detail_can_update'] == 1 ? 'checked':'')?> name="up_<?=$row['menu_id']?>" 
					value="<?=($row['akses_detail_can_update'] == 1 ? '1':'0')?>"></td>
					
					<td style="width:80px;text-align:center;font-size:12px;">
					<input disabled type="checkbox" <?=($row['akses_detail_can_delete'] == 1 ? 'checked':'')?>  name="de_<?=$row['menu_id']?>" 
					value="<?=($row['akses_detail_can_delete'] == 1 ? '1':'0')?>"></td>
					
					<td style="width:80px;text-align:center;font-size:12px;">
					<input disabled type="checkbox" <?=($row['akses_detail_can_approve'] == 1 ? 'checked':'')?>  name="ap_<?=$row['menu_id']?>"
					value="<?=($row['akses_detail_can_approve'] == 1 ? '1':'0')?>"></td>
					
					<td style="width:80px;text-align:center;font-size:12px;">
					<input disabled type="checkbox" <?=($row['akses_detail_can_print'] == 1 ? 'checked':'')?>  name="pr_<?=$row['menu_id']?>" 
					value="<?=($row['akses_detail_can_print'] == 1 ? '1':'0')?>"></td>
				
				
				</tr>		
			    <?php 
				$no++;
				endforeach;?>	
			 </body>	
             <thead>
               <tr>
                    <th class="text-center" width="100%" colspan=10>&nbsp;</th>
                </tr>
            </thead>
				
            
        </table>
    </div>		
				
				
				
				
			</div>

		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$( "#jabatan_kode" ).rules( "add", {
			required: true,
			remote: {
				url: base_url+'master/jabatan/checkKodeJabatan',
				type: "post",
				data: {
					jabatan_kode: function() {
						return $( "#jabatan_kode" ).val();
					}
				}
			},
			messages: {
				required: "Wajib Diisi",
				remote: "Kode jabatan Telah Digunakan"
			}
		});
		$( "#akses_nama1" ).rules( "add", {
			required: true,
			remote: {
				url: base_url+'pengaturan/hakakses/check_hakakses',
				type: "post",
				data: {
					akses_nama: function() {
						return $( "#akses_nama" ).val();
					}
				}

			},
			messages: {
				required: "Wajib Diisi",
				remote: "Nama Hak Akses Sudah Tersedia"
			}
		});
	});
</script>

<SCRIPT language="javascript">
    $(document).ready(function(){
		$('input[type=checkbox]').click(function() {
		  $(this).val(1);	
		  //alert("val="+$(this).val());
		});
    });
</SCRIPT>
