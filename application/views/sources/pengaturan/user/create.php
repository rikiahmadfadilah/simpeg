
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Tambah Data Pengguna</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Pilih Pengguna</label>
						<div class="col-md-9">
							<select name="user_nip" id="user_nip" data-placeholder="Pilih Pengguna" class="select-size-xs wajib">
								<option value=""></option>
								<?php foreach ($pegawai as $i) {
									echo '<option value="'.$i["NIP"].'">'.$i["NIP"].' - '.$i["NAMA"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Pilih Hak Akses</label>
						<div class="col-md-9">
							<select name="user_akses_id[]" id="user_akses_id" multiple="multiple" data-placeholder="Pilih Hak Akses" class="select-size-xs wajib">
								<option value=""></option>
								<?php foreach ($akses as $a) {

									echo '<option value="'.$a["akses_id"].'">'.$a["akses_nama"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Pilih Default Hak Akses</label>
						<div class="col-md-9">
							<select name="role_is_default" id="role_is_default" data-placeholder="Pilih Default Hak Akses" class="select-size-xs wajib">
								<option></option>
							</select>
						</div>
					</div>
					<!-- <div class="form-group form-group-xs" style="display: none;" id="unit_kerja">
						<label class="col-md-3 control-label">Pilih Unit Kerja</label>
						<div class="col-md-9">
							<select class="select-size-xs wajib" name="user_operator_unit_kerja_id" id="user_operator_unit_kerja_id" data-placeholder="Pilih Unit Kerja">
								<option></option>
								<?php foreach ($unitkerja as $unit){?>
									<option value="<?php echo $unit['unit_kerja_id']?>"><?php echo $unit['unit_kerja_nama_with_tab']?></option>
								<?php }?>
							</select>
						</div>
					</div> -->
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
					<a href="<?php echo base_url();?>pengaturan/user" class="btn btn-xs btn-warning">Kembali</a>
					<input type="hidden" name="user_default_akses_id" id="user_default_akses_id"  class="form-control input-xs wajib" type="text" value="0" >
					
				</div>
			</div>

		</form>
	</div>
</div>
<script type="text/javascript">
	function chekcode(ini){
		
	}
	$(document).ready(function() {
		$( "#user_kode" ).rules( "add", {
			required: true,
			remote: {
				url: base_url+'pengaturan/user/checkKodeuser',
				type: "post",
				data: {
					user_kode: function() {
						return $( "#user_kode" ).val();
					}
				}
			},
			messages: {
				required: "Wajib Diisi",
				remote: "Kode user Telah Digunakan"
			}
		});
		$( "#user_nama" ).rules( "add", {
			required: true,
			remote: {
				url: base_url+'pengaturan/user/checkNamauser',
				type: "post",
				data: {
					user_nama: function() {
						return $( "#user_nama" ).val();
					}
				}

			},
			messages: {
				required: "Wajib Diisi",
				remote: "Nama user Sudah Tersedia"
			}
		});
		
	});

	$(document).ready(function() {

	});

	$('#user_akses_id').on('change', function() {
		$('#role_is_default').select2('val','');
		var akses = $('#user_akses_id').val()+"";
		var arrakses = akses.split(","); 
		var defoption = "<option></option>";
		var defaultakses = $('#user_default_akses_id').val();

		for (var i = 0; i < arrakses.length; i++) {
			var optionname = $("#user_akses_id option[value='"+arrakses[i]+"']").text();
			defoption += "<option value='"+arrakses[i]+"' "+((defaultakses==arrakses[i])?"selected":"")+">"+optionname+"</option>";
		}
		$('#role_is_default').html(defoption);
		$("#role_is_default").select2("destroy").select2( {containerCssClass: 'select-xs'});
		if (($.inArray("2", arrakses) != -1)) {
			$('#unit_kerja').show();
		}
		else{
			$('#unit_kerja').hide();
		}
	});
</script>
