
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Perubahan Data Pengguna</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Pengguna</label>
						<div class="col-md-9">
							<select name="user_nip" id="user_nip" data-placeholder="Pengguna" class="select-size-xs wajib">
								<option value="<?php echo $user["pegawai_nip"];?>"><?php echo $user["pegawai_nama"];?></option>
								<?php foreach ($users as $i) {
									echo '<option value="'.$i["pegawai_nip"].'" '.(($user["user_nip"]==$i["pegawai_nip"])?"selected":"").'>'.$i["pegawai_nama"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Username</label>
						<div class="col-md-9">
							<input type="text" name="username" id="username" readonly="readonly" class="form-control input-xs wajib" type="text" value="<?php echo $user["username"]?>" >
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Password</label>
						<div class="col-md-9">
							<input type="password" name="password" id="password" class="form-control input-xs wajib" type="text" value="#@$%%%$#@@" >
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Pilih Hak Akses</label>
						<div class="col-md-9">
							<select name="user_akses_id[]" id="user_akses_id" multiple="multiple" data-placeholder="Pilih Hak Akses" class="select-size-xs wajib">
								<option></option>
								<?php foreach ($akses as $i) {
									$selected = "";
									foreach ($roles as $rl) {
										if($rl["role_akses_id"]==$i["akses_id"]){
											$selected = "selected";
										}
									}
									if($selected==""){
										if($user["user_default_akses_id"]==$i["akses_id"]){
											$selected = "selected";
										}
									}
									echo '<option value="'.$i["akses_id"].'" '.$selected.'>'.$i["akses_nama"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Pilih Default Hak Akses</label>
						<div class="col-md-9">
							<select name="role_is_default" id="role_is_default" data-placeholder="Pilih Default Hak Akses" class="select-size-xs wajib">
								<option></option>
							</select>
						</div>
					</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-9 col-md-offset-3">
						<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
						<a href="<?php echo base_url();?>pengaturan/user" class="btn btn-xs btn-warning">Kembali</a>
						<input type="hidden" name="user_id" id="user_id"  class="form-control input-xs wajib" type="text" value="<?php echo $user["user_id"];?>" >
						<input type="hidden" name="user_default_akses_id" id="user_default_akses_id"  class="form-control input-xs wajib" type="text" value="<?php echo $user["user_default_akses_id"];?>" >
						<!-- <input type="hidden" name="user_akses_id" id="user_akses_id"  class="form-control input-xs wajib" type="text" value="<?php echo $user["user_akses_id"];?>" > -->
					</div>
				</div>

			</form>
		</div>
	</div>
	<script type="text/javascript">

		$(document).ready(function() {
			$('#user_akses_id').change();
		});

		$('#user_akses_id').on('change', function() {
			$('#role_is_default').select2('val','');
			var akses = $('#user_akses_id').val()+"";
			var arrakses = akses.split(","); 
			var defoption = "<option></option>";
			var defaultakses = $('#user_default_akses_id').val();

			for (var i = 0; i < arrakses.length; i++) {
				var optionname = $("#user_akses_id option[value='"+arrakses[i]+"']").text();
				defoption += "<option value='"+arrakses[i]+"' "+((defaultakses==arrakses[i])?"selected":"")+">"+optionname+"</option>";
			}
			$('#role_is_default').html(defoption);
			$("#role_is_default").select2("destroy").select2( {containerCssClass: 'select-xs'});
			//$('#role_is_default').select2('val',defaultakses);
			if (($.inArray("2", arrakses) != -1)) {
				$('#unit_kerja').show();
				$("#user_operator_unit_kerja_id").prop('required',true);
			}else if(($.inArray("4", arrakses) != -1)) {
				$('#unit_kerja').show();
				$("#user_operator_unit_kerja_id").prop('required',true);
			}else if(($.inArray("5", arrakses) != -1)) {
				$('#unit_kerja').show();
				$("#user_operator_unit_kerja_id").prop('required',true);
			}else{
				$('#unit_kerja').hide();
				$("#user_operator_unit_kerja_id").prop('required',false);
			}		});
	</script>
