
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Lihat Data User</h6>
	</div>
	<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">

		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">NIP Pengguna</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $user["pegawai_nip"];?>" disabled="disabled">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Nama Pengguna</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $user["pegawai_nama"];?>" disabled="disabled">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Nama Hak Akses</label>
						<div class="col-md-9">
							<select name="user_akses_id[]" disabled="disabled" id="user_akses_id" multiple="multiple" data-placeholder="Pilih Hak Akses" class="select-size-xs wajib">
								<option></option>
								<?php foreach ($akses as $i) {
									$selected = "";
									foreach ($roles as $rl) {
										if($rl["role_akses_id"]==$i["akses_id"]){
											$selected = "selected";
										}
									}
									echo '<option value="'.$i["akses_id"].'" '.$selected.'>'.$i["akses_nama"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Default Hak Akses</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $user["user_default_akses_nama"];?>" disabled="disabled">
						</div>
					</div>
					<div class="form-group form-group-xs" style="display: <?php (($user["unit_kerja_nama"]!=null)?"block":"none"); ?>;" id="unit_kerja">
						<label class="col-md-3 control-label">Unit Kerja</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $user["unit_kerja_nama"];?>" disabled="disabled">
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<a href="<?php echo base_url();?>pengaturan/user" class="btn btn-xs btn-warning">Kembali</a>
					<input type="hidden" name="user_id" id="user_id"  class="form-control input-xs wajib" type="text" value="<?php echo $user["user_id"];?>" >
				</div>
			</div>
		</div>
		

	</form>
</div>
<script type="text/javascript">
	
	$(document).ready(function() {
		

	});
</script>
