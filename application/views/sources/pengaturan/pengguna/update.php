
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Perubahan Data Pengguna</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
			
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pilih Pegawai</label>
						<div class="col-lg-9">
							<select data-placeholder="Pilih Pegawai" class="select-size-xs" id="user_nip" name="user_nip" disabled>
								<option></option>
								<?php foreach ($pegawai as $p) {
									  foreach ($user as $usr) {  
										echo '<option value="'.$p["pegawai_nip"].'" 
										'.(($usr["user_nip"]==$p["pegawai_nip"])?"selected":"").'>'.$p["pegawai_nip"].' - '.$p["pegawai_nama"].'</option>';
									  }}?>
							</select>
						</div>
					</div>

					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pilih Jenis Hak Akses</label>
						<div class="col-lg-9">
							<select data-placeholder="Pilih Jenis Hak Akses" class="select-size-xs" id="akses_id" name="akses_id">
								<option></option>
								<?php foreach ($hakakses as $ha) {
									  foreach ($user as $usr) {
										echo '<option value="'.$ha["akses_id"].'"
										'.(($usr["user_akses_id"]==$ha["akses_id"])?"selected":"").'>'.$ha["akses_nama"].'</option>';
									  }}?>
							</select>
						</div>
					</div>

					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Password</label>
						<div class="col-lg-9">
							<input data-placeholder="Isi Password" type="password" name="password" id="password"  class="form-control input-xs" type="text">
						</div>
					</div>
				
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
					<a href="<?php echo base_url();?>pengaturan/pengguna" class="btn btn-xs btn-warning">Kembali</a>
					<?
					  foreach ($user as $usr) {
						echo '<input type="hidden" name="user_id" id="user_id"  value="'.$usr["user_id"].'" class="form-control input-xs" type="text">';
					  }
					?>
				</div>
			</div>
				
				
			</div>

		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$( "#user_nip" ).rules( "add", {
			required: true,
			remote: {
				url: base_url+'pengaturan/pengguna/check_pengguna',
				type: "post",
				data: {
					user_nip: function() {
						return $( "#user_nip" ).val();
					}
				}
			},
			messages: {
				required: "Wajib Diisi",
				remote: "Nama Pengguna Telah Digunakan"
			}
		});

	});
</script>

<SCRIPT language="javascript">
    $(document).ready(function(){
		$('input[type=checkbox]').click(function() {
		  $(this).val(1);	
		  //alert("val="+$(this).val());
		});
    });
</SCRIPT>
