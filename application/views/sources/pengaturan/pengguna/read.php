
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Lihat Data Pengguna</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
			
				    <?php foreach ($pegawai as $pg) {?>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pilih Pegawai</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $pg["pegawai_nama"];?>" disabled="disabled">
						</div>
					</div>

					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pilih Jenis Hak Akses</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $pg["user_default_akses_nama"];?>" disabled="disabled">
						</div>
					</div>
				    <?}?>

				
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<!--button type="submit" class="btn btn-xs btn-primary">Simpan</button--> 
					<a href="<?php echo base_url();?>pengaturan/pengguna" class="btn btn-xs btn-warning">Kembali</a>
					
				</div>
			</div>
				
				
			</div>

		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$( "#user_nip" ).rules( "add", {
			required: true,
			remote: {
				url: base_url+'pengaturan/pengguna/check_pengguna',
				type: "post",
				data: {
					user_nip: function() {
						return $( "#user_nip" ).val();
					}
				}
			},
			messages: {
				required: "Wajib Diisi",
				remote: "Nama Pengguna Telah Digunakan"
			}
		});

	});
</script>

<SCRIPT language="javascript">
    $(document).ready(function(){
		$('input[type=checkbox]').click(function() {
		  $(this).val(1);	
		  //alert("val="+$(this).val());
		});
    });
</SCRIPT>
