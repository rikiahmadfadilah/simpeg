<style type="text/css">
.form-horizontal .checkbox .checker, .form-horizontal .checkbox-inline .checker{
	top:6px;
}
.form-horizontal .radio, .form-horizontal .checkbox, .form-horizontal .radio-inline, .form-horizontal .checkbox-inline{
	padding-top:6px;
}
</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Tambah Data Pengumuman</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Tanggal Pengumuman</label>
						<div class="col-md-9">
							<input type="text" name="pengumuman_tanggal" id="pengumuman_tanggal" class="form-control input-xs wajib pickttlstart">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Judul</label>
						<div class="col-md-9">
							<input type="text" name="pengumuman_judul" id="pengumuman_judul"  class="form-control input-xs wajib" type="text">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Isi</label>
						<div class="col-md-9">
							<textarea name="pengumuman_isi" id="pengumuman_isi" rows="10" cols="3" style="min-height: 200px;" class="form-control elastic wajib" ></textarea>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Kirim Mobile ?</label>
						<div class="col-md-9">
							<label class="checkbox-inline">
								<input type="checkbox" id="pengumuman_is_send_mobile" name="pengumuman_is_send_mobile" class="styled" value="1">
								Kirim Pengumuman dengan Push Notif Mobile
							</label>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
					<a href="<?php echo base_url();?>master/kota" class="btn btn-xs btn-warning">Kembali</a>
					
				</div>
			</div>

		</form>
	</div>
</div>
<script type="text/javascript">
	function chekcode(ini){
		
	}
	$(document).ready(function() {
		
		$('.pickttlstart').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});

		
	});
</script>
