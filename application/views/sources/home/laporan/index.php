<style>
	.input-sm {
		height: 26px;
	}
</style>
<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
	<ul class="breadcrumb">
		<li><a href='<?php echo base_url();?>'>SIMPRO MPMP</a></li>
		<li class="active">Laporan</li>
	</ul>

	<div class="visible-xs breadcrumb-toggle">
		<a class="btn btn-link btn-lg btn-icon" data-toggle="collapse" data-target=".breadcrumb-buttons"><i class="icon-menu2"></i></a>
	</div>

</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h6 class="panel-title"><i class="icon-copy"></i>Laporan</h6>
	</div>
	<form class="form-horizontal need_validation" action="" role="form" method="post" enctype="multipart/form-data" >
		<div class="panel-body">
			<div class="form-horizontal">
				<div class="form-group">
					<label for="" class="col-sm-2 control-label text-right">
						Tipe Laporan <span class="mandatory">*</span> : 
					</label>
					<div class="col-sm-10">
						<select id="tipe_laporan" class="wajib select2" name="tipe_laporan" data-placeholder="Pilih Tipe Laporan" onchange="getLaporan(this)">
							<option></option>
							<option value="1">Bill Of Quantity</option>
							<option value="2">Permintaan</option>
							<option value="3">Pengesahan</option>
							<option value="4">Purchase Order</option>
						</select> 
					</div>
				</div>
				<div class="form-group listproyek" style="display: none;">
					<label for="" class="col-sm-2 control-label text-right">
						Pilih Proyek <span class="mandatory">*</span> : 
					</label>
					<div class="col-sm-10">
						<select id="estimation_id" class="wajib select2" name="estimation_id" data-placeholder="Pilih Proyek">
							<option></option>
							<?php if(count($Estimation)>0){?>
							<?php foreach ($Estimation as $est) {?>
							<option value="<?php echo $est["estimation_id"];?>"><?php echo $est["project_name"];?> - <?php echo $est["NamaKota"];?></option>
							<?php } ?>
							<?php } ?>
						</select> 
					</div>
				</div>
				<div class="form-group listpermintaan" style="display: none;">
					<label for="" class="col-sm-2 control-label text-right">
						Pilih BPB <span class="mandatory">*</span> : 
					</label>
					<div class="col-sm-10">
						<select id="request_item_id" class="wajib select2" name="request_item_id" data-placeholder="Pilih BPB">
							<option></option>
							<?php if(count($Permintaan)>0){?>
							<?php foreach ($Permintaan as $bpb) {?>
							<option value="<?php echo $bpb["request_item_id"];?>"><?php echo $bpb["request_item_bpb_number"];?> - <?php echo $bpb["project_name"];?></option>
							<?php } ?>
							<?php } ?>
						</select> 
					</div>
				</div>
				<div class="form-group listpengesahan" style="display: none;">
					<label for="" class="col-sm-2 control-label text-right">
						Pilih BPB <span class="mandatory">*</span> : 
					</label>
					<div class="col-sm-10">
						<select id="request_item_id2" class="wajib select2" name="request_item_id2" data-placeholder="Pilih BPB">
							<option></option>
							<?php if(count($Pengesahan)>0){?>
							<?php foreach ($Pengesahan as $bpb) {?>
							<option value="<?php echo $bpb["request_item_id"];?>"><?php echo $bpb["request_item_bpb_number"];?> - <?php echo $bpb["project_name"];?></option>
							<?php } ?>
							<?php } ?>
						</select> 
					</div>
				</div>
				<div class="form-group listpo" style="display: none;">
					<label for="" class="col-sm-2 control-label text-right">
						Pilih Purchase Order <span class="mandatory">*</span> : 
					</label>
					<div class="col-sm-10">
						<select id="purchase_order_id" class="wajib select2" name="purchase_order_id" data-placeholder="Pilih Purchase Order">
							<option></option>
							<?php if(count($PO)>0){?>
							<?php foreach ($PO as $po) {?>
							<option value="<?php echo $po["purchase_order_id"];?>"><?php echo $po["purchase_order_number"];?> (<?php echo $po["request_item_bpb_number"];?> - <?php echo $po["project_name"];?>)</option>
							<?php } ?>
							<?php } ?>
						</select> 
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-2"></div>
					<div class="col-sm-10">
						<button type="button" class="btn btn-xs btn-primary" id="simpan" onclick="getReport()">Download</button>
					</div>

				</div>
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
	function getReport(){
		var form = $( ".need_validation" );
		var cek = form.valid();
		var tipe_laporan = $('#tipe_laporan').val();
		var estimation_id = $('#estimation_id').val();
		var request_item_id = $('#request_item_id').val();
		var request_item_id2 = $('#request_item_id2').val();
		var purchase_order_id = $('#purchase_order_id').val();
		if(cek){
			if(tipe_laporan==1){
				window.open(baseurl+"home/laporan/download_estimasi/" + estimation_id, "_blank");
			}else if(tipe_laporan==2){
				window.open(baseurl+"home/laporan/download_permintaan/" + request_item_id, "_blank");
			}else if(tipe_laporan==3){
				window.open(baseurl+"home/laporan/download_pengesahan/" + request_item_id2, "_blank");
			}else if(tipe_laporan==4){
				window.open(baseurl+"home/laporan/download_purchase_order/" + purchase_order_id, "_blank");
			}
		}
	}
	function getLaporan(ini) {
		var vals = $(ini).val();
		$('.listproyek').hide();
		$('.listpermintaan').hide();
		$('.listpengesahan').hide();
		$('.listpo').hide();
		
		if(vals==1){
			$('.listproyek').slideDown('fast');
		}else if(vals==2){
			$('.listpermintaan').slideDown('fast');
		}else if(vals==3){
			$('.listpengesahan').slideDown('fast');
		}else if(vals==4){
			$('.listpo').slideDown('fast');
		}
	}
	function formatnomor(){
		angkadoank();
	}
</script>