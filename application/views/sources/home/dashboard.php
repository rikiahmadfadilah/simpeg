<!-- <script type="text/javascript" src="http://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script type="text/javascript" src="http://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript" src="http://code.highcharts.com/highcharts-3d.js"></script> -->

<!-- <script src="<?php echo base_url();?>assets/js/plugins/highcharts/highcharts.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/highcharts/highcharts-more.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/highcharts/highcharts-more.js"></script> -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/highcharts/highcharts.js"></script>
<script type="text/javascript" src="http://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/highcharts/highcharts-3d.js"></script>
<!-- <script src="<?php echo base_url();?>assets/js/plugins/highcharts/es-modules/modules/exporting.src.js"></script> -->

<?php
    foreach($grafik2 as $result2){
        $GOL[] = $result2->GOL; 
        $JML[] = (float) $result2->JML; 
    }
?>

<?php
    foreach($grafik3 as $result3){
        $ESL[] = $result3->ESL; 
        $JML3[] = (float) $result3->JML; 
    }
?>

<?php
    foreach($grafik4 as $result3){
        $PENDIDIKAN[] = $result3->PENDIDIKAN; 
        $JML4[] = (float) $result3->JML; 
    }
?>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
    <div class="panel-heading">
        <h6 class="panel-title">Dashboard</h6>
        
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-6">
          <div class="grafik" style="width:100%; height:400px;"></div>
        </div>
        <div class="col-md-6">
          <div id="container"></div>
         <!--  <button id="plain">Plain</button>
          <button id="inverted">Inverted</button>
          <button id="polar">Polar</button> -->
        </div>
      </div>
      <br>
      <br>
      <div class="row">
        <div class="col-md-6">
          <div id="eselon"></div>
        </div>
        <div class="col-md-6">
          <div id="pendidikan"></div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
    $('.grafik').highcharts({
      chart: {
        type: 'pie',
        options3d: {
          enabled: true,
          alpha: 45,
          beta: 0
        },
        marginTop: 80
      },
      credits: {
        enabled: false
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      title: {
        text: 'JUMLAH JENIS KELAMIN'
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        categories: <?php echo $array_jk; ?>,
        labels: {
          style: {
            fontSize: '10px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },
      legend: {
        enabled: true
      },
      plotOptions: {
        pie: {
          size: '100%',
          allowPointSelect: true,
          cursor: 'pointer',
          depth: 35,
          dataLabels: {
            enabled: false
          },
          showInLegend: true
        }
      },
      series: <?php echo $array_jml_jk; ?>
    });

    $('#container').highcharts({
        chart: {
            type: 'column',
            margin: 75,
            options3d: {
                enabled: false,
                alpha: 10,
                beta: 25,
                depth: 70
            }
        },
        title: {
            text: '',
            style: {
                    fontSize: '18px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        subtitle: {
           text: '',
           style: {
                    fontSize: '15px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        title: {
        text: 'JUMLAH JENIS GOLONGAN'
        },
        plotOptions: {
            column: {
                depth: 25
            }
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories:  <?php echo json_encode($GOL);?>,
            labels: {
              style: {
                fontSize: '10px',
                fontFamily: 'Verdana, sans-serif'
              }
            }
        },
        exporting: { 
            enabled: true 
        },
        yAxis: {
            title: {
                text: 'Jumlah'
            },
        },
        tooltip: {
             formatter: function() {
                 return this.x + '</b>, JUMLAH = <b>' + Highcharts.numberFormat(this.y,0) + '</b>';
                 // return 'The value for <b>' + this.x + '</b> is <b>' + Highcharts.numberFormat(this.y,0) + '</b>, in '+ this.series.name;
             }
          },
        series: [{
            name: 'Data',
            data: <?php echo json_encode($JML);?>,
            shadow : true,
            dataLabels: {
                enabled: true,
                color: '#045396',
                align: 'center',
                formatter: function() {
                     return Highcharts.numberFormat(this.y, 0);
                }, // one decimal
                y: 0, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });

    $('#eselon').highcharts({
        chart: {
            type: 'column',
            margin: 75,
            options3d: {
                enabled: false,
                alpha: 10,
                beta: 25,
                depth: 70
            }
        },
        title: {
            text: '',
            style: {
                    fontSize: '18px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        subtitle: {
           text: '',
           style: {
                    fontSize: '15px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        title: {
        text: 'JUMLAH JENIS ESELON'
        },
        plotOptions: {
            column: {
                depth: 25
            }
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories:  <?php echo json_encode($ESL);?>,
            labels: {
              style: {
                fontSize: '10px',
                fontFamily: 'Verdana, sans-serif'
              }
            }
        },
        exporting: { 
            enabled: true 
        },
        yAxis: {
            title: {
                text: 'Jumlah'
            },
        },
        tooltip: {
             formatter: function() {
                 return this.x + '</b>, JUMLAH = <b>' + Highcharts.numberFormat(this.y,0) + '</b>';
                 // return 'The value for <b>' + this.x + '</b> is <b>' + Highcharts.numberFormat(this.y,0) + '</b>, in '+ this.series.name;
             }
          },
        series: [{
            name: 'Data',
            data: <?php echo json_encode($JML3);?>,
            shadow : true,
            dataLabels: {
                enabled: true,
                color: '#045396',
                align: 'center',
                formatter: function() {
                     return Highcharts.numberFormat(this.y, 0);
                }, // one decimal
                y: 0, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });

     $('#pendidikan').highcharts({
        chart: {
            type: 'column',
            margin: 75,
            options3d: {
                enabled: false,
                alpha: 10,
                beta: 25,
                depth: 70
            }
        },
        title: {
            text: '',
            style: {
                    fontSize: '18px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        subtitle: {
           text: '',
           style: {
                    fontSize: '15px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        title: {
        text: 'JUMLAH JENIS PENDIDIKAN'
        },
        plotOptions: {
            column: {
                depth: 25
            }
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories:  <?php echo json_encode($PENDIDIKAN);?>,
            labels: {
              style: {
                fontSize: '10px',
                fontFamily: 'Verdana, sans-serif'
              }
            }
        },
        exporting: { 
            enabled: true 
        },
        yAxis: {
            title: {
                text: 'Jumlah'
            },
        },
        tooltip: {
             formatter: function() {
                 return this.x + '</b>, JUMLAH = <b>' + Highcharts.numberFormat(this.y,0) + '</b>';
                 // return 'The value for <b>' + this.x + '</b> is <b>' + Highcharts.numberFormat(this.y,0) + '</b>, in '+ this.series.name;
             }
          },
        series: [{
            name: 'Data',
            data: <?php echo json_encode($JML4);?>,
            shadow : true,
            dataLabels: {
                enabled: true,
                color: '#045396',
                align: 'center',
                formatter: function() {
                     return Highcharts.numberFormat(this.y, 0);
                }, // one decimal
                y: 0, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });


  $('#plain').click(function () {
      chart.update({
          chart: {
              inverted: false,
              polar: false
          },
          subtitle: {
              text: 'Plain'
          }
      });
  });

  $('#inverted').click(function () {
      chart.update({
          chart: {
              inverted: true,
              polar: false
          },
          subtitle: {
              text: 'Inverted'
          }
      });
  });

  $('#polar').click(function () {
      chart.update({
          chart: {
              inverted: false,
              polar: true
          },
          subtitle: {
              text: 'Polar'
          }
      });
  });
     
</script>