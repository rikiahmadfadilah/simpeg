
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Data Rekapitulasi Pegawai</h6>
	</div>
	<div class="panel-body">
		<form class="need_validation form-horizontal" action="<?php echo base_url('laporan/rekap_pegawai/rekap') ?>" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-xs pegawai_unit_kerja_kode">
						<label class="col-md-3 control-label">Unit Kerja</label>
						<div class="col-md-9">
							<select name="pegawai_unit_kerja_kode" id="pegawai_unit_kerja_kode" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs wajib">
								<option value=""></option>
								<option value="0000000000">KEMENTERIAN KELAUTAN DAN PERIKANAN bukan</option>
								<?php foreach ($unit_kerja as $i) {
									echo '<option value="'.$i["unit_kerja_kode"].'" parent_id_kode="'.$i["unit_kerja_id_kode"].'" jabatan_id="'.$i["jabatan_id"].'" '.(($unit_kerja_kode==$i["unit_kerja_kode"])?"selected":"").'>'.$i["unit_kerja_nama_with_tab"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<!-- <div class="form-group form-group-xs pegawai_unit_kerja_kode">
						<label class="col-md-3 control-label">Pilih Rekapitulasi</label>
						<div class="col-md-9">
							<select name="jenis_rekapitulasi" id="jenis_rekapitulasi" data-placeholder="Pilih Rekapitulasi" class="select-size-xs">
								<option value=""></option>
								<option value="1" selected="selected">Jen.Kelamin & Golongan</option>
								<option value="2">Jen.Kelamin & Jenjang Jabatan</option>
								<option value="3">Jen.Kelamin & Status Pegawai</option>
								<option value="4">Jen.Kelamin & Pendidikan</option>
								<option value="5">Jen.Kelamin & Kelompok Umur</option>
							</select>
						</div>
					</div> -->
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3" id="tampil">
					<button type="submit" class="btn btn-xs btn-primary">Lihat</button>
				</div>
			</div>
			</br>
		</form>
	</div>
</div>