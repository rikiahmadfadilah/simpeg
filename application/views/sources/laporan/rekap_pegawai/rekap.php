
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Data Rekapitulasi Pegawai</h6>
	</div>
	<div class="panel-body">
		<form class="need_validation form-horizontal" action="<?php echo base_url('laporan/rekap_pegawai/rekap') ?>" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-xs pegawai_unit_kerja_kode">
						<label class="col-md-3 control-label">Unit Kerja</label>
						<div class="col-md-9">
							<select name="pegawai_unit_kerja_kode" id="pegawai_unit_kerja_kode" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs wajib">
								<option value=""></option>
								<option value="0000000000" <?php echo ($unit_kerja_kode =='0000000000'?'selected':'')?> >KEMENTERIAN KELAUTAN DAN PERIKANAN bukan</option>
								<?php foreach ($unit_kerja as $i) {
									echo '<option value="'.$i["unit_kerja_kode"].'" parent_id_kode="'.$i["unit_kerja_id_kode"].'" jabatan_id="'.$i["jabatan_id"].'" '.(($unit_kerja_kode==$i["unit_kerja_kode"])?"selected":"").'>'.$i["unit_kerja_nama_with_tab"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs pegawai_unit_kerja_kode">
						<label class="col-md-3 control-label">Pilih Rekapitulasi</label>
						<div class="col-md-9">
							<select name="jenis_rekapitulasi" id="jenis_rekapitulasi" data-placeholder="Pilih Rekapitulasi" class="select-size-xs">
								<option value=""></option>
								<option value="1" selected="selected">Jen.Kelamin & Golongan</option>
								<option value="2">Jen.Kelamin & Jenjang Jabatan</option>
								<option value="3">Jen.Kelamin & Status Pegawai</option>
								<option value="4">Jen.Kelamin & Pendidikan</option>
								<option value="5">Jen.Kelamin & Kelompok Umur</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3" id="tampil">
					<button type="submit" class="btn btn-xs btn-primary">Lihat</button>
				</div>
			</div>
			</br>
			<div id="jk_golongan">
                <?php 
                    $uker_esel = $rekap_jk_golongan["unit_kerja_eselon"];
                    $jml_jk_gol = $rekap_jk_golongan;
                ?>
				<legend class="text-bold" style="padding-bottom:5px;border-bottom:solid 1px #00bcd4;"></legend>
                <table class="table table-xxs datatable-basic table-bordered datatable-responsive epagawai_table" id="list_data_table">
                    <tbody>
                        <!-- <?php if($uker_esel == '1A'){echo "satu";}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_gol['e2_lk_gol_1'];}else{echo "kkp";} ?> -->
                        <tr bgcolor="#336699">
                            <td width="5%" rowspan="2" align="center"><font color="#FFFFFF"><b>NO</b></font></td>
                            <td width="20%" rowspan="2"><font color="#FFFFFF"><b>UNIT KERJA</b></font></td>
                            <td width="20%" rowspan="2"><font color="#FFFFFF"><b>JENIS KELAMIN</b></font></td>
                            <td width="40%" colspan="4">
                                <p align="center"><font color="#FFFFFF"><b>GOLONGAN</b></font></p>
                            </td>
                            <td width="15%" rowspan="2">
                                <p align="center"><font color="#FFFFFF"><b>JUMLAH</b></font></p>
                            </td>
                        </tr>
                        <tr bgcolor="#336699">
                            <td align="center" width="8%"><font color="#FFFFFF"><b>I</b></font></td>
                            <td align="center" width="8%"><font color="#FFFFFF"><b>II</b></font></td>
                            <td align="center" width="8%"><font color="#FFFFFF"><b>III</b></font></td>
                            <td align="center" width="8%"><font color="#FFFFFF"><b>IV</b></font></td>
                        </tr>
                        <tr bgcolor="#00ADD9">
                            <td width="3%" align="center"><b><a href="<?php echo base_url('laporan/jk_dan_golongan_per_ue/rekapgolpdf') ?>" target="_blank"><font color="black">1</font></a></b></td>
                            <td width="97%" colspan="7"><b><?php echo $jml_jk_gol['unit_kerja_nama']?></b></td>
                        </tr>
                        <tr>
                            <td bgcolor="#FFFFFF" colspan="2" rowspan="3">&nbsp;</td>
                            <td bgcolor="#FFFFFF">LAKI-LAKI</td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_gol['e1_lk_gol_1'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_gol['e2_lk_gol_1'];}else{echo $jml_jk_gol['kkp_lk_gol_1'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_gol['e1_lk_gol_2'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_gol['e2_lk_gol_2'];}else{echo $jml_jk_gol['kkp_lk_gol_2'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_gol['e1_lk_gol_3'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_gol['e2_lk_gol_3'];}else{echo $jml_jk_gol['kkp_lk_gol_3'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_gol['e1_lk_gol_4'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_gol['e2_lk_gol_4'];}else{echo $jml_jk_gol['kkp_lk_gol_4'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><b><?php if($uker_esel == '1A'){echo $jml_jk_gol['e1_jml_lk_gol'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_gol['e2_jml_lk_gol'];}else{echo $jml_jk_gol['kkp_jml_lk_gol'];}?><b></b></b></td>
                        </tr>
                        <tr>
                            <td bgcolor="#FFFFFF">PEREMPUAN</td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_gol['e1_pr_gol_1'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_gol['e2_pr_gol_1'];}else{echo $jml_jk_gol['kkp_pr_gol_1'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_gol['e1_pr_gol_2'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_gol['e2_pr_gol_2'];}else{echo $jml_jk_gol['kkp_pr_gol_2'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_gol['e1_pr_gol_3'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_gol['e2_pr_gol_3'];}else{echo $jml_jk_gol['kkp_pr_gol_3'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_gol['e1_pr_gol_4'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_gol['e2_pr_gol_4'];}else{echo $jml_jk_gol['kkp_pr_gol_4'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><b><?php if($uker_esel == '1A'){echo $jml_jk_gol['e1_jml_pr_gol'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_gol['e2_jml_pr_gol'];}else{echo $jml_jk_gol['kkp_jml_pr_gol'];}?><b></b></b></td>
                        </tr>
                        <tr>
                            <td bgcolor="#FFFFFF">JUMLAH</td>
                            <td bgcolor="#FFFFFF" align="center"><b><?php if($uker_esel == '1A'){echo $jml_jk_gol['e1_jml_gol_1'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_gol['e2_jml_gol_1'];}else{echo $jml_jk_gol['kkp_jml_gol_1'];}?><b></b></b></td>
                            <td bgcolor="#FFFFFF" align="center"><b><?php if($uker_esel == '1A'){echo $jml_jk_gol['e1_jml_gol_2'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_gol['e2_jml_gol_2'];}else{echo $jml_jk_gol['kkp_jml_gol_2'];}?><b></b></b></td>
                            <td bgcolor="#FFFFFF" align="center"><b><?php if($uker_esel == '1A'){echo $jml_jk_gol['e1_jml_gol_3'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_gol['e2_jml_gol_3'];}else{echo $jml_jk_gol['kkp_jml_gol_3'];}?><b></b></b></td>
                            <td bgcolor="#FFFFFF" align="center"><b><?php if($uker_esel == '1A'){echo $jml_jk_gol['e1_jml_gol_4'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_gol['e2_jml_gol_4'];}else{echo $jml_jk_gol['kkp_jml_gol_4'];}?><b></b></b></td>
                            <td align="center" bgcolor="#336699"><b><font color="#FFFFFF"><?php if($uker_esel == '1A'){echo $jml_jk_gol['e1_jml_gol'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_gol['e2_jml_gol'];}else{echo $jml_jk_gol['kkp_jml_gol'];}?><b></b></font></b></td>
                        </tr>
                        <input type="hidden" name="kode" id="kode" value="<?php echo substr($unit_kerja_kode, 2, 10); ?>">
                	</tbody>
            	</table>
			</div>
			<div id="jk_jabatan" style="display: none;">
				<?php 
                    $uker_esel = $rekap_jk_golongan["unit_kerja_eselon"];
                    $jml_jk_jab = $rekap_jk_jabatan;
                ?>
                <legend class="text-bold" style="padding-bottom:5px;border-bottom:solid 1px #00bcd4;"></legend>
				<table class="table table-xxs datatable-basic table-bordered datatable-responsive epagawai_table" id="list_data_table">
                    <tbody>
                        <tr bgcolor="#52527A">
                            <td rowspan="2" width="5%" align="center"><font color="#FFFFFF"><b>NO</b></font></td>
                            <td rowspan="2" width="5%"><font color="#FFFFFF"><b>UNIT KERJA</b></font></td>
                            <td rowspan="2" width="10%"><font color="#FFFFFF"><b>JENIS KELAMIN</b></font></td>
                            <td colspan="9" align="center" width="55%"><font color="#FFFFFF"><b>JABATAN ESELON</b></font></td>
                            <td align="center" width="10%" rowspan="2"><font color="#FFFFFF"><b>JAB.<br>FUNG</b></font></td>
                            <td align="center" width="8%" rowspan="2"><font color="#FFFFFF"><b>PELAK-<br>SANA</b></font></td>
                            <td rowspan="2" width="7%"><font color="#FFFFFF"><b>JUMLAH</b></font></td>
                        </tr>
                        <tr bgcolor="#52527A">
                            <td width="6%" align="center"><font color="#FFFFFF"><b>I.a</b></font></td>
                            <td width="6%" align="center"><font color="#FFFFFF"><b>I.b</b></font></td>
                            <td width="6%" align="center"><font color="#FFFFFF"><b>II.a</b></font></td>
                            <td width="6%" align="center"><font color="#FFFFFF"><b>II.b</b></font></td>
                            <td width="6%" align="center"><font color="#FFFFFF"><b>III.a</b></font></td>
                            <td width="6%" align="center"><font color="#FFFFFF"><b>III.b</b></font></td>
                            <td width="6%" align="center"><font color="#FFFFFF"><b>IV.a</b></font></td>
                            <td width="6%" align="center"><font color="#FFFFFF"><b>IV.b</b></font></td>
                            <td width="6%" align="center"><font color="#FFFFFF"><b>&nbsp;V&nbsp;</b></font></td>
                        </tr>
                        <tr bgcolor="#AAAAC6">
                            <td align="center"><b>1</b></td>
                            <td colspan="14"><b><?php echo $jml_jk_jab['unit_kerja_nama']?></b></td>
                        </tr>
                        <tr>
                            <td colspan="2" rowspan="3" bgcolor="#FFFFFF">&nbsp;</td>
                            <td bgcolor="#FFFFFF">LAKI-LAKI</td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_lk_1a'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_lk_1a'];}else{echo $jml_jk_jab['kkp_lk_1a'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_lk_1b'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_lk_1b'];}else{echo $jml_jk_jab['kkp_lk_1b'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_lk_2a'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_lk_2a'];}else{echo $jml_jk_jab['kkp_lk_2a'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_lk_2b'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_lk_2b'];}else{echo $jml_jk_jab['kkp_lk_2b'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_lk_3a'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_lk_3a'];}else{echo $jml_jk_jab['kkp_lk_3a'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_lk_3b'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_lk_3b'];}else{echo $jml_jk_jab['kkp_lk_3b'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_lk_4a'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_lk_4a'];}else{echo $jml_jk_jab['kkp_lk_4a'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_lk_4b'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_lk_4b'];}else{echo $jml_jk_jab['kkp_lk_4b'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_lk_v'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_lk_v'];}else{echo $jml_jk_jab['kkp_lk_v'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_lk_jabfunc'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_lk_jabfunc'];}else{echo $jml_jk_jab['kkp_lk_jabfunc'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_lk_pelaksana'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_lk_pelaksana'];}else{echo $jml_jk_jab['kkp_lk_pelaksana'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><b><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_jml_lk'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_jml_lk'];}else{echo $jml_jk_jab['kkp_jml_lk'];}?><b></b></b></td>
                        </tr>
                        <tr>
                            <td bgcolor="#FFFFFF">PEREMPUAN</td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_pr_1a'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_pr_1a'];}else{echo $jml_jk_jab['kkp_pr_1a'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_pr_1b'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_pr_1b'];}else{echo $jml_jk_jab['kkp_pr_1b'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_pr_2a'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_pr_2a'];}else{echo $jml_jk_jab['kkp_pr_2a'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_pr_2b'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_pr_2b'];}else{echo $jml_jk_jab['kkp_pr_2b'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_pr_3a'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_pr_3a'];}else{echo $jml_jk_jab['kkp_pr_3a'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_pr_3b'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_pr_3b'];}else{echo $jml_jk_jab['kkp_pr_3b'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_pr_4a'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_pr_4a'];}else{echo $jml_jk_jab['kkp_pr_4a'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_pr_4b'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_pr_4b'];}else{echo $jml_jk_jab['kkp_pr_4b'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_pr_v'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_pr_v'];}else{echo $jml_jk_jab['kkp_pr_v'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_pr_jabfunc'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_pr_jabfunc'];}else{echo $jml_jk_jab['kkp_pr_jabfunc'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_pr_pelaksana'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_pr_pelaksana'];}else{echo $jml_jk_jab['kkp_pr_pelaksana'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><b><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_jml_pr'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_jml_pr'];}else{echo $jml_jk_jab['kkp_jml_pr'];}?><b></b></b></td>
                        </tr>
                        <tr>
                            <td bgcolor="#FFFFFF">JUMLAH</td>
                            <td bgcolor="#FFFFFF" align="center"><b><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_jml_1a'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_jml_1a'];}else{echo $jml_jk_jab['kkp_jml_1a'];}?><b></b></b></td>
                            <td bgcolor="#FFFFFF" align="center"><b><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_jml_1b'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_jml_1b'];}else{echo $jml_jk_jab['kkp_jml_1b'];}?><b></b></b></td>
                            <td bgcolor="#FFFFFF" align="center"><b><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_jml_2a'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_jml_2a'];}else{echo $jml_jk_jab['kkp_jml_2a'];}?><b></b></b></td>
                            <td bgcolor="#FFFFFF" align="center"><b><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_jml_2b'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_jml_2b'];}else{echo $jml_jk_jab['kkp_jml_2b'];}?><b></b></b></td>
                            <td bgcolor="#FFFFFF" align="center"><b><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_jml_3a'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_jml_3a'];}else{echo $jml_jk_jab['kkp_jml_3a'];}?><b></b></b></td>
                            <td bgcolor="#FFFFFF" align="center"><b><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_jml_3b'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_jml_3b'];}else{echo $jml_jk_jab['kkp_jml_3b'];}?><b></b></b></td>
                            <td bgcolor="#FFFFFF" align="center"><b><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_jml_4a'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_jml_4a'];}else{echo $jml_jk_jab['kkp_jml_4a'];}?><b></b></b></td>
                            <td bgcolor="#FFFFFF" align="center"><b><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_jml_4b'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_jml_4b'];}else{echo $jml_jk_jab['kkp_jml_4b'];}?><b></b></b></td>
                            <td bgcolor="#FFFFFF" align="center"><b><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_jml_v'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_jml_v'];}else{echo $jml_jk_jab['kkp_jml_v'];}?><b></b></b></td>
                            <td bgcolor="#FFFFFF" align="center"><b><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_jml_jabfunc'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_jml_jabfunc'];}else{echo $jml_jk_jab['kkp_jml_jabfunc'];}?><b></b></b></td>
                            <td bgcolor="#FFFFFF" align="center"><b><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_jml_pelaksana'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_jml_pelaksana'];}else{echo $jml_jk_jab['kkp_jml_pelaksana'];}?><b></b></b></td>
                            <td bgcolor="#52527A" align="center"><b><font color="#FFFFFF"><?php if($uker_esel == '1A'){echo $jml_jk_jab['e1_jml'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_jab['e2_jml'];}else{echo $jml_jk_jab['kkp_jml'];}?><b></b></font><b></b></b></td>
                        </tr>
                    </tbody>
            	</table>
			</div>
			<div id="jk_status" style="display: none;">
                <?php 
                    $uker_esel = $rekap_jk_golongan["unit_kerja_eselon"];
                    $jml_jk_stat = $rekap_jk_status;
                ?>
				<legend class="text-bold" style="padding-bottom:5px;border-bottom:solid 1px #00bcd4;"></legend>
				<table class="table table-xxs datatable-basic table-bordered datatable-responsive epagawai_table" id="list_data_table">
                    <tbody>
	                    <tr bgcolor="#993300">
	                        <td width="5%" rowspan="2" align="center"><font color="#FFFFFF"><b>NO</b></font></td>
	                        <td width="7%" rowspan="2"><font color="#FFFFFF"><b>UNIT KERJA</b></font></td>
	                        <td width="13%" rowspan="2"><font color="#FFFFFF"><b>JENIS KELAMIN</b></font></td>
	                        <td width="65%" colspan="4"><p align="center"><font color="#FFFFFF"><b>STATUS</b></font></p></td>
	                        <td width="10%" rowspan="2"><p align="center"><font color="#FFFFFF"><b>JUMLAH</b></font></p></td>
	                    </tr>
                        <tr bgcolor="#993300">
                            <td align="center" width="14%"><font color="#FFFFFF"><b>CPNS</b></font></td>
                            <td align="center" width="14%"><font color="#FFFFFF"><b>PNS</b></font></td>
                            <td align="center" width="18%"><font color="#FFFFFF"><b>PNS DKP/DPB DARI DEP. LAIN</b></font></td>
                            <td align="center" width="19%"><font color="#FFFFFF"><b>PNS DKP/DPB KE DEP. LAIN</b></font></td>
                        </tr>
                        <tr bgcolor="#FFAC59">
                            <td width="3%" align="center"><b>1</b></td>
                            <td width="97%" colspan="7"><b><?php echo $jml_jk_stat['unit_kerja_nama']?></b></td>
                        </tr>
                        <tr>
                            <td bgcolor="#FFFFFF" colspan="2" rowspan="3">&nbsp;</td>
                            <td bgcolor="#FFFFFF">LAKI-LAKI</td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_stat['e1_lk_cpns'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_stat['e2_lk_cpns'];}else{echo $jml_jk_stat['kkp_lk_cpns'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_stat['e1_lk_pns'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_stat['e2_lk_pns'];}else{echo $jml_jk_stat['kkp_lk_pns'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_stat['e1_lk_dari_lain'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_stat['e2_lk_dari_lain'];}else{echo $jml_jk_stat['kkp_lk_dari_lain'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_stat['e1_lk_ke_lain'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_stat['e2_lk_ke_lain'];}else{echo $jml_jk_stat['kkp_lk_ke_lain'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_stat['e1_lk_jml'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_stat['e2_lk_jml'];}else{echo $jml_jk_stat['kkp_lk_jml'];}?></td>
                        </tr>
                        <tr>
                            <td bgcolor="#FFFFFF">PEREMPUAN</td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_stat['e1_pr_cpns'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_stat['e2_pr_cpns'];}else{echo $jml_jk_stat['kkp_pr_cpns'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_stat['e1_pr_pns'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_stat['e2_pr_pns'];}else{echo $jml_jk_stat['kkp_pr_pns'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_stat['e1_pr_dari_lain'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_stat['e2_pr_dari_lain'];}else{echo $jml_jk_stat['kkp_pr_dari_lain'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_stat['e1_pr_ke_lain'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_stat['e2_pr_ke_lain'];}else{echo $jml_jk_stat['kkp_pr_ke_lain'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_stat['e1_pr_jml'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_stat['e2_pr_jml'];}else{echo $jml_jk_stat['kkp_pr_jml'];}?></td>
                        </tr>
                        <tr>
                            <td bgcolor="#FFFFFF">JUMLAH</td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_stat['e1_jml_cpns'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_stat['e2_jml_cpns'];}else{echo $jml_jk_stat['kkp_jml_cpns'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_stat['e1_jml_pns'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_stat['e2_jml_pns'];}else{echo $jml_jk_stat['kkp_jml_pns'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_stat['e1_jml_dari_lain'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_stat['e2_jml_dari_lain'];}else{echo $jml_jk_stat['kkp_jml_dari_lain'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_stat['e1_jml_ke_lain'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_stat['e2_jml_ke_lain'];}else{echo $jml_jk_stat['kkp_jml_ke_lain'];}?></td>
                            <td align="center" bgcolor="#993300"><b><font color="#FFFFFF"><?php if($uker_esel == '1A'){echo $jml_jk_stat['e1_jml'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_stat['e2_jml'];}else{echo $jml_jk_stat['kkp_jml'];}?><b></b></font><b></b></b></td>
                        </tr>
                    </tbody>
                </table>
			</div>
			<div id="jk_pendidikan" style="display: none;">
                <?php 
                    $uker_esel = $rekap_jk_golongan["unit_kerja_eselon"];
                    $jml_jk_pen = $rekap_jk_pendidikan;
                ?>
				<legend class="text-bold" style="padding-bottom:5px;border-bottom:solid 1px #00bcd4;"></legend>
				<table class="table table-xxs datatable-basic table-bordered datatable-responsive epagawai_table" id="list_data_table">
                    <tbody>
                        <tr>
                            <td bgcolor="#0099CC" rowspan="2" width="5%" align="center"><b>NO</b></td>
                            <td bgcolor="#0099CC" rowspan="2" width="5%"><b>UNIT KERJA</b></td>
                            <td bgcolor="#0099CC" rowspan="2" width="5%"><b>JENIS KELAMIN</b></td>
                            <td bgcolor="#0099CC" colspan="11" align="center" width="77%"><b>PENDIDIKAN</b></td>
                            <td bgcolor="#0099CC" width="8%" rowspan="2"><b>JUMLAH</b></td>
                        </tr>
                        <tr>
                            <td bgcolor="#0099CC" width="7%" align="center"><b>S3</b></td>
                            <td bgcolor="#0099CC" width="7%" align="center"><b>S2</b></td>
                            <td bgcolor="#0099CC" width="7%" align="center"><b>S1</b></td>
                            <td bgcolor="#0099CC" width="7%" align="center"><b>D4</b></td>
                            <td bgcolor="#0099CC" width="7%" align="center"><b>SM</b></td>
                            <td bgcolor="#0099CC" width="7%" align="center"><b>D3</b></td>
                            <td bgcolor="#0099CC" width="7%" align="center"><b>D2</b></td>
                            <td bgcolor="#0099CC" width="7%" align="center"><b>D1</b></td>
                            <td bgcolor="#0099CC" width="7%" align="center"><b>SLTA</b></td>
                            <td bgcolor="#0099CC" width="7%" align="center"><b>SLTP</b></td>
                            <td bgcolor="#0099CC" width="7%" align="center"><b>SD</b></td>
                        </tr>
                        <tr bgcolor="#0093B7">
                            <td>
                                <b><a href="rekappendidikanpdf.php" target="_blank"><font color="black">1</font></a></b>
                            </td>
                            <td colspan="14">
                                <b><?php echo $jml_jk_pen['unit_kerja_nama']?></b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" rowspan="3" bgcolor="#FFFFFF">&nbsp;</td>
                            <td bgcolor="#FFFFFF">LAKI-LAKI</td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_lk_s3'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_lk_s3'];}else{echo $jml_jk_pen['kkp_lk_s3'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_lk_s2'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_lk_s2'];}else{echo $jml_jk_pen['kkp_lk_s2'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_lk_s1'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_lk_s1'];}else{echo $jml_jk_pen['kkp_lk_s1'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_lk_d4'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_lk_d4'];}else{echo $jml_jk_pen['kkp_lk_d4'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_lk_sm'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_lk_sm'];}else{echo $jml_jk_pen['kkp_lk_sm'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_lk_d3'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_lk_d3'];}else{echo $jml_jk_pen['kkp_lk_d3'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_lk_d2'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_lk_d2'];}else{echo $jml_jk_pen['kkp_lk_d2'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_lk_d1'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_lk_d1'];}else{echo $jml_jk_pen['kkp_lk_d1'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_lk_slta'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_lk_slta'];}else{echo $jml_jk_pen['kkp_lk_slta'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_lk_sltp'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_lk_sltp'];}else{echo $jml_jk_pen['kkp_lk_sltp'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_lk_sd'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_lk_sd'];}else{echo $jml_jk_pen['kkp_lk_sd'];}?></td>
                            <td bgcolor="#FFFFFF" align="center">
                                <b><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_lk_jml'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_lk_jml'];}else{echo $jml_jk_pen['kkp_lk_jml'];}?></b>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#FFFFFF">PEREMPUAN</td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_pr_s3'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_pr_s3'];}else{echo $jml_jk_pen['kkp_pr_s3'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_pr_s2'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_pr_s2'];}else{echo $jml_jk_pen['kkp_pr_s2'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_pr_s1'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_pr_s1'];}else{echo $jml_jk_pen['kkp_pr_s1'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_pr_d4'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_pr_d4'];}else{echo $jml_jk_pen['kkp_pr_d4'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_pr_sm'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_pr_sm'];}else{echo $jml_jk_pen['kkp_pr_sm'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_pr_d3'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_pr_d3'];}else{echo $jml_jk_pen['kkp_pr_d3'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_pr_d2'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_pr_d2'];}else{echo $jml_jk_pen['kkp_pr_d2'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_pr_d1'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_pr_d1'];}else{echo $jml_jk_pen['kkp_pr_d1'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_pr_slta'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_pr_slta'];}else{echo $jml_jk_pen['kkp_pr_slta'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_pr_sltp'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_pr_sltp'];}else{echo $jml_jk_pen['kkp_pr_sltp'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_pr_sd'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_pr_sd'];}else{echo $jml_jk_pen['kkp_pr_sd'];}?></td>
                            <td bgcolor="#FFFFFF" align="center">
                                <b><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_pr_jml'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_pr_jml'];}else{echo $jml_jk_pen['kkp_pr_jml'];}?></b>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#FFFFFF">JUMLAH</td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_jml_s3'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_jml_s3'];}else{echo $jml_jk_pen['kkp_jml_s3'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_jml_s2'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_jml_s2'];}else{echo $jml_jk_pen['kkp_jml_s2'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_jml_s1'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_jml_s1'];}else{echo $jml_jk_pen['kkp_jml_s1'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_jml_d4'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_jml_d4'];}else{echo $jml_jk_pen['kkp_jml_d4'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_jml_sm'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_jml_sm'];}else{echo $jml_jk_pen['kkp_jml_sm'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_jml_d3'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_jml_d3'];}else{echo $jml_jk_pen['kkp_jml_d3'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_jml_d2'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_jml_d2'];}else{echo $jml_jk_pen['kkp_jml_d2'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_jml_d1'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_jml_d1'];}else{echo $jml_jk_pen['kkp_jml_d1'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_jml_slta'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_jml_slta'];}else{echo $jml_jk_pen['kkp_jml_slta'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_jml_sltp'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_jml_sltp'];}else{echo $jml_jk_pen['kkp_jml_sltp'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_jml_sd'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_jml_sd'];}else{echo $jml_jk_pen['kkp_jml_sd'];}?></td>
                            <td bgcolor="#FFFFFF" align="center">
                                <b><?php if($uker_esel == '1A'){echo $jml_jk_pen['e1_jml'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_pen['e2_jml'];}else{echo $jml_jk_pen['kkp_jml'];}?></b>
                            </td>
                        </tr>
                    </tbody>
                </table>
			</div>
			<div id="jk_umur" style="display: none;">
                <?php 
                    $uker_esel = $rekap_jk_golongan["unit_kerja_eselon"];
                    $jml_jk_umur = $rekap_jk_umur;
                ?>
				<legend class="text-bold" style="padding-bottom:5px;border-bottom:solid 1px #00bcd4;"></legend>
				<table class="table table-xxs datatable-basic table-bordered datatable-responsive epagawai_table" id="list_data_table">
                    <tbody>
                        <tr bgcolor="#006200">
                            <td width="5%" rowspan="2" align="center"><font color="#FFFFFF"><b>NO</b></font></td>
                            <td width="20%" rowspan="2"><font color="#FFFFFF"><b>UNIT KERJA</b></font></td>
                            <td width="20%" rowspan="2"><font color="#FFFFFF"><b>JENIS KELAMIN</b></font></td>
                            <td width="40%" colspan="5"><p align="center"><font color="#FFFFFF"><b>USIA</b></font></p></td>
                            <td width="15%" rowspan="2"><p align="center"><font color="#FFFFFF"><b>JUMLAH</b></font></p></td>
                        </tr>
                        <tr bgcolor="#006200">
                            <td align="center" width="8%"><font color="#FFFFFF"><b>&gt;56</b></font></td>
                            <td align="center" width="8%"><font color="#FFFFFF"><b>46-55</b></font></td>
                            <td align="center" width="8%"><font color="#FFFFFF"><b>36-45</b></font></td>
                            <td align="center" width="8%"><font color="#FFFFFF"><b>26-35</b></font></td>
                            <td align="center" width="8%"><font color="#FFFFFF"><b>&lt;25</b></font></td>
                        </tr>
                        <tr bgcolor="#9BFF9B">
                            <td width="3%" align="center">
                                <b>1</b>
                            </td>
                            <td width="97%" colspan="8">
                                <b><?php echo $jml_jk_umur['unit_kerja_nama']?></b>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#FFFFFF" colspan="2" rowspan="3">&nbsp;</td>
                            <td bgcolor="#FFFFFF">LAKI-LAKI</td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_umur['e1_lk_leb_55'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_umur['e2_lk_leb_55'];}else{echo $jml_jk_umur['kkp_lk_leb_55'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_umur['e1_lk_45_55'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_umur['e2_lk_45_55'];}else{echo $jml_jk_umur['kkp_lk_45_55'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_umur['e1_lk_35_45'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_umur['e2_lk_35_45'];}else{echo $jml_jk_umur['kkp_lk_35_45'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_umur['e1_lk_25_35'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_umur['e2_lk_25_35'];}else{echo $jml_jk_umur['kkp_lk_25_35'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_umur['e1_lk_kur_25'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_umur['e2_lk_kur_25'];}else{echo $jml_jk_umur['kkp_lk_kur_25'];}?></td>
                            <td bgcolor="#FFFFFF" align="center">
                                <b><?php if($uker_esel == '1A'){echo $jml_jk_umur['e1_lk_jml'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_umur['e2_lk_jml'];}else{echo $jml_jk_umur['kkp_lk_jml'];}?><b></b></b>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#FFFFFF">PEREMPUAN</td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_umur['e1_pr_leb_55'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_umur['e2_pr_leb_55'];}else{echo $jml_jk_umur['kkp_pr_leb_55'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_umur['e1_pr_45_55'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_umur['e2_pr_45_55'];}else{echo $jml_jk_umur['kkp_pr_45_55'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_umur['e1_pr_35_45'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_umur['e2_pr_35_45'];}else{echo $jml_jk_umur['kkp_pr_35_45'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_umur['e1_pr_25_35'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_umur['e2_pr_25_35'];}else{echo $jml_jk_umur['kkp_pr_25_35'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_umur['e1_pr_kur_25'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_umur['e2_pr_kur_25'];}else{echo $jml_jk_umur['kkp_pr_kur_25'];}?></td>
                            <td bgcolor="#FFFFFF" align="center">
                                <b><?php if($uker_esel == '1A'){echo $jml_jk_umur['e1_pr_jml'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_umur['e2_pr_jml'];}else{echo $jml_jk_umur['kkp_pr_jml'];}?><b></b></b>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#FFFFFF">JUMLAH</td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_umur['e1_jml_leb_55'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_umur['e2_jml_leb_55'];}else{echo $jml_jk_umur['kkp_jml_leb_55'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_umur['e1_jml_45_55'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_umur['e2_jml_45_55'];}else{echo $jml_jk_umur['kkp_jml_45_55'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_umur['e1_jml_35_45'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_umur['e2_jml_35_45'];}else{echo $jml_jk_umur['kkp_jml_35_45'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_umur['e1_jml_25_35'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_umur['e2_jml_25_35'];}else{echo $jml_jk_umur['kkp_jml_25_35'];}?></td>
                            <td bgcolor="#FFFFFF" align="center"><?php if($uker_esel == '1A'){echo $jml_jk_umur['e1_jml_kur_25'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_umur['e2_jml_kur_25'];}else{echo $jml_jk_umur['kkp_jml_kur_25'];}?></td>
                            <td bgcolor="#FFFFFF" align="center">
                                <b><?php if($uker_esel == '1A'){echo $jml_jk_umur['e1_jml'];}else if($uker_esel == '2A' OR $uker_esel == '2B'){echo $jml_jk_umur['e2_jml'];}else{echo $jml_jk_umur['kkp_jml'];}?><b></b></b>
                            </td>
                        </tr>
                    </tbody>
                </table>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$('#jenis_rekapitulasi').change(function(){
		var jr = $('option:selected', this).val();
		// alert(jr);
		if(jr == 1){
			$('#jk_golongan').show();
			$('#jk_jabatan').hide();
			$('#jk_status').hide();
			$('#jk_pendidikan').hide();
			$('#jk_umur').hide();
		}else if(jr == 2){
			$('#jk_jabatan').show();
			$('#jk_golongan').hide();
			$('#jk_status').hide();
			$('#jk_pendidikan').hide();
			$('#jk_umur').hide();
		}else if(jr == 3){
			$('#jk_status').show();
			$('#jk_golongan').hide();
			$('#jk_jabatan').hide();
			$('#jk_pendidikan').hide();
			$('#jk_umur').hide();
		}else if(jr == 4){
			$('#jk_pendidikan').show();
			$('#jk_golongan').hide();
			$('#jk_jabatan').hide();
			$('#jk_status').hide();
			$('#jk_umur').hide();
		}else if(jr == 5){
			$('#jk_umur').show();
			$('#jk_golongan').hide();
			$('#jk_jabatan').hide();
			$('#jk_status').hide();
			$('#jk_pendidikan').hide();
		}else{
			$('#jk_jabatan').hide();
			$('#jk_golongan').hide();
			$('#jk_pendidikan').hide();
			$('#jk_status').hide();
			$('#jk_umur').hide();
		}
	});
    var kode = $('#kode').val();
    var kode_kkp = $('#pegawai_unit_kerja_kode').val();
    // alert(kode);
    if(kode == '00000000'){
        $('#jumlah_jk_golongan_uker').show();
        $('#jumlah_jk_golongan_semua').hide();
    }else{}
    if(kode_kkp == '0000000000'){
        $('#jumlah_jk_golongan_semua').show();
        $('#jumlah_jk_jabatan_semua').show();
        $('#jumlah_jk_status_semua').show();
        $('#jumlah_jk_golongan_uker').hide();
    }else{}
</script>