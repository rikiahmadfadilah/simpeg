
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Daftar Pegawai Cuti</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Unit Kerja</label>
						<div class="col-md-7">
							<select name="unker1" id="unker1" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs">
								<option value=""></option>
								<?php foreach ($unker as $i) {
									echo '<option value="'.$i["kode_unker"].'">'.$i["nama_unker"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Unit Kerja</label>
						<div class="col-md-7">
							<select name="unker2" id="unker2" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs">
								<option value=""></option>
								<?php foreach ($unker as $i) {
									echo '<option value="'.$i["kode_unker"].'">'.$i["nama_unker"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">NIP/Nama</label>
						<div class="col-md-7">
							<input type="tex" name="nip_nama" id="nip_nama" class="form-control input-xs">
						</div>
					</div>	
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Tahun</label>
						<div class="col-md-3">
						<select name="tahun" id="tahun" data-placeholder="Tahun" data-id="0" class="select-size-xs">
							<option value=""></option>
							<?php
								$thn = date('Y');
								for($i=$thn;$i>=2000;$i--) {
									echo '<option value="'.$i.'">'.$i.'</option>';
								}
							?>
						</select>
						</div>
					</div>				
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3" id="tampil">
					<a href="javascript:void(0);" class="btn btn-xs btn-primary">Lihat</a>
                    <input id="filter" name="filter" type="hidden" min="1900" max="2050" class="form-control input-xs wajib">
				</div>
			</div>
			</br>

			<div id="list_data" style="display: none;">
				<legend class="text-bold" style="padding-bottom:5px;border-bottom:solid 1px #00bcd4;"></legend>
				<table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive ropeg_table" id="">
					<thead>
						<tr>
                            <td colspan="9" valign="middle" align="center">
                            <b><font size="3">
                            DAFTAR PEGAWAI CUTI<br>KEMENKO
                            KEMARITIMAN</font></b></td>
                        </tr>
                        <!-- <tr>
                            <td align="left" colspan="13"><b>SEKRETARIAT JENDERAL</b></td>
                        </tr> -->
                	</thead>
                </table>
                <div class="center" align="right">
                    <a href="javascript:read_preview()" class="btn bg-slate btn-raised legitRipple">PRINT PREVIEW</a>
                </div>
                <table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive ropeg_table" id="list_data_table">
					<thead>
                        <tr>
                            <td>
                            	<b>NO.</b>
                            </td>
                            <td width="23%">
                            	<p align="center">
                            		<b>NIP/Nama</b>
                            	</p>
                            </td>
                            <td width="8%" valign="top">
                            	<p align="center">
                            		<b>Cuti Tahunan</b>
                            	</p>
                            </td>
                            <td width="7%" valign="top">
                            	<p align="center">
                            		<b>Cuti Besar</b>
                            	</p>
                            </td>
                            <td width="10%" align="center" valign="top">
                            	<b>Cuti Sakit</b>
                            </td>
                            <td width="10%" valign="top">
                            	<p align="center"><b>Cuti Bersalin</b>
                            	</p>
                            </td>
                            <td width="10%" valign="top">
                            	<p align="center">
                            		<b>Cuti Alasan Penting</b>
                            </td>
                            <td width="10%" valign="top">
                            	<p align="center">
                            		<b>Total</b>
                            	</p>
                            </td>
                        </tr>
					</thead>
					
					
                </table>
			</div>
			
			
		</form>
		
		
	</div>
</div>

<script type="text/javascript">
	$('#tampil').click(function(){
    	$("#list_data").slideDown('fast');
        
		var unker1    = $('#unker1 option:selected').attr('value');
		var unker2    = $('#unker2 option:selected').attr('value');
		var nip_nama    = $('#nip_nama').attr('value');
		var tahun    = $('#tahun option:selected').attr('value');
        
	    $('#list_data_table').dataTable( {
            "processing": true,
            "serverSide": true,
            "bServerSide": true,
            "ordering": true,
            "destroy": true,
            "sAjaxSource": base_url+"laporan/rekap_cuti/list_data?unker1="+unker1+"&unker2="+unker2+"&nip_nama="+nip_nama+"&tahun="+tahun,
            "aaSorting": [],
            "order": [],
            "iDisplayLength": 10,
            "aoColumns": [
            { "bSortable": false, "sClass": "text-center" },
            { "bSortable": false, "sClass": "text-left" },
            { "bSortable": false, "sClass": "text-left" },
            { "bSortable": false, "sClass": "text-left" },
            { "bSortable": false, "sClass": "text-left" },
            { "bSortable": false, "sClass": "text-left" },
            { "bSortable": false, "sClass": "text-center" },
            { "bSortable": false, "sClass": "text-center" },
            ],
            "fnDrawCallback": function () {
                set_default_datatable();
            },
        });
	});

</script>
<script type="text/javascript"> 
	function read_preview(){
		var unker1    = $('#unker1 option:selected').attr('value');
		var unker2    = $('#unker2 option:selected').attr('value');
		var nip_nama    = $('#nip_nama').attr('value');
		var tahun    = $('#tahun option:selected').attr('value');
		
        var win = window.open("../laporan/rekap_cuti/preview?unker1="+unker1+"&unker2="+unker2+"&nip_nama="+nip_nama+"&tahun="+tahun, '_blank');
        win.focus();
    }
</script>
