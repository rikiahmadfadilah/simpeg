<style type="text/css">
    table.ropeg_table thead tr {
        background-color: #ebecff;
    }
    table.ropeg_table th, table.ropeg_table td {
      font-size : 12px;
    }
    table.ropeg_table td {
     vertical-align: top !important;
    }
    table.ropeg_table tfoot tr{
      background-color: #ebecff;

    }
    table.ropeg_table{
     margin-bottom: 10px;
    }
    .btn-tfoot-table{
      padding-top: 2px;
      padding-bottom: 2px;
    }
</style>
    <div align="center">
    <b>
        <font size="3">
            REKAPITULASI CUTI
            <br>
            KEMENKO KEMARITIMAN
            <br>
            PERIODE <?php echo date('d-m-Y'); ?>
        </font>
    </b>
    </div>
    <br>
    <table class="ropeg_table" border="1" id="" width="100%">
	<thead>
        <tr>
            <td width="1%">
                <b>NO.</b>
            </td>
            <td width="20%">
                <p align="center">
                    <b>NIP/Nama</b>
                </p>
            </td>
            <td width="4%" valign="top">
                <p align="center">
                    <b>Cuti Tahunan</b>
                </p>
            </td>
            <td width="4%" valign="top">
                <p align="center">
                    <b>Cuti Besar</b>
                </p>
            </td>
            <td width="4%" align="center" valign="top">
                <b>Cuti Sakit</b>
            </td>
            <td width="4%" valign="top">
                <p align="center"><b>Cuti Bersalin</b>
                </p>
            </td>
            <td width="4%" valign="top">
                <p align="center">
                    <b>Cuti Alasan Penting</b>
            </td>
            <td width="4%" valign="top">
                <p align="center">
                    <b>Total</b>
                </p>
            </td>
        </tr>
	</thead>
    <tbody>
        <?php $no = 1; foreach ($preview as $key => $p) { ?>
            <?php $masa_kerja_golongan = 2; ?>
        <tr>
            <td align="center"><?php echo $no; ?></td>
            <td><?php echo $p['NIP'] ?>/<?php echo $p['NAMA'] ?></td>
            <td><?php echo count_cuti_tahunan($p['ID'],$tahun) ?></td>
            <td><?php echo count_cuti_besar($p['ID'],$tahun) ?></td>
            <td><?php echo count_cuti_sakit($p['ID'],$tahun) ?></td>
            <td><?php echo count_cuti_ap($p['ID'],$tahun) ?></td>
            <td><?php echo count_cuti_bersalin($p['ID'],$tahun) ?></td>
            <td><?php echo count_cuti_all($p['ID'],$tahun) ?></td>
        </tr>
        <?php $no++; } ?>
    </tbody>
	
	
</table>