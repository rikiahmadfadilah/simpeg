<!DOCTYPE html>
<html>
<style type="text/css">
    table.ropeg_table thead tr {
        background-color: #ebecff;
    }
    table.ropeg_table th, table.ropeg_table td {
      font-size : 12px;
    }
    table.ropeg_table td {
     vertical-align: top !important;
    }
    table.ropeg_table tfoot tr{
      background-color: #ebecff;

    }
    table.ropeg_table{
     margin-bottom: 10px;
    }
    .btn-tfoot-table{
      padding-top: 2px;
      padding-bottom: 2px;
    }
</style>
<head>
	<title>REKAPITULASI JENIS KELAMIN DAN UNIT KERJA</title>
</head>
<body>
	<center><h4>REKAPITULASI JENIS KELAMIN DAN UNIT KERJA<br>KEMENKO KEMARITIMAN</h4></center>
	<table class="ropeg_table" border="1" id="" width="100%" id="list_data">
			<thead>
				<tr>
					<th class="text-center" rowspan="2">NO</th>
					<th class="text-left" rowspan="2">UNIT KERJA</th>
					<th class="text-center" colspan="2">JUMLAH JENIS KELAMIN</th>
					<th class="text-center" rowspan="2">JUMLAH</th>
				</tr>
				<tr>
					<th class="text-center">LAKI</th>
					<th class="text-center">PEREMPUAN</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$jml_lk = 0;
				$jml_pr = 0;
				$jml = 0;
				$no = 1; foreach ($rekap_unker as $ru) { ?>
				<tr>
					<td class="text-center"><?php echo $no; ?></td>
					<td><?php echo $ru['nama_unker'] ?></td>
					<td class="text-center"><?php echo $ru['jml_lk'] ?></td>
					<td class="text-center"><?php echo $ru['jml_pr'] ?></td>
					<td class="text-center"><?php echo $ru['jml'] ?></td>
				</tr>
				<?php 
				$jml_lk += $ru['jml_lk'];
				$jml_pr += $ru['jml_pr'];
				$jml += $ru['jml'];
				?>
				<?php $no++;} ?>
				<tr>
					<td colspan="2">JUMLAH</td>
					<td class="text-center"><?php echo $jml_lk ?></td>
					<td class="text-center"><?php echo $jml_pr ?></td>
					<td class="text-center"><?php echo $jml ?></td>
				</tr>
			</tbody>
		</table>
</body>
</html>