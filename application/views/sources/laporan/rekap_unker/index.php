<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Rekaptulasi Jenis Kelamin dan Unit Kerja</h6>
		<div class="heading-elements">
	        <a href="javascript:read_preview()" class="btn bg-slate btn-raised legitRipple">PRINT PREVIEW</a>
		</div>

	</div>

	<div class="panel-body">

		<table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive ropeg_table" id="list_data">
			<thead>
				<tr>
					<th class="text-center" rowspan="2">NO</th>
					<th class="text-left" rowspan="2">UNIT KERJA</th>
					<th class="text-center" colspan="2">JUMLAH JENIS KELAMIN</th>
					<th class="text-center" rowspan="2">JUMLAH</th>
				</tr>
				<tr>
					<th class="text-center">LAKI</th>
					<th class="text-center">PEREMPUAN</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$jml_lk = 0;
				$jml_pr = 0;
				$jml = 0;
				$no = 1; foreach ($rekap_unker as $ru) { ?>
				<tr>
					<td class="text-center"><?php echo $no; ?></td>
					<td><?php echo $ru['nama_unker'] ?></td>
					<td class="text-center"><?php echo $ru['jml_lk'] ?></td>
					<td class="text-center"><?php echo $ru['jml_pr'] ?></td>
					<td class="text-center"><?php echo $ru['jml'] ?></td>
				</tr>
				<?php 
				$jml_lk += $ru['jml_lk'];
				$jml_pr += $ru['jml_pr'];
				$jml += $ru['jml'];
				?>
				<?php $no++;} ?>
				<tr>
					<td colspan="2">JUMLAH</td>
					<td class="text-center"><?php echo $jml_lk ?></td>
					<td class="text-center"><?php echo $jml_pr ?></td>
					<td class="text-center"><?php echo $jml ?></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript"> 
    function read_preview(){
        var win = window.open("../laporan/rekap_unker/preview", '_blank');
        win.focus();
    }
</script>
