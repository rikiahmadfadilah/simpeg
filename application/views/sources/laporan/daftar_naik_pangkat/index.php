
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
    <div class="panel-heading">
        <h6 class="panel-title">Daftar Pegawai Akan Naik Pangkat</h6>
    </div>

    <div class="panel-body">
        <form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group form-group-xs">
                        <label class="col-md-3 control-label">Unit Kerja</label>
                        <div class="col-md-7">
                            <select name="unker1" id="unker1" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs">
                                <option value=""></option>
                                <?php foreach ($unker as $i) {
                                    echo '<option value="'.$i["kode_unker"].'">'.$i["nama_unker"].'</option>';
                                }?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-group-xs">
                        <label class="col-md-3 control-label">Unit Kerja</label>
                        <div class="col-md-7">
                            <select name="unker2" id="unker2" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs">
                                <option value=""></option>
                                <?php foreach ($unker as $i) {
                                    echo '<option value="'.$i["kode_unker"].'">'.$i["nama_unker"].'</option>';
                                }?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-group-xs">
                        <label class="col-md-3 control-label">Periode Bulan</label>
                        <div class="col-md-7">
                            <select name="periode" id="periode" data-placeholder="" data-id="0" class="select-size-xs">
                                <option value="04">April</option>
                                <option value="10">Oktober</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-group-xs">
                        <label class="col-lg-3 control-label">Tahun</label>
                        <div class="col-lg-1">
                            <input type="number" min="1900" value="<?php echo date('Y') ?>" class="form-control input-xs wajib" placeholder="" name="tahun" id="tahun">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-md-offset-3" id="tampil">
                    <a href="javascript:void(0);" class="btn btn-xs btn-primary">Lihat</a>
                    <input id="filter" name="filter" type="hidden" min="1900" max="2050" class="form-control input-xs wajib">
                </div>
            </div>
            </br>

            <div id="list_data" style="display: none;">
                <legend class="text-bold" style="padding-bottom:5px;border-bottom:solid 1px #00bcd4;"></legend>
                <table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive ropeg_table" id="">
                    <thead>
                        <tr>
                            <td colspan="9" valign="middle" align="center">
                            <b><font size="3">
                            DAFTAR PEGAWAI YANG AKAN NAIK PANGKAT<br>KEMENKO
                            KEMARITIMAN<br>PERIODE BULAN <b id="bulantxt"></b> <b id="tahuntxt"></b></font></b></td>
                        </tr>
                        <!-- <tr>
                            <td align="left" colspan="13"><b>SEKRETARIAT JENDERAL</b></td>
                        </tr> -->
                    </thead>
                </table>
                <div class="center" align="right">
                    <a href="javascript:read_preview()" class="btn bg-slate btn-raised legitRipple">PRINT PREVIEW</a>
                </div>
                <table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive ropeg_table" id="list_data_table">
                    <thead>
                        <tr>
                            <td width="2%" valign="top" rowspan="2">
                            <p align="center"><b>NO</b></p></td>
                            <td width="20%" valign="top" rowspan="2">
                            <p align="center"><b>NAMA PEGAWAI<br>TEMPAT/TANGGAL LAHIR</b></p></td>
                            <td width="13%" valign="top" rowspan="2">
                            <p align="center"><b>NIP</b><b><br>AGAMA</b></p></td>
                            <td width="11%" valign="top" colspan="2">
                            <p align="center"><b>PANGKAT<br>LAMA</b></p></td>
                            <td width="20%" valign="top" colspan="2">
                            <p align="center"><b>JABATAN</b></p></td>
                            <td width="6%" valign="top" colspan="2">
                            <p align="center"><b>MASA<br>KERJA LAMA</b></p></td>
                            <td width="10%" valign="top" colspan="2">
                            <p align="center"><b>LATIHAN JABATAN</b></p></td>
                            <td width="11%" valign="top" colspan="2">
                            <p align="center"><b>PANGKAT<br>BARU</b></p></td>
                            <td width="11%" valign="top" rowspan="2">
                            <p align="center"><b>PHOTO</b></p></td>
                        </tr>
                        <tr>
                            <td width="3%" valign="top" align="center">
                            <b>GOL/<br>RU</b></td>
                            <td width="8%" valign="top" align="center">
                            <b>TMT</b></td>
                            <td valign="top" align="center" width="12%">
                            <b>NAMA</b></td>
                            <td valign="top" align="center" width="8%">
                            <b>TMT</b></td>
                            <td valign="top" align="center" width="3%">
                            <b>THN</b></td>
                            <td valign="top" align="center" width="3%">
                            <b>BLN</b></td>
                            <td valign="top" align="center" width="7%">
                            <b>NAMA</b></td>
                            <td valign="top" align="center" width="3%">
                            <b>THN</b></td>
                            <td valign="top" align="center" width="3%">
                            <b>GOL/<br>RU</b></td>
                            <td valign="top" align="center" width="8%">
                            <b>TMT</b></td>
                        </tr>
                    </thead>
                </table>
            </div>
        </form>
        
        
    </div>
</div>

<script type="text/javascript">
    $('#tampil').click(function(){
        $("#list_data").slideDown('fast');
        
        var unker1    = $('#unker1 option:selected').attr('value');
        var unker2    = $('#unker2 option:selected').attr('value');
        var periode    = $('#periode option:selected').attr('value');
        if(periode == '04'){
            $("#bulantxt").html("").append('APRIL');
        }else{
            $("#bulantxt").html("").append('OKTOBER');
        }
        var thn    = $('#tahun').val();
        var tahun    = ($('#tahun').val() - 4);
        $("#tahuntxt").html("").append(thn);
        // alert(tahun);
        
        $('#list_data_table').dataTable( {
            "processing": true,
            "serverSide": true,
            "bServerSide": true,
            "ordering": true,
            "destroy": true,
            "sAjaxSource": base_url+"laporan/daftar_naik_pangkat/list_data?unker1="+unker1+"&unker2="+unker2+"&periode="+periode+"&tahun="+tahun,
            "aaSorting": [],
            "order": [],
            "iDisplayLength": 10,
            "aoColumns": [
                { "bSortable": false, "sClass": "text-left" },
                { "bSortable": false, "sClass": "text-left" },
                { "bSortable": false, "sClass": "text-left" },
                { "bSortable": false, "sClass": "text-left" },
                { "bSortable": false, "sClass": "text-left" },
                { "bSortable": false, "sClass": "text-left" },
                { "bSortable": false, "sClass": "text-left" },
                { "bSortable": false, "sClass": "text-left" },
                { "bSortable": false, "sClass": "text-left" },
                { "bSortable": false, "sClass": "text-center" },
                { "bSortable": false, "sClass": "text-center" },
                { "bSortable": false, "sClass": "text-left" },
                { "bSortable": false, "sClass": "text-center" },
                { "bSortable": false, "sClass": "text-center" }
            ],
            "fnDrawCallback": function () {
                set_default_datatable();
            },
        });
    });

</script>
<script type="text/javascript"> 
    function read_preview(){
        var unker1    = $('#unker1 option:selected').attr('value');
        var unker2    = $('#unker2 option:selected').attr('value');
        var periode    = $('#periode option:selected').attr('value');
        var tahun    = $('#tahun').val();
        var eselon2    = $('#eselon2 option:selected').attr('value');
        var golongan1    = $('#golongan1 option:selected').attr('value');
        var golongan2    = $('#golongan2 option:selected').attr('value');
        
        var win = window.open("../laporan/daftar_naik_pangkat/preview?unker1="+unker1+"&unker2="+unker2+"&periode="+periode+"&tahun="+tahun, '_blank');
        win.focus();
    }
</script>
