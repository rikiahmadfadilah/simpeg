<style type="text/css">
    table.ropeg_table thead tr {
        background-color: #ebecff;
    }
    table.ropeg_table th, table.ropeg_table td {
      font-size : 12px;
    }
    table.ropeg_table td {
     vertical-align: top !important;
    }
    table.ropeg_table tfoot tr{
      background-color: #ebecff;

    }
    table.ropeg_table{
     margin-bottom: 10px;
    }
    .btn-tfoot-table{
      padding-top: 2px;
      padding-bottom: 2px;
    }
</style>
  <div align="center">
    <b>
        <font size="3">
            DAFTAR PEGAWAI YANG AKAN NAIK PANGKAT
            <br>
            KEMENKO KEMARITIMAN
            <br>
            <?php if($periode == 10){
              $bulan = 'OKTOBER';
            }else{
              $bulan = 'APRIL';
            } ?>
            PERIODE BULAN <?php echo $bulan ?> <?php echo $tahun ?>
        </font>
    </b>
  </div>
  <br>
   <table class="ropeg_table" id="list_data_table" border="1">
      <thead>
          <tr>
              <td width="2%" valign="top" rowspan="2">
              <p align="center"><b>NO</b></p></td>
              <td width="20%" valign="top" rowspan="2">
              <p align="center"><b>NAMA PEGAWAI<br>TEMPAT/TANGGAL LAHIR</b></p></td>
              <td width="13%" valign="top" rowspan="2">
              <p align="center"><b>NIP</b><b><br>AGAMA</b></p></td>
              <td width="11%" valign="top" colspan="2">
              <p align="center"><b>PANGKAT<br>LAMA</b></p></td>
              <td width="20%" valign="top" colspan="2">
              <p align="center"><b>JABATAN</b></p></td>
              <td width="6%" valign="top" colspan="2">
              <p align="center"><b>MASA<br>KERJA LAMA</b></p></td>
              <td width="10%" valign="top" colspan="2">
              <p align="center"><b>LATIHAN JABATAN</b></p></td>
              <td width="11%" valign="top" colspan="2">
              <p align="center"><b>PANGKAT<br>BARU</b></p></td>
              <td width="11%" valign="top" rowspan="2">
              <p align="center"><b>PHOTO</b></p></td>
          </tr>
          <tr>
              <td width="3%" valign="top" align="center">
              <b>GOL/<br>RU</b></td>
              <td width="8%" valign="top" align="center">
              <b>TMT</b></td>
              <td valign="top" align="center" width="12%">
              <b>NAMA</b></td>
              <td valign="top" align="center" width="8%">
              <b>TMT</b></td>
              <td valign="top" align="center" width="3%">
              <b>THN</b></td>
              <td valign="top" align="center" width="3%">
              <b>BLN</b></td>
              <td valign="top" align="center" width="7%">
              <b>NAMA</b></td>
              <td valign="top" align="center" width="3%">
              <b>THN</b></td>
              <td valign="top" align="center" width="3%">
              <b>GOL/<br>RU</b></td>
              <td valign="top" align="center" width="8%">
              <b>TMT</b></td>
          </tr>
      </thead>
      <tbody>
        <?php $no = 1; foreach ($preview as $key => $p){ ?>
        <tr>
          <td><?php echo $no; ?></td>
          <td><?php echo $p['NAMA'] ?><br><?php echo $p['TMP_TGL_LAHIR'] ?></td>
          <td><?php echo $p['NIP'] ?><br><?php echo $p['AGAMA_NAMA'] ?></td>
          <td><?php echo $p['GOLONGAN'] ?></td>
          <td><?php echo dateEnToId($p["TMT_GOL"], 'd-m-Y') ?></td>
          <td><?php echo $p['NAMA_JAB'] ?></td>
          <td><?php echo dateEnToId($p["TMT_JAB"], 'd-m-Y') ?></td>
          <td><?php echo $p['MASA_KERJA_THN'] ?></td>
          <td><?php echo $p['MASA_KERJA_BLN'] ?></td>
          <td><?php echo $p['NAMA_DIKLAT'] ?></td>
          <td><?php echo $p['THN_DIKLAT'] ?></td>
          <td><?php echo new_pangkat($p["GOLONGAN"]) ?></td>
          <td><?php echo dateEnToId($p["TMT_GOL"], 'd-m-').''.(dateEnToId($p["TMT_GOL"], 'Y') + 4) ?></td>
          <td><img src="<?php echo base_url();?>assets/images/pegawai/<?php echo $p["PHOTO"] ?>"  alt="PHOTO" name="photo" width="70" height="80" align="left" id="photo"></td>
        </tr>
        <?php $no++;} ?>
      </tbody>
  </table>