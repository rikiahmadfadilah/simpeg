<style type="text/css">
    table.ropeg_table thead tr {
        background-color: #ebecff;
    }
    table.ropeg_table th, table.ropeg_table td {
      font-size : 12px;
    }
    table.ropeg_table td {
     vertical-align: top !important;
    }
    table.ropeg_table tfoot tr{
      background-color: #ebecff;

    }
    table.ropeg_table{
     margin-bottom: 10px;
    }
    .btn-tfoot-table{
      padding-top: 2px;
      padding-bottom: 2px;
    }
</style>
    <div align="center">
    <b>
        <font size="3">
            DAFTAR PEGAWAI CUTI
            <br>
            KEMENKO KEMARITIMAN
        </font>
    </b>
    </div>
    <br>
    <table class="ropeg_table" border="1" id="" width="100%">
	<thead>
        <tr>
            <td>
                <b>NO.</b>
            </td>
            <td width="10%">
                <p align="center">
                    <b>Nip/Nama/Golongan</b>
                </p>
            </td>
            <td width="15%" valign="top">
                <p align="center">
                    <b>Jabatan</b>
                </p>
            </td>
            <td width="20%" valign="top">
                <p align="center">
                    <b>Unit Kerja</b>
                </p>
            </td>
            <td width="8%" align="center" valign="top">
                <b>Jenis Cuti</b>
            </td>
            <td width="8%" valign="top">
                <p align="center"><b>Tanggal Cuti</b>
                </p>
            </td>
            <td width="10%" valign="top">
                <p align="center">
                    <b>Jumlah<br>Hari/Bulan</b>
            </td>
            <td width="10%" valign="top">
                <p align="center">
                    <b>Nama</b>
                </p>
            </td>
            <td width="20%">
                <b>Jabatan & Unit Kerja</b>
            </td>
        </tr>
	</thead>
    <tbody>
        <?php $no = 1; foreach ($preview as $key => $p) { ?>
        <tr>
           <td><?php echo $no; ?></td>
            <td><?php echo $p["NIP"].'/'.$p["NAMA"].'/'.$p["GOLONGAN_NAMA"].' - '.$p["PANGKAT"]?>
            <td><?php echo $p["NAMA_JAB"]?>
            <td><?php echo $p["UNIT_KERJA"]?>
            <td><?php echo $p["JENIS_CUTI"]?>
            <td align="center"><?php echo dateEnToId($p["tgl_start"], 'd-m-Y').'<br>s/d<br>'.dateEnToId($p["tgl_end"], 'd-m-Y')?></td>
            <td><?php echo $p["jml_hari_nama"].' '.$p['satuan_jml_nama']?>
            <td><?php echo $p["nama_kepala"]?>
            <td><?php echo $p["nama_unker_kepala"]?>
        </tr>
        <?php $no++; } ?>
    </tbody>
	
	
</table>