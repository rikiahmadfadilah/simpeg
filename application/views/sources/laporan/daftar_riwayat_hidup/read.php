<script src="<?php echo base_url();?>assets/js/core/libraries/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/core/libraries/bootstrap.min.js"></script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Daftar Riwayat Hidup</title>
<style type="text/css">
.style1 {
        font-family: Arial, Helvetica, sans-serif;
        font-size:30px;
        color: #0000FF;
}
.style2 {
        font-family: Arial;
        font-size:12px; 
}
.style3 {
        font-weight: bold;
}
</style>
</head>
<body>
    <form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
<table id="table1" border="0" cellpadding="3" width="100%">
<tbody>
<tr>
  <th width="11%">
    <img src="<?php echo base_url();?>assets/images/logo_maritim.png" width="100" height="100">
  </th>
  <th>
    <span class="style1">DATA RIWAYAT HIDUP</span>
  </th>
</tr>
<div align="right"><button href="javascript:void(0);" target="_blank" id="print1">PRINT</button></div>
<div align="right"><button href="javascript:void(0);" target="_blank" id="print2" style="display: none;">PRINT</button></div>
</tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="style2" width="100%">
<tbody>
<tr>
  <td colspan="3">
    <font size="+1"><b>I. DATA PRIBADI</b></font>
  </td>
</tr>
</tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="style2" width="100%">
<tbody>
<tr>
  <td width="18%">N A M A</td>
  <td width="1%">:</td>
  <td width="60%">
    <strong><?php echo $pegawai["NAMA"]; ?></strong>
  </td>
  <td rowspan="9">
    <img src="<?php echo base_url();?>assets/images/pegawai/<?php echo $pegawai["PHOTO"] ?>"  alt="PHOTO" name="photo" width="120" height="160" align="left" id="photo">
  </td>
</tr>
<tr>
  <td>NIP</td>
  <td>:</td>
  <td>
    <strong><?php echo $pegawai["NIP"]; ?></strong>
  </td>
</tr>
<tr>
  <td>KARPEG/KARIS-KARSU/NPWP</td>
  <td>:</td>
  <td><?php echo $pegawai["NO_KARPEG"]; ?>/<?php echo $pegawai["NO_KARIS"]; ?>/<?php echo $pegawai["NO_NPWP"]; ?></td>
</tr>
<tr>
  <td>TEMPAT/TANGGAL LAHIR</td>
  <td>:</td>
  <td><?php echo $pegawai["TMP_TGL_LAHIR"]; ?></td>
</tr>
<tr>
  <td>JENIS KELAMIN</td>
  <td>:</td>
  <td><?php echo $pegawai["JENIS_KELAMIN"]; ?></td>
</tr>
<tr>
  <td>AGAMA</td>
  <td>:</td>
  <td><?php echo $pegawai["AGAMA_NAMA"]; ?></td>
</tr>
<tr>
  <td>STATUS KELUARGA</td>
  <td>:</td>
  <td><?php echo $pegawai["STATUS_KAWIN"]; ?></td>
</tr>
<tr>
  <td>PENDIDIKAN AKHIR</td>
  <td>:</td>
  <td><?php echo $pegawai["NAMA_PENDIDIKAN"]; ?></td>
</tr>
<tr>
  <td>PROGRAM STUDI</td>
  <td>:</td>
  <td><?php echo $pegawai["PRO_STUDI"]; ?></td>
</tr>
<tr>
  <td valign="top">UNIT KERJA</td>
  <td valign="top">:</td>
  <td>
    <strong><?php echo $pegawai["UNIT_KERJA"]; ?></strong>
  </td>
</tr>
<tr>
  <td>DIKLAT PENJENJANGAN</td>
  <td>:</td>
    <td><?php echo $pegawai["NAMA_DIKLAT"]; ?>/<?php echo dateEnToID($pegawai["TGL_DIKLAT"], 'd-m-Y'); ?></td>
  <td></td>
</tr>
<!-- <tr>
  <td></td>
  <td></td>
  <td>-/</td>
  <td></td>
</tr> -->
<tr>
  <td>PANGKAT,GOLONGAN,TMT GOL/TMT CPNS</td>
  <td>:</td>
  <td><?php echo $pegawai["PANGKAT"]; ?>, <?php echo $pegawai["GOLONGAN_NAMA"]; ?>, <?php echo dateEnToID($pegawai["TMT_GOL"], 'd-m-Y'); ?> / <?php echo $pegawai["TMT_CPNS"]; ?></td>
  <td></td>
</tr>
<tr>
  <td>MASA KERJA GOLONGAN</td>
  <td>:</td>
  <td></td>
  <td></td>
</tr>
<tr>
  <td>MASA KERJA KESELURUHAN</td>
  <td>:</td>
  <td></td>
  <td></td>
</tr>
<tr>
  <td valign="top">JABATAN STRUKTURAL/TMT</td>
  <td valign="top">:</td>
  <td>
    <strong><?php echo $pegawai["NAMA_JAB"]; ?> / <?php echo dateEnToID($pegawai["TMT_JAB"], 'd-m-Y'); ?></strong>
  </td>
  <td></td>
</tr>
<tr>
  <td>JABATAN FUNGSIONAL</td>
  <td>:</td>
  <td><?php echo $pegawai["NAMA_JABFUNG"]; ?> / <?php echo dateEnToID($pegawai["TMT_JABFUNG"], 'd-m-Y'); ?></td>
  <td></td>
</tr>
<tr>
  <td>BIDANG KEAHLIAN</td>
  <td>:</td>
  <td>
    / </td>
  <td></td>
</tr>
<tr>
  <td>ALAMAT RUMAH</td>
  <td>:</td>
  <td><?php echo $pegawai["ALAMAT"]; ?></td>
  <td></td>
</tr>
<tr>
  <td>KOTA/KODE POS</td>
  <td>:</td>
  <td> / <?php echo $pegawai["KODE_POS"]; ?></td>
  <td></td>
</tr>
<tr>
  <td>PROPINSI/NEGARA</td>
  <td>:</td>
  <td>/INDONESIA</td>
  <td></td>
</tr>
<tr>
  <td>TELEPHONE RUMAH/HP</td>
  <td>:</td>
  <td><?php echo $pegawai["TELPON"]; ?> / <?php echo $pegawai["NO_HP_SMS"]; ?></td>
  <td></td>
</tr>
<tr>
  <td>ALAMAT KANTOR</td>
  <td>:</td>
  <td></td>
  <td></td>
</tr>
<tr>
  <td>TELPHONE/EXT</td>
  <td>:</td>
  <td>/</td>
  <td></td>
</tr>
</tbody>
</table>
<br>
<div align="left" class="style2">
  <font size="+1"><b>II. DATA PENDIDIKAN FORMAL</b></font>
</div>
<table border="1" cellpadding="0" cellspacing="0" class="style2" width="100%">
<thead>
<tr class="style3">
  <td>TINGKAT</td>
  <td>NAMA SEKOLAH</td>
  <td>PROGRAM STUDI</td>
  <td>LULUS</td>
  <td>LOKASI</td>
  <td>KEPALA/DEKAN</td>
</tr>
</thead>
<tbody>
  <?php foreach ($dt_pendidikan as $dp) {  ?>
  <tr>
    <td><?php echo $dp["NAMA_PENDIDIKAN"];?></td>
    <td><?php echo $dp["NAMA_SEK_PT"];?></td>
    <td><?php echo $dp["PRO_STUDI"];?></td>
    <td><?php echo $dp["THN_LULUS"];?></td>
    <td><?php echo $dp["LOKASI"];?></td>
    <td><?php echo $dp["NAMA_KEPSEK_REKTOR"];?></td>
  </tr>
  <?php } ?>
</tbody>
</table>
<!-- <br>
<div align="left" class="style2">
  <font size="+1"><b>III. DATA PENDIDIKAN INFORMAL</b></font>
</div>
<table border="1" cellpadding="1" cellspacing="0" class="style2" width="100%">
<thead>
<tr class="style3">
  <td>NAMA KURSUS</td>
  <td>LOKASI</td>
  <td>MULAI</td>
  <td>SELESAI</td>
  <td>PENYELENGGARA</td>
  <td>JML JAM</td>
</tr>
</thead>
<tbody>
  <?php foreach ($pendidikan_nonformal as $pnf) {  ?>
  <tr>
    <td><?php echo $pnf["pendidikan_nonformal_nama"];?></td>
    <td><?php echo $pnf["pendidikan_nonformal_lokasi"];?></td>
    <td><?php echo $pnf["pendidikan_nonformal_tanggal_mulai"];?></td>
    <td><?php echo $pnf["pendidikan_nonformal_tanggal_selesai"];?></td>
    <td><?php echo $pnf["pendidikan_nonformal_penyelenggara"];?></td>
    <td><?php echo $pnf["pendidikan_nonformal_jumlah_jam"];?></td>
  </tr>
  <?php } ?>
</tbody>
</table> -->
<br>
<div align="left" class="style2">
  <font size="+1"><b>III. DATA RIWAYAT KEPANGKATAN</b></font>
</div>
<table border="1" cellpadding="1" cellspacing="0" class="style2" width="100%">
<thead>
<tr class="style3">
  <td>GOLONGAN</td>
  <td>TMT GOL</td>
  <td>PEJABAT</td>
  <td>NOMOR SK</td>
  <td>TGL SK</td>
</tr>
</thead>
<tbody>
  <?php foreach ($dt_kepangkatan as $dk) {  ?>
  <tr>
    <td><?php echo $dk["GOL"];?></td>
    <td><?php echo dateEnToID($dk["TMT_GOL"], 'd-m-Y');?></td>
    <td><?php echo $dk["PEJABAT"];?></td>
    <td><?php echo $dk["NOMOR_SK"];?></td>
    <td><?php echo dateEnToID($dk["TGL_SK"], 'd-m-Y');?></td>
  </tr>
  <?php } ?>
</tbody>
</table>
<br>
<div align="left" class="style2">
  <font size="+1"><b>IV. DATA RIWAYAT JABATAN</b></font>
</div>
<table border="1" cellpadding="1" cellspacing="0" class="style2" width="100%">
<thead>
<tr class="style3">
  <td>ESELON</td>
  <td>NAMA JABATAN</td>
  <td>TMT JABATAN</td>
  <td>NOMOR SK</td>
  <td>TGL SK</td>
</tr>
</thead>
<tbody>
  <?php foreach ($dt_jabatan as $dj) {  ?>
  <tr>
    <td><?php echo $dj["NAMA_ESELON"];?></td>
    <td><?php echo $dj["NAMA_JAB"];?></td>
    <td><?php echo dateEnToID($dj["TGL_MULAI"], 'd-m-Y');?></td>
    <td><?php echo $dj["NOMOR_SK"];?></td>
    <td><?php echo dateEnToID($dj["TANGGAL_SK"], 'd-m-Y');?></td>
  </tr>
  <?php } ?>
</tbody>
</table>
<br>
<div align="left" class="style2">
  <font size="+1"><b>V. DATA DIKLAT PENJENJANGAN/LEMHANAS</b></font>
</div>
<table border="1" cellpadding="1" cellspacing="0" class="style2" width="100%">
<thead>
<tr class="style3">
  <td>NAMA DIKLAT</td>
  <td>LOKASI</td>
  <td>TGL MULAI</td>
  <td>TGL SELESAI</td>
  <td>PREDIKAT</td>
  <td>PENYELENGGARA</td>
  <td>JML JAM</td>
</tr>
</thead>
<tbody>
  <?php foreach ($dt_diklat as $dd) {  ?>
  <tr>
    <td><?php echo $dd["NAMA_DIKLAT"];?></td>
    <td><?php echo $dd["LOKASI_TEXT"];?></td>
    <td><?php echo dateEnToID($dd["TGL_MULAI"], 'd-m-Y');?></td>
    <td><?php echo dateEnToID($dd["TGL_SELESAI"], 'd-m-Y');?></td>
    <td><?php echo $dd["PREDIKAT_TEXT"];?></td>
    <td><?php echo $dd["PENYELENGGARA"];?></td>
    <td><?php echo $dd["JUMLAH_JAM"];?></td>
  </tr>
  <?php } ?>
</tbody>
</table>
<br>
<div align="left" class="style2">
  <font size="+1"><b>VI. DATA ISTRI/SUAMI</b></font>
</div>
<table border="1" cellpadding="1" cellspacing="0" class="style2" width="100%">
<thead>
<tr class="style3">
  <td>NAMA ISTRI/SUAMI</td>
  <td>NOMOR KARIS/KARSU</td>
  <td>TGL LAHIR</td>
  <td>TGL NIKAH</td>
  <!-- <td>PENDIDIKAN</td> -->
  <td>PEKERJAAN</td>
  <td>KETERANGAN</td>
</tr>
</thead>
<tbody>
  <?php foreach ($dt_keluarga as $dk) {  ?>
    <?php if($dk["HUBUNGAN"] == 1){ ?>
  <tr>
    <td><?php echo $dk["NAMA_KEL"];?></td>
    <td><?php echo $dk["NO_KARIS_KARSU"];?></td>
    <td><?php echo $dk["TGL_LAHIR"];?></td>
    <td><?php echo $dk["TGL_NIKAH"];?></td>
    <!-- <td><?php echo $dk["pendidikan_nama"];?></td> -->
    <td><?php echo $dk["PEKERJAAN"];?></td>
    <td>-</td>
  </tr>
  <?php } ?>
  <?php } ?>
</tbody>
</table>
<br>
<div align="left" class="style2">
  <font size="+1"><b>VII. DATA ANAK</b></font>
</div>
<table border="1" cellpadding="1" cellspacing="0" class="style2" width="100%">
<thead>
<tr class="style3">
  <td>NAMA ANAK</td>
  <td>JNS KELAMIN</td>
  <td>TMP LAHIR</td>
  <td>TGL LAHIR</td>
  <!-- <td>USIA</td> -->
  <!-- <td>KONDISI</td> -->
</tr>
</thead>
<tbody>
  <?php foreach ($dt_keluarga as $dk) {  ?>
    <?php if($dk["HUBUNGAN"] == 2){ ?>
  <tr>
    <td><?php echo $dk["NAMA_KEL"];?></td>
    <td><?php echo $dk["JENIS_KELAMIN_TEXT"];?></td>
    <td><?php echo $dk["TMP_LAHIR"];?></td>
    <td><?php echo $dk["TGL_LAHIR"];?></td>
    <!-- <td><?php echo $dk["anak_usia"];?></td> -->
    <!-- <td><?php echo $dk["anak_kondisi_nama"];?></td> -->
  </tr>
    <?php } ?>
  <?php } ?>
</tbody>
</table>
<br>
<div align="left" class="style2">
  <font size="+1"><b>VIII. DATA KELUARGA</b></font>
</div>
<table border="1" cellpadding="1" cellspacing="0" class="style2" width="100%">
<thead>
<tr class="style3">
  <td>NAMA KELUARGA</td>
  <td>JNS KELAMIN</td>
  <td>TGL LAHIR</td>
  <td>HUBUNGAN</td>
  <td>PEKERJAAN</td>
  <!-- <td>KONDISI</td> -->
</tr>
</thead>
<tbody>
  <?php foreach ($dt_keluarga as $dk) {  ?>
    <?php if($dk["HUBUNGAN"] != 1 AND $dk["HUBUNGAN"] != 2){ ?>
  <tr>
    <td><?php echo $dk["NAMA_KEL"];?></td>
    <td><?php echo $dk["JENIS_KELAMIN_TEXT"];?></td>
    <td><?php echo $dk["TGL_LAHIR"];?></td>
    <td><?php echo $dk["HUBUNGAN_KELUARGA"];?></td>
    <td><?php echo $dk["PEKERJAAN"];?></td>
    <!-- <td><?php echo $dk["keluarga_kondisi_nama"];?></td> -->
  </tr>
    <?php } ?>
  <?php } ?>
</tbody>
</table>
<!-- <br>
<div align="left" class="style2">
  <font size="+1"><b>X. DATA SEMINAR/LOKAKARYA</b></font>
</div>
<table border="1" cellpadding="1" cellspacing="0" class="style2" width="100%">
  <thead>
    <tr class="style3">
      <td>NAMA SEMINAR/LOKAKARYA</td>
      <td>LOKASI</td>
      <td>PENYELENGGARA</td>
      <td>TAHUN</td>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($seminar as $s) {  ?>
    <tr>
      <td><?php echo $s["seminar_nama_kegiatan"];?></td>
      <td><?php echo $s["seminar_lokasi"];?></td>
      <td><?php echo $s["seminar_penyelenggara"];?></td>
      <td><?php echo $s["seminar_tahun"];?></td>
    </tr>
    <?php } ?>
  </tbody>
</table> -->
<br>
<div align="left" class="style2">
  <font size="+1"><b>IX. DATA TANDA JASA</b></font>
</div>
<table border="1" cellpadding="1" cellspacing="0" class="style2" width="100%">
<thead>
<tr class="style3">
  <td>NAMA TANDA JASA</td>
  <td>TGL PEROLEH</td>
  <td>NOMOR SK</td>
  <td>PEMBERI</td>
  <td>JABATAN</td>
</tr>
</thead>
<tbody>
  <?php foreach ($dt_penghargaan as $dp) {  ?>
  <tr>
    <td><?php echo $dp["NAMA_TANDA"];?></td>
    <td><?php echo $dp["TGL_OLEH"];?></td>
    <td><?php echo $dp["NOMOR_SK"];?></td>
    <td><?php echo $dp["PEMBERI"];?></td>
    <td><?php echo $dp["JABATAN"];?></td>
  </tr>
  <?php } ?>
</tbody>
</table>
<br>
<div align="left" class="style2">
  <font size="+1"><b>X. DATA INDIKASI HUKUMAN DISIPLIN</b></font>
</div>
<table border="1" cellpadding="1" cellspacing="0" class="style2" width="100%">
<thead>
<tr class="style3">
  <td>KODE HUKUM</td>
  <!-- <td>NOMOR SK</td> -->
  <td>TGL SK</td>
  <td>TGL BERLAKU</td>
  <!-- <td>PEJABAT</td> -->
</tr>
</thead>
<tbody>
  <?php foreach ($dt_hukdis as $hd) {  ?>
  <tr>
     <td><?php echo $hd["KODE_HUKDIS"];?></td>
    <td><?php echo $hd["TGL_MONEV"];?></td>
    <td><?php echo $hd["TGL_SELESAI"];?></td>
    <!-- <td><?php echo $hd["jenis_pejabat_berwenang_nama"];?></td> -->
  </tr>
  <?php } ?>
</tbody>
</table>
<br>
<div align="left" class="style2">
  <font size="+1"><b>XI. PENILAIAN KINERJA PEGAWAI</b></font>
</div>
<table border="1" cellpadding="1" cellspacing="0" class="style2" width="100%">
<tbody>
<tr>
 <!--  <td rowspan="2" align="center">
    <b>NO</b>
  </td> -->
  <td rowspan="2" align="center">
    <b>TAHUN</b>
  </td>
  <td rowspan="2" align="center">
    <b>PERIODE</b>
  </td>
  <td rowspan="2" align="center">
    <b>JABATAN</b>
  </td><td rowspan="2" align="center">
    <b>UNIT KERJA</b>
  </td>
  </td><td rowspan="2" align="center">
    <b>SKP 100%</b>
  </td>
  <td colspan="7" align="center">
    <b>PENILAIAN PERILAKU</b>
  </td>
  <td colspan="3" align="center">
    <b>NILAI KINERJA (SKP 60% + PERILAKU 40%)</b>
  </td>
</tr>
<tr>
  <td align="center">
    <b>ORIENTASI PELAYANAN</b>
  </td>
  <td align="center">
    <b>INTEGRITAS</b>
  </td>
  <td align="center">
    <b>KOMITMEN</b>
  </td>
  <td align="center">
    <b>DISIPLIN</b>
  </td>
  <td align="center">
    <b>KERJASAMA</b>
  </td>
  <td align="center">
    <b>KEPEMIMPINAN</b>
  </td>
  <td align="center">
    <b>RATA-RATA</b>
  </td>
  <td align="center">
    <b>SKP 60%</b>
  </td>
  <td align="center">
    <b>PERILAKU 40%</b>
  </td>
  <td align="center">
    <b>TOTAL NILAI</b>
  </td>
</tr>
</tbody>
</table>
<br>
<div align="left" class="style2">
  <font size="+1"><b>XII. DATA ASSESSMENT</b></font>
</div>
<table border="1" cellpadding="1" cellspacing="0" class="style2" width="100%">
<thead>
<tr class="style3">
  <td>TGL ASSESSMENT</td>
  <td>NILAI JOB FIT(%)</td>
  <td>REKOMENDASI</td>
</tr>
</thead>
<tbody>
  <?php foreach ($dt_assessment as $da) {  ?>
  <tr>
     <td><?php echo dateEnToID($da["TANGGAL_ASSESSMENT"], 'd-m-Y');?></td>
    <td><?php echo $da["NILAI_JOB_FIT"];?></td>
    <td><?php echo $da["REKOMENDASI_TEXTT"];?></td>
  </tr>
  <?php } ?>
</tbody>
</table>
<br>
<div align="left" class="style2">
  <font size="+1"><b>XIII. URUTAN BERDASARKAN DAFTAR URUT KEPANGKATAN</b></font>
</div>
<table border="1" cellpadding="1" cellspacing="0" class="style2" width="100%">
<tbody>
<tr class="style3">
  <td align="center">LINGKUP UNIT KERJA</td>
  <td align="center">LINGKUP ESELON I</td>
  <td align="center">LINGKUP KEMARITIMAN</td>
</tr>
<!-- <tr>
  <td align="center">KE - <?php echo $urut['NO_URUT_UNITKERJA']; ?></td>
  <td align="center">KE - <?php echo $urut['NO_URUT_ESELON']; ?></td>
  <td align="center">KE - <?php echo $urut['NO_URUT_KKP']; ?></td>
</tr> -->
</tbody>
</table>
</form>
</body>
<script type="text/javascript">
  $( "#print1" ).click(function() {
    $('#print1').hide();
    window.print();
    $('#print2').show();
  });
  $( "#print2" ).click(function() {
    $('#print2').hide();
    window.print();
    $('#print1').show();
  });
</script>
</html>   