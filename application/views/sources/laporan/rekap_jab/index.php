<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Rekapitulasi Jenis Kelamin dan Jabatan</h6>
		<div class="heading-elements">
            <a href="javascript:read_preview()" class="btn bg-slate btn-raised legitRipple">PRINT PREVIEW</a>
		</div>
	</div>

	<div class="panel-body">
		<td>
            <table border="0" bordercolor="white" width="100%" cellspacing="1" cellpadding="0" id="table6">
            <tbody>
            <tr>
                <td bgcolor="#000000">
                    <table border="1" bordercolor="black" width="100%" cellspacing="0" id="table7">
                    <tbody>
                    <tr>
                        <td>
                            <table border="1" bordercolor="black" width="100%" cellspacing="1" cellpadding="5" id="table2">
                            <tbody>
                            <tr bgcolor="#52527A">
                                <td rowspan="2" width="5%" align="center">
                                    <font color="#FFFFFF"><b>NO</b></font>
                                </td>
                                <td rowspan="2" width="5%">
                                    <font color="#FFFFFF"><b>UNIT KERJA</b></font>
                                </td>
                                <td rowspan="2" width="10%">
                                    <font color="#FFFFFF"><b>JENIS KELAMIN</b></font>
                                </td>
                                <td colspan="9" align="center" width="55%">
                                    <font color="#FFFFFF"><b>JABATAN ESELON</b></font>
                                </td>
                                <td align="center" width="10%" rowspan="2">
                                    <font color="#FFFFFF"><b>
                                    JAB.<br>FUNG</b></font>
                                </td>
                                <td align="center" width="8%" rowspan="2">
                                    <font color="#FFFFFF"><b>
                                    PELAK-<br>SANA</b></font>
                                </td>
                                <td rowspan="2" width="7%">
                                    <font color="#FFFFFF"><b>JUMLAH</b></font>
                                </td>
                            </tr>
                            <tr bgcolor="#52527A">
                                <td width="6%" align="center">
                                    <font color="#FFFFFF"><b>I.a</b></font>
                                </td>
                                <td width="6%" align="center">
                                    <font color="#FFFFFF"><b>I.b</b></font>
                                </td>
                                <td width="6%" align="center">
                                    <font color="#FFFFFF"><b>II.a</b></font>
                                </td>
                                <td width="6%" align="center">
                                    <font color="#FFFFFF"><b>II.b</b></font>
                                </td>
                                <td width="6%" align="center">
                                    <font color="#FFFFFF"><b>III.a</b></font>
                                </td>
                                <td width="6%" align="center">
                                    <font color="#FFFFFF"><b>III.b</b></font>
                                </td>
                                <td width="6%" align="center">
                                    <font color="#FFFFFF"><b>IV.a</b></font>
                                </td>
                                <td width="6%" align="center">
                                    <font color="#FFFFFF"><b>IV.b</b></font>
                                </td>
                                <td width="6%" align="center">
                                    <font color="#FFFFFF"><b>&nbsp;V&nbsp;</b></font>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="15" bgcolor="#FFFFFF" height="1"></td>
                            </tr>
                            
                            
                            <?php
                                $esel_1a = 0;
                                $esel_1b = 0;
                                $esel_2a = 0;
                                $esel_2b = 0;
                                $esel_3a = 0;
                                $esel_3b = 0;
                                $esel_4a = 0;
                                $esel_4b = 0;
                                $esel_v = 0;
                                $esel_jabfunc = 0;
                                $pelaksana = 0;
                                $jml = 0;
                                $no=1; foreach ($rekap_jab as $rj){?>                     
            
                            <tr bgcolor="#AAAAC6">
                                <td align="center">
                                    <b><?php echo $no?></b>
                                </td>
                                <td colspan="14">
                                    <b><?php echo $rj['nama_unker']?></b>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" rowspan="3" bgcolor="#FFFFFF">&nbsp;</td>
                                <td bgcolor="#FFFFFF">LAKI-LAKI</td>
                                <td bgcolor="#FFFFFF" align="center"><?php echo ($rj['lk_esel_1a']?$rj['lk_esel_1a']:0)?></td>
                                <td bgcolor="#FFFFFF" align="center"><?php echo ($rj['lk_esel_1b']?$rj['lk_esel_1b']:0)?></td>
                                <td bgcolor="#FFFFFF" align="center"><?php echo ($rj['lk_esel_2a']?$rj['lk_esel_2a']:0)?></td>
                                <td bgcolor="#FFFFFF" align="center"><?php echo ($rj['lk_esel_2b']?$rj['lk_esel_2b']:0)?></td>
                                <td bgcolor="#FFFFFF" align="center"><?php echo ($rj['lk_esel_3a']?$rj['lk_esel_3a']:0)?></td>
                                <td bgcolor="#FFFFFF" align="center"><?php echo ($rj['lk_esel_3b']?$rj['lk_esel_3b']:0)?></td>
                                <td bgcolor="#FFFFFF" align="center"><?php echo ($rj['lk_esel_4a']?$rj['lk_esel_4a']:0)?></td>
                                <td bgcolor="#FFFFFF" align="center"><?php echo ($rj['lk_esel_4b']?$rj['lk_esel_4b']:0)?></td>
                                <td bgcolor="#FFFFFF" align="center"><?php echo ($rj['lk_esel_v']?$rj['lk_v']:0)?></td>
                                <td bgcolor="#FFFFFF" align="center"><?php echo ($rj['lk_jabfunc']?$rj['lk_jabfunc']:0)?></td>
                                <td bgcolor="#FFFFFF" align="center"><?php echo ($rj['lk_pelaksana']?$rj['lk_pelaksana']:0)?></td>
                                <td bgcolor="#FFFFFF" align="center">
                                    <b><?php echo ($rj['jml_lk']?$rj['jml_lk']:0)?><b></b></b>
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="#FFFFFF">PEREMPUAN</td>
                                <td bgcolor="#FFFFFF" align="center"><?php echo ($rj['pr_esel_1a']?$rj['pr_esel_1a']:0)?></td>
                                <td bgcolor="#FFFFFF" align="center"><?php echo ($rj['pr_esel_1b']?$rj['pr_esel_1b']:0)?></td>
                                <td bgcolor="#FFFFFF" align="center"><?php echo ($rj['pr_esel_2a']?$rj['pr_esel_2a']:0)?></td>
                                <td bgcolor="#FFFFFF" align="center"><?php echo ($rj['pr_esel_2b']?$rj['pr_esel_2b']:0)?></td>
                                <td bgcolor="#FFFFFF" align="center"><?php echo ($rj['pr_esel_3a']?$rj['pr_esel_3a']:0)?></td>
                                <td bgcolor="#FFFFFF" align="center"><?php echo ($rj['pr_esel_3b']?$rj['pr_esel_3b']:0)?></td>
                                <td bgcolor="#FFFFFF" align="center"><?php echo ($rj['pr_esel_4a']?$rj['pr_esel_4a']:0)?></td>
                                <td bgcolor="#FFFFFF" align="center"><?php echo ($rj['pr_esel_4b']?$rj['pr_esel_4b']:0)?></td>
                                <td bgcolor="#FFFFFF" align="center"><?php echo ($rj['pr_esel_v']?$rj['pr_esel_v']:0)?></td>
                                <td bgcolor="#FFFFFF" align="center"><?php echo ($rj['pr_jabfunc']?$rj['pr_jabfunc']:0)?></td>
                                <td bgcolor="#FFFFFF" align="center"><?php echo ($rj['pr_pelaksana']?$rj['pr_pelaksana']:0)?></td>
                                <td bgcolor="#FFFFFF" align="center">
                                    <b><?php echo ($rj['jml_pr']?$rj['jml_pr']:0)?><b></b></b>
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="#FFFFFF">JUMLAH</td>
                                <td bgcolor="#FFFFFF" align="center">
                                    <b><?php echo ($rj['jml_esel_1a']?$rj['jml_esel_1a']:0)?><b></b></b>
                                </td>
                                <td bgcolor="#FFFFFF" align="center">
                                    <b><?php echo ($rj['jml_esel_1b']?$rj['jml_esel_1b']:0)?><b></b></b>
                                </td>
                                <td bgcolor="#FFFFFF" align="center">
                                    <b><?php echo ($rj['jml_esel_2a']?$rj['jml_esel_2a']:0)?><b></b></b>
                                </td>
                                <td bgcolor="#FFFFFF" align="center">
                                    <b><?php echo ($rj['jml_esel_2b']?$rj['jml_esel_2b']:0)?><b></b></b>
                                </td>
                                <td bgcolor="#FFFFFF" align="center">
                                    <b><?php echo ($rj['jml_esel_3a']?$rj['jml_esel_3a']:0)?><b></b></b>
                                </td>
                                <td bgcolor="#FFFFFF" align="center">
                                    <b><?php echo ($rj['jml_esel_3b']?$rj['jml_esel_3b']:0)?><b></b></b>
                                </td>
                                <td bgcolor="#FFFFFF" align="center">
                                    <b><?php echo ($rj['jml_esel_4a']?$rj['jml_esel_4a']:0)?><b></b></b>
                                </td>
                                <td bgcolor="#FFFFFF" align="center">
                                    <b><?php echo ($rj['jml_esel_4b']?$rj['jml_esel_4b']:0)?><b></b></b>
                                </td>
                                <td bgcolor="#FFFFFF" align="center">
                                    <b><?php echo ($rj['jml_esel_v']?$rj['jml_esel_v']:0)?><b></b></b>
                                </td>
                                <td bgcolor="#FFFFFF" align="center">
                                    <b><?php echo ($rj['jml_jabfunc']?$rj['jml_jabfunc']:0)?><b></b></b>
                                </td>
                                <td bgcolor="#FFFFFF" align="center">
                                    <b><?php echo ($rj['jml_pelaksana']?$rj['jml_pelaksana']:0)?><b></b></b>
                                </td>
                                <td bgcolor="#52527A" align="center">
                                    <b><font color="#FFFFFF"><?php echo ($rj['jml']?$rj['jml']:0)?><b></b></font><b></b></b>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="15" bgcolor="#FFFFFF" height="1"></td>
                            </tr>
                            <?php 
                                $esel_1a += $rj['jml_esel_1a'];
                                $esel_1b += $rj['jml_esel_1b'];
                                $esel_2a += $rj['jml_esel_2a'];
                                $esel_2b += $rj['jml_esel_2b'];
                                $esel_3a += $rj['jml_esel_3a'];
                                $esel_3b += $rj['jml_esel_3b'];
                                $esel_4a += $rj['jml_esel_4a'];
                                $esel_4b += $rj['jml_esel_4b'];
                                $esel_v += $rj['jml_esel_v'];
                                $esel_jabfunc += $rj['jml_jabfunc'];
                                $pelaksana += $rj['jml_pelaksana'];
                                $jml += $rj['jml'];
                            ?>
                            <?
                            $no++; } ?>
                            

                           <tr>
                                <td bgcolor="#E8E8E8" colspan="3">
                                    <b>JUMLAH</b>
                                </td>
                                <td bgcolor="#000000" align="center">
                                    <font color="#FFFFFF"><b><?php echo $esel_1a ?><b></b></b></font>
                                </td>
                                <td bgcolor="#000000" align="center">
                                    <font color="#FFFFFF"><b><?php echo $esel_1b ?><b></b></b></font>
                                </td>
                                <td bgcolor="#000000" align="center">
                                    <font color="#FFFFFF"><b><?php echo $esel_2a ?><b></b></b></font>
                                </td>
                                <td bgcolor="#000000" align="center">
                                    <font color="#FFFFFF"><b><?php echo $esel_2b ?><b></b></b></font>
                                </td>
                                <td bgcolor="#000000" align="center">
                                    <font color="#FFFFFF"><b><?php echo $esel_3a ?><b></b></b></font>
                                </td>
                                <td bgcolor="#000000" align="center">
                                    <font color="#FFFFFF"><b><?php echo $esel_3b ?><b></b></b></font>
                                </td>
                                <td bgcolor="#000000" align="center">
                                    <font color="#FFFFFF"><b><?php echo $esel_4a ?><b></b></b></font>
                                </td>
                                <td bgcolor="#000000" align="center">
                                    <font color="#FFFFFF"><b><?php echo $esel_4b ?><b></b></b></font>
                                </td>
                                <td bgcolor="#000000" align="center">
                                    <font color="#FFFFFF"><b><?php echo $esel_v ?><b></b></b></font>
                                </td>
                                <td bgcolor="#000000" align="center">
                                    <font color="#FFFFFF"><b><?php echo $esel_jabfunc ?><b></b></b></font>
                                </td>
                                <td bgcolor="#000000" align="center">
                                    <font color="#FFFFFF"><b><?php echo $pelaksana ?><b></b></b></font>
                                </td>
                                <td bgcolor="#000000" align="center">
                                    <font color="#FFFFFF"><b><font color="#FFFFFF"><?php echo $jml ?><b></b></font><b></b></b></font>
                                </td>
                            </tr>
                            </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td bgcolor="#FFFFFF" colspan="6">&nbsp;</td>
            </tr>
            </tbody>
            </table>
        </td>
	</div>
</div>
<script type="text/javascript"> 
    function read_preview(){
        var win = window.open("../laporan/rekap_jabatan/preview", '_blank');
        win.focus();
    }
</script>