
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
    <div class="panel-heading">
        <h6 class="panel-title">Daftar Urut Kepangkatan</h6>
    </div>

    <div class="panel-body">
        <form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group form-group-xs">
                        <label class="col-md-3 control-label">Unit Kerja</label>
                        <div class="col-md-7">
                            <select name="unker1" id="unker1" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs">
                                <option value=""></option>
                                <?php foreach ($unker as $i) {
                                    echo '<option value="'.$i["kode_unker"].'">'.$i["nama_unker"].'</option>';
                                }?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-group-xs">
                        <label class="col-md-3 control-label">Unit Kerja</label>
                        <div class="col-md-7">
                            <select name="unker2" id="unker2" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs">
                                <option value=""></option>
                                <?php foreach ($unker as $i) {
                                    echo '<option value="'.$i["kode_unker"].'">'.$i["nama_unker"].'</option>';
                                }?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-group-xs">
                        <label class="col-md-3 control-label">Eselon</label>
                        <div class="col-md-3">
                        <select name="eselon1" id="eselon1" data-placeholder="Eselon" data-id="0" class="select-size-xs">
                            <option value=""></option>
                            <option>-</option>
                            <?php foreach ($eselon as $i) {
                                echo '<option value="'.$i["KODE_ESL"].'">'.$i["ESELON"].'</option>';
                            }?>
                        </select>
                        </div>
                        <div class="col-md-1">
                            <div class="text-center">
                                <b>S/D</b>
                            </div>
                        </div>
                        <div class="col-md-3">
                        <select name="eselon2" id="eselon2" data-placeholder="Eselon" data-id="0" class="select-size-xs">
                            <option value=""></option>
                            <option>-</option>
                            <?php foreach ($eselon as $i) {
                                echo '<option value="'.$i["KODE_ESL"].'">'.$i["ESELON"].'</option>';
                            }?>
                        </select>
                        </div>
                    </div>              
                    <div class="form-group form-group-xs">
                        <label class="col-md-3 control-label">Golongan</label>
                        <div class="col-md-3">
                        <select name="golongan1" id="golongan1" data-placeholder="Golongan" data-id="0" class="select-size-xs">
                            <option value=""></option>
                            <option>-</option>
                            <?php foreach ($golongan as $i) {
                                echo '<option value="'.$i["KODE_GOL"].'">'.$i["GOL"].'</option>';
                            }?>
                        </select>
                        </div>
                        <div class="col-md-1">
                            <div class="text-center">
                                <b>S/D</b>
                            </div>
                        </div>
                        <div class="col-md-3">
                        <select name="golongan2" id="golongan2" data-placeholder="Golongan" data-id="0" class="select-size-xs">
                            <option value=""></option>
                            <option>-</option>
                            <?php foreach ($golongan as $i) {
                                echo '<option value="'.$i["KODE_GOL"].'">'.$i["GOL"].'</option>';
                            }?>
                        </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-md-offset-3" id="tampil">
                    <a href="javascript:void(0);" class="btn btn-xs btn-primary">Lihat</a>
                    <input id="filter" name="filter" type="hidden" min="1900" max="2050" class="form-control input-xs wajib">
                </div>
            </div>
            </br>

            <div id="list_data" style="display: none;">
                <legend class="text-bold" style="padding-bottom:5px;border-bottom:solid 1px #00bcd4;"></legend>
                <table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive ropeg_table" id="">
                    <thead>
                        <tr>
                            <td colspan="9" valign="middle" align="center">
                            <b><font size="3">
                            DAFTAR URUT KEPANGKATAN<br>KEMENKO
                            KEMARITIMAN</font></b></td>
                        </tr>
                        <!-- <tr>
                            <td align="left" colspan="13"><b>SEKRETARIAT JENDERAL</b></td>
                        </tr> -->
                    </thead>
                </table>
                <div class="center" align="right">
                    <a href="javascript:read_preview()" class="btn bg-slate btn-raised legitRipple">PRINT PREVIEW</a>
                </div>
                <table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive ropeg_table" id="list_data_table">
                    <thead>
                        <tr>
                            <td align="center" rowspan="3"><b>NO</b></td>
                            <td align="center" colspan="3"><b>PEGAWAI</b></td>
                            <td align="center" colspan="2"><b>PANGKAT</b></td>
                            <td align="center" colspan="2"><b>JABATAN</b></td>
                            <td align="center" rowspan="3"><b>MASA KERJA GOL</b></td>
                            <td align="center" colspan="2"><b>LATIHAN JABATAN</b></td>
                            <td align="center" colspan="2"><b>PENDIDIKAN AKHIR</b></td>
                            <td align="center" rowspan="3"><b>FOTO</b></td>
                        </tr>
                        <tr>
                            <td align="center" rowspan="2"><b>NIP</b></td>
                            <td align="center" rowspan="2"><b>NAMA</b><br><b>TEMPAT/TANGGAL LAHIR</b><br><b>STATUS PEGAWAI</b></td>
                            <td align="center" rowspan="2"><b>USIA</b></td>
                            <td align="center" rowspan="2"><b>GOL/<br>RU</b></td>
                            <td align="center" rowspan="2"><b>TMT</b></td>
                            <td align="center" rowspan="2"><b>NAMA</b></td>
                            <td align="center" rowspan="2"><b>TMT</b></td>
                            <td align="center" rowspan="2"><b>NAMA</b></td>
                            <td align="center" rowspan="2"><b>TAHUN</b></td>
                            <td align="center" rowspan="2"><b>NAMA</b>
                            <td align="center" rowspan="2"><b>PROGRAM STUDI</b>
                        </tr>
                        <tr style="display: none;">
                            <td align="center" colspan="2">
                                <b>USIA</b>
                            </td>
                        </tr>
                    </thead>
                </table>
            </div>
        </form>
        
        
    </div>
</div>

<script type="text/javascript">
    $('#tampil').click(function(){
        $("#list_data").slideDown('fast');
        
        var unker1    = $('#unker1 option:selected').attr('value');
        var unker2    = $('#unker2 option:selected').attr('value');
        var eselon1    = $('#eselon1 option:selected').attr('value');
        var eselon2    = $('#eselon2 option:selected').attr('value');
        var golongan1    = $('#golongan1 option:selected').attr('value');
        var golongan2    = $('#golongan2 option:selected').attr('value');
        
        $('#list_data_table').dataTable( {
            "processing": true,
            "serverSide": true,
            "bServerSide": true,
            "ordering": true,
            "destroy": true,
            "sAjaxSource": base_url+"laporan/daftar_urut_kepangkatan/list_data?unker1="+unker1+"&unker2="+unker2+"&eselon1="+eselon1+"&eselon2="+eselon2+"&golongan1="+golongan1+"&golongan2="+golongan2  ,
            "aaSorting": [],
            "order": [],
            "iDisplayLength": 10,
            "aoColumns": [
                { "bSortable": false, "sClass": "text-center" },
                { "bSortable": false, "sClass": "text-left" },
                { "bSortable": false, "sClass": "text-left" },
                { "bSortable": false, "sClass": "text-left" },
                { "bSortable": false, "sClass": "text-left" },
                { "bSortable": false, "sClass": "text-left" },
                { "bSortable": false, "sClass": "text-left" },
                { "bSortable": false, "sClass": "text-left" },
                { "bSortable": false, "sClass": "text-left" },
                { "bSortable": false, "sClass": "text-left" },
                { "bSortable": false, "sClass": "text-left" },
                { "bSortable": false, "sClass": "text-left" },
                { "bSortable": false, "sClass": "text-center" },
                { "bSortable": false, "sClass": "text-center" },
            ],
            "fnDrawCallback": function () {
                set_default_datatable();
            },
        });
    });

</script>
<script type="text/javascript"> 
    function read_preview(){
        var unker1    = $('#unker1 option:selected').attr('value');
        var unker2    = $('#unker2 option:selected').attr('value');
        var eselon1    = $('#eselon1 option:selected').attr('value');
        var eselon2    = $('#eselon2 option:selected').attr('value');
        var golongan1    = $('#golongan1 option:selected').attr('value');
        var golongan2    = $('#golongan2 option:selected').attr('value');
        
        var win = window.open("../laporan/daftar_urut_kepangkatan/preview?unker1="+unker1+"&unker2="+unker2+"&eselon1="+eselon1+"&eselon2="+eselon2+"&golongan1="+golongan1+"&golongan2="+golongan2, '_blank');
        win.focus();
    }
</script>
