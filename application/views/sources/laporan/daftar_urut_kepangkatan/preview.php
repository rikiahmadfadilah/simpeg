<style type="text/css">
    table.ropeg_table thead tr {
        background-color: #ebecff;
    }
    table.ropeg_table th, table.ropeg_table td {
      font-size : 12px;
    }
    table.ropeg_table td {
     vertical-align: top !important;
    }
    table.ropeg_table tfoot tr{
      background-color: #ebecff;

    }
    table.ropeg_table{
     margin-bottom: 10px;
    }
    .btn-tfoot-table{
      padding-top: 2px;
      padding-bottom: 2px;
    }
</style>
  <div align="center">
    <b>
        <font size="3">
            DAFTAR URUT KEPANGKATAN
            <br>
            KEMENKO KEMARITIMAN
        </font>
    </b>
  </div>
  <br>
  <table class="ropeg_table" border="1" id="list_data_table" width="100%">
    <thead>
        <tr>
            <td align="center" rowspan="3"><b>NO</b></td>
            <td align="center" colspan="3"><b>PEGAWAI</b></td>
            <td align="center" colspan="2"><b>PANGKAT</b></td>
            <td align="center" colspan="2"><b>JABATAN</b></td>
            <td align="center" rowspan="3"><b>MASA KERJA GOL</b></td>
            <td align="center" colspan="2"><b>LATIHAN JABATAN</b></td>
            <td align="center" colspan="2"><b>PENDIDIKAN AKHIR</b></td>
            <td align="center" rowspan="3"><b>FOTO</b></td>
        </tr>
        <tr>
            <td align="center" rowspan="2"><b>NIP</b></td>
            <td align="center" rowspan="2"><b>NAMA</b><br><b>TEMPAT/TANGGAL LAHIR</b><br><b>STATUS PEGAWAI</b></td>
            <td align="center" rowspan="2"><b>USIA</b></td>
            <td align="center" rowspan="2"><b>GOL/<br>RU</b></td>
            <td align="center" rowspan="2"><b>TMT</b></td>
            <td align="center" rowspan="2"><b>NAMA</b></td>
            <td align="center" rowspan="2"><b>TMT</b></td>
            <td align="center" rowspan="2"><b>NAMA</b></td>
            <td align="center" rowspan="2"><b>TAHUN</b></td>
            <td align="center" rowspan="2"><b>NAMA</b>
            <td align="center" rowspan="2"><b>PROGRAM STUDI</b>
        </tr>
        <tr style="display: none;">
            <td align="center" colspan="2">
                <b>USIA</b>
            </td>
        </tr>
    </thead>
    <tbody>
      <?php $no = 1; foreach ($preview as $key => $p) { ?>
      <tr>
        <td><?php echo $no ?></td>
        <td><?php echo $p['NIP'] ?></td>
        <td><?php echo $p['NAMA'] ?><br><?php echo $p['TMP_TGL_LAHIR'] ?><br><?php echo $p['STAT_KEPEG'] ?></td>
        <td><?php echo $p['USIA'] ?></td>
        <td><?php echo $p['GOLONGAN_NAMA'] ?></td>
        <td><?php echo dateEnToId($p["TMT_GOL"], 'd-m-Y') ?></td>
        <td><?php echo $p['NAMA_JAB'] ?></td>
        <td><?php echo dateEnToId($p["TMT_JAB"], 'd-m-Y') ?></td>
        <td>2</td>
        <td><?php echo $p['NAMA_DIKLAT'] ?></td>
        <td><?php echo dateEnToId($p["TGL_DIKLAT"], 'Y') ?></td>
        <td><?php echo $p['NAMA_PENDIDIKAN'] ?></td>
        <td><?php echo $p['PRO_STUDI'] ?></td>
        <td><img src="<?php echo base_url();?>assets/images/pegawai/<?php echo $p["PHOTO"] ?>"  alt="PHOTO" name="photo" width="70" height="80" align="left" id="photo"></td>
      </tr>
      <?php $no++; } ?>
    </tbody>
</table>