<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Rekapitulasi Jenis Kelamin dan Usia</h6>
		<div class="heading-elements">
            <a href="javascript:read_preview()" class="btn bg-slate btn-raised legitRipple">PRINT PREVIEW</a>
		</div>
	</div>

	<div class="panel-body">
        <table border="1" bordercolor="black" width="100%" cellspacing="1" cellpadding="1" id="table2">
        <tbody>
        <tr bgcolor="#006200">
            <td width="5%" rowspan="2" align="center">
                <font color="#FFFFFF"><b>NO</b></font>
            </td>
            <td width="20%" rowspan="2">
                <font color="#FFFFFF"><b>UNIT KERJA</b></font>
            </td>
            <td width="20%" rowspan="2">
                <font color="#FFFFFF"><b>JENIS KELAMIN</b></font>
            </td>
            <td width="40%" colspan="5">
                <p align="center">
                    <font color="#FFFFFF"><b>USIA</b></font>
                </p>
            </td>
            <td width="15%" rowspan="2">
                <p align="center">
                    <font color="#FFFFFF"><b>JUMLAH</b></font>
                </p>
            </td>
        </tr>
        <tr bgcolor="#006200">
            <td align="center" width="8%">
                <font color="#FFFFFF"><b>&gt;56</b></font>
            </td>
            <td align="center" width="8%">
                <font color="#FFFFFF"><b>46-55</b></font>
            </td>
            <td align="center" width="8%">
                <font color="#FFFFFF"><b>36-45</b></font>
            </td>
            <td align="center" width="8%">
                <font color="#FFFFFF"><b>26-35</b></font>
            </td>
            <td align="center" width="8%">
                <font color="#FFFFFF"><b>&lt;25</b></font>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FFFFFF" width="100%" colspan="9"></td>
        </tr>
        <?php 
        $jml_56 = 0;
        $jml_46_55 = 0;
        $jml_36_45 = 0;
        $jml_26_35 = 0;
        $jml_25 = 0;
        $jml = 0;

        $no=1; foreach ($rekap_usia as $ru){?> 
        <tr bgcolor="#FFAC59">
        <tr bgcolor="#9BFF9B">
            <td width="3%" align="center">
                <b><?php echo $no?></b>
            </td>
            <td width="97%" colspan="8">
                <b><?php echo $ru['nama_unker']?></b>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FFFFFF" colspan="2" rowspan="3">&nbsp;</td>
            <td bgcolor="#FFFFFF">LAKI-LAKI</td>
            <td bgcolor="#FFFFFF" align="center"><?php echo ($ru['lk_56']?$ru['lk_56']:0)?></td>
            <td bgcolor="#FFFFFF" align="center"><?php echo ($ru['lk_46_55']?$ru['lk_46_55']:0)?></td>
            <td bgcolor="#FFFFFF" align="center"><?php echo ($ru['lk_36_45']?$ru['lk_36_45']:0)?></td>
            <td bgcolor="#FFFFFF" align="center"><?php echo ($ru['lk_26_35']?$ru['lk_26_35']:0)?></td>
            <td bgcolor="#FFFFFF" align="center"><?php echo ($ru['lk_25']?$ru['lk_25']:0)?></td>
            <td bgcolor="#FFFFFF" align="center">
                <b><?php echo ($ru['lk_jml']?$ru['lk_jml']:0)?><b></b></b>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FFFFFF">PEREMPUAN</td>
            <td bgcolor="#FFFFFF" align="center"><?php echo ($ru['pr_56']?$ru['pr_56']:0)?></td>
            <td bgcolor="#FFFFFF" align="center"><?php echo ($ru['pr_46_55']?$ru['pr_46_55']:0)?></td>
            <td bgcolor="#FFFFFF" align="center"><?php echo ($ru['pr_36_45']?$ru['pr_36_45']:0)?></td>
            <td bgcolor="#FFFFFF" align="center"><?php echo ($ru['pr_26_35']?$ru['pr_26_35']:0)?></td>
            <td bgcolor="#FFFFFF" align="center"><?php echo ($ru['pr_25']?$ru['pr_25']:0)?></td>
            <td bgcolor="#FFFFFF" align="center">
                <b><?php echo ($ru['pr_jml']?$ru['pr_jml']:0)?><b></b></b>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FFFFFF">JUMLAH</td>
            <td bgcolor="#FFFFFF" align="center"><?php echo ($ru['jml_56']?$ru['jml_56']:0)?></td>
            <td bgcolor="#FFFFFF" align="center"><?php echo ($ru['jml_46_55']?$ru['jml_46_55']:0)?></td>
            <td bgcolor="#FFFFFF" align="center"><?php echo ($ru['jml_36_45']?$ru['jml_36_45']:0)?></td>
            <td bgcolor="#FFFFFF" align="center"><?php echo ($ru['jml_26_35']?$ru['jml_26_35']:0)?></td>
            <td bgcolor="#FFFFFF" align="center"><?php echo ($ru['jml_25']?$ru['jml_25']:0)?></td>
            <td bgcolor="#006200" align="center">
                <font color="#FFFFFF"><b><?php echo ($ru['jml']?$ru['jml']:0)?><b></b></b>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FFFFFF" width="100%" colspan="9"></td>
        </tr>
        <?php 
        $jml_56 += $ru['jml_56'];
        $jml_46_55 += $ru['jml_46_55'];
        $jml_36_45 += $ru['jml_36_45'];
        $jml_26_35 += $ru['jml_26_35'];
        $jml_25 += $ru['jml_25'];
        $jml += $ru['jml'];
        ?>
        <?php $no++; } ?>
        <tr>
            <td bgcolor="#E8E8E8" colspan="3">
                <b>JUMLAH</b>
            </td>
            <td bgcolor="#000" align="center"><font color="#FFFFFF"><?php echo $jml_56?></td>
            <td bgcolor="#000" align="center"><font color="#FFFFFF"><?php echo $jml_46_55?></td>
            <td bgcolor="#000" align="center"><font color="#FFFFFF"><?php echo $jml_36_45?></td>
            <td bgcolor="#000" align="center"><font color="#FFFFFF"><?php echo $jml_26_35?></td>
            <td bgcolor="#000" align="center"><font color="#FFFFFF"><?php echo $jml_25?></td>
            <td bgcolor="#000" align="center"><font color="#FFFFFF">
                <b><?php echo $jml?><b></b></b>
            </td>
        </tr>
        </tbody>
        </table>
	</div>
</div>

<script type="text/javascript"> 
    function read_preview(){
        var win = window.open("../laporan/rekap_usia/preview", '_blank');
        win.focus();
    }
</script>