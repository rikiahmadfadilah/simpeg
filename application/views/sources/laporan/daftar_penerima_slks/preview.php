<style type="text/css">
    table.ropeg_table thead tr {
        background-color: #ebecff;
    }
    table.ropeg_table th, table.ropeg_table td {
      font-size : 12px;
    }
    table.ropeg_table td {
     vertical-align: top !important;
    }
    table.ropeg_table tfoot tr{
      background-color: #ebecff;

    }
    table.ropeg_table{
     margin-bottom: 10px;
    }
    .btn-tfoot-table{
      padding-top: 2px;
      padding-bottom: 2px;
    }
</style>
<title>DAFTAR PEGAWAI PENERIMA SLKS</title>
    <div align="center">
    <b>
        <font size="3">
            DAFTAR PEGAWAI PENERIMA SLKS 
            <br>
            KEMENKO KEMARITIMAN
        </font>
    </b>
    </div>
    <br>
    <table class="ropeg_table" border="1" id="" width="100%">
	<thead>
        <tr>
            <td align="center" width="2%" rowspan="2">
                <b>NO</b>
            </td>
            <td align="center" width="18%" rowspan="2">
                <b>NAMA PEGAWAI<br>TEMPAT/TANGGAL LAHIR</b>
            </td>
            <td align="center" width="3%" rowspan="2">
                <b>NIP</b>
            </td>
            <td align="center" colspan="2">
                <b>PANGKAT</b>
            </td>
            <td align="center" colspan="2">
                <b>JABATAN</b>
            </td>
            <td align="center" rowspan="2">
                <b>MASA KERJA KESELURUHAN</b>
            </td>
            <td align="center" rowspan="2">
                <b>RIWAYAT PENGHARGAAN</b>
            </td>
        </tr>
        <tr>
            <td align="center" width="5%">
                <b>GOL/<br>RU</b>
            </td>
            <td align="center" width="7%">
                <b>TMT</b>
            </td>
            <td align="center" width="18%">
                <b>NAMA</b>
            </td>
            <td align="center" width="7%">
                <b>TMT</b>
            </td>
        </tr>
    </thead>
    <tbody>
    <?php $no=1; foreach ($preview as $p){ ?>              
        <tr>
            <td><?php echo $no?></td>
            <td><?php echo $p['NAMA']?><br><?php echo $p['TMP_TGL_LAHIR']?></td>
            <td><?php echo $p['NIP']?></td>
            <td><?php echo $p['GOLONGAN_NAMA']?><br><?php echo dateEnToId($p['TMT_GOL'], 'd-m-Y');?></td>
            <td><?php echo $p['PANGKAT']?><br><?php echo dateEnToId($p['TMT_GOL'], 'd-m-Y');?></td>
            <td><?php echo $p['NAMA_JAB']?><?php echo $p['NAMA_JABFUNG']?></td>
            <td><?php echo dateEnToId($p['TMT_JAB'], 'd-m-Y');?><br><?php echo dateEnToId($p['TMT_JABFUNG'], 'd-m-Y');?></td>
            <td class="text-center"><?php echo $p['MASA_KERJA_KESELURUHAN_NAME']?></td>
            <td></td>
         </tr>
    <?php $no++; } ?>
    </tbody>
</table>
	
	
</table>