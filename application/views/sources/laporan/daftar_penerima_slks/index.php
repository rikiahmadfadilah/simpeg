
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Daftar Pegawai Penerima SLKS</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-xs unker1">
                        <label class="col-md-3 control-label">Unit Kerja</label>
                        <div class="col-md-7">
                            <select name="unker1" id="unker1" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs">
                                <!-- <option value=""></option> -->
                                <?php foreach ($unker as $i) {
                                    echo '<option value="'.$i["kode_unker"].'" parent_id_kode="'.$i["idparent"].'">'.$i["nama_unker"].'</option>';
                                }?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-group-xs">
                        <label class="col-md-3 control-label">Masa Kerja Mulai CPNS</label>
                        <div class="col-md-3">
                        <select name="masa_kerja_1" id="masa_kerja_1" data-placeholder="Golongan" data-id="0" class="select-size-xs">
                            <option value="10">10</option>
                            <?php
                                for($i=9;$i<=40;$i++) {
                                    echo '<option value="'.$i.'">'.$i.'</option>';
                                }
                            ?>
                        </select>
                        <span class="help-block">Tahun</span>
                        </div>
                        <div class="col-md-1">
                            <div class="text-center">
                                <b>S/D</b>
                            </div>
                        </div>
                        <div class="col-md-3">
                        <select name="masa_kerja_2" id="masa_kerja_2" data-placeholder="Golongan" data-id="0" class="select-size-xs">
                            <option value="10">10</option>
                            <?php
                                for($i=9;$i<=40;$i++) {
                                    echo '<option value="'.$i.'">'.$i.'</option>';
                                }
                            ?>
                        </select>
                        <span class="help-block">Tahun</span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs">
                        <label class="col-md-3 control-label">Per Tanggal</label>
                        <div class="col-md-9">
                            <input type="text" name="tanggal" id="tanggal" class="form-control input-xs wajib picktgl" value="<?php echo date('d-m-Y'); ?>">
                        </div>
                    </div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3" id="tampil">
					<a href="javascript:void(0);" class="btn btn-xs btn-primary">Lihat</a>
                    <input id="filter" name="filter" type="hidden" min="1900" max="2050" class="form-control input-xs wajib">
				</div>
			</div>
			</br>
			<div id="list_data" style="display: none;">
				<legend class="text-bold" style="padding-bottom:5px;border-bottom:solid 1px #00bcd4;"></legend>
				<div class="center" align="right">
                    <a href="javascript:read_preview()" class="btn bg-slate btn-raised legitRipple">PRINT PREVIEW</a>
                </div>
                <table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive epagawai_table" id="list_data_table">
                	<thead>
                        <tr>
                            <td align="center" width="2%" rowspan="2"><b>NO</b></td>
                            <td align="center" width="18%" rowspan="2"><b>NAMA PEGAWAI<br>TEMPAT/TANGGAL LAHIR</b></td>
                            <td align="center" width="3%" rowspan="2"><b>NIP</b></td>
                            <td align="center" colspan="2"><b>PANGKAT</b></td>
                            <td align="center" colspan="2"><b>JABATAN</b></td>
                            <td align="center" rowspan="2"><b>MASA KERJA KESELURUHAN</b></td>
                            <!-- <td align="center" colspan="2"><b>SKLS TERAKHIR<br>(DATA DASAR)</b></td> -->
                            <td align="center" rowspan="2"><b>RIWAYAT PENGHARGAAN</b></td>
                        </tr>
                        <tr>
                            <td align="center" width="5%"><b>GOL/<br>RU</b></td>
                            <td align="center" width="4%"><b>TMT</b></td>
                            <td align="center" width="6%"><b>NAMA</b></td>
                            <td align="center" width="4%"><b>TMT</b></td>
                            <!-- <td align="center" width="4%"><b>THN</b></td>
                            <td align="center" width="4%"><b>BLN</b></td>
                            <td align="center" width="6%"><b>NAMA</b></td>
                            <td align="center" width="7%"><b>TAHUN</b></td> -->
                        </tr>
                    </thead>
                </table>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">
    $('#tampil').click(function(){
        $("#list_data").show();
        
        var unker1    = $('#unker1 option:selected').attr('value');
        
        if(unker1)var data  = unker1;
        else var data  = '';

        var masa_kerja_1  = $('#masa_kerja_1 option:selected').attr('value');
        var masa_kerja_2  = $('#masa_kerja_2 option:selected').attr('value');

        if(masa_kerja_1)var data  = data + '-' +masa_kerja_1;
        else var data  = data + '-';
        if(masa_kerja_2)var data  = data + '_' +masa_kerja_2;
        else var data  = data + '_';

        // var tanggal_1  = $('#tanggal').val();
        // var tanggal_2  = $('#tanggal').val();

        // if(tanggal_1)var data  = data + '-' +tanggal_1;
        // else var data  = data + '-';
        // if(tanggal_2)var data  = data + '_' +tanggal_2;
        // else var data  = data + '_';

        $('#filter').val(data);
        $('#list_data_table').dataTable( {
            "processing": true,
            "serverSide": true,
            "bServerSide": true,
            "destroy": true,
            "sAjaxSource": base_url+"laporan/daftar_penerima_slks/list_data/"+data,
            "aaSorting": [],
            "order": [],
            "iDisplayLength": 10,
            "aoColumns": [
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-center" },
            { "sClass": "text-left" },
            ],
            "fnDrawCallback": function () {
                set_default_datatable();
            },
        });
    });
    $('.picktgl').pickadate({
            monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
            weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
            selectMonths: true,
            selectYears: 80,
            max: true,
            today:'Hari ini',
            clear: 'Hapus',
            close: 'Keluar',
            format: 'dd-mm-yyyy',
            formatSubmit: 'yyyy-mm-dd'
        });
    function read_preview(){
        var filter         = $('#filter').val();
        var win = window.open("../laporan/daftar_penerima_slks/preview/"+filter, '_blank');
        win.focus();
        
    }
</script>