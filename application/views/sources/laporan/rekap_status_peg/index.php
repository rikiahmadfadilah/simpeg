<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Rekapitulasi Jenis Kelamin dan Status Pegawai</h6>
		<div class="heading-elements">
            <a href="javascript:read_preview()" class="btn bg-slate btn-raised legitRipple">PRINT PREVIEW</a>
		</div>
	</div>

	<div class="panel-body">
		<tr>
            <td>
                <table border="1" bordercolor="black" width="100%" cellspacing="1" cellpadding="1" id="table2">
                <tbody>
                <tr bgcolor="#993300">
                    <td width="5%" rowspan="2" align="center">
                        <font color="#FFFFFF"><b>NO</b></font>
                    </td>
                    <td width="7%" rowspan="2">
                        <font color="#FFFFFF"><b>UNIT KERJA</b></font>
                    </td>
                    <td width="13%" rowspan="2">
                        <font color="#FFFFFF"><b>JENIS KELAMIN</b></font>
                    </td>
                    <td width="65%" colspan="5">
                        <p align="center">
                            <font color="#FFFFFF"><b>STATUS</b></font>
                        </p>
                    </td>
                    <td width="10%" rowspan="2">
                        <p align="center">
                            <font color="#FFFFFF"><b>JUMLAH</b></font>
                        </p>
                    </td>
                </tr>
                <tr bgcolor="#993300">
                    <td align="center" width="14%">
                        <font color="#FFFFFF"><b>CPNS</b></font>
                    </td>
                    <td align="center" width="14%">
                        <font color="#FFFFFF"><b>PNS</b></font>
                    </td>
                    <td align="center" width="18%">
                        <font color="#FFFFFF"><b>PNS DPK</b></font>
                    </td>
                    <td align="center" width="19%">
                        <font color="#FFFFFF"><b>PNS DPB</b></font>
                    </td>
                    <td align="center" width="19%">
                        <font color="#FFFFFF"><b>DLL</b></font>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#FFFFFF" width="100%" colspan="8"></td>
                </tr>
                <?php
                    $jml_cpns = 0;
                    $jml_pns = 0;
                    $jml_dpk = 0;
                    $jml_dpb = 0;
                    $jml_dll = 0;
                    $jml = 0;
                    $no=1; foreach ($rekap_status_peg as $rsp){?>
                <tr bgcolor="#FFAC59">
                    <td width="3%" align="center">
                        <b><?php echo $no?></b>
                    </td>
                    <td width="97%" colspan="8">
                        <b><?php echo $rsp['nama_unker']?></b>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#FFFFFF" colspan="2" rowspan="3">&nbsp;</td>
                    <td bgcolor="#FFFFFF">LAKI-LAKI</td>
                    <td bgcolor="#FFFFFF" align="center"><?php echo ($rsp['lk_cpns']?$rsp['lk_cpns']:0)?></td>
                    <td bgcolor="#FFFFFF" align="center"><?php echo ($rsp['lk_pns']?$rsp['lk_pns']:0)?></td>
                    <td bgcolor="#FFFFFF" align="center"><?php echo ($rsp['lk_dpk']?$rsp['lk_dpk']:0)?></td>
                    <td bgcolor="#FFFFFF" align="center"><?php echo ($rsp['lk_dpb']?$rsp['lk_dpb']:0)?></td>
                    <td bgcolor="#FFFFFF" align="center"><?php echo ($rsp['lk_dll']?$rsp['lk_dll']:0)?></td>
                    <td bgcolor="#FFFFFF" align="center">
                        <b><?php echo ($rsp['jml_lk']?$rsp['jml_lk']:0)?><b></b></b>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#FFFFFF">PEREMPUAN</td>
                    <td bgcolor="#FFFFFF" align="center"><?php echo ($rsp['pr_cpns']?$rsp['pr_cpns']:0)?></td>
                    <td bgcolor="#FFFFFF" align="center"><?php echo ($rsp['pr_pns']?$rsp['pr_pns']:0)?></td>
                    <td bgcolor="#FFFFFF" align="center"><?php echo ($rsp['pr_dpk']?$rsp['pr_dpk']:0)?></td>
                    <td bgcolor="#FFFFFF" align="center"><?php echo ($rsp['pr_dpb']?$rsp['pr_dpb']:0)?></td>
                    <td bgcolor="#FFFFFF" align="center"><?php echo ($rsp['pr_dll']?$rsp['pr_dll']:0)?></td>
                    <td bgcolor="#FFFFFF" align="center">
                        <b><?php echo ($rsp['jml_pr']?$rsp['jml_pr']:0)?><b></b></b>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#FFFFFF">JUMLAH</td>
                    <td bgcolor="#FFFFFF" align="center"><?php echo ($rsp['jml_cpns']?$rsp['jml_cpns']:0)?></td>
                    <td bgcolor="#FFFFFF" align="center"><?php echo ($rsp['jml_pns']?$rsp['jml_pns']:0)?></td>
                    <td bgcolor="#FFFFFF" align="center"><?php echo ($rsp['jml_dpk']?$rsp['jml_dpk']:0)?></td>
                    <td bgcolor="#FFFFFF" align="center"><?php echo ($rsp['jml_dpb']?$rsp['jml_dpb']:0)?></td>
                    <td bgcolor="#FFFFFF" align="center"><?php echo ($rsp['jml_dll']?$rsp['jml_dll']:0)?></td>
                    <td align="center" bgcolor="#993300">
                        <b><font color="#FFFFFF"><?php echo ($rsp['jml']?$rsp['jml']:0)?><b></b></font><b></b></b>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#FFFFFF" width="100%" colspan="8"></td>
                </tr>
                <?php
                    $jml_cpns += $rsp['jml_cpns'];
                    $jml_pns += $rsp['jml_pns'];
                    $jml_dpk += $rsp['jml_dpk'];
                    $jml_dpb += $rsp['jml_dpb'];
                    $jml_dll += $rsp['jml_dll'];
                    $jml += $rsp['jml'];
                    $no++; } ?>
               <tr>
                    <td bgcolor="#E8E8E8" colspan="3" rowspan="3">
                        <b>JUMLAH</b>
                    </td>
                    <td bgcolor="#000000" align="center">
                        <font color="#FFFFFF"><b><?php echo $jml_cpns ?><b></b></b></font>
                    </td>
                    <td bgcolor="#000000" align="center">
                        <font color="#FFFFFF"><b><?php echo $jml_pns ?><b></b></b></font>
                    </td>
                    <td bgcolor="#000000" align="center">
                        <font color="#FFFFFF"><b><?php echo $jml_dpk ?><b></b></b></font>
                    </td>
                    <td bgcolor="#000000" align="center">
                        <font color="#FFFFFF"><b><?php echo $jml_dpb ?><b></b></b></font>
                    </td>
                    <td align="center" bgcolor="#000000">
                        <font color="#FFFFFF"><b><?php echo $jml_dll ?><b></b></b></font>
                    </td>
                    <td align="center" bgcolor="#000000">
                        <font color="#FFFFFF"><b><?php echo $jml ?><b></b></b></font>
                    </td>
                </tr>
                </tbody>
                </table>
            </td>
        </tr>
	</div>
</div>

<script type="text/javascript"> 
    function read_preview(){
        var win = window.open("../laporan/rekap_status_pegawai/preview", '_blank');
        win.focus();
    }
</script>