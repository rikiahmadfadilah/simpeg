<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Rekapitulasi Jenis Kelamin dan Golongan</h6>
		<div class="heading-elements">
            <a href="javascript:read_preview()" class="btn bg-slate btn-raised legitRipple">PRINT PREVIEW</a>
		</div>
	</div>

	<div class="panel-body">
		<!-- <table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive ropeg_table" id="list_data">
			<thead>
				<tr bgcolor="#336699">
                    <td width="5%" rowspan="2" align="center"><font color="#FFFFFF"><b>NO</b></font></td>
                    <td width="20%" rowspan="2"><font color="#FFFFFF"><b>UNIT KERJA</b></font></td>
                    <td width="20%" rowspan="2"><font color="#FFFFFF"><b>JENIS KELAMIN</b></font></td>
                    <td width="40%" colspan="4">
                    <p align="center"><font color="#FFFFFF"><b>GOLONGAN</b></font></p></td>
                    <td width="15%" rowspan="2">
                    <p align="center"><font color="#FFFFFF"><b>JUMLAH</b></font></p></td>
                </tr>
				<tr>
					<th class="text-center"style="width: 175px;min-width: 175px;">NIP</th>
					<th class="text-center">Nama</th>
					<th class="text-center">Tempat/Tanggal Lahir</th>
					<th class="text-center">Nama Jabatan</th>
					<th class="text-center">Unit Kerja</th>
					<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
				</tr>
			</thead>
			
		</table> -->
		<table border="1" bordercolor="black" width="100%" cellspacing="1" cellpadding="1" id="table2">
            <tbody>
            	<tr bgcolor="#336699">
                    <td width="5%" rowspan="2" align="center"><font color="#FFFFFF"><b>NO</b></font></td>
                    <td width="20%" rowspan="2"><font color="#FFFFFF"><b>UNIT KERJA</b></font></td>
                    <td width="20%" rowspan="2"><font color="#FFFFFF"><b>JENIS KELAMIN</b></font></td>
                    <td width="40%" colspan="4"><p align="center"><font color="#FFFFFF"><b>GOLONGAN</b></font></p></td>
                    <td width="15%" rowspan="2"><p align="center"><font color="#FFFFFF"><b>JUMLAH</b></font></p></td>
                </tr>
                <tr bgcolor="#336699">
                        <td align="center" width="8%"><font color="#FFFFFF"><b>I</b></font></td>
                        <td align="center" width="8%"><font color="#FFFFFF"><b>II</b></font></td>
                        <td align="center" width="8%"><font color="#FFFFFF"><b>III</b></font></td>
                        <td align="center" width="8%"><font color="#FFFFFF"><b>IV</b></font></td>
                </tr>
                <?php 
					$jml_gol_1 = 0;
                	$jml_gol_2 = 0;
                	$jml_gol_3 = 0;
                	$jml_gol_4 = 0;
                	$jml_gol = 0;
                $no = 1; foreach ($rekap_gol as $rg) { ?>
                <tr>
                    <td></td>
                </tr>
                <tr bgcolor="#00ADD9">
                    <td width="3%" align="center"><b><?php echo $no; ?></b></td>
                    <td width="97%" colspan="7"><b><?php echo $rg['nama_unker'] ?></b></td>
                </tr>
                <tr>
                    <td bgcolor="#FFFFFF" colspan="2" rowspan="3">&nbsp;</td>
                    <td bgcolor="#FFFFFF">LAKI-LAKI</td>
                    <td bgcolor="#FFFFFF" align="center"><?php echo $rg['lk_gol_1'] ?></td>
                    <td bgcolor="#FFFFFF" align="center"><?php echo $rg['lk_gol_2'] ?></td>
                    <td bgcolor="#FFFFFF" align="center"><?php echo $rg['lk_gol_3'] ?></td>
                    <td bgcolor="#FFFFFF" align="center"><?php echo $rg['lk_gol_4'] ?></td>
                    <td bgcolor="#FFFFFF" align="center"><b><?php echo $rg['lk_gol_jml'] ?></b></td>
                </tr>
                <tr>
                    <td bgcolor="#FFFFFF">PEREMPUAN</td>
                    <td bgcolor="#FFFFFF" align="center"><?php echo $rg['pr_gol_1'] ?></td>
                    <td bgcolor="#FFFFFF" align="center"><?php echo $rg['pr_gol_2'] ?></td>
                    <td bgcolor="#FFFFFF" align="center"><?php echo $rg['pr_gol_3'] ?></td>
                    <td bgcolor="#FFFFFF" align="center"><?php echo $rg['pr_gol_4'] ?></td>
                    <td bgcolor="#FFFFFF" align="center"><b><?php echo $rg['pr_gol_jml'] ?></b></td>
                </tr>
                <tr>
                    <td bgcolor="#FFFFFF">JUMLAH</td>
                    <td bgcolor="#FFFFFF" align="center"><b><?php echo $rg['jml_gol_1'] ?></b></td>
                    <td bgcolor="#FFFFFF" align="center"><b><?php echo $rg['jml_gol_2'] ?></b></td>
                    <td bgcolor="#FFFFFF" align="center"><b><?php echo $rg['jml_gol_3'] ?></b></td>
                    <td bgcolor="#FFFFFF" align="center"><b><?php echo $rg['jml_gol_4'] ?></b></td>
                    <td align="center" bgcolor="#336699"><b><font color="#FFFFFF"><?php echo $rg['jml_gol'] ?><b></b></font></b></td>
                </tr>
                <tr>
                    <td bgcolor="#FFFFFF" width="100%" colspan="8"></td>
                </tr>
                <?php 
                	$jml_gol_1 += $rg['jml_gol_1'];
                	$jml_gol_2 += $rg['jml_gol_2'];
                	$jml_gol_3 += $rg['jml_gol_3'];
                	$jml_gol_4 += $rg['jml_gol_4'];
                	$jml_gol +=  $rg['jml_gol'];
                ?>
            	<?php $no++;} ?>
                <tr>
                    <td bgcolor="#E8E8E8" colspan="3" rowspan="3"><b>JUMLAH</b></td>
                    <td bgcolor="#000000" align="center"><font color="#FFFFFF"><b><?php echo $jml_gol_1; ?></b></font></td>
                    <td bgcolor="#000000" align="center"><font color="#FFFFFF"><b><?php echo $jml_gol_2; ?></b></font></td>
                    <td bgcolor="#000000" align="center"><font color="#FFFFFF"><b><?php echo $jml_gol_3; ?></b></font></td>
                    <td bgcolor="#000000" align="center"><font color="#FFFFFF"><b><?php echo $jml_gol_4; ?></b></font></td>
                    <td align="center" bgcolor="#000000"><font color="#FFFFFF"><b><?php echo $jml_gol; ?></b></font></td>
                </tr>
                </tbody></table>
	</div>
</div>

<script type="text/javascript"> 
    function read_preview(){
        var win = window.open("../laporan/rekap_gol/preview", '_blank');
        win.focus();
    }
</script>