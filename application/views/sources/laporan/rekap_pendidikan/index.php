<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Rekapitulasi Jenis Kelamin dan Tingkat Pendidikan</h6>
		<div class="heading-elements">
            <a href="javascript:read_preview()" class="btn bg-slate btn-raised legitRipple">PRINT PREVIEW</a>
		</div>
	</div>

	<div class="panel-body">
        <table border="1" bordercolor="black" width="100%" cellspacing="0" id="table7">
            <tbody>
                <tr>
                    <td>
                        <table border="1" bordercolor="black" width="100%" cellspacing="1" cellpadding="5" id="table2">
                            <tbody>
                                <tr>
                                    <td bgcolor="#0099CC" rowspan="2" width="5%" align="center">
                                        <b>NO</b>
                                    </td>
                                    <td bgcolor="#0099CC" rowspan="2" width="5%">
                                        <b>UNIT KERJA</b>
                                    </td>
                                    <td bgcolor="#0099CC" rowspan="2" width="5%">
                                        <b>JENIS KELAMIN</b>
                                    </td>
                                    <td bgcolor="#0099CC" colspan="11" align="center" width="77%">
                                        <b>PENDIDIKAN</b>
                                    </td>
                                    <td bgcolor="#0099CC" width="8%" rowspan="2">
                                        <b>JUMLAH</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td bgcolor="#0099CC" width="7%" align="center">
                                        <b>S3</b>
                                    </td>
                                    <td bgcolor="#0099CC" width="7%" align="center">
                                        <b>S2</b>
                                    </td>
                                    <td bgcolor="#0099CC" width="7%" align="center">
                                        <b>S1</b>
                                    </td>
                                    <td bgcolor="#0099CC" width="7%" align="center">
                                        <b>D4</b>
                                    </td>
                                    <td bgcolor="#0099CC" width="7%" align="center">
                                        <b>SM</b>
                                    </td>
                                    <td bgcolor="#0099CC" width="7%" align="center">
                                        <b>D3</b>
                                    </td>
                                    <td bgcolor="#0099CC" width="7%" align="center">
                                        <b>D2</b>
                                    </td>
                                    <td bgcolor="#0099CC" width="7%" align="center">
                                        <b>D1</b>
                                    </td>
                                    <td bgcolor="#0099CC" width="7%" align="center">
                                        <b>SLTA</b>
                                    </td>
                                    <td bgcolor="#0099CC" width="7%" align="center">
                                        <b>SLTP</b>
                                    </td>
                                    <td bgcolor="#0099CC" width="7%" align="center">
                                        <b>SD</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="15" bgcolor="#FFFFFF" height="1"></td>
                                </tr>
                                <?php
                                $jml_s3 = 0;
                                $jml_s2 = 0;
                                $jml_s1 = 0;
                                $jml_d4 = 0;
                                $jml_sm = 0;
                                $jml_d3 = 0;
                                $jml_d2 = 0;
                                $jml_d1 = 0;
                                $jml_slta = 0;
                                $jml_sltp = 0;
                                $jml_sd = 0;
                                $jml = 0;
                                $no = 1; foreach ($rekap_pendidikan as $rp) { ?>
                                <tr bgcolor="#FFAC59">
                                        <tr bgcolor="#0093B7">
                                            <td>
                                                <b><a href="rekappendidikanpdf.php" target="_blank"><font color="black"><?php echo $no; ?></font></a></b>
                                            </td>
                                            <td colspan="14">
                                                <b><?php echo $rp['nama_unker'] ?></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" rowspan="3" bgcolor="#FFFFFF">&nbsp;</td>
                                            <td bgcolor="#FFFFFF">LAKI-LAKI</td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['lk_s3'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['lk_s2'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['lk_s1'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['lk_d4'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['lk_sm'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['lk_d3'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['lk_d2'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['lk_d1'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['lk_slta'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['lk_sltp'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['lk_sd'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center">
                                                <b><?php echo $rp['lk_jml'] ?></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF">PEREMPUAN</td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['pr_s3'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['pr_s2'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['pr_s1'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['pr_d4'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['pr_sm'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['pr_d3'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['pr_d2'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['pr_d1'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['pr_slta'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['pr_sltp'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['pr_sd'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center">
                                                <b><?php echo $rp['pr_jml'] ?></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF">JUMLAH</td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['jml_s3'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['jml_s2'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['jml_s1'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['jml_d4'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['jml_sm'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['jml_d3'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['jml_d2'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['jml_d1'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['jml_slta'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['jml_sltp'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $rp['jml_sd'] ?></td>
                                            <td bgcolor="#FFFFFF" align="center">
                                                <b><?php echo $rp['jml'] ?></b>
                                            </td>
                                        </tr>
                                        <?php 
                                            $jml_s3 += $rp['jml_s3'];
                                            $jml_s2 += $rp['jml_s2'];
                                            $jml_s1 += $rp['jml_s1'];
                                            $jml_d4 += $rp['jml_d4'];
                                            $jml_sm += $rp['jml_sm'];
                                            $jml_d3 += $rp['jml_d3'];
                                            $jml_d2 += $rp['jml_d2'];
                                            $jml_d1 += $rp['jml_d1'];
                                            $jml_slta += $rp['jml_slta'];
                                            $jml_sltp += $rp['jml_sltp'];
                                            $jml_sd += $rp['jml_sd'];
                                            $jml += $rp['jml'];
                                        ?>
                                        <?php $no++; } ?>
                                        <tr>
                                            <td colspan="15" bgcolor="#FFFFFF" height="1"></td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#E8E8E8" colspan="3">
                                                <b>JUMLAH</b>
                                            </td>
                                            <td bgcolor="#000000" align="center">
                                                <font color="#FFFFFF"><b><?php echo $jml_s3 ?></b></font>
                                            </td>
                                            <td bgcolor="#000000" align="center">
                                                <font color="#FFFFFF"><b><?php echo $jml_s2 ?></b></font>
                                            </td>
                                            <td bgcolor="#000000" align="center">
                                                <font color="#FFFFFF"><b><?php echo $jml_s1 ?></b></font>
                                            </td>
                                            <td bgcolor="#000000" align="center">
                                                <font color="#FFFFFF"><b><?php echo $jml_d4 ?></b></font>
                                            </td>
                                            <td bgcolor="#000000" align="center">
                                                <font color="#FFFFFF"><b><?php echo $jml_sm ?></b></font>
                                            </td>
                                            <td bgcolor="#000000" align="center">
                                                <font color="#FFFFFF"><b><?php echo $jml_d3 ?></b></font>
                                            </td>
                                            <td bgcolor="#000000" align="center">
                                                <font color="#FFFFFF"><b><?php echo $jml_d2 ?></b></font>
                                            </td>
                                            <td bgcolor="#000000" align="center">
                                                <font color="#FFFFFF"><b><?php echo $jml_d1 ?></b></font>
                                            </td>
                                            <td bgcolor="#000000" align="center">
                                                <font color="#FFFFFF"><b><?php echo $jml_slta ?></b></font>
                                            </td>
                                            <td bgcolor="#000000" align="center">
                                                <font color="#FFFFFF"><b><?php echo $jml_sltp ?></b></font>
                                            </td>
                                            <td bgcolor="#000000" align="center">
                                                <font color="#FFFFFF"><b><?php echo $jml_sd ?></b></font>
                                            </td>
                                            <td bgcolor="#000000" align="center">
                                                <font color="#FFFFFF"><b><?php echo $jml ?></b></font>
                                            </td>
                                        </tr>
                                        </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                                </table>
	</div>
</div>

<script type="text/javascript"> 
    function read_preview(){
        var win = window.open("../laporan/rekap_pendidikan/preview", '_blank');
        win.focus();
    }
</script>