<style type="text/css">
    table.ropeg_table thead tr {
        background-color: #ebecff;
    }
    table.ropeg_table th, table.ropeg_table td {
      font-size : 12px;
    }
    table.ropeg_table td {
     vertical-align: top !important;
    }
    table.ropeg_table tfoot tr{
      background-color: #ebecff;

    }
    table.ropeg_table{
     margin-bottom: 10px;
    }
    .btn-tfoot-table{
      padding-top: 2px;
      padding-bottom: 2px;
    }
</style>
    <div align="center">
    <b>
        <font size="3">
            DAFTAR NOMINATIF PEGAWAI
            <br>
            KEMENKO KEMARITIMAN
        </font>
    </b>
    </div>
    <br>
    <table class="ropeg_table" border="1" id="" width="100%">
	<thead>
        <tr>
            <td>
            	<b>NO.</b>
            </td>
            <td width="23%">
            	<p align="center">
            		<b>NAMA PEGAWAI<br>NIP<br>TEMPAT/TANGGAL LAHIR<br>NOMOR KARPEG<br>KARIS/KARSU</b>
            	</p>
            </td>
            <td width="8%" valign="top">
            	<p align="center">
            		<b>JNS. KELAMIN<br>STAT-KEL<br>AGAMA</b>
            	</p>
            </td>
            <td width="7%" valign="top">
            	<p align="center">
            		<b>GOL/RU<br>TMT<br>MASA KERJA<br>GOLONGAN<br>TERAKHIR</b>
            	</p>
            </td>
            <td width="23%" align="center" valign="top">
            	<b>JAB.STRUKTURAL<br>TMT STRUKTURAL<br>MASA KERJA JAB. TERAKHIR<hr>JAB.FUNGSIONAL<br>TMT FUNGSIONAL</b>
            </td>
            <td width="10%" valign="top">
            	<p align="center"><b>PENDIDIKAN AKHIR/TAHUN<br>SEKOLAH/UNIVERSITAS<br>FAKULTAS<br>JURUSAN</b>
            	</p>
            </td>
            <td width="10%" valign="top">
            	<p align="center">
            		<b>DIKLAT PERJENJANGAN<br>NAMA DIKLAT<br>TAHUN</b></p><hr><b>LEMHANAS/ANGKATAN</b>
            </td>
            <td width="10%" valign="top">
            	<p align="center">
            		<b>TGL.CAPEG<br>MASA KERJA KESELURUHAN<br>STATUS KEPEGAWAIAN</b>
            	</p>
            </td>
            <td>
            	<p align="center">
            		<b>FOTO</b>
            	</p>
            </td>
        </tr>
	</thead>
    <tbody>
        <?php $no = 1; foreach ($preview as $key => $p) { ?>
            <?php $masa_kerja_golongan = 2; ?>
        <tr>
            <td align="center"><?php echo $no; ?></td>
            <td><?php echo $p['NAMA'] ?><br><?php echo $p['NIP'] ?><br><?php echo $p['TMP_TGL_LAHIR'] ?><br><?php echo $p['NO_KARPEG'] ?><br><?php echo $p['NO_KARIS'] ?></td>
            <td><?php echo $p['JENIS_KELAMIN'] ?><br><?php echo $p['STATUS_KAWIN'] ?><br><?php echo $p['AGAMA_NAMA'] ?></td>
            <td><?php echo $p['GOLONGAN_NAMA'] ?><br><?php echo dateEnToId($p["TMT_GOL"], 'd-m-Y') ?><br><?php echo $masa_kerja_golongan ?><br><?php echo $p['GOLONGAN_NAMA'] ?></td>
            <td><?php echo $p['NAMA_JAB'] ?><br><?php echo dateEnToId($p["TMT_JAB"], 'd-m-Y') ?><br><?php echo $p['NAMA_JABFUNG'] ?><br><?php echo dateEnToId($p["TMT_JABFUNG"], 'd-m-Y') ?></td>
            <td><?php echo $p['NAMA_PENDIDIKAN'].'/'.$p["THN_LULUS"] ?><br><?php echo $p['NAMA_SEK_PT'] ?><br><?php echo $p['PRO_STUDI'] ?></td>
            <td><?php echo $p['KATEGORI_DIKLAT_TEXT'] ?><br><?php echo $p['NAMA_DIKLAT'] ?><br><?php echo dateEnToId($p["TGL_DIKLAT"], 'Y') ?><br>/</td>
            <td><?php echo dateEnToId($p["TMT_CPNS"], 'd-m-Y') ?><br><br><?php echo $p['STAT_KEPEG'] ?></td>
            <td><img src="<?php echo base_url();?>assets/images/pegawai/<?php echo $p["PHOTO"] ?>"  alt="PHOTO" name="photo" width="70" height="80" align="left" id="photo"></td>
        </tr>
        <?php $no++; } ?>
    </tbody>
	
	
</table>