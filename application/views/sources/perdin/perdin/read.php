
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Lihat Data Perjalanan Dinas</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">No SPT</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $perdin["perdin_no_sp"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">NIP Pejabat Pembuat Komitmen</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $perdin["ppk_nip"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Nama Pejabat Pembuat Komitmen</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $perdin["ppk_nama"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">NIP Pegawai</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $perdin["pegawai_nip"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Nama Pegawai</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $perdin["pegawai_nama"];?>" readonly="readonly">
						</div>
					</div>
					<!-- <div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Pangkat dan Golongan</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $perdin["golongan_nama"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Jabatan/Instansi</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $perdin["jabatan_nama"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Tingkat Biaya Perjalanan Dinas</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $perdin["perdin_tingkat_biaya"];?>" readonly="readonly">
						</div>
					</div> -->
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Maksud Perjalanan Dinas</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $perdin["perdin_maksud_tujuan"];?>" readonly="readonly">
						</div>
					</div>
					<!-- <div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Alat angkutan yang dipergunakan</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $perdin["perdin_alat_angkut"];?>" readonly="readonly">
						</div>
					</div> -->
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Tempat Berangkat</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $perdin["perdin_berangkat_kota_nama"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Tempat Tujuan</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $perdin["perdin_tujuan_kota_nama"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Dari Tanggal</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo dateEnToId($perdin["perdin_tanggal_mulai"], 'd F Y');?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Sampai Tanggal</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo dateEnToId($perdin["perdin_tanggal_selesai"], 'd F Y');?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Lama Perjalanan Dinas (Hari)</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $jumlah_perdin;?>" readonly="readonly">
						</div>
					</div>
					<!-- <div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Pembebanan Anggaran Instansi</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $perdin["perdin_anggaran_instansi"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Pembebanan Anggaran Akun</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $perdin["perdin_anggaran_akun"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Keterangan lain-lain</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $perdin["perdin_anggaran_keterangan"];?>" readonly="readonly">
						</div>
					</div> -->
					<!-- <div class="form-group">
						<label class="col-lg-3 control-label">Pengikut</label>
					        <div class="col-lg-9">
					        	<input type="button" style="width: 80px;min-width: 80px;" class="add-row" value="Add Row">
								<table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive epagawai_table">
					        		<thead>
							            <tr>
							                <th>NIP</th>
							                <th>Nama</th>
							                <th>Tanggal Lahir</th>
							                <th>Keterangan</th>
							            </tr>
							        </thead>
							        <tbody>
							        	<?php foreach ($pengikut as $p) { ?>
							            <tr>
							            	<td><?php echo $p["perdin_detail_pegawai_nip"]; ?></td>
							            	<td><?php echo $p["pegawai_nama"]; ?></td>
							            	<td><?php echo dateEnToID($p["pegawai_tanggal_lahir"], 'd M Y'); ?></td>
							            	<td>ket</td>
							            </tr>
							        	<?php } ?>
							        </tbody>
							    </table>
							    <button type="button" class="delete-row" style="width: 80px;min-width: 80px;">Hapus</button>
							</div>
					</div> -->
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<a href="<?php echo base_url();?>perdin/perdin" class="btn btn-xs btn-warning">Kembali</a>
					<input type="hidden" name="perdin_id" id="perdin_id"  class="form-control input-xs wajib" type="text" value="<?php echo $perdin["perdin_id"];?>" >
					
				</div>
			</div>

		</form>
	</div>
</div>
<script type="text/javascript">
	
	$(document).ready(function() {
		

	});
</script>
