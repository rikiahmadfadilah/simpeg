
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Tambah Data Perjalanan Dinas</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">No SPT</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" placeholder="No SPT" name="perdin_no_sp">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Pejabat Pembuat Komitmen</label>
						<div class="col-md-9">
							<select name="perdin_ppk_nip" id="perdin_ppk_nip" data-placeholder="Pilih Pejabat Pembuat Komitmen" class="select-size-xs wajib">
								<option value=""></option>
								<?php foreach ($pegawai as $i) {
									echo '<option value="'.$i["NIP"].'">'.$i["NIP"].'-'.$i["NAMA"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Pegawai</label>
				        <div class="col-lg-9">
				        	<input type="button" class="btn btn-xs btn-success add-row" id="addrow" value="Tambah"><br>
							<table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive ropeg_table order-list">
				        		<thead>
						            <tr>
						                <th>NIP</th>
						                <th>Jabatan</th>
						                <th>Aksi</th>
						            </tr>
						        </thead>
						        <tbody>
						            <!-- <tr>
						            	<td>
						            		<select name="perdin_ketua_nip" id="perdin_ketua_nip" data-placeholder="Pilih Pegawai" class="select-size-xs wajib">
												<option value=""></option>
												<?php foreach ($pegawai as $i) {
													echo '<option value="'.$i["pegawai_nip"].'">'.$i["pegawai_nip"].'-'.$i["pegawai_nama"].'</option>';
												}?>
											</select>
										</td>
										<td>
											<input type="text" class="form-control wajib" name="perdin_ketua_jabatan_nama[]"/>
										</td>
										<td></td>
						            </tr> -->
						        </tbody>
						    </table>
						</div>
					</div>
					<!-- <div class="form-group form-group-xs">
						<label class="col-md-3 control-label">NIP/Nama Pegawai</label>
						<div class="col-md-9">
							<select name="perdin_ketua_nip" id="perdin_ketua_nip" data-placeholder="Pilih Pegawai" class="select-size-xs wajib">
								<option value=""></option>
								<?php foreach ($pegawai as $i) {
									echo '<option value="'.$i["pegawai_nip"].'">'.$i["pegawai_nip"].'-'.$i["pegawai_nama"].'</option>';
								}?>
							</select>
						</div>
					</div> -->
					<!-- <div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Pangkat/Golongan</label>
						<div class="col-md-3">
							<select name="perdin_ketua_golongan_id" id="perdin_ketua_golongan_id" data-placeholder="Pilih Pangkat/Golongan" class="select-size-xs wajib">
								<option value=""></option>
								<?php foreach ($pangkatgolongan as $i) {
									echo '<option value="'.$i["golongan_id"].'">'.$i["golongan_nama"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Jabatan/Instansi</label>
						<div class="col-md-3">
							<select name="perdin_ketua_jabatan_id" id="perdin_ketua_jabatan_id" data-placeholder="Pilih Jabatan/Instansi" class="select-size-xs wajib">
								<option value=""></option>
								<?php foreach ($jabataninstansi as $i) {
									echo '<option value="'.$i["jabatan_id"].'">'.$i["jabatan_nama"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Tingkat Biaya Perjalanan Dinas</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" placeholder="Tingkat Biaya Perjalanan Dinas" name="perdin_tingkat_biaya" id="perdin_tingkat_biaya">
						</div>
					</div> -->
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Maksud Perjalanan Dinas</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" placeholder="Maksud Perjalanan Dinas" name="perdin_maksud_tujuan">
						</div>
					</div>
					<!-- <div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Alat angkutan yang dipergunakan</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" placeholder="Alat angkutan yang dipergunakan" name="perdin_alat_angkut">
						</div>
					</div> -->
					<!-- <div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Tempat Berangkat_kode</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" placeholder="Tempat Berangkat" name="perdin_tempat_berangkat">
						</div>
					</div> -->
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label"><b>Tempat Berangkat</b></label>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-2 control-label"></label>
						<label class="col-lg-1 control-label">Negara</b></label>
						<div class="col-lg-4">
							<select name="perdin_berangkat_negara_kode" id="perdin_berangkat_negara_kode" data-placeholder="Pilih Negara" class="select-size-xs wajib" onchange="get_provinsi(this)">
								<option></option>
								<?php foreach ($negara as $i) {

									echo '<option value="'.$i["negara_id"].'" '.(($i["negara_type"]==1)?"selected":"").' data-negara-type="'.$i["negara_type"].'">'.$i["negara_nama"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs dalam_negeri">
							<label class="col-lg-2 control-label"></label>
							<label class="col-lg-1 control-label">Provinsi</b></label>
							<div class="col-lg-4">
								<select name="perdin_berangkat_provinsi_kode" id="perdin_berangkat_provinsi_kode" data-placeholder="Pilih Provinsi" class="select-size-xs wajib" onchange="get_kota(this,'perdin_berangkat_kota_kode')">
									<option></option>
									<?php foreach ($provinsi as $i) {

										echo '<option value="'.$i["provinsi_kode"].'">'.$i["provinsi_nama"].'</option>';
									}?>
								</select>
							</div>
					</div>
					<div class="form-group form-group-xs dalam_negeri">
						<label class="col-lg-2 control-label"></label>
						<label class="col-lg-1 control-label">Kota</b></label>
						<div class="col-lg-4">
							<select name="perdin_berangkat_kota_kode" id="perdin_berangkat_kota_kode" data-placeholder="Pilih Kota" class="select-size-xs wajib" onchange="get_kecamatan(this,'perdin_berangkat_kecamatan_kode')">
								<option></option>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs dalam_negeri">
						<label class="col-lg-2 control-label"></label>
						<label class="col-lg-1 control-label">Kecamatan</b></label>
						<div class="col-lg-4">
							<select name="perdin_berangkat_kecamatan_kode" id="perdin_berangkat_kecamatan_kode" data-placeholder="Pilih Kecamatan" class="select-size-xs">
								<option></option>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label"><b>Tempat Tujuan</b></label>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-2 control-label"></label>
						<label class="col-lg-1 control-label">Negara</b></label>
						<div class="col-lg-4">
							<select name="perdin_tujuan_negara_kode" id="perdin_tujuan_negara_kode" data-placeholder="Pilih Negara" class="select-size-xs wajib" onchange="get_provinsi(this)">
								<option></option>
								<?php foreach ($negara as $i) {

									echo '<option value="'.$i["negara_id"].'" '.(($i["negara_type"]==1)?"selected":"").' data-negara-type="'.$i["negara_type"].'">'.$i["negara_nama"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs dalam_negeri">
						<label class="col-lg-2 control-label"></label>
						<label class="col-lg-1 control-label">Provinsi</b></label>
							<div class="col-lg-4">
								<select name="perdin_tujuan_provinsi_kode" id="perdin_tujuan_provinsi_kode" data-placeholder="Pilih Provinsi" class="select-size-xs wajib" onchange="get_kota(this,'perdin_tujuan_kota_kode')">
									<option></option>
									<?php foreach ($provinsi as $i) {

										echo '<option value="'.$i["provinsi_kode"].'">'.$i["provinsi_nama"].'</option>';
									}?>
								</select>
							</div>
					</div>
					<div class="form-group form-group-xs dalam_negeri">
						<label class="col-lg-2 control-label"></label>
						<label class="col-lg-1 control-label">Kota</b></label>
						<div class="col-lg-4">
							<select name="perdin_tujuan_kota_kode" id="perdin_tujuan_kota_kode" data-placeholder="Pilih Kota" class="select-size-xs wajib" onchange="get_kecamatan(this,'perdin_tujuan_kecamatan_kode')">
								<option></option>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs dalam_negeri">
						<label class="col-lg-2 control-label"></label>
						<label class="col-lg-1 control-label">Kecamatan</b></label>
						<div class="col-lg-4">
							<select name="perdin_tujuan_kecamatan_kode" id="perdin_tujuan_kecamatan_kode" data-placeholder="Pilih Kecamatan" class="select-size-xs">
								<option></option>
							</select>
						</div>
					</div>
					<!-- <div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Lamanya Perjalanan Dinas</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" placeholder="Lamanya Perjalanan Dinas" name="perdin_tempat_tujuan">
						</div>
					</div> -->
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Dari Tanggal</label>
						<div class="col-lg-6">
							<input type="text" class="form-control input-xs wajib pickmulaiperdin" name="perdin_tanggal_mulai" placeholder="Dari Tanggal">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Sampai Tanggal</label>
						<div class="col-lg-6">
							<input type="text" class="form-control input-xs wajib pickselesaiperdin" name="perdin_tanggal_selesai" placeholder="Sampai Tanggal">
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
					<a href="<?php echo base_url();?>perdin/perdin" class="btn btn-xs btn-warning">Kembali</a>
					
				</div>
			</div>

		</form>
	</div>
</div>

<script type="text/javascript">
	function get_provinsi(ini){
		var IsDalamNegeri = $('option:selected', ini).attr('data-negara-type');
		if(IsDalamNegeri!=1){
			$('.dalam_negeri').slideUp(function(){
				$('#perdin_berangkat_provinsi_kode').select2('val', ' ');
				$('#perdin_berangkat_provinsi_kode').trigger('change');
				$('#perdin_berangkat_kota_kode').html('<option></option>');
				$('#perdin_berangkat_kecamatan_kode').html('<option></option>');
			});
		}else{
			$('.dalam_negeri').slideDown(function(){
			});
		}
		
	}
	function  get_kota(ini,nextopsi) {
		var provinsi_kode = $(ini).val();
		$.ajax({
			url: base_url+'perdin/perdin/get_kota',
			type: "post",
			dataType: 'json',
			data: { provinsi_kode: provinsi_kode},
			success: function (data) {
				var kota = data.kota;
				var newoption = "<option></option>";
				for(var i = 0; i < kota.length; i ++){
					newoption+='<option value="'+kota[i].kota_kode+'">'+kota[i].kota_nama+'</option>';
				}
				$('#'+nextopsi).html(newoption);
			}
		});
	}
	function  get_kecamatan(ini,nextopsi) {
		var kota_kode = $(ini).val();
		$.ajax({
			url: base_url+'perdin/perdin/get_kecamatan',
			type: "post",
			dataType: 'json',
			data: { kota_kode: kota_kode},
			success: function (data) {
				var kecamatan = data.kecamatan;
				var newoption = "<option></option>";
				for(var i = 0; i < kecamatan.length; i ++){
					newoption+='<option value="'+kecamatan[i].kecamatan_kode+'">'+kecamatan[i].kecamatan_nama+'</option>';
				}
				$('#'+nextopsi).html(newoption);
			}
		});
	}
	$(document).ready(function() {
		$('.pickmulaiperdin').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			min: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
	});
	$(document).ready(function() {
		$('.pickselesaiperdin').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			min: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
	});
	$(document).ready(function () {
	    var counter = 0;
	 //    $.ajax({
		// 	url: '<?php echo base_url();?>cuti/pengajuan_cuti/get_atasan',
		// 	type: 'POST',
		// 	dataType: 'json',
		// 	data: {unit_parrent: unit_parrent},
		// 	success: function (data) {
		// 		$.each(data, function(index, val) {
		// 			console.log(val.pegawai_nama);
		// 			$('#cuti_detail_pejabat_nip').append('<option value="'+val.pegawai_nip+'">'+val.pegawai_nama+'</option>');
		// 		});
		// 	}
		// });
	    $("#addrow").on("click", function () {
	        var newRow = $("<tr>");
	        var cols = "";

	        // cols += '<td><select name"perdin_ketua_nip[]' + counter + '" class="select-size-xs wajib" data-placeholder="Pilih Pegawai"><option></option><option>1</option></select></td>';
	        cols += '<td><input type="text" class="form-control wajib" name="perdin_ketua_nip[]' + counter + '"/></td>';
	        cols += '<td><input type="text" class="form-control wajib" name="perdin_ketua_jabatan_nama[]' + counter + '"/></td>';

	        cols += '<td><input type="button" class="ibtnDel btn btn-xs btn-danger " value="Delete"></td>';
	        newRow.append(cols);
	        $("table.order-list").append(newRow);
	        counter++;
	    });



	    $("table.order-list").on("click", ".ibtnDel", function (event) {
	        $(this).closest("tr").remove();       
	        counter -= 1
	    });


	});
</script>
