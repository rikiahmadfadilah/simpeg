
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Perubahan Data Perjalanan Dinas</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">No SP</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" name="perdin_no_sp" value="<?php echo $perdin["perdin_no_sp"];?>">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Pejabat Pembuat Komitmen</label>
						<div class="col-md-9">
							<select name="perdin_ppk_nip" id="perdin_ppk_nip" data-placeholder="Pilih Provinsi" class="select-size-xs wajib">
								<option></option>
								<?php foreach ($pegawai as $i) {
									echo '<option value="'.$i["pegawai_nip"].'" '.(($perdin["perdin_ppk_nip"]==$i["pegawai_nip"])?"selected":"").'>'.$i["pegawai_nip"].'-'.$i["pegawai_nama"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Nama/NIP Pegawai</label>
						<div class="col-md-9">
							<select name="perdin_ketua_nip" id="perdin_ketua_nip" data-placeholder="Pilih Provinsi" class="select-size-xs wajib">
								<option></option>
								<?php foreach ($pegawai as $i) {
									echo '<option value="'.$i["pegawai_nip"].'" '.(($perdin["perdin_ketua_nip"]==$i["pegawai_nip"])?"selected":"").'>'.$i["pegawai_nip"].'-'.$i["pegawai_nama"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<!-- <div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Pangkat/Golongan</label>
						<div class="col-md-9">
							<select name="perdin_ketua_golongan_id" id="perdin_ketua_golongan_id" data-placeholder="Pilih Provinsi" class="select-size-xs wajib">
								<option></option>
								<?php foreach ($pangkatgolongan as $i) {
									echo '<option value="'.$i["golongan_id"].'" '.(($perdin["perdin_ketua_golongan_id"]==$i["golongan_id"])?"selected":"").'>'.$i["golongan_nama"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Jabatan/Instansi</label>
						<div class="col-md-3">
							<select name="perdin_ketua_jabatan_id" id="perdin_ketua_jabatan_id" data-placeholder="Pilih Jabatan/Instansi" class="select-size-xs wajib">
								<option></option>
								<?php foreach ($jabataninstansi as $i) {
									echo '<option value="'.$i["jabatan_id"].'" '.(($perdin["perdin_ketua_jabatan_id"]==$i["jabatan_id"])?"selected":"").'>'.$i["jabatan_nama"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Tingkat Biaya Perjalanan Dinas</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" name="perdin_tingkat_biaya" id="perdin_tingkat_biaya" value="<?php echo $perdin["perdin_tingkat_biaya"];?>">
						</div>
					</div> -->
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Maksud Perjalanan Dinas</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" name="perdin_maksud_tujuan" value="<?php echo $perdin["perdin_maksud_tujuan"];?>">
						</div>
					</div>
					<!-- <div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Alat angkutan yang dipergunakan</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" name="perdin_alat_angkut" value="<?php echo $perdin["perdin_alat_angkut"];?>">
						</div>
					</div> -->
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Tempat Berangkat</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" name="perdin_tempat_berangkat" value="<?php echo $perdin["perdin_tempat_berangkat"];?>">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Tempat Tujuan</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" name="perdin_tempat_tujuan" value="<?php echo $perdin["perdin_tempat_tujuan"];?>">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Dari Tanggal</label>
						<div class="col-lg-6">
							<input type="text" class="form-control input-xs wajib pickperdin" name="perdin_tanggal_mulai" value="<?php echo $perdin["perdin_tanggal_mulai"];?>">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Sampai Tanggal</label>
						<div class="col-lg-6">
							<input type="text" class="form-control input-xs wajib pickperdin" name="perdin_tanggal_selesai" value="<?php echo $perdin["perdin_tanggal_selesai"];?>">
						</div>
					</div>
					<!-- <div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Pembebanan Anggaran Instansi</label>
						<div class="col-lg-9">
							<input type="number" class="form-control input-xs wajib" name="perdin_anggaran_instansi" value="<?php echo $perdin["perdin_anggaran_instansi"];?>">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Pembebanan Anggaran Akun</label>
						<div class="col-lg-9">
							<input type="number" class="form-control input-xs wajib" name="perdin_anggaran_akun" value="<?php echo $perdin["perdin_anggaran_akun"];?>">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Keterangan lain-lain</label>
						<div class="col-lg-9">
							<textarea rows="5" cols="5" class="form-control input-xs wajib" name="perdin_anggaran_keterangan" value="<?php echo $perdin["perdin_anggaran_keterangan"];?>"><?php echo $perdin["perdin_anggaran_keterangan"];?></textarea>
						</div>
					</div> -->
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
					<a href="<?php echo base_url();?>perdin/perdin" class="btn btn-xs btn-warning">Kembali</a>
					<input type="hidden" name="perdin_id" id="perdin_id"  class="form-control input-xs wajib" type="text" value="<?php echo $perdin["perdin_id"];?>" >
					
				</div>
			</div>

		</form>
	</div>
</div>
<script type="text/javascript">
	$('.pickperdin').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			min: true,
			max: false,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
</script>
