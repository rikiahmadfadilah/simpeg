<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
    <div class="panel-heading">
        <h6 class="panel-title">Data Perjalanan Dinas</h6>
        <div class="heading-elements">
            <div class="heading-btn">
                <a href="<?php echo base_url().'perdin/perdin/create';?>" class="btn btn-primary btn-labeled btn-xs"><b><i class=" icon-plus3"></i></b> Tambah Data Perjalanan Dinas</a>
            </div>
        </div>
    </div>

    <div class="panel-body">
        <table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive ropeg_table" id="list_data">
            <thead>
                <tr>
                    <td class="text-center" rowspan="2"><b>No SPT</b></td>
                    <td class="text-center" rowspan="2"><b>Nama</b></td>
                    <td class="text-center" rowspan="2" style="width: 175px;min-width: 175px;"><b>NIP</b></td>
                    <td class="text-center" rowspan="2"><b>Kota Tujuan</b></td>
                    <td class="text-center" colspan="2"><b>Periode Perjalanan Dinas</b></td>
                    <td class="text-center" rowspan="2" style="width: 80px;min-width: 80px;"><b>Aksi</b></td>
                </tr>
                <tr>
                    <td class="text-center"><b>Dari Tanggal</b></td>
                    <td class="text-center"><b>Sampai Tanggal</b></td>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#list_data').dataTable( {
            "processing": true,
            "serverSide": true,
            "bServerSide": true,
            "sAjaxSource": base_url+"perdin/perdin/list_data_aktif",
            "aaSorting": [],
            "order": [],
            "iDisplayLength": 10,
            "aoColumns": [
            { "bSortable": false, "sClass": "text-left" },
            { "bSortable": false, "sClass": "text-left" },
            { "bSortable": false, "sClass": "text-left" },
            { "bSortable": false, "sClass": "text-center" },
            { "bSortable": false, "sClass": "text-center" },
            { "bSortable": false, "sClass": "text-center" },
            { "bSortable": false, "sClass": "text-center" },
            ],
            "fnDrawCallback": function () {
                set_default_datatable();
            },
        });
    });
    function set_aktif(id){
        $('#text_konfirmasi').html('Anda yakin mengaktifkan data ini ?');
        $('#konfirmasipenyimpanan').modal('show');
        $('#setujukonfirmasibutton').unbind();
        $('#setujukonfirmasibutton').on('click', function () {
            $('#konfirmasipenyimpanan').modal('hide');
            $('.angkadoank').inputmask('remove');
            $('#text_konfirmasi').html('Anda yakin dengan data ini ?');
            window.location.href = base_url+'perdin/perdin/aktif/'+id;
        });
    }
    function set_non_aktif(id){
        $('#text_konfirmasi').html('Anda yakin nonaktifkan data ini ?');
        $('#konfirmasipenyimpanan').modal('show');
        $('#setujukonfirmasibutton').unbind();
        $('#setujukonfirmasibutton').on('click', function () {
            $('#konfirmasipenyimpanan').modal('hide');
            $('.angkadoank').inputmask('remove');
            $('#text_konfirmasi').html('Anda yakin dengan data ini ?');
            window.location.href = base_url+'perdin/perdin/non_aktif/'+id;
        });
    }
</script>
