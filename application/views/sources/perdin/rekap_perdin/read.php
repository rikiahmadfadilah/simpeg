
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Data Pegawai</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">NIP Pegawai</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $pegawai["pegawai_nip"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Nama Pegawai</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $pegawai["pegawai_nama"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Jabatan</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $pegawai["pegawai_nama_jabatan"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Unit Kerja</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $pegawai["unit_kerja_hirarki_name_full"];?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Banyak Perjalanan Dinas</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php echo $banyak_perdin;?>" readonly="readonly">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Lama Perjalanan Dinas</label>
						<div class="col-md-9">
							<input type="text" class="form-control input-xs wajib" type="text" value="<?php if($bln > 0){echo $bln; echo' Bulan ';}else{} if($mng > 0){echo $mng; echo' Minggu ';}else{} echo $hr; echo' Hari ';?>" readonly="readonly">
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<a href="<?php echo base_url();?>perdin/rekap_perdin" class="btn btn-xs btn-warning">Kembali</a>
					<input type="hidden" name="perdin_id" id="perdin_id"  class="form-control input-xs wajib" type="text" value="<?php echo $pegawai["pegawai_nip"];?>" >
					
				</div>
			</div>

		</form>
	</div>
</div>
<script type="text/javascript">
	
	$(document).ready(function() {
		

	});
</script>
