<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
    <div class="panel-heading">
        <h6 class="panel-title">Rekapitulasi Perjalanan Dinas</h6>
        <div class="heading-elements">
            <div class="heading-btn">
            </div>
        </div>
    </div>

    <div class="panel-body">
        <table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive epagawai_table" id="list_data">
            <thead>
                <tr>
                    <td class="text-center"><b>NIP</b></td>
                    <td class="text-center"><b>Nama</b></td>
                    <td class="text-center"><b>Jabatan</b></td>
                    <td class="text-center"><b>Unit Kerja</b></td>
                    <td class="text-center" style="width: 80px;min-width: 80px;"><b>Aksi</b></td>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#list_data').dataTable( {
            "processing": true,
            "serverSide": true,
            "bServerSide": true,
            "sAjaxSource": base_url+"perdin/rekap_perdin/list_data_aktif",
            "aaSorting": [],
            "order": [],
            "iDisplayLength": 10,
            "aoColumns": [
            { "bSortable": false, "sClass": "text-left" },
            { "bSortable": false, "sClass": "text-left" },
            { "bSortable": false, "sClass": "text-left" },
            { "bSortable": false, "sClass": "text-left" },
            { "bSortable": false, "sClass": "text-center" },
            ],
            "fnDrawCallback": function () {
                set_default_datatable();
            },
        });
    });
</script>
