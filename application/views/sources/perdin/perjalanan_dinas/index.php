<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">DAFTAR PERJALANAN DINAS</h6>
		<div class="heading-elements">
			<div class="heading-btn">
				<a href="<?php echo base_url().'perjalanan_dinas/perjalanan_dinas/create';?>" class="btn btn-primary btn-labeled btn-xs"><b><i class=" icon-plus3"></i></b> Tambah Data Perjalanan Dinas</a>
			</div>
		</div>
	</div>

	<div class="panel-body">
		<table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive epagawai_table" id="list_data">
			<thead>
				<tr>
					<th class="text-center" style="width: 175px;min-width: 175px;">NIP</th>
					<th class="text-center">Nama</th>
					<th class="text-center">Tugas</th>
					<th class="text-center">Periode Perjalanan Dinas</th>
					<th class="text-center" style="width: 90px;min-width: 90px;">Aksi</th>
				</tr>
			</thead>
			
		</table>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#list_data').dataTable( {
			"processing": true,
			"serverSide": true,
			"bServerSide": true,
			"sAjaxSource": base_url+"perjalanan_dinas/perjalanan_dinas/list_data",
			 "aaSorting": [],
			 "order": [],
			 "iDisplayLength": 50,
			"aoColumns": [
			// { "sClass": "text-left" },
			{ "sClass": "text-left" },
			{ "sClass": "text-left" },
			{ "sClass": "text-left" },
			{ "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-center" },
			],
			"fnDrawCallback": function () {
				set_default_datatable();
			},
		});
	});
</script>
