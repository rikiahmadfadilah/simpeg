<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">UPDATE DATA PERJALANAN DINAS</h6>
	</div>

	<div class="panel-body" >
		
		<?php foreach($perdin as $pd){ ?>
		<form class="form-horizontal" method="post" action="<?php echo base_url(). 'perjalanan_dinas/perjalanan_dinas/update_action'; ?>" style="margin-top: 20px;" onsubmit="return validasi_input(this)">
			<div class="col-md-12">
				<fieldset>

					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">NIP</label>
						<div class="col-lg-9">
							<input type="hidden" name="perdin_id" value="<?php echo $pd->perdin_id ?>">
							<select data-placeholder="Pilih Pegawai" class="select-size-xs" name="perdin_detail_pegawai_nip">
								<option></option>
								<?php
                                  foreach ($pegawai as $p) 
                                  {
                                    if ($p['pegawai_nip']==$getPegawai['perdin_ppk_nip']) 
                                  {                                
                                        echo "<option value=".$p['pegawai_nip']." selected>".$p['pegawai_nama']."</option>";
                                  } else 
                                  {
                                        echo "<option value=".$p['pegawai_nip'].">".$p['pegawai_nama']."</option>";
                                  }
                                  }
                                ?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Pangkat dan Golongan</label>
						<div class="col-lg-9">
							<select data-placeholder="Pilih Pangkat dan Golongan" class="select-size-xs" name="perdin_ketua_golongan_id">
								<option></option>
								<?php
                                  foreach ($pangkatgolongan as $pg) 
                                  {
                                    if ($pg['golongan_id']==$getPegawai['perdin_ketua_golongan_id']) 
                                  {                                
                                        echo "<option value=".$pg['golongan_id']." selected>".$pg['golongan_name']."</option>";
                                  } else 
                                  {
                                        echo "<option value=".$pg['golongan_id'].">".$pg['golongan_name']."</option>";
                                  }
                                  }
                                ?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Jabatan/Instansi</label>
						<div class="col-lg-9">
							<select data-placeholder="Pilih Jabatan/Instansi" class="select-size-xs" name="perdin_ketua_jabatan_id">
								<option></option>
								<?php
                                  foreach ($jabataninstansi as $ji) 
                                  {
                                    if ($ji['jabatan_id']==$getJabatanInstansi['perdin_ketua_jabatan_id']) 
                                  {                                
                                        echo "<option value=".$ji['jabatan_id']." selected>".$ji['jabatan_name']."</option>";
                                  } else 
                                  {
                                        echo "<option value=".$ji['jabatan_id'].">".$ji['jabatan_name']."</option>";
                                  }
                                  }
                                ?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Tingkat Biaya Perjalanan Dinas</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" placeholder="Tingkat Biaya Perjalanan Dinas" name="perdin_tingkat_biaya" value="<?php echo $pd->perdin_tingkat_biaya ?>">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Maksud Perjalanan Dinas</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" placeholder="Alasan Cuti" name="perdin_maksud_tujuan" value="<?php echo $pd->perdin_maksud_tujuan ?>">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Alat angkut yang digunakan</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" placeholder="Alasan Cuti" name="perdin_alat_angkut" value="<?php echo $pd->perdin_alat_angkut ?>">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Tempat Berangkat</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" placeholder="Alasan Cuti" name="perdin_tempat_berangkat" value="<?php echo $pd->perdin_tempat_berangkat ?>">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Tempat Tujuan</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" placeholder="Alasan Cuti" name="perdin_tempat_tujuan" value="<?php echo $pd->perdin_tempat_tujuan ?>">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Tanggal Berangkat</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib pickcperdin" placeholder="Alasan Cuti" name="perdin_tanggal_mulai" value="<?php echo $pd->perdin_tanggal_mulai ?>">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Tanggal harus kembali</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib pickcperdin" placeholder="Alasan Cuti" name="perdin_tanggal_selesai" value="<?php echo $pd->perdin_tanggal_selesai ?>">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Pembebanan Anggaran Instansi</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" placeholder="Alasan Cuti" name="perdin_anggaran_instansi" value="<?php echo $pd->perdin_anggaran_instansi ?>">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Pembebanan Anggaran Akun</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" placeholder="Alasan Cuti" name="perdin_anggaran_akun" value="<?php echo $pd->perdin_anggaran_akun ?>">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Keterangan lain-lain</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" placeholder="Alasan Cuti" name="perdin_anggaran_keterangan" value="<?php echo $pd->perdin_anggaran_keterangan ?>">
						</div>
					</div>
				</fieldset>
			</div>
			<div class="col-md-12">
				<div class="text-right">
						<a href="<?php echo base_url('perjalanan_dinas/perjalanan_dinas'); ?>" type="submit" class="btn btn-default btn-md">Kembali <i class="icon-undo2 position-right"></i></a>
						<button type="submit" class="btn btn-primary">Simpan Data <i class="icon-arrow-right14 position-right"></i></button>
					</div>
			</div>
		</form>
		<?php } ?>

		
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		var d = new Date();

		$('.pickcperdin').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			min: true,
			max: false,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
	});

	function validasi_input(form){
  		if (form.perdin_detail_pegawai_nip.value == ""){
    		alert("Pilih pegawai masih kosong!");
    		form.perdin_detail_pegawai_nip.focus();
    		return (false);
  			}
  		else if (form.perdin_ketua_golongan_id.value == ""){
    		alert("Pilih pangkat/golongan masih kosong!");
    		form.perdin_ketua_golongan_id.focus();
    		return (false);
  			}
  		else if (form.perdin_ketua_jabatan_id.value == ""){
    		alert("Pilih jabatan/instansi masih kosong!");
    		form.perdin_ketua_jabatan_id.focus();
    		return (false);
  			}
  		else if (form.perdin_tingkat_biaya.value == ""){
    		alert("Tingkat biaya perjalanan dinas masih kosong!");
    		form.perdin_tingkat_biaya.focus();
    		return (false);
  			}
  		else if (form.perdin_maksud_tujuan.value == ""){
    		alert("Maksud dan tujuan perjalanan dinas masih kosong!");
    		form.perdin_maksud_tujuan.focus();
    		return (false);
  			}
  		else if (form.perdin_alat_angkut.value == ""){
    		alert("Alat angkut yang dipergunakan masih kosong!");
    		form.perdin_alat_angkut.focus();
    		return (false);
  			}
  		else if (form.perdin_tempat_berangkat.value == ""){
    		alert("Tempat berangkat masih kosong!");
    		form.perdin_tempat_berangkat.focus();
    		return (false);
  			}
  		else if (form.perdin_tanggal_selesai.value == ""){
    		alert("Tanggal harus kembali masih kosong!");
    		form.perdin_tanggal_selesai.focus();
    		return (false);
  			}
  		else if (form.perdin_anggaran_instansi.value == ""){
    		alert("Pembebanan anggaran instansi masih kosong!");
    		form.perdin_anggaran_instansi.focus();
    		return (false);
  			}
  		else if (form.perdin_anggaran_akun.value == ""){
    		alert("Pembebanan anggaran akun masih kosong!");
    		form.perdin_anggaran_akun.focus();
    		return (false);
  			}
  		else if (form.perdin_anggaran_keterangan.value == ""){
    		alert("Keterangan lain-lain masih kosong!");
    		form.perdin_anggaran_keterangan.focus();
    		return (false);
  			}
  		else if (form.perdin_tingkat_biaya.value != ""){
  			var x = (form.perdin_tingkat_biaya.value);
  			var status = true;
  			var list = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
  				for (i=0; i<=x.length-1; i++)
  				{
  					if (x[i] in list) cek = true;
  					else cek = false;
 					status = status && cek;
  				}
  					if (status == false)
  				{
  					alert("Tingkat biaya harus angka!");
  					form.perdin_tingkat_biaya.focus();
  				return false;
  				}
  			}
  		else if (form.perdin_anggaran_instansi.value != ""){
  			var x = (form.perdin_anggaran_instansi.value);
  			var status = true;
  			var list = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
  				for (i=0; i<=x.length-1; i++)
  				{
  					if (x[i] in list) cek = true;
  					else cek = false;
 					status = status && cek;
  				}
  					if (status == false)
  				{
  					alert("Pembebanan anggaran instansi harus angka!");
  					form.perdin_anggaran_instansi.focus();
  				return false;
  				}
  			}
  		else if (form.perdin_anggaran_akun.value != ""){
  			var x = (form.perdin_anggaran_akun.value);
  			var status = true;
  			var list = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
  				for (i=0; i<=x.length-1; i++)
  				{
  					if (x[i] in list) cek = true;
  					else cek = false;
 					status = status && cek;
  				}
  					if (status == false)
  				{
  					alert("Pembebanan anggaran akun harus angka!");
  					form.perdin_anggaran_akun.focus();
  				return false;
  				}
  			}
			return (true);
	}
</script>
