<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">DETAIL DATA PERJALANAN DINAS</h6>
	</div>

	<div class="panel-body" >
		
		<?php foreach($perdin as $pd){ ?>
		<form class="form-horizontal" method="" action="detail" style="margin-top: 20px;">
			<div class="col-md-12">
				<fieldset>

					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">NIP</label>
						<div class="col-lg-9">
							<label class="control-label col-lg-9">: <?php echo $pd->pegawai_nip ?></label>	
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Nama Pegawai</label>
						<div class="col-lg-9">
							<label class="control-label col-lg-9">: <?php echo $pd->pegawai_nama ?></label>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Pangkat dan Golongan</label>
						<div class="col-lg-9">
							<label class="control-label col-lg-9">: <?php echo $pd->golongan_name ?></label>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Jabatan/Instansi</label>
						<div class="col-lg-9">
							<label class="control-label col-lg-9">: <?php echo $pd->jabatan_name ?></label>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Tingkat Biaya Perjalanan Dinas</label>
						<div class="col-lg-9">
							<label class="control-label col-lg-9">: <?php echo $pd->perdin_tingkat_biaya ?></label>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Maksud Perjalanan Dinas</label>
						<div class="col-lg-9">
							<label class="control-label col-lg-9">: <?php echo $pd->perdin_maksud_tujuan ?></label>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Alat angkutan yang dipergunakan</label>
						<div class="col-lg-9">
							<label class="control-label col-lg-9">: <?php echo $pd->perdin_alat_angkut ?></label>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Tempat Berangkat</label>
						<div class="col-lg-9">
							<label class="control-label col-lg-9">: <?php echo $pd->perdin_tempat_berangkat ?></label>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Tempat Tujuan</label>
						<div class="col-lg-9">
							<label class="control-label col-lg-9">: <?php echo $pd->perdin_tempat_tujuan ?></label>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Tanggal Berangkat</label>
						<div class="col-lg-9">
							<label class="control-label col-lg-9">: <?php echo $pd->perdin_tanggal_mulai ?></label>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Tanggal harus kembali</label>
						<div class="col-lg-9">
							<label class="control-label col-lg-9">: <?php echo $pd->perdin_tanggal_selesai ?></label>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Pembebanan Anggaran Instansi</label>
						<div class="col-lg-9">
							<label class="control-label col-lg-9">: <?php echo $pd->perdin_anggaran_instansi ?></label>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Pembebanan Anggaran Akun</label>
						<div class="col-lg-9">
							<label class="control-label col-lg-9">: <?php echo $pd->perdin_anggaran_akun ?></label>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Keterangan lain-lain</label>
						<div class="col-lg-9">
							<label class="control-label col-lg-9">: <?php echo $pd->perdin_anggaran_keterangan ?></label>
						</div>
					</div>
				</fieldset>
			</div>
			<div class="col-md-12">
				<div class="text-right">
						<a href="<?php echo base_url('perjalanan_dinas/perjalanan_dinas'); ?>" type="submit" class="btn btn-default btn-md">Kembali <i class="icon-undo2 position-right"></i></a>
					</div>
			</div>
		</form>
		<?php } ?>

		
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		var d = new Date();

		$('.pickcuti').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			min: true,
			max: false,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
	});
</script>
