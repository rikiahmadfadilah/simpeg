<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">TAMBAH DATA PERJALANAN DINAS</h6>
	</div>

	<div class="panel-body" >
		
		<form class="form-horizontal" method="post" action="create_action" style="margin-top: 20px;" onsubmit="return validasi_input(this)">
			<div class="col-md-12">
				<fieldset>

					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">NIP</label>
						<div class="col-lg-9">
							<select data-placeholder="Pilih NIP" class="select-size-xs" name="perdin_detail_pegawai_nip" id="pilih">
								<option></option>
								<?php foreach ($pegawai as $p) {
									echo '<option value="'.$p["pegawai_nip"].'">'.$p["pegawai_nip"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<!-- <div id="hasil"></div> -->

					<!-- <div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Nama</label>
						<div class="col-lg-9" >
							<input type="text" class="form-control input-xs wajib" value="" disabled>
						</div>
					</div> -->

					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Pangkat dan Golongan</label>
						<div class="col-lg-9">
							<select data-placeholder="Pilih Pangkat dan Golongan" class="select-size-xs" name="perdin_ketua_golongan_id">
								<option></option>
								<?php foreach ($pangkatgolongan as $pg) {
									echo '<option value="'.$pg["golongan_id"].'">'.$pg["golongan_name"].'</option>';
								}?>
							</select>
						</div>
					</div>

					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Jabatan/Instansi</label>
						<div class="col-lg-9">
							<select data-placeholder="Pilih Jabatan/Instansi" class="select-size-xs" name="perdin_ketua_jabatan_id">
								<option></option>
								<?php foreach ($jabataninstansi as $ji) {
									echo '<option value="'.$ji["jabatan_id"].'">'.$ji["jabatan_name"].'</option>';
								}?>
							</select>
						</div>
					</div>

					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Tingkat Biaya Perjalanan Dinas</label>
						<div class="col-lg-9">
							<input type="number" class="form-control input-xs wajib" placeholder="Tingkat Biaya Perjalanan Dinas" name="perdin_tingkat_biaya">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Maksud Perjalanan Dinas</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" placeholder="Maksud Perjalanan Dinas" name="perdin_maksud_tujuan">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Alat angkutan yang dipergunakan</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" placeholder="Alat angkutan yang dipergunakan" name="perdin_alat_angkut">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Tempat Berangkat</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" placeholder="Tempat Berangkat" name="perdin_tempat_berangkat">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Tempat Tujuan</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" placeholder="Tempat Tujuan" name="perdin_tempat_tujuan">
						</div>
					</div>
					<!-- <div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Lamanya Perjalanan Dinas</label>
						<div class="col-lg-9">
							<input type="text" class="form-control input-xs wajib" placeholder="Lamanya Perjalanan Dinas" name="perdin_tempat_tujuan">
						</div>
					</div> -->
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Tanggal Berangkat</label>
						<div class="col-lg-6">
							<input type="text" class="form-control input-xs wajib pickperdin" name="perdin_tanggal_mulai" placeholder="Tanggal Berangkat">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Tanggal harus kembali</label>
						<div class="col-lg-6">
							<input type="text" class="form-control input-xs wajib pickperdin" name="perdin_tanggal_selesai" placeholder="Tanggal harus kembali">
						</div>
					</div>
					<!-- <div class="form-group">
						<label class="col-lg-3 control-label">Pengikut</label>
					        <div class="col-lg-9">
					        	<input type="button" style="width: 80px;min-width: 80px;" class="add-row" value="Add Row">
								<table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive epagawai_table">
					        		<thead>
							            <tr>
							                <th>Nama</th>
							                <th>Tanggal Lahir</th>
							                <th>Keterangan</th>
							                <th>Pilih</th>
							            </tr>
							        </thead>
							        <tbody>
							            <tr>
							            	<td><input type="text" class="form-control input-xs wajib"></td>
							                <td><input type="text" class="form-control input-xs wajib picktgl"></td>
							                <td><input type="text" class="form-control input-xs wajib" id="keterangan"></td></td>
							                <td></td>
							            </tr>
							        </tbody>
							    </table>
							    <button type="button" class="delete-row" style="width: 80px;min-width: 80px;">Hapus</button>
							</div>
					</div> -->
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Pembebanan Anggaran Instansi</label>
						<div class="col-lg-9">
							<input type="number" class="form-control input-xs wajib" placeholder="Pembebanan Anggaran Instansi" name="perdin_anggaran_instansi">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Pembebanan Anggaran Akun</label>
						<div class="col-lg-9">
							<input type="number" class="form-control input-xs wajib" placeholder="Pembebanan Anggaran Akun" name="perdin_anggaran_akun">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-lg-3 control-label">Keterangan lain-lain</label>
						<div class="col-lg-9">
							<textarea rows="5" cols="5" class="form-control input-xs wajib" name="perdin_anggaran_keterangan" placeholder="Keterangan lain-lain"></textarea>
						</div>
					</div>
				</fieldset>
			</div>
			<div class="col-md-12">
				<div class="text-right">
						<button type="submit" class="btn btn-primary">Simpan Data <i class="icon-arrow-right14 position-right"></i></button>
					</div>
			</div>
		</form>

		
	</div>
</div>
<script type="text/javascript">
	$('#pilih').change(function(event) {
        $('#hasil').html('This is ' + $('#pilih').val() + ' and other info');
    }); 

	$(document).ready(function() {
		var d = new Date();

		$('.pickperdins').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			min: true,
			max: false,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
	});

	$(document).ready(function() {
		var d = new Date();

		$('.picktgl').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			min: false,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
	});

	$(document).ready(function(){
        $(".add-row").click(function(){
            var markup = "<tr id='myTableRow'><td><input type='text' class='form-control input-xs wajib' name='nama'></td><td><input type='text' class='form-control input-xs wajib' name='tgl_lahir' picktgl></td><td><input type='text' class='form-control input-xs wajib' name='keterangan'></td><td><input type='checkbox' name='record'></td></tr>";
            $("table tbody").append(markup);
        });

        $(".delete-row").click(function(){
            $("table tbody").find('input[name="record"]').each(function(){
            	if($(this).is(":checked")){
                    $(this).parents("tr").remove();
                }
            });
        });

    });

	function validasi_input(form){
  		if (form.perdin_detail_pegawai_nip.value == ""){
    		alert("Pilih pegawai masih kosong!");
    		form.perdin_detail_pegawai_nip.focus();
    		return (false);
  			}
  		else if (form.perdin_ketua_golongan_id.value == ""){
    		alert("Pilih pangkat/golongan masih kosong!");
    		form.perdin_ketua_golongan_id.focus();
    		return (false);
  			}
  		else if (form.perdin_ketua_jabatan_id.value == ""){
    		alert("Pilih jabatan/instansi masih kosong!");
    		form.perdin_ketua_jabatan_id.focus();
    		return (false);
  			}
  		else if (form.perdin_tingkat_biaya.value == ""){
    		alert("Tingkat biaya perjalanan dinas masih kosong!");
    		form.perdin_tingkat_biaya.focus();
    		return (false);
  			}
  		else if (form.perdin_maksud_tujuan.value == ""){
    		alert("Maksud dan tujuan perjalanan dinas masih kosong!");
    		form.perdin_maksud_tujuan.focus();
    		return (false);
  			}
  		else if (form.perdin_alat_angkut.value == ""){
    		alert("Alat angkut yang dipergunakan masih kosong!");
    		form.perdin_alat_angkut.focus();
    		return (false);
  			}
  		else if (form.perdin_tempat_berangkat.value == ""){
    		alert("Tempat berangkat masih kosong!");
    		form.perdin_tempat_berangkat.focus();
    		return (false);
  			}
  		else if (form.perdin_tanggal_selesai.value == ""){
    		alert("Tanggal harus kembali masih kosong!");
    		form.perdin_tanggal_selesai.focus();
    		return (false);
  			}
  		else if (form.perdin_anggaran_instansi.value == ""){
    		alert("Pembebanan anggaran instansi masih kosong!");
    		form.perdin_anggaran_instansi.focus();
    		return (false);
  			}
  		else if (form.perdin_anggaran_akun.value == ""){
    		alert("Pembebanan anggaran akun masih kosong!");
    		form.perdin_anggaran_akun.focus();
    		return (false);
  			}
  		else if (form.perdin_anggaran_keterangan.value == ""){
    		alert("Keterangan lain-lain masih kosong!");
    		form.perdin_anggaran_keterangan.focus();
    		return (false);
  			}
  		else if (form.perdin_tingkat_biaya.value != ""){
  			var x = (form.perdin_tingkat_biaya.value);
  			var status = true;
  			var list = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
  				for (i=0; i<=x.length-1; i++)
  				{
  					if (x[i] in list) cek = true;
  					else cek = false;
 					status = status && cek;
  				}
  					if (status == false)
  				{
  					alert("Tingkat biaya harus angka!");
  					form.perdin_tingkat_biaya.focus();
  				return false;
  				}
  			}
  		else if (form.perdin_anggaran_instansi.value != ""){
  			var x = (form.perdin_anggaran_instansi.value);
  			var status = true;
  			var list = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
  				for (i=0; i<=x.length-1; i++)
  				{
  					if (x[i] in list) cek = true;
  					else cek = false;
 					status = status && cek;
  				}
  					if (status == false)
  				{
  					alert("Pembebanan anggaran instansi harus angka!");
  					form.perdin_anggaran_instansi.focus();
  				return false;
  				}
  			}
  		else if (form.perdin_anggaran_akun.value != ""){
  			var x = (form.perdin_anggaran_akun.value);
  			var status = true;
  			var list = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
  				for (i=0; i<=x.length-1; i++)
  				{
  					if (x[i] in list) cek = true;
  					else cek = false;
 					status = status && cek;
  				}
  					if (status == false)
  				{
  					alert("Pembebanan anggaran akun harus angka!");
  					form.perdin_anggaran_akun.focus();
  				return false;
  				}
  			}
			return (true);
	}

</script>
