<style type="text/css">
	.mandatory{
		color: red;
	}
</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h4 class="panel-title">BUAT KEPANGKATAN PEGAWAI</h4></br>
		<!-- <div class="heading-elements">
			<div class="heading-btn">
				<a href="<?php echo base_url().'skp/kinerjapegawai';?>" class="btn btn-danger btn-labeled btn-xs"><b><i class=" icon-backward"></i></b> Kembali</a>
			</div>
		</div> -->
	</div>
	<div class="panel-body">
		<div class="row">
			<form class="form-horizontal" method="post" action="" style="margin-top: 20px;" onsubmit="return validasi_input(this)">
			<div class="col-md-6">
				<legend class="text-bold">Data Pegawai</legend>
				<div class="form-horizontal">
					<div class="form-group form-group-xs" style="margin-bottom: 0px;">
						<label class="control-label col-xs-3">NIP</label>
						<label class="control-label col-xs-9"><?php echo $pegawai['pegawai_nip_fix']; ?></label>
					</div>
					<div class="form-group form-group-xs" style="margin-bottom: 0px;">
						<label class="control-label col-xs-3">Nama</label>
						<label class="control-label col-xs-9"><?php echo $pegawai['pegawai_nama_lengkap']; ?></label>
					</div>
					<div class="form-group form-group-xs" style="margin-bottom: 0px;">
						<label class="control-label col-xs-3">Pangkat</label>
						<label class="control-label col-xs-9"><?php echo "-"//$pegawai['pegawai_nip_fix']; ?></label>
					</div>
					<div class="form-group form-group-xs" style="margin-bottom: 0px;">
						<label class="control-label col-xs-3">Jabatan</label>
						<label class="control-label col-xs-9"><?php echo $pegawai['pegawai_nama_jabatan']; ?></label>
					</div>
					<div class="form-group form-group-xs" style="margin-bottom: 0px;">
						<label class="control-label col-xs-3">Unit Kerja</label>
						<label class="control-label col-xs-9"><?php echo $pegawai['pegawai_nama_unit_kerja']; ?></label>
					</div>
					<div class="form-group form-group-xs" style="margin-bottom: 0px;">
						<label class="control-label col-xs-3">Eselon</label>
						<label class="control-label col-xs-9"><?php echo $pegawai['pegawai_esel_kini']; ?></label>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				
			</div>
			<div class="clear" style="padding:10px;">&nbsp;</div>

			<div class="col-md-12">
				<legend class="text-bold">Data Kepangkatan </legend>
				<div class="form-horizontal">
					
					<div class="form-group form-group-xs">
						<label class="control-label col-xs-2">GOLONGAN <span class="mandatory">*</span></label>
						<div class="col-xs-1">
							<input type="text" class="form-control input-xs wajib" name="kepangkatan_golongan" id="kepangkatan_golongan" placeholder="" >
						</div>						
					</div>
					<div class="form-group form-group-xs">
						<label class="control-label col-xs-2">TMT GOL <span class="mandatory">*</span></label>
						<div class="col-xs-4">
							<input type="number" class="form-control input-xs wajib pickcuti" name="skp_kegiatan_jabatan_kuantitas_output" id="skp_kegiatan_jabatan_kuantitas_output" placeholder="">
						</div>		
					</div>
					<div class="form-group form-group-xs">
						<label class="control-label col-xs-2">PEJABAT <span class="mandatory">*</span></label>
						<div class="col-xs-4">
							<input type="text" class="form-control input-xs wajib" name="kepangkatan_pejabat" id="kepangkatan_pejabat" placeholder="" >
						</div>						
					</div>
					<div class="form-group form-group-xs">
						<label class="control-label col-xs-2">NOMOR SK <span class="mandatory">*</span></label>
						<div class="col-xs-4">
							<input type="text" class="form-control input-xs wajib" name="kepangkatan_nomor_sk" id="kepangkatan_nomor_sk" placeholder="" >
						</div>						
					</div>
					<div class="form-group form-group-xs">
						<label class="control-label col-xs-2">TANGGAL SK <span class="mandatory">*</span></label>
						<div class="col-xs-4">
							<input type="number" class="form-control input-xs wajib pickcuti" name="kepangkatan_tanggal_sk" id="kepangkatan_tanggal_sk" placeholder="" >
						</div>						
					</div>
					
				</div>
			</div>
			<div class="col-md-12">
				<hr/>
				<div class="text-left">
					<input type="hidden" class="form-control input-xs wajib" name="kepangkatan_pegawai_nip" value="<?php echo $pegawai['pegawai_nip']; ?>">
					<button type="submit" class="btn btn-primary btn-xs">Simpan Data <i class="icon-arrow-right14 position-right"></i></button>
					<a href="<?php echo base_url().'kepegawaian/kepangkatan/update/'.$pegawai['pegawai_id'];?>" class="btn btn-danger btn-labeled btn-xs"><b><i class=" icon-backward"></i></b> Kembali</a>
				</div>
			</div>
		</div>
		
		</form>
		
	</div>
	
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#list_data_skp').dataTable( {
			"fnDrawCallback": function () {
				set_default_datatable();
			},
		});
		$(".info-penilai").hide();
		$(".info-atasan").hide();
		$('.pickcuti').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			//min: true,
			//max: false,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
	});

	function  setPejabatPenilai(ini) {
		var TenderRUPId = $('option:selected', ini).attr('data-TenderRUPId');
		var nip = $('option:selected', ini).attr('data-nip');
		var nama = $('option:selected', ini).attr('data-nama');
		var pangkat = $('option:selected', ini).attr('data-pangkat');
		var eselon = $('option:selected', ini).attr('data-eselon');
		var jabatan = $('option:selected', ini).attr('data-jabatan');
		var unit = $('option:selected', ini).attr('data-unit');

		$('#NIP_PENILAI').html("").html(nip);
		$('#Nama_PENILAI').html("").html(nama);
		$('#Pangkat_PENILAI').html("").html(pangkat);
		$('#Eselon_PENILAI').html("").html(eselon);
		$('#Jabatan_PENILAI').html("").html(jabatan);
		$('#Unit_PENILAI').html("").html(unit);
		$(".info-penilai").show();
	}

	function  setAtasanPejabatPenilai(ini) {
		var TenderRUPId = $('option:selected', ini).attr('data-TenderRUPId');
		var nip = $('option:selected', ini).attr('data-nip');
		var nama = $('option:selected', ini).attr('data-nama');
		var pangkat = $('option:selected', ini).attr('data-pangkat');
		var eselon = $('option:selected', ini).attr('data-eselon');
		var jabatan = $('option:selected', ini).attr('data-jabatan');
		var unit = $('option:selected', ini).attr('data-unit');

		$('#NIP_ATASAN').html("").html(nip);
		$('#Nama_ATASAN').html("").html(nama);
		$('#Pangkat_ATASAN').html("").html(pangkat);
		$('#Eselon_ATASAN').html("").html(eselon);
		$('#Jabatan_ATASAN').html("").html(jabatan);
		$('#Unit_ATASAN').html("").html(unit);
		$(".info-atasan").show();
	}

	function validasi_input(form){
		//alert(form.skp_tanggal_mulai_submit.value);
  		/*if (form.skp_status_pegawai.value == ""){
    		alert("Status Pegawai masih kosong!");
    		form.skp_status_pegawai.focus();
    		return (false);
  			}
  		else if (form.skp_tanggal_mulai_submit.value == ""){
    		alert("Tanggal Mulai masih kosong!");
    		form.skp_tanggal_mulai_submit.focus();
    		return (false);
  			}
  		else if (form.skp_tanggal_selesai_submit.value == ""){
    		alert("Tanggal Selesai masih kosong!");
    		form.skp_tanggal_selesai_submit.focus();
    		return (false);
  			}
  		else if (form.skp_catatan.value == ""){
    		alert("Catatan masih kosong!");
    		form.skp_catatan.focus();
    		return (false);
  			}
  		else if (form.skp_pejabat_penilai_nip.value == ""){
    		alert("Pejabat Penilai masih kosong!");
    		form.skp_pejabat_penilai_nip.focus();
    		return (false);
  			}
  		else if (form.skp_atasan_pejabat_penilai_nip.value == ""){
    		alert("Atasan Pejabat Penilai masih kosong!");
    		form.skp_atasan_pejabat_penilai_nip.focus();
    		return (false);
  			}
  		else if (form.cuti_detail_alamat.value == ""){
    		alert("Alamat selama menjalankan cuti masih kosong!");
    		form.cuti_detail_alamat.focus();
    		return (false);
  			}*/
  		return (true);
	}
</script>