<style type="text/css">
.control-label{
	font-weight: bold;
}
</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Kepangkatan</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nip"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tempat Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_tempat_lahir"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Jabatan</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama_jabatan"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Unit Kerja</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["unit_kerja_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<a href="<?php echo base_url();?>kepegawaian/kepangkatan" class="btn btn-xs btn-warning">Kembali</a></td>

							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Daftar Data Kepangkatan</span>
					<div class="col-md-12">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
							<thead>
								<tr>
									<th class="text-center" width="5%">Golongan</th>
									<th class="text-center" width="10%">Masa Kerja Golongan</th>
									<th class="text-center" width="10%">TMT Golongan</th>									
									<th class="text-center" width="40%%">Pejabat</th>
									<th class="text-center" width="25%">No. SK</th>
									<th class="text-center"  width="10%">Tgl. SK</th>
									<th class="text-center"  width="10%">Keterangan SK</th>
									<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($history)>0){?>
								<?php foreach ($history as $h) {  ?>
								<tr>
									<td><?php echo $h["golongan_nama"];?></td>
									<td><?php echo $h["kepangkatan_tahun_masa_kerja_golongan"].' Tahun '.$h["kepangkatan_bulan_masa_kerja_golongan"].' Bulan '?></td>
									<td><?php echo dateEnToId($h["kepangkatan_tanggal_tmt_golongan"], 'd-m-Y');?></td>
									<td><?php echo $h["kepangkatan_nama_pejabat"];?></td>
									<td><?php echo $h["kepangkatan_nomor_sk"];?></td>									
									<td><?php echo dateEnToId($h["kepangkatan_tanggal_sk"], 'd-m-Y');?></td>
									<td><?php echo $h["kepangkatan_jenis_sk_text"];?></td>
									<td>
										<ul class="icons-list">
											<li><a href="javascript:void(0)" onclick="update_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
											<li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>
											<li><a onclick="set_non_aktif(<?php echo $h["kepangkatan_id"];?>,<?php echo $h["kepangkatan_pegawai_id"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
										</ul>
									</td>
								</tr>
								<?php } ?>
								<?php }else{ ?>
								<tr>
									<td class="text-center" colspan="9">Data Belum Tersedia</td>
								</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<td class="text-right" colspan="9"><a href="javascript:void(0)" onclick="tambah_data();" class="btn btn-xs btn-primary btn-tfoot-table">Tambah Data</a>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="col-md-12 form_tambah_data" style="display: none;">
						<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan Data Kepangkatan</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<!-- <div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Golongan CPNS</label>
									<div class="col-md-9">
										<div class="row">
											<div class="form-group form-group-xs">
												<label class="col-md-1 control-label">Golongan</label>
												<div class="col-md-3">
													<select name="pegawai_cpns_golongan_id" id="pegawai_cpns_golongan_id" data-placeholder="Pilih Golongan" class="select-size-xs ga_wajibzz">
														<option></option>
														<?php foreach ($golongan as $i) {
															echo '<option value="'.$i["golongan_id"].'" '.(($pegawai["pegawai_cpns_golongan_id"]==$i["golongan_id"])?"selected":"").'>'.$i["golongan_nama"].'</option>';
														}?>
													</select>
												</div>
												<label class="col-md-1 control-label">TMT</label>
												<div class="col-md-3">
													<input type="text" name="pegawai_cpns_tanggal_tmt" id="pegawai_cpns_tanggal_tmt" value="<?php echo dateEnToId($pegawai["pegawai_cpns_tanggal_tmt"], 'd-m-Y');?>" class="form-control input-xs ga_wajibzz pickttl">
												</div>
											</div>
										</div>
										
									</div>
								</div> -->
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Golongan</label>
									<div class="col-md-9">
										<select name="kepangkatan_golongan_id" id="kepangkatan_golongan_id" data-placeholder="Golongan" class="select-size-xs wajib" >
											<option value=""></option>
											<?php foreach ($golongan as $i) {
												echo '<option value="'.$i["golongan_id"].'" >'.$i["golongan_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Masa Kerja Golongan</label>
									<div class="col-md-9">
										<div class="row">
											<div class="form-group form-group-xs">
												<label class="col-md-1 control-label">Tahun</label>
												<div class="col-md-3">
													<input type="text" name="kepangkatan_tahun_masa_kerja_golongan" id="kepangkatan_tahun_masa_kerja_golongan" value="0" class="form-control input-xs ga_wajibzz">
												</div>
												<label class="col-md-1 control-label">Bulan</label>
												<div class="col-md-3">
													<input type="text" name="kepangkatan_bulan_masa_kerja_golongan" id="kepangkatan_bulan_masa_kerja_golongan" value="0" class="form-control input-xs ga_wajibzz">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">TMT Golongan</label>
									<div class="col-md-9">
										<input type="text" name="kepangkatan_tanggal_tmt_golongan" id="kepangkatan_tanggal_tmt_golongan" class="form-control input-xs wajib pickttl">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Nomor SK</label>
									<div class="col-md-9">
										<input type="text" name="kepangkatan_nomor_sk" id="kepangkatan_nomor_sk" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tanggal SK</label>
									<div class="col-md-9">
										<input type="text" name="kepangkatan_tanggal_sk" id="kepangkatan_tanggal_sk" class="form-control input-xs wajib pickttl">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Pejabat (diisi jabatan penandatangan)</label>
									<div class="col-md-9">
										<input type="text" name="kepangkatan_nama_pejabat" id="kepangkatan_nama_pejabat" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Jenis SK</label>
									<div class="col-md-9">
										<select name="kepangkatan_jenis_sk" id="kepangkatan_jenis_sk" data-placeholder="Jenis SK" class="select-size-xs wajib" >
											<option value=""></option>
											<option value="1">CPNS</option>
											<option value="2">PNS</option>
											<option value="3">KP</option>
										</select>
									</div>
								</div>								
								<div class="form-group form-group-xs">
									<div class="col-md-9 col-md-offset-3">
										<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
										<input type="hidden" name="kepangkatan_pegawai_id" id="kepangkatan_pegawai_id" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_id"];?>">
										<input type="hidden" name="kepangkatan_pegawai_nip" id="kepangkatan_pegawai_nip" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_nip"];?>">
										<input type="hidden" name="isUpdate" id="isUpdate" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="kepangkatan_id" id="kepangkatan_id" class="form-control input-xs wajib" value="0">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 form_lihat_data" style="display: none;">
						<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Lihat Data Kepangkatan</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Golongan</label>
									<div class="col-md-9">
										<div class="form-control-static golongan_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">TMT Golongan</label>
									<div class="col-md-9">
										<div class="form-control-static kepangkatan_tanggal_tmt_golongan">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Pejabat</label>
									<div class="col-md-9">
										<div class="form-control-static kepangkatan_nama_pejabat">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-lg-3 control-label">Nomor SK</label>
									<div class="col-lg-9">
										<div class="form-control-static kepangkatan_nomor_sk">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tanggal SK</label>
									<div class="col-md-9">
										<div class="form-control-static kepangkatan_tanggal_sk">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tahun Masa Kerja Golongan</label>
									<div class="col-md-9">
										<div class="form-control-static kepangkatan_tahun_masa_kerja_golongan">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Bulan Masa Kerja Golongan</label>
									<div class="col-md-9">
										<div class="form-control-static kepangkatan_bulan_masa_kerja_golongan">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<div class="col-md-9 col-md-offset-3">
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>
	<script type="text/javascript">
		function set_non_aktif(id,pegawai_id){
			$('#text_konfirmasi').html('Anda yakin menghapus data ini ?');
			$('#konfirmasipenyimpanan').modal('show');
			$('#setujukonfirmasibutton').unbind();
			$('#setujukonfirmasibutton').on('click', function () {
				$('#konfirmasipenyimpanan').modal('hide');
				$('.angkadoank').inputmask('remove');
				$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
				window.location.href = base_url+'kepegawaian/kepangkatan/hapus/'+id+'/'+pegawai_id;
			});
		}
		function lihat_data(data){
			$('.form_tambah_data').hide();
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					lihat_data(data);
				});
			}else{
				$('.form_lihat_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_lihat_data").offset().top-60
					}, 500);

					var golongan_nama = datajson.golongan_nama;
					var kepangkatan_tanggal_tmt_golongan = datajson.kepangkatan_tanggal_tmt_golongan;
					var kepangkatan_nama_pejabat = datajson.kepangkatan_nama_pejabat;
					var kepangkatan_nomor_sk = datajson.kepangkatan_nomor_sk;
					var kepangkatan_tanggal_sk = datajson.kepangkatan_tanggal_sk;
					var kepangkatan_tahun_masa_kerja_golongan = datajson.kepangkatan_tahun_masa_kerja_golongan;
					var kepangkatan_bulan_masa_kerja_golongan = datajson.kepangkatan_bulan_masa_kerja_golongan;

					$(".golongan_nama").html(golongan_nama);
					$(".kepangkatan_tanggal_tmt_golongan").html(kepangkatan_tanggal_tmt_golongan);
					$(".kepangkatan_nama_pejabat").html(kepangkatan_nama_pejabat);
					$(".kepangkatan_nomor_sk").html(kepangkatan_nomor_sk);
					$(".kepangkatan_tanggal_sk").html(kepangkatan_tanggal_sk);
					$(".kepangkatan_tahun_masa_kerja_golongan").html(kepangkatan_tahun_masa_kerja_golongan);
					$(".kepangkatan_bulan_masa_kerja_golongan").html(kepangkatan_bulan_masa_kerja_golongan);

				});
			}
		}
		function update_data(data){
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			
			$('.form_lihat_data').hide();
			$('.label_aksi_form').html('Form Perubahan Data Kepangkatan');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					update_data(data);
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
					$('#isUpdate').val(1);

					var kepangkatan_id = datajson.kepangkatan_id; 
					var kepangkatan_pegawai_id = datajson.kepangkatan_pegawai_id; 
					var kepangkatan_pegawai_nip = datajson.kepangkatan_pegawai_nip; 
					var kepangkatan_golongan_id = datajson.kepangkatan_golongan_id; 
					var kepangkatan_tanggal_tmt_golongan = datajson.kepangkatan_tanggal_tmt_golongan; 
					var kepangkatan_nama_pejabat = datajson.kepangkatan_nama_pejabat; 
					var kepangkatan_nomor_sk = datajson.kepangkatan_nomor_sk; 
					var kepangkatan_tanggal_sk = datajson.kepangkatan_tanggal_sk; 
					var kepangkatan_tahun_masa_kerja_golongan = datajson.kepangkatan_tahun_masa_kerja_golongan; 
					var kepangkatan_bulan_masa_kerja_golongan = datajson.kepangkatan_bulan_masa_kerja_golongan;
					var kepangkatan_jenis_sk = datajson.kepangkatan_jenis_sk; 

					//alert(Tampil);

					$("#kepangkatan_id").val(kepangkatan_id);
					$("#kepangkatan_pegawai_id").val(kepangkatan_pegawai_id);
					$("#kepangkatan_pegawai_nip").val(kepangkatan_pegawai_nip);
					$("#kepangkatan_tanggal_tmt_golongan").val(kepangkatan_tanggal_tmt_golongan);
					$("#kepangkatan_nama_pejabat").val(kepangkatan_nama_pejabat);
					$("#kepangkatan_nomor_sk").val(kepangkatan_nomor_sk);
					$("#kepangkatan_tanggal_sk").val(kepangkatan_tanggal_sk);
					$("#kepangkatan_tahun_masa_kerja_golongan").val(kepangkatan_tahun_masa_kerja_golongan);
					$("#kepangkatan_bulan_masa_kerja_golongan").val(kepangkatan_bulan_masa_kerja_golongan);

					$("#kepangkatan_golongan_id").val(kepangkatan_golongan_id);
					$('#kepangkatan_golongan_id').trigger('change');
					$("#kepangkatan_jenis_sk").val(kepangkatan_jenis_sk);
					$('#kepangkatan_jenis_sk').trigger('change');
				});
			}
		}

		function tambah_data(){
			$('.form_lihat_data').hide();
			reset_form();
			$('.label_aksi_form').html('Form Penambahan Data Kepangkatan');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					$('.form_tambah_data').slideDown('fast',function(){
						$('html, body').animate({
							scrollTop: $(".form_tambah_data").offset().top-60
						}, 500);
						
					});
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
				});
			}
		}
		function batal_data(){
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					reset_form();
				});
			}
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					reset_form();
				});
			}
		}
		function reset_form(){
			// $("#kepangkatan_id").val('');
			// $("#kepangkatan_pegawai_id").val('');
			$("#kepangkatan_pegawai_nip").val('');
			$("#kepangkatan_tanggal_tmt_golongan").val('');
			$("#kepangkatan_nama_pejabat").val('');
			$("#kepangkatan_nomor_sk").val('');
			$("#kepangkatan_tanggal_sk").val('');
			$("#kepangkatan_tahun_masa_kerja_golongan").val('');
			$("#kepangkatan_bulan_masa_kerja_golongan").val('');
			$("#kepangkatan_golongan_id").val('');
			$('#kepangkatan_golongan_id').trigger('change');
			$('.validation-error-label').css('display','none');
		}
		
		$(document).ready(function() {

			var d = new Date();

			$('.pickttl').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});
			$('.pickttlstart').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});

			$('.pickttlend').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});

		//On value change
		$(".pickttlstart").change(function(e) {
	    	//var date = rangeDemoConv.parse($(".pickttlstar").val()).getTime();
	    	//alert(date);
	    	//min: new Date(2015,3,20),

	    	var mulai1 = $("#diklat_tanggal_mulai").val().split(" ");
	    	var tgl = mulai1[0];
	    	var bulan = mulai1[1];
	    	var tahun = mulai1[2];

	    	$('.pickttlend').pickadate({
	    		monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
	    		weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
	    		selectMonths: true,
	    		selectYears: 80,
	    		min: new Date(tahun, 5, 20),
	    		max: true,
	    		today:'Hari ini',
	    		clear: 'Hapus',
	    		close: 'Keluar',
	    		format: 'dd mmmm yyyy',
	    		formatSubmit: 'yyyy-mm-dd'
	    	});

	    	var yesterday  = new Date((new Date()).valueOf()-1000*60*60*24);
	    	var neededDates = datas.prdetail.PRD_DATE_NEED;
	    	var neededDate = neededDates.split('-');

	    	var tglneed = $('#tanggal_dibutuhkan').pickadate({
	    		monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
	    		monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agt', 'Sep', 'Okt', 'Nov', 'Des'],
	    		weekdaysFull: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu'],
	    		weekdaysShort: ['Min', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
	    		selectYears: true,
	    		selectMonths: true,
	    		format: 'd mmmm yyyy',
	    		formatSubmit: 'yyyy-mm-dd',
	    		today: 'Hari ini',
	    		clear: 'Reset',
	    		close: 'Batal',
	    		disable: [
	    		{ from: [0,0,0], to: yesterday }
	    		]
	    	});

	    	var dpicker = tglneed.pickadate('picker');
	    	dpicker.set('select', [neededDate[0], (neededDate[1]-1), neededDate[2]]);
	        // try {
	        //     var fromDay = rangeDemoConv.parse($("#rangeDemoStart").val()).getTime();
	        //     var dayLater = new Date(fromDay+oneDay);
	        //         dayLater.setHours(0,0,0,0);
	        //     var ninetyDaysLater = new Date(fromDay+(90*oneDay));
	        //         ninetyDaysLater.setHours(23,59,59,999);

	        //     // End date
	        //     $("#rangeDemoFinish")
	        //     .AnyTime_noPicker()
	        //     .removeAttr("disabled")
	        //     .val(rangeDemoConv.format(dayLater))
	        //     .AnyTime_picker({
	        //         earliest: dayLater,
	        //         format: rangeDemoFormat,
	        //         latest: ninetyDaysLater
	        //     });
	        // }
	        // catch(e) {
	        //     // Disable End date field
	        //     $("#rangeDemoFinish").val("").attr("disabled","disabled");
	        // }
	    });

	});

function ToJavaScriptDateViewDoank(value) {
        //var pattern = /Date\(([^)]+)\)/;
        //var results = pattern.exec(value);
        //var dt = new Date(parseFloat(results[1]));
        var MonthInd = GetMonthIndInt((value.getMonth() + 1));
        return value.getDate() + " " + MonthInd + " " + value.getFullYear();
    }

    function GetMonthIndInt(month) {
    	switch (month) {
    		case 1:
    		return "Januari"
    		break;
    		case 2:
    		return "Februari"
    		break;
    		case 3:
    		return "Maret"
    		break;
    		case 4:
    		return "April"
    		break;
    		case 5:
    		return "Mei"
    		break;
    		case 6:
    		return "Juni"
    		break;
    		case 7:
    		return "Juli"
    		break;
    		case 8:
    		return "Agustus"
    		break;
    		case 9:
    		return "September"
    		break;
    		case 10:
    		return "Oktober"
    		break;
    		case 11:
    		return "November"
    		break;
    		case 12:
    		return "Desember"
    		break;
    		default:
    		return "Undefined"
    	}
    }

    $('[name="kepangkatan_tahun_masa_kerja_golongan"]').formatter({
			pattern: '{{99}}'
	});
	$('[name="kepangkatan_bulan_masa_kerja_golongan"]').formatter({
			pattern: '{{99}}'
	});
</script>
