<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">DP3</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nip"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tempat Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_tempat_lahir"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Jabatan</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama_jabatan"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Unit Kerja</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["unit_kerja_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<a href="<?php echo base_url();?>kepegawaian/dp3" class="btn btn-xs btn-warning">Kembali</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Daftar Data DP3</span>
					<div class="col-md-12">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
							<thead>
								<tr>
									<th class="text-center">Tahun Penilaian</th>
									<th class="text-center">Kesetiaan</th>
									<th class="text-center">Prestasi</th>
									<th class="text-center">Tanggung Jawab</th>
									<th class="text-center">Ketaatan</th>
									<th class="text-center">Kejujuran</th>
									<th class="text-center">Kerjasama</th>
									<th class="text-center">Prakarsa</th>
									<th class="text-center">Kepemimpinan</th>
									<th class="text-center">Rata-Rata</th>
									<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($history)>0){?>
									<?php foreach ($history as $h) {  ?>
										<tr>
											<td><?php echo $h["dp3_tahun_penilaian"];?></td>
											<td><?php echo $h["dp3_kesetiaan"];?></td>
											<td><?php echo $h["dp3_prestasi"];?></td>
											<td><?php echo $h["dp3_tanggung_jawab"];?></td>
											<td><?php echo $h["dp3_ketaatan"];?></td>
											<td><?php echo $h["dp3_kejujuran"];?></td>
											<td><?php echo $h["dp3_kerjasama"];?></td>
											<td><?php echo $h["dp3_prakarsa"];?></td>
											<td><?php echo $h["dp3_kepemimpinan"];?></td>
											<?php
											 	if($h["dp3_kepemimpinan"] > 1){
												$jumlah = $h["dp3_kesetiaan"]+$h["dp3_prestasi"]+$h["dp3_tanggung_jawab"]+$h["dp3_ketaatan"]+$h["dp3_kejujuran"]+$h["dp3_kerjasama"]+$h["dp3_prakarsa"]+$h["dp3_kepemimpinan"];
												$rata_rata = $jumlah/8;
												}else{
												$jumlah = $h["dp3_kesetiaan"]+$h["dp3_prestasi"]+$h["dp3_tanggung_jawab"]+$h["dp3_ketaatan"]+$h["dp3_kejujuran"]+$h["dp3_kerjasama"]+$h["dp3_prakarsa"];
												$rata_rata = $jumlah/7;
												}
											 ?>
											<td><?php echo round($rata_rata, 2);?></td>
											<td>
												<ul class="icons-list">
													<li><a href="javascript:void(0)" onclick="update_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
													<li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>
													<li><a onclick="set_non_aktif(<?php echo $h["dp3_id"];?>,<?php echo $h["dp3_pegawai_id"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
												</ul>
											</td>
										</tr>
									<?php } ?>
								<?php }else{ ?>
									<tr>
										<td class="text-center" colspan="7">Data Belum Tersedia</td>
									</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<td class="text-right" colspan="12"><a href="javascript:void(0)" onclick="tambah_data();" class="btn btn-xs btn-primary btn-tfoot-table">Tambah Data</a>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="col-md-12 form_tambah_data" style="display: none;">
						<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan Data dp3</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">NIP Penilai</label>
									<div class="col-md-4">
										<input type="text" name="dp3_penilai_nip" id="dp3_penilai_nip" class="form-control input-xs wajib">
									</div>
									<div class="col-md-5">
										<select name="header_pegawai" id="header_pegawai"  data-placeholder="Pilih Penilai" class="select-size-xs gakwajib">
											<option value=""></option>
										</select>
									</div>
								</div>
								<!-- <div class="form-group form-group-xs">
									<label class="col-md-3 control-label">NIP Penilai</label>
									<div class="col-md-4">
										<input type="text" name="dp3_penilai_nip" id="dp3_penilai_nip" class="form-control input-xs wajib">
									</div>
									<div class="col-md-5">
										<select name="dp3_penilai_nips" id="dp3_penilai_nips" data-placeholder="Cari" class="select-size-xs" onchange="setPenilai(this)">
											<option value=""></option>
											<?php foreach ($atasan as $i) {
												echo '<option value="'.$i["pegawai_nip"].'" data-nip="'.$i["pegawai_nip"].'" data-nama="'.$i["pegawai_nama_lengkap"].'" data-jabatan="'.$i["pegawai_nama_jabatan"].'" data-golongan="'.$i["golongan_nama"].'" data-unit="'.$i["unit_kerja_nama"].'">'.$i["pegawai_nip"].'-'.$i["pegawai_nama"].'</option>';
											}?>
										</select>
									</div>
								</div> -->
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Nama Penilai</label>
									<div class="col-md-9">
										<input type="text" name="dp3_penilai_nama" id="dp3_penilai_nama" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Jabatan Penilai</label>
									<div class="col-md-9">
										<input type="text" name="dp3_penilai_jabatan" id="dp3_penilai_jabatan" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Golongan Penilai 
									</label>
									<div class="col-md-1">
										<input type="text" name="dp3_penilai_golongan" id="dp3_penilai_golongan" class="form-control input-xs wajib">
									</div>
									<span class="help-block">* Penulisan Golongan Menggunakan huruf besar Misalkan 1A, 4E</span>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Unit Kerja Penilai</label>
									<div class="col-md-9">
										<input type="text" name="dp3_penilai_unit_kerja" id="dp3_penilai_unit_kerja" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">NIP Atasan</label>
									<div class="col-md-4">
										<input type="text" name="dp3_atasan_nip" id="dp3_atasan_nip" class="form-control input-xs wajib">
									</div>
									<div class="col-md-5">
										<select name="header_pegawai_atasan" id="header_pegawai_atasan" data-placeholder="Pilih Atasan" class="select-size-xs gakwajib">
											<option value=""></option>
										</select>
									</div>
								</div>
								<!-- <div class="form-group form-group-xs">
									<label class="col-md-3 control-label">NIP Atasan</label>
									<div class="col-md-4">
										<input type="text" name="dp3_atasan_nip" id="dp3_atasan_nip" class="form-control input-xs wajib">
									</div>
									<div class="col-md-5">
										<select name="dp3_atasan_nips" id="dp3_atasan_nips" data-placeholder="Cari" class="select-size-xs" onchange="setAtasan(this)">
											<option value=""></option>
											<?php foreach ($atasan as $i) {
												echo '<option value="'.$i["pegawai_nip"].'" data-nip="'.$i["pegawai_nip"].'" data-nama="'.$i["pegawai_nama_lengkap"].'" data-jabatan="'.$i["pegawai_nama_jabatan"].'" data-golongan="'.$i["golongan_nama"].'" data-unit="'.$i["unit_kerja_nama"].'">'.$i["pegawai_nip"].'-'.$i["pegawai_nama"].'</option>';
											}?>
										</select>
									</div>
								</div> -->
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Nama Atasan</label>
									<div class="col-md-9">
										<input type="text" name="dp3_atasan_nama" id="dp3_atasan_nama" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Jabatan Atasan</label>
									<div class="col-md-9">
										<input type="text" name="dp3_atasan_jabatan" id="dp3_atasan_jabatan" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Golongan Atasan</label>
									<div class="col-md-1">
										<input type="text" name="dp3_atasan_golongan" id="dp3_atasan_golongan" class="form-control input-xs wajib">
									</div>
									<span class="help-block">* Penulisan Golongan Menggunakan huruf besar Misalkan 1A, 4E</span>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Unit Kerja Atasan</label>
									<div class="col-md-9">
										<input type="text" name="dp3_atasan_unit_kerja" id="dp3_atasan_unit_kerja" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tahun Penilaian</label>
									<div class="col-md-2">
										<select name="dp3_tahun_penilaian" id="dp3_tahun_penilaian" data-placeholder="Tahun" class="select-size-xs wajib">
											<option value=""></option>
											<?php
											$thn = date('Y');
											for($i=$thn;$i>=1950;$i--) {
												echo '<option value="'.$i.'">'.$i.'</option>';
											}
											?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Kesetiaan</label>
									<div class="col-md-2">
										<input type="number" min="0" max="100" name="dp3_kesetiaan" id="dp3_kesetiaan" class="form-control input-xs wajib">
									</div>
									<span class="help-block">(Diisi misal: 90.38)</span>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Prestasi</label>
									<div class="col-md-2">
										<input type="number" min="0" max="100" name="dp3_prestasi" id="dp3_prestasi" class="form-control input-xs wajib">
									</div>
									<span class="help-block">(Diisi misal: 90.38)</span>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tanggung Jawab</label>
									<div class="col-md-2">
										<input type="number" min="0" max="100" name="dp3_tanggung_jawab" id="dp3_tanggung_jawab" class="form-control input-xs wajib">
									</div>
									<span class="help-block">(Diisi misal: 90.38)</span>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Ketaatan</label>
									<div class="col-md-2">
										<input type="number" min="0" max="100" name="dp3_ketaatan" id="dp3_ketaatan" class="form-control input-xs wajib">
									</div>
									<span class="help-block">(Diisi misal: 90.38)</span>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Kejujuran</label>
									<div class="col-md-2">
										<input type="number" min="0" max="100" name="dp3_kejujuran" id="dp3_kejujuran" class="form-control input-xs wajib">
									</div>
									<span class="help-block">(Diisi misal: 90.38)</span>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Kerjasama</label>
									<div class="col-md-2">
										<input type="number" min="0" max="100" name="dp3_kerjasama" id="dp3_kerjasama" class="form-control input-xs wajib">
									</div>
									<span class="help-block">(Diisi misal: 90.38)</span>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Prakarsa</label>
									<div class="col-md-2">
										<input type="number" min="0" max="100" name="dp3_prakarsa" id="dp3_prakarsa" class="form-control input-xs wajib">
									</div>
									<span class="help-block">(Diisi misal: 90.38)</span>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Kepemimpinan</label>
									<div class="col-md-2">
										<input type="number" min="0" max="100" name="dp3_kepemimpinan" id="dp3_kepemimpinan" class="form-control input-xs wajib">
									</div>
									<span class="help-block">(apabila tidak ada nilai kepemimpinan diisi 0)</span>
								</div>
								<!-- <div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Rata-Rata</label>
									<div class="col-md-2"> -->
										<input type="hidden" min="0" max="100" name="dp3_rata_rata" id="dp3_rata_rata" class="form-control input-xs wajib">
									<!-- </div>
								</div> -->
								<div class="form-group form-group-xs">
									<div class="col-md-9 col-md-offset-3">
										<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
										<input type="hidden" name="dp3_pegawai_id" id="dp3_pegawai_id" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_id"];?>">
										<input type="hidden" name="dp3_pegawai_nip" id="dp3_pegawai_nip" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_nip"];?>">
										<input type="hidden" name="isUpdate" id="isUpdate" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="dp3_id" id="dp3_id" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="dp3_jurusan_code_tingkat_temp" id="dp3_jurusan_code_tingkat_temp" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="dp3_jurusan_code_temp" id="dp3_jurusan_code_temp" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="dp3_jurusan_code_fakultas_temp" id="dp3_jurusan_code_fakultas_temp" class="form-control input-xs wajib" value="0">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 form_lihat_data" style="display: none;">
						<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Lihat Data dp3</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">NIP Penilai</label>
									<div class="col-md-9">
										<div class="form-control-static dp3_penilai_nip">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Nama Penilai</label>
									<div class="col-md-9">
										<div class="form-control-static dp3_penilai_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Jabatan  Penilai</label>
									<div class="col-md-9">
										<div class="form-control-static dp3_penilai_jabatan">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Golongan  Penilai</label>
									<div class="col-md-9">
										<div class="form-control-static dp3_penilai_golongan">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Unit Kerja Penilai</label>
									<div class="col-md-9">
										<div class="form-control-static dp3_penilai_unit_kerja">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">NIP Atasan</label>
									<div class="col-md-9">
										<div class="form-control-static dp3_atasan_nip">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Nama Atasan</label>
									<div class="col-md-9">
										<div class="form-control-static dp3_atasan_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Jabatan Atasan</label>
									<div class="col-md-9">
										<div class="form-control-static dp3_atasan_jabatan">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Golongan Atasan</label>
									<div class="col-md-9">
										<div class="form-control-static dp3_atasan_golongan">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Unit Kerja Atasan</label>
									<div class="col-md-9">
										<div class="form-control-static dp3_atasan_unit_kerja">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tahun Penilaian</label>
									<div class="col-md-9">
										<div class="form-control-static dp3_tahun_penilaian">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Kesetiaan</label>
									<div class="col-md-9">
										<div class="form-control-static dp3_kesetiaan">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Prestasi</label>
									<div class="col-md-9">
										<div class="form-control-static dp3_prestasi">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tanggung Jawab</label>
									<div class="col-md-9">
										<div class="form-control-static dp3_tanggung_jawab">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Ketaatan</label>
									<div class="col-md-9">
										<div class="form-control-static dp3_ketaatan">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Kejujuran</label>
									<div class="col-md-9">
										<div class="form-control-static dp3_kejujuran">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Kerjasama</label>
									<div class="col-md-9">
										<div class="form-control-static dp3_kerjasama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Prakarsa</label>
									<div class="col-md-9">
										<div class="form-control-static dp3_prakarsa">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Kepemimpinan</label>
									<div class="col-md-9">
										<div class="form-control-static dp3_kepemimpinan">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Rata-Rata</label>
									<div class="col-md-9">
										<div class="form-control-static dp3_rata_rata">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<div class="col-md-9 col-md-offset-3">
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>
	<script type="text/javascript">
		$(":input").bind("keyup change", function(e) {
		   var dp3_kesetiaan = parseFloat($("#dp3_kesetiaan").val());
		   var dp3_prestasi = parseFloat($("#dp3_prestasi").val());
		   var dp3_tanggung_jawab = parseFloat($("#dp3_tanggung_jawab").val());
		   var dp3_ketaatan = parseFloat($("#dp3_ketaatan").val());
		   var dp3_kejujuran = parseFloat($("#dp3_kejujuran").val());
		   var dp3_kerjasama = parseFloat($("#dp3_kerjasama").val());
		   var dp3_prakarsa = parseFloat($("#dp3_prakarsa").val());
		   var dp3_kepemimpinan = parseFloat($("#dp3_kepemimpinan").val());
		   var dp3_rata_rata = (dp3_kesetiaan+dp3_prestasi+dp3_tanggung_jawab+dp3_ketaatan+dp3_kejujuran+dp3_kerjasama+dp3_prakarsa+dp3_kepemimpinan)/8;
		   $("#dp3_rata_rata").val(dp3_rata_rata);
		});
		function set_non_aktif(id,pegawai_id){
			$('#text_konfirmasi').html('Anda yakin menghapus data ini ?');
			$('#konfirmasipenyimpanan').modal('show');
			$('#setujukonfirmasibutton').unbind();
			$('#setujukonfirmasibutton').on('click', function () {
				$('#konfirmasipenyimpanan').modal('hide');
				$('.angkadoank').inputmask('remove');
				$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
				window.location.href = base_url+'kepegawaian/dp3/hapus/'+id+'/'+pegawai_id;
			});
		}
		function lihat_data(data){
			$('.form_tambah_data').hide();
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			// console.log(datajson);
			reset_form();
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					lihat_data(data);
				});
			}else{
				$('.form_lihat_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_lihat_data").offset().top-60
					}, 500);
					var dp3_id = datajson.dp3_id;
					var dp3_penilai_nip = datajson.dp3_penilai_nip;
					var dp3_penilai_nama = datajson.dp3_penilai_nama;
					var dp3_penilai_jabatan = datajson.dp3_penilai_jabatan;
					var dp3_penilai_golongan = datajson.dp3_penilai_golongan;
					var dp3_penilai_unit_kerja = datajson.dp3_penilai_unit_kerja;
					var dp3_atasan_nip = datajson.dp3_atasan_nip;
					var dp3_atasan_nama = datajson.dp3_atasan_nama;
					var dp3_atasan_jabatan = datajson.dp3_atasan_jabatan;
					var dp3_atasan_golongan = datajson.dp3_atasan_golongan;
					var dp3_atasan_unit_kerja = datajson.dp3_atasan_unit_kerja;
					var dp3_tahun_penilaian = datajson.dp3_tahun_penilaian;
					var dp3_kesetiaan = datajson.dp3_kesetiaan;
					var dp3_prestasi = datajson.dp3_prestasi;
					var dp3_tanggung_jawab = datajson.dp3_tanggung_jawab;
					var dp3_ketaatan = datajson.dp3_ketaatan;
					var dp3_kejujuran = datajson.dp3_kejujuran;
					var dp3_kerjasama = datajson.dp3_kerjasama;
					var dp3_prakarsa = datajson.dp3_prakarsa;
					var dp3_kepemimpinan = datajson.dp3_kepemimpinan;
					var dp3_rata_rata = datajson.dp3_rata_rata;
					if(dp3_kepemimpinan > 1){
						var jumlah = dp3_kesetiaan+dp3_prestasi+dp3_tanggung_jawab+dp3_ketaatan+dp3_kejujuran+dp3_kerjasama+dp3_prakarsa+dp3_kepemimpinan;
						var rata_rata = Math.round(jumlah/8, 2);
					}else{
						var jumlah = dp3_kesetiaan+dp3_prestasi+dp3_tanggung_jawab+dp3_ketaatan+dp3_kejujuran+dp3_kerjasama+dp3_prakarsa;
						var rata_rata = Math.round(jumlah/7, 2);
					}

					$('.dp3_penilai_nip').html(dp3_penilai_nip);
					$('.dp3_penilai_nama').html(dp3_penilai_nama);
					$('.dp3_penilai_jabatan').html(dp3_penilai_jabatan);
					$('.dp3_penilai_golongan').html(dp3_penilai_golongan);
					$('.dp3_penilai_unit_kerja').html(dp3_penilai_unit_kerja);
					$('.dp3_atasan_nip').html(dp3_atasan_nip);
					$('.dp3_atasan_nama').html(dp3_atasan_nama);
					$('.dp3_atasan_jabatan').html(dp3_atasan_jabatan);
					$('.dp3_atasan_golongan').html(dp3_atasan_golongan);
					$('.dp3_atasan_unit_kerja').html(dp3_atasan_unit_kerja);
					$('.dp3_tahun_penilaian').html(dp3_tahun_penilaian);
					$('.dp3_kesetiaan').html(dp3_kesetiaan);
					$('.dp3_prestasi').html(dp3_prestasi);
					$('.dp3_tanggung_jawab').html(dp3_tanggung_jawab);
					$('.dp3_ketaatan').html(dp3_ketaatan);
					$('.dp3_kejujuran').html(dp3_kejujuran);
					$('.dp3_kerjasama').html(dp3_kerjasama);
					$('.dp3_prakarsa').html(dp3_prakarsa);
					$('.dp3_kepemimpinan').html(dp3_kepemimpinan);
					$('.dp3_rata_rata').html(rata_rata);
				});
			}
		}
		function update_data(data){
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			$('.form_lihat_data').hide();
			$('.label_aksi_form').html('Form Perubahan Data dp3');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					update_data(data);
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
					$('#isUpdate').val(1);
					var dp3_id = datajson.dp3_id;
					// var dp3_penilai_nips = datajson.dp3_penilai_nip;
					var dp3_penilai_nip = datajson.dp3_penilai_nip;
					var dp3_penilai_nama = datajson.dp3_penilai_nama;
					var dp3_penilai_jabatan = datajson.dp3_penilai_jabatan;
					var dp3_penilai_golongan = datajson.dp3_penilai_golongan;
					var dp3_penilai_unit_kerja = datajson.dp3_penilai_unit_kerja;
					// var dp3_atasan_nips = datajson.dp3_atasan_nip;
					var dp3_atasan_nip = datajson.dp3_atasan_nip;
					var dp3_atasan_nama = datajson.dp3_atasan_nama;
					var dp3_atasan_jabatan = datajson.dp3_atasan_jabatan;
					var dp3_atasan_golongan = datajson.dp3_atasan_golongan;
					var dp3_atasan_unit_kerja = datajson.dp3_atasan_unit_kerja;
					var dp3_tahun_penilaian = datajson.dp3_tahun_penilaian;
					var dp3_kesetiaan = datajson.dp3_kesetiaan;
					var dp3_prestasi = datajson.dp3_prestasi;
					var dp3_tanggung_jawab = datajson.dp3_tanggung_jawab;
					var dp3_ketaatan = datajson.dp3_ketaatan;
					var dp3_kejujuran = datajson.dp3_kejujuran;
					var dp3_kerjasama = datajson.dp3_kerjasama;
					var dp3_prakarsa = datajson.dp3_prakarsa;
					var dp3_kepemimpinan = datajson.dp3_kepemimpinan;
					var dp3_rata_rata = datajson.dp3_rata_rata;

					$('#dp3_id').val(dp3_id);
					$('#dp3_penilai_nip').val(dp3_penilai_nip);
					$('#dp3_penilai_nama').val(dp3_penilai_nama);
					$('#dp3_penilai_jabatan').val(dp3_penilai_jabatan);
					$('#dp3_penilai_golongan').val(dp3_penilai_golongan);
					$('#dp3_penilai_unit_kerja').val(dp3_penilai_unit_kerja);
					$('#dp3_atasan_nip').val(dp3_atasan_nip);
					$('#dp3_atasan_nama').val(dp3_atasan_nama);
					$('#dp3_atasan_jabatan').val(dp3_atasan_jabatan);
					$('#dp3_atasan_golongan').val(dp3_atasan_golongan);
					$('#dp3_atasan_unit_kerja').val(dp3_atasan_unit_kerja);
					$('#dp3_kesetiaan').val(dp3_kesetiaan);
					$('#dp3_prestasi').val(dp3_prestasi);
					$('#dp3_tanggung_jawab').val(dp3_tanggung_jawab);
					$('#dp3_ketaatan').val(dp3_ketaatan);
					$('#dp3_kejujuran').val(dp3_kejujuran);
					$('#dp3_kerjasama').val(dp3_kerjasama);
					$('#dp3_prakarsa').val(dp3_prakarsa);
					$('#dp3_kepemimpinan').val(dp3_kepemimpinan);
					$('#dp3_rata_rata').val(dp3_rata_rata);
					$('#dp3_id').val(datajson.dp3_id);

					// $('#dp3_atasan_nips').select2('val', '');
					// $('#dp3_atasan_nips').val(dp3_atasan_nip);
					// $('#dp3_atasan_nips').trigger('change');

					// $('#dp3_penilai_nips').select2('val', '');
					// $('#dp3_penilai_nips').val(dp3_penilai_nip);
					// $('#dp3_penilai_nips').trigger('change');

					$('#dp3_tahun_penilaian').select2('val', '');
					$('#dp3_tahun_penilaian').val(dp3_tahun_penilaian);
					$('#dp3_tahun_penilaian').trigger('change');

				});
			}
		}
		function tambah_data(){
			$('.form_lihat_data').hide();
			reset_form();
			$('.label_aksi_form').html('Form Penambahan Data dp3');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					$('.form_tambah_data').slideDown('fast',function(){
						$('html, body').animate({
							scrollTop: $(".form_tambah_data").offset().top-60
						}, 500);
						
					});
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
				});
			}
		}
		function batal_data(){
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					reset_form();
				});
			}
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					reset_form();
				});
			}
		}
		function reset_form(){
			$('#dp3_id').val(0);
			$('#dp3_penilai_nips').select2('val', ' ');
			$('#dp3_penilai_nip').val('');
			$('#dp3_atasan_nips').select2('val', ' ');
			$('#dp3_atasan_nip').val('');
			$('#dp3_tahun_penilaian').select2('val', ' ');
			$('#dp3_kesetiaan').val('');
			$('#dp3_prestasi').val('');
			$('#dp3_tanggung_jawab').val('');
			$('#dp3_ketaatan').val('');
			$('#dp3_kejujuran').val('');
			$('#dp3_kerjasama').val('');
			$('#dp3_prakarsa').val('');
			$('#dp3_kepemimpinan').val('');
			$('#dp3_rata_rata').val('');
			$('#dp3_id').val(0);
			$('.validation-error-label').css('display','none');
		}

		function setPenilai(ini) {
			var TenderRUPId = $('option:selected', ini).attr('data-TenderRUPId');
			var nip = $('option:selected', ini).attr('data-nip');
			var nama = $('option:selected', ini).attr('data-nama');
			var jabatan = $('option:selected', ini).attr('data-jabatan');
			var golongan = $('option:selected', ini).attr('data-golongan');
			var unit = $('option:selected', ini).attr('data-unit');

			$('#dp3_penilai_nip').val("").val(nip);
			$('#dp3_penilai_nama').val("").val(nama);
			$('#dp3_penilai_jabatan').val("").val(jabatan);
			$('#dp3_penilai_golongan').val("").val(golongan);
			$('#dp3_penilai_unit_kerja').val("").val(unit);
			$(".info-atasan").show();
		}

		function setAtasan(ini) {
			var TenderRUPId = $('option:selected', ini).attr('data-TenderRUPId');
			var nip = $('option:selected', ini).attr('data-nip');
			var nama = $('option:selected', ini).attr('data-nama');
			var jabatan = $('option:selected', ini).attr('data-jabatan');
			var golongan = $('option:selected', ini).attr('data-golongan');
			var unit = $('option:selected', ini).attr('data-unit');

			$('#dp3_atasan_nip').val("").val(nip);
			$('#dp3_atasan_nama').val("").val(nama);
			$('#dp3_atasan_jabatan').val("").val(jabatan);
			$('#dp3_atasan_golongan').val("").val(golongan);
			$('#dp3_atasan_unit_kerja').val("").val(unit);
			$(".info-atasan").show();
		}

		$('#header_pegawai').on('change', function() {
			var pegawai_id = $(this).val();

			$.ajax({
				url: base_url+'kepegawaian/dp3/getDataPegawai',
				type: "post",
				dataType: 'json',
				data: { pegawai_id: pegawai_id},
				success: function (data) {
					if(data.pegawai_penilai!=null){
						$('#dp3_penilai_nip').val(data.pegawai_penilai.pegawai_nip);
						$('#dp3_penilai_nama').val(data.pegawai_penilai.pegawai_nama_lengkap);
						$('#dp3_penilai_jabatan').val(data.pegawai_penilai.pegawai_nama_jabatan);
						$('#dp3_penilai_golongan').val(data.pegawai_penilai.golongan_kode);
						$('#dp3_penilai_unit_kerja').val(data.pegawai_penilai.unit_kerja_nama);
					}else{
						$('#dp3_penilai_nip').val('-');
						$('#dp3_penilai_nama').val('-');
						$('#dp3_penilai_jabatan').val('-');
						$('#dp3_penilai_golongan').val('-');
						$('#dp3_penilai_unit_kerja').val('-');
					}
				}
			});
		});
		$('#header_pegawai_atasan').on('change', function() {
			var pegawai_id = $(this).val();

			$.ajax({
				url: base_url+'kepegawaian/dp3/getDataPegawai',
				type: "post",
				dataType: 'json',
				data: { pegawai_id: pegawai_id},
				success: function (data) {
					if(data.pegawai_penilai!=null){
						$('#dp3_atasan_nip').val(data.pegawai_penilai.pegawai_nip);
						$('#dp3_atasan_nama').val(data.pegawai_penilai.pegawai_nama_lengkap);
						$('#dp3_atasan_jabatan').val(data.pegawai_penilai.pegawai_nama_jabatan);
						$('#dp3_atasan_golongan').val(data.pegawai_penilai.golongan_kode);
						$('#dp3_atasan_unit_kerja').val(data.pegawai_penilai.unit_kerja_nama);
					}else{
						$('#dp3_atasan_nip').val('-');
						$('#dp3_atasan_nama').val('-');
						$('#dp3_atasan_jabatan').val('-');
						$('#dp3_atasan_golongan').val('-');
						$('#dp3_atasan_unit_kerja').val('-');
					}
				}
			});
		});
	$(document).ready(function() {
		$("#header_pegawai").select2({
			ajax: {
				url: base_url+"kepegawaian/dp3/getDataListPegawai",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						q: params.term, 
						page: params.page,
						
					};
				},
				processResults: function (data, params) {
					params.page = params.page || 1;

					return {
						results: data.items,
						pagination: {
							more: (params.page * 5) < data.total_count
						}
					};
				},
				cache: true
			},
			containerCssClass: 'select-xs',
			placeholder: 'Pencarian Pegawai',
			escapeMarkup: function (markup) { return markup; }, 
			minimumInputLength: 3,
			templateResult: formatRepoPegawai,
			templateSelection: formatRepoPegawaiSelection,

		});

		$("#header_pegawai_atasan").select2({
			ajax: {
				url: base_url+"kepegawaian/dp3/getDataListPegawai",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						q: params.term, 
						page: params.page,
						
					};
				},
				processResults: function (data, params) {
					params.page = params.page || 1;

					return {
						results: data.items,
						pagination: {
							more: (params.page * 5) < data.total_count
						}
					};
				},
				cache: true
			},
			containerCssClass: 'select-xs',
			placeholder: 'Pencarian Pegawai',
			escapeMarkup: function (markup) { return markup; }, 
			minimumInputLength: 3,
			templateResult: formatRepoPegawai,
			templateSelection: formatRepoPegawaiSelection,

		});

		function formatRepoPegawai (repo) {
			if (repo.loading) {
				return repo.text;
			}

			var markup = "<div class='select2-result-repository clearfix'>" +
			"<div class='select2-result-repository__avatar'><img src='http://simpeg.kkp.go.id/photo/"+repo.pegawai_image_path + "' /></div>" +
			"<div class='select2-result-repository__meta'>" +
			"<div class='select2-result-repository__title'>" + repo.pegawai_nama_lengkap + "</div>"+
			"<div class='select2-result-repository__description'>NIP : " + repo.pegawai_nip + "<br>Jabatan : " + repo.pegawai_nama_jabatan + "<br>Unit Kerja : " + repo.unit_kerja_nama + "</div>"+
			"</div>";
			
			return markup;
		}
		function formatRepoPegawaiSelection (repo) {
			return repo.pegawai_nama_lengkap || repo.text;
		}
	});

	</script>
