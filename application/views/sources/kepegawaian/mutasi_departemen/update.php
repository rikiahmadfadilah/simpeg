<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Mutasi Ke Instansi Luar KKP</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nip"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tempat Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_tempat_lahir"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Jabatan</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama_jabatan"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Unit Kerja</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["unit_kerja_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<a href="<?php echo base_url();?>kepegawaian/mutasi_departemen" class="btn btn-xs btn-warning">Kembali</a>
							</div>
						</div>
					</div>
				</div>
					<div class="col-md-12 form_tambah_data">
						<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Mutasi Ke Instansi Luar KKP</span>
						<div class="col-md-12">
							<div class="col-md-12">
							<?php if(count($history)>0){?>
								<?php foreach ($history as $h) {  ?>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tanggal Mutasi</label>
									<div class="col-md-9">
											<input type="text" name="mutasi_departemen_tanggal" id="mutasi_departemen_tanggal" class="form-control input-xs wajib pickmutasidepartemen" placeholder="" value="<?php echo $h["mutasi_departemen_tanggal"];?>">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Ke Departemen</label>
									<div class="col-md-9">
											<input type="text" name="mutasi_departemen_ke" id="mutasi_departemen_ke" class="form-control input-xs wajib" placeholder="" value="<?php echo $h["mutasi_departemen_ke"];?>">
									</div>
								</div>
								<?php } ?>
							<?php } else{ ?>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tanggal Mutasi</label>
									<div class="col-md-9">
											<input type="text" name="mutasi_departemen_tanggal" id="mutasi_departemen_tanggal" class="form-control input-xs wajib pickmutasidepartemen" placeholder="" >
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Ke Departemen</label>
									<div class="col-md-9">
											<input type="text" name="mutasi_departemen_ke" id="mutasi_departemen_ke" class="form-control input-xs wajib" placeholder="" >
									</div>
								</div>
							<?php } ?>
								<div class="form-group form-group-xs">
									<div class="col-md-9 col-md-offset-3">
										<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
										<!-- <a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a> -->
										<input type="hidden" name="pegawai_id" id="pegawai_id" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_id"];?>">
										<input type="hidden" name="pegawai_nip" id="pegawai_nip" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_nip"];?>">
										<input type="hidden" name="isUpdate" id="isUpdate" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="mutasi_departemen_pegawai_id" id="mutasi_departemen_pegawai_id" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_id"];?>">
										<input type="hidden" name="mutasi_departemen_pegawai_nip" id="mutasi_departemen_pegawai_nip" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_nip"];?>">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>
	<script type="text/javascript">
		function update_data(data){
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			$('.form_lihat_data').hide();
			$('.label_aksi_form').html('Form Perubahan Data Kemampuan Bahasa Asing');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					update_data(data);
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
					$('#isUpdate').val(1);
					var mutasi_departemen_id = datajson.mutasi_departemen_id;
					var mutasi_departemen_tanggal = datajson.mutasi_departemen_tanggal;
					var mutasi_departemen_ke = datajson.mutasi_departemen_ke;
					
					$('#mutasi_departemen_id').val(mutasi_departemen_id);
					$('#mutasi_departemen_tanggal').val(mutasi_departemen_tanggal);
					$('#mutasi_departemen_ke').val(mutasi_departemen_ke);
					$('#mutasi_departemen_id').val(datajson.mutasi_departemen_id);
				});
			}
		}
		function tambah_data(){
			$('.form_lihat_data').hide();
			reset_form();
			$('.label_aksi_form').html('Form Penambahan Data Kenaikan Gaji Berkala');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					$('.form_tambah_data').slideDown('fast',function(){
						$('html, body').animate({
							scrollTop: $(".form_tambah_data").offset().top-60
						}, 500);
						
					});
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
				});
			}
		}
		
		$(document).ready(function() {
			$('.pickmutasidepartemen').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
			});
		});
	</script>
