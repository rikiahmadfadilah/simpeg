<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Data Pendidikan Formal</h6>
		
	</div>

	<div class="panel-body">
		<table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive epagawai_table" id="list_data">
			<thead>
				<tr>
					<th class="text-center"style="width: 175px;min-width: 175px;">NIP</th>
					<th class="text-center">Nama</th>
					<th class="text-center">Nama Jabatan</th>
					<th class="text-center">Unit Kerja</th>
					<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
				</tr>
			</thead>
			
		</table>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#list_data').dataTable( {
			"processing": true,
			"serverSide": true,
			"bServerSide": true,
			"sAjaxSource": base_url+"kepegawaian/pendidikan_formal/list_data_aktif",
			 "aaSorting": [],
			 "order": [],
			 "iDisplayLength": 10,
			"aoColumns": [
			{ "sClass": "text-left" },
			{ "sClass": "text-left" },
			{ "sClass": "text-left" },
			{ "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-center" },
			],
			"fnDrawCallback": function () {
				set_default_datatable();
			},
		});
	});
	function set_aktif(id){
		$('#text_konfirmasi').html('Anda yakin mengaktifkan data ini ?');
		$('#konfirmasipenyimpanan').modal('show');
		$('#setujukonfirmasibutton').unbind();
		$('#setujukonfirmasibutton').on('click', function () {
			$('#konfirmasipenyimpanan').modal('hide');
			$('.angkadoank').inputmask('remove');
			$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
			window.location.href = base_url+'kepegawaian/pendidikan_formal/aktif/'+id;
		});
	}
	function set_non_aktif(id){
		$('#text_konfirmasi').html('Anda yakin nonaktifkan data ini ?');
		$('#konfirmasipenyimpanan').modal('show');
		$('#setujukonfirmasibutton').unbind();
		$('#setujukonfirmasibutton').on('click', function () {
			$('#konfirmasipenyimpanan').modal('hide');
			$('.angkadoank').inputmask('remove');
			$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
			window.location.href = base_url+'kepegawaian/pendidikan_formal/non_aktif/'+id;
		});
	}
</script>
