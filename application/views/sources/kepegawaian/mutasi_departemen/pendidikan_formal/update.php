<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Pendidikan Formal</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nip"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tempat Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_tempat_lahir"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Jabatan</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama_jabatan"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Unit Kerja</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["unit_kerja_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<a href="<?php echo base_url();?>kepegawaian/pendidikan_formal" class="btn btn-xs btn-warning">Kembali</a></td>

							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Daftar Data Pendidikan</span>
					<div class="col-md-12">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
							<thead>
								<tr>
									<th class="text-center">Nama Sekolah</th>
									<th class="text-center">Program Studi</th>
									<th class="text-center">Tahun<br>Masuk - Lulus</th>
									<th class="text-center">Lokasi</th>
									<th class="text-center" style="width: 80px;min-width: 90px;">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($history)>0){?>
									<?php foreach ($history as $h) {  ?>
										<tr>
											<td><?php echo $h["pendidikan_formal_nama"];?></td>
											<td><?php echo $h["pendidikan_formal_pro_studi_text"];?></td>
											<td class="text-center"><?php echo $h["pendidikan_formal_thn_masuk"];?> - <?php echo $h["pendidikan_formal_thn_lulus"];?></td>
											<td><?php echo $h["pendidikan_formal_lokasi"];?></td>
											<td>
												<ul class="icons-list">
													<li><a href="javascript:void(0)" onclick="update_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
													<li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>
													<li><a onclick="set_non_aktif(<?php echo $h["pendidikan_formal_id"];?>,<?php echo $h["pendidikan_formal_pegawai_id"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
												</ul>
											</td>
										</tr>
									<?php } ?>
								<?php }else{ ?>
									<tr>
										<td class="text-center" colspan="7">Data Belum Tersedia</td>
									</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<td class="text-right" colspan="7"><a href="javascript:void(0)" onclick="tambah_data();" class="btn btn-xs btn-primary btn-tfoot-table">Tambah Data</a>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="col-md-12 form_tambah_data" style="display: none;">
						<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan Data Pendidikan</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Pendidikan</label>
									<div class="col-md-9">
										<select name="pendidikan_formal_pendidikan_id" id="pendidikan_formal_pendidikan_id" data-placeholder="Pendidikan" class="select-size-xs wajib"  onchange="get_tingkat_pendidikan(this,'pendidikan_formal_jurusan_id_tingkat')">
											<option value=""></option>
											<?php foreach ($pendidikan as $i) {
												echo '<option value="'.$i["pendidikan_id"].'" data-kode="'.$i["pendidikan_kode"].'">'.$i["pendidikan_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tingkat Pendidikan</label>
									<div class="col-md-9">
										<select name="pendidikan_formal_jurusan_id_tingkat" id="pendidikan_formal_jurusan_id_tingkat" data-placeholder="Tingkat" class="select-size-xs wajib">
											<option value=""></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs" id="sd" style="display: none;">
									<label class="col-md-3 control-label">Nama Sekolah</label>
									<div class="col-md-9">
										<!-- <select name="pendidikan_formal_sd" id="pendidikan_formal_sd" data-oldvalue="<?php echo $pegawai["pegawai_id"];?>" data-placeholder="Sekolah SD" class="select-size-xs gakwajib" onchange="setNama(this); get_prodi(this,'pendidikan_formal_pro_studi');">
											<option value=""></option>
											<?php foreach ($sd as $i) {
												echo '<option value="'.$i["sekolah_sd_id"].'" data-kode="'.$i["sekolah_sd_npsn"].'" data-nama="'.$i["sekolah_sd_nama"].'">'.$i["sekolah_sd_nama"].'</option>';
											}?>
										</select> -->
										<select name="header_sd" id="header_sd" data-default-id="<?php echo $sd["sekolah_sd_id"];?>" data-default-npsn="<?php echo $sd["sekolah_sd_npsn"];?>"  data-placeholder="Pilih Sekolah SD" class="select-size-xs gakwajib">
											<option value=""></option>
											<option value="<?php echo $sd["sekolah_sd_id"];?>" selected="selected"><?php echo $sd["sekolah_sd_nama"];?></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs" id="smp" style="display: none;">
									<label class="col-md-3 control-label">Nama Sekolah</label>
									<div class="col-md-9">
										<select name="header_smp" id="header_smp" data-default-id="<?php echo $smp["sekolah_smp_id"];?>" data-default-npsn="<?php echo $smp["sekolah_smp_npsn"];?>"  data-placeholder="Pilih Sekolah SMP" class="select-size-xs gakwajib">
											<option value=""></option>
											<option value="<?php echo $smp["sekolah_smp_id"];?>" selected="selected"><?php echo $smp["sekolah_smp_nama"];?></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs" id="sma" style="display: none;">
									<label class="col-md-3 control-label">Nama Sekolah</label>
									<div class="col-md-9">
										<select name="header_sma" id="header_sma" data-default-id="<?php echo $sma["sekolah_sma_id"];?>" data-default-npsn="<?php echo $sma["sekolah_sma_npsn"];?>"  data-placeholder="Pilih Sekolah SMA" class="select-size-xs gakwajib">
											<option value=""></option>
											<option value="<?php echo $sma["sekolah_sma_id"];?>" selected="selected"><?php echo $sma["sekolah_sma_nama"];?></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs" id="univ" style="display: none;">
									<label class="col-md-3 control-label">Nama Universitas</label>
									<div class="col-md-9">
										<select name="header_univ" id="header_univ" data-default-id="<?php echo $univ["univ_id"];?>" data-default-npsn="<?php echo $univ["univ_npsn"];?>"  data-placeholder="Pilih Universitas" class="select-size-xs gakwajib">
											<option value=""></option>
											<option value="<?php echo $univ["univ_id"];?>" selected="selected"><?php echo $univ["univ_nama"];?></option>
										</select>
									</div>
								</div>
								<!-- <div class="form-group form-group-xs" id="univ" style="display: none;">
									<label class="col-md-3 control-label">Nama Universitas</label>
									<div class="col-md-9">
										<select name="pendidikan_formal_univ" id="pendidikan_formal_univ" data-oldvalue="<?php echo $pegawai["pegawai_id"];?>" data-placeholder="Universitas" class="select-size-xs gakwajib" onchange="setNama(this); get_prodi(this,'pendidikan_formal_pro_studi');">
											<option value=""></option>
											<?php foreach ($univ as $i) {
												echo '<option value="'.$i["univ_id"].'" data-kode="'.$i["univ_npsn"].'" data-nama="'.$i["univ_nama"].'">'.$i["univ_nama"].'</option>';
											}?>
										</select>
									</div>
								</div> -->
								<div class="form-group form-group-xs" style="display: none;">
									<label class="col-md-3 control-label"></label>
									<div class="col-md-9">
										<input type="text" name="pendidikan_sekolah_universitas_id" id="pendidikan_sekolah_universitas_id" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label"></label>
									<div class="col-md-9">
										<input type="hidden" name="pendidikan_formal_nama" id="pendidikan_formal_nama" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs" id="prodi" style="display: none;">
									<label class="col-md-3 control-label">Program Studi</label>
									<div class="col-md-9">
										<select name="pendidikan_formal_pro_studi" id="pendidikan_formal_pro_studi" data-placeholder="Program Studi" class="select-size-xs gakwajib">
											<option value=""></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tahun Masuk</label>
									<div class="col-md-9">
										<select name="pendidikan_formal_thn_masuk" id="pendidikan_formal_thn_masuk" data-placeholder="Tahun Masuk" class="select-size-xs wajib" onchange="set_tahun_akhir(this,'pendidikan_formal_thn_lulus')">
											<option value=""></option>
											<?
											for($i=2018;$i>=1950;$i--) {
												echo '<option value="'.$i.'">'.$i.'</option>';
											}
											?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tahun Lulus</label>
									<div class="col-md-9">
										<select name="pendidikan_formal_thn_lulus" id="pendidikan_formal_thn_lulus" data-placeholder="Tahun Lulus" class="select-size-xs wajib">
											<option value=""></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tempat Belajar</label>
									<div class="col-md-9">
										<select name="pendidikan_formal_tmp_belajar" id="pendidikan_formal_tmp_belajar" data-placeholder="Tempat Belajar" class="select-size-xs wajib">
											<option value=""></option>
											<option value="1">Dalam Negeri</option>
											<option value="2">Luar Negeri</option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Lokasi</label>
									<div class="col-md-9">
										<input type="text" name="pendidikan_formal_lokasi" id="pendidikan_formal_lokasi" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Nomor Ijazah</label>
									<div class="col-md-9">
										<input type="text" name="pendidikan_formal_no_ijazah" id="pendidikan_formal_no_ijazah" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Nama Kepala Sekolah/Rektor</label>
									<div class="col-md-9">
										<input type="text" name="pendidikan_formal_nama_kep" id="pendidikan_formal_nama_kep" class="form-control input-xs wajib">
									</div>
								</div>

								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">IPK</label>
									<div class="col-md-9">
										<input type="text" disabled="disabled" name="pegawai_pendidikan_ipk" id="pegawai_pendidikan_ipk" class="form-control input-xs wajib">
									</div>
										<input type="text" disabled="disabled" name="prodinya" id="prodinya" class="form-control input-xs gakwajib" style="display: none;">
								</div>


								<div class="form-group form-group-xs">
									<div class="col-md-9 col-md-offset-3">
										<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
										<input type="hidden" name="pendidikan_formal_pegawai_id" id="pendidikan_formal_pegawai_id" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_id"];?>">
										<input type="hidden" name="pendidikan_formal_pegawai_nip" id="pendidikan_formal_pegawai_nip" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_nip"];?>">
										<input type="hidden" name="isUpdate" id="isUpdate" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="pendidikan_formal_id" id="pendidikan_formal_id" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="pendidikan_formal_jurusan_code_tingkat_temp" id="pendidikan_formal_jurusan_code_tingkat_temp" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="pendidikan_formal_univ_temp" id="pendidikan_formal_univ_temp" class="form-control input-xs wajib" value="0">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 form_lihat_data" style="display: none;">
						<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Lihat Data Pendidikan</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Nama Sekolah/Universitas</label>
									<div class="col-md-9">
										<div class="form-control-static pendidikan_formal_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Pendidikan</label>
									<div class="col-md-9">
										<div class="form-control-static pendidikan_formal_pendidikan">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tingkat Pendidikan</label>
									<div class="col-md-9">
										<div class="form-control-static pendidikan_formal_tingkat">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Program Studi</label>
									<div class="col-md-9">
										<div class="form-control-static pendidikan_formal_pro_studi_text">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tahun Masuk</label>
									<div class="col-md-9">
										<div class="form-control-static pendidikan_formal_thn_masuk">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tahun Lulus</label>
									<div class="col-md-9">
										<div class="form-control-static pendidikan_formal_thn_lulus">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tempat Belajar</label>
									<div class="col-md-9">
										<div class="form-control-static pendidikan_formal_tmp_belajar_text">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Lokasi</label>
									<div class="col-md-9">
										<div class="form-control-static pendidikan_formal_lokasi">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Nomor Ijazah</label>
									<div class="col-md-9">
										<div class="form-control-static pendidikan_formal_no_ijazah">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Nama Kepala Sekolah/Rektor</label>
									<div class="col-md-9">
										<div class="form-control-static pendidikan_formal_nama_kep">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">IPK</label>
									<div class="col-md-9">
										<div class="form-control-static pegawai_pendidikan_ipk">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<div class="col-md-9 col-md-offset-3">
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>
	<script type="text/javascript">
		function setNama(ini) {
			var TenderRUPId = $('option:selected', ini).attr('data-TenderRUPId');
			var nama = $('option:selected', ini).attr('data-nama');
			var id = $('option:selected', ini).val();

			$('#pendidikan_formal_nama').val("").val(nama);
			$('#pendidikan_sekolah_universitas_id').val("").val(id);
		}
		
		$(function(){
		$('#pendidikan_formal_pendidikan_id').on('change', function() {
			if($(this).val() == "11")
			{
				$("#sd").show();
				$("#smp").hide();
				$("#sma").hide();
				$("#prodi").hide();
				$("#univ").hide();
			}
			else if($(this).val() == "10")
			{
				$("#smp").show();
				$("#sd").hide();
				$("#sma").hide();
				$("#prodi").hide();
				$("#univ").hide();
			}
			else if($(this).val() == "9")
			{
				$("#sma").show();
				$("#sd").hide();
				$("#smp").hide();
				$("#prodi").hide();
				$("#univ").hide();
			}
			else if($(this).val() <= "9")
			{
				$("#prodi").show();
				$("#univ").show();
				$("#sd").hide();
				$("#smp").hide();
				$("#sma").hide();
			}
		});
	});
		function set_non_aktif(id,pegawai_id){
			$('#text_konfirmasi').html('Anda yakin menghapus data ini ?');
			$('#konfirmasipenyimpanan').modal('show');
			$('#setujukonfirmasibutton').unbind();
			$('#setujukonfirmasibutton').on('click', function () {
				$('#konfirmasipenyimpanan').modal('hide');
				$('.angkadoank').inputmask('remove');
				$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
				window.location.href = base_url+'kepegawaian/pendidikan_formal/hapus/'+id+'/'+pegawai_id;
			});
		}
		function lihat_data(data){
			$('.form_tambah_data').hide();
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					lihat_data(data);
				});
			}else{
				$('.form_lihat_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_lihat_data").offset().top-60
					}, 500);
					var pendidikan_formal_id = datajson.pendidikan_formal_id;
					var pendidikan_formal_nama = datajson.pendidikan_formal_nama;
					var pendidikan_formal_pendidikan = datajson.pendidikan_formal_pendidikan;
					var pendidikan_formal_tingkat = datajson.pendidikan_formal_tingkat;
					var pendidikan_formal_jurusan_id_fakultas = datajson.pendidikan_formal_jurusan_id_fakultas;
					var pendidikan_formal_jurusan_id = datajson.pendidikan_formal_jurusan_id;
					var pendidikan_formal_pro_studi_text = datajson.pendidikan_formal_pro_studi_text;
					var pendidikan_formal_thn_masuk = datajson.pendidikan_formal_thn_masuk;
					var pendidikan_formal_thn_lulus = datajson.pendidikan_formal_thn_lulus;
					var pendidikan_formal_tmp_belajar_text = datajson.pendidikan_formal_tmp_belajar_text;
					var pendidikan_formal_lokasi = datajson.pendidikan_formal_lokasi;
					var pendidikan_formal_no_ijazah = datajson.pendidikan_formal_no_ijazah;
					var pendidikan_formal_nama_kep = datajson.pendidikan_formal_nama_kep;
					var pegawai_pendidikan_ipk = datajson.pendidikan_formal_ipk;

					$('.pendidikan_formal_nama').html(pendidikan_formal_nama);
					$('.pendidikan_formal_pendidikan').html(pendidikan_formal_pendidikan);
					$('.pendidikan_formal_tingkat').html(pendidikan_formal_tingkat);
					$('.pendidikan_formal_thn_masuk').html(pendidikan_formal_thn_masuk);
					$('.pendidikan_formal_tmp_belajar_text').html(pendidikan_formal_tmp_belajar_text);
					$('.pendidikan_formal_pro_studi_text').html(pendidikan_formal_pro_studi_text);
					$('.pendidikan_formal_lokasi').html(pendidikan_formal_lokasi);
					$('.pendidikan_formal_no_ijazah').html(pendidikan_formal_no_ijazah);
					$('.pendidikan_formal_nama_kep').html(pendidikan_formal_nama_kep);
					$('.pendidikan_formal_thn_lulus').html(pendidikan_formal_thn_lulus);
					$('.pegawai_pendidikan_ipk').html(pegawai_pendidikan_ipk);
				});
			}
		}
		function update_data(data){
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			$('.form_lihat_data').hide();
			$('.label_aksi_form').html('Form Perubahan Data Pendidikan');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					update_data(data);
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
					$('#isUpdate').val(1);
					var pendidikan_formal_id = datajson.pendidikan_formal_id;
					var pendidikan_formal_nama = datajson.pendidikan_formal_nama;
					var pendidikan_sekolah_universitas_id = datajson.pendidikan_sekolah_universitas_id;
					var pendidikan_formal_pendidikan_id = datajson.pendidikan_formal_pendidikan_id;
					var pendidikan_formal_jurusan_id_tingkat = datajson.pendidikan_formal_tingkat;
					var pendidikan_formal_sd = datajson.pendidikan_formal_nama;
					var tingkat_jurusan_kode = datajson.tingkat_jurusan_kode;
					var pendidikan_formal_pro_studi = datajson.prodinya;
					var pendidikan_formal_thn_masuk = datajson.pendidikan_formal_thn_masuk;
					var pendidikan_formal_thn_lulus = datajson.pendidikan_formal_thn_lulus;
					var pendidikan_formal_tmp_belajar = datajson.pendidikan_formal_tmp_belajar;
					var pendidikan_formal_lokasi = datajson.pendidikan_formal_lokasi;
					var pendidikan_formal_no_ijazah = datajson.pendidikan_formal_no_ijazah;
					var pendidikan_formal_nama_kep = datajson.pendidikan_formal_nama_kep;
					var pegawai_pendidikan_ipk = datajson.pendidikan_formal_ipk;
					var prodinya = datajson.prodinya;

					$('#pendidikan_formal_id').val(pendidikan_formal_id);
					$('#pendidikan_formal_nama').val(pendidikan_formal_nama);
					$('#pendidikan_sekolah_universitas_id').val(pendidikan_sekolah_universitas_id);
					$('#pendidikan_formal_jurusan_id_tingkat').val(pendidikan_formal_jurusan_id_tingkat);
					$('#pendidikan_formal_jurusan_code_tingkat_temp').val(tingkat_jurusan_kode);
					$('#pendidikan_formal_univ_temp').val(prodinya);
					$('#pendidikan_formal_id').val(datajson.pendidikan_formal_id);
					
					if (datajson.pendidikan_formal_pendidikan_id <= "8") {
						$("#header_univ").append('<option value="' + pendidikan_sekolah_universitas_id + '">' + pendidikan_formal_nama + '</option>');
						$("#header_univ").val(pendidikan_sekolah_universitas_id).trigger('change');
					} else if(datajson.pendidikan_formal_pendidikan_id == "9"){
						$("#header_sma").append('<option value="' + pendidikan_sekolah_universitas_id + '">' + pendidikan_formal_nama + '</option>');
						$("#header_sma").val(pendidikan_sekolah_universitas_id).trigger('change');
					}else if(datajson.pendidikan_formal_pendidikan_id == "10"){
						$("#header_smp").append('<option value="' + pendidikan_sekolah_universitas_id + '">' + pendidikan_formal_nama + '</option>');
						$("#header_smp").val(pendidikan_sekolah_universitas_id).trigger('change');
					}else if(datajson.pendidikan_formal_pendidikan_id == "11"){
						$("#header_sd").append('<option value="' + pendidikan_sekolah_universitas_id + '">' + pendidikan_formal_nama + '</option>');
						$("#header_sd").val(pendidikan_sekolah_universitas_id).trigger('change');
					}

					$('#pendidikan_formal_pendidikan_id').select2('val', '');
					$('#pendidikan_formal_pendidikan_id').val(pendidikan_formal_pendidikan_id);
					$('#pendidikan_formal_pendidikan_id').trigger('change');

					$('#pendidikan_formal_thn_masuk').select2('val', '');
					$('#pendidikan_formal_thn_masuk').val(pendidikan_formal_thn_masuk);
					$('#pendidikan_formal_thn_masuk').trigger('change');

					$('#pendidikan_formal_tmp_belajar').select2('val',pendidikan_formal_tmp_belajar);

					$('#pendidikan_formal_pro_studi').val(pendidikan_formal_pro_studi);
					$('#pendidikan_formal_lokasi').val(pendidikan_formal_lokasi);
					$('#pendidikan_formal_no_ijazah').val(pendidikan_formal_no_ijazah);
					$('#pendidikan_formal_nama_kep').val(pendidikan_formal_nama_kep);
					$('#pendidikan_formal_thn_lulus').val(pendidikan_formal_thn_lulus);
					$('#pendidikan_formal_thn_lulus').trigger('change');
					$('#pegawai_pendidikan_ipk').val(pegawai_pendidikan_ipk);
					$('#prodinya').val(prodinya);
				});
			}
		}
		function tambah_data(){
			$('.form_lihat_data').hide();
			reset_form();
			$('.label_aksi_form').html('Form Penambahan Data Pendidikan');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					$('.form_tambah_data').slideDown('fast',function(){
						$('html, body').animate({
							scrollTop: $(".form_tambah_data").offset().top-60
						}, 500);
						
					});
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
				});
			}
		}
		function batal_data(){
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					reset_form();
				});
			}
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					reset_form();
				});
			}
		}
		function reset_form(){
			$('#pendidikan_formal_id').val(0);
			$('#pendidikan_formal_nama').val('');
			$('#pendidikan_sekolah_universitas_id').val('');
			$('#pendidikan_formal_jurusan_code_tingkat_temp').val('');
			$('#pendidikan_formal_univ_temp').val('');
			$('#pendidikan_formal_univ').select2('val', ' ');
			$('#pendidikan_formal_pro_studi').select2('val', ' ');
			$('#pendidikan_formal_lokasi').val('');
			$('#pendidikan_formal_no_ijazah').val('');
			$('#pendidikan_formal_nama_kep').val('');
			$('#pendidikan_formal_tmp_belajar').select2('val',' ');
			$('#pendidikan_formal_pendidikan_id').select2('val', ' ');
			$('#pendidikan_formal_thn_masuk').select2('val',' ');
			$('#pegawai_pendidikan_ipk').val('');
			$('#prodinya').val('');
			$('.validation-error-label').css('display','none');
		}
		function set_tahun_akhir(ini,nextopsi){
			var tahunmulai = parseInt($(ini).val());
			var d = new Date();
			var tahunakhir = d.getFullYear();
			var newoption = "<option value=''></option>";
			for(var i=tahunakhir; i >=tahunmulai; i--) {
				newoption += '<option value="'+i+'">'+i+'</option>';
			}
			$('#'+nextopsi).html(newoption);
		}
		function  get_tingkat_pendidikan(ini,nextopsi) {
			var pendidikan_kode = $('option:selected', ini).attr('data-kode');
			$.ajax({
				url: base_url+'kepegawaian/pendidikan_formal/get_tingkat_pendidikan',
				type: "post",
				dataType: 'json',
				data: { pendidikan_kode: pendidikan_kode},
				success: function (data) {
					var isUpdate = parseInt($('#isUpdate').val());
					var pendidikan_formal_pendidikan_id = parseInt($('#pendidikan_formal_pendidikan_id').val());
					
					if(pendidikan_formal_pendidikan_id < 9){
						$( "#pegawai_pendidikan_ipk" ).prop( "disabled", false );
					}
					else $( "#pegawai_pendidikan_ipk" ).prop( "disabled", true );
					
					var pendidikan_formal_jurusan_code_tingkat_temp = $('#pendidikan_formal_jurusan_code_tingkat_temp').val();
					var tingkat_pendidikan = data.tingkat_pendidikan;
					var newoption = "<option value=''></option>";
					for(var i = 0; i < tingkat_pendidikan.length; i ++){
						newoption+='<option value="'+tingkat_pendidikan[i].jurusan_kode+'" '+((tingkat_pendidikan.length==1 || (isUpdate==1 && pendidikan_formal_jurusan_code_tingkat_temp == tingkat_pendidikan[i].jurusan_kode))?"selected":"")+'>'+tingkat_pendidikan[i].jurusan_grup+'</option>';
					}
					$('#'+nextopsi).html(newoption);

					// get_fakultas($('#pendidikan_formal_jurusan_id_tingkat'),'pendidikan_formal_jurusan_id_fakultas');
				}
			});
		}
		function  get_prodi(ini,nextopsi) {
			var univ_id = $(ini).val();
			var oldvalue = $(ini).attr('data-oldvalue');
			var prodinya = ($('#prodinya').val());
			// alert(prodinya);
			$.ajax({
				url: base_url+'kepegawaian/pendidikan_formal/get_prodi',
				type: "post",
				dataType: 'json',
				data: { univ_id: univ_id},
				success: function (data) {
					var isUpdate = parseInt($('#isUpdate').val());
					var pendidikan_formal_univ_temp = $('#pendidikan_formal_univ_temp').val();

					var prodi = data.prodi;
					var newoption = "<option=''></option>";
					for(var i = 0; i < prodi.length; i ++){
						newoption+='<option value="'+prodi[i].prodi_id+'" '+(((isUpdate==1 && pendidikan_formal_univ_temp == prodi[i].prodi_id))?"selected":"")+'>'+prodi[i].prodi_nama+'</option>';
					}
					$('#'+nextopsi).html(newoption);
					// $('#'+nextopsi).change();
				}
			});
		}
		$('#header_sd').on('change', function() {
			var sekolah_sd_id = $(this).val();
			$.ajax({
				url: base_url+'kepegawaian/pendidikan_formal/getDataSD',
				type: "post",
				dataType: 'json',
				data: { sekolah_sd_id: sekolah_sd_id},
				success: function (data) {
					if(data.sekolah_sd!=null){
						$('#pendidikan_sekolah_universitas_id').val(data.sekolah_sd.sekolah_sd_id);
						$('#pendidikan_formal_nama').val(data.sekolah_sd.sekolah_sd_nama);
					}else{
						$('#pendidikan_formal_nama').val('');
						$('#pendidikan_sekolah_universitas_id').val('');
					}
				}
			});
		});
		$('#header_smp').on('change', function() {
			var sekolah_smp_id = $(this).val();
			$.ajax({
				url: base_url+'kepegawaian/pendidikan_formal/getDataSMP',
				type: "post",
				dataType: 'json',
				data: { sekolah_smp_id: sekolah_smp_id},
				success: function (data) {
					if(data.sekolah_smp!=null){
						$('#pendidikan_sekolah_universitas_id').val(data.sekolah_smp.sekolah_smp_id);
						$('#pendidikan_formal_nama').val(data.sekolah_smp.sekolah_smp_nama);
					}else{
						$('#pendidikan_sekolah_universitas_id').val('');
						$('#pendidikan_formal_nama').val('');
					}
				}
			});
		});
		$('#header_sma').on('change', function() {
			var sekolah_sma_id = $(this).val();
			$.ajax({
				url: base_url+'kepegawaian/pendidikan_formal/getDataSMA',
				type: "post",
				dataType: 'json',
				data: { sekolah_sma_id: sekolah_sma_id},
				success: function (data) {
					if(data.sekolah_sma!=null){
						$('#pendidikan_sekolah_universitas_id').val(data.sekolah_sma.sekolah_sma_id);
						$('#pendidikan_formal_nama').val(data.sekolah_sma.sekolah_sma_nama);
					}else{
						$('#pendidikan_sekolah_universitas_id').val('');
						$('#pendidikan_formal_nama').val('');
					}
				}
			});
		});
		$('#header_univ').on('change', function() {
			var univ_id = $(this).val();
			// alert(univ_id);
			$.ajax({
				url: base_url+'kepegawaian/pendidikan_formal/get_prodi',
				type: "post",
				dataType: 'json',
				data: { univ_id: univ_id},
				success: function (data) {
					var isUpdate = parseInt($('#isUpdate').val());
					var pendidikan_formal_univ_temp = $('#pendidikan_formal_univ_temp').val();

					var prodi = data.prodi;
					var newoption = "<option=''></option>";
					for(var i = 0; i < prodi.length; i ++){
						newoption+='<option value="'+prodi[i].prodi_id+'" '+(((isUpdate==1 && pendidikan_formal_univ_temp == prodi[i].prodi_id))?"selected":"")+'>'+prodi[i].prodi_nama+'</option>';
					}
					$('#pendidikan_formal_pro_studi').html(newoption);
					$('#pendidikan_formal_pro_studi').change();
					// $('#'+nextopsi).html(newoption);
					// $('#'+nextopsi).change();
				}
			});

			$.ajax({
				url: base_url+'kepegawaian/pendidikan_formal/getDataUNIV',
				type: "post",
				dataType: 'json',
				data: { univ_id: univ_id},
				success: function (data) {
					if(data.univ!=null){
						$('#pendidikan_sekolah_universitas_id').val(data.univ.univ_id);
						$('#pendidikan_formal_nama').val(data.univ.univ_nama);
					}else{
						$('#pendidikan_sekolah_universitas_id').val('');
						$('#pendidikan_formal_nama').val('a');
					}
				}
			});
		});
		
		$(document).ready(function() {
			$( "#pendidikan_formal_pendidikan_id").rules( "add", {
				required: true,
				remote: {
					url: base_url+'kepegawaian/pendidikan_formal/check_tingat_pendidikan',
					type: "post",
					data: {
						pendidikan_formal_pendidikan_id: function() {
							return $( "#pendidikan_formal_pendidikan_id" ).val();
						},
						pendidikan_formal_pegawai_id: function() {
							return $( "#pendidikan_formal_pegawai_id" ).val();
						},
						isUpdate: function() {
							return $( "#isUpdate" ).val();
						}					
					}
				},
				messages: {
					required: "Wajib Diisi",
					remote: "Pendidikan Telah Digunakan"
				}
			});
		});
      	$("#header_sd").select2({
			ajax: {
				url: base_url+"kepegawaian/pendidikan_formal/getDataListSD",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						q: params.term, 
						page: params.page,
						
					};
				},
				processResults: function (data, params) {
					params.page = params.page || 1;

					return {
						results: data.items,
						pagination: {
							more: (params.page * 5) < data.total_count
						}
					};
				},
				cache: true
			},
			containerCssClass: 'select-xs',
			placeholder: 'Pencarian Sekolah SD',
			escapeMarkup: function (markup) { return markup; }, 
			minimumInputLength: 3,
			templateResult: formatRepoSD,
			templateSelection: formatRepoSDSelection,

		});

		function formatRepoSD (repo) {
			if (repo.loading) {
				return repo.text;
			}

			var markup = "<div class='select2-result-repository clearfix'>" +
			"<div class='select2-result-repository__title'>" + repo.sekolah_sd_nama + "</div>"+
			"<div class='select2-result-repository__description'>NPSN : " + repo.sekolah_sd_npsn + "<br>Alamat : " + repo.sekolah_sd_alamat + "</div>"+
			"</div>";
			
			return markup;
		}
		function formatRepoSDSelection (repo) {
			return repo.sekolah_sd_nama || repo.text;
		}

		$("#header_smp").select2({
			ajax: {
				url: base_url+"kepegawaian/pendidikan_formal/getDataListSMP",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						q: params.term, 
						page: params.page,
						
					};
				},
				processResults: function (data, params) {
					params.page = params.page || 1;

					return {
						results: data.items,
						pagination: {
							more: (params.page * 5) < data.total_count
						}
					};
				},
				cache: true
			},
			containerCssClass: 'select-xs',
			placeholder: 'Pencarian Sekolah SMP',
			escapeMarkup: function (markup) { return markup; }, 
			minimumInputLength: 3,
			templateResult: formatRepoSMP,
			templateSelection: formatRepoSMPSelection,

		});

		function formatRepoSMP (repo) {
			if (repo.loading) {
				return repo.text;
			}

			var markup = "<div class='select2-result-repository clearfix'>" +
			"<div class='select2-result-repository__title'>" + repo.sekolah_smp_nama + "</div>"+
			"<div class='select2-result-repository__description'>NPSN : " + repo.sekolah_smp_npsn + "<br>Alamat : " + repo.sekolah_smp_alamat + "</div>"+
			"</div>";
			
			return markup;
		}
		function formatRepoSMPSelection (repo) {
			return repo.sekolah_smp_nama || repo.text;
		}

		$("#header_sma").select2({
			ajax: {
				url: base_url+"kepegawaian/pendidikan_formal/getDataListSMA",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						q: params.term, 
						page: params.page,
						
					};
				},
				processResults: function (data, params) {
					params.page = params.page || 1;

					return {
						results: data.items,
						pagination: {
							more: (params.page * 5) < data.total_count
						}
					};
				},
				cache: true
			},
			containerCssClass: 'select-xs',
			placeholder: 'Pencarian Sekolah SMA',
			escapeMarkup: function (markup) { return markup; }, 
			minimumInputLength: 3,
			templateResult: formatRepoSMA,
			templateSelection: formatRepoSMASelection,

		});

		function formatRepoSMA (repo) {
			if (repo.loading) {
				return repo.text;
			}

			var markup = "<div class='select2-result-repository clearfix'>" +
			"<div class='select2-result-repository__title'>" + repo.sekolah_sma_nama + "</div>"+
			"<div class='select2-result-repository__description'>NPSN : " + repo.sekolah_sma_npsn + "<br>Alamat : " + repo.sekolah_sma_alamat + "</div>"+
			"</div>";
			
			return markup;
		}
		function formatRepoSMASelection (repo) {
			return repo.sekolah_sma_nama || repo.text;
		}

		$("#header_univ").select2({
			ajax: {
				url: base_url+"kepegawaian/pendidikan_formal/getDataListUNIV",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						q: params.term, 
						page: params.page,
						
					};
				},
				processResults: function (data, params) {
					params.page = params.page || 1;

					return {
						results: data.items,
						pagination: {
							more: (params.page * 5) < data.total_count
						}
					};
				},
				cache: true
			},
			containerCssClass: 'select-xs',
			placeholder: 'Pencarian Universitas',
			escapeMarkup: function (markup) { return markup; }, 
			minimumInputLength: 3,
			templateResult: formatRepoUNIV,
			templateSelection: formatRepoUNIVSelection,

		});

		function formatRepoUNIV (repo) {
			if (repo.loading) {
				return repo.text;
			}

			var markup = "<div class='select2-result-repository clearfix'>" +
			"<div class='select2-result-repository__title'>" + repo.univ_nama + "</div>"+
			"<div class='select2-result-repository__description'>NPSN : " + repo.univ_npsn + "<br>Alamat : " + repo.univ_alamat + "</div>"+
			"</div>";
			
			return markup;
		}
		function formatRepoUNIVSelection (repo) {
			return repo.univ_nama || repo.text;
		}
	</script>
