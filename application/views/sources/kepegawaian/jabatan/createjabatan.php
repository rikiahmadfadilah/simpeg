<script src="<?php echo base_url();?>assets/js/plugins/forms/wizards/stepy.min.js"></script>
<script src="<?php echo base_url();?>assets/js/demo_pages/wizard_stepy.js"></script>

<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">TAMBAH DATA JABATAN</h6>
	</div>

	<div class="panel-body" >
		<div class="row">
			<form class="" action="" method="post">
				
					<legend class="text-semibold">Jabatan</legend>

					
						<div class="col-md-6">							
							<div class="form-horizontal">
								
								
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">NIP Pegawai</label>
									<div class="col-lg-9">
										<input type="text" name="jenjang_jabatan_pegawai_nip" id="jenjang_jabatan_pegawai_nip" class="form-control input-xs wajib ktp" value="<?php echo $pegawai_nip; ?>">
										<input type="hidden" name="pegawai_id" id="pegawai_id" class="form-control input-xs wajib ktp" value="<?php echo $pegawai_id; ?>">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Eselon</label>
									<div class="col-lg-9">
										<select name="jenjang_jabatan_eselon" id="jenjang_jabatan_eselon" data-placeholder="Pilih Eselon" class="select-size-xs">
											<option></option>
											<?php foreach ($eselon as $i) {
												echo '<option value="'.$i["eselon_nama"].'">'.$i["eselon_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Nama Jabatan</label>
									<div class="col-lg-9">
										<select name="jenjang_jabatan_jabatan" id="jenjang_jabatan_jabatan" data-placeholder="Pilih Jabatan" class="select-size-xs">
											<option></option>
											<?php foreach ($jabatan as $i) {
												echo '<option value="'.$i["jabatan_name"].'">'.$i["jabatan_name"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Unit Kerja</label>
									<div class="col-lg-9">
										<select name="jenjang_jabatan_unit_kerja" id="jenjang_jabatan_unit_kerja" data-placeholder="Pilih Unit Kerja" class="select-size-xs"  onchange="get_tingkat_pendidikan(this,'pegawai_tingkat_pendidikan_id')">
											<option></option>
											<?php foreach ($unit_kerja as $i) {
												echo '<option value="'.$i["unit_kerja_nama"].'">'.$i["unit_kerja_nama_with_tab"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Nomor SK</label>
									<div class="col-lg-9">
										<input type="text" name="jenjang_jabatan_nomor_sk" id="jenjang_jabatan_nomor_sk" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Tanggal SK</label>
									<div class="col-lg-9">
										<input type="text" name="jenjang_jabatan_tanggal_sk" id="jenjang_jabatan_tanggal_sk" class="form-control input-xs wajib pickttl">
									</div>
								</div>	
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Tanggal Masuk/Selesai</label>
									<div class="col-lg-9">
										<div class="row">
											<div class="col-md-6">
												<input type="text" name="jenjang_jabatan_tanggal_mulai" id="jenjang_jabatan_tanggal_mulai" class="form-control input-xs wajib pickttl">
											</div>

											<div class="col-md-6">
												<input type="text" name="jenjang_jabatan_tanggal_selesai" id="jenjang_jabatan_tanggal_selesai" class="form-control input-xs wajib pickttl">
											</div>
										</div>
									</div>
								</div>														
								
							</div>
							
						</div>

						<div class="col-md-6">
							
							

						</div>
					

			
				<div class="col-md-12">
					<div class="form-group form-group-xs" style="text-align: right">
						<a href="<?php echo base_url().'kepegawaian/jabatan/detail/'.$pegawai_id.'';?>" class="btn btn-xs btn-warning" >Kembali <i class="icon-close position-right"></i></a>&nbsp;
						<button type="submit" class="btn btn-xs btn-success" >Simpan <i class="icon-checkmark5 position-right"></i></button>
					</div>
					
				</div>
				
			</form>
		</div>
		
		
	</div>
</div>
<script type="text/javascript">
	function  get_kota(ini,nextopsi) {
		var provinsi_kode = $(ini).val();
		$.ajax({
			url: base_url+'kepegawaian/pns/get_kota',
			type: "post",
			dataType: 'json',
			data: { provinsi_kode: provinsi_kode},
			success: function (data) {
				var kota = data.kota;
				var newoption = "<option></option>";
				for(var i = 0; i < kota.length; i ++){
					newoption+='<option value="'+kota[i].kota_kode+'">'+kota[i].kota_nama+'</option>';
				}
				$('#'+nextopsi).html(newoption);
			}
		});
	}
	function  get_tingkat_pendidikan(ini,nextopsi) {
		var pendidikan_kode = $('option:selected', ini).attr('data-kode');
		$.ajax({
			url: base_url+'kepegawaian/pns/get_tingkat_pendidikan',
			type: "post",
			dataType: 'json',
			data: { pendidikan_kode: pendidikan_kode},
			success: function (data) {
				var tingkat_pendidikan = data.tingkat_pendidikan;
				var newoption = "<option></option>";
				for(var i = 0; i < tingkat_pendidikan.length; i ++){
					newoption+='<option value="'+tingkat_pendidikan[i].jurusan_kode+'" '+((tingkat_pendidikan.length==1)?"selected":"")+'>'+tingkat_pendidikan[i].jurusan_grup+'</option>';
				}
				$('#'+nextopsi).html(newoption);
				get_jurusan($('#pegawai_tingkat_pendidikan_id'),'pegawai_jurusan_id');
			}
		});
	}
	function  get_jurusan(ini,nextopsi) {
		var jurusan_kode = $(ini).val();
		$.ajax({
			url: base_url+'kepegawaian/pns/get_jurusan',
			type: "post",
			dataType: 'json',
			data: { jurusan_kode: jurusan_kode},
			success: function (data) {
				var jurusan = data.jurusan;
				var newoption = "<option></option>";
				
				for(var i = 0; i < jurusan.length; i ++){
					if(jurusan[i].jurusan_grup != jurusan[i].jurusan_nama){
						newoption+='<option value="'+jurusan[i].jurusan_kode+'">'+jurusan[i].jurusan_nama+'</option>';
					}
				}
				$('#'+nextopsi).html(newoption);
			}
		});
	}
	$(document).ready(function() {
		$('[name="pegawai_nip"]').formatter({
			pattern: '{{99999999}}  {{999999}}  {{9}}  {{999}}'
		});
		$('.kodepos').formatter({
			pattern: '{{99999}}'
		});
		$('.ktp').formatter({
			pattern: '{{9999999999999999}}'
		});
		$('.npwp').formatter({
			pattern: '{{99}}  {{999}}  {{999}}  {{9999}} {{999}}'
		});
		//$('#pegawai_ktp_negara_id').select2('readonly',true);
		$("#pegawai_ktp_negara_id").prop("disabled", true);
		var d = new Date();

		$('.pickttl').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
		
		var form = $(".steps-pegawai").show();


	});
	function readURL(input) {

		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
				$('#image_pegawai').attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}

	$("#pegawai_photo").change(function() {
		readURL(this);
	});
</script>
