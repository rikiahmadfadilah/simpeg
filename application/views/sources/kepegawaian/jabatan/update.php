<style type="text/css">
.control-label{
	font-weight: bold;
}
</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Riwayat Jabatan</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nip"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tempat Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_tempat_lahir"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Jabatan</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama_jabatan"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Unit Kerja</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["unit_kerja_hirarki_name_full"];?></div>
							</div>
						</div><br>
						<!-- <?php echo $jabatan_last['jenjang_jabatan_jabatan_nama'].', '.$jabatan_last['jenjang_jabatan_tanggal_mulai'] ?> -->
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<a href="<?php echo base_url();?>kepegawaian/jabatan" class="btn btn-xs btn-warning">Kembali</a></td>

							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Daftar Riwayat Jabatan</span>
					<div class="col-md-12">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
							<thead>
								<tr>
									<th class="text-center">Eselon</th>
									<th class="text-center">Nama Jabatan</th>									
									<th class="text-center" width="19%">Terhitung Mulai Tanggal</th>
									<th class="text-center">Nomor SK</th>
									<th class="text-center" width="10%">Tanggal SK</th>
									<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
								<!-- <tr>
									<th class="text-center">Mulai</th>
									<th class="text-center">Selesai</th>
								</tr> -->
							</thead>
							<tbody>
								<?php if(count($history)>0){?>
									<?php foreach ($history as $h) {  ?>
										<tr>
											<td><?php if($h["jabatan_nama"] == ''){echo '-';}else{echo $h["jabatan_nama"];}?></td>
											<td><?php echo $h["jenjang_jabatan_jabatan_nama"];?></td>
											<td class="text-center"><?php echo dateEnToId($h["jenjang_jabatan_tanggal_mulai"], 'd-m-Y');?></td>
											<!-- <td class="text-center"><?php if($h["jenjang_jabatan_tanggal_selesai"] == '1900-01-01'){echo "-";}else{echo dateEnToId($h["jenjang_jabatan_tanggal_selesai"], 'd-m-Y');}?></td>									 -->
											<td><?php echo $h["jenjang_jabatan_nomor_sk"];?></td>
											<td class="text-center"><?php echo dateEnToId($h["jenjang_jabatan_tanggal_sk"], 'd-m-Y');?></td>
											<td>
												<ul class="icons-list">
													<li><a href="javascript:void(0)" onclick="update_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
													<li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>
													<li><a onclick="set_non_aktif(<?php echo $h["jenjang_jabatan_id"];?>,<?php echo $h["jenjang_jabatan_pegawai_id"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
												</ul>
											</td>
										</tr>
									<?php } ?>
								<?php }else{ ?>
									<tr>
										<td class="text-center" colspan="9">Data Belum Tersedia</td>
									</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<td class="text-right" colspan="9"><a href="javascript:void(0)" onclick="tambah_data();" class="btn btn-xs btn-primary btn-tfoot-table">Tambah Data</a>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="col-md-12 form_tambah_data" style="display: none;">
						<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan Data Jenjang Jabatan</span>
						<div class="col-md-12">

							<div class="col-md-12">
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Unit Kerja</label>
									<div class="col-lg-9">
										<select name="jenjang_jabatan_unit_kerja_id[]" id="jenjang_jabatan_unit_kerja_id_1" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs ga_wajib" onchange="get_unit_kerja_hirarki(this,1)">
											<option></option>
											<?php foreach ($unit_kerja as $i) {
												echo '<option value="'.$i["unit_kerja_id"].'" parent_id_kode="'.$i["unit_kerja_id_kode"].'" unit_kerja_kode="'.$i["unit_kerja_kode"].'" jabatan_id="'.$i["jabatan_id"].'" unit_kerja_nama="'.$i["unit_kerja_nama"].'">'.$i["unit_kerja_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs jenjang_jabatan_unit_kerja_id_2" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="jenjang_jabatan_unit_kerja_id[]" id="jenjang_jabatan_unit_kerja_id_2" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs"  onchange="get_unit_kerja_hirarki(this,2)">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs jenjang_jabatan_unit_kerja_id_3" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="jenjang_jabatan_unit_kerja_id[]" id="jenjang_jabatan_unit_kerja_id_3" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs"  onchange="get_unit_kerja_hirarki(this,3)">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs jenjang_jabatan_unit_kerja_id_4" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="jenjang_jabatan_unit_kerja_id[]" id="jenjang_jabatan_unit_kerja_id_4" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs"  onchange="get_unit_kerja_hirarki(this,4)">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs jenjang_jabatan_unit_kerja_id_5" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="jenjang_jabatan_unit_kerja_id[]" id="jenjang_jabatan_unit_kerja_id_5" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs" onchange="get_unit_kerja_hirarki(this,5)">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Unit Kerja Lama</label>
									<div class="col-md-9">
										<textarea name="jenjang_jabatan_unit_kerja_lama_nama" id="jenjang_jabatan_unit_kerja_lama_nama" class="form-control input-xs wajib" style="height: 60px"></textarea>
										<!-- <input type="text" name="jenjang_jabatan_unit_kerja_lama_nama" id="jenjang_jabatan_unit_kerja_lama_nama" class="form-control input-xs wajib"> -->
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Tipe Jabatan</label>
									<div class="col-lg-9">
										<select name="jenjang_jabatan_tipe" id="jenjang_jabatan_tipe" data-placeholder="Pilih Tipe Jabatan" data-id="0" class="select-size-xs wajib">
											<option></option>
											<option value="1">Struktural</option>
											<option value="2">Fungsional Tertentu</option>
											<option value="3">Fungsional Umum</option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs" id="eselon" style="display: none;">
									<label class="col-lg-3 control-label">Eselon</label>
									<div class="col-lg-9">
										<select name="jenjang_jabatan_jabatan_id" id="jenjang_jabatan_jabatan_id" data-placeholder="Pilih Eselon" data-id="0" class="select-size-xs ga_wajib">
											<option></option>
											<?php foreach ($jabatan as $i) {
												echo '<option value="'.$i["jabatan_id"].'" kode="'.$i["jabatan_kode"].'" jabid="'.$i["jabatan_id"].'">'.$i["jabatan_nama"].'</option>';
											}?>
										</select>
										<input type="hidden" name="jenjang_jabatan_jfu_id" id="jenjang_jabatan_jfu_id" value="11">
										<input type="hidden" name="jenjang_jabatan_jft_id" id="jenjang_jabatan_jft_id" value="10">
									</div>
								</div>
								<!-- <div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Nama Jabatan</label>
									<div class="col-lg-9">
										<select name="jenjang_jabatan_jabatan_nama" id="jenjang_jabatan_jabatan_nama" data-placeholder="Pilih Jabatan" data-id="0" class="select-size-xs ga_wajib">
										</select>
									</div>
								</div> -->
								<div class="form-group form-group-xs" style="display: none;" id="js">
									<label class="col-lg-3 control-label">Nama Jabatan</label>
									<div class="col-lg-9">
										<select name="jenjang_jabatan_js_nama" id="jenjang_jabatan_js_nama" data-placeholder="Pilih Jabatan" data-id="0" class="select-size-xs ga_wajib" onchange="setjabatanid(this);setJabatan(this)">
											<option></option>
											<?php foreach ($struktural as $i) {
												echo '<option value="'.$i["jabatan_unit_kerja_kode"].'" kode="'.$i["jabatan_kode"].'" jabid="'.$i["jabatan_id"].'" nama_jabatan="'.$i["jabatan_nama"].'" unit_kerja_kode="'.$i["jabatan_unit_kerja_kode"].'">'.$i["jabatan_nama"].'</option>';
											}?>
										</select>
										<input type="text" name="jenjang_js_nama" id="jenjang_js_nama" style="display: none;">
									</div>
								</div>
								<div class="form-group form-group-xs" style="display: none;" id="jfu">
									<label class="col-lg-3 control-label">Nama Jabatan</label>
									<div class="col-lg-9">
										<select name="jenjang_jabatan_jfu_nama" id="jenjang_jabatan_jfu_nama" data-placeholder="Pilih Jabatan" data-id="0" class="select-size-xs ga_wajib" onchange="setjabatanid(this);">
											<option></option>
											<?php foreach ($fungsional_umum as $i) {
												echo '<option value="'.$i["jabatan_nama"].'" kode="'.$i["jabatan_kode"].'" jabid="'.$i["jabatan_id"].'">'.$i["jabatan_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs" style="display: none;" id="jft">
									<label class="col-lg-3 control-label">Nama Jabatan</label>
									<div class="col-lg-9">
										<select name="jenjang_jabatan_jft_nama" id="jenjang_jabatan_jft_nama" data-placeholder="Pilih Jabatan" data-id="0" class="select-size-xs ga_wajib" onchange="setjabatanid(this);">
											<option></option>
											<?php foreach ($fungsional_tertentu as $k) {
												echo '<option value="'.$k["jabatan_jenjang"].'" kode="'.$k["jabatan_kode"].'">'.$k["jabatan_kode"].' - '.$k["jabatan_nama"].' - '.$k["jabatan_jenjang"].'</option>';
			                                    // $dt =$k["jabatan_kode"].' - '.$k["jabatan_nama"];
			                                    // if($k["jabatan_kode"])$dt =  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$k["jabatan_kode"].' - '.$k["jabatan_jenjang"];                                    
			                                    // echo '<option value="'.$k["jabatan_jenjang"].'">'.$dt.'</option>';
											}?>
										</select>
									</div>
								</div>
								<!-- <div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Nama Jabatan</label>
									<div class="col-md-9">
										<input type="text" name="jenjang_jabatan_jabatan_nama" id="jenjang_jabatan_jabatan_nama" class="form-control input-xs wajib">
									</div>
								</div> -->
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Terhitung Mulai Tanggal</label>
									<div class="col-md-9">
										<input type="text" name="jenjang_jabatan_tanggal_mulai" id="jenjang_jabatan_tanggal_mulai" class="form-control input-xs wajib pickttl">
									</div>
								</div>

								<!-- <div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tanggal Selesai</label>
									<div class="col-md-9"> -->
										<input type="hidden" name="jenjang_jabatan_tanggal_selesai" id="jenjang_jabatan_tanggal_selesai" class="form-control input-xs ga_wajib">
									<!-- </div>
								</div> -->

								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Nomor SK</label>
									<div class="col-md-9">
										<input type="text" name="jenjang_jabatan_nomor_sk" id="jenjang_jabatan_nomor_sk" class="form-control input-xs wajib">
									</div>
								</div>

								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tanggal SK</label>
									<div class="col-md-9">
										<input type="text" name="jenjang_jabatan_tanggal_sk" id="jenjang_jabatan_tanggal_sk" class="form-control input-xs wajib pickttl">
									</div>
								</div>
								<div class="form-group form-group-xs" id="ak" style="display: none;">
									<label class="col-md-3 control-label">Angka Kredit</label>
									<div class="col-md-9">
										<input type="text" name="jenjang_jabatan_angka_kredit_old" id="jenjang_jabatan_angka_kredit_old" class="form-control input-xs gak_wajib" onKeyPress="return goodchars(event,'0123456789.',this)" style="display: noe;">
										<input type="text" name="jenjang_jabatan_angka_kredit" id="jenjang_jabatan_angka_kredit" class="form-control input-xs gak_wajib" onKeyPress="return goodchars(event,'0123456789.',this)">
									</div>
								</div>

								<div class="form-group form-group-xs">
									<div class="col-md-9 col-md-offset-3">
										<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
										<input type="hidden" name="jenjang_jabatan_pegawai_id" id="jenjang_jabatan_pegawai_id" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_id"];?>">
										<input type="hidden" name="jenjang_jabatan_pegawai_nip" id="jenjang_jabatan_pegawai_nip" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_nip"];?>">
										<input type="hidden" name="isUpdate" id="isUpdate" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="jenjang_jabatan_id" id="jenjang_jabatan_id" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="jabatan_fix_id" id="jabatan_fix_id" class="form-control input-xs wajib" value="0">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 form_lihat_data" style="display: none;">
						<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Lihat Data Jenjang Jabatan</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Unit Kerja</label>
									<div class="col-md-9">
										<div class="form-control-static unit_kerja_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Unit Kerja Lama</label>
									<div class="col-md-9">
										<div class="form-control-static jenjang_jabatan_unit_kerja_lama_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tipe Jabatan</label>
									<div class="col-md-9">
										<div class="form-control-static jenjang_jabatan_tipe">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Eselon</label>
									<div class="col-md-9">
										<div class="form-control-static jabatan_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Nama Jabatan</label>
									<div class="col-md-9">
										<div class="form-control-static jenjang_jabatan_jabatan_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-lg-3 control-label">Terhitung Mulai Tanggal</label>
									<div class="col-lg-9">
										<div class="form-control-static jenjang_jabatan_tanggal_mulai">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik" style="display: none;">
									<label class="col-md-3 control-label">Tanggal Selesai</label>
									<div class="col-md-9">
										<div class="form-control-static jenjang_jabatan_tanggal_selesai">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Nomor SK</label>
									<div class="col-md-9">
										<div class="form-control-static jenjang_jabatan_nomor_sk">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tanggal SK</label>
									<div class="col-md-9">
										<div class="form-control-static jenjang_jabatan_tanggal_sk">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Angka Kredit</label>
									<div class="col-md-9">
										<div class="form-control-static jenjang_jabatan_angka_kredit">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<div class="col-md-9 col-md-offset-3">
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>
	<script type="text/javascript">
		function setjabatanid(ini){
    	var jabid  = $('option:selected', ini).attr('jabid');
    	$('#jabatan_fix_id').val(jabid);
		}
		function set_non_aktif(id,pegawai_id){
			$('#text_konfirmasi').html('Anda yakin menghapus data ini ?');
			$('#konfirmasipenyimpanan').modal('show');
			$('#setujukonfirmasibutton').unbind();
			$('#setujukonfirmasibutton').on('click', function () {
				$('#konfirmasipenyimpanan').modal('hide');
				$('.angkadoank').inputmask('remove');
				$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
				window.location.href = base_url+'kepegawaian/jabatan/hapus/'+id+'/'+pegawai_id;
			});
		}
		function lihat_data(data){
			$('.form_tambah_data').hide();
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					lihat_data(data);
				});
			}else{
				$('.form_lihat_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_lihat_data").offset().top-60
					}, 500);

					var jabatan_nama = datajson.jabatan_nama;
					var jenjang_jabatan_jabatan_nama = datajson.jenjang_jabatan_jabatan_nama;
					var unit_kerja_nama = datajson.unit_kerja_nama;
					var jenjang_jabatan_unit_kerja_lama_nama = datajson.jenjang_jabatan_unit_kerja_lama_nama;
					var jenjang_jabatan_tipe = datajson.jenjang_jabatan_tipe_text;
					var jenjang_jabatan_tanggal_mulai = datajson.jenjang_jabatan_tanggal_mulai;
					var jenjang_jabatan_tanggal_selesai = datajson.jenjang_jabatan_tanggal_selesai;
					var jenjang_jabatan_nomor_sk = datajson.jenjang_jabatan_nomor_sk;
					var jenjang_jabatan_tanggal_sk = datajson.jenjang_jabatan_tanggal_sk;
					var jenjang_jabatan_angka_kredit = datajson.jenjang_jabatan_angka_kredit;

					$(".jabatan_nama").html(jabatan_nama);
					$(".jenjang_jabatan_jabatan_nama").html(jenjang_jabatan_jabatan_nama);
					$(".unit_kerja_nama").html(unit_kerja_nama);
					$(".jenjang_jabatan_unit_kerja_lama_nama").html(jenjang_jabatan_unit_kerja_lama_nama);
					$(".jenjang_jabatan_tipe").html(jenjang_jabatan_tipe);
					$(".jenjang_jabatan_tanggal_mulai").html(jenjang_jabatan_tanggal_mulai);
					$(".jenjang_jabatan_tanggal_selesai").html(jenjang_jabatan_tanggal_selesai);
					$(".jenjang_jabatan_nomor_sk").html(jenjang_jabatan_nomor_sk);
					$(".jenjang_jabatan_tanggal_sk").html(jenjang_jabatan_tanggal_sk);
					$(".jenjang_jabatan_angka_kredit").html(jenjang_jabatan_angka_kredit);

				});
			}
		}
		function update_data(data){
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			
			$('.form_lihat_data').hide();
			$('.label_aksi_form').html('Form Perubahan Data Riwayat Jabatan');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					update_data(data);
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
					$('#isUpdate').val(1);

					// var unit_kerja_level = datajson.jenjang_jabatan_id;
					// var hirarki = datajson.unit_kerja_hirarki.split('|');
					// alert(hirarki[2]);
					// alert(datajson.unit_kerja_id);
					var unit_kerja_id_5 = datajson.unit_kerja_id_5;
					var unit_kerja_id_4 = datajson.unit_kerja_id_4;
					var unit_kerja_id_3 = datajson.unit_kerja_id_3;
					var unit_kerja_id_2 = datajson.unit_kerja_id_2;
					var unit_kerja_id = datajson.unit_kerja_id;
					if(unit_kerja_id_5 != null && unit_kerja_id_5 != 1){
						$('#jenjang_jabatan_unit_kerja_id_1').attr('data-id', unit_kerja_id_4);
						$('#jenjang_jabatan_unit_kerja_id_2').attr('data-id', unit_kerja_id_3);
						$('#jenjang_jabatan_unit_kerja_id_3').attr('data-id', unit_kerja_id_2);
						$('#jenjang_jabatan_unit_kerja_id_4').attr('data-id', unit_kerja_id);

						$("#jenjang_jabatan_unit_kerja_id_1").val(unit_kerja_id_5);
						$('#jenjang_jabatan_unit_kerja_id_1').trigger('change');						
					}

					else{
						if(unit_kerja_id_4 != null && unit_kerja_id_4 != 1){		
							$('#jenjang_jabatan_unit_kerja_id_1').attr('data-id', unit_kerja_id_3);
							$('#jenjang_jabatan_unit_kerja_id_2').attr('data-id', unit_kerja_id_2);
							$('#jenjang_jabatan_unit_kerja_id_3').attr('data-id', unit_kerja_id);

							$("#jenjang_jabatan_unit_kerja_id_1").val(unit_kerja_id_4);
							$('#jenjang_jabatan_unit_kerja_id_1').trigger('change');
							
						}
						else{
							if(unit_kerja_id_3 != null && unit_kerja_id_3 != 1){
								$('#jenjang_jabatan_unit_kerja_id_1').attr('data-id', unit_kerja_id_2);
								$('#jenjang_jabatan_unit_kerja_id_2').attr('data-id', unit_kerja_id);

								$("#jenjang_jabatan_unit_kerja_id_1").val(unit_kerja_id_3);
								$('#jenjang_jabatan_unit_kerja_id_1').trigger('change');
								
							}
							else{
								if(unit_kerja_id_2 != null && unit_kerja_id_2 != 1){
									$('#jenjang_jabatan_unit_kerja_id_1').attr('data-id', unit_kerja_id);

									$("#jenjang_jabatan_unit_kerja_id_1").val(unit_kerja_id_2);
									$('#jenjang_jabatan_unit_kerja_id_1').trigger('change');
									
								}
								else{
									if(unit_kerja_id != null && unit_kerja_id != 1){
										$("#jenjang_jabatan_unit_kerja_id_1").val(unit_kerja_id);
										$('#jenjang_jabatan_unit_kerja_id_1').trigger('change');
									}
								}
							}
						}
					}
					var jenjang_jabatan_id = datajson.jenjang_jabatan_id;
					var jenjang_jabatan_tipe = datajson.jenjang_jabatan_tipe;
					var jenjang_jabatan_jabatan_id = datajson.jenjang_jabatan_jabatan_id;
					var jenjang_jabatan_jabatan_nama = datajson.jenjang_jabatan_jabatan_nama;
					var jenjang_jabatan_unit_kerja_id = datajson.jenjang_jabatan_unit_kerja_id;
					var jenjang_jabatan_tanggal_mulai = datajson.jenjang_jabatan_tanggal_mulai;
					var jenjang_jabatan_tanggal_selesai = datajson.jenjang_jabatan_tanggal_selesai;
					var jenjang_jabatan_nomor_sk = datajson.jenjang_jabatan_nomor_sk;
					var jenjang_jabatan_tanggal_sk = datajson.jenjang_jabatan_tanggal_sk;
					var jenjang_jabatan_angka_kredit = datajson.jenjang_jabatan_angka_kredit;

					$('#jenjang_jabatan_id').val(jenjang_jabatan_id);
					// $("#jenjang_jabatan_jabatan_nama").val(jenjang_jabatan_jabatan_nama);
					$("#jenjang_jabatan_unit_kerja_id").val(jenjang_jabatan_unit_kerja_id);
					$("#jenjang_jabatan_tanggal_mulai").val(jenjang_jabatan_tanggal_mulai);
					$("#jenjang_jabatan_tanggal_selesai").val(jenjang_jabatan_tanggal_selesai);
					$("#jenjang_jabatan_nomor_sk").val(jenjang_jabatan_nomor_sk);
					$("#jenjang_jabatan_tanggal_sk").val(jenjang_jabatan_tanggal_sk);
					$("#jenjang_jabatan_angka_kredit").val(jenjang_jabatan_angka_kredit);
					$("#jenjang_jabatan_angka_kredit_old").val(jenjang_jabatan_angka_kredit);
					$('#jenjang_jabatan_id').val(datajson.jenjang_jabatan_id);

					$('#jenjang_jabatan_tipe').select2('val', '');
					$('#jenjang_jabatan_tipe').val(jenjang_jabatan_tipe);
					$('#jenjang_jabatan_tipe').trigger('change');

					$('#jenjang_jabatan_jabatan_id').select2('val', '');
					$('#jenjang_jabatan_jabatan_id').val(jenjang_jabatan_jabatan_id);
					$('#jenjang_jabatan_jabatan_id').trigger('change');

					$('#jenjang_jabatan_js_nama').select2('val', '');
					$('#jenjang_jabatan_js_nama').val(jenjang_jabatan_jabatan_nama);
					$('#jenjang_jabatan_js_nama').trigger('change');

					$('#jenjang_jabatan_jft_nama').select2('val', '');
					$('#jenjang_jabatan_jft_nama').val(jenjang_jabatan_jabatan_nama);
					$('#jenjang_jabatan_jft_nama').trigger('change');

					$('#jenjang_jabatan_jfu_nama').select2('val', '');
					$('#jenjang_jabatan_jfu_nama').val(jenjang_jabatan_jabatan_nama);
					$('#jenjang_jabatan_jfu_nama').trigger('change');
				});
}
}

function tambah_data(){
	$('.form_lihat_data').hide();
	reset_form();
	$('.label_aksi_form').html('Form Penambahan Data Riwayat Jabatan');
	if($('.form_tambah_data').is(":visible")){
		$('.form_tambah_data').slideUp('slow',function(){
			$('.form_tambah_data').slideDown('fast',function(){
				$('html, body').animate({
					scrollTop: $(".form_tambah_data").offset().top-60
				}, 500);

			});
		});
	}else{
		$('.form_tambah_data').slideDown('fast',function(){
			$('html, body').animate({
				scrollTop: $(".form_tambah_data").offset().top-60
			}, 500);
		});
	}
}
function batal_data(){
	if($('.form_tambah_data').is(":visible")){
		$('.form_tambah_data').slideUp('slow',function(){
			reset_form();
		});
	}
	if($('.form_lihat_data').is(":visible")){
		$('.form_lihat_data').slideUp('slow',function(){
			reset_form();
		});
	}
}
function reset_form(){
			// $("#jenjang_jabatan_id").val('');
			// $("#jenjang_jabatan_pegawai_id").val('');
			//$("#jenjang_jabatan_pegawai_nip").val('');
			// $('#jenjang_jabatan_unit_kerja_id_1').attr('data-id', 0);
			// $('#jenjang_jabatan_unit_kerja_id_2').attr('data-id', 0);
			// $('#jenjang_jabatan_unit_kerja_id_3').attr('data-id', 0);
			// $('#jenjang_jabatan_unit_kerja_id_4').attr('data-id', 0);

			$("#jenjang_jabatan_tipe").select2('val', ' ');
			$('#jenjang_jabatan_jabatan_id').select2('val', ' ');
			$("#jenjang_jabatan_js_nama").select2('val', ' ');
			$("#jenjang_jabatan_jft_nama").select2('val', ' ');
			$("#jenjang_jabatan_jfu_nama").select2('val', ' ');
			$("#jenjang_js_nama").val('');
			$("#jenjang_jabatan_unit_kerja_id").val('');
			$("#jenjang_jabatan_tanggal_mulai").val('');
			$("#jenjang_jabatan_tanggal_selesai").val('');
			$("#jenjang_jabatan_nomor_sk").val('');
			$("#jenjang_jabatan_tanggal_sk").val('');
			$("#jenjang_jabatan_angka_kredit").val('');
			$('#jenjang_jabatan_unit_kerja_id_1').select2('val', ' ');

			// $("#jenjang_jabatan_unit_kerja_id_1").val('');
			// $('#jenjang_jabatan_unit_kerja_id_1').trigger('change');

			// $("#jenjang_jabatan_unit_kerja_id_2").val('');
			// $('#jenjang_jabatan_unit_kerja_id_2').trigger('change');

			// $("#jenjang_jabatan_unit_kerja_id_3").val('');
			// $('#jenjang_jabatan_unit_kerja_id_3').trigger('change');

			// $("#jenjang_jabatan_unit_kerja_id_4").val('');
			// $('#jenjang_jabatan_unit_kerja_id_4').trigger('change');

			// $("#jenjang_jabatan_unit_kerja_id_5").val('');
			// $('#jenjang_jabatan_unit_kerja_id_5').trigger('change');
			$('.validation-error-label').css('display','none');
		}
		
		$(document).ready(function() {
			$('.select-size-xs').select2();
			var d = new Date();

			$('.pickttl').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});
			$('.pickttlstart').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});

			$('.pickttlend').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});

		//On value change
		$(".pickttlstart").change(function(e) {

			var mulai1 = $("#diklat_tanggal_mulai").val().split(" ");
			var tgl = mulai1[0];
			var bulan = mulai1[1];
			var tahun = mulai1[2];

			$('.pickttlend').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				min: new Date(tahun, 5, 20),
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});

			var yesterday  = new Date((new Date()).valueOf()-1000*60*60*24);
			var neededDates = datas.prdetail.PRD_DATE_NEED;
			var neededDate = neededDates.split('-');

			var tglneed = $('#tanggal_dibutuhkan').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agt', 'Sep', 'Okt', 'Nov', 'Des'],
				weekdaysFull: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu'],
				weekdaysShort: ['Min', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectYears: true,
				selectMonths: true,
				format: 'd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd',
				today: 'Hari ini',
				clear: 'Reset',
				close: 'Batal',
				disable: [
				{ from: [0,0,0], to: yesterday }
				]
			});

			var dpicker = tglneed.pickadate('picker');
			dpicker.set('select', [neededDate[0], (neededDate[1]-1), neededDate[2]]);

		});

	});

		function ToJavaScriptDateViewDoank(value) {
        //var pattern = /Date\(([^)]+)\)/;
        //var results = pattern.exec(value);
        //var dt = new Date(parseFloat(results[1]));
        var MonthInd = GetMonthIndInt((value.getMonth() + 1));
        return value.getDate() + " " + MonthInd + " " + value.getFullYear();
    }

    function GetMonthIndInt(month) {
    	switch (month) {
    		case 1:
    		return "Januari"
    		break;
    		case 2:
    		return "Februari"
    		break;
    		case 3:
    		return "Maret"
    		break;
    		case 4:
    		return "April"
    		break;
    		case 5:
    		return "Mei"
    		break;
    		case 6:
    		return "Juni"
    		break;
    		case 7:
    		return "Juli"
    		break;
    		case 8:
    		return "Agustus"
    		break;
    		case 9:
    		return "September"
    		break;
    		case 10:
    		return "Oktober"
    		break;
    		case 11:
    		return "November"
    		break;
    		case 12:
    		return "Desember"
    		break;
    		default:
    		return "Undefined"
    	}
    }

    function  get_unit_kerja_hirarki(ini,lvl) {
    	var attrid = $(ini).attr('data-id');
    	//alert(attrid);
    	if(lvl==1){
    		$('.jenjang_jabatan_unit_kerja_id_5').slideUp('fast',function(){
    			$('#jenjang_jabatan_unit_kerja_id_5').select2('val', '');
    			$('.jenjang_jabatan_unit_kerja_id_4').slideUp('fast',function(){
    				$('#jenjang_jabatan_unit_kerja_id_4').select2('val', '');
    				$('.jenjang_jabatan_unit_kerja_id_3').slideUp('fast',function(){
    					$('#jenjang_jabatan_unit_kerja_id_3').select2('val', '');
    					$('.jenjang_jabatan_unit_kerja_id_2').slideUp('fast',function(){
    						$('#jenjang_jabatan_unit_kerja_id_2').select2('val', '');
    					});
    				});
    			});
    		});
    	}else if(lvl==2){
    		$('.jenjang_jabatan_unit_kerja_id_5').slideUp('fast',function(){
    			$('#jenjang_jabatan_unit_kerja_id_5').select2('val', '');
    			$('.jenjang_jabatan_unit_kerja_id_4').slideUp('fast',function(){
    				$('#jenjang_jabatan_unit_kerja_id_4').select2('val', '');
    				$('.jenjang_jabatan_unit_kerja_id_3').slideUp('fast',function(){
    					$('#jenjang_jabatan_unit_kerja_id_3').select2('val', '');
    				});
    			});
    		});
    	}else if(lvl==3){
    		$('.jenjang_jabatan_unit_kerja_id_5').slideUp('fast',function(){
    			$('#jenjang_jabatan_unit_kerja_id_5').select2('val', '');
    			$('.jenjang_jabatan_unit_kerja_id_4').slideUp('fast',function(){
    				$('#jenjang_jabatan_unit_kerja_id_4').select2('val', '');
    			});
    		});
    	}else if(lvl==4){
    		$('.jenjang_jabatan_unit_kerja_id_5').slideUp('fast',function(){
    			$('#jenjang_jabatan_unit_kerja_id_5').select2('val', '');
    		});
    	}
    	var parent_id_kode  = $('option:selected', ini).attr('parent_id_kode');
    	var jabatan_id  = $('option:selected', ini).attr('jabatan_id');
    	var unit_kerja_kode  = $('option:selected', ini).attr('unit_kerja_kode');
    	var unit_kerja_nama  = $('option:selected', ini).attr('unit_kerja_nama');



    	// $('#jenjang_jabatan_jabatan_id').select2('val', '');
    	// $('#jenjang_jabatan_jabatan_id').val(jabatan_id);
    	// $('#jenjang_jabatan_jabatan_id').trigger('change');
    	
    	
    	$('#jenjang_jabatan_js_nama').select2('val', 'unit_kerja_kode');
    	$('#jenjang_jabatan_js_nama').val(unit_kerja_kode);
    	$('#jenjang_jabatan_js_nama').trigger('change');
    	
    	$.ajax({
    		url: base_url+'kepegawaian/pns/get_unit_kerja_hirarki',
    		type: "post",
    		dataType: 'json',
    		data: { unit_kerja_parent_id_kode: parent_id_kode, unit_kerja_level: (lvl+2)},
    		success: function (data) {

    			var unit_kerja = data.unit_kerja;
    			var newoption = "<option></option>";

    			for(var i = 0; i < unit_kerja.length; i ++){
    				newoption+='<option value="'+unit_kerja[i].unit_kerja_id+'" parent_id_kode="'+unit_kerja[i].unit_kerja_id_kode+'" unit_kerja_kode="'+unit_kerja[i].unit_kerja_kode+'"  jabatan_id="'+unit_kerja[i].jabatan_id+'" unit_kerja_nama="'+unit_kerja[i].unit_kerja_nama+'" '+((unit_kerja[i].unit_kerja_id == attrid)?"selected":"")+'>'+unit_kerja[i].unit_kerja_nama+'</option>';
    			}
    			$('#jenjang_jabatan_unit_kerja_id_'+(lvl+1)).html(newoption);
    			if(unit_kerja.length>0){
    				$('.jenjang_jabatan_unit_kerja_id_'+(lvl+1)).slideDown('fast');
    			}else{
    				$('.jenjang_jabatan_unit_kerja_id_'+(lvl+1)).slideUp('fast');
    			}
    			if(attrid > 1){
    				$('#jenjang_jabatan_unit_kerja_id_'+(lvl+1)).trigger('change');
    			}
    		}
    	});
    	var newlistunker = [];
    	var i = 0;
    	$('select[name="jenjang_jabatan_unit_kerja_id[]"]').each(function(){
    		var ids = $(this).attr('id');
    		var namaunker = $('#'+ids+' option:selected').text();
    		if(namaunker!=""){
    			newlistunker[i] =namaunker;
    			i++;	
    		}

    	});
    	$('#jenjang_jabatan_unit_kerja_lama_nama').val(newlistunker.join(', '));
    	$('#jenjang_jabatan_unit_kerja_lama_nama').trigger('change');

    }

 //    $('#jenjang_jabatan_tipe').change(function(event) {
	// 	var jabatan_tipe = $('option:selected', this).val();
	// 	// alert(jabatan_tipe);
	// 	$.ajax({
	// 		url: '<?php echo base_url();?>kepegawaian/jabatan/get_jabatan',
	// 		type: 'POST',
	// 		dataType: 'json',
	// 		data: {jabatan_tipe: jabatan_tipe},
	// 		success: function (data) {
	// 			$.each(data, function(index, val) {
	// 				$('#jenjang_jabatan_jabatan_nama').append('<option value="'+val.jabatan_nama+'">'+val.jabatan_nama+'</option>'); 
	// 				var hasil = "<option value=''></option>";
	// 				for(var i = 0; i < data.length; i ++){
	// 					hasil+='<option value="'+data[i].jabatan_nama+'">'+data[i].jabatan_nama+'</option>';
	// 				}
	// 				var hasil = '<option value="'+val.jabatan_nama+'">'+val.jabatan_nama+'</option>';
	// 				$('#jenjang_jabatan_jabatan_nama').html(hasil);
	// 			});
	// 		}
	// 	});

	// });
	$('#jenjang_jabatan_tipe').change(function(){
		var jabatan_tipe = $('option:selected', this).val();
		// alert(jabatan_tipe);
		if(jabatan_tipe == 1){
			$('#js').show();
			$('#eselon').show();
			$('#ak').hide();
			$('#jft').hide();
			$('#jfu').hide();
		}else if(jabatan_tipe == 2){
			$('#jft').show();
			$('#ak').show();
			$('#eselon').hide();
			$('#js').hide();
			$('#jfu').hide();
		}else if(jabatan_tipe == 3){
			$('#jfu').show();
			$('#eselon').hide();
			$('#ak').hide();
			$('#js').hide();
			$('#jft').hide();
		}else{
			$('#jfu').hide();
			$('#eselon').hide();
			$('#ak').hide();
			$('#js').hide();
			$('#jft').hide();
		}
	});

	function getkey(e)
	{
		if (window.event)
			return window.event.keyCode;
		else if (e)
			return e.which;
		else
			return null;
	}
	function goodchars(e, goods, field)
	{
		var key, keychar;
		key = getkey(e);
		if (key == null) return true;

		keychar = String.fromCharCode(key);
		keychar = keychar.toLowerCase();
		goods = goods.toLowerCase();

		// check goodkeys
		if (goods.indexOf(keychar) != -1)
			return true;
		// control keys
		if ( key==null || key==0 || key==8 || key==9 || key==27 )
			return true;

		if (key == 13) {
			var i;
			for (i = 0; i < field.form.elements.length; i++)
				if (field == field.form.elements[i])
					break;
				i = (i + 1) % field.form.elements.length;
				field.form.elements[i].focus();
				return false;
			};
		// else return false
		return false;
	}

	function setJabatan(ini) {
	// $('#jenjang_jabatan_js_nama').on('change', function() {
		var nama_jabatan = $('option:selected', ini).attr('nama_jabatan');

		$('#jenjang_js_nama').val("").val(nama_jabatan);
	}
</script>
