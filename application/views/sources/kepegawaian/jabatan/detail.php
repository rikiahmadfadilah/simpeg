<script src="<?php echo base_url();?>assets/js/plugins/forms/wizards/stepy.min.js"></script>
<script src="<?php echo base_url();?>assets/js/demo_pages/wizard_stepy.js"></script>

<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Riwayat Pegawai</h6>
		<div class="heading-elements">
			<div class="heading-btn">
				<a href="<?php echo base_url().'kepegawaian/jabatan/createjabatan/'.$pegawai_id.'/'.$pegawai_nip.'';?>" class="btn btn-primary btn-labeled btn-xs"><b><i class=" icon-plus3"></i></b> Tambah Data Jabatan</a>
			</div>
		</div>
	</div>

	<div class="panel-body" >
		<div class="row">
			<table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive epagawai_table" id="list_data">
				<thead>
					<tr>
						<th rowspan="2" class="text-center"style="width: 175px;min-width: 175px;">NIP</th>
						<th rowspan="2" class="text-center">Nama Jabatan</th>
						<th rowspan="2" class="text-center">Unit Kerja</th>
						<th rowspan="2" class="text-center">Nomor SK</th>
						<th colspan="2" class="text-center">Periode</th>					
						<th rowspan="2" class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
					</tr>
					<tr>
						<th class="text-center">Masuk</th>
						<th class="text-center">Keluar</th>
					</tr>
				</thead>
				<tbody>
					<?php if (count($jabatan) > 0) {
						 foreach ($jabatan as $i) {
						echo '<tr><td>'.$i["jenjang_jabatan_pegawai_nip"].'</td><td>'.$i["jenjang_jabatan_jabatan"].'</td><td>'.$i["jenjang_jabatan_unit_kerja"].'</td><td>'.$i["jenjang_jabatan_nomor_sk"].'</td><td>'.$i["jenjang_jabatan_tanggal_mulai"].'</td><td>'.$i["jenjang_jabatan_tanggal_selesai"].'</td><td><ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/jabatan/detail/'.$i["jenjang_jabatan_pegawai_nip"].'"><i class="icon-file-text"></i></a></li>
				</ul></td></tr>';
				# code...
					}
					
					}?>
				</tbody>
			</table>
		</div>	
		
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#list_data').dataTable( {
			// "processing": true,
			// "serverSide": true,
			// "bServerSide": true,
			// "sAjaxSource": base_url+"kepegawaian/jabatan/list_data_jabatan?$param1=<?php echo $pegawai_nip ?>",
			// "aaSorting": [],
			// "order": [],
			// "iDisplayLength": 50,
			// "aoColumns": [
			// { "sClass": "text-left" },
			// { "sClass": "text-left" },
			// { "sClass": "text-left" },
			// { "sClass": "text-left" },
			// { "sClass": "text-center" },
			// { "sClass": "text-center" },
			// { "bSortable": false, "sClass": "text-center" },
			// ],
			// "fnDrawCallback": function () {
			// 	set_default_datatable();
			// },
		});
	});
</script>