<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Data Pegawai PNS</h6>
		<div class="heading-elements">
			<div class="heading-btn">
				<a href="<?php echo base_url().'kepegawaian/pns/create';?>" class="btn btn-primary btn-labeled btn-xs"><b><i class=" icon-plus3"></i></b> Tambah Data Pegawai PNS</a>
			</div>
		</div>
	</div>

	<div class="panel-body">
		<table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive epagawai_table" id="list_data">
			<thead>
				<tr>
					<th class="text-center"style="width: 175px;min-width: 175px;">NIP</th>
					<th class="text-center">Nama</th>
					<th class="text-center">Tempat/Tanggal Lahir</th>
					<th class="text-center">Nama Jabatan</th>
					<th class="text-center">Unit Kerja</th>
					<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
				</tr>
			</thead>
			
		</table>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#list_data').DataTable( {
			"processing": true,
			"serverSide": true,
			"sAjaxSource": base_url+"kepegawaian/pns/list_data",
			 "aaSorting": [],
			 "order": [],
			 "iDisplayLength": 10,
			"aoColumns": [
			{ "sClass": "text-left" },
			{ "sClass": "text-left" },
			{ "sClass": "text-left" },
			{ "sClass": "text-left" },
			{ "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-center" },
			],
			"fnDrawCallback": function () {
				set_default_datatable();
			},
		});
	});
</script>
