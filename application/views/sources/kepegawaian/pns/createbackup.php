<script src="<?php echo base_url();?>assets/js/plugins/forms/wizards/stepy.min.js"></script>
<script src="<?php echo base_url();?>assets/js/demo_pages/wizard_stepy.js"></script>

<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">TAMBAH DATA PEGAWAI PNS</h6>
	</div>

	<div class="panel-body" >
		<div class="row">
			<form class="stepy-validation " action="#">
				<fieldset title="1">
					<legend class="text-semibold">Biodata</legend>

					<div class="row">
						<div class="col-md-6">
							<legend class="text-bold">Data Pokok</legend>
							<div class="form-horizontal">
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">NIP</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_nip" id="pegawai_nip"  class="form-control input-xs wajib" type="text">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">KTP</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_nomor_ktp" id="pegawai_nomor_ktp" class="form-control input-xs wajib ktp">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">NPWP</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_nomor_npwp" id="pegawai_nomor_npwp" class="form-control input-xs wajib npwp">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Gelar</label>
									<div class="col-lg-9">
										<div class="row">
											<div class="col-md-6">
												<input type="text" name="pegawai_gelar_depan" id="pegawai_gelar_depan" class="form-control input-xs wajib" placeholder="Depan" type="text">
											</div>

											<div class="col-md-6">
												<input type="text" name="pegawai_gelar_belakang" id="pegawai_gelar_belakang" class="form-control input-xs wajib" placeholder="Belakang" type="text">
											</div>
										</div>
									</div>
								</div>

								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Nama Lengkap</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_nama" id="pegawai_nama" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Tempat Lahir</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_tempat_lahir" id="pegawai_tempat_lahir" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Tanggal Lahir</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_tanggal_lahir" id="pegawai_tanggal_lahir" class="form-control input-xs wajib pickttl">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Jenis Kelamin</label>
									<div class="col-lg-9">
										<?php foreach ($jenis_kelamin as $i) {
											echo '<label class="radio-inline">';
											echo '<input name="pegawai_jenis_kelamin_id" id="pegawai_jenis_kelamin_id_'.$i["jenis_kelamin_id"].'" value="'.$i["jenis_kelamin_id"].'" type="radio">';
											echo ''.$i["jenis_kelamin_nama"].'';
											echo '</label>';
										}?>

									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Status Kawin</label>
									<div class="col-lg-9">
										<select name="pegawai_perkawinan_id" id="pegawai_perkawinan_id" data-placeholder="Pilih Status Kawin" class="select-size-xs">
											<option></option>
											<?php foreach ($status_perkawinan as $i) {
												echo '<option value="'.$i["perkawinan_id"].'">'.$i["perkawinan_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Agama</label>
									<div class="col-lg-9">
										<select name="pegawai_agama_id" id="pegawai_agama_id" data-placeholder="Pilih Agama" class="select-size-xs">
											<option></option>
											<?php foreach ($agama as $i) {
												echo '<option value="'.$i["agama_id"].'">'.$i["agama_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Golongan Darah</label>
									<div class="col-lg-9">
										<select name="pegawai_golongan_darah_id" id="pegawai_golongan_darah_id" data-placeholder="Pilih Golongan Darah" class="select-size-xs">
											<option></option>
											<?php foreach ($golongan_darah as $i) {
												echo '<option value="'.$i["golongan_darah_id"].'">'.$i["golongan_darah_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<legend class="text-bold">Data Tambahan</legend>
								<div class="form-horizontal">
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">Photo</label>
										<div class="col-lg-9">
											<img src="http://localhost/epegawai/assets/images/default_user.jpg" height="150px;" id="image_pegawai">
											<div class="clearfix"></div>
											<input type="file" name="pegawai_photo" id="pegawai_photo" class="form-control input-xs" style="padding-top: 0px !important;padding-bottom: 0px !important;">
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">Email KKP</label>
										<div class="col-lg-9">
											<input type="text" name="pegawai_alamat_email" id="pegawai_alamat_email" class="form-control input-xs wajib">
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">No. HP</label>
										<div class="col-lg-9">
											<input type="text" name="pegawai_telepon" id="pegawai_telepon" data-mask="(999) 999-9999" class="form-control input-xs wajib">
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">No. KARPEG</label>
										<div class="col-lg-9">
											<input type="text" name="pegawai_nomor_karpeg" id="pegawai_nomor_karpeg" class="form-control input-xs wajib">
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">No. ASKES</label>
										<div class="col-lg-9">
											<input type="text" name="pegawai_nomor_askes" id="pegawai_nomor_askes" class="form-control input-xs wajib">
										</div>
									</div>
									<div class="form-group form-group-xs">
										<label class="col-lg-3 control-label">No. KARIS</label>
										<div class="col-lg-9">
											<input type="text" name="pegawai_nomor_karis" id="pegawai_nomor_karis" class="form-control input-xs wajib">
										</div>
									</div>
								</div>
							</div>
							
						</div>

						<div class="col-md-6">
							
							<legend class="text-bold">Alamat Domisili</legend>
							<div class="form-horizontal">
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Negara</label>
									<div class="col-lg-9">
										<select name="pegawai_domisili_negara_type" id="pegawai_domisili_negara_type" data-placeholder="Pilih Negara" class="select-size-xs">
											<option></option>
											<?php foreach ($negara as $i) {

												echo '<option value="'.$i["negara_id"].'" '.(($i["negara_type"]==1)?"selected":"").'>'.$i["negara_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Provinsi</label>
									<div class="col-lg-9">
										<select name="pegawai_domisili_provinsi_id" id="pegawai_domisili_provinsi_id" data-placeholder="Pilih Provinsi" class="select-size-xs" onchange="get_kota(this,'pegawai_domisili_kota_id')">
											<option></option>
											<?php foreach ($provinsi as $i) {

												echo '<option value="'.$i["provinsi_id"].'">'.$i["provinsi_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Kota</label>
									<div class="col-lg-9">
										<select name="pegawai_domisili_kota_id" id="pegawai_domisili_kota_id" data-placeholder="Pilih Kota" class="select-size-xs">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label">Alamat</label>
									<div class="col-lg-9">
										<textarea name="pegawai_domisili_alamat" id="pegawai_domisili_alamat" rows="3" cols="3" class="form-control elastic wajib" ></textarea>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Kode Pos</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_domisili_kodepos" id="pegawai_domisili_kodepos" class="form-control input-xs wajib kodepos">
									</div>
								</div>
							</div>
							<legend class="text-bold">Alamat Sesuai KTP</legend>
							<div class="form-horizontal">
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Provinsi</label>
									<div class="col-lg-9">
										<select name="pegawai_ktp_provinsi_id" id="pegawai_ktp_provinsi_id" data-placeholder="Pilih Provinsi" class="select-size-xs" onchange="get_kota(this,'pegawai_ktp_kota_id')">
											<option></option>
											<?php foreach ($provinsi as $i) {
												echo '<option value="'.$i["provinsi_id"].'">'.$i["provinsi_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Kota</label>
									<div class="col-lg-9">
										<select name="pegawai_ktp_kota_id" id="pegawai_ktp_kota_id" data-placeholder="Pilih Kota" class="select-size-xs">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label">Alamat</label>
									<div class="col-lg-9">
										<textarea name="pegawai_ktp_alamat" id="pegawai_ktp_alamat" rows="3" cols="3" class="form-control elastic wajib" ></textarea>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Kode Pos</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_ktp_kodepos" id="pegawai_ktp_kodepos" class="form-control input-xs wajib kodepos">
									</div>
								</div>
							</div>

						</div>
					</div>
				</fieldset>

				<fieldset title="2">
					<legend class="text-semibold">Pendidikan</legend>

					<div class="row">
						<div class="col-md-6">
							<legend class="text-bold">Pendidikan</legend>
							<div class="form-horizontal">
								
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Pendidikan Terakhir</label>
									<div class="col-lg-3">
										<select name="pegawai_pendidikan_terakhir_id" id="pegawai_pendidikan_terakhir_id" data-placeholder="Pendidikan Terakhir" class="select-size-xs"  onchange="get_tingkat_pendidikan(this,'pegawai_tingkat_pendidikan_id')">
											<option></option>
											<?php foreach ($pendidikan as $i) {
												echo '<option value="'.$i["pendidikan_id"].'" data-kode="'.$i["pendidikan_kode"].'">'.$i["pendidikan_nama"].'</option>';
											}?>
										</select>
									</div>
									<label class="col-lg-3 control-label">Tahun Lulus</label>
									<div class="col-lg-3">
										<select name="pegawai_pendidikan_tahun_lulus" id="pegawai_pendidikan_tahun_lulus" data-placeholder="Pilih Tahun Lulus" class="select-size-xs">
											<option></option>
											<?
											for($i=2018;$i>=1950;$i--) {
												echo '<option value="'.$i.'">'.$i.'</option>';
											}
											?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Tingkat Pendidikan</label>
									<div class="col-lg-9">
										<select name="pegawai_tingkat_pendidikan_id" id="pegawai_tingkat_pendidikan_id" data-placeholder="Pilih Tingkat" class="select-size-xs" onchange="get_jurusan(this,'pegawai_jurusan_id')">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Jurusan</label>
									<div class="col-lg-9">
										<select name="pegawai_jurusan_id" id="pegawai_jurusan_id" data-placeholder="Pilih Jurusan" class="select-size-xs">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Nama Sekolah</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_nama_pendidikan" id="pegawai_nama_pendidikan" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Tempat Belajar</label>
									<div class="col-lg-3">
										<select name="pegawai_tempat_pendidikan" id="pegawai_tempat_pendidikan" data-placeholder="Tempat Belajar" class="select-size-xs">
											<option></option>
											<option value="1">Dalam Negeri</option>
											<option value="2">Luar Negeri</option>
										</select>
									</div>
									<label class="col-lg-3 control-label">IPK</label>
									<div class="col-lg-3">
										<input type="text" name="pegawai_nilai_ipk" id="pegawai_nilai_ipk" class="form-control input-xs wajib kodepos">

									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Program Studi</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_program_studi" id="pegawai_program_studi" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Nilai TOEFL</label>
									<div class="col-lg-3">
										<input type="text" name="pegawai_nilai_toefl" id="pegawai_nilai_toefl" class="form-control input-xs wajib">

									</div>
									<label class="col-lg-3 control-label">Nilai IELT</label>
									<div class="col-lg-3">
										<input type="text" name="pegawai_nilai_ielt" id="pegawai_nilai_ielt" class="form-control input-xs wajib">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<legend class="text-bold">Diklat</legend>
							<div class="form-horizontal">
								
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">DIKLAT Perjenjangan</label>
									<div class="col-lg-3">
										<select name="pegawai_pendidikan_terakhir_id" id="pegawai_pendidikan_terakhir_id" data-placeholder="Pendidikan Terakhir" class="select-size-xs">
											<option></option>
											<option value="1">DiklatPim Tk.I</option>
											<option value="2">DiklatPim Tk.II</option>
											<option value="3">DiklatPim Tk.III</option>
											<option value="4">DiklatPim Tk.IV</option>
											<option value="5">DiklatPim Tk.V</option>
										</select>
									</div>
									<label class="col-lg-3 control-label">Tahun</label>
									<div class="col-lg-3">
										<select name="pegawai_pendidikan_tahun_lulus" id="pegawai_pendidikan_tahun_lulus" data-placeholder="Pilih Tahun Lulus" class="select-size-xs">
											<option></option>
											<?
											for($i=2018;$i>=1950;$i--) {
												echo '<option value="'.$i.'">'.$i.'</option>';
											}
											?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Nama Diklat</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_nama_pendidikan" id="pegawai_nama_pendidikan" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">LEMHANAS</label>
									<div class="col-lg-9">
										<select name="pegawai_jurusan_id" id="pegawai_jurusan_id" data-placeholder="Pilih Jurusan" class="select-size-xs">
											<option></option>
											<option value="1">Sudah</option>
											<option value="1">Belum</option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Predikat</label>
									<div class="col-lg-3">
										<input type="text" name="pegawai_predikat" id="pegawai_predikat" class="form-control input-xs wajib">

									</div>
									<label class="col-lg-3 control-label">Angkatan</label>
									<div class="col-lg-3">
										<input type="text" name="pegawai_nama_pendidikan" id="pegawai_nama_pendidikan" class="form-control input-xs wajib">

									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Catatan</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_catatan" id="pegawai_catatan" class="form-control input-xs wajib">
									</div>
								</div>
							</div>
						</div>
					</div>
				</fieldset>

				<fieldset title="3">
					<legend class="text-semibold">Jabatan</legend>

					<div class="row">
						<div class="col-md-12">
							<legend class="text-bold">Jabatan</legend>
							<div class="form-horizontal">
								
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Unit Kerja</label>
									<div class="col-lg-9">
										<select name="pegawai_unit_kerja_id[]" id="pegawai_unit_kerja_id_1" data-placeholder="Pilih Unit Kerja" class="select-size-xs"  onchange="get_tingkat_pendidikan(this,'pegawai_tingkat_pendidikan_id')">
											<option></option>
											<?php foreach ($unit_kerja as $i) {
												echo '<option value="'.$i["unit_kerja_kode"].'">'.$i["unit_kerja_nama_with_tab"].'</option>';
											}?>
										</select>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</fieldset>


				<button type="submit" class="btn btn-xs btn-success stepy-finish">Simpan <i class="icon-checkmark5 position-right"></i></button>
			</form>
		</div>
		
		
	</div>
</div>
<script type="text/javascript">
	function  get_kota(ini,nextopsi) {
		var provinsi_kode = $(ini).val();
		$.ajax({
			url: base_url+'kepegawaian/pns/get_kota',
			type: "post",
			dataType: 'json',
			data: { provinsi_kode: provinsi_kode},
			success: function (data) {
				var kota = data.kota;
				var newoption = "<option></option>";
				for(var i = 0; i < kota.length; i ++){
					newoption+='<option value="'+kota[i].kota_kode+'">'+kota[i].kota_nama+'</option>';
				}
				$('#'+nextopsi).html(newoption);
			}
		});
	}
	function  get_tingkat_pendidikan(ini,nextopsi) {
		var pendidikan_kode = $('option:selected', ini).attr('data-kode');
		$.ajax({
			url: base_url+'kepegawaian/pns/get_tingkat_pendidikan',
			type: "post",
			dataType: 'json',
			data: { pendidikan_kode: pendidikan_kode},
			success: function (data) {
				var tingkat_pendidikan = data.tingkat_pendidikan;
				var newoption = "<option></option>";
				for(var i = 0; i < tingkat_pendidikan.length; i ++){
					newoption+='<option value="'+tingkat_pendidikan[i].jurusan_kode+'" '+((tingkat_pendidikan.length==1)?"selected":"")+'>'+tingkat_pendidikan[i].jurusan_grup+'</option>';
				}
				$('#'+nextopsi).html(newoption);
				get_jurusan($('#pegawai_tingkat_pendidikan_id'),'pegawai_jurusan_id');
			}
		});
	}
	function  get_jurusan(ini,nextopsi) {
		var jurusan_kode = $(ini).val();
		$.ajax({
			url: base_url+'kepegawaian/pns/get_jurusan',
			type: "post",
			dataType: 'json',
			data: { jurusan_kode: jurusan_kode},
			success: function (data) {
				var jurusan = data.jurusan;
				var newoption = "<option></option>";
				
				for(var i = 0; i < jurusan.length; i ++){
					if(jurusan[i].jurusan_grup != jurusan[i].jurusan_nama){
						newoption+='<option value="'+jurusan[i].jurusan_kode+'">'+jurusan[i].jurusan_nama+'</option>';
					}
				}
				$('#'+nextopsi).html(newoption);
			}
		});
	}
	$(document).ready(function() {
		$('[name="pegawai_nip"]').formatter({
			pattern: '{{99999999}}  {{999999}}  {{9}}  {{999}}'
		});
		$('.kodepos').formatter({
			pattern: '{{99999}}'
		});
		$('.ktp').formatter({
			pattern: '{{9999999999999999}}'
		});
		$('.npwp').formatter({
			pattern: '{{99}}  {{999}}  {{999}}  {{9999}} {{999}}'
		});
		//$('#pegawai_ktp_negara_id').select2('readonly',true);
		$("#pegawai_ktp_negara_id").prop("disabled", true);
		var d = new Date();

		$('.pickttl').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
		
		var form = $(".steps-pegawai").show();


	});
	function readURL(input) {

		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
				$('#image_pegawai').attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}

	$("#pegawai_photo").change(function() {
		readURL(this);
	});
</script>
