
<script src="<?php echo base_url();?>assets/js/plugins/forms/wizards/steps.min.js"></script>
<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Update Data Pegawai PNS</h6>
	</div>

	<div class="panel-body">
		<form class="steps-validation need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<h6>Biodata</h6>
			<fieldset>
				<div class="row">
					<div class="col-md-12">
						<legend class="text-bold" style="padding-bottom:5px;border-bottom:solid 1px #00bcd4;">Data Pokok</legend>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">NIP</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_nip" id="pegawai_nip" value="<?php echo $pegawai["pegawai_nip"];?>" class="form-control input-xs ga_wajibzz" type="text">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">KTP</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_nomor_ktp" id="pegawai_nomor_ktp" value="<?php echo $pegawai["pegawai_nomor_ktp"];?>" class="form-control input-xs ga_wajibzz ktp">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">NPWP</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_nomor_npwp" id="pegawai_nomor_npwp" value="<?php echo $pegawai["pegawai_nomor_npwp"];?>" class="form-control input-xs ga_wajibzz npwp">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Gelar</label>
									<div class="col-lg-9">
										<div class="row">
											<div class="col-md-6">
												<input type="text" name="pegawai_gelar_depan" id="pegawai_gelar_depan" value="<?php echo $pegawai["pegawai_gelar_depan"];?>" class="form-control input-xs ga_wajibzz" placeholder="Depan" type="text">
											</div>

											<div class="col-md-6">
												<input type="text" name="pegawai_gelar_belakang" id="pegawai_gelar_belakang" value="<?php echo $pegawai["pegawai_gelar_belakang"];?>" class="form-control input-xs ga_wajibzz" placeholder="Belakang" type="text">
											</div>
										</div>
									</div>
								</div>

								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Nama Lengkap</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_nama" id="pegawai_nama" value="<?php echo $pegawai["pegawai_nama"];?>" class="form-control input-xs ga_wajibzz">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Tempat Lahir</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_tempat_lahir" id="pegawai_tempat_lahir" value="<?php echo $pegawai["pegawai_tempat_lahir"];?>" class="form-control input-xs ga_wajibzz">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Tanggal Lahir </label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_tanggal_lahir" id="pegawai_tanggal_lahir" value="<?php echo dateEnToId($pegawai["pegawai_tanggal_lahir"],'d F Y');?>" class="form-control input-xs ga_wajibzz pickttl">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Jenis Kelamin</label>
									<div class="col-lg-9">
										<?php foreach ($jenis_kelamin as $i) {
											echo '<label class="radio-inline">';
											echo '<input name="pegawai_jenis_kelamin_id" '.(($pegawai["pegawai_jenis_kelamin_id"]==$i["jenis_kelamin_id"])?"checked":"").' id="pegawai_jenis_kelamin_id_'.$i["jenis_kelamin_id"].'" value="'.$i["jenis_kelamin_id"].'" type="radio">';
											echo ''.$i["jenis_kelamin_nama"].'';
											echo '</label>';
										}?>

									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Status Kawin</label>
									<div class="col-lg-9">
										<select name="pegawai_perkawinan_id" id="pegawai_perkawinan_id" data-placeholder="Pilih Status Kawin" class="select2-wizard">
											<option></option>
											<?php foreach ($status_perkawinan as $i) {
												echo '<option value="'.$i["perkawinan_id"].'" '.(($pegawai["pegawai_perkawinan_id"]==$i["perkawinan_id"])?"selected":"").'>'.$i["perkawinan_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Agama</label>
									<div class="col-lg-9">
										<select name="pegawai_agama_id" id="pegawai_agama_id" data-placeholder="Pilih Agama" class="select2-wizard">
											<option></option>
											<?php foreach ($agama as $i) {
												echo '<option value="'.$i["agama_id"].'" '.(($pegawai["pegawai_agama_id"]==$i["agama_id"])?"selected":"").'>'.$i["agama_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Golongan Darah</label>
									<div class="col-lg-9">
										<select name="pegawai_golongan_darah_id" id="pegawai_golongan_darah_id" data-placeholder="Pilih Golongan Darah" class="select2-wizard">
											<option></option>
											<?php foreach ($golongan_darah as $i) {
												echo '<option value="'.$i["golongan_darah_id"].'" '.(($pegawai["pegawai_golongan_darah_id"]==$i["golongan_darah_id"])?"selected":"").'>'.$i["golongan_darah_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Alamat Domisili</span>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Negara</label>
									<div class="col-lg-9">
										<select name="pegawai_domisili_negara_id" id="pegawai_domisili_negara_id" data-placeholder="Pilih Negara" class="select2-wizard" onchange="get_provinsi(this)">
											<option></option>
											<?php foreach ($negara as $i) {

												echo '<option value="'.$i["negara_id"].'" '.(($i["negara_type"]==1)?"selected":"").' data-negara-type="'.$i["negara_type"].'">'.$i["negara_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs dalam_negeri">
									<label class="col-lg-3 control-label">Provinsi</label>
									<div class="col-lg-9">
										<select name="pegawai_domisili_provinsi_id" id="pegawai_domisili_provinsi_id" data-oldvalue="<?php echo $pegawai["pegawai_domisili_kota_id"];?>" data-placeholder="Pilih Provinsi" class="select2-wizard" onchange="get_kota(this,'pegawai_domisili_kota_id')">
											<option></option>
											<?php foreach ($provinsi as $i) {

												echo '<option value="'.$i["provinsi_kode"].'" '.(($pegawai["pegawai_domisili_provinsi_id"]==$i["provinsi_kode"])?"selected":"").'>'.$i["provinsi_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label"></label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_domisili_provinsi_text" id="pegawai_domisili_provinsi_text" value="<?php echo $pegawai["pegawai_domisili_provinsi_text"];?>" class="form-control input-xs ga_wajibzz" readonly="readonly">
									</div>
								</div>
								<div class="form-group form-group-xs dalam_negeri">
									<label class="col-lg-3 control-label">Kota</label>
									<div class="col-lg-9">
										<select name="pegawai_domisili_kota_id" id="pegawai_domisili_kota_id" data-oldvalue="<?php echo $pegawai["pegawai_domisili_kec_id"];?>" data-placeholder="Pilih Kota" class="select2-wizard" onchange="get_kecamatan(this,'pegawai_domisili_kec_id')">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label"></label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_domisili_kota_text" id="pegawai_domisili_kota_text" value="<?php echo $pegawai["pegawai_domisili_kota_text"];?>" class="form-control input-xs ga_wajibzz" readonly="readonly">
									</div>
								</div>
								<div class="form-group form-group-xs dalam_negeri">
									<label class="col-lg-3 control-label">Kecamatan</label>
									<div class="col-lg-9">
										<select name="pegawai_domisili_kec_id" id="pegawai_domisili_kec_id" data-oldvalue="<?php echo $pegawai["pegawai_domisili_kel_id"];?>" data-placeholder="Pilih Kecamatan" class="select2-wizard" onchange="get_kelurahan(this,'pegawai_domisili_kel_id')">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs dalam_negeri">
									<label class="col-lg-3 control-label">Kelurahan</label>
									<div class="col-lg-9">
										<select name="pegawai_domisili_kel_id" id="pegawai_domisili_kel_id" data-placeholder="Pilih Kelurahan" class="select2-wizard">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label">Alamat</label>
									<div class="col-lg-9">
										<textarea name="pegawai_domisili_alamat" id="pegawai_domisili_alamat" rows="3" cols="3" class="form-control elastic ga_wajibzz" ><?php echo $pegawai["pegawai_domisili_alamat"];?></textarea>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Kode Pos</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_domisili_kodepos" id="pegawai_domisili_kodepos" value="<?php echo $pegawai["pegawai_domisili_kodepos"];?>" class="form-control input-xs ga_wajibzz kodepos">
									</div>
								</div>
								<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Alamat Sesuai KTP</span>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Provinsi</label>
									<div class="col-lg-9">
										<select name="pegawai_ktp_provinsi_id" id="pegawai_ktp_provinsi_id" data-oldvalue="<?php echo $pegawai["pegawai_ktp_kota_id"];?>" data-placeholder="Pilih Provinsi" class="select2-wizard" onchange="get_kota(this,'pegawai_ktp_kota_id')">
											<option></option>
											<?php foreach ($provinsi as $i) {
												echo '<option value="'.$i["provinsi_kode"].'" '.(($pegawai["pegawai_ktp_provinsi_id"]==$i["provinsi_kode"])?"selected":"").'>'.$i["provinsi_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label"></label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_ktp_provinsi_text" id="pegawai_ktp_provinsi_text" value="<?php echo $pegawai["pegawai_ktp_provinsi_text"];?>" class="form-control input-xs ga_wajibzz" readonly="readonly">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Kota</label>
									<div class="col-lg-9">
										<select name="pegawai_ktp_kota_id" id="pegawai_ktp_kota_id" data-oldvalue="<?php echo $pegawai["pegawai_ktp_kec_id"];?>" data-placeholder="Pilih Kota" class="select2-wizard" onchange="get_kecamatan(this,'pegawai_ktp_kec_id')">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label"></label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_ktp_kota_text" id="pegawai_ktp_kota_text" value="<?php echo $pegawai["pegawai_ktp_kota_text"];?>" class="form-control input-xs ga_wajibzz" readonly="readonly">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Kecamatan</label>
									<div class="col-lg-9">
										<select name="pegawai_ktp_kec_id" id="pegawai_ktp_kec_id" data-oldvalue="<?php echo $pegawai["pegawai_ktp_kel_id"];?>" data-placeholder="Pilih Kecamatan" class="select2-wizard" onchange="get_kelurahan(this,'pegawai_ktp_kel_id')">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Kelurahan</label>
									<div class="col-lg-9">
										<select name="pegawai_ktp_kel_id" id="pegawai_ktp_kel_id" data-placeholder="Pilih Kelurahan" class="select2-wizard">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label">Alamat</label>
									<div class="col-lg-9">
										<textarea name="pegawai_ktp_alamat" id="pegawai_ktp_alamat" rows="3" cols="3" class="form-control elastic ga_wajibzz" ><?php echo $pegawai["pegawai_ktp_alamat"];?></textarea>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Kode Pos</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_ktp_kodepos" id="pegawai_ktp_kodepos" value="<?php echo $pegawai["pegawai_ktp_kodepos"];?>" class="form-control input-xs ga_wajibzz kodepos">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<legend class="text-bold" style="padding-bottom:5px;border-bottom:solid 1px #00bcd4;">Data Tambahan</legend>
						<div class="col-md-6">
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">Photo</label>
								<div class="col-lg-9">
									<?php if($pegawai["pegawai_image_path"] != ''){ ?>
									<img src="<?php echo base_url().'assets/images/pegawai/photo/'.$pegawai["pegawai_image_path"];?>" height="150px;" id="image_pegawai">
									<?php }else{ ?>	
									<img src="<?php echo base_url().'assets/images/default_user.jpg';?>" height="150px;" id="image_pegawai">
									<?php } ?>
									<div class="clearfix"></div>
									<input type="file" name="pegawai_photo" id="pegawai_photo" class="form-control input-xs" style="padding-top: 0px !important;padding-bottom: 0px !important;" accept="image/*">
									<input type="hidden" name="pegawai_image_path" id="pegawai_image_path" value="<?php echo $pegawai["pegawai_image_path"]; ?>">
								</div>
							</div>

						</div>
						<div class="col-md-6">
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">Email Lain</label>
								<div class="col-lg-9">
									<input type="text" name="pegawai_email_lain" value="<?php echo $pegawai["pegawai_email_lain"];?>" id="pegawai_email_lain" class="form-control input-xs">
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">Email KKP</label>
								<div class="col-lg-9">
									<input type="text" name="pegawai_email" id="pegawai_email" value="<?php echo $pegawai["pegawai_email"];?>" class="form-control input-xs ga_wajibzz">
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">No. HP</label>
								<div class="col-lg-9">
									<input type="text" name="pegawai_handphone" id="pegawai_handphone" value="<?php echo $pegawai["pegawai_handphone"];?>" class="form-control input-xs ga_wajibzz">
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">No. KARPEG</label>
								<div class="col-lg-9">
									<input type="text" name="pegawai_nomor_karpeg" id="pegawai_nomor_karpeg" value="<?php echo $pegawai["pegawai_nomor_karpeg"];?>" class="form-control input-xs ga_wajibzz">
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">No. ASKES</label>
								<div class="col-lg-9">
									<input type="text" name="pegawai_nomor_askes" id="pegawai_nomor_askes" value="<?php echo $pegawai["pegawai_nomor_askes"];?>" class="form-control input-xs ga_wajibzz">
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">No. KARIS/KARSU</label>
								<div class="col-lg-9">
									<input type="text" name="pegawai_nomor_karis" id="pegawai_nomor_karis" value="<?php echo $pegawai["pegawai_nomor_karis"];?>" class="form-control input-xs ga_wajibzz">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				
			</fieldset>


			<!-- <h6>Pendidikan</h6>
			<fieldset>
				<div class="row">
					<div class="col-md-12">
						
						<div class="row">
							<div class="col-md-6">
								<legend class="text-bold" style="padding-bottom:5px;border-bottom:solid 1px #00bcd4;">Pendidikan</legend>
								
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Pendidikan Terakhir</label>
									<div class="col-md-9">
										<select name="pegawai_pendidikan_terakhir_id" id="pegawai_pendidikan_terakhir_id" data-placeholder="Pendidikan" data-oldvalue="<?php echo $pegawai["pegawai_pendidikan_tingkat_id"];?>" class="select2-wizard"  onchange="get_tingkat_pendidikan(this,'pegawai_pendidikan_tingkat_id')">
											<option value=""></option>
											<?php foreach ($pendidikan as $i) {
												echo '<option value="'.$i["pendidikan_id"].'" data-kode="'.$i["pendidikan_kode"].'" '.(($pegawai["pegawai_pendidikan_terakhir_id"]==$i["pendidikan_id"])?"selected":"").'>'.$i["pendidikan_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tingkat Pendidikan</label>
									<div class="col-md-9">
										<select name="pegawai_pendidikan_tingkat_id" id="pegawai_pendidikan_tingkat_id" data-oldvalue="<?php echo $pegawai["pegawai_pendidikan_fakultas_id"];?>" data-placeholder="Tingkat" class="select2-wizard" onchange="get_fakultas(this,'pegawai_pendidikan_fakultas_id')">
											<option value=""></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Fakultas</label>
									<div class="col-lg-9">
										<select name="pegawai_pendidikan_fakultas_id" id="pegawai_pendidikan_fakultas_id" data-oldvalue="<?php echo $pegawai["pegawai_pendidikan_jurusan_id"];?>" data-placeholder="Fakultas" class="select2-wizard" onchange="get_jurusan(this,'pegawai_pendidikan_jurusan_id')">
											<option value=""></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Jurusan</label>
									<div class="col-md-9">
										<select name="pegawai_pendidikan_jurusan_id" id="pegawai_pendidikan_jurusan_id" data-placeholder="Jurusan" class="select2-wizard">
											<option value=""></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Program Studi</label>
									<div class="col-md-9">
										<input type="text" name="pegawai_pendidikan_program_studi" id="pegawai_pendidikan_program_studi" value="<?php echo $pegawai["pegawai_pendidikan_program_studi"];?>" class="form-control input-xs">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Tahun Masuk</label>
									<div class="col-lg-3">
										<select name="pegawai_pendidikan_tahun_masuk" id="pegawai_pendidikan_tahun_masuk" data-placeholder="Tahun Masuk" class="select2-wizard" onchange="set_tahun_akhir(this,'pegawai_pendidikan_tahun_lulus')">
											<option value=""></option>
											<?
											$yearstart = date("Y");
											$yearend = $yearstart-68;
											for($i=$yearstart;$i>=$yearend;$i--) {
												echo '<option value="'.$i.'" '.(($i==$pegawai["pegawai_pendidikan_tahun_masuk"])?"selected":"").'>'.$i.'</option>';
											}
											?>
										</select>
									</div>
									<label class="col-lg-3 control-label">Tahun Lulus</label>
									<div class="col-lg-3">
										<select name="pegawai_pendidikan_tahun_lulus" id="pegawai_pendidikan_tahun_lulus" data-placeholder="Tahun Lulus" class="select2-wizard">
											<option></option>
											<?
											$yearstart = date("Y");
											$yearend = $yearstart-68;
											for($i=$yearstart;$i>=$yearend;$i--) {
												echo '<option value="'.$i.'" '.(($i==$pegawai["pegawai_pendidikan_tahun_lulus"])?"selected":"").'>'.$i.'</option>';
											}
											?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Nama Sekolah</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_pendidikan_nama" id="pegawai_pendidikan_nama" value="<?php echo $pegawai["pegawai_pendidikan_nama"];?>" class="form-control input-xs ga_wajibzz">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Tempat Belajar</label>
									<div class="col-lg-3">
										<select name="pegawai_pendidikan_tempat" id="pegawai_pendidikan_tempat" data-placeholder="Tempat Belajar" class="select2-wizard">
											<option></option>
											<option value="1" <?php echo (($pegawai["pegawai_pendidikan_tempat"]=="1")?"selected":"");?> >Dalam Negeri</option>
											<option value="2" <?php echo (($pegawai["pegawai_pendidikan_tempat"]=="2")?"selected":"");?> >Luar Negeri</option>
										</select>
									</div>
									<label class="col-lg-3 control-label">IPK</label>
									<div class="col-lg-3">
										<input type="text" name="pegawai_pendidikan_ipk" id="pegawai_pendidikan_ipk" value="<?php echo $pegawai["pegawai_pendidikan_ipk"];?>" class="form-control input-xs">

									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Nilai TOEFL</label>
									<div class="col-lg-3">
										<input type="text" name="pegawai_nilai_toefl" id="pegawai_nilai_toefl" value="<?php echo $pegawai["pegawai_nilai_toefl"];?>" class="form-control input-xs">

									</div>
									<label class="col-lg-3 control-label">Nilai IELT</label>
									<div class="col-lg-3">
										<input type="text" name="pegawai_nilai_ielt" id="pegawai_nilai_ielt" value="<?php echo $pegawai["pegawai_nilai_ielt"];?>" class="form-control input-xs">

									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Catatan</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_catatan" id="pegawai_catatan" value="<?php echo $pegawai["pegawai_catatan"];?>" class="form-control input-xs ga_wajibzz">
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<legend class="text-bold" style="padding-bottom:5px;border-bottom:solid 1px #00bcd4;">Diklat</legend>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Jenis Diklat</label>
									<div class="col-md-9">
										<select name="pegawai_diklat_jenis_id" id="pegawai_diklat_jenis_id" data-placeholder="Jenis Diklat" class="select2-wizard wajibzz" >
											<option value=""></option>
											<?php foreach ($diklat as $i) {
												echo '<option value="'.$i["jenis_diklat_id"].'" '.(($pegawai["pegawai_diklat_jenis_id"]==$i["jenis_diklat_id"])?"selected":"").'>'.$i["jenis_diklat_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Angkatan</label>
									<div class="col-md-9">
										<input type="text" name="pegawai_diklat_angkatan" id="pegawai_diklat_angkatan" value="<?php echo $pegawai["pegawai_diklat_angkatan"];?>" class="form-control input-xs wajibzz">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Nama Diklat</label>
									<div class="col-md-9">
										<input type="text" name="pegawai_diklat_nama" id="pegawai_diklat_nama" value="<?php echo $pegawai["pegawai_diklat_nama"];?>" class="form-control input-xs wajibzz">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Penyelenggara</label>
									<div class="col-md-9">
										<input type="text" name="pegawai_diklat_penyelenggara" id="pegawai_diklat_penyelenggara" value="<?php echo $pegawai["pegawai_diklat_penyelenggara"];?>" class="form-control input-xs wajibzz">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Durasi</label>
									<div class="col-md-9">
										<input class="form-control input-xs wajibzz" type="text" name="pegawai_diklat_durasi" id="pegawai_diklat_durasi" value="<?php echo $pegawai["pegawai_diklat_durasi"];?>">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Tanggal Mulai</label>
									<div class="col-lg-3">
										<input type="text" name="pegawai_diklat_tanggal_mulai" id="pegawai_diklat_tanggal_mulai" class="form-control input-xs wajibzz pickttlstart" value="<?php echo dateEnToId($pegawai["pegawai_diklat_tanggal_mulai"],'d F Y');?>">
									</div>
									<label class="col-md-3 control-label">Tanggal Selesai</label>
									<div class="col-md-3">
										<input type="text" name="pegawai_diklat_tanggal_selesai" id="pegawai_diklat_tanggal_selesai" value="<?php echo dateEnToId($pegawai["pegawai_diklat_tanggal_selesai"],'d F Y');?>" class="form-control input-xs wajibzz pickttlend">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Predikat</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_diklat_predikat" id="pegawai_diklat_predikat" value="<?php echo $pegawai["pegawai_diklat_predikat"];?>" class="form-control input-xs wajibzz">
									</div>
								</div>								
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Lokasi</label>
									<div class="col-md-9">
										<input type="text" name="pegawai_diklat_lokasi" id="pegawai_diklat_lokasi" value="<?php echo $pegawai["pegawai_diklat_lokasi"];?>" class="form-control input-xs wajibzz">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Nomor Sertifikat</label>
									<div class="col-md-9">
										<input type="text" name="pegawai_diklat_nomor_sertifikat" id="pegawai_diklat_nomor_sertifikat" value="<?php echo $pegawai["pegawai_diklat_nomor_sertifikat"];?>"  class="form-control input-xs wajibzz">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tanggal Sertifikat</label>
									<div class="col-md-9">
										<input type="text" name="pegawai_diklat_tanggal_sertifikat" id="pegawai_diklat_tanggal_sertifikat"  value="<?php echo dateEnToId($pegawai["pegawai_diklat_tanggal_sertifikat"],'d F Y');?>" class="form-control input-xs pickttl">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">LEMHANAS</label>
									<div class="col-lg-9">
										<select name="pegawai_diklat_lemhanas" id="pegawai_diklat_lemhanas" data-placeholder="Pilih" class="select2-wizard">
											<option></option>
											<option value="1" <?php echo (($pegawai["pegawai_diklat_lemhanas"]=="1")?"selected":"");?>>Sudah</option>
											<option value="0" <?php echo (($pegawai["pegawai_diklat_lemhanas"]=="0")?"selected":"");?>>Belum</option>
										</select>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</fieldset> -->

			<h6>Jabatan</h6>
			<fieldset>
				<div class="row">
					<div class="col-md-12">
						<legend class="text-bold" style="padding-bottom:5px;border-bottom:solid 1px #00bcd4;">Jabatan</legend>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Unit Kerja</label>
									<div class="col-lg-9">
										<select name="pegawai_unit_kerja_id[]" id="pegawai_unit_kerja_id_1" data-placeholder="Unit Kerja"  data-id="0" class="select2-wizard ga_wajibzz"  onchange="get_unit_kerja_hirarki(this,1)" disabled="true">
											<option></option>
											<?php foreach ($unit_kerja as $i) {
												echo '<option value="'.$i["unit_kerja_kode"].'" parent_id_kode="'.$i["unit_kerja_id_kode"].'" jabatan_id="'.$i["jabatan_id"].'">'.$i["unit_kerja_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs pegawai_unit_kerja_id_2" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="pegawai_unit_kerja_id[]" id="pegawai_unit_kerja_id_2" data-placeholder="Unit Kerja"  data-id="0" class="select2-wizard"  onchange="get_unit_kerja_hirarki(this,2)" disabled="true">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs pegawai_unit_kerja_id_3" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="pegawai_unit_kerja_id[]" id="pegawai_unit_kerja_id_3" data-placeholder="Unit Kerja"  data-id="0" class="select2-wizard"  onchange="get_unit_kerja_hirarki(this,3)" disabled="true">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs pegawai_unit_kerja_id_4" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="pegawai_unit_kerja_id[]" id="pegawai_unit_kerja_id_4" data-placeholder="Unit Kerja"  data-id="0" class="select2-wizard"  onchange="get_unit_kerja_hirarki(this,4)" disabled="true">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs pegawai_unit_kerja_id_5" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="pegawai_unit_kerja_id[]" id="pegawai_unit_kerja_id_5" data-placeholder="Unit Kerja"  data-id="0" class="select2-wizard" onchange="get_unit_kerja_hirarki(this,5)" disabled="true">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Status Pegawai</label>
									<div class="col-lg-9">
										<select name="pegawai_status_pegawai_id" id="pegawai_status_pegawai_id" data-placeholder="Pilih Status Pegawai" class="select2-wizard ga_wajibzz">
											<option></option>
											<?php foreach ($status_pegawai as $i) {
												echo '<option value="'.$i["status_pegawai_kode"].'" '.(($pegawai["pegawai_status_pegawai_id"]==$i["status_pegawai_kode"])?"selected":"").'>'.$i["status_pegawai_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Instansi Asal</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_instansi_asal" id="pegawai_instansi_asal" value="<?php echo $pegawai["pegawai_instansi_asal"];?>" class="form-control input-xs ga_wajibzz">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Formasi Saat CPNS</label>
									<div class="col-lg-9">
										<select name="pegawai_cpns_fungsional_tipe" id="pegawai_cpns_fungsional_tipe" data-placeholder="Pilih Formasi Saat CPNS" class="select2-wizard ga_wajibzz" onchange="getFungsionalTertentuByJabatanCPNS(this)">
											<option></option>
											<option value="1" <?php echo (($pegawai["pegawai_cpns_fungsional_tipe"]=="1")?"selected":"");?>>Fungsional Umum</option>
											<option value="2" <?php echo (($pegawai["pegawai_cpns_fungsional_tipe"]=="2")?"selected":"");?>>Fungsional Tertentu</option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs fungsional_tertentu_cpns" style="display: none;">
									<div class="col-lg-9 col-lg-offset-3">
										<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Formasi Fungsional Tertentu CPNS</span>
										<div class="form-group form-group-xs">
											<label class="col-lg-3 control-label">Nama</label>
											<div class="col-lg-9">
												<select name="pegawai_cpns_formasi_id" id="pegawai_cpns_formasi_id" data-placeholder="Pilih Nama Formasi" class="select2-wizard ga_wajibzz">
													<option></option>
													<?php foreach ($fungsional_tertentu as $i) {
														echo '<option value="'.$i["fungsional_id"].'" '.(($i["fungsional_id"]==$pegawai["pegawai_cpns_formasi_id"])?"selected":"").'>'.$i["fungsional_kode"].' - '.$i["fungsional_nama"].'</option>';
													}?>
												</select>
												<input type="hidden" id="pegawai_cpns_nama_formasi" name="pegawai_cpns_nama_formasi" value="<?php echo $pegawai["pegawai_cpns_nama_formasi"];?>">
											</div>
										</div>
										<div class="form-group form-group-xs">
											<label class="col-lg-3 control-label">Status</label>
											<div class="col-lg-9">
												<select name="pegawai_cpns_status_formasi" id="pegawai_cpns_status_formasi" data-placeholder="Pilih Status Formasi" class="select2-wizard ga_wajibzz">
													<option></option>
													<?php foreach ($fungsional_tertentu_formasi as $i) {
														echo '<option value="'.$i["formasi_fung_cpns_id"].'" '.(($i["formasi_fung_cpns_id"]==$pegawai["pegawai_cpns_status_formasi"])?"selected":"").'>'.$i["formasi_fung_cpns_nama"].'</option>';
													}?>
												</select>
											</div>
										</div>
										<div class="form-group form-group-xs">
											<label class="col-lg-3 control-label">Catatan</label>
											<div class="col-lg-9">
												<input type="text" name="pegawai_cpns_fungsional_catatan" id="pegawai_cpns_fungsional_catatan" class="form-control input-xs ga_wajibzz" value="<?php echo $pegawai["pegawai_cpns_fungsional_catatan"];?>" placeholder="Catatan Formasi">
											</div>
										</div>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Golongan CPNS</label>
									<div class="col-lg-3">
										<select name="pegawai_cpns_golongan_id" id="pegawai_cpns_golongan_id" data-placeholder="Golongan" class="select2-wizard ga_wajibzz">
											<option></option>
											<?php foreach ($golongan as $i) {
												echo '<option value="'.$i["golongan_id"].'" '.(($i["golongan_id"]==$pegawai["pegawai_cpns_golongan_id"])?"selected":"").'>'.$i["golongan_kode"].'</option>';
											}?>
										</select>
									</div>
									<label class="col-lg-3 control-label">TMT CPNS</label>
									<div class="col-lg-3">
										<input type="text" name="pegawai_cpns_tanggal_tmt" id="pegawai_cpns_tanggal_tmt" value="<?php echo dateEnToId($pegawai["pegawai_cpns_tanggal_tmt"],'d-m-Y');?>" class="form-control input-xs ga_wajibzz pickttl">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Golongan AKHIR</label>
									<div class="col-lg-3">
										<select name="pegawai_golongan_id" id="pegawai_golongan_id" data-placeholder="Golongan" class="select2-wizard ga_wajibzz" disabled="true">
											<option></option>
											<?php foreach ($golongan as $i) {
												echo '<option value="'.$i["golongan_id"].'" '.(($i["golongan_id"]==$pegawai["pegawai_golongan_id"])?"selected":"").'>'.$i["golongan_kode"].'</option>';
											}?>
										</select>
									</div>
									<label class="col-lg-3 control-label">TMT GOL AKHIR</label>
									<div class="col-lg-3">
										<input type="text" name="pegawai_tanggal_tmt" id="pegawai_tanggal_tmt"  value="<?php echo dateEnToId($pegawai["pegawai_tanggal_tmt"],'d-m-Y');?>" class="form-control input-xs ga_wajibzz pickttl" disabled="true">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Masa Kerja Golongan</label>
									<div class="col-lg-9">
										<div class="row">
											<div class="form-group form-group-xs">
												<label class="col-lg-3 control-label">Tahun</label>
												<div class="col-lg-3">
													<input type="text" name="pegawai_tahun_masa_kerja_golongan" id="pegawai_tahun_masa_kerja_golongan" value="<?php echo $pegawai["pegawai_tahun_masa_kerja_golongan"];?>" class="form-control input-xs ga_wajibzz" disabled="true">

												</div>
												<label class="col-lg-3 control-label">Bulan</label>
												<div class="col-lg-3">
													<input type="text" name="pegawai_bulan_masa_kerja_golongan" id="pegawai_bulan_masa_kerja_golongan" value="<?php echo $pegawai["pegawai_bulan_masa_kerja_golongan"];?>" class="form-control input-xs ga_wajibzz" disabled="true">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Masa Kerja Berkala</label>
									<div class="col-lg-9">
										<div class="row">
											<div class="form-group form-group-xs">
												<label class="col-lg-3 control-label">Tahun</label>
												<div class="col-lg-3">
													<input type="text" name="pegawai_tahun_masa_kerja_berkala" id="pegawai_tahun_masa_kerja_berkala" value="<?php echo $pegawai["pegawai_tahun_masa_kerja_berkala"];?>" class="form-control input-xs ga_wajibzz">

												</div>
												<label class="col-lg-3 control-label">Bulan</label>
												<div class="col-lg-3">
													<input type="text" name="pegawai_bulan_masa_kerja_berkala" id="pegawai_bulan_masa_kerja_berkala"  value="<?php echo $pegawai["pegawai_bulan_masa_kerja_berkala"];?>"  class="form-control input-xs ga_wajibzz">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">TMT Berkala</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_tanggal_tmt_berkala" id="pegawai_tanggal_tmt_berkala" value="<?php echo $pegawai["pegawai_tanggal_tmt_berkala"];?>" class="form-control input-xs ga_wajibzz pickttl">
									</div>
								</div>
								
							</div>
							<div class="col-md-6">
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Jabatan / Eselon</label>
									<div class="col-lg-9">
										<select name="pegawai_jabatan_id" id="pegawai_jabatan_id" data-placeholder="Pilih Jabatan / Eselon" class="select2-wizard ga_wajibzz" onchange="getFungsionalTertentuByJabatan(this)" disabled="true">
											<option></option>
											<?php foreach ($jabatan as $i) {
												echo '<option value="'.$i["jabatan_id"].'" kode="'.$i["jabatan_kode"].'" '.(($i["jabatan_id"]==$pegawai["pegawai_jabatan_id"])?"selected":"").'>'.$i["jabatan_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs fungsional_tertentu_pegawai" style="display: none;">
									<div class="col-lg-12">
										<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Formasi Fungsional Tertentu</span>
										<div class="form-group form-group-xs">
											<label class="col-lg-3 control-label">Nama</label>
											<div class="col-lg-9">
												<select name="pegawai_jabatan_fungsional_id" id="pegawai_jabatan_fungsional_id" data-placeholder="Pilih Nama Formasi" class="select2-wizard ga_wajibzz">
													<option></option>
													<?php foreach ($fungsional_tertentu as $i) {
														echo '<option value="'.$i["fungsional_id"].'" '.(($i["fungsional_id"]==$pegawai["pegawai_jabatan_fungsional_id"])?"selected":"").'>'.$i["fungsional_kode"].' - '.$i["fungsional_nama"].'</option>';
													}?>
												</select>
											</div>
										</div>
										<div class="form-group form-group-xs">
											<label class="col-lg-3 control-label">TMT Fungsional</label>
											<div class="col-lg-9">
												<input type="text" name="pegawai_tanggal_tmt_jabfung" id="pegawai_tanggal_tmt_jabfung" value="<?php echo $pegawai["pegawai_tanggal_tmt_jabfung"];?>"  class="form-control input-xs ga_wajibzz pickttl">
											</div>
										</div>
										<div class="form-group form-group-xs">
											<label class="col-lg-3 control-label">Angka Kredit Komulatif</label>
											<div class="col-lg-9">
												<input type="text" name="pegawai_fungsional_kredit" id="pegawai_fungsional_kredit" value="<?php echo $pegawai["pegawai_fungsional_kredit"];?>" class="form-control input-xs ga_wajibzz" >
											</div>
										</div>
										<div class="form-group form-group-xs">
											<label class="col-lg-3 control-label">Bidang Keahlian 1</label>
											<div class="col-lg-9">
												<input type="text" name="pegawai_jab_fung_bidang_1" id="pegawai_jab_fung_bidang_1" value="<?php echo $pegawai["pegawai_jab_fung_bidang_1"];?>" class="form-control input-xs ga_wajibzz" >
											</div>
										</div>
										<div class="form-group form-group-xs">
											<label class="col-lg-3 control-label">Bidang Keahlian 2</label>
											<div class="col-lg-9">
												<input type="text" name="pegawai_jab_fung_bidang_2" id="pegawai_jab_fung_bidang_2" value="<?php echo $pegawai["pegawai_jab_fung_bidang_2"];?>" class="form-control input-xs ga_wajibzz" >
											</div>
										</div>
										<div class="form-group form-group-xs">
											<label class="col-lg-3 control-label">TMT Pembebasan</label>
											<div class="col-lg-3">
												<input type="text" name="pegawai_tanggal_pembebasan_tmt_jab_fung" id="pegawai_tanggal_pembebasan_tmt_jab_fung" class="form-control input-xs ga_wajibzz pickttl"  value="<?php echo $pegawai["pegawai_tanggal_pembebasan_tmt_jab_fung"];?>" >
											</div>
											<label class="col-lg-3 control-label">TMT Pengaktifan</label>
											<div class="col-lg-3">
												<input type="text" name="pegawai_tanggal_aktif_tmt_jab_fung" id="pegawai_tanggal_aktif_tmt_jab_fung"  value="<?php echo $pegawai["pegawai_tanggal_aktif_tmt_jab_fung"];?>" class="form-control input-xs ga_wajibzz pickttl">
											</div>
										</div>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Nama Jabatan</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_nama_jabatan" id="pegawai_nama_jabatan" value="<?php echo $pegawai["pegawai_nama_jabatan"];?>" class="form-control input-xs ga_wajibzz" disabled="true">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">TMT Jabatan</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_tanggal_tmt_jabatan" id="pegawai_tanggal_tmt_jabatan" value="<?php echo dateEnToId($pegawai["pegawai_tanggal_tmt_jabatan"],'d-m-Y');?>" class="form-control input-xs ga_wajibzz pickttl" disabled="true">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Pra Jabatan</label>
									<div class="col-lg-3">
										<select name="pegawai_telah_pra_jabatan" id="pegawai_telah_pra_jabatan" data-placeholder="Pra Jabatan" class="select2-wizard ga_wajibzz">
											<option></option>
											<option value="1" <?php echo (($pegawai["pegawai_telah_pra_jabatan"]=="1")?"selected":"");?>>Sudah</option>
											<option value="0" <?php echo (($pegawai["pegawai_telah_pra_jabatan"]=="0")?"selected":"");?>>Belum</option>
										</select>
									</div>
									<label class="col-lg-3 control-label">Tahun</label>
									<div class="col-lg-3">
										<select name="pegawai_tahun_pra_jabatan" id="pegawai_tahun_pra_jabatan" data-placeholder="Pilih Tahun" class="select2-wizard">
											<option></option>
											<?php
											$yearstart = date("Y");
											$yearend = $yearstart-68;
											for($i=$yearstart;$i>=$yearend;$i--) {
												echo '<option value="'.$i.'" '.(($i==$pegawai["pegawai_tahun_pra_jabatan"])?"selected":"").'>'.$i.'</option>';
											}
											?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Sumpah Jabatan</label>
									<div class="col-lg-3">
										<select name="pegawai_telah_sumpah_jabatan" id="pegawai_telah_sumpah_jabatan" data-placeholder="Sumpah Jabatan" class="select2-wizard ga_wajibzz">
											<option></option>
											<option value="1" <?php echo (($pegawai["pegawai_telah_sumpah_jabatan"]=="1")?"selected":"");?>>Sudah</option>
											<option value="0" <?php echo (($pegawai["pegawai_telah_sumpah_jabatan"]=="0")?"selected":"");?>>Belum</option>
										</select>
									</div>
									<label class="col-lg-3 control-label">Tahun</label>
									<div class="col-lg-3">
										<select name="pegawai_tahun_sumpah_jabatan" id="pegawai_tahun_sumpah_jabatan" data-placeholder="Pilih Tahun" class="select2-wizard">
											<option></option>
											<?php
											$yearstart = date("Y");
											$yearend = $yearstart-68;
											for($i=$yearstart;$i>=$yearend;$i--) {
												echo '<option value="'.$i.'" '.(($i==$pegawai["pegawai_tahun_sumpah_jabatan"])?"selected":"").'>'.$i.'</option>';
											}
											?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Test Kesehatan</label>
									<div class="col-lg-3">
										<select name="pegawai_telah_test_kesehatan" id="pegawai_telah_test_kesehatan" data-placeholder="Test Kesehatan" class="select2-wizard ga_wajibzz">
											<option></option>
											<option value="1" <?php echo (($pegawai["pegawai_telah_test_kesehatan"]=="1")?"selected":"");?>>Sudah</option>
											<option value="0" <?php echo (($pegawai["pegawai_telah_test_kesehatan"]=="0")?"selected":"");?>>Belum</option>
										</select>
									</div>
									<label class="col-lg-3 control-label">Tahun</label>
									<div class="col-lg-3">
										<select name="pegawai_tahun_test_kesehatan" id="pegawai_tahun_test_kesehatan" data-placeholder="Pilih Tahun" class="select2-wizard">
											<option></option>
											<?php
											$yearstart = date("Y");
											$yearend = $yearstart-68;
											for($i=$yearstart;$i>=$yearend;$i--) {
												echo '<option value="'.$i.'" '.(($i==$pegawai["pegawai_tahun_test_kesehatan"])?"selected":"").'>'.$i.'</option>';
											}
											?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Nomor Pendidikan</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_nomor_pendidikan" id="pegawai_nomor_pendidikan" value="<?php echo $pegawai["pegawai_nomor_pendidikan"];?>" class="form-control input-xs ga_wajibzz">
										<input type="hidden" id="pegawai_id" name="pegawai_id" value="<?php echo $pegawai["pegawai_id"];?>">
										<input type="hidden" id="unit_kerja_id" value="<?php echo $pegawai["unit_kerja_kode"];?>">
										<input type="hidden" id="unit_kerja_id_2" value="<?php echo $pegawai["unit_kerja_kode_2"];?>">
										<input type="hidden" id="unit_kerja_id_3" value="<?php echo $pegawai["unit_kerja_kode_3"];?>">
										<input type="hidden" id="unit_kerja_id_4" value="<?php echo $pegawai["unit_kerja_kode_4"];?>">
										<input type="hidden" id="unit_kerja_id_5" value="<?php echo $pegawai["unit_kerja_kode_5"];?>">
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>
<script type="text/javascript">
	function set_tahun_akhir(ini,nextopsi){
		var tahunmulai = parseInt($(ini).val());
		var d = new Date();
		var tahunakhir = d.getFullYear();
		var newoption = "<option value=''></option>";
		for(var i=tahunakhir; i >=tahunmulai; i--) {
			newoption += '<option value="'+i+'">'+i+'</option>';
		}
		$('#'+nextopsi).html(newoption);
	}
	function getFungsionalTertentuByJabatan(ini){
		var kode  = $('option:selected', ini).attr('kode');
		if(kode=="88"){
			$('.fungsional_tertentu_pegawai').slideDown('fast');
		}else{
			$('.fungsional_tertentu_pegawai').slideUp('fast');
		}
	}
	function getFungsionalTertentuByJabatanCPNS(ini){
		var vals  = $(ini).val();
		if(vals=="2"){
			$('.fungsional_tertentu_cpns').slideDown('fast');
		}else{
			$('.fungsional_tertentu_cpns').slideUp('fast');
		}
	}
	function  get_unit_kerja_hirarki(ini,lvl) {
		var attrid = $(ini).attr('data-id');
		if(lvl==1){
			$('.pegawai_unit_kerja_id_5').slideUp('fast',function(){
				$('#pegawai_unit_kerja_id_5').select2('val', '');
				$('.pegawai_unit_kerja_id_4').slideUp('fast',function(){
					$('#pegawai_unit_kerja_id_4').select2('val', '');
					$('.pegawai_unit_kerja_id_3').slideUp('fast',function(){
						$('#pegawai_unit_kerja_id_3').select2('val', '');
						$('.pegawai_unit_kerja_id_2').slideUp('fast',function(){
							$('#pegawai_unit_kerja_id_2').select2('val', '');
						});
					});
				});
			});
		}else if(lvl==2){
			$('.pegawai_unit_kerja_id_5').slideUp('fast',function(){
				$('#pegawai_unit_kerja_id_5').select2('val', '');
				$('.pegawai_unit_kerja_id_4').slideUp('fast',function(){
					$('#pegawai_unit_kerja_id_4').select2('val', '');
					$('.pegawai_unit_kerja_id_3').slideUp('fast',function(){
						$('#pegawai_unit_kerja_id_3').select2('val', '');
					});
				});
			});
		}else if(lvl==3){
			$('.pegawai_unit_kerja_id_5').slideUp('fast',function(){
				$('#pegawai_unit_kerja_id_5').select2('val', '');
				$('.pegawai_unit_kerja_id_4').slideUp('fast',function(){
					$('#pegawai_unit_kerja_id_4').select2('val', '');
				});
			});
		}else if(lvl==4){
			$('.pegawai_unit_kerja_id_5').slideUp('fast',function(){
				$('#pegawai_unit_kerja_id_5').select2('val', '');
			});
		}
		var parent_id_kode  = $('option:selected', ini).attr('parent_id_kode');
		var jabatan_id  = $('option:selected', ini).attr('jabatan_id');

		// $('#pegawai_jabatan_id').select2('val', '');
		// $('#pegawai_jabatan_id').val(jabatan_id);
		// $('#pegawai_jabatan_id').trigger('change');
		$.ajax({
			url: base_url+'kepegawaian/pns/get_unit_kerja_hirarki',
			type: "post",
			dataType: 'json',
			data: { unit_kerja_parent_id_kode: parent_id_kode, unit_kerja_level: (lvl+2)},
			success: function (data) {

				var unit_kerja = data.unit_kerja;
				var newoption = "<option></option>";

				for(var i = 0; i < unit_kerja.length; i ++){
					newoption+='<option value="'+unit_kerja[i].unit_kerja_kode+'" parent_id_kode="'+unit_kerja[i].unit_kerja_id_kode+'"  jabatan_id="'+unit_kerja[i].jabatan_id+'" '+((unit_kerja[i].unit_kerja_kode == attrid)?"selected":"")+'>'+unit_kerja[i].unit_kerja_nama+'</option>';
				}
				$('#pegawai_unit_kerja_id_'+(lvl+1)).html(newoption);
				if(unit_kerja.length>0){
					$('.pegawai_unit_kerja_id_'+(lvl+1)).slideDown('fast');
				}else{
					$('.pegawai_unit_kerja_id_'+(lvl+1)).slideUp('fast');

				}
				if(attrid != 0){
					$('#pegawai_unit_kerja_id_'+(lvl+1)).trigger('change');
				}
			}
		});
		
	}
	function get_provinsi(ini){
		var IsDalamNegeri = $('option:selected', ini).attr('data-negara-type');
		if(IsDalamNegeri!=1){
			$('.dalam_negeri').slideUp(function(){
				$('#pegawai_domisili_provinsi_id').select2('val', ' ');
				$('#pegawai_domisili_provinsi_id').trigger('change');
				$('#pegawai_domisili_kota_id').html('<option></option>');
				$('#pegawai_domisili_kec_id').html('<option></option>');
				$('#pegawai_domisili_kel_id').html('<option></option>');
			});
		}else{
			$('.dalam_negeri').slideDown(function(){
			});
		}
		
	}
	function  get_kota(ini,nextopsi) {
		var provinsi_kode = $(ini).val();
		var oldvalue = $(ini).attr('data-oldvalue');
		$.ajax({
			url: base_url+'kepegawaian/pns/get_kota',
			type: "post",
			dataType: 'json',
			data: { provinsi_kode: provinsi_kode},
			success: function (data) {
				var kota = data.kota;
				var newoption = "<option></option>";
				for(var i = 0; i < kota.length; i ++){
					newoption+='<option value="'+kota[i].kota_kode+'"  '+((oldvalue==kota[i].kota_kode)?"selected":"")+'>'+kota[i].kota_nama+'</option>';
				}
				$('#'+nextopsi).html(newoption);
				$('#'+nextopsi).change();
			}
		});
	}
	function  get_kecamatan(ini,nextopsi) {
		var kota_kode = $(ini).val();
		var oldvalue = $(ini).attr('data-oldvalue');
		$.ajax({
			url: base_url+'kepegawaian/pns/get_kecamatan',
			type: "post",
			dataType: 'json',
			data: { kota_kode: kota_kode},
			success: function (data) {
				var kecamatan = data.kecamatan;
				var newoption = "<option></option>";
				for(var i = 0; i < kecamatan.length; i ++){
					newoption+='<option value="'+kecamatan[i].kecamatan_kode+'" '+((oldvalue==kecamatan[i].kecamatan_kode)?"selected":"")+'>'+kecamatan[i].kecamatan_nama+'</option>';
				}
				$('#'+nextopsi).html(newoption);
				$('#'+nextopsi).change();
			}
		});
	}
	function  get_kelurahan(ini,nextopsi) {
		var kecamatan_kode = $(ini).val();
		var oldvalue = $(ini).attr('data-oldvalue');
		$.ajax({
			url: base_url+'kepegawaian/pns/get_kelurahan',
			type: "post",
			dataType: 'json',
			data: { kecamatan_kode: kecamatan_kode},
			success: function (data) {
				var kelurahan = data.kelurahan;
				var newoption = "<option></option>";
				for(var i = 0; i < kelurahan.length; i ++){
					newoption+='<option value="'+kelurahan[i].kelurahan_kode+'" '+((oldvalue==kelurahan[i].kelurahan_kode)?"selected":"")+'>'+kelurahan[i].kelurahan_nama+'</option>';
				}
				$('#'+nextopsi).html(newoption);
			}
		});
	}
	
	function  get_tingkat_pendidikan(ini,nextopsi) {
		var pendidikan_kode = $('option:selected', ini).attr('data-kode');
		var oldvalue = $(ini).attr('data-oldvalue');
		$.ajax({
			url: base_url+'kepegawaian/pendidikan_formal/get_tingkat_pendidikan',
			type: "post",
			dataType: 'json',
			data: { pendidikan_kode: pendidikan_kode},
			success: function (data) {
				var isUpdate = parseInt($('#isUpdate').val());
				var pendidikan_formal_jurusan_code_tingkat_temp = $('#pendidikan_formal_jurusan_code_tingkat_temp').val();
				var tingkat_pendidikan = data.tingkat_pendidikan;
				var newoption = "<option value=''></option>";
				for(var i = 0; i < tingkat_pendidikan.length; i ++){
					newoption+='<option value="'+tingkat_pendidikan[i].jurusan_kode+'" '+((tingkat_pendidikan.length==1 || (isUpdate==1 && pendidikan_formal_jurusan_code_tingkat_temp == tingkat_pendidikan[i].jurusan_kode) || oldvalue == tingkat_pendidikan[i].jurusan_kode) ?"selected":"")+'>'+tingkat_pendidikan[i].jurusan_grup+'</option>';
				}
				$('#'+nextopsi).html(newoption);

				get_fakultas($('#pegawai_pendidikan_tingkat_id'),'pegawai_pendidikan_fakultas_id');
			}
		});
	}
	function  get_fakultas(ini,nextopsi) {
		var jurusan_kode = $(ini).val();
		var oldvalue = $(ini).attr('data-oldvalue');
		$.ajax({
			url: base_url+'kepegawaian/pendidikan_formal/get_fakultas',
			type: "post",
			dataType: 'json',
			data: { jurusan_kode: jurusan_kode},
			success: function (data) {
				var isUpdate = parseInt($('#isUpdate').val());
				var pendidikan_formal_jurusan_code_fakultas_temp = $('#pendidikan_formal_jurusan_code_fakultas_temp').val();

				var jurusan = data.jurusan;
				var newoption = "<option value=''></option>";

				for(var i = 0; i < jurusan.length; i ++){
					newoption+='<option value="'+jurusan[i].jurusan_kode+'" '+(((isUpdate==1 && pendidikan_formal_jurusan_code_fakultas_temp == jurusan[i].jurusan_kode) || oldvalue == jurusan[i].jurusan_kode)?"selected":"")+'>'+jurusan[i].jurusan_nama+'</option>';
				}
				$('#'+nextopsi).html(newoption);
				get_jurusan($('#pegawai_pendidikan_fakultas_id'),'pegawai_pendidikan_jurusan_id');
			}
		});
	}
	function  get_jurusan(ini,nextopsi) {
		var jurusan_kode = $(ini).val();
		var oldvalue = $(ini).attr('data-oldvalue');
		$.ajax({
			url: base_url+'kepegawaian/pendidikan_formal/get_jurusan',
			type: "post",
			dataType: 'json',
			data: { jurusan_kode: jurusan_kode},
			success: function (data) {
				var isUpdate = parseInt($('#isUpdate').val());
				var pendidikan_formal_jurusan_code_temp = $('#pendidikan_formal_jurusan_code_temp').val();

				var jurusan = data.jurusan;
				var newoption = "<option value=''></option>";

				for(var i = 0; i < jurusan.length; i ++){
					if(jurusan[i].jurusan_grup != jurusan[i].jurusan_nama){
						newoption+='<option value="'+jurusan[i].jurusan_kode+'" '+(((isUpdate==1 && pendidikan_formal_jurusan_code_temp == jurusan[i].jurusan_kode) || oldvalue == jurusan[i].jurusan_kode)?"selected":"")+'>'+jurusan[i].jurusan_nama+'</option>';
					}
				}
				$('#'+nextopsi).html(newoption);
			}
		});
	}
	

	function readURL(input) {

		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
				$('#image_pegawai').attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}


	$(document).ready(function() {
		var form = $('form.need_validation').show();
		$(".steps-validation").steps({
			headerTag: "h6",
			bodyTag: "fieldset",
			saveState: true,
			titleTemplate: '<span class="number">#index#</span> #title#',
			autoFocus: true,
			onContentLoaded: function (event, currentIndex) {
				$('.select2-wizard').select2({
					containerCssClass: 'select-xs',
				});
			},
			onStepChanging: function (event, currentIndex, newIndex) {
				if (currentIndex > newIndex) {
					return true;
				} 
				if (currentIndex < newIndex) {
					form.find(".body:eq(" + newIndex + ") label.error").remove();
					form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
				}

				form.validate().settings.ignore = ":disabled,:hidden";
				return form.valid();
			},
			onFinished: function (event, currentIndex) {
				$('form.steps-validation').submit();
			},
			labels: {
				cancel: "Batal",
				current: "Tahapan :",
				pagination: "Pagination",
				finish: "Simpan dan Selesai",
				next: "Selanjutnya",
				previous: "Sebelumnya",
				loading: "Loading ..."
			}
		});
		$('.select2-wizard').select2({
			containerCssClass: 'select-xs',
		});
		$("#pegawai_photo").change(function() {
			readURL(this);
		});

		$('[name="pegawai_nip"]').formatter({
			pattern: '{{99999999}}  {{999999}}  {{9}}  {{999}}'
		});
		$('.kodepos').formatter({
			pattern: '{{99999}}'
		});
		$('.ktp').formatter({
			pattern: '{{9999999999999999}}'
		});
		$('.npwp').formatter({
			pattern: '{{99}}  {{999}}  {{999}}  {{9999}} {{999}}'
		});
		$("#pegawai_ktp_negara_id").prop("disabled", true);
		var d = new Date();

		$('.pickttl').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
		$('.pickttlstart').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});

		$('.pickttlend').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
		
		$('#pegawai_nip,#pegawai_nomor_ktp,#pegawai_nomor_npwp').change().keyup().keydown();
		$('#pegawai_domisili_provinsi_id').change();
		$('#pegawai_ktp_provinsi_id').change();
		$('#pegawai_pendidikan_terakhir_id').change();
		$('#pegawai_cpns_fungsional_tipe').change();
		$('#pegawai_jabatan_id').change();
		
		var unit_kerja_id_5 = $('#unit_kerja_id_5').val();
		var unit_kerja_id_4 = $('#unit_kerja_id_4').val();
		var unit_kerja_id_3 = $('#unit_kerja_id_3').val();
		var unit_kerja_id_2 = $('#unit_kerja_id_2').val();
		var unit_kerja_id = $('#unit_kerja_id').val();
		if(unit_kerja_id_5 != null && unit_kerja_id_5 != 0){
			$('#pegawai_unit_kerja_id_1').attr('data-id', unit_kerja_id_4);
			$('#pegawai_unit_kerja_id_2').attr('data-id', unit_kerja_id_3);
			$('#pegawai_unit_kerja_id_3').attr('data-id', unit_kerja_id_2);
			$('#pegawai_unit_kerja_id_4').attr('data-id', unit_kerja_id);

			$("#pegawai_unit_kerja_id_1").val(unit_kerja_id_5);
			$('#pegawai_unit_kerja_id_1').trigger('change');						
		}

		else{
			if(unit_kerja_id_4 != null && unit_kerja_id_4 != 0){
				$('#pegawai_unit_kerja_id_1').attr('data-id', unit_kerja_id_3);
				$('#pegawai_unit_kerja_id_2').attr('data-id', unit_kerja_id_2);
				$('#pegawai_unit_kerja_id_3').attr('data-id', unit_kerja_id);

				$("#pegawai_unit_kerja_id_1").val(unit_kerja_id_4);
				$('#pegawai_unit_kerja_id_1').trigger('change');

			}
			else{
				if(unit_kerja_id_3 != null && unit_kerja_id_3 != 0){
					$('#pegawai_unit_kerja_id_1').attr('data-id', unit_kerja_id_2);
					$('#pegawai_unit_kerja_id_2').attr('data-id', unit_kerja_id);

					$("#pegawai_unit_kerja_id_1").val(unit_kerja_id_3);
					$('#pegawai_unit_kerja_id_1').trigger('change');

				}
				else{
					if(unit_kerja_id_2 != null && unit_kerja_id_2 != 0){
						$('#pegawai_unit_kerja_id_1').attr('data-id', unit_kerja_id);

						$("#pegawai_unit_kerja_id_1").val(unit_kerja_id_2);
						$('#pegawai_unit_kerja_id_1').trigger('change');

					}
					else{
						if(unit_kerja_id != null && unit_kerja_id != 0){
							$("#pegawai_unit_kerja_id_1").val(unit_kerja_id);
							$('#pegawai_unit_kerja_id_1').trigger('change');

							
						}
					}
				}
			}
		}
	});
</script>
