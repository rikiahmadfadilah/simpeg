<style type="text/css">
.control-label{
	font-weight: bold;
}
</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Data <?php echo (($pegawai["pegawai_jenis_kelamin_id"] == 1)?"Istri":"Suami");?> </h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nip"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tempat Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_tempat_lahir"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Jabatan</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama_jabatan"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Unit Kerja</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["unit_kerja_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<a href="<?php echo base_url();?>kepegawaian/jabatan" class="btn btn-xs btn-warning">Kembali</a></td>

							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Daftar Data <?php echo (($pegawai["pegawai_jenis_kelamin_id"] == 1)?"Istri":"Suami");?></span>
					<div class="col-md-12">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
							<thead>
								<tr>
									<th class="text-center">Nama <?php echo (($pegawai["pegawai_jenis_kelamin_id"] == 1)?"Istri":"Suami");?></th>
									<th class="text-center">Tanggal Lahir</th>									
									<th class="text-center">Pendidikan</th>
									<th class="text-center">Pekerjaan</th>
									<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($history)>0){?>
								<?php foreach ($history as $h) {  ?>
								<tr>
									<td><?php echo $h["suami_istri_nama"];?></td>
									<td><?php echo $h["suami_istri_tanggal_lahir"];?></td>
									<td><?php echo $h["pendidikan_nama"];?></td>
									<td><?php echo $h["suami_istri_pekerjaan"];?></td>
									<td>
										<ul class="icons-list">
											<li><a href="javascript:void(0)" onclick="update_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
											<li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>
											<li><a onclick="set_non_aktif(<?php echo $h["suami_istri_id"];?>,<?php echo $h["suami_istri_pegawai_id"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
										</ul>
									</td>
								</tr>
								<?php } ?>
								<?php }else{ ?>
								<tr>
									<td class="text-center" colspan="9">Data Belum Tersedia</td>
								</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<td class="text-right" colspan="9"><a href="javascript:void(0)" onclick="tambah_data();" class="btn btn-xs btn-primary btn-tfoot-table">Tambah Data</a>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="col-md-12 form_tambah_data" style="display: none;">
						<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan Data  <?php echo (($pegawai["pegawai_jenis_kelamin_id"] == 1)?"Istri":"Suami");?> </span>
						<div class="col-md-12">

							<div class="col-md-12">
								
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Nomor <?php echo (($pegawai["pegawai_jenis_kelamin_id"] == 1)?"Karis":"Karsu");?></label>
									<div class="col-md-9">
										<input type="text" name="suami_istri_no_karis_karsu" id="suami_istri_no_karis_karsu" class="form-control input-xs wajib">
										<input type="hidden" name="suami_istri_jenis" id="suami_istri_jenis" class="form-control input-xs wajib" value="<?php echo (($pegawai["pegawai_jenis_kelamin_id"] == 1)?"1":"2");?>">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Nama <?php echo (($pegawai["pegawai_jenis_kelamin_id"] == 1)?"Istri":"Suami");?></label>
									<div class="col-md-9">
										<input type="text" name="suami_istri_nama" id="suami_istri_nama" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tanggal Lahir</label>
									<div class="col-md-9">
										<input type="text" name="suami_istri_tanggal_lahir" id="suami_istri_tanggal_lahir" class="form-control input-xs wajib pickttl">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tanggal Nikah</label>
									<div class="col-md-9">
										<input type="text" name="suami_istri_tanggal_nikah" id="suami_istri_tanggal_nikah" class="form-control input-xs wajib pickttl">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Pendidikan</label>
									<div class="col-md-9">
										<select name="suami_istri_pendidikan_id" id="suami_istri_pendidikan_id" data-placeholder="Pendidikan" class="select-size-xs wajib" >
											<option value=""></option>
											<?php foreach ($pendidikan as $i) {
												echo '<option value="'.$i["pendidikan_id"].'" >'.$i["pendidikan_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Pekerjaan</label>
									<div class="col-md-9">
										<input type="text" name="suami_istri_pekerjaan" id="suami_istri_pekerjaan" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Status <?php echo (($pegawai["pegawai_jenis_kelamin_id"] == 1)?"Istri":"Suami");?></label>
									<div class="col-md-9">
										<select name="suami_istri_jenis_status" id="suami_istri_jenis_status" data-placeholder="Status <?php echo (($pegawai["pegawai_jenis_kelamin_id"] == 1)?"Istri":"Suami");?>" class="select-size-xs wajib" onchange="PilihStatusPernikahan(this)">
											<option value=""></option>
											<option value="1">Saat ini</option>
											<option value="2">Telah meninggal</option>
											<option value="3">Telah Bercerai</option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs IsNotSaatIni" style="display:none">
									<label class="col-md-3 control-label">Tanggal Cerai/Wafat</label>
									<div class="col-md-9">
										<input type="text" name="suami_istri_tanggal_status" id="suami_istri_tanggal_status" class="form-control input-xs pickttl">
									</div>
								</div>
								<div class="form-group form-group-xs IsNotSaatIni" style="display:none">
									<label class="col-md-3 control-label">SK Cerai/Wafat</label>
									<div class="col-md-9">
										<input type="text" name="suami_istri_sk_cerai_wafat" id="suami_istri_sk_cerai_wafat" class="form-control input-xs">
									</div>
								</div>								
								<div class="form-group form-group-xs">
									<div class="col-md-9 col-md-offset-3">
										<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
										<input type="hidden" name="suami_istri_pegawai_id" id="suami_istri_pegawai_id" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_id"];?>">
										<input type="hidden" name="suami_istri_pegawai_nip" id="suami_istri_pegawai_nip" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_nip"];?>">
										<input type="hidden" name="isUpdate" id="isUpdate" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="suami_istri_id" id="suami_istri_id" class="form-control input-xs wajib" value="0">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 form_lihat_data" style="display: none;">
						<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Lihat Data  <?php echo (($pegawai["pegawai_jenis_kelamin_id"] == 1)?"Istri":"Suami");?> </span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Nomor <?php echo (($pegawai["pegawai_jenis_kelamin_id"] == 1)?"Karis":"Karsu");?></label>
									<div class="col-md-9">
										<div class="form-control-static suami_istri_no_karis_karsu">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Nama <?php echo (($pegawai["pegawai_jenis_kelamin_id"] == 1)?"Istri":"Suami");?></label>
									<div class="col-md-9">
										<div class="form-control-static suami_istri_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tanggal Lahir</label>
									<div class="col-md-9">
										<div class="form-control-static suami_istri_tanggal_lahir">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-lg-3 control-label">Tanggal Nikah</label>
									<div class="col-lg-9">
										<div class="form-control-static suami_istri_tanggal_nikah">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Pendidikan</label>
									<div class="col-md-9">
										<div class="form-control-static pendidikan_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Pekerjaan</label>
									<div class="col-md-9">
										<div class="form-control-static suami_istri_pekerjaan">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Status <?php echo (($pegawai["pegawai_jenis_kelamin_id"] == 1)?"Istri":"Suami");?></label>
									<div class="col-md-9">
										<div class="form-control-static suami_istri_jenis_status">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik IsNotSaatIniView" style="display: none">
									<label class="col-md-3 control-label text_tcw">Tanggal Cerai/Wafat</label>
									<div class="col-md-9">
										<div class="form-control-static suami_istri_tanggal_status">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik IsNotSaatIniView" style="display: none">
									<label class="col-md-3 control-label text_skcw">SK Cerai/Wafat</label>
									<div class="col-md-9">
										<div class="form-control-static suami_istri_sk_cerai_wafat">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<div class="col-md-9 col-md-offset-3">
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>
	<script type="text/javascript">
		function set_non_aktif(id,pegawai_id){
			$('#text_konfirmasi').html('Anda yakin menghapus data ini ?');
			$('#konfirmasipenyimpanan').modal('show');
			$('#setujukonfirmasibutton').unbind();
			$('#setujukonfirmasibutton').on('click', function () {
				$('#konfirmasipenyimpanan').modal('hide');
				$('.angkadoank').inputmask('remove');
				$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
				window.location.href = base_url+'kepegawaian/suami_istri/hapus/'+id+'/'+pegawai_id;
			});
		}
		function lihat_data(data){
			$('.form_tambah_data').hide();
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					lihat_data(data);
				});
			}else{
				$('.form_lihat_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_lihat_data").offset().top-60
					}, 500);

					var suami_istri_id = datajson.suami_istri_id;
					var suami_istri_jenis = datajson.suami_istri_jenis;
					var suami_istri_nama = datajson.suami_istri_nama;
					var suami_istri_no_karis_karsu = datajson.suami_istri_no_karis_karsu;
					var suami_istri_tanggal_lahir = datajson.suami_istri_tanggal_lahir;
					var suami_istri_tanggal_nikah = datajson.suami_istri_tanggal_nikah;
					var pendidikan_nama = datajson.pendidikan_nama;
					var suami_istri_pekerjaan = datajson.suami_istri_pekerjaan;
					var suami_istri_jenis_status = datajson.suami_istri_jenis_status;
					var suami_istri_tanggal_status = datajson.suami_istri_tanggal_status;
					var suami_istri_sk_cerai_wafat = datajson.suami_istri_sk_cerai_wafat;

					//alert(Tampil);
					var suami_istri_jenis_statusView ='';
					if(suami_istri_jenis_status == 1){
						suami_istri_jenis_statusView ='Saat Ini';
						$(".IsNotSaatIniView").hide();
					}
					else if(suami_istri_jenis_status == 2){
						suami_istri_jenis_statusView ='Telah Meninggal';
						$(".IsNotSaatIniView").show();
					}
					else if(suami_istri_jenis_status == 2){
						suami_istri_jenis_statusView ='Telah Meninggal';
						$(".IsNotSaatIniView").show();
					}
					
					$(".suami_istri_nama").html(suami_istri_nama);
					$(".suami_istri_no_karis_karsu").html(suami_istri_no_karis_karsu);
					$(".suami_istri_tanggal_lahir").html(suami_istri_tanggal_lahir);
					$(".suami_istri_tanggal_nikah").html(suami_istri_tanggal_nikah);
					$(".pendidikan_nama").html(pendidikan_nama);
					$(".suami_istri_pekerjaan").html(suami_istri_pekerjaan);
					$(".suami_istri_jenis_status").html(suami_istri_jenis_statusView);
					$(".suami_istri_tanggal_status").html(suami_istri_tanggal_status);
					$(".suami_istri_sk_cerai_wafat").html(suami_istri_sk_cerai_wafat);					
					
				});
			}
		}
		function update_data(data){
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			
			$('.form_lihat_data').hide();
			$('.label_aksi_form').html('Form Perubahan Data  <?php echo (($pegawai["pegawai_jenis_kelamin_id"] == 1)?"Istri":"Suami");?> ');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					update_data(data);
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
					$('#isUpdate').val(1);
					
					// var suami_istri_pegawai_id = datajson.suami_istri_pegawai_id;
					// var suami_istri_pegawai_nip = datajson.suami_istri_pegawai_nip;
					var suami_istri_id = datajson.suami_istri_id;
					var suami_istri_jenis = datajson.suami_istri_jenis;
					var suami_istri_nama = datajson.suami_istri_nama;
					var suami_istri_no_karis_karsu = datajson.suami_istri_no_karis_karsu;
					var suami_istri_tanggal_lahir = datajson.suami_istri_tanggal_lahir;
					var suami_istri_tanggal_nikah = datajson.suami_istri_tanggal_nikah;
					var suami_istri_pendidikan_id = datajson.suami_istri_pendidikan_id;
					var suami_istri_pekerjaan = datajson.suami_istri_pekerjaan;
					var suami_istri_jenis_status = datajson.suami_istri_jenis_status;
					var suami_istri_tanggal_status = datajson.suami_istri_tanggal_status;
					var suami_istri_sk_cerai_wafat = datajson.suami_istri_sk_cerai_wafat;

					//alert(Tampil);

					$("#suami_istri_id").val(suami_istri_id);
					$("#suami_istri_jenis").val(suami_istri_jenis);
					$("#suami_istri_nama").val(suami_istri_nama);
					$("#suami_istri_no_karis_karsu").val(suami_istri_no_karis_karsu);
					$("#suami_istri_tanggal_lahir").val(suami_istri_tanggal_lahir);
					$("#suami_istri_tanggal_nikah").val(suami_istri_tanggal_nikah);
					$("#suami_istri_pendidikan_id").val(suami_istri_pendidikan_id);
					$("#suami_istri_pendidikan_id").trigger('change');
					$("#suami_istri_pekerjaan").val(suami_istri_pekerjaan);
					$("#suami_istri_jenis_status").val(suami_istri_jenis_status);
					$("#suami_istri_jenis_status").trigger('change');
					$("#suami_istri_tanggal_status").val(suami_istri_tanggal_status);
					$("#suami_istri_sk_cerai_wafat").val(suami_istri_sk_cerai_wafat);

					
				});
			}
		}

		function tambah_data(){
			$('.form_lihat_data').hide();
			reset_form();
			$('.label_aksi_form').html('Form Penambahan Data  <?php echo (($pegawai["pegawai_jenis_kelamin_id"] == 1)?"Istri":"Suami");?> ');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					$('.form_tambah_data').slideDown('fast',function(){
						$('html, body').animate({
							scrollTop: $(".form_tambah_data").offset().top-60
						}, 500);
						
					});
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
				});
			}
		}
		function batal_data(){
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					reset_form();
				});
			}
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					reset_form();
				});
			}
		}
		function reset_form(){
			// $("#suami_istri_id").val('');
			// $("#suami_istri_pegawai_id").val('');
			$("#suami_istri_id").val('');
			$("#suami_istri_jenis").val('');
			$("#suami_istri_nama").val('');
			$("#suami_istri_no_karis_karsu").val('');
			$("#suami_istri_tanggal_lahir").val('');
			$("#suami_istri_tanggal_nikah").val('');
			$("#suami_istri_pendidikan_id").val('');
			$("#suami_istri_pendidikan_id").trigger('change');
			$("#suami_istri_pekerjaan").val('');
			$("#suami_istri_jenis_status").val('');
			$("#suami_istri_jenis_status").trigger('change');
			$("#suami_istri_tanggal_status").val('');
			$("#suami_istri_sk_cerai_wafat").val('');
			$('.validation-error-label').css('display','none');
		}
		
		$(document).ready(function() {

			var d = new Date();

			$('.pickttl').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});
			$('.pickttlstart').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});

			$('.pickttlend').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});

		//On value change
		$(".pickttlstart").change(function(e) {
	    	//var date = rangeDemoConv.parse($(".pickttlstar").val()).getTime();
	    	//alert(date);
	    	//min: new Date(2015,3,20),

	    	var mulai1 = $("#diklat_tanggal_mulai").val().split(" ");
	    	var tgl = mulai1[0];
	    	var bulan = mulai1[1];
	    	var tahun = mulai1[2];

	    	$('.pickttlend').pickadate({
	    		monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
	    		weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
	    		selectMonths: true,
	    		selectYears: 80,
	    		min: new Date(tahun, 5, 20),
	    		max: true,
	    		today:'Hari ini',
	    		clear: 'Hapus',
	    		close: 'Keluar',
	    		format: 'dd mmmm yyyy',
	    		formatSubmit: 'yyyy-mm-dd'
	    	});

	    	var yesterday  = new Date((new Date()).valueOf()-1000*60*60*24);
	    	var neededDates = datas.prdetail.PRD_DATE_NEED;
	    	var neededDate = neededDates.split('-');

	    	var tglneed = $('#tanggal_dibutuhkan').pickadate({
	    		monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
	    		monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agt', 'Sep', 'Okt', 'Nov', 'Des'],
	    		weekdaysFull: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu'],
	    		weekdaysShort: ['Min', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
	    		selectYears: true,
	    		selectMonths: true,
	    		format: 'd mmmm yyyy',
	    		formatSubmit: 'yyyy-mm-dd',
	    		today: 'Hari ini',
	    		clear: 'Reset',
	    		close: 'Batal',
	    		disable: [
	    		{ from: [0,0,0], to: yesterday }
	    		]
	    	});

	    	var dpicker = tglneed.pickadate('picker');
	    	dpicker.set('select', [neededDate[0], (neededDate[1]-1), neededDate[2]]);
	        // try {
	        //     var fromDay = rangeDemoConv.parse($("#rangeDemoStart").val()).getTime();
	        //     var dayLater = new Date(fromDay+oneDay);
	        //         dayLater.setHours(0,0,0,0);
	        //     var ninetyDaysLater = new Date(fromDay+(90*oneDay));
	        //         ninetyDaysLater.setHours(23,59,59,999);

	        //     // End date
	        //     $("#rangeDemoFinish")
	        //     .AnyTime_noPicker()
	        //     .removeAttr("disabled")
	        //     .val(rangeDemoConv.format(dayLater))
	        //     .AnyTime_picker({
	        //         earliest: dayLater,
	        //         format: rangeDemoFormat,
	        //         latest: ninetyDaysLater
	        //     });
	        // }
	        // catch(e) {
	        //     // Disable End date field
	        //     $("#rangeDemoFinish").val("").attr("disabled","disabled");
	        // }
	    });

	});

function ToJavaScriptDateViewDoank(value) {
        //var pattern = /Date\(([^)]+)\)/;
        //var results = pattern.exec(value);
        //var dt = new Date(parseFloat(results[1]));
        var MonthInd = GetMonthIndInt((value.getMonth() + 1));
        return value.getDate() + " " + MonthInd + " " + value.getFullYear();
    }

    function GetMonthIndInt(month) {
    	switch (month) {
    		case 1:
    		return "Januari"
    		break;
    		case 2:
    		return "Februari"
    		break;
    		case 3:
    		return "Maret"
    		break;
    		case 4:
    		return "April"
    		break;
    		case 5:
    		return "Mei"
    		break;
    		case 6:
    		return "Juni"
    		break;
    		case 7:
    		return "Juli"
    		break;
    		case 8:
    		return "Agustus"
    		break;
    		case 9:
    		return "September"
    		break;
    		case 10:
    		return "Oktober"
    		break;
    		case 11:
    		return "November"
    		break;
    		case 12:
    		return "Desember"
    		break;
    		default:
    		return "Undefined"
    	}
    }
    function PilihStatusPernikahan(ini){
    	var thisval = $(ini).val();
    	if(thisval == 1){
    		$(".IsNotSaatIni").hide();
    		$("#suami_istri_tanggal_status").removeClass('wajib');
    		$("#suami_istri_sk_cerai_wafat").removeClass('wajib');
    	}
    	else{
    		$(".IsNotSaatIni").show();
    		$("#suami_istri_tanggal_status").addClass('wajib');
    		$("#suami_istri_sk_cerai_wafat").addClass('wajib');
    	}
    }
</script>
