<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">DATA PEGAWAI NON PNS</h6>
		<div class="heading-elements">
			<div class="heading-btn">
				<a href="<?php echo base_url().'kepegawaian/nonpns/create';?>" class="btn btn-primary btn-labeled btn-xs"><b><i class=" icon-plus3"></i></b> Tambah Data Pegawai Non PNS</a>
			</div>
		</div>
	</div>
	<div class="panel-body">
		<div class="col-md-12" align="right">
			<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
				<div class="form-group form-group-xs">
					<label class="col-md-2 control-label">STATUS NON PNS </label>
					<div class="col-md-2">
						<select name="status_nonpns" id="status_nonpns" class="select-size-xs col">
							<option value="1">AKTIF</option>
							<option value="0">NON AKTIF</option>
							<option value="99">AKTIF & NON AKTIF</option>
						</select>
					</div>
				</div>
			</form>
		</div>
		<!-- <ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#aktif">Aktif</a></li>
			<li><a data-toggle="tab" href="#nonaktif">Non Aktif</a></li>
			<li><a data-toggle="tab" href="#aktifnonaktif">Aktif & Non Aktif</a></li>
		</ul>  -->
		<!-- <div class="tab-content"> -->
			<!-- <div class="col-lg-12 tab-pane fade in active" id="aktif"> -->
				<table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive epagawai_table" id="list_data">
					<thead>
						<tr>
							<th class="text-center"style="width: 175px;min-width: 175px;">NIP</th>
							<th class="text-center">Nama</th>
							<th class="text-center">Tempat/Tanggal Lahir</th>
							<th class="text-center">Nama Jabatan</th>
							<th class="text-center">Unit Kerja</th>
							<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
						</tr>
					</thead>
				</table>
			<!-- </div> -->
			<!-- <div class="col-lg-12 tab-pane fade" id="nonaktif">
				<table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive epagawai_table" id="list_data_nonaktif">
					<thead>
						<tr>
							<th class="text-center"style="width: 175px;min-width: 175px;">NIP</th>
							<th class="text-center">Nama</th>
							<th class="text-center">Tempat/Tanggal Lahir</th>
							<th class="text-center">Nama Jabatan</th>
							<th class="text-center">Unit Kerja</th>
							<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
						</tr>
					</thead>
				</table>
			</div>
			<div class="col-lg-12 tab-pane fade" id="aktifnonaktif">
				<table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive epagawai_table" id="list_data_aktifnonaktif">
					<thead>
						<tr>
							<th class="text-center"style="width: 175px;min-width: 175px;">NIP</th>
							<th class="text-center">Nama</th>
							<th class="text-center">Tempat/Tanggal Lahir</th>
							<th class="text-center">Nama Jabatan</th>
							<th class="text-center">Unit Kerja</th>
							<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
						</tr>
					</thead>
				</table>
			</div> -->
		<!-- </div> -->
	</div>
</div>
<script type="text/javascript">
	var $ = jQuery;
	var tablenonpns;
	$(document).ready(function() {
		showdatatable($("#status_nonpns").val());
	});
	function showdatatable(status_nonpns){
		tablenonpns = $('#list_data').dataTable( {
			"processing": true,
			"serverSide": true,
			"bServerSide": true,
			"destroy": true,
			"sAjaxSource": base_url+"kepegawaian/nonpns/list_data/"+status_nonpns,
			 "aaSorting": [],
			 "order": [],
			 "iDisplayLength": 50,
			"aoColumns": [
			{ "sClass": "text-left" },
			{ "sClass": "text-left" },
			{ "sClass": "text-left" },
			{ "sClass": "text-left" },
			{ "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-center" },
			],
			"fnDrawCallback": function () {
				set_default_datatable();
			},
		});
	}
	$("#status_nonpns").change(function(){
		var status_nonpns = $(this).val();
		showdatatable(status_nonpns);
		
	});
		// $('#list_data_nonaktif').dataTable( {
		// 	"processing": true,
		// 	"serverSide": true,
		// 	"bServerSide": true,
		// 	"sAjaxSource": base_url+"kepegawaian/nonpns/list_data_nonaktif",
		// 	 "aaSorting": [],
		// 	 "order": [],
		// 	 "iDisplayLength": 50,
		// 	"aoColumns": [
		// 	{ "sClass": "text-left" },
		// 	{ "sClass": "text-left" },
		// 	{ "sClass": "text-left" },
		// 	{ "sClass": "text-left" },
		// 	{ "sClass": "text-left" },
		// 	{ "bSortable": false, "sClass": "text-center" },
		// 	],
		// 	"fnDrawCallback": function () {
		// 		set_default_datatable();
		// 	},
		// });
		// $('#list_data_aktifnonaktif').dataTable( {
		// 	"processing": true,
		// 	"serverSide": true,
		// 	"bServerSide": true,
		// 	"sAjaxSource": base_url+"kepegawaian/nonpns/list_data_aktifnonaktif",
		// 	 "aaSorting": [],
		// 	 "order": [],
		// 	 "iDisplayLength": 50,
		// 	"aoColumns": [
		// 	{ "sClass": "text-left" },
		// 	{ "sClass": "text-left" },
		// 	{ "sClass": "text-left" },
		// 	{ "sClass": "text-left" },
		// 	{ "sClass": "text-left" },
		// 	{ "bSortable": false, "sClass": "text-center" },
		// 	],
		// 	"fnDrawCallback": function () {
		// 		set_default_datatable();
		// 	},
		// });
</script>
