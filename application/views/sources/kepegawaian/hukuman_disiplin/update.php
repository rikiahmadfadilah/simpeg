<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Hukuman Disiplin</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nip"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tempat Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_tempat_lahir"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Jabatan</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama_jabatan"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Unit Kerja</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["unit_kerja_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<a href="<?php echo base_url();?>kepegawaian/hukuman_disiplin" class="btn btn-xs btn-warning">Kembali</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Daftar Hukuman Disiplin</span>
					<div class="col-md-12">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
							<thead>
								<tr>
									<th class="text-center">Jenis Hukuman</th>
									<th class="text-center">SK</th>
									<th class="text-center">TMT SK</th>
									<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($history)>0){?>
									<?php foreach ($history as $h) {  ?>
										<tr>
											<td><?php echo $h["jenis_hukuman_nama"];?></td>
											<td><?php echo $h["hukuman_disiplin_nomor_sk"];?></td>
											<td><?php echo $h["hukuman_disiplin_tanggal_sk"];?></td>
											<td>
												<ul class="icons-list">
													<li><a href="javascript:void(0)" onclick="update_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
													<li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>
													<li><a onclick="set_non_aktif(<?php echo $h["hukuman_disiplin_id"];?>,<?php echo $h["hukuman_disiplin_pegawai_id"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
												</ul>
											</td>
										</tr>
									<?php } ?>
								<?php }else{ ?>
									<tr>
										<td class="text-center" colspan="7">Data Belum Tersedia</td>
									</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<td class="text-right" colspan="7"><a href="javascript:void(0)" onclick="tambah_data();" class="btn btn-xs btn-primary btn-tfoot-table">Tambah Data</a>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="col-md-12 form_tambah_data" style="display: none;">
						<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan Data Hukuman Disiplin</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Pelanggaran PP</label>
									<div class="col-md-9">
										<select name="hukuman_disiplin_pp_pelanggaran_id" id="hukuman_disiplin_pp_pelanggaran_id" data-placeholder="Pelanggaran PP" class="select-size-xs wajib">
											<option value=""></option>
											<?php foreach ($pp_pelanggaran as $i) {
												echo '<option value="'.$i["pp_pelanggaran_id"].'" data-kode="'.$i["pp_pelanggaran_id"].'">'.$i["pp_pelanggaran_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Kode Hukuman</label>
									<div class="col-md-9">
										<select name="hukuman_disiplin_jenis_hukuman_id" id="hukuman_disiplin_jenis_hukuman_id" data-placeholder="Kode Hukuman" class="select-size-xs wajib">
											<option value=""></option>
											<?php foreach ($jenis_hukuman as $i) {
												echo '<option value="'.$i["jenis_hukuman_id"].'" data-kode="'.$i["jenis_hukuman_id"].'">'.$i["jenis_hukuman_kode"].'-'.$i["jenis_hukuman_nama"].'</option>';
											}?>
										</select>
										<span class="help-block">** Apabila Kode Hukuman disiplin tidak tercantum pada pilihan diatas, maka pilih kode hukuman disiplin lainnya kemudian di isi Hukuman disiplinnya pada kotak dibawah ini</span>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Hukuman Disiplin lainnya</label>
									<div class="col-md-9">
										<input type="text" name="hukuman_disiplin_kode_hukuman_lain" id="hukuman_disiplin_kode_hukuman_lain" class="form-control input-xs">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Nomor SK</label>
									<div class="col-md-9">
										<input type="text" name="hukuman_disiplin_nomor_sk" id="hukuman_disiplin_nomor_sk" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tanggal SK</label>
									<div class="col-md-9">
										<input type="text" name="hukuman_disiplin_tanggal_sk" id="hukuman_disiplin_tanggal_sk" class="form-control input-xs wajib pickhukumandisiplin">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tanggal Berlaku</label>
									<div class="col-md-9">
										<input type="text" name="hukuman_disiplin_tanggal_berlaku" id="hukuman_disiplin_tanggal_berlaku" class="form-control input-xs wajib pickhukumandisiplin">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Kode Pejabat Berwenang</label>
									<div class="col-md-9">
										<select name="hukuman_disiplin_jenis_pejabat_berwenang_id" id="hukuman_disiplin_jenis_pejabat_berwenang_id" data-placeholder="Kode Pejabat Berwenang" class="select-size-xs wajib">
											<option value=""></option>
											<?php foreach ($jenis_pejabat as $i) {
												echo '<option value="'.$i["jenis_pejabat_berwenang_id"].'" data-kode="'.$i["jenis_pejabat_berwenang_id"].'">'.$i["jenis_pejabat_berwenang_kode"].'-'.$i["jenis_pejabat_berwenang_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Ket.Pejabat Berwenang</label>
									<div class="col-md-9">
										<input type="text" name="hukuman_disiplin_ket_pejabatan" id="hukuman_disiplin_ket_pejabatan" class="form-control input-xs">
											<span class="help-block">(* bisa di isi nama pejabat/jabatan)</span>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Penyampaian Keputusan Hukuman</label>
									<div class="col-md-9">
										<select name="hukuman_disiplin_peny_kep_huk_id" id="hukuman_disiplin_peny_kep_huk_id" data-placeholder="Penyampaian Keputusan Hukuman" class="select-size-xs wajib">
											<option value=""></option>
											<?php foreach ($jenis_peny_keputusan as $i) {
												echo '<option value="'.$i["peny_keputusan_hukuman_id"].'" data-kode="'.$i["peny_keputusan_hukuman_id"].'">'.$i["peny_keputusan_hukuman_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Upaya Administratif</label>
									<div class="col-md-9">
										<select name="hukuman_disiplin_upaya_adm_hukuman_id" id="hukuman_disiplin_upaya_adm_hukuman_id" data-placeholder="Upaya Administratif" class="select-size-xs wajib">
											<option value=""></option>
											<?php foreach ($upaya_adm as $i) {
												echo '<option value="'.$i["upaya_adm_hukuman_id"].'" data-kode="'.$i["upaya_adm_hukuman_id"].'">'.$i["upaya_adm_hukuman_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Status</label>
									<div class="col-md-9">
										<select name="hukuman_disiplin_status_hukuman_id" id="hukuman_disiplin_status_hukuman_id" data-placeholder="Status" class="select-size-xs wajib">
											<option value=""></option>
											<?php foreach ($status_hukuman as $i) {
												echo '<option value="'.$i["status_hukuman_id"].'" data-kode="'.$i["status_hukuman_id"].'">'.$i["status_hukuman_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Catatan</label>
									<div class="col-md-9">
										<input type="text" name="hukuman_disiplin_catatan" id="hukuman_disiplin_catatan" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<div class="col-md-9 col-md-offset-3">
										<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
										<input type="hidden" name="hukuman_disiplin_pegawai_id" id="hukuman_disiplin_pegawai_id" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_id"];?>">
										<input type="hidden" name="hukuman_disiplin_pegawai_nip" id="hukuman_disiplin_pegawai_nip" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_nip"];?>">
										<input type="hidden" name="isUpdate" id="isUpdate" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="hukuman_disiplin_id" id="hukuman_disiplin_id" class="form-control input-xs wajib" value="0">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 form_lihat_data" style="display: none;">
						<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Lihat Data Hukuman Disiplin</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Pelanggara PP</label>
									<div class="col-md-9">
										<div class="form-control-static pp_pelanggaran_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Jenis Hukuman</label>
									<div class="col-md-9">
										<div class="form-control-static jenis_hukuman_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Jenis Hukuman Lainnya</label>
									<div class="col-md-9">
										<div class="form-control-static hukuman_disiplin_kode_hukuman_lain">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Nomor SK</label>
									<div class="col-md-9">
										<div class="form-control-static hukuman_disiplin_nomor_sk">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-lg-3 control-label">Tanggal SK</label>
									<div class="col-lg-9">
										<div class="form-control-static hukuman_disiplin_tanggal_sk">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tanggal Berlaku</label>
									<div class="col-md-9">
										<div class="form-control-static hukuman_disiplin_tanggal_berlaku">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Pejabat Berwenang</label>
									<div class="col-md-9">
										<div class="form-control-static jenis_pejabat_berwenang_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Ket. Pejabat Berwenang</label>
									<div class="col-md-9">
										<div class="form-control-static hukuman_disiplin_ket_pejabatan">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Penyampaian Keputusan Hukuman</label>
									<div class="col-md-9">
										<div class="form-control-static peny_keputusan_hukuman_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Upaya Administratif</label>
									<div class="col-md-9">
										<div class="form-control-static upaya_adm_hukuman_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Status</label>
									<div class="col-md-9">
										<div class="form-control-static status_hukuman_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Catatan</label>
									<div class="col-md-9">
										<div class="form-control-static hukuman_disiplin_catatan">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<div class="col-md-9 col-md-offset-3">
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>
	<script type="text/javascript">
		function set_non_aktif(id,pegawai_id){
			$('#text_konfirmasi').html('Anda yakin menghapus data ini ?');
			$('#konfirmasipenyimpanan').modal('show');
			$('#setujukonfirmasibutton').unbind();
			$('#setujukonfirmasibutton').on('click', function () {
				$('#konfirmasipenyimpanan').modal('hide');
				$('.angkadoank').inputmask('remove');
				$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
				window.location.href = base_url+'kepegawaian/hukuman_disiplin/hapus/'+id+'/'+pegawai_id;
			});
		}
		function lihat_data(data){
			$('.form_tambah_data').hide();
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					lihat_data(data);
				});
			}else{
				$('.form_lihat_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_lihat_data").offset().top-60
					}, 500);
					var hukuman_disiplin_id = datajson.hukuman_disiplin_id;
					var pp_pelanggaran_nama = datajson.pp_pelanggaran_nama;
					var jenis_hukuman_nama = datajson.jenis_hukuman_nama;
					var hukuman_disiplin_kode_hukuman_lain = datajson.hukuman_disiplin_kode_hukuman_lain;
					var hukuman_disiplin_nomor_sk = datajson.hukuman_disiplin_nomor_sk;
					var hukuman_disiplin_tanggal_sk = datajson.hukuman_disiplin_tanggal_sk;
					var hukuman_disiplin_tanggal_berlaku = datajson.hukuman_disiplin_tanggal_berlaku;
					var jenis_pejabat_berwenang_nama = datajson.jenis_pejabat_berwenang_nama;
					var hukuman_disiplin_ket_pejabatan = datajson.hukuman_disiplin_ket_pejabatan;
					var peny_keputusan_hukuman_nama = datajson.peny_keputusan_hukuman_nama;
					var upaya_adm_hukuman_nama = datajson.upaya_adm_hukuman_nama;
					var status_hukuman_nama = datajson.status_hukuman_nama;
					var hukuman_disiplin_catatan = datajson.hukuman_disiplin_catatan;

					$('.pp_pelanggaran_nama').html(pp_pelanggaran_nama);
					$('.jenis_hukuman_nama').html(jenis_hukuman_nama);
					$('.hukuman_disiplin_kode_hukuman_lain').html(hukuman_disiplin_kode_hukuman_lain);
					$('.hukuman_disiplin_nomor_sk').html(hukuman_disiplin_nomor_sk);
					$('.hukuman_disiplin_tanggal_sk').html(hukuman_disiplin_tanggal_sk);
					$('.hukuman_disiplin_tanggal_berlaku').html(hukuman_disiplin_tanggal_berlaku);
					$('.jenis_pejabat_berwenang_nama').html(jenis_pejabat_berwenang_nama);
					$('.hukuman_disiplin_ket_pejabatan').html(hukuman_disiplin_ket_pejabatan);
					$('.peny_keputusan_hukuman_nama').html(peny_keputusan_hukuman_nama);
					$('.upaya_adm_hukuman_nama').html(upaya_adm_hukuman_nama);
					$('.status_hukuman_nama').html(status_hukuman_nama);
					$('.hukuman_disiplin_catatan').html(hukuman_disiplin_catatan);
					
				});
			}
		}
		function update_data(data){
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			$('.form_lihat_data').hide();
			$('.label_aksi_form').html('Form Perubahan Data hukuman_disiplin');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					update_data(data);
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
					$('#isUpdate').val(1);
					var hukuman_disiplin_id = datajson.hukuman_disiplin_id;
					var hukuman_disiplin_pp_pelanggaran_id = datajson.hukuman_disiplin_pp_pelanggaran_id;
					var hukuman_disiplin_jenis_hukuman_id = datajson.hukuman_disiplin_jenis_hukuman_id;
					var hukuman_disiplin_kode_hukuman_lain = datajson.hukuman_disiplin_kode_hukuman_lain;
					var hukuman_disiplin_nomor_sk = datajson.hukuman_disiplin_nomor_sk;
					var hukuman_disiplin_tanggal_sk = datajson.hukuman_disiplin_tanggal_sk;
					var hukuman_disiplin_tanggal_berlaku = datajson.hukuman_disiplin_tanggal_berlaku;
					var hukuman_disiplin_jenis_pejabat_berwenang_id = datajson.hukuman_disiplin_jenis_pejabat_berwenang_id;
					var hukuman_disiplin_ket_pejabatan = datajson.hukuman_disiplin_ket_pejabatan;
					var hukuman_disiplin_peny_kep_huk_id = datajson.hukuman_disiplin_peny_kep_huk_id;
					var hukuman_disiplin_upaya_adm_hukuman_id = datajson.hukuman_disiplin_upaya_adm_hukuman_id;
					var hukuman_disiplin_status_hukuman_id = datajson.hukuman_disiplin_status_hukuman_id;
					var hukuman_disiplin_catatan = datajson.hukuman_disiplin_catatan;

					$('#hukuman_disiplin_id').val(hukuman_disiplin_id);
					$('#hukuman_disiplin_kode_hukuman_lain').val(hukuman_disiplin_kode_hukuman_lain);
					$('#hukuman_disiplin_nomor_sk').val(hukuman_disiplin_nomor_sk);
					$('#hukuman_disiplin_tanggal_sk').val(hukuman_disiplin_tanggal_sk);
					$('#hukuman_disiplin_tanggal_berlaku').val(hukuman_disiplin_tanggal_berlaku);
					$('#hukuman_disiplin_ket_pejabatan').val(hukuman_disiplin_ket_pejabatan);
					$('#hukuman_disiplin_catatan').val(hukuman_disiplin_catatan);
					$('#hukuman_disiplin_id').val(datajson.hukuman_disiplin_id);

					$('#hukuman_disiplin_pp_pelanggaran_id').select2('val', '');
					$('#hukuman_disiplin_pp_pelanggaran_id').val(hukuman_disiplin_pp_pelanggaran_id);
					$('#hukuman_disiplin_pp_pelanggaran_id').trigger('change');

					$('#hukuman_disiplin_jenis_hukuman_id').select2('val', '');
					$('#hukuman_disiplin_jenis_hukuman_id').val(hukuman_disiplin_jenis_hukuman_id);
					$('#hukuman_disiplin_jenis_hukuman_id').trigger('change');

					$('#hukuman_disiplin_jenis_pejabat_berwenang_id').select2('val', '');
					$('#hukuman_disiplin_jenis_pejabat_berwenang_id').val(hukuman_disiplin_jenis_pejabat_berwenang_id);
					$('#hukuman_disiplin_jenis_pejabat_berwenang_id').trigger('change');

					$('#hukuman_disiplin_peny_kep_huk_id').select2('val', '');
					$('#hukuman_disiplin_peny_kep_huk_id').val(hukuman_disiplin_peny_kep_huk_id);
					$('#hukuman_disiplin_peny_kep_huk_id').trigger('change');

					$('#hukuman_disiplin_upaya_adm_hukuman_id').select2('val', '');
					$('#hukuman_disiplin_upaya_adm_hukuman_id').val(hukuman_disiplin_upaya_adm_hukuman_id);
					$('#hukuman_disiplin_upaya_adm_hukuman_id').trigger('change');

					$('#hukuman_disiplin_status_hukuman_id').select2('val', '');
					$('#hukuman_disiplin_status_hukuman_id').val(hukuman_disiplin_status_hukuman_id);
					$('#hukuman_disiplin_status_hukuman_id').trigger('change');
				});
			}
		}
		function tambah_data(){
			$('.form_lihat_data').hide();
			reset_form();
			$('.label_aksi_form').html('Form Penambahan Data hukuman_disiplin');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					$('.form_tambah_data').slideDown('fast',function(){
						$('html, body').animate({
							scrollTop: $(".form_tambah_data").offset().top-60
						}, 500);
						
					});
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
				});
			}
		}
		function batal_data(){
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					reset_form();
				});
			}
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					reset_form();
				});
			}
		}
		function reset_form(){
			$('#hukuman_disiplin_id').val(0);
			$('#hukuman_disiplin_pp_pelanggaran_id').select2('val', ' ');
			$('#hukuman_disiplin_jenis_hukuman_id').select2('val', ' ');
			$('#hukuman_disiplin_kode_hukuman_lain').val('');
			$('#hukuman_disiplin_nomor_sk').val('');
			$('#hukuman_disiplin_tanggal_sk').val('');
			$('#hukuman_disiplin_tanggal_berlaku').val('');
			$('#hukuman_disiplin_jenis_pejabat_berwenang_id').select2('val',' ');
			$('#hukuman_disiplin_ket_pejabatan').val('');
			$('#hukuman_disiplin_peny_kep_huk_id').select2('val', ' ');
			$('#hukuman_disiplin_upaya_adm_hukuman_id').select2('val',' ');
			$('#hukuman_disiplin_status_hukuman_id').select2('val',' ');
			$('#hukuman_disiplin_catatan').val('');
			$('.validation-error-label').css('display','none');
		}
		$(document).ready(function(){
			$('.pickhukumandisiplin').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});
		})
		$(document).ready(function() {
			var isi = "-";
			console.log(isi);
			$('#hukuman_disiplin_kode_hukuman_lain').val(isi);

			console.log();


		});
	</script>
