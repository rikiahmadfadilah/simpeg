<style type="text/css">
.control-label{
	font-weight: bold;
}
</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Riwayat Unit Kerja</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nip"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tempat Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_tempat_lahir"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Jabatan</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama_jabatan"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Unit Kerja</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["unit_kerja_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<a href="<?php echo base_url();?>kepegawaian/unit_kerja" class="btn btn-xs btn-warning">Kembali</a></td>

							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Daftar Riwayat Unit Kerja</span>
					<div class="col-md-12">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
							<thead>
								<tr>
									<th rowspan="2" class="text-center">Eselon</th>
									<!-- <th rowspan="2" class="text-center">Nama Jabatan</th> -->
									<th rowspan="2" class="text-center">Nama Unit Kerja</th>									
									<th colspan="2" class="text-center">Tanggal</th>
									<th rowspan="2" class="text-center">Nomor SK</th>
									<th rowspan="2" class="text-center">Tanggal SK</th>
									<th rowspan="2" class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
								<tr>
									<th class="text-center">Mulai</th>
									<th class="text-center">Selesai</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($history)>0){?>
								<?php foreach ($history as $h) {  ?>
								<tr>
									<td><?php echo $h["jabatan_nama"];?></td>
									<!-- <td><?php echo $h["unit_kerja_pegawai_jabatan_nama"];?></td> -->
									<td><?php echo $h["unit_kerja_nama"];?></td>
									<td class="text-center"><?php echo dateEnToId($h["unit_kerja_pegawai_tanggal_mulai"], 'd-m-Y');?></td>
									<td class="text-center"><?php echo dateEnToId($h["unit_kerja_pegawai_tanggal_selesai"], 'd-m-Y');?></td>									
									<td><?php echo $h["unit_kerja_pegawai_nomor_sk"];?></td>
									<td class="text-center"><?php echo dateEnToId($h["unit_kerja_pegawai_tanggal_sk"], 'd-m-Y');?></td>
									<td>
										<ul class="icons-list">
											<li><a href="javascript:void(0)" onclick="update_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
											<li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>
											<li><a onclick="set_non_aktif(<?php echo $h["unit_kerja_pegawai_id"];?>,<?php echo $h["unit_kerja_pegawai_pegawai_id"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
										</ul>
									</td>
								</tr>
								<?php } ?>
								<?php }else{ ?>
								<tr>
									<td class="text-center" colspan="9">Data Belum Tersedia</td>
								</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<td class="text-right" colspan="9"><a href="javascript:void(0)" onclick="tambah_data();" class="btn btn-xs btn-primary btn-tfoot-table">Tambah Data</a>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="col-md-12 form_tambah_data" style="display: none;">
						<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan Data Jenjang Jabatan</span>
						<div class="col-md-12">

							<div class="col-md-12">
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Unit Kerja</label>
									<div class="col-lg-9">
										<select name="unit_kerja_pegawai_unit_kerja_id[]" id="unit_kerja_pegawai_unit_kerja_id_1" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs ga_wajib" onchange="get_unit_kerja_hirarki(this,1)">
											<option></option>
											<?php foreach ($unit_kerja as $i) {
												echo '<option value="'.$i["unit_kerja_id"].'" parent_id_kode="'.$i["unit_kerja_id_kode"].'" jabatan_id="'.$i["jabatan_id"].'">'.$i["unit_kerja_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs unit_kerja_pegawai_unit_kerja_id_2" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="unit_kerja_pegawai_unit_kerja_id[]" id="unit_kerja_pegawai_unit_kerja_id_2" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs"  onchange="get_unit_kerja_hirarki(this,2)">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs unit_kerja_pegawai_unit_kerja_id_3" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="unit_kerja_pegawai_unit_kerja_id[]" id="unit_kerja_pegawai_unit_kerja_id_3" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs"  onchange="get_unit_kerja_hirarki(this,3)">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs unit_kerja_pegawai_unit_kerja_id_4" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="unit_kerja_pegawai_unit_kerja_id[]" id="unit_kerja_pegawai_unit_kerja_id_4" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs"  onchange="get_unit_kerja_hirarki(this,4)">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs unit_kerja_pegawai_unit_kerja_id_5" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="unit_kerja_pegawai_unit_kerja_id[]" id="unit_kerja_pegawai_unit_kerja_id_5" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs" onchange="get_unit_kerja_hirarki(this,5)">
											<option></option>
										</select>
									</div>
								</div>

								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Jabatan / Eselon</label>
									<div class="col-lg-9">
										<select name="unit_kerja_pegawai_jabatan_id" id="unit_kerja_pegawai_jabatan_id" data-placeholder="Pilih Jabatan / Eselon" data-id="0" class="select-size-xs ga_wajib">
											<option></option>
											<?php foreach ($jabatan as $i) {
												echo '<option value="'.$i["jabatan_id"].'" kode="'.$i["jabatan_kode"].'">'.$i["jabatan_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>

								<!-- <div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Nama Jabatan</label>
									<div class="col-md-9">
										<input type="text" name="unit_kerja_pegawai_jabatan_nama" id="unit_kerja_pegawai_jabatan_nama" class="form-control input-xs wajib">
									</div>
								</div> -->
								
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tanggal Mulai</label>
									<div class="col-md-9">
										<input type="text" name="unit_kerja_pegawai_tanggal_mulai" id="unit_kerja_pegawai_tanggal_mulai" class="form-control input-xs wajib pickttl">
									</div>
								</div>

								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tanggal Selesai</label>
									<div class="col-md-9">
										<input type="text" name="unit_kerja_pegawai_tanggal_selesai" id="unit_kerja_pegawai_tanggal_selesai" class="form-control input-xs wajib pickttl">
									</div>
								</div>

								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Nomor SK</label>
									<div class="col-md-9">
										<input type="text" name="unit_kerja_pegawai_nomor_sk" id="unit_kerja_pegawai_nomor_sk" class="form-control input-xs wajib">
									</div>
								</div>

								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tanggal SK</label>
									<div class="col-md-9">
										<input type="text" name="unit_kerja_pegawai_tanggal_sk" id="unit_kerja_pegawai_tanggal_sk" class="form-control input-xs wajib pickttl">
									</div>
								</div>

								<div class="form-group form-group-xs">
									<div class="col-md-9 col-md-offset-3">
										<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
										<input type="hidden" name="unit_kerja_pegawai_pegawai_id" id="unit_kerja_pegawai_pegawai_id" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_id"];?>">
										<input type="hidden" name="unit_kerja_pegawai_pegawai_nip" id="unit_kerja_pegawai_pegawai_nip" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_nip"];?>">
										<input type="hidden" name="isUpdate" id="isUpdate" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="unit_kerja_pegawai_id" id="unit_kerja_pegawai_id" class="form-control input-xs wajib" value="0">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 form_lihat_data" style="display: none;">
						<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Lihat Data Unit Kerja</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Unit Kerja</label>
									<div class="col-md-9">
										<div class="form-control-static unit_kerja_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Eselon</label>
									<div class="col-md-9">
										<div class="form-control-static jabatan_nama">-</div>
									</div>
								</div>
								<!-- <div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Nama Jabatan</label>
									<div class="col-md-9">
										<div class="form-control-static unit_kerja_pegawai_jabatan_nama">-</div>
									</div>
								</div> -->
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-lg-3 control-label">Tanggal Mulai</label>
									<div class="col-lg-9">
										<div class="form-control-static unit_kerja_pegawai_tanggal_mulai">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tanggal Selesai</label>
									<div class="col-md-9">
										<div class="form-control-static unit_kerja_pegawai_tanggal_selesai">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Nomor SK</label>
									<div class="col-md-9">
										<div class="form-control-static unit_kerja_pegawai_nomor_sk">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tanggal SK</label>
									<div class="col-md-9">
										<div class="form-control-static unit_kerja_pegawai_tanggal_sk">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<div class="col-md-9 col-md-offset-3">
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>
	<script type="text/javascript">
		function set_non_aktif(id,pegawai_id){
			$('#text_konfirmasi').html('Anda yakin menghapus data ini ?');
			$('#konfirmasipenyimpanan').modal('show');
			$('#setujukonfirmasibutton').unbind();
			$('#setujukonfirmasibutton').on('click', function () {
				$('#konfirmasipenyimpanan').modal('hide');
				$('.angkadoank').inputmask('remove');
				$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
				window.location.href = base_url+'kepegawaian/unit_kerja/hapus/'+id+'/'+pegawai_id;
			});
		}
		function lihat_data(data){
			$('.form_tambah_data').hide();
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					lihat_data(data);
				});
			}else{
				$('.form_lihat_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_lihat_data").offset().top-60
					}, 500);

					var jabatan_nama = datajson.jabatan_nama;
					// var unit_kerja_pegawai_jabatan_nama = datajson.unit_kerja_pegawai_jabatan_nama;
					var unit_kerja_nama = datajson.unit_kerja_nama;
					var unit_kerja_pegawai_tanggal_mulai = datajson.unit_kerja_pegawai_tanggal_mulai;
					var unit_kerja_pegawai_tanggal_selesai = datajson.unit_kerja_pegawai_tanggal_selesai;
					var unit_kerja_pegawai_nomor_sk = datajson.unit_kerja_pegawai_nomor_sk;
					var unit_kerja_pegawai_tanggal_sk = datajson.unit_kerja_pegawai_tanggal_sk;

					$(".jabatan_nama").html(jabatan_nama);
					// $(".unit_kerja_pegawai_jabatan_nama").html(unit_kerja_pegawai_jabatan_nama);
					$(".unit_kerja_nama").html(unit_kerja_nama);
					$(".unit_kerja_pegawai_tanggal_mulai").html(unit_kerja_pegawai_tanggal_mulai);
					$(".unit_kerja_pegawai_tanggal_selesai").html(unit_kerja_pegawai_tanggal_selesai);
					$(".unit_kerja_pegawai_nomor_sk").html(unit_kerja_pegawai_nomor_sk);
					$(".unit_kerja_pegawai_tanggal_sk").html(unit_kerja_pegawai_tanggal_sk);
					
				});
			}
		}
		function update_data(data){
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			
			$('.form_lihat_data').hide();
			$('.label_aksi_form').html('Form Perubahan Data Riwayat Unit Kerja');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					update_data(data);
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
					$('#isUpdate').val(1);

					// var unit_kerja_level = datajson.unit_kerja_pegawai_id;
					// var hirarki = datajson.unit_kerja_hirarki.split('|');
					// alert(hirarki[2]);
					// alert(datajson.unit_kerja_id);
					var unit_kerja_id_5 = datajson.unit_kerja_id_5;
					var unit_kerja_id_4 = datajson.unit_kerja_id_4;
					var unit_kerja_id_3 = datajson.unit_kerja_id_3;
					var unit_kerja_id_2 = datajson.unit_kerja_id_2;
					var unit_kerja_id = datajson.unit_kerja_id;
					if(unit_kerja_id_5 != null && unit_kerja_id_5 != 1){
						$('#unit_kerja_pegawai_unit_kerja_id_1').attr('data-id', unit_kerja_id_4);
						$('#unit_kerja_pegawai_unit_kerja_id_2').attr('data-id', unit_kerja_id_3);
						$('#unit_kerja_pegawai_unit_kerja_id_3').attr('data-id', unit_kerja_id_2);
						$('#unit_kerja_pegawai_unit_kerja_id_4').attr('data-id', unit_kerja_id);

						$("#unit_kerja_pegawai_unit_kerja_id_1").val(unit_kerja_id_5);
						$('#unit_kerja_pegawai_unit_kerja_id_1').trigger('change');						
					}

					else{
						if(unit_kerja_id_4 != null && unit_kerja_id_4 != 1){		
							$('#unit_kerja_pegawai_unit_kerja_id_1').attr('data-id', unit_kerja_id_3);
							$('#unit_kerja_pegawai_unit_kerja_id_2').attr('data-id', unit_kerja_id_2);
							$('#unit_kerja_pegawai_unit_kerja_id_3').attr('data-id', unit_kerja_id);

							$("#unit_kerja_pegawai_unit_kerja_id_1").val(unit_kerja_id_4);
							$('#unit_kerja_pegawai_unit_kerja_id_1').trigger('change');
							
						}
						else{
							if(unit_kerja_id_3 != null && unit_kerja_id_3 != 1){
								$('#unit_kerja_pegawai_unit_kerja_id_1').attr('data-id', unit_kerja_id_2);
								$('#unit_kerja_pegawai_unit_kerja_id_2').attr('data-id', unit_kerja_id);

								$("#unit_kerja_pegawai_unit_kerja_id_1").val(unit_kerja_id_3);
								$('#unit_kerja_pegawai_unit_kerja_id_1').trigger('change');
								
							}
							else{
								if(unit_kerja_id_2 != null && unit_kerja_id_2 != 1){
									$('#unit_kerja_pegawai_unit_kerja_id_1').attr('data-id', unit_kerja_id);

									$("#unit_kerja_pegawai_unit_kerja_id_1").val(unit_kerja_id_2);
									$('#unit_kerja_pegawai_unit_kerja_id_1').trigger('change');
									
								}
								else{
									if(unit_kerja_id != null && unit_kerja_id != 1){
										$("#unit_kerja_pegawai_unit_kerja_id_1").val(unit_kerja_id);
										$('#unit_kerja_pegawai_unit_kerja_id_1').trigger('change');
									}
								}
							}
						}
					}
					var unit_kerja_pegawai_id = datajson.unit_kerja_pegawai_id;
					var unit_kerja_pegawai_jabatan_id = datajson.unit_kerja_pegawai_jabatan_id;
					// var unit_kerja_pegawai_jabatan_nama = datajson.unit_kerja_pegawai_jabatan_nama;
					var unit_kerja_pegawai_unit_kerja_id = datajson.unit_kerja_pegawai_unit_kerja_id;
					var unit_kerja_pegawai_tanggal_mulai = datajson.unit_kerja_pegawai_tanggal_mulai;
					var unit_kerja_pegawai_tanggal_selesai = datajson.unit_kerja_pegawai_tanggal_selesai;
					var unit_kerja_pegawai_nomor_sk = datajson.unit_kerja_pegawai_nomor_sk;
					var unit_kerja_pegawai_tanggal_sk = datajson.unit_kerja_pegawai_tanggal_sk;

					$('#unit_kerja_pegawai_id').val(unit_kerja_pegawai_id);
					// $("#unit_kerja_pegawai_jabatan_nama").val(unit_kerja_pegawai_jabatan_nama);
					$("#unit_kerja_pegawai_unit_kerja_id").val(unit_kerja_pegawai_unit_kerja_id);
					$("#unit_kerja_pegawai_tanggal_mulai").val(unit_kerja_pegawai_tanggal_mulai);
					$("#unit_kerja_pegawai_tanggal_selesai").val(unit_kerja_pegawai_tanggal_selesai);
					$("#unit_kerja_pegawai_nomor_sk").val(unit_kerja_pegawai_nomor_sk);
					$("#unit_kerja_pegawai_tanggal_sk").val(unit_kerja_pegawai_tanggal_sk);
					$('#unit_kerja_pegawai_id').val(datajson.unit_kerja_pegawai_id);

					$('#unit_kerja_pegawai_jabatan_id').select2('val', '');
					$('#unit_kerja_pegawai_jabatan_id').val(unit_kerja_pegawai_jabatan_id);
					$('#unit_kerja_pegawai_jabatan_id').trigger('change');
				});
}
}

function tambah_data(){
	$('.form_lihat_data').hide();
	reset_form();
	$('.label_aksi_form').html('Form Penambahan Data Unit Kerja');
	if($('.form_tambah_data').is(":visible")){
		$('.form_tambah_data').slideUp('slow',function(){
			$('.form_tambah_data').slideDown('fast',function(){
				$('html, body').animate({
					scrollTop: $(".form_tambah_data").offset().top-60
				}, 500);

			});
		});
	}else{
		$('.form_tambah_data').slideDown('fast',function(){
			$('html, body').animate({
				scrollTop: $(".form_tambah_data").offset().top-60
			}, 500);
		});
	}
}
function batal_data(){
	if($('.form_tambah_data').is(":visible")){
		$('.form_tambah_data').slideUp('slow',function(){
			reset_form();
		});
	}
	if($('.form_lihat_data').is(":visible")){
		$('.form_lihat_data').slideUp('slow',function(){
			reset_form();
		});
	}
}
function reset_form(){
			// $("#unit_kerja_pegawai_id").val('');
			// $("#unit_kerja_pegawai_pegawai_id").val('');
			//$("#unit_kerja_pegawai_pegawai_nip").val('');
			$('#unit_kerja_pegawai_unit_kerja_id_1').attr('data-id', 0);
			$('#unit_kerja_pegawai_unit_kerja_id_2').attr('data-id', 0);
			$('#unit_kerja_pegawai_unit_kerja_id_3').attr('data-id', 0);
			$('#unit_kerja_pegawai_unit_kerja_id_4').attr('data-id', 0);

			// $("#unit_kerja_pegawai_jabatan_nama").val('');
			$("#unit_kerja_pegawai_unit_kerja_id").val('');
			$("#unit_kerja_pegawai_tanggal_mulai").val('');
			$("#unit_kerja_pegawai_tanggal_selesai").val('');
			$("#unit_kerja_pegawai_nomor_sk").val('');
			$("#unit_kerja_pegawai_tanggal_sk").val('');
			$('#unit_kerja_pegawai_unit_kerja_id_1').select2('val', ' ');


			// $("#unit_kerja_pegawai_unit_kerja_id_1").val('');
			// $('#unit_kerja_pegawai_unit_kerja_id_1').trigger('change');

			// $("#unit_kerja_pegawai_unit_kerja_id_2").val('');
			// $('#unit_kerja_pegawai_unit_kerja_id_2').trigger('change');

			// $("#unit_kerja_pegawai_unit_kerja_id_3").val('');
			// $('#unit_kerja_pegawai_unit_kerja_id_3').trigger('change');

			// $("#unit_kerja_pegawai_unit_kerja_id_4").val('');
			// $('#unit_kerja_pegawai_unit_kerja_id_4').trigger('change');

			// $("#unit_kerja_pegawai_unit_kerja_id_5").val('');
			// $('#unit_kerja_pegawai_unit_kerja_id_5').trigger('change');
			$('.validation-error-label').css('display','none');
		}
		
		$(document).ready(function() {
			$('.select-size-xs').select2();
			var d = new Date();

			$('.pickttl').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});
			$('.pickttlstart').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});

			$('.pickttlend').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});

		//On value change
		$(".pickttlstart").change(function(e) {

			var mulai1 = $("#diklat_tanggal_mulai").val().split(" ");
			var tgl = mulai1[0];
			var bulan = mulai1[1];
			var tahun = mulai1[2];

			$('.pickttlend').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				min: new Date(tahun, 5, 20),
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});

			var yesterday  = new Date((new Date()).valueOf()-1000*60*60*24);
			var neededDates = datas.prdetail.PRD_DATE_NEED;
			var neededDate = neededDates.split('-');

			var tglneed = $('#tanggal_dibutuhkan').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agt', 'Sep', 'Okt', 'Nov', 'Des'],
				weekdaysFull: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu'],
				weekdaysShort: ['Min', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectYears: true,
				selectMonths: true,
				format: 'd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd',
				today: 'Hari ini',
				clear: 'Reset',
				close: 'Batal',
				disable: [
				{ from: [0,0,0], to: yesterday }
				]
			});

			var dpicker = tglneed.pickadate('picker');
			dpicker.set('select', [neededDate[0], (neededDate[1]-1), neededDate[2]]);

		});

	});

		function ToJavaScriptDateViewDoank(value) {
        //var pattern = /Date\(([^)]+)\)/;
        //var results = pattern.exec(value);
        //var dt = new Date(parseFloat(results[1]));
        var MonthInd = GetMonthIndInt((value.getMonth() + 1));
        return value.getDate() + " " + MonthInd + " " + value.getFullYear();
    }

    function GetMonthIndInt(month) {
    	switch (month) {
    		case 1:
    		return "Januari"
    		break;
    		case 2:
    		return "Februari"
    		break;
    		case 3:
    		return "Maret"
    		break;
    		case 4:
    		return "April"
    		break;
    		case 5:
    		return "Mei"
    		break;
    		case 6:
    		return "Juni"
    		break;
    		case 7:
    		return "Juli"
    		break;
    		case 8:
    		return "Agustus"
    		break;
    		case 9:
    		return "September"
    		break;
    		case 10:
    		return "Oktober"
    		break;
    		case 11:
    		return "November"
    		break;
    		case 12:
    		return "Desember"
    		break;
    		default:
    		return "Undefined"
    	}
    }

    function  get_unit_kerja_hirarki(ini,lvl) {
    	var attrid = $(ini).attr('data-id');
    	//alert(attrid);
    	if(lvl==1){
    		$('.unit_kerja_pegawai_unit_kerja_id_5').slideUp('fast',function(){
    			$('#unit_kerja_pegawai_unit_kerja_id_5').select2('val', '');
    			$('.unit_kerja_pegawai_unit_kerja_id_4').slideUp('fast',function(){
    				$('#unit_kerja_pegawai_unit_kerja_id_4').select2('val', '');
    				$('.unit_kerja_pegawai_unit_kerja_id_3').slideUp('fast',function(){
    					$('#unit_kerja_pegawai_unit_kerja_id_3').select2('val', '');
    					$('.unit_kerja_pegawai_unit_kerja_id_2').slideUp('fast',function(){
    						$('#unit_kerja_pegawai_unit_kerja_id_2').select2('val', '');
    					});
    				});
    			});
    		});
    	}else if(lvl==2){
    		$('.unit_kerja_pegawai_unit_kerja_id_5').slideUp('fast',function(){
    			$('#unit_kerja_pegawai_unit_kerja_id_5').select2('val', '');
    			$('.unit_kerja_pegawai_unit_kerja_id_4').slideUp('fast',function(){
    				$('#unit_kerja_pegawai_unit_kerja_id_4').select2('val', '');
    				$('.unit_kerja_pegawai_unit_kerja_id_3').slideUp('fast',function(){
    					$('#unit_kerja_pegawai_unit_kerja_id_3').select2('val', '');
    				});
    			});
    		});
    	}else if(lvl==3){
    		$('.unit_kerja_pegawai_unit_kerja_id_5').slideUp('fast',function(){
    			$('#unit_kerja_pegawai_unit_kerja_id_5').select2('val', '');
    			$('.unit_kerja_pegawai_unit_kerja_id_4').slideUp('fast',function(){
    				$('#unit_kerja_pegawai_unit_kerja_id_4').select2('val', '');
    			});
    		});
    	}else if(lvl==4){
    		$('.unit_kerja_pegawai_unit_kerja_id_5').slideUp('fast',function(){
    			$('#unit_kerja_pegawai_unit_kerja_id_5').select2('val', '');
    		});
    	}
    	var parent_id_kode  = $('option:selected', ini).attr('parent_id_kode');
    	var jabatan_id  = $('option:selected', ini).attr('jabatan_id');

    	$('#unit_kerja_pegawai_jabatan_id').select2('val', '');
    	$('#unit_kerja_pegawai_jabatan_id').val(jabatan_id);
    	$('#unit_kerja_pegawai_jabatan_id').trigger('change');

    	$.ajax({
    		url: base_url+'kepegawaian/pns/get_unit_kerja_hirarki',
    		type: "post",
    		dataType: 'json',
    		data: { unit_kerja_parent_id_kode: parent_id_kode, unit_kerja_level: (lvl+2)},
    		success: function (data) {

    			var unit_kerja = data.unit_kerja;
    			var newoption = "<option></option>";

    			for(var i = 0; i < unit_kerja.length; i ++){
    				newoption+='<option value="'+unit_kerja[i].unit_kerja_id+'" parent_id_kode="'+unit_kerja[i].unit_kerja_id_kode+'"  jabatan_id="'+unit_kerja[i].jabatan_id+'" '+((unit_kerja[i].unit_kerja_id == attrid)?"selected":"")+'>'+unit_kerja[i].unit_kerja_nama+'</option>';
    			}
    			$('#unit_kerja_pegawai_unit_kerja_id_'+(lvl+1)).html(newoption);
    			if(unit_kerja.length>0){
    				$('.unit_kerja_pegawai_unit_kerja_id_'+(lvl+1)).slideDown('fast');
    			}else{
    				$('.unit_kerja_pegawai_unit_kerja_id_'+(lvl+1)).slideUp('fast');
    			}
    			if(attrid > 1){
    				$('#unit_kerja_pegawai_unit_kerja_id_'+(lvl+1)).trigger('change');
    			}
    		}
    	});
    }
</script>
