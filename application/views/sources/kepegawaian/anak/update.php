<style type="text/css">
.control-label{
	font-weight: bold;
}
</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Data Anak</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nip"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tempat Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_tempat_lahir"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Jabatan</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama_jabatan"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Unit Kerja</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["unit_kerja_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<a href="<?php echo base_url();?>kepegawaian/anak" class="btn btn-xs btn-warning">Kembali</a></td>

							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Daftar Data Anak</span>
					<div class="col-md-12">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
							<thead>
								<tr>
									<th class="text-center">Nama Anak</th>
									<th class="text-center">Jenis Kelamin</th>									
									<th class="text-center">Tempat & Tanggal Lahir</th>
									<!-- <th class="text-center">Tanggal Lahir</th> -->
									<th class="text-center">Usia</th>
									<!-- <th class="text-center">Pendidikan</th> -->
									<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($history)>0){?>
								<?php foreach ($history as $h) {  ?>
								<tr>
									<td><?php echo $h["anak_pegawai_nama"];?></td>
									<td><?php echo $h["jenis_kelamin_nama"];?></td>
									<td><?php echo $h["anak_tempat_lahir"];?>, <?php echo $h["anak_tanggal_lahir"]; ?></td>
									<!-- <td><?php echo $h["anak_tanggal_lahir"];?></td>	 -->
									<td><?php echo $h["anak_usia"];?></td>									
									<!-- <td><?php echo $h["pendidikan_nama"];?></td> -->
									<td>
										<ul class="icons-list">
											<li><a href="javascript:void(0)" onclick="update_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
											<li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>
											<li><a onclick="set_non_aktif(<?php echo $h["anak_id"];?>,<?php echo $h["anak_pegawai_id"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
										</ul>
									</td>
								</tr>
								<?php } ?>
								<?php }else{ ?>
								<tr>
									<td class="text-center" colspan="9">Data Belum Tersedia</td>
								</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<td class="text-right" colspan="9"><a href="javascript:void(0)" onclick="tambah_data();" class="btn btn-xs btn-primary btn-tfoot-table">Tambah Data</a>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="col-md-12 form_tambah_data" style="display: none;">
						<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan Data Anak</span>
						<div class="col-md-12">

							<div class="col-md-12">
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Nama Anak</label>
									<div class="col-md-9">
										<input type="text" name="anak_pegawai_nama" id="anak_pegawai_nama" class="form-control input-xs wajib" placeholder="Nama Anak">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Jenis Kelamin</label>
									<div class="col-md-9">
										<select name="anak_jenis_kelamin_id" id="anak_jenis_kelamin_id" data-placeholder="Jenis Kelamin" class="select-size-xs wajib" >
											<option value=""></option>
											<?php foreach ($listjk as $i) {
												echo '<option value="'.$i["jenis_kelamin_id"].'" >'.$i["jenis_kelamin_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tempat/Tanggal Lahir</label>
									<div class="col-md-2">
										<input type="text" name="anak_tempat_lahir" id="anak_tempat_lahir" class="form-control input-xs wajib" placeholder="Tempat Lahir">
									</div>
									<div class="col-md-4">
										<input type="text" name="anak_tanggal_lahir" id="anak_tanggal_lahir" class="form-control input-xs wajib pickttl" placeholder="Tanggal Lahir">
									</div>
								</div>
								<!-- <div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Usia</label>
									<div class="col-md-3">
										<input type="text" name="anak_usia_tahun" id="anak_usia_tahun" class="form-control input-xs wajib">
											<span class="help-block">Tahun</span>
									</div>
									<div class="col-md-3">
										<input type="text" name="anak_usia_bulan" id="anak_usia_bulan" class="form-control input-xs wajib">
											<span class="help-block">Bulan</span>
									</div>
								</div> -->
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Status anak</label>
									<div class="col-md-9">
										<select name="anak_jenis_anak" id="anak_jenis_anak" data-placeholder="Status anak" class="select-size-xs wajib" >
											<option value=""></option>
											<option value="1">Anak kandung</option>
											<option value="2">Anak Tiri</option>
											<option value="3">Anak Angkat</option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Pekerjaan</label>
									<div class="col-md-9">
										<select name="anak_pekerjaan" id="anak_pekerjaan" data-placeholder="Pekerjaan" class="select-size-xs wajib" >
											<option value=""></option>
											<option value="1">Sudah Bekerja</option>
											<option value="2">Belum Bekerja</option>
										</select>
									</div>
								</div>
								<!-- <div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Pendidikan</label>
									<div class="col-md-9">
										<select name="anak_pendidikan_id" id="anak_pendidikan_id" data-placeholder="Pendidikan" class="select-size-xs wajib" >
											<option value=""></option>
											<?php foreach ($pendidikan as $i) {
												echo '<option value="'.$i["pendidikan_id"].'" >'.$i["pendidikan_nama"].'</option>';
											}?>
										</select>
									</div>
								</div> -->
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Masuk KP4</label>
									<div class="col-md-9">
										<select name="anak_masuk_kp4" id="anak_masuk_kp4" data-placeholder="Masuk KP4" class="select-size-xs wajib" >
											<option value=""></option>
											<option value="1">Ya</option>
											<option value="2">Tidak</option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Kondisi</label>
									<div class="col-md-9">
										<select name="anak_kondisi" id="anak_kondisi" data-placeholder="Kondisi" class="select-size-xs wajib" >
											<option value=""></option>
											<option value="1">Masih Hidup</option>
											<option value="2">Almarhum / Almarhumah</option>
										</select>
									</div>
								</div>								
								<div class="form-group form-group-xs">
									<div class="col-md-9 col-md-offset-3">
										<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
										<input type="hidden" name="anak_pegawai_id" id="anak_pegawai_id" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_id"];?>">
										<input type="hidden" name="anak_pegawai_nip" id="anak_pegawai_nip" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_nip"];?>">
										<input type="hidden" name="isUpdate" id="isUpdate" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="anak_id" id="anak_id" class="form-control input-xs wajib" value="0">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 form_lihat_data" style="display: none;">
						<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Lihat Data Anak</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Nama Anak</label>
									<div class="col-md-9">
										<div class="form-control-static anak_pegawai_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Jenis Kelamin</label>
									<div class="col-md-9">
										<div class="form-control-static jenis_kelamin_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tempat / Tanggal Lahir</label>
									<div class="col-md-9">
										<div class="form-control-static anak_ttl">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Usia</label>
									<div class="col-md-9">
										<div class="form-control-static anak_usia">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-lg-3 control-label">Status anak</label>
									<div class="col-lg-9">
										<div class="form-control-static anak_jenis_anak_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Pekerjaan</label>
									<div class="col-md-9">
										<div class="form-control-static anak_pekerjaan_nama">-</div>
									</div>
								</div>
								<!-- <div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Pendidikan</label>
									<div class="col-md-9">
										<div class="form-control-static pendidikan_nama">-</div>
									</div>
								</div> -->
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Masuk KP4</label>
									<div class="col-md-9">
										<div class="form-control-static anak_masuk_kp4_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Kondisi</label>
									<div class="col-md-9">
										<div class="form-control-static anak_kondisi_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<div class="col-md-9 col-md-offset-3">
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>
	<script type="text/javascript">
		function set_non_aktif(id,pegawai_id){
			$('#text_konfirmasi').html('Anda yakin menghapus data ini ?');
			$('#konfirmasipenyimpanan').modal('show');
			$('#setujukonfirmasibutton').unbind();
			$('#setujukonfirmasibutton').on('click', function () {
				$('#konfirmasipenyimpanan').modal('hide');
				$('.angkadoank').inputmask('remove');
				$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
				window.location.href = base_url+'kepegawaian/anak/hapus/'+id+'/'+pegawai_id;
			});
		}
		function lihat_data(data){
			$('.form_tambah_data').hide();
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					lihat_data(data);
				});
			}else{
				$('.form_lihat_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_lihat_data").offset().top-60
					}, 500);

					var anak_pegawai_nama = datajson.anak_pegawai_nama;
					var jenis_kelamin_nama = datajson.jenis_kelamin_nama;
					var anak_tempat_lahir = datajson.anak_tempat_lahir;
					var anak_tanggal_lahir = datajson.anak_tanggal_lahir;
					var anak_usia_tahun = datajson.anak_usia_tahun;
					var anak_usia_bulan = datajson.anak_usia_bulan;
					var anak_jenis_anak_nama = datajson.anak_jenis_anak_nama;
					var anak_pekerjaan_nama = datajson.anak_pekerjaan_nama;
					// var pendidikan_nama = datajson.pendidikan_nama;
					var anak_masuk_kp4_nama = datajson.anak_masuk_kp4_nama;
					var anak_kondisi_nama = datajson.anak_kondisi_nama;

					$(".anak_pegawai_nama").html(anak_pegawai_nama);
					$(".jenis_kelamin_nama").html(jenis_kelamin_nama);
					$(".anak_ttl").html(anak_tempat_lahir+', '+ anak_tanggal_lahir);
					$(".anak_usia").html(anak_usia_tahun+' Tahun '+ anak_usia_bulan+' Bulan');
					$(".anak_jenis_anak_nama").html(anak_jenis_anak_nama);
					$(".anak_pekerjaan_nama").html(anak_pekerjaan_nama);
					// $(".pendidikan_nama").html(pendidikan_nama);
					$(".anak_masuk_kp4_nama").html(anak_masuk_kp4_nama);
					$(".anak_kondisi_nama").html(anak_kondisi_nama);
					
				});
			}
		}
		function update_data(data){
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			
			$('.form_lihat_data').hide();
			$('.label_aksi_form').html('Form Perubahan Data Anak');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					update_data(data);
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
					$('#isUpdate').val(1);

					var anak_id = datajson.anak_id; 

					var anak_pegawai_nama = datajson.anak_pegawai_nama;
					var anak_jenis_kelamin_id = datajson.anak_jenis_kelamin_id;
					var anak_tempat_lahir = datajson.anak_tempat_lahir;
					var anak_tanggal_lahir = datajson.anak_tanggal_lahir;
					// var anak_usia_tahun = datajson.anak_usia_tahun;
					// var anak_usia_bulan = datajson.anak_usia_bulan;
					var anak_jenis_anak = datajson.anak_jenis_anak;
					var anak_pekerjaan = datajson.anak_pekerjaan;
					// var anak_pendidikan_id = datajson.anak_pendidikan_id;
					var anak_masuk_kp4 = datajson.anak_masuk_kp4;
					var anak_kondisi  = datajson.anak_kondisi ;

					//alert(Tampil);

					$("#anak_id").val(anak_id);
					$("#anak_pegawai_nama").val(anak_pegawai_nama);
					$("#anak_jenis_kelamin_id").val(anak_jenis_kelamin_id);
					$('#anak_jenis_kelamin_id').trigger('change');
					$("#anak_tempat_lahir").val(anak_tempat_lahir);
					$("#anak_tanggal_lahir").val(anak_tanggal_lahir);
					// $("#anak_usia_tahun").val(anak_usia_tahun);
					// $("#anak_usia_bulan").val(anak_usia_bulan);
					$("#anak_jenis_anak").val(anak_jenis_anak);
					$('#anak_jenis_anak').trigger('change');
					$("#anak_pekerjaan").val(anak_pekerjaan);
					$('#anak_pekerjaan').trigger('change');
					// $("#anak_pendidikan_id").val(anak_pendidikan_id);
					// $('#anak_pendidikan_id').trigger('change');
					$("#anak_masuk_kp4").val(anak_masuk_kp4);
					$('#anak_masuk_kp4').trigger('change');
					$("#anak_kondisi").val(anak_kondisi);
					$('#anak_kondisi').trigger('change');
					
				});
			}
		}

		function tambah_data(){
			$('.form_lihat_data').hide();
			reset_form();
			$('.label_aksi_form').html('Form Penambahan Data Anak');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					$('.form_tambah_data').slideDown('fast',function(){
						$('html, body').animate({
							scrollTop: $(".form_tambah_data").offset().top-60
						}, 500);
						
					});
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
				});
			}
		}
		function batal_data(){
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					reset_form();
				});
			}
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					reset_form();
				});
			}
		}
		function reset_form(){
			// $("#anak_id").val('');
			// $("#anak_pegawai_id").val('');
			$("#anak_id").val('');
			$("#anak_pegawai_nama").val('');
			$("#anak_jenis_kelamin_id").val('');
			$('#anak_jenis_kelamin_id').trigger('change');
			$("#anak_tempat_lahir").val('');
			$("#anak_tanggal_lahir").val('');
			// $("#anak_usia_tahun").val('');
			// $("#anak_usia_bulan").val('');
			$("#anak_jenis_anak").val('');
			$('#anak_jenis_anak').trigger('change');
			$("#anak_pekerjaan").val('');
			$('#anak_pekerjaan').trigger('change');
			// $("#anak_pendidikan_id").val('');
			// $('#anak_pendidikan_id').trigger('change');
			$("#anak_masuk_kp4").val('');
			$('#anak_masuk_kp4').trigger('change');
			$("#anak_kondisi").val('');
			$('#anak_kondisi').trigger('change');
			$('.validation-error-label').css('display','none');
		}
		
		$(document).ready(function() {

			var d = new Date();

			$('.pickttl').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});
			$('.pickttlstart').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});

			$('.pickttlend').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});

		//On value change
		$(".pickttlstart").change(function(e) {
	    	//var date = rangeDemoConv.parse($(".pickttlstar").val()).getTime();
	    	//alert(date);
	    	//min: new Date(2015,3,20),

	    	var mulai1 = $("#diklat_tanggal_mulai").val().split(" ");
	    	var tgl = mulai1[0];
	    	var bulan = mulai1[1];
	    	var tahun = mulai1[2];

	    	$('.pickttlend').pickadate({
	    		monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
	    		weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
	    		selectMonths: true,
	    		selectYears: 80,
	    		min: new Date(tahun, 5, 20),
	    		max: true,
	    		today:'Hari ini',
	    		clear: 'Hapus',
	    		close: 'Keluar',
	    		format: 'dd mmmm yyyy',
	    		formatSubmit: 'yyyy-mm-dd'
	    	});

	    	var yesterday  = new Date((new Date()).valueOf()-1000*60*60*24);
	    	var neededDates = datas.prdetail.PRD_DATE_NEED;
	    	var neededDate = neededDates.split('-');

	    	var tglneed = $('#tanggal_dibutuhkan').pickadate({
	    		monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
	    		monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agt', 'Sep', 'Okt', 'Nov', 'Des'],
	    		weekdaysFull: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu'],
	    		weekdaysShort: ['Min', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
	    		selectYears: true,
	    		selectMonths: true,
	    		format: 'd mmmm yyyy',
	    		formatSubmit: 'yyyy-mm-dd',
	    		today: 'Hari ini',
	    		clear: 'Reset',
	    		close: 'Batal',
	    		disable: [
	    		{ from: [0,0,0], to: yesterday }
	    		]
	    	});

	    	var dpicker = tglneed.pickadate('picker');
	    	dpicker.set('select', [neededDate[0], (neededDate[1]-1), neededDate[2]]);
	        // try {
	        //     var fromDay = rangeDemoConv.parse($("#rangeDemoStart").val()).getTime();
	        //     var dayLater = new Date(fromDay+oneDay);
	        //         dayLater.setHours(0,0,0,0);
	        //     var ninetyDaysLater = new Date(fromDay+(90*oneDay));
	        //         ninetyDaysLater.setHours(23,59,59,999);

	        //     // End date
	        //     $("#rangeDemoFinish")
	        //     .AnyTime_noPicker()
	        //     .removeAttr("disabled")
	        //     .val(rangeDemoConv.format(dayLater))
	        //     .AnyTime_picker({
	        //         earliest: dayLater,
	        //         format: rangeDemoFormat,
	        //         latest: ninetyDaysLater
	        //     });
	        // }
	        // catch(e) {
	        //     // Disable End date field
	        //     $("#rangeDemoFinish").val("").attr("disabled","disabled");
	        // }
	    });

	});

function ToJavaScriptDateViewDoank(value) {
        //var pattern = /Date\(([^)]+)\)/;
        //var results = pattern.exec(value);
        //var dt = new Date(parseFloat(results[1]));
        var MonthInd = GetMonthIndInt((value.getMonth() + 1));
        return value.getDate() + " " + MonthInd + " " + value.getFullYear();
    }

    function GetMonthIndInt(month) {
    	switch (month) {
    		case 1:
    		return "Januari"
    		break;
    		case 2:
    		return "Februari"
    		break;
    		case 3:
    		return "Maret"
    		break;
    		case 4:
    		return "April"
    		break;
    		case 5:
    		return "Mei"
    		break;
    		case 6:
    		return "Juni"
    		break;
    		case 7:
    		return "Juli"
    		break;
    		case 8:
    		return "Agustus"
    		break;
    		case 9:
    		return "September"
    		break;
    		case 10:
    		return "Oktober"
    		break;
    		case 11:
    		return "November"
    		break;
    		case 12:
    		return "Desember"
    		break;
    		default:
    		return "Undefined"
    	}
    }
</script>
