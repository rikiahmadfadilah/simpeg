<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Berkas Elektronik</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nip"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tempat Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_tempat_lahir"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Jabatan</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama_jabatan"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Unit Kerja</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["unit_kerja_hirarki_name_full"];?></div><br>
							</div>
						</div><br>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<a href="<?php echo base_url();?>kepegawaian/berkas_elektronik" class="btn btn-xs btn-warning">Kembali</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Daftar Data Berkas Elektronik</span>
					<div class="col-md-12">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
							<thead>
								<tr>
									<th class="text-center">Nama Dokumen</th>
									<th class="text-center">Jenis Dokumen</th>
									<th class="text-center">File</th>
									<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($history)>0){?>
									<?php $no = 1; foreach ($history as $h) {  ?>
										<tr>
											<td><?php echo $h["berkas_nama_dokumen"];?></td>
											<td><?php if($h["jenis_dokumen_name"] !=''){echo $h["jenis_dokumen_name"];}else{echo $h["berkas_nama_dokumen"];}?></td>
											<td><a href="<?php echo base_url().'assets/berkas_elektronik/'.$h["berkas_path"];?>" target="_blank"><?php echo $h["berkas_file"];?></a></td>
											<td>
												<ul class="icons-list">
													<li><a href="javascript:void(0)" onclick="update_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
													<li><a onclick="set_non_aktif(<?php echo $h["berkas_id"];?>,<?php echo $h["berkas_pegawai_id"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
												</ul>
											</td>
										</tr>
									<?php $no++; } ?>
								<?php }else{ ?>
									<tr>
										<td class="text-center" colspan="7">Data Belum Tersedia</td>
									</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<td class="text-right" colspan="7"><a href="<?php echo base_url('kepegawaian/berkas_elektronik/get_folder_berkas_elektronik/'.base64_encode($pegawai["pegawai_nip_lama"])) ?>" class="btn btn-xs btn-success btn-tfoot-table">Efile Lama</a> <a href="<?php echo base_url('kepegawaian/berkas_elektronik/upload_berkas_elektronik/'.$pegawai["pegawai_id"]) ?>" class="btn btn-xs btn-primary btn-tfoot-table">Tambah Data</a>
								</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="col-md-12 form_tambah_data" style="display: none;">
						<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Edit Berkas Elektronik</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Nama Dokumen</label>
									<div class="col-md-9">
										<input type="text" name="berkas_nama_dokumen" id="berkas_nama_dokumen" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Jenis Dokumen</label>
									<div class="col-md-9">
										<select name="berkas_jenis_dokumen_id" id="berkas_jenis_dokumen_id" data-placeholder="Status Belajar" class="select-size-xs wajib">
											<option value=""></option>
											<?php foreach ($jenis_dokumen as $jd) {
												echo '<option value="'.$jd["jenis_dokumen_id"].'">'.$jd["jenis_dokumen_name"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<div class="col-md-9 col-md-offset-3">
										<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
										<input type="hidden" name="pegawai_id" id="pegawai_id" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_id"];?>">
										<input type="hidden" name="pegawai_nip" id="pegawai_nip" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_nip"];?>">
										<input type="hidden" name="isUpdate" id="isUpdate" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="berkas_id" id="berkas_id" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_nip"];?>">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
		function set_non_aktif(id,pegawai_id){
			$('#text_konfirmasi').html('Anda yakin menghapus data ini ?');
			$('#konfirmasipenyimpanan').modal('show');
			$('#setujukonfirmasibutton').unbind();
			$('#setujukonfirmasibutton').on('click', function () {
				$('#konfirmasipenyimpanan').modal('hide');
				$('.angkadoank').inputmask('remove');
				$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
				window.location.href = base_url+'kepegawaian/berkas_elektronik/hapus/'+id+'/'+pegawai_id;
			});
		}
		function update_data(data){
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			// reset_form();
			$('.label_aksi_form').html('Form Perubahan Data Berkas Elektronik');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					update_data(data);
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
					$('#isUpdate').val(1);
					var berkas_id = datajson.berkas_id;
					var pegawai_id = datajson.pegawai_id;
					var pegawai_nip = datajson.pegawai_nip;
					var berkas_nama_dokumen = datajson.berkas_nama_dokumen;
					var berkas_jenis_dokumen_id = datajson.berkas_jenis_dokumen_id;

					$('#berkas_id').val(berkas_id);
					$('#pegawai_id').val(pegawai_id);
					$('#pegawai_nip').val(pegawai_nip);
					$('#berkas_nama_dokumen').val(berkas_nama_dokumen);

					$('#berkas_jenis_dokumen_id').select2('val', '');
					$('#berkas_jenis_dokumen_id').val(berkas_jenis_dokumen_id);
					$('#berkas_jenis_dokumen_id').trigger('change');
					// $('#berkas_jenis_dokumen_id').val(berkas_jenis_dokumen_id);

				});
			}
		}
		function batal_data(){
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					// reset_form();
				});
			}
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					// reset_form();
				});
			}
		}
	</script>
