<script src="<?php echo base_url();?>assets/js/plugins/forms/wizards/steps.min.js"></script>
<style type="text/css">
.control-label{
	font-weight: bold;
}
.select2-result-repository__avatar img{
	border-radius: 0;
}
.datatable-header{
	padding: 0 20px 0 20px;
}
</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Data Pegawai</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nip"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tempat Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_tempat_lahir"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Jabatan</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama_jabatan"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Unit Kerja</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["unit_kerja_hirarki_name_full"];?></div><br>
							</div>
						</div><br>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-md-12 form_tambah_data" >
					<div class="col-md-12">
						<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Upload File</span>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Nama Dokumen</label>
							<div class="col-md-9">
								<div class="clearfix"></div>
								<input type="text" name="berkas_nama_dokumen" id="berkas_nama_dokumen" class="form-control input-xs wajib">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Jenis Dokumen</label>
							<div class="col-md-9">
								<select name="berkas_jenis_dokumen_id" id="berkas_jenis_dokumen_id" data-placeholder="Jenis Dokumen" class="select-size-xs wajib">
									<option value=""></option>
									<?php foreach ($jenis_dokumen as $jd) {
										echo '<option value="'.$jd["jenis_dokumen_id"].'">'.$jd["jenis_dokumen_name"].'</option>';
									}?>
								</select>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<label class="col-md-3 control-label">Upload File</label>
							<div class="col-md-9">
								<input type="file" name="berkas_path" id="berkas_path" class="form-control input-xs wajib" style="padding-top: 0px !important;padding-bottom: 0px !important;" accept="image/jpeg,image/gif,image/png,image/jpg,application/pdf,image/x-eps">
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<input type="hidden" name="pegawai_nip" id="pegawai_nip" class="form-control input-xs wajib" value="<?php echo $pegawai['pegawai_nip'] ?>">
								<button type="submit" class="btn btn-xs btn-primary">Simpan</button>
								<a href="<?php echo base_url()?>kepegawaian/berkas_elektronik/update/<?php echo $pegawai["pegawai_id"]?>" class="btn btn-xs btn-danger">Batal</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
</script>
