<script src="<?php echo base_url();?>assets/js/plugins/forms/wizards/steps.min.js"></script>
<style type="text/css">
.control-label{
	font-weight: bold;
}
.select2-result-repository__avatar img{
	border-radius: 0;
}
.datatable-header{
	padding: 0 20px 0 20px;
}
</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">E-File Lama</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="clearfix"></div>
				<div class="col-md-12 form_tambah_data" >
					<div class="col-md-12">
						<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Data E-File Lama</span>
						<table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive epagawai_table" id="list_data">
							<thead>
								<tr>
									<th class="text-center"style="width: 175px;min-width: 175px;">File</th>
								</tr>
							</thead>
							<tbody>
								<?php if(!empty($data_folder)){ ?>
								<?php foreach ($data_folder as $df) { ?>
								<tr>
									<?php if($df == '.'){}else if($df == '..'){}else{ ?>
									<td><a href="<?php echo base_url('efile/'.$nip.'/'.$df); ?>" target="blank"><?php echo $df; ?></a></td>
									<?php } ?>
								</tr>
								<?php } ?>
								<?php }else{ ?>
									<tr>
										<td><center>Belum ada dokumen yang sudah diupload.</center></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
						<!-- <div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<a href="<?php echo base_url('kepegawaian/berkas_elektronik/update/'.base64_encode($nip));?>" class="btn btn-xs btn-danger">Kembali</a>
							</div>
						</div> -->
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
</script>
