<style type="text/css">
.control-label{
	font-weight: bold;
}
</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Riwayat Jabatan Fungsional</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nip"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tempat Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_tempat_lahir"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Jabatan</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama_jabatan"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Unit Kerja</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["unit_kerja_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<a href="<?php echo base_url();?>kepegawaian/jabatan" class="btn btn-xs btn-warning">Kembali</a></td>

							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Daftar Riwayat Jabatan Fungsional</span>
					<div class="col-md-12">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
							<thead>
								<tr>
									<th class="text-center">Unit Kerja</th>
									<th class="text-center">Jabatan Fungsional</th>									
									<th class="text-center">TMT Jab. Fungsional</th>
									<th class="text-center">Nomor SK</th>
									<th class="text-center">Tanggal SK</th>
									<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($history)>0){?>
								<?php foreach ($history as $h) {  ?>
								<tr>
									<td><?php echo $h["jenjang_jabatan_fungsional_unit_kerja"];?></td>
									<td><?php echo $h["fungsional_jenjang"];?></td>
									<td><?php echo $h["jenjang_jabatan_fungsional_tmt_jab_fung"];?></td>									
									<td><?php echo $h["jenjang_jabatan_fungsional_nomor_sk"];?></td>
									<td><?php echo $h["jenjang_jabatan_fungsional_tanggal_sk"];?></td>
									<td>
										<ul class="icons-list">
											<li><a href="javascript:void(0)" onclick="update_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
											<li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>
											<li><a onclick="set_non_aktif(<?php echo $h["jenjang_jabatan_fungsional_id"];?>,<?php echo $h["jenjang_jabatan_fungsional_pegawai_id"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
										</ul>
									</td>
								</tr>
								<?php } ?>
								<?php }else{ ?>
								<tr>
									<td class="text-center" colspan="9">Data Belum Tersedia</td>
								</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<td class="text-right" colspan="9"><a href="javascript:void(0)" onclick="tambah_data();" class="btn btn-xs btn-primary btn-tfoot-table">Tambah Data</a>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="col-md-12 form_tambah_data" style="display: none;">
						<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan Data Jenjang Jabatan Fungsional</span>
						<div class="col-md-12">

							<div class="col-md-12">
								<!-- <div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Unit Kerja</label>
									<div class="col-lg-9">
										<select name="jenjang_jabatan_fungsional_unit_kerja[]" id="jenjang_jabatan_fungsional_unit_kerja_1" data-placeholder="Unit Kerja" class="select-size-xs ga_wajib" onchange="get_unit_kerja_hirarki(this,1)">
											<option></option>
											<?php foreach ($unit_kerja as $i) {
												echo '<option value="'.$i["unit_kerja_id"].'" parent_id_kode="'.$i["unit_kerja_id_kode"].'" jabatan_id="'.$i["jabatan_id"].'">'.$i["unit_kerja_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs jenjang_jabatan_fungsional_unit_kerja_2" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="jenjang_jabatan_fungsional_unit_kerja[]" id="jenjang_jabatan_fungsional_unit_kerja_2" data-placeholder="Unit Kerja" class="select-size-xs"  onchange="get_unit_kerja_hirarki(this,2)">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs jenjang_jabatan_fungsional_unit_kerja_3" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="jenjang_jabatan_fungsional_unit_kerja[]" id="jenjang_jabatan_fungsional_unit_kerja_3" data-placeholder="Unit Kerja" class="select-size-xs"  onchange="get_unit_kerja_hirarki(this,3)">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs jenjang_jabatan_fungsional_unit_kerja_4" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="jenjang_jabatan_fungsional_unit_kerja[]" id="jenjang_jabatan_fungsional_unit_kerja_4" data-placeholder="Unit Kerja" class="select-size-xs"  onchange="get_unit_kerja_hirarki(this,4)">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs jenjang_jabatan_fungsional_unit_kerja_5" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="jenjang_jabatan_fungsional_unit_kerja[]" id="jenjang_jabatan_fungsional_unit_kerja_5" data-placeholder="Unit Kerja" class="select-size-xs" onchange="get_unit_kerja_hirarki(this,5)">
											<option></option>
										</select>
									</div>
								</div> -->

								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Unit Kerja</label>
									<div class="col-md-9">
										<input type="text" name="jenjang_jabatan_fungsional_unit_kerja" id="jenjang_jabatan_fungsional_unit_kerja" class="form-control input-xs wajib">
									</div>
								</div>

								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Jabatan Fungsional</label>
									<div class="col-lg-9">
										<select name="jenjang_jabatan_fungsional_fungsional_id" id="jenjang_jabatan_fungsional_fungsional_id" data-placeholder="Pilih Jabatan Fungsional" class="select-size-xs ga_wajib">
											<option></option>
											<?php foreach ($jabatanfungsional as $key) { ?>
											<optgroup label="<?php echo $key["fungsional_kode"].' - '.$key["fungsional_nama"] ?>">
												<?php foreach ($jabatanfungsionalchild as $v) { ?>
												<?php if($key["kode"] == $v["kode"]){?>
												<option value="<?php echo $v["fungsional_id"] ?>"><?php echo $v["fungsional_kode"].' - '.$v["fungsional_nama"].' - '.$v["fungsional_jenjang"] ?></option>
												<?php } ?>
												<?php } ?>
												<?php } ?>											
											</select>
										</div>
									</div>

									<div class="form-group form-group-xs">
										<label class="col-md-3 control-label">TMT Jabatan Fungsional</label>
										<div class="col-md-9">
											<input type="text" name="jenjang_jabatan_fungsional_tmt_jab_fung" id="jenjang_jabatan_fungsional_tmt_jab_fung" class="form-control input-xs wajib pickttl">
										</div>
									</div>

									<div class="form-group form-group-xs">
										<label class="col-md-3 control-label">Nomor SK</label>
										<div class="col-md-9">
											<input type="text" name="jenjang_jabatan_fungsional_nomor_sk" id="jenjang_jabatan_fungsional_nomor_sk" class="form-control input-xs wajib">
										</div>
									</div>

									<div class="form-group form-group-xs">
										<label class="col-md-3 control-label">Tanggal SK</label>
										<div class="col-md-9">
											<input type="text" name="jenjang_jabatan_fungsional_tanggal_sk" id="jenjang_jabatan_fungsional_tanggal_sk" class="form-control input-xs wajib pickttl">
										</div>
									</div>

									<div class="form-group form-group-xs">
										<label class="col-md-3 control-label">Angka Kredit</label>
										<div class="col-md-9">
											<input type="text" name="jenjang_jabatan_fungsional_angka_kredit" id="jenjang_jabatan_fungsional_angka_kredit" class="form-control input-xs wajib">
										</div>
									</div>

									<div class="form-group form-group-xs">
										<div class="col-md-9 col-md-offset-3">
											<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
											<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
											<input type="hidden" name="jenjang_jabatan_fungsional_pegawai_id" id="jenjang_jabatan_fungsional_pegawai_id" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_id"];?>">
											<input type="hidden" name="jenjang_jabatan_fungsional_pegawai_nip" id="jenjang_jabatan_fungsional_pegawai_nip" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_nip"];?>">
											<input type="hidden" name="isUpdate" id="isUpdate" class="form-control input-xs wajib" value="0">
											<input type="hidden" name="jenjang_jabatan_fungsional_id" id="jenjang_jabatan_fungsional_id" class="form-control input-xs wajib" value="0">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 form_lihat_data" style="display: none;">
							<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Lihat Data Jenjang Jabatan Fungsional</span>
							<div class="col-md-12">
								<div class="col-md-12">
									<div class="form-group form-group-xs lihat_data_statik">
										<label class="col-md-3 control-label">Unit Kerja</label>
										<div class="col-md-9">
											<div class="form-control-static jenjang_jabatan_fungsional_unit_kerja">-</div>
										</div>
									</div>
									<div class="form-group form-group-xs lihat_data_statik">
										<label class="col-md-3 control-label">Jabatan Fungsional</label>
										<div class="col-md-9">
											<div class="form-control-static fungsional_jenjang">-</div>
										</div>
									</div>
									<div class="form-group form-group-xs lihat_data_statik">
										<label class="col-md-3 control-label">TMT Jabatan Fungsional</label>
										<div class="col-md-9">
											<div class="form-control-static jenjang_jabatan_fungsional_tmt_jab_fung">-</div>
										</div>
									</div>
									<div class="form-group form-group-xs lihat_data_statik">
										<label class="col-lg-3 control-label">Nomor SK</label>
										<div class="col-lg-9">
											<div class="form-control-static jenjang_jabatan_fungsional_nomor_sk">-</div>
										</div>
									</div>
									<div class="form-group form-group-xs lihat_data_statik">
										<label class="col-md-3 control-label">Tanggal SK</label>
										<div class="col-md-9">
											<div class="form-control-static jenjang_jabatan_fungsional_tanggal_sk">-</div>
										</div>
									</div>
									<div class="form-group form-group-xs lihat_data_statik">
										<label class="col-md-3 control-label">Angka Kredit</label>
										<div class="col-md-9">
											<div class="form-control-static jenjang_jabatan_fungsional_angka_kredit">-</div>
										</div>
									</div>
									<div class="form-group form-group-xs lihat_data_statik">
										<div class="col-md-9 col-md-offset-3">
											<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</form>
			</div>
		</div>
		<script type="text/javascript">
			function set_non_aktif(id,pegawai_id){
				$('#text_konfirmasi').html('Anda yakin menghapus data ini ?');
				$('#konfirmasipenyimpanan').modal('show');
				$('#setujukonfirmasibutton').unbind();
				$('#setujukonfirmasibutton').on('click', function () {
					$('#konfirmasipenyimpanan').modal('hide');
					$('.angkadoank').inputmask('remove');
					$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
					window.location.href = base_url+'kepegawaian/kepangkatan/hapus/'+id+'/'+pegawai_id;
				});
			}
			function lihat_data(data){
				$('.form_tambah_data').hide();
				var decoded = $("<div/>").html(data).text();
				var datajson = $.parseJSON(decoded);
				reset_form();
				if($('.form_lihat_data').is(":visible")){
					$('.form_lihat_data').slideUp('slow',function(){
						lihat_data(data);
					});
				}else{
					$('.form_lihat_data').slideDown('fast',function(){
						$('html, body').animate({
							scrollTop: $(".form_lihat_data").offset().top-60
						}, 500);

						var jenjang_jabatan_fungsional_unit_kerja = datajson.jenjang_jabatan_fungsional_unit_kerja;
						var fungsional_jenjang = datajson.fungsional_jenjang;
						var jenjang_jabatan_fungsional_tmt_jab_fung = datajson.jenjang_jabatan_fungsional_tmt_jab_fung;
						var jenjang_jabatan_fungsional_nomor_sk = datajson.jenjang_jabatan_fungsional_nomor_sk;
						var jenjang_jabatan_fungsional_tanggal_sk = datajson.jenjang_jabatan_fungsional_tanggal_sk;
						var jenjang_jabatan_fungsional_angka_kredit = datajson.jenjang_jabatan_fungsional_angka_kredit;

						$(".jenjang_jabatan_fungsional_unit_kerja").html(jenjang_jabatan_fungsional_unit_kerja);
						$(".fungsional_jenjang").html(fungsional_jenjang);
						$(".jenjang_jabatan_fungsional_tmt_jab_fung").html(jenjang_jabatan_fungsional_tmt_jab_fung);
						$(".jenjang_jabatan_fungsional_nomor_sk").html(jenjang_jabatan_fungsional_nomor_sk);
						$(".jenjang_jabatan_fungsional_tanggal_sk").html(jenjang_jabatan_fungsional_tanggal_sk);
						$(".jenjang_jabatan_fungsional_angka_kredit").html(jenjang_jabatan_fungsional_angka_kredit);

					});
				}
			}
			function update_data(data){
				var decoded = $("<div/>").html(data).text();
				var datajson = $.parseJSON(decoded);
				reset_form();

				$('.form_lihat_data').hide();
				$('.label_aksi_form').html('Form Perubahan Data Riwayat Jabatan Fungsional');
				if($('.form_tambah_data').is(":visible")){
					$('.form_tambah_data').slideUp('slow',function(){
						update_data(data);
					});
				}else{
					$('.form_tambah_data').slideDown('fast',function(){
						$('html, body').animate({
							scrollTop: $(".form_tambah_data").offset().top-60
						}, 500);
						$('#isUpdate').val(1);

						var jenjang_jabatan_fungsional_id = datajson.jenjang_jabatan_fungsional_id;
						var jenjang_jabatan_fungsional_unit_kerja = datajson.jenjang_jabatan_fungsional_unit_kerja;
						var jenjang_jabatan_fungsional_fungsional_id = datajson.jenjang_jabatan_fungsional_fungsional_id;
						var jenjang_jabatan_fungsional_tmt_jab_fung = datajson.jenjang_jabatan_fungsional_tmt_jab_fung;
						var jenjang_jabatan_fungsional_nomor_sk = datajson.jenjang_jabatan_fungsional_nomor_sk;
						var jenjang_jabatan_fungsional_tanggal_sk = datajson.jenjang_jabatan_fungsional_tanggal_sk;
						var jenjang_jabatan_fungsional_angka_kredit = datajson.jenjang_jabatan_fungsional_angka_kredit;

						$("#jenjang_jabatan_fungsional_id").val(jenjang_jabatan_fungsional_id);
						$("#jenjang_jabatan_fungsional_unit_kerja").val(jenjang_jabatan_fungsional_unit_kerja);
						$("#jenjang_jabatan_fungsional_fungsional_id").val(jenjang_jabatan_fungsional_fungsional_id);
						$("#jenjang_jabatan_fungsional_fungsional_id").trigger('change');
						$("#jenjang_jabatan_fungsional_tmt_jab_fung").val(jenjang_jabatan_fungsional_tmt_jab_fung);
						$("#jenjang_jabatan_fungsional_nomor_sk").val(jenjang_jabatan_fungsional_nomor_sk);
						$("#jenjang_jabatan_fungsional_tanggal_sk").val(jenjang_jabatan_fungsional_tanggal_sk);
						$("#jenjang_jabatan_fungsional_angka_kredit").val(jenjang_jabatan_fungsional_angka_kredit);

					});
				}
			}

			function tambah_data(){
				$('.form_lihat_data').hide();
				reset_form();
				$('.label_aksi_form').html('Form Penambahan Data Jenjang Jabatan Fungsional');
				if($('.form_tambah_data').is(":visible")){
					$('.form_tambah_data').slideUp('slow',function(){
						$('.form_tambah_data').slideDown('fast',function(){
							$('html, body').animate({
								scrollTop: $(".form_tambah_data").offset().top-60
							}, 500);

						});
					});
				}else{
					$('.form_tambah_data').slideDown('fast',function(){
						$('html, body').animate({
							scrollTop: $(".form_tambah_data").offset().top-60
						}, 500);
					});
				}
			}
			function batal_data(){
				if($('.form_tambah_data').is(":visible")){
					$('.form_tambah_data').slideUp('slow',function(){
						reset_form();
					});
				}
				if($('.form_lihat_data').is(":visible")){
					$('.form_lihat_data').slideUp('slow',function(){
						reset_form();
					});
				}
			}
			function reset_form(){
			// $("#jenjang_jabatan_fungsional_id").val('');
			// $("#jenjang_jabatan_fungsional_pegawai_id").val('');
			//$("#jenjang_jabatan_fungsional_pegawai_nip").val('');
			$("#jenjang_jabatan_fungsional_id").val('');
			$("#jenjang_jabatan_fungsional_unit_kerja").val('');
			$("#jenjang_jabatan_fungsional_fungsional_id").val('');
			$("#jenjang_jabatan_fungsional_fungsional_id").trigger('change');
			$("#jenjang_jabatan_fungsional_tmt_jab_fung").val('');
			$("#jenjang_jabatan_fungsional_nomor_sk").val('');
			$("#jenjang_jabatan_fungsional_tanggal_sk").val('');
			$("#jenjang_jabatan_fungsional_angka_kredit").val('');
			$('.validation-error-label').css('display','none');
		}
		
		$(document).ready(function() {
			$('.select-size-xs').select2();
			var d = new Date();

			$('.pickttl').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});
			$('.pickttlstart').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});

			$('.pickttlend').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});

		//On value change
		$(".pickttlstart").change(function(e) {

			var mulai1 = $("#diklat_tanggal_mulai").val().split(" ");
			var tgl = mulai1[0];
			var bulan = mulai1[1];
			var tahun = mulai1[2];

			$('.pickttlend').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				min: new Date(tahun, 5, 20),
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});

			var yesterday  = new Date((new Date()).valueOf()-1000*60*60*24);
			var neededDates = datas.prdetail.PRD_DATE_NEED;
			var neededDate = neededDates.split('-');

			var tglneed = $('#tanggal_dibutuhkan').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agt', 'Sep', 'Okt', 'Nov', 'Des'],
				weekdaysFull: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu'],
				weekdaysShort: ['Min', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectYears: true,
				selectMonths: true,
				format: 'd mmmm yyyy',
				formatSubmit: 'yyyy-mm-dd',
				today: 'Hari ini',
				clear: 'Reset',
				close: 'Batal',
				disable: [
				{ from: [0,0,0], to: yesterday }
				]
			});

			var dpicker = tglneed.pickadate('picker');
			dpicker.set('select', [neededDate[0], (neededDate[1]-1), neededDate[2]]);

		});

	});

		function ToJavaScriptDateViewDoank(value) {
        //var pattern = /Date\(([^)]+)\)/;
        //var results = pattern.exec(value);
        //var dt = new Date(parseFloat(results[1]));
        var MonthInd = GetMonthIndInt((value.getMonth() + 1));
        return value.getDate() + " " + MonthInd + " " + value.getFullYear();
    }

    function GetMonthIndInt(month) {
    	switch (month) {
    		case 1:
    		return "Januari"
    		break;
    		case 2:
    		return "Februari"
    		break;
    		case 3:
    		return "Maret"
    		break;
    		case 4:
    		return "April"
    		break;
    		case 5:
    		return "Mei"
    		break;
    		case 6:
    		return "Juni"
    		break;
    		case 7:
    		return "Juli"
    		break;
    		case 8:
    		return "Agustus"
    		break;
    		case 9:
    		return "September"
    		break;
    		case 10:
    		return "Oktober"
    		break;
    		case 11:
    		return "November"
    		break;
    		case 12:
    		return "Desember"
    		break;
    		default:
    		return "Undefined"
    	}
    }

    function  get_unit_kerja_hirarki(ini,lvl) {    	
    	if(lvl==1){
    		$('.jenjang_jabatan_fungsional_unit_kerja_5').slideUp('fast',function(){
    			$('#jenjang_jabatan_fungsional_unit_kerja_5').select2('val', '');
    			$('.jenjang_jabatan_fungsional_unit_kerja_4').slideUp('fast',function(){
    				$('#jenjang_jabatan_fungsional_unit_kerja_4').select2('val', '');
    				$('.jenjang_jabatan_fungsional_unit_kerja_3').slideUp('fast',function(){
    					$('#jenjang_jabatan_fungsional_unit_kerja_3').select2('val', '');
    					$('.jenjang_jabatan_fungsional_unit_kerja_2').slideUp('fast',function(){
    						$('#jenjang_jabatan_fungsional_unit_kerja_2').select2('val', '');
    					});
    				});
    			});
    		});
    	}else if(lvl==2){
    		$('.jenjang_jabatan_fungsional_unit_kerja_5').slideUp('fast',function(){
    			$('#jenjang_jabatan_fungsional_unit_kerja_5').select2('val', '');
    			$('.jenjang_jabatan_fungsional_unit_kerja_4').slideUp('fast',function(){
    				$('#jenjang_jabatan_fungsional_unit_kerja_4').select2('val', '');
    				$('.jenjang_jabatan_fungsional_unit_kerja_3').slideUp('fast',function(){
    					$('#jenjang_jabatan_fungsional_unit_kerja_3').select2('val', '');
    				});
    			});
    		});
    	}else if(lvl==3){
    		$('.jenjang_jabatan_fungsional_unit_kerja_5').slideUp('fast',function(){
    			$('#jenjang_jabatan_fungsional_unit_kerja_5').select2('val', '');
    			$('.jenjang_jabatan_fungsional_unit_kerja_4').slideUp('fast',function(){
    				$('#jenjang_jabatan_fungsional_unit_kerja_4').select2('val', '');
    			});
    		});
    	}else if(lvl==4){
    		$('.jenjang_jabatan_fungsional_unit_kerja_5').slideUp('fast',function(){
    			$('#jenjang_jabatan_fungsional_unit_kerja_5').select2('val', '');
    		});
    	}
    	var parent_id_kode  = $('option:selected', ini).attr('parent_id_kode');
    	var jabatan_id  = $('option:selected', ini).attr('jabatan_id');

    	$('#jenjang_jabatan_jabatan_id').select2('val', '');
    	$('#jenjang_jabatan_jabatan_id').val(jabatan_id);
    	$('#jenjang_jabatan_jabatan_id').trigger('change');

    	$.ajax({
    		url: base_url+'kepegawaian/pns/get_unit_kerja_hirarki',
    		type: "post",
    		dataType: 'json',
    		data: { unit_kerja_parent_id_kode: parent_id_kode, unit_kerja_level: (lvl+2)},
    		success: function (data) {

    			var unit_kerja = data.unit_kerja;
    			var newoption = "<option></option>";

    			for(var i = 0; i < unit_kerja.length; i ++){
    				newoption+='<option value="'+unit_kerja[i].unit_kerja_id+'" parent_id_kode="'+unit_kerja[i].unit_kerja_id_kode+'"  jabatan_id="'+unit_kerja[i].jabatan_id+'">'+unit_kerja[i].unit_kerja_nama+'</option>';
    			}
    			$('#jenjang_jabatan_fungsional_unit_kerja_'+(lvl+1)).html(newoption);
    			if(unit_kerja.length>0){
    				$('.jenjang_jabatan_fungsional_unit_kerja_'+(lvl+1)).slideDown('fast');
    			}else{
    				$('.jenjang_jabatan_fungsional_unit_kerja_'+(lvl+1)).slideUp('fast');

    			}
    		}
    	});
    }
</script>
