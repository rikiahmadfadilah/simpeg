<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Organisasi</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nip"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tempat Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_tempat_lahir"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Jabatan</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama_jabatan"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Unit Kerja</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["unit_kerja_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<a href="<?php echo base_url();?>kepegawaian/organisasi" class="btn btn-xs btn-warning">Kembali</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Daftar Data Organisasi</span>
					<div class="col-md-12">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
							<thead>
								<tr>
									<th class="text-center">Nama Organisasi</th>
									<th class="text-center">Kedudukan</th>
									<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($history)>0){?>
									<?php foreach ($history as $h) {  ?>
										<tr>
											<td><?php echo $h["organisasi_nama"];?></td>
											<td><?php echo $h["organisasi_kedudukan"];?></td>
											<td>
												<ul class="icons-list">
													<li><a href="javascript:void(0)" onclick="update_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
													<li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>
													<li><a onclick="set_non_aktif(<?php echo $h["organisasi_id"];?>,<?php echo $h["organisasi_pegawai_id"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
												</ul>
											</td>
										</tr>
									<?php } ?>
								<?php }else{ ?>
									<tr>
										<td class="text-center" colspan="7">Data Belum Tersedia</td>
									</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<td class="text-right" colspan="7"><a href="javascript:void(0)" onclick="tambah_data();" class="btn btn-xs btn-primary btn-tfoot-table">Tambah Data</a>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="col-md-12 form_tambah_data" style="display: none;">
						<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan Data Organisasi</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Nama Organisasi</label>
									<div class="col-md-9">
										<input type="text" name="organisasi_nama" id="organisasi_nama" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Kedudukan</label>
									<div class="col-md-9">
										<input type="text" name="organisasi_kedudukan" id="organisasi_kedudukan" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tanggal Mulai</label>
									<div class="col-md-9">
										<input type="text" name="organisasi_tanggal_mulai" id="organisasi_tanggal_mulai" class="form-control input-xs wajib pickorganisasi">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tanggal Selesai</label>
									<div class="col-md-9">
										<input type="text" name="organisasi_tanggal_selesai" id="organisasi_tanggal_selesai" class="form-control input-xs wajib pickorganisasi">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Nomor SK</label>
									<div class="col-md-9">
										<input type="text" name="organisasi_nomor_sk" id="organisasi_nomor_sk" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Jabatan</label>
									<div class="col-md-9">
										<input type="text" name="organisasi_nama_jabatan" id="organisasi_nama_jabatan" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tahun</label>
									<div class="col-md-9">
										<select name="organisasi_tahun" id="organisasi_tahun" data-placeholder="Tahun" class="select-size-xs wajib">
											<option value=""></option>
											<?php
											for($i=2019;$i>=1950;$i--) {
												echo '<option value="'.$i.'">'.$i.'</option>';
											}
											?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<div class="col-md-9 col-md-offset-3">
										<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
										<input type="hidden" name="organisasi_pegawai_id" id="organisasi_pegawai_id" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_id"];?>">
										<input type="hidden" name="organisasi_pegawai_nip" id="organisasi_pegawai_nip" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_nip"];?>">
										<input type="hidden" name="isUpdate" id="isUpdate" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="organisasi_id" id="organisasi_id" class="form-control input-xs wajib" value="0">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 form_lihat_data" style="display: none;">
						<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Lihat Data organisasi</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Nama Organisasi</label>
									<div class="col-md-9">
										<div class="form-control-static organisasi_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Kedudukan</label>
									<div class="col-md-9">
										<div class="form-control-static organisasi_kedudukan">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tanggal Mulai</label>
									<div class="col-md-9">
										<div class="form-control-static organisasi_tanggal_mulai">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-lg-3 control-label">Tanggal Selesai</label>
									<div class="col-lg-9">
										<div class="form-control-static organisasi_tanggal_selesai">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">No SK</label>
									<div class="col-md-9">
										<div class="form-control-static organisasi_nomor_sk">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Jabatan</label>
									<div class="col-md-9">
										<div class="form-control-static organisasi_nama_jabatan">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tahun</label>
									<div class="col-md-9">
										<div class="form-control-static organisasi_tahun">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<div class="col-md-9 col-md-offset-3">
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>
	<script type="text/javascript">
		function set_non_aktif(id,pegawai_id){
			$('#text_konfirmasi').html('Anda yakin menghapus data ini ?');
			$('#konfirmasipenyimpanan').modal('show');
			$('#setujukonfirmasibutton').unbind();
			$('#setujukonfirmasibutton').on('click', function () {
				$('#konfirmasipenyimpanan').modal('hide');
				$('.angkadoank').inputmask('remove');
				$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
				window.location.href = base_url+'kepegawaian/organisasi/hapus/'+id+'/'+pegawai_id;
			});
		}
		function lihat_data(data){
			$('.form_tambah_data').hide();
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					lihat_data(data);
				});
			}else{
				$('.form_lihat_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_lihat_data").offset().top-60
					}, 500);
					var organisasi_id = datajson.organisasi_id;
					var organisasi_nama = datajson.organisasi_nama;
					var organisasi_kedudukan = datajson.organisasi_kedudukan;
					var organisasi_tanggal_mulai = datajson.organisasi_tanggal_mulai;
					var organisasi_tanggal_selesai = datajson.organisasi_tanggal_selesai;
					var organisasi_nomor_sk = datajson.organisasi_nomor_sk;
					var organisasi_nama_jabatan = datajson.organisasi_nama_jabatan;
					var organisasi_tahun = datajson.organisasi_tahun;

					$('.organisasi_nama').html(organisasi_nama);
					$('.organisasi_kedudukan').html(organisasi_kedudukan);
					$('.organisasi_tanggal_mulai').html(organisasi_tanggal_mulai);
					$('.organisasi_tanggal_selesai').html(organisasi_tanggal_selesai);
					$('.organisasi_nomor_sk').html(organisasi_nomor_sk);
					$('.organisasi_nama_jabatan').html(organisasi_nama_jabatan);
					$('.organisasi_tahun').html(organisasi_tahun);
				});
			}
		}
		function update_data(data){
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			$('.form_lihat_data').hide();
			$('.label_aksi_form').html('Form Perubahan Data organisasi');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					update_data(data);
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
					$('#isUpdate').val(1);
					var organisasi_id = datajson.organisasi_id;
					var organisasi_nama = datajson.organisasi_nama;
					var organisasi_kedudukan = datajson.organisasi_kedudukan;
					var organisasi_tanggal_mulai = datajson.organisasi_tanggal_mulai;
					var organisasi_tanggal_selesai = datajson.organisasi_tanggal_selesai;
					var organisasi_nomor_sk = datajson.organisasi_nomor_sk;
					var organisasi_nama_jabatan = datajson.organisasi_nama_jabatan;
					var organisasi_tahun = datajson.organisasi_tahun;

					$('#organisasi_id').val(organisasi_id);
					$('#organisasi_nama').val(organisasi_nama);
					$('#organisasi_kedudukan').val(organisasi_kedudukan);
					$('#organisasi_tanggal_mulai').val(organisasi_tanggal_mulai);
					$('#organisasi_tanggal_selesai').val(organisasi_tanggal_selesai);
					$('#organisasi_nomor_sk').val(organisasi_nomor_sk);
					$('#organisasi_nama_jabatan').val(organisasi_nama_jabatan);
					$('#organisasi_id').val(datajson.organisasi_id);

					$('#organisasi_tahun').select2('val', '');
					$('#organisasi_tahun').val(organisasi_tahun);
					$('#organisasi_tahun').trigger('change');

				});
			}
		}
		function tambah_data(){
			$('.form_lihat_data').hide();
			reset_form();
			$('.label_aksi_form').html('Form Penambahan Data organisasi');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					$('.form_tambah_data').slideDown('fast',function(){
						$('html, body').animate({
							scrollTop: $(".form_tambah_data").offset().top-60
						}, 500);
						
					});
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
				});
			}
		}
		function batal_data(){
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					reset_form();
				});
			}
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					reset_form();
				});
			}
		}
		function reset_form(){
			$('#organisasi_id').val(0);
			$('#organisasi_nama').val('');
			$('#organisasi_kedudukan').val('');
			$('#organisasi_tanggal_mulai').val('');
			$('#organisasi_tanggal_selesai').val('');
			$('#organisasi_id').val(0);
			$('#organisasi_nomor_sk').val('');
			$('#organisasi_nama_jabatan').val('');
			$('#organisasi_tahun').select2('val',' ');
			$('.validation-error-label').css('display','none');
		}
		
		$(document).ready(function() {
			$('.pickorganisasi').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});

		});
	</script>
