<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">seminar Formal</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nip"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tempat Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_tempat_lahir"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Jabatan</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama_jabatan"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Unit Kerja</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["unit_kerja_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<a href="<?php echo base_url();?>kepegawaian/seminar" class="btn btn-xs btn-warning">Kembali</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Daftar Data seminar</span>
					<div class="col-md-12">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
							<thead>
								<tr>
									<th class="text-center">Nama Seminar</th>
									<th class="text-center">Tempat</th>
									<th class="text-center">Tahun</th>
									<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($history)>0){?>
									<?php foreach ($history as $h) {  ?>
										<tr>
											<td><?php echo $h["seminar_nama_kegiatan"];?></td>
											<td><?php echo $h["seminar_tempat_text"];?></td>
											<td><?php echo $h["seminar_tahun"];?></td>
											<td>
												<ul class="icons-list">
													<li><a href="javascript:void(0)" onclick="update_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
													<li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>
													<li><a onclick="set_non_aktif(<?php echo $h["seminar_id"];?>,<?php echo $h["seminar_pegawai_id"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
												</ul>
											</td>
										</tr>
									<?php } ?>
								<?php }else{ ?>
									<tr>
										<td class="text-center" colspan="7">Data Belum Tersedia</td>
									</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<td class="text-right" colspan="7"><a href="javascript:void(0)" onclick="tambah_data();" class="btn btn-xs btn-primary btn-tfoot-table">Tambah Data</a>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="col-md-12 form_tambah_data" style="display: none;">
						<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan Data seminar</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Nama Seminar</label>
									<div class="col-md-9">
										<input type="text" name="seminar_nama_kegiatan" id="seminar_nama_kegiatan" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Lokasi</label>
									<div class="col-md-9">
										<input type="text" name="seminar_lokasi" id="seminar_lokasi" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tempat</label>
									<div class="col-md-9">
										<select name="seminar_tempat" id="seminar_tempat" data-placeholder="Tempat" class="select-size-xs wajib">
											<option></option>
											<option value="1">Dalam Negeri</option>
											<option value="2">Luar Negeri</option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Penyelenggara</label>
									<div class="col-md-9">
										<input type="text" name="seminar_penyelenggara" id="seminar_penyelenggara" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tahun</label>
									<div class="col-md-9">
										<select name="seminar_tahun" id="seminar_tahun" data-placeholder="Tahun Masuk" class="select-size-xs wajib">
											<option value=""></option>
											<?php
											for($i=2019;$i>=1950;$i--) {
												echo '<option value="'.$i.'">'.$i.'</option>';
											}
											?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Kedudukan</label>
									<div class="col-md-9">
										<select name="seminar_kedudukan_id" id="seminar_kedudukan_id" data-placeholder="Kedudukan" class="select-size-xs wajib">
											<option value=""></option>
											<?php foreach ($kedudukan as $i) {
												echo '<option value="'.$i["kedudukan_id"].'" data-kode="'.$i["kedudukan_id"].'">'.$i["kedudukan_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<div class="col-md-9 col-md-offset-3">
										<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
										<input type="hidden" name="seminar_pegawai_id" id="seminar_pegawai_id" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_id"];?>">
										<input type="hidden" name="seminar_pegawai_nip" id="seminar_pegawai_nip" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_nip"];?>">
										<input type="hidden" name="isUpdate" id="isUpdate" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="seminar_id" id="seminar_id" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="seminar_jurusan_code_tingkat_temp" id="seminar_jurusan_code_tingkat_temp" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="seminar_jurusan_code_temp" id="seminar_jurusan_code_temp" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="seminar_jurusan_code_fakultas_temp" id="seminar_jurusan_code_fakultas_temp" class="form-control input-xs wajib" value="0">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 form_lihat_data" style="display: none;">
						<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Lihat Data seminar</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Nama Seminar</label>
									<div class="col-md-9">
										<div class="form-control-static seminar_nama_kegiatan">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Lokasi</label>
									<div class="col-md-9">
										<div class="form-control-static seminar_lokasi">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tempat</label>
									<div class="col-md-9">
										<div class="form-control-static seminar_tempat_text">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Penyelenggara</label>
									<div class="col-md-9">
										<div class="form-control-static seminar_penyelenggara">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tahun</label>
									<div class="col-md-9">
										<div class="form-control-static seminar_tahun">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Kedudukan</label>
									<div class="col-md-9">
										<div class="form-control-static kedudukan_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<div class="col-md-9 col-md-offset-3">
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>
	<script type="text/javascript">
		function set_non_aktif(id,pegawai_id){
			$('#text_konfirmasi').html('Anda yakin menghapus data ini ?');
			$('#konfirmasipenyimpanan').modal('show');
			$('#setujukonfirmasibutton').unbind();
			$('#setujukonfirmasibutton').on('click', function () {
				$('#konfirmasipenyimpanan').modal('hide');
				$('.angkadoank').inputmask('remove');
				$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
				window.location.href = base_url+'kepegawaian/seminar/hapus/'+id+'/'+pegawai_id;
			});
		}
		function lihat_data(data){
			$('.form_tambah_data').hide();
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					lihat_data(data);
				});
			}else{
				$('.form_lihat_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_lihat_data").offset().top-60
					}, 500);
					var seminar_id = datajson.seminar_id;
					var seminar_nama_kegiatan = datajson.seminar_nama_kegiatan;
					var seminar_lokasi = datajson.seminar_lokasi;
					var seminar_tempat_text = datajson.seminar_tempat_text;
					var seminar_penyelenggara = datajson.seminar_penyelenggara;
					var seminar_tahun = datajson.seminar_tahun;
					var kedudukan_nama = datajson.kedudukan_nama;

					$('.seminar_nama_kegiatan').html(seminar_nama_kegiatan);
					$('.seminar_lokasi').html(seminar_lokasi);
					$('.seminar_tempat_text').html(seminar_tempat_text);
					$('.seminar_penyelenggara').html(seminar_penyelenggara);
					$('.seminar_tahun').html(seminar_tahun);
					$('.kedudukan_nama').html(kedudukan_nama);
				});
			}
		}
		function update_data(data){
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			$('.form_lihat_data').hide();
			$('.label_aksi_form').html('Form Perubahan Data seminar');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					update_data(data);
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
					$('#isUpdate').val(1);
					var seminar_id = datajson.seminar_id;
					var seminar_nama_kegiatan = datajson.seminar_nama_kegiatan;
					var seminar_lokasi = datajson.seminar_lokasi;
					var seminar_tempat = datajson.seminar_tempat;
					var seminar_penyelenggara = datajson.seminar_penyelenggara;
					var seminar_tahun = datajson.seminar_tahun;
					var seminar_kedudukan_id = datajson.seminar_kedudukan_id;

					$('#seminar_id').val(seminar_id);
					$('#seminar_nama_kegiatan').val(seminar_nama_kegiatan);
					$('#seminar_lokasi').val(seminar_lokasi);
					$('#seminar_penyelenggara').val(seminar_penyelenggara);
					$('#seminar_id').val(datajson.seminar_id);

					$('#seminar_tempat').select2('val', '');
					$('#seminar_tempat').val(seminar_tempat);
					$('#seminar_tempat').trigger('change');

					$('#seminar_tahun').select2('val', '');
					$('#seminar_tahun').val(seminar_tahun);
					$('#seminar_tahun').trigger('change');

					$('#seminar_kedudukan_id').select2('val', '');
					$('#seminar_kedudukan_id').val(seminar_kedudukan_id);
					$('#seminar_kedudukan_id').trigger('change');
				});
			}
		}
		function tambah_data(){
			$('.form_lihat_data').hide();
			reset_form();
			$('.label_aksi_form').html('Form Penambahan Data seminar');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					$('.form_tambah_data').slideDown('fast',function(){
						$('html, body').animate({
							scrollTop: $(".form_tambah_data").offset().top-60
						}, 500);
						
					});
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
				});
			}
		}
		function batal_data(){
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					reset_form();
				});
			}
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					reset_form();
				});
			}
		}
		function reset_form(){
			$('#seminar_id').val(0);
			$('#seminar_nama_kegiatan').val('');
			$('#seminar_lokasi').val('');
			$('#seminar_penyelenggara').val('');
			$('#seminar_id').val(0);
			$('#seminar_tempat').select2('val',' ');
			$('#seminar_tahun').select2('val', ' ');
			$('#seminar_kedudukan_id').select2('val',' ');
			$('.validation-error-label').css('display','none');
		}
		$(document).ready(function() {


		});
	</script>
