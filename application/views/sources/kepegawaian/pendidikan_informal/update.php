<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Pendidikan Informal</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nip"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tempat Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_tempat_lahir"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Jabatan</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama_jabatan"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Unit Kerja</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["unit_kerja_hirarki_name_full"];?></div><br>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<a href="<?php echo base_url();?>kepegawaian/pendidikan_informal" class="btn btn-xs btn-warning">Kembali</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Daftar Data Pendidikan</span>
					<div class="col-md-12">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
							<thead>
								<tr>
									<th class="text-center">Nama Kursus</th>
									<th class="text-center">Tanggal<br>Mulai - Selesai</th>
									<th class="text-center">Tempat</th>
									<th class="text-center">Kelompok</th>
									<th class="text-center">Penyelenggara</th>
									<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($history)>0){?>
									<?php foreach ($history as $h) {  ?>
										<tr>
											<td><?php echo $h["pendidikan_nonformal_nama"];?></td>
											<td class="text-center"><?php echo dateEnToId($h["pendidikan_nonformal_tanggal_mulai"], 'd-m-Y');?> - <?php echo dateEnToId($h["pendidikan_nonformal_tanggal_selesai"], 'd-m-Y');?></td>
											<td><?php echo $h["pendidikan_nonformal_tempat_text"];?></td>
											<td><?php echo $h["pendidikan_nonformal_kelompok_text"];?></td>
											<td><?php echo $h["pendidikan_nonformal_penyelenggara"];?></td>
											<td>
												<ul class="icons-list">
													<li><a href="javascript:void(0)" onclick="update_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
													<li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>
													<li><a onclick="set_non_aktif(<?php echo $h["pendidikan_nonformal_id"];?>,<?php echo $h["pendidikan_nonformal_pegawai_id"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
												</ul>
											</td>
										</tr>
									<?php } ?>
								<?php }else{ ?>
									<tr>
										<td class="text-center" colspan="7">Data Belum Tersedia</td>
									</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<td class="text-right" colspan="7"><a href="javascript:void(0)" onclick="tambah_data();" class="btn btn-xs btn-primary btn-tfoot-table">Tambah Data</a>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="col-md-12 form_tambah_data" style="display: none;">
						<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan Data Pendidikan</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Nama Pendidikan</label>
									<div class="col-md-9">
										<input type="text" name="pendidikan_nonformal_nama" id="pendidikan_nonformal_nama" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Kelompok</label>
									<div class="col-md-9">
										<select name="pendidikan_nonformal_kelompok" id="pendidikan_nonformal_kelompok" data-placeholder="Kelompok" class="select-size-xs wajib">
											<option></option>
											<option value="1">Umum</option>
											<option value="2">Khusus</option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tanggal Mulai</label>
									<div class="col-md-9">
										<input type="text" name="pendidikan_nonformal_tanggal_mulai" id="pendidikan_nonformal_tanggal_mulai" class="form-control input-xs wajib pickmulaiinformal">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tanggal Selesai</label>
									<div class="col-md-9">
										<input type="text" name="pendidikan_nonformal_tanggal_selesai" id="pendidikan_nonformal_tanggal_selesai" class="form-control input-xs wajib pickselesaiinformal">
									</div>
								</div>
								<!-- <div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tahun Masuk</label>
									<div class="col-md-9">
										<select name="pendidikan_nonformal_tanggal_mulai" id="pendidikan_nonformal_tanggal_mulai" data-placeholder="Tahun Masuk" class="select-size-xs wajib" onchange="set_tahun_akhir(this,'pendidikan_nonformal_tanggal_selesai')">
											<option value=""></option>
											<?
											for($i=2018;$i>=1950;$i--) {
												echo '<option value="'.$i.'">'.$i.'</option>';
											}
											?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tahun Lulus</label>
									<div class="col-md-9">
										<select name="pendidikan_nonformal_tanggal_selesai" id="pendidikan_nonformal_tanggal_selesai" data-placeholder="Tahun Lulus" class="select-size-xs wajib">
											<option value=""></option>

										</select>
									</div>
								</div> -->
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tempat Belajar</label>
									<div class="col-md-9">
										<select name="pendidikan_nonformal_tempat" id="pendidikan_nonformal_tempat" data-placeholder="Tempat Belajar" class="select-size-xs wajib">
											<option value=""></option>
											<option value="1">Dalam Negeri</option>
											<option value="2">Luar Negeri</option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Lokasi</label>
									<div class="col-md-9">
										<input type="text" name="pendidikan_nonformal_lokasi" id="pendidikan_nonformal_lokasi" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Penyelenggara</label>
									<div class="col-md-9">
										<input type="text" name="pendidikan_nonformal_penyelenggara" id="pendidikan_nonformal_penyelenggara" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Jumlah Jam</label>
									<div class="col-md-1">
										<input type="text" name="pendidikan_nonformal_jumlah_jam" id="pendidikan_nonformal_jumlah_jam" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<div class="col-md-9 col-md-offset-3">
										<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
										<input type="hidden" name="pendidikan_nonformal_pegawai_id" id="pendidikan_nonformal_pegawai_id" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_id"];?>">
										<input type="hidden" name="pendidikan_nonformal_pegawai_nip" id="pendidikan_nonformal_pegawai_nip" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_nip"];?>">
										<input type="hidden" name="isUpdate" id="isUpdate" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="pendidikan_nonformal_id" id="pendidikan_nonformal_id" class="form-control input-xs wajib" value="0">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 form_lihat_data" style="display: none;">
						<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Lihat Data Pendidikan</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Nama Pendidikan</label>
									<div class="col-md-9">
										<div class="form-control-static pendidikan_nonformal_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Kelompok</label>
									<div class="col-md-9">
										<div class="form-control-static pendidikan_nonformal_kelompok_text">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tahun Masuk</label>
									<div class="col-md-9">
										<div class="form-control-static pendidikan_nonformal_tanggal_mulai">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-lg-3 control-label">Tahun Lulus</label>
									<div class="col-lg-9">
										<div class="form-control-static pendidikan_nonformal_tanggal_selesai">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tempat Belajar</label>
									<div class="col-md-9">
										<div class="form-control-static pendidikan_nonformal_tempat_text">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Lokasi</label>
									<div class="col-md-9">
										<div class="form-control-static pendidikan_nonformal_lokasi">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Penyelenggara</label>
									<div class="col-md-9">
										<div class="form-control-static pendidikan_nonformal_penyelenggara">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Jumlah Jam</label>
									<div class="col-md-9">
										<div class="form-control-static pendidikan_nonformal_jumlah_jam">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<div class="col-md-9 col-md-offset-3">
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>
	<script type="text/javascript">
		function set_non_aktif(id,pegawai_id){
			$('#text_konfirmasi').html('Anda yakin menghapus data ini ?');
			$('#konfirmasipenyimpanan').modal('show');
			$('#setujukonfirmasibutton').unbind();
			$('#setujukonfirmasibutton').on('click', function () {
				$('#konfirmasipenyimpanan').modal('hide');
				$('.angkadoank').inputmask('remove');
				$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
				window.location.href = base_url+'kepegawaian/pendidikan_informal/hapus/'+id+'/'+pegawai_id;
			});
		}
		function lihat_data(data){
			$('.form_tambah_data').hide();
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					lihat_data(data);
				});
			}else{
				$('.form_lihat_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_lihat_data").offset().top-60
					}, 500);
					var pendidikan_nonformal_id = datajson.pendidikan_nonformal_id;
					var pendidikan_nonformal_nama = datajson.pendidikan_nonformal_nama;
					var pendidikan_nonformal_kelompok_text = datajson.pendidikan_nonformal_kelompok_text;
					var pendidikan_nonformal_tanggal_mulai = datajson.pendidikan_nonformal_tanggal_mulai;
					var pendidikan_nonformal_tanggal_selesai = datajson.pendidikan_nonformal_tanggal_selesai;
					var pendidikan_nonformal_tempat_text = datajson.pendidikan_nonformal_tempat_text;
					var pendidikan_nonformal_lokasi = datajson.pendidikan_nonformal_lokasi;
					var pendidikan_nonformal_penyelenggara = datajson.pendidikan_nonformal_penyelenggara;
					var pendidikan_nonformal_jumlah_jam = datajson.pendidikan_nonformal_jumlah_jam;

					$('.pendidikan_nonformal_nama').html(pendidikan_nonformal_nama);
					$('.pendidikan_nonformal_kelompok_text').html(pendidikan_nonformal_kelompok_text);
					$('.pendidikan_nonformal_tanggal_mulai').html(pendidikan_nonformal_tanggal_mulai);
					$('.pendidikan_nonformal_tanggal_selesai').html(pendidikan_nonformal_tanggal_selesai);
					$('.pendidikan_nonformal_tempat_text').html(pendidikan_nonformal_tempat_text);
					$('.pendidikan_nonformal_lokasi').html(pendidikan_nonformal_lokasi);
					$('.pendidikan_nonformal_penyelenggara').html(pendidikan_nonformal_penyelenggara);
					$('.pendidikan_nonformal_jumlah_jam').html(pendidikan_nonformal_jumlah_jam);
				});
			}
		}
		function update_data(data){
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			$('.form_lihat_data').hide();
			$('.label_aksi_form').html('Form Perubahan Data Pendidikan');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					update_data(data);
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
					$('#isUpdate').val(1);
					var pendidikan_nonformal_id = datajson.pendidikan_nonformal_id;
					var pendidikan_nonformal_nama = datajson.pendidikan_nonformal_nama;
					var pendidikan_nonformal_kelompok = datajson.pendidikan_nonformal_kelompok;
					var pendidikan_nonformal_tanggal_mulai = datajson.pendidikan_nonformal_tanggal_mulai;
					var pendidikan_nonformal_tanggal_selesai = datajson.pendidikan_nonformal_tanggal_selesai;
					var pendidikan_nonformal_tempat = datajson.pendidikan_nonformal_tempat;
					var pendidikan_nonformal_lokasi = datajson.pendidikan_nonformal_lokasi;
					var pendidikan_nonformal_penyelenggara = datajson.pendidikan_nonformal_penyelenggara;
					var pendidikan_nonformal_jumlah_jam = datajson.pendidikan_nonformal_jumlah_jam;
					
					$('#pendidikan_nonformal_id').val(pendidikan_nonformal_id);
					$('#pendidikan_nonformal_nama').val(pendidikan_nonformal_nama);
					$('#pendidikan_nonformal_tanggal_mulai').val(pendidikan_nonformal_tanggal_mulai);
					$('#pendidikan_nonformal_tanggal_selesai').val(pendidikan_nonformal_tanggal_selesai);
					$('#pendidikan_nonformal_lokasi').val(pendidikan_nonformal_lokasi);
					$('#pendidikan_nonformal_penyelenggara').val(pendidikan_nonformal_penyelenggara);
					$('#pendidikan_nonformal_jumlah_jam').val(pendidikan_nonformal_jumlah_jam);
					// $('#pendidikan_nonformal_id').val(datajson.pendidikan_nonformal_id);

					$('#pendidikan_nonformal_kelompok').select2('val', '');
					$('#pendidikan_nonformal_kelompok').val(pendidikan_nonformal_kelompok);
					$('#pendidikan_nonformal_kelompok').trigger('change');

					// $('#pendidikan_nonformal_tanggal_mulai').select2('val', '');
					// $('#pendidikan_nonformal_tanggal_mulai').val(pendidikan_nonformal_tanggal_mulai);
					// $('#pendidikan_nonformal_tanggal_mulai').trigger('change');

					// $('#pendidikan_nonformal_tanggal_selesai').select2('val', '');
					// $('#pendidikan_nonformal_tanggal_selesai').val(pendidikan_nonformal_tanggal_selesai);
					// $('#pendidikan_nonformal_tanggal_selesai').trigger('change');

					$('#pendidikan_nonformal_tempat').select2('val', '');
					$('#pendidikan_nonformal_tempat').val(pendidikan_nonformal_tempat);
					$('#pendidikan_nonformal_tempat').trigger('change');

				});
			}
		}
		function tambah_data(){
			$('.form_lihat_data').hide();
			reset_form();
			$('.label_aksi_form').html('Form Penambahan Data Pendidikan');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					$('.form_tambah_data').slideDown('fast',function(){
						$('html, body').animate({
							scrollTop: $(".form_tambah_data").offset().top-60
						}, 500);
						
					});
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
				});
			}
		}
		function batal_data(){
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					reset_form();
				});
			}
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					reset_form();
				});
			}
		}
		function reset_form(){
			$('#pendidikan_nonformal_id').val(0);
			$('#pendidikan_nonformal_nama').val('');
			$('#pendidikan_nonformal_tempat').val('');
			$('#pendidikan_nonformal_lokasi').val('');
			$('#pendidikan_nonformal_tanggal_mulai').val('');
			$('#pendidikan_nonformal_tanggal_selesai').val('');
			$('#pendidikan_nonformal_penyelenggara').val('');
			$('#pendidikan_nonformal_jumlah_jam').val('');
			$('#pendidikan_nonformal_kelompok').select2('val',' ');
			$('#pendidikan_nonformal_tempat').select2('val',' ');
			$('.validation-error-label').css('display','none');
		}
		function set_tahun_akhir(ini,nextopsi){
			var tahunmulai = parseInt($(ini).val());
			var d = new Date();
			var tahunakhir = d.getFullYear();
			var newoption = "<option value=''></option>";
			for(var i=tahunakhir; i >=tahunmulai; i--) {
				newoption += '<option value="'+i+'">'+i+'</option>';
			}
			$('#'+nextopsi).html(newoption);
		}
		$(document).ready(function() {
		
			$('.pickmulaiinformal').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});
		});
		$(document).ready(function() {
		
			$('.pickselesaiinformal').pickadate({
				monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
				weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
				selectMonths: true,
				selectYears: 80,
				max: true,
				today:'Hari ini',
				clear: 'Hapus',
				close: 'Keluar',
				format: 'dd mmm yyyy',
				formatSubmit: 'yyyy-mm-dd'
			});
		});

		$(document).ready(function() {


		});
	</script>
