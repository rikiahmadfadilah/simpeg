<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">kemampuan_bahasa Formal</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nip"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tempat Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_tempat_lahir"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Jabatan</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama_jabatan"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Unit Kerja</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["unit_kerja_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<a href="<?php echo base_url();?>kepegawaian/kemampuan_bahasa" class="btn btn-xs btn-warning">Kembali</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Daftar Data kemampuan_bahasa</span>
					<div class="col-md-12">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
							<thead>
								<tr>
									<th class="text-center">Nama Bahasa</th>
									<th class="text-center">Kategori</th>
									<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($history)>0){?>
									<?php foreach ($history as $h) {  ?>
										<tr>
											<td><?php echo $h["bahasa_nama"];?></td>
											<td><?php echo $h["kemampuan_bahasa_kategori_text"];?></td>
											<td>
												<ul class="icons-list">
													<li><a href="javascript:void(0)" onclick="update_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
													<li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>
													<li><a onclick="set_non_aktif(<?php echo $h["kemampuan_bahasa_id"];?>,<?php echo $h["kemampuan_bahasa_pegawai_id"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
												</ul>
											</td>
										</tr>
									<?php } ?>
								<?php } else{ ?>
									<tr>
										<td class="text-center" colspan="7">Data Belum Tersedia</td>
									</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<td class="text-right" colspan="7"><a href="javascript:void(0)" onclick="tambah_data();" class="btn btn-xs btn-primary btn-tfoot-table">Tambah Data</a>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="col-md-12 form_tambah_data" style="display: none;">
						<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan Data Kemampuan Bahasa</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Jenis Bahasa</label>
									<div class="col-md-9">
										<select name="kemampuan_bahasa_bahasa_id" id="kemampuan_bahasa_bahasa_id" data-placeholder="Jenis Bahasa" class="select-size-xs wajib">
											<option value=""></option>
											<?php foreach ($bahasa as $i) {
												echo '<option value="'.$i["bahasa_id"].'" data-kode="'.$i["bahasa_id"].'">'.$i["bahasa_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Bahasa Lainnya</label>
									<div class="col-md-9">
										<input type="text" name="kemampuan_bahasa_lain" id="kemampuan_bahasa_lain" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Kategori</label>
									<div class="col-md-9">
										<select name="kemampuan_bahasa_kategori" id="kemampuan_bahasa_kategori" data-placeholder="Kategori" class="select-size-xs wajib">
											<option></option>
											<option value="1">KURANG</option>
											<option value="2">SEDANG</option>
											<option value="3">BAIK</option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<div class="col-md-9 col-md-offset-3">
										<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
										<input type="hidden" name="kemampuan_bahasa_pegawai_id" id="kemampuan_bahasa_pegawai_id" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_id"];?>">
										<input type="hidden" name="kemampuan_bahasa_pegawai_nip" id="kemampuan_bahasa_pegawai_nip" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_nip"];?>">
										<input type="hidden" name="isUpdate" id="isUpdate" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="kemampuan_bahasa_id" id="kemampuan_bahasa_id" class="form-control input-xs wajib" value="0">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 form_lihat_data" style="display: none;">
						<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Lihat Data Kemampuan Bahasa</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Jenis Bahasa</label>
									<div class="col-md-9">
										<div class="form-control-static bahasa_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Bahasa Lainnya</label>
									<div class="col-md-9">
										<div class="form-control-static kemampuan_bahasa_lain">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Kategori</label>
									<div class="col-md-9">
										<div class="form-control-static kemampuan_bahasa_kategori_text">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<div class="col-md-9 col-md-offset-3">
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>
	<script type="text/javascript">
		function set_non_aktif(id,pegawai_id){
			$('#text_konfirmasi').html('Anda yakin menghapus data ini ?');
			$('#konfirmasipenyimpanan').modal('show');
			$('#setujukonfirmasibutton').unbind();
			$('#setujukonfirmasibutton').on('click', function () {
				$('#konfirmasipenyimpanan').modal('hide');
				$('.angkadoank').inputmask('remove');
				$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
				window.location.href = base_url+'kepegawaian/kemampuan_bahasa/hapus/'+id+'/'+pegawai_id;
			});
		}
		function lihat_data(data){
			$('.form_tambah_data').hide();
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					lihat_data(data);
				});
			}else{
				$('.form_lihat_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_lihat_data").offset().top-60
					}, 500);
					var kemampuan_bahasa_id = datajson.kemampuan_bahasa_id;
					var bahasa_nama = datajson.bahasa_nama;
					var kemampuan_bahasa_kategori_text = datajson.kemampuan_bahasa_kategori_text;
					var kemampuan_bahasa_lain = datajson.kemampuan_bahasa_lain;

					$('.bahasa_nama').html(bahasa_nama);
					$('.kemampuan_bahasa_kategori_text').html(kemampuan_bahasa_kategori_text);
					$('.kemampuan_bahasa_lain').html(kemampuan_bahasa_lain);
				});
			}
		}
		function update_data(data){
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			$('.form_lihat_data').hide();
			$('.label_aksi_form').html('Form Perubahan Data kemampuan_bahasa');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					update_data(data);
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
					$('#isUpdate').val(1);
					var kemampuan_bahasa_id = datajson.kemampuan_bahasa_id;
					var kemampuan_bahasa_bahasa_id = datajson.kemampuan_bahasa_bahasa_id;
					var kemampuan_bahasa_lain = datajson.kemampuan_bahasa_lain;
					var kemampuan_bahasa_kategori = datajson.kemampuan_bahasa_kategori;
					
					$('#kemampuan_bahasa_id').val(kemampuan_bahasa_id);
					$('#kemampuan_bahasa_lain').val(kemampuan_bahasa_lain);
					$('#kemampuan_bahasa_id').val(datajson.kemampuan_bahasa_id);

					$('#kemampuan_bahasa_bahasa_id').select2('val', '');
					$('#kemampuan_bahasa_bahasa_id').val(kemampuan_bahasa_bahasa_id);
					$('#kemampuan_bahasa_bahasa_id').trigger('change');

					$('#kemampuan_bahasa_kategori').select2('val', '');
					$('#kemampuan_bahasa_kategori').val(kemampuan_bahasa_kategori);
					$('#kemampuan_bahasa_kategori').trigger('change');

				});
			}
		}
		function tambah_data(){
			$('.form_lihat_data').hide();
			reset_form();
			$('.label_aksi_form').html('Form Penambahan Data kemampuan_bahasa');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					$('.form_tambah_data').slideDown('fast',function(){
						$('html, body').animate({
							scrollTop: $(".form_tambah_data").offset().top-60
						}, 500);
						
					});
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
				});
			}
		}
		function batal_data(){
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					reset_form();
				});
			}
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					reset_form();
				});
			}
		}
		function reset_form(){
			$('#kemampuan_bahasa_id').val(0);
			$('#kemampuan_bahasa_bahasa_id').select2('val',' ');
			$('#kemampuan_bahasa_lain').val('');
			$('#kemampuan_bahasa_kategori').select2('val',' ');
			$('.validation-error-label').css('display','none');
		}
		$(document).ready(function() {


		});
	</script>
