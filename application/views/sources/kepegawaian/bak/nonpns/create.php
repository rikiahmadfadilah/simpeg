
<script src="<?php echo base_url();?>assets/js/plugins/forms/wizards/steps.min.js"></script>
<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Tambah Data Pegawai Non PNS</h6>
	</div>

	<div class="panel-body">
		<form class="steps-validation need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<h6>Biodata</h6>
			<fieldset>
				<div class="row">
					<div class="col-md-12">
						<legend class="text-bold" style="padding-bottom:5px;border-bottom:solid 1px #00bcd4;">Data Pokok</legend>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group form-group-xs" style="display: none;">
									<label class="col-lg-3 control-label">NIP</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_nip" id="pegawai_nip" value="<?php echo time();?>" class="form-control input-xs ga_wajibzz" type="text">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">KTP</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_nomor_ktp" id="pegawai_nomor_ktp" class="form-control input-xs ga_wajibzz ktp">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">NPWP</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_nomor_npwp" id="pegawai_nomor_npwp" class="form-control input-xs ga_wajibzz npwp">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Gelar</label>
									<div class="col-lg-9">
										<div class="row">
											<div class="col-md-6">
												<input type="text" name="pegawai_gelar_depan" id="pegawai_gelar_depan" class="form-control input-xs ga_wajibzz" placeholder="Depan" type="text">
											</div>

											<div class="col-md-6">
												<input type="text" name="pegawai_gelar_belakang" id="pegawai_gelar_belakang" class="form-control input-xs ga_wajibzz" placeholder="Belakang" type="text">
											</div>
										</div>
									</div>
								</div>

								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Nama Lengkap</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_nama" id="pegawai_nama" class="form-control input-xs ga_wajibzz">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Tempat Lahir</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_tempat_lahir" id="pegawai_tempat_lahir" class="form-control input-xs ga_wajibzz">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Tanggal Lahir</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_tanggal_lahir" id="pegawai_tanggal_lahir" class="form-control input-xs ga_wajibzz pickttl">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Jenis Kelamin</label>
									<div class="col-lg-9">
										<?php foreach ($jenis_kelamin as $i) {
											echo '<label class="radio-inline">';
											echo '<input name="pegawai_jenis_kelamin_id" id="pegawai_jenis_kelamin_id_'.$i["jenis_kelamin_id"].'" value="'.$i["jenis_kelamin_id"].'" type="radio">';
											echo ''.$i["jenis_kelamin_nama"].'';
											echo '</label>';
										}?>

									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Status Kawin</label>
									<div class="col-lg-9">
										<select name="pegawai_perkawinan_id" id="pegawai_perkawinan_id" data-placeholder="Pilih Status Kawin" class="select2-wizard">
											<option></option>
											<?php foreach ($status_perkawinan as $i) {
												echo '<option value="'.$i["perkawinan_id"].'">'.$i["perkawinan_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Agama</label>
									<div class="col-lg-9">
										<select name="pegawai_agama_id" id="pegawai_agama_id" data-placeholder="Pilih Agama" class="select2-wizard">
											<option></option>
											<?php foreach ($agama as $i) {
												echo '<option value="'.$i["agama_id"].'">'.$i["agama_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Golongan Darah</label>
									<div class="col-lg-9">
										<select name="pegawai_golongan_darah_id" id="pegawai_golongan_darah_id" data-placeholder="Pilih Golongan Darah" class="select2-wizard">
											<option></option>
											<?php foreach ($golongan_darah as $i) {
												echo '<option value="'.$i["golongan_darah_id"].'">'.$i["golongan_darah_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Alamat Domisili</span>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Negara</label>
									<div class="col-lg-9">
										<select name="pegawai_domisili_negara_id" id="pegawai_domisili_negara_id" data-placeholder="Pilih Negara" class="select2-wizard" onchange="get_provinsi(this)">
											<option></option>
											<?php foreach ($negara as $i) {

												echo '<option value="'.$i["negara_id"].'" '.(($i["negara_type"]==1)?"selected":"").' data-negara-type="'.$i["negara_type"].'">'.$i["negara_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs dalam_negeri">
									<label class="col-lg-3 control-label">Provinsi</label>
									<div class="col-lg-9">
										<select name="pegawai_domisili_provinsi_id" id="pegawai_domisili_provinsi_id" data-placeholder="Pilih Provinsi" class="select2-wizard" onchange="get_kota(this,'pegawai_domisili_kota_id')">
											<option></option>
											<?php foreach ($provinsi as $i) {

												echo '<option value="'.$i["provinsi_kode"].'">'.$i["provinsi_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs dalam_negeri">
									<label class="col-lg-3 control-label">Kota</label>
									<div class="col-lg-9">
										<select name="pegawai_domisili_kota_id" id="pegawai_domisili_kota_id" data-placeholder="Pilih Kota" class="select2-wizard" onchange="get_kecamatan(this,'pegawai_domisili_kec_id')">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs dalam_negeri">
									<label class="col-lg-3 control-label">Kecamatan</label>
									<div class="col-lg-9">
										<select name="pegawai_domisili_kec_id" id="pegawai_domisili_kec_id" data-placeholder="Pilih Kecamatan" class="select2-wizard" onchange="get_kelurahan(this,'pegawai_domisili_kel_id')">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs dalam_negeri">
									<label class="col-lg-3 control-label">Kelurahan</label>
									<div class="col-lg-9">
										<select name="pegawai_domisili_kel_id" id="pegawai_domisili_kel_id" data-placeholder="Pilih Kelurahan" class="select2-wizard">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label">Alamat</label>
									<div class="col-lg-9">
										<textarea name="pegawai_domisili_alamat" id="pegawai_domisili_alamat" rows="3" cols="3" class="form-control elastic ga_wajibzz" ></textarea>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Kode Pos</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_domisili_kodepos" id="pegawai_domisili_kodepos" class="form-control input-xs ga_wajibzz kodepos">
									</div>
								</div>
								<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Alamat Sesuai KTP</span>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Provinsi</label>
									<div class="col-lg-9">
										<select name="pegawai_ktp_provinsi_id" id="pegawai_ktp_provinsi_id" data-placeholder="Pilih Provinsi" class="select2-wizard" onchange="get_kota(this,'pegawai_ktp_kota_id')">
											<option></option>
											<?php foreach ($provinsi as $i) {
												echo '<option value="'.$i["provinsi_kode"].'">'.$i["provinsi_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Kota</label>
									<div class="col-lg-9">
										<select name="pegawai_ktp_kota_id" id="pegawai_ktp_kota_id" data-placeholder="Pilih Kota" class="select2-wizard" onchange="get_kecamatan(this,'pegawai_ktp_kec_id')">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Kecamatan</label>
									<div class="col-lg-9">
										<select name="pegawai_ktp_kec_id" id="pegawai_ktp_kec_id" data-placeholder="Pilih Kecamatan" class="select2-wizard" onchange="get_kelurahan(this,'pegawai_ktp_kel_id')">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Kelurahan</label>
									<div class="col-lg-9">
										<select name="pegawai_ktp_kel_id" id="pegawai_ktp_kel_id" data-placeholder="Pilih Kelurahan" class="select2-wizard">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label">Alamat</label>
									<div class="col-lg-9">
										<textarea name="pegawai_ktp_alamat" id="pegawai_ktp_alamat" rows="3" cols="3" class="form-control elastic ga_wajibzz" ></textarea>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Kode Pos</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_ktp_kodepos" id="pegawai_ktp_kodepos" class="form-control input-xs ga_wajibzz kodepos">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<legend class="text-bold" style="padding-bottom:5px;border-bottom:solid 1px #00bcd4;">Data Tambahan</legend>
						<div class="col-md-6">
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">Photo</label>
								<div class="col-lg-9">
									<img src="<?php echo base_url();?>assets/images/default_user.jpg" height="150px;" id="image_pegawai">
									<div class="clearfix"></div>
									<input type="file" name="pegawai_photo" id="pegawai_photo" class="form-control input-xs" style="padding-top: 0px !important;padding-bottom: 0px !important;" accept="image/*">
								</div>
							</div>

						</div>
						<div class="col-md-6">
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">Email Lain</label>
								<div class="col-lg-9">
									<input type="text" name="pegawai_email_lain" id="pegawai_email_lain" class="form-control input-xs">
								</div>
							</div>
							<div class="form-group form-group-xs">
								<label class="col-lg-3 control-label">No. HP</label>
								<div class="col-lg-9">
									<input type="text" name="pegawai_handphone" id="pegawai_handphone" class="form-control input-xs ga_wajibzz">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				
			</fieldset>


			<h6>Pendidikan</h6>
			<fieldset>
				<div class="row">
					<div class="col-md-12">
						
						<div class="row">
							<div class="col-md-12">
								<legend class="text-bold" style="padding-bottom:5px;border-bottom:solid 1px #00bcd4;">Pendidikan</legend>
								
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Pendidikan Terakhir</label>
									<div class="col-md-9">
										<select name="pegawai_pendidikan_terakhir_id" id="pegawai_pendidikan_terakhir_id" data-placeholder="Pendidikan" class="select2-wizard"  onchange="get_tingkat_pendidikan(this,'pegawai_pendidikan_tingkat_id')">
											<option value=""></option>
											<?php foreach ($pendidikan as $i) {
												echo '<option value="'.$i["pendidikan_id"].'" data-kode="'.$i["pendidikan_kode"].'">'.$i["pendidikan_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tingkat Pendidikan</label>
									<div class="col-md-9">
										<select name="pegawai_pendidikan_tingkat_id" id="pegawai_pendidikan_tingkat_id" data-placeholder="Tingkat" class="select2-wizard" onchange="get_fakultas(this,'pegawai_pendidikan_fakultas_id')">
											<option value=""></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Fakultas</label>
									<div class="col-lg-9">
										<select name="pegawai_pendidikan_fakultas_id" id="pegawai_pendidikan_fakultas_id" data-placeholder="Fakultas" class="select2-wizard" onchange="get_jurusan(this,'pegawai_pendidikan_jurusan_id')">
											<option value=""></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Jurusan</label>
									<div class="col-md-9">
										<select name="pegawai_pendidikan_jurusan_id" id="pegawai_pendidikan_jurusan_id" data-placeholder="Jurusan" class="select2-wizard">
											<option value=""></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Program Studi</label>
									<div class="col-md-9">
										<input type="text" name="pegawai_pendidikan_program_studi" id="pegawai_pendidikan_program_studi" class="form-control input-xs">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Tahun Masuk</label>
									<div class="col-lg-3">
										<select name="pegawai_pendidikan_tahun_masuk" id="pegawai_pendidikan_tahun_masuk" data-placeholder="Tahun Masuk" class="select2-wizard" onchange="set_tahun_akhir(this,'pegawai_pendidikan_tahun_lulus')">
											<option value=""></option>
											<?
											$yearstart = date("Y");
											$yearend = $yearstart-68;
											for($i=$yearstart;$i>=$yearend;$i--) {
												echo '<option value="'.$i.'">'.$i.'</option>';
											}
											?>
										</select>
									</div>
									<label class="col-lg-3 control-label">Tahun Lulus</label>
									<div class="col-lg-3">
										<select name="pegawai_pendidikan_tahun_lulus" id="pegawai_pendidikan_tahun_lulus" data-placeholder="Tahun Lulus" class="select2-wizard">
											<option></option>
											<?
											$yearstart = date("Y");
											$yearend = $yearstart-68;
											for($i=$yearstart;$i>=$yearend;$i--) {
												echo '<option value="'.$i.'">'.$i.'</option>';
											}
											?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Nama Sekolah</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_pendidikan_nama" id="pegawai_pendidikan_nama" class="form-control input-xs ga_wajibzz">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Tempat Belajar</label>
									<div class="col-lg-3">
										<select name="pegawai_pendidikan_tempat" id="pegawai_pendidikan_tempat" data-placeholder="Tempat Belajar" class="select2-wizard">
											<option></option>
											<option value="1">Dalam Negeri</option>
											<option value="2">Luar Negeri</option>
										</select>
									</div>
									<label class="col-lg-3 control-label">IPK</label>
									<div class="col-lg-3">
										<input type="text" name="pegawai_pendidikan_ipk" id="pegawai_pendidikan_ipk" class="form-control input-xs">

									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Nilai TOEFL</label>
									<div class="col-lg-3">
										<input type="text" name="pegawai_nilai_toefl" id="pegawai_nilai_toefl" class="form-control input-xs">

									</div>
									<label class="col-lg-3 control-label">Nilai IELT</label>
									<div class="col-lg-3">
										<input type="text" name="pegawai_nilai_ielt" id="pegawai_nilai_ielt" class="form-control input-xs">

									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Catatan</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_catatan" id="pegawai_catatan" class="form-control input-xs ga_wajibzz">
									</div>
								</div>
							</div>
						
						</div>
					</div>
				</div>
			</fieldset>

			<h6>Jabatan</h6>
			<fieldset>
				<div class="row">
					<div class="col-md-12">
						<legend class="text-bold" style="padding-bottom:5px;border-bottom:solid 1px #00bcd4;">Jabatan</legend>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Unit Kerja</label>
									<div class="col-lg-9">
										<select name="pegawai_unit_kerja_id[]" id="pegawai_unit_kerja_id_1" data-placeholder="Unit Kerja" class="select2-wizard ga_wajibzz"  onchange="get_unit_kerja_hirarki(this,1)">
											<option></option>
											<?php foreach ($unit_kerja as $i) {
												echo '<option value="'.$i["unit_kerja_kode"].'" parent_id_kode="'.$i["unit_kerja_id_kode"].'" jabatan_id="'.$i["jabatan_id"].'">'.$i["unit_kerja_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs pegawai_unit_kerja_id_2" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="pegawai_unit_kerja_id[]" id="pegawai_unit_kerja_id_2" data-placeholder="Unit Kerja" class="select2-wizard"  onchange="get_unit_kerja_hirarki(this,2)">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs pegawai_unit_kerja_id_3" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="pegawai_unit_kerja_id[]" id="pegawai_unit_kerja_id_3" data-placeholder="Unit Kerja" class="select2-wizard"  onchange="get_unit_kerja_hirarki(this,3)">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs pegawai_unit_kerja_id_4" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="pegawai_unit_kerja_id[]" id="pegawai_unit_kerja_id_4" data-placeholder="Unit Kerja" class="select2-wizard"  onchange="get_unit_kerja_hirarki(this,4)">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs pegawai_unit_kerja_id_5" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="pegawai_unit_kerja_id[]" id="pegawai_unit_kerja_id_5" data-placeholder="Unit Kerja" class="select2-wizard" onchange="get_unit_kerja_hirarki(this,5)">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Jabatan Sebagai / Nama TUSI</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_nama_jabatan" id="pegawai_nama_jabatan" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Tanggal Awal Masuk</label>
									<div class="col-lg-9">
										<input type="text" name="pegawai_cpns_tanggal_tmt" id="pegawai_cpns_tanggal_tmt" class="form-control input-xs ga_wajibzz pickttl">
									</div>
								</div>
								
								
							</div>
						
						</div>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>
<script type="text/javascript">
	function set_tahun_akhir(ini,nextopsi){
		var tahunmulai = parseInt($(ini).val());
		var d = new Date();
		var tahunakhir = d.getFullYear();
		var newoption = "<option value=''></option>";
		for(var i=tahunakhir; i >=tahunmulai; i--) {
			newoption += '<option value="'+i+'">'+i+'</option>';
		}
		$('#'+nextopsi).html(newoption);
	}
	function getFungsionalTertentuByJabatan(ini){
		var kode  = $('option:selected', ini).attr('kode');
		if(kode=="88"){
			$('.fungsional_tertentu_pegawai').slideDown('fast');
		}else{
			$('.fungsional_tertentu_pegawai').slideUp('fast');
		}
	}
	function getFungsionalTertentuByJabatanCPNS(ini){
		var vals  = $(ini).val();
		if(vals=="2"){
			$('.fungsional_tertentu_cpns').slideDown('fast');
		}else{
			$('.fungsional_tertentu_cpns').slideUp('fast');
		}
	}
	function  get_unit_kerja_hirarki(ini,lvl) {
		if(lvl==1){
			$('.pegawai_unit_kerja_id_5').slideUp('fast',function(){
				$('#pegawai_unit_kerja_id_5').select2('val', '');
				$('.pegawai_unit_kerja_id_4').slideUp('fast',function(){
					$('#pegawai_unit_kerja_id_4').select2('val', '');
					$('.pegawai_unit_kerja_id_3').slideUp('fast',function(){
						$('#pegawai_unit_kerja_id_3').select2('val', '');
						$('.pegawai_unit_kerja_id_2').slideUp('fast',function(){
							$('#pegawai_unit_kerja_id_2').select2('val', '');
						});
					});
				});
			});
		}else if(lvl==2){
			$('.pegawai_unit_kerja_id_5').slideUp('fast',function(){
				$('#pegawai_unit_kerja_id_5').select2('val', '');
				$('.pegawai_unit_kerja_id_4').slideUp('fast',function(){
					$('#pegawai_unit_kerja_id_4').select2('val', '');
					$('.pegawai_unit_kerja_id_3').slideUp('fast',function(){
						$('#pegawai_unit_kerja_id_3').select2('val', '');
					});
				});
			});
		}else if(lvl==3){
			$('.pegawai_unit_kerja_id_5').slideUp('fast',function(){
				$('#pegawai_unit_kerja_id_5').select2('val', '');
				$('.pegawai_unit_kerja_id_4').slideUp('fast',function(){
					$('#pegawai_unit_kerja_id_4').select2('val', '');
				});
			});
		}else if(lvl==4){
			$('.pegawai_unit_kerja_id_5').slideUp('fast',function(){
				$('#pegawai_unit_kerja_id_5').select2('val', '');
			});
		}
		var parent_id_kode  = $('option:selected', ini).attr('parent_id_kode');
		var jabatan_id  = $('option:selected', ini).attr('jabatan_id');

		$('#pegawai_jabatan_id').select2('val', '');
		$('#pegawai_jabatan_id').val(jabatan_id);
		$('#pegawai_jabatan_id').trigger('change');
		$.ajax({
			url: base_url+'kepegawaian/pns/get_unit_kerja_hirarki',
			type: "post",
			dataType: 'json',
			data: { unit_kerja_parent_id_kode: parent_id_kode, unit_kerja_level: (lvl+2)},
			success: function (data) {

				var unit_kerja = data.unit_kerja;
				var newoption = "<option></option>";

				for(var i = 0; i < unit_kerja.length; i ++){
					newoption+='<option value="'+unit_kerja[i].unit_kerja_kode+'" parent_id_kode="'+unit_kerja[i].unit_kerja_id_kode+'"  jabatan_id="'+unit_kerja[i].jabatan_id+'">'+unit_kerja[i].unit_kerja_nama+'</option>';
				}
				$('#pegawai_unit_kerja_id_'+(lvl+1)).html(newoption);
				if(unit_kerja.length>0){
					$('.pegawai_unit_kerja_id_'+(lvl+1)).slideDown('fast');
				}else{
					$('.pegawai_unit_kerja_id_'+(lvl+1)).slideUp('fast');

				}
			}
		});
	}
	function get_provinsi(ini){
		var IsDalamNegeri = $('option:selected', ini).attr('data-negara-type');
		if(IsDalamNegeri!=1){
			$('.dalam_negeri').slideUp(function(){
				$('#pegawai_domisili_provinsi_id').select2('val', ' ');
				$('#pegawai_domisili_provinsi_id').trigger('change');
				$('#pegawai_domisili_kota_id').html('<option></option>');
				$('#pegawai_domisili_kec_id').html('<option></option>');
				$('#pegawai_domisili_kel_id').html('<option></option>');
			});
		}else{
			$('.dalam_negeri').slideDown(function(){
			});
		}
		
	}
	function  get_kota(ini,nextopsi) {
		var provinsi_kode = $(ini).val();
		$.ajax({
			url: base_url+'kepegawaian/pns/get_kota',
			type: "post",
			dataType: 'json',
			data: { provinsi_kode: provinsi_kode},
			success: function (data) {
				var kota = data.kota;
				var newoption = "<option></option>";
				for(var i = 0; i < kota.length; i ++){
					newoption+='<option value="'+kota[i].kota_kode+'">'+kota[i].kota_nama+'</option>';
				}
				$('#'+nextopsi).html(newoption);
			}
		});
	}
	function  get_kecamatan(ini,nextopsi) {
		var kota_kode = $(ini).val();
		$.ajax({
			url: base_url+'kepegawaian/pns/get_kecamatan',
			type: "post",
			dataType: 'json',
			data: { kota_kode: kota_kode},
			success: function (data) {
				var kecamatan = data.kecamatan;
				var newoption = "<option></option>";
				for(var i = 0; i < kecamatan.length; i ++){
					newoption+='<option value="'+kecamatan[i].kecamatan_kode+'">'+kecamatan[i].kecamatan_nama+'</option>';
				}
				$('#'+nextopsi).html(newoption);
			}
		});
	}
	function  get_kelurahan(ini,nextopsi) {
		var kecamatan_kode = $(ini).val();
		$.ajax({
			url: base_url+'kepegawaian/pns/get_kelurahan',
			type: "post",
			dataType: 'json',
			data: { kecamatan_kode: kecamatan_kode},
			success: function (data) {
				var kelurahan = data.kelurahan;
				var newoption = "<option></option>";
				for(var i = 0; i < kelurahan.length; i ++){
					newoption+='<option value="'+kelurahan[i].kelurahan_kode+'">'+kelurahan[i].kelurahan_nama+'</option>';
				}
				$('#'+nextopsi).html(newoption);
			}
		});
	}
	
	function  get_tingkat_pendidikan(ini,nextopsi) {
		var pendidikan_kode = $('option:selected', ini).attr('data-kode');
		$.ajax({
			url: base_url+'kepegawaian/pendidikan_formal/get_tingkat_pendidikan',
			type: "post",
			dataType: 'json',
			data: { pendidikan_kode: pendidikan_kode},
			success: function (data) {
				var isUpdate = parseInt($('#isUpdate').val());
				var pendidikan_formal_jurusan_code_tingkat_temp = $('#pendidikan_formal_jurusan_code_tingkat_temp').val();
				var tingkat_pendidikan = data.tingkat_pendidikan;
				var newoption = "<option value=''></option>";
				for(var i = 0; i < tingkat_pendidikan.length; i ++){
					newoption+='<option value="'+tingkat_pendidikan[i].jurusan_kode+'" '+((tingkat_pendidikan.length==1 || (isUpdate==1 && pendidikan_formal_jurusan_code_tingkat_temp == tingkat_pendidikan[i].jurusan_kode))?"selected":"")+'>'+tingkat_pendidikan[i].jurusan_grup+'</option>';
				}
				$('#'+nextopsi).html(newoption);

				get_fakultas($('#pegawai_pendidikan_tingkat_id'),'pegawai_pendidikan_fakultas_id');
			}
		});
	}
	function  get_fakultas(ini,nextopsi) {
		var jurusan_kode = $(ini).val();
		$.ajax({
			url: base_url+'kepegawaian/pendidikan_formal/get_fakultas',
			type: "post",
			dataType: 'json',
			data: { jurusan_kode: jurusan_kode},
			success: function (data) {
				var isUpdate = parseInt($('#isUpdate').val());
				var pendidikan_formal_jurusan_code_fakultas_temp = $('#pendidikan_formal_jurusan_code_fakultas_temp').val();

				var jurusan = data.jurusan;
				var newoption = "<option value=''></option>";

				for(var i = 0; i < jurusan.length; i ++){
					newoption+='<option value="'+jurusan[i].jurusan_kode+'" '+(((isUpdate==1 && pendidikan_formal_jurusan_code_fakultas_temp == jurusan[i].jurusan_kode))?"selected":"")+'>'+jurusan[i].jurusan_nama+'</option>';
				}
				$('#'+nextopsi).html(newoption);
				get_jurusan($('#pegawai_pendidikan_fakultas_id'),'pegawai_pendidikan_jurusan_id');
			}
		});
	}
	function  get_jurusan(ini,nextopsi) {
		var jurusan_kode = $(ini).val();
		$.ajax({
			url: base_url+'kepegawaian/pendidikan_formal/get_jurusan',
			type: "post",
			dataType: 'json',
			data: { jurusan_kode: jurusan_kode},
			success: function (data) {
				var isUpdate = parseInt($('#isUpdate').val());
				var pendidikan_formal_jurusan_code_temp = $('#pendidikan_formal_jurusan_code_temp').val();

				var jurusan = data.jurusan;
				var newoption = "<option value=''></option>";

				for(var i = 0; i < jurusan.length; i ++){
					if(jurusan[i].jurusan_grup != jurusan[i].jurusan_nama){
						newoption+='<option value="'+jurusan[i].jurusan_kode+'" '+(((isUpdate==1 && pendidikan_formal_jurusan_code_temp == jurusan[i].jurusan_kode))?"selected":"")+'>'+jurusan[i].jurusan_nama+'</option>';
					}
				}
				$('#'+nextopsi).html(newoption);
			}
		});
	}
	

	function readURL(input) {

		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
				$('#image_pegawai').attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}


	$(document).ready(function() {
		var form = $('form.need_validation').show();
		$(".steps-validation").steps({
			headerTag: "h6",
			bodyTag: "fieldset",
			saveState: true,
			titleTemplate: '<span class="number">#index#</span> #title#',
			autoFocus: true,
			onContentLoaded: function (event, currentIndex) {
				$('.select2-wizard').select2({
					containerCssClass: 'select-xs',
				});
			},
			onStepChanging: function (event, currentIndex, newIndex) {
				if (currentIndex > newIndex) {
					return true;
				} 
				if (currentIndex < newIndex) {
					form.find(".body:eq(" + newIndex + ") label.error").remove();
					form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
				}

				form.validate().settings.ignore = ":disabled,:hidden";
				return form.valid();
			},
			onFinished: function (event, currentIndex) {
				$('form.steps-validation').submit();
			},
			labels: {
				cancel: "Batal",
				current: "Tahapan :",
				pagination: "Pagination",
				finish: "Simpan dan Selesai",
				next: "Selanjutnya",
				previous: "Sebelumnya",
				loading: "Loading ..."
			}
		});
		$('.select2-wizard').select2({
			containerCssClass: 'select-xs',
		});
		$("#pegawai_photo").change(function() {
			readURL(this);
		});

		$('[name="pegawai_nip"]').formatter({
			pattern: '{{99999999}}  {{999999}}  {{9}}  {{999}}'
		});
		$('.kodepos').formatter({
			pattern: '{{99999}}'
		});
		$('.ktp').formatter({
			pattern: '{{9999999999999999}}'
		});
		$('.npwp').formatter({
			pattern: '{{99}}  {{999}}  {{999}}  {{9999}} {{999}}'
		});
		$("#pegawai_ktp_negara_id").prop("disabled", true);
		var d = new Date();

		$('.pickttl').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
		$('.pickttlstart').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});

		$('.pickttlend').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
		$( "#pegawai_nip" ).rules( "add", {
			required: true,
			remote: {
				url: base_url+'kepegawaian/pns/checkNipExist',
				type: "post",
				data: {
					pegawai_nip: function() {
						return $( "#pegawai_nip" ).val();
					}
				}
			},
			messages: {
				required: "Wajib Diisi",
				remote: "NIP Telah Digunakan"
			}
		});
		$( "#pegawai_nomor_ktp" ).rules( "add", {
			required: true,
			remote: {
				url: base_url+'kepegawaian/pns/checkKTPExist',
				type: "post",
				data: {
					pegawai_nomor_ktp: function() {
						return $( "#pegawai_nomor_ktp" ).val();
					}
				}
			},
			messages: {
				required: "Wajib Diisi",
				remote: "NO KTP Telah Digunakan"
			}
		});
		$( "#pegawai_nomor_npwp" ).rules( "add", {
			required: true,
			remote: {
				url: base_url+'kepegawaian/pns/checkNPWPExist',
				type: "post",
				data: {
					pegawai_nomor_npwp: function() {
						return $( "#pegawai_nomor_npwp" ).val();
					}
				}
			},
			messages: {
				required: "Wajib Diisi",
				remote: "NO NPWP Telah Digunakan"
			}
		});
	});
</script>
