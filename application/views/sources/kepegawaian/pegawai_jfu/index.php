<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Data Pribadi</h6>
	</div>
	<div class="panel-body">
		<form class="need_validation form-horizontal" action="<?php echo base_url('kepegawaian/pegawai_jfu/update') ?>" method="post" enctype="multipart/form-data">
            <div class="row">
                <!-- <div class="col-md-6">
                    <div class="form-group form-group-xs">
                        <label class="col-lg-3 control-label">NIP</label>
                        <div class="col-lg-9">
                            <input type="text" name="pegawai_nip" id="pegawai_nip" value="<?php echo $data["pegawai_nip"];?>" class="form-control input-xs ga_wajibzz" type="text">
                        </div>
                    </div>
                    <div class="form-group form-group-xs">
                        <label class="col-lg-3 control-label">KTP</label>
                        <div class="col-lg-9">
                            <input type="text" name="pegawai_nomor_ktp" id="pegawai_nomor_ktp" value="<?php echo $data["pegawai_nomor_ktp"];?>" class="form-control input-xs ga_wajibzz ktp">
                        </div>
                    </div>
                    <div class="form-group form-group-xs">
                        <label class="col-lg-3 control-label">NPWP</label>
                        <div class="col-lg-9">
                            <input type="text" name="pegawai_nomor_npwp" id="pegawai_nomor_npwp" value="<?php echo $data["pegawai_nomor_npwp"];?>" class="form-control input-xs ga_wajibzz npwp">
                        </div>
                    </div>
                    <div class="form-group form-group-xs">
                        <label class="col-lg-3 control-label">Gelar</label>
                        <div class="col-lg-9">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" name="pegawai_gelar_depan" id="pegawai_gelar_depan" value="<?php echo $data["pegawai_gelar_depan"];?>" class="form-control input-xs ga_wajibzz" placeholder="Depan" type="text">
                                </div>

                                <div class="col-md-6">
                                    <input type="text" name="pegawai_gelar_belakang" id="pegawai_gelar_belakang" value="<?php echo $data["pegawai_gelar_belakang"];?>" class="form-control input-xs ga_wajibzz" placeholder="Belakang" type="text">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-group-xs">
                        <label class="col-lg-3 control-label">Nama Lengkap</label>
                        <div class="col-lg-9">
                            <input type="text" name="pegawai_nama" id="pegawai_nama" value="<?php echo $data["pegawai_nama"];?>" class="form-control input-xs ga_wajibzz">
                        </div>
                    </div>
                    <div class="form-group form-group-xs">
                        <label class="col-lg-3 control-label">Tempat Lahir</label>
                        <div class="col-lg-9">
                            <input type="text" name="pegawai_tempat_lahir" id="pegawai_tempat_lahir" value="<?php echo $data["pegawai_tempat_lahir"];?>" class="form-control input-xs ga_wajibzz">
                        </div>
                    </div>
                    <div class="form-group form-group-xs">
                        <label class="col-lg-3 control-label">Tanggal Lahir </label>
                        <div class="col-lg-9">
                            <input type="text" name="pegawai_tanggal_lahir" id="pegawai_tanggal_lahir" value="<?php echo dateEnToId($data["pegawai_tanggal_lahir"],'d F Y');?>" class="form-control input-xs ga_wajibzz pickttl">
                        </div>
                    </div>
                    <div class="form-group form-group-xs">
                        <label class="col-lg-3 control-label">Jenis Kelamin</label>
                        <div class="col-lg-9">
                            <input type="text" name="pegawai_tempat_lahir" id="pegawai_tempat_lahir" value="<?php if($data["pegawai_jenis_kelamin_id"] == 1){ echo $jk = "Laki-Laki";}else{ echo $jk = "Perempuan";} ?>" class="form-control input-xs ga_wajibzz">
                        </div>
                    </div>
                    <div class="form-group form-group-xs">
                        <label class="col-lg-3 control-label">Status Kawin</label>
                        <div class="col-lg-9">
                            <select name="pegawai_perkawinan_id" id="pegawai_perkawinan_id" data-placeholder="Pilih Status Kawin" class="select2-wizard">
                                <option></option>
                                <?php foreach ($status_perkawinan as $i) {
                                    echo '<option value="'.$i["perkawinan_id"].'" '.(($data["pegawai_perkawinan_id"]==$i["perkawinan_id"])?"selected":"").'>'.$i["perkawinan_nama"].'</option>';
                                }?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-group-xs">
                        <label class="col-lg-3 control-label">Agama</label>
                        <div class="col-lg-9">
                            <select name="pegawai_agama_id" id="pegawai_agama_id" data-placeholder="Pilih Agama" class="select2-wizard">
                                <option></option>
                                <?php foreach ($agama as $i) {
                                    echo '<option value="'.$i["agama_id"].'" '.(($data["pegawai_agama_id"]==$i["agama_id"])?"selected":"").'>'.$i["agama_nama"].'</option>';
                                }?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-group-xs">
                        <label class="col-lg-3 control-label">Golongan Darah</label>
                        <div class="col-lg-9">
                            <select name="pegawai_golongan_darah_id" id="pegawai_golongan_darah_id" data-placeholder="Pilih Golongan Darah" class="select2-wizard">
                                <option></option>
                                <?php foreach ($golongan_darah as $i) {
                                    echo '<option value="'.$i["golongan_darah_id"].'" '.(($data["pegawai_golongan_darah_id"]==$i["golongan_darah_id"])?"selected":"").'>'.$i["golongan_darah_nama"].'</option>';
                                }?>
                            </select>
                        </div>
                    </div>
                </div> -->
                <div class="col-md-12">
                    <!-- <div class="form-group form-group-xs">
                        <img src="<?php echo base_url();?>assets/images/pegawai/photo/<?php echo $data['pegawai_image_path']; ?>" height="150px;" id="image_pegawai">
                    </div> -->
                    <div class="form-group form-group-xs">
                        <label class="col-md-3 control-label">NIP</label>
                        <div class="col-md-9">
                            <div class="form-control-static"><?php echo $data['pegawai_nip']; ?></div>
                        </div>
                    </div>
                    <div class="form-group form-group-xs">
                        <label class="col-md-3 control-label">Nama</label>
                        <div class="col-md-9">
                            <div class="form-control-static"><?php echo $data['pegawai_nama_lengkap']; ?></div>
                        </div>
                    </div>
                    <div class="form-group form-group-xs">
                        <label class="col-md-3 control-label">Unit Kerja</label>
                        <div class="col-md-9">
                             <div class="form-control-static"><?php echo htmlspecialchars_decode($data['unit_kerja_hirarki_name_full_break']); ?></div>
                        </div>
                    </div><br><br>
                    <div class="form-group form-group-xs">
                        <label class="col-md-3 control-label">Tangggal Lahir</label>
                        <div class="col-md-9">
                            <?php echo date("d M Y", strtotime($data['pegawai_tanggal_lahir'])) ?>
                        </div>
                    </div>
                    <div class="form-group form-group-xs">
                        <label class="col-md-3 control-label">Telepon Seluler</label>
                        <div class="col-md-7">
                                <input type="text" name="pegawai_handphone" id="pegawai_handphone" class="form-control input-xs pickmutasipensiun" placeholder="" value="<?php echo $data['pegawai_handphone'] ?>" disabled="true">
                        </div>
                    </div>
                    <div class="form-group form-group-xs">
                        <label class="col-md-3 control-label">Emai KKP</label>
                        <div class="col-md-9">
                                <input type="text" name="pegawai_email" id="pegawai_email" class="form-control input-xs pickmutasipensiun" placeholder="" value="<?php echo $data['pegawai_email'] ?>" disabled="true">
                        </div>
                    </div>
                    <div class="form-group form-group-xs">
                        <label class="col-md-3 control-label">KTP</label>
                        <div class="col-md-9">
                                <input type="text" name="pegawai_nomor_ktp" id="pegawai_nomor_ktp" class="form-control input-xs pickmutasipensiun" placeholder="" value="<?php echo $data['pegawai_nomor_ktp'] ?>" disabled="true">
                        </div>
                    </div>
                    <div class="form-group form-group-xs">
                        <label class="col-md-3 control-label">NPWP</label>
                        <div class="col-md-9">
                                <input type="text" name="pegawai_nomor_npwp" id="pegawai_nomor_npwp" class="form-control input-xs pickmutasipensiun" placeholder="" value="<?php echo $data['pegawai_nomor_npwp'] ?>" disabled="true">
                        </div>
                    </div>
                    <div class="form-group form-group-xs">
                        <label class="col-md-3 control-label">Nama Kontak Darurat</label>
                        <div class="col-md-7">
                                <input type="text" name="pegawai_nama_kontak_darurat" id="pegawai_nama_kontak_darurat" class="form-control input-xs pickmutasipensiun" placeholder="" value="<?php echo $data['pegawai_nama_kontak_darurat'] ?>" disabled="true">
                        </div>
                    </div>
                    <div class="form-group form-group-xs">
                        <label class="col-md-3 control-label">No Kontak Darurat</label>
                        <div class="col-md-7">
                                <input type="text" name="pegawai_no_kontak_darurat" id="pegawai_no_kontak_darurat" class="form-control input-xs pickmutasipensiun" placeholder="" value="<?php echo $data['pegawai_no_kontak_darurat'] ?>" disabled="true">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-md-offset-3">
                    <a href="javascript:void()" id="edit" type="button" class="btn btn-xs btn-info">Edit</a>
                    <button type="submit" class="btn btn-xs btn-primary" style="display: none;" id="simpan">Simpan</button> 
                    <a href="<?php echo base_url('home/dashboard') ?>" type="button" class="btn btn-xs btn-danger">Kembali</a>
                    <a href="<?php echo base_url('kepegawaian/drh_pribadi') ?>" type="button" class="btn btn-xs btn-success">Lihat DRH</a>
                    <input type="hidden" name="user_id" id="user_id"  class="form-control input-xs wajib" type="text" value="<?php echo $data['pegawai_id'] ?>">
                    <!-- <input type="hidden" name="user_akses_id" id="user_akses_id"  class="form-control input-xs wajib" type="text" value="<?php echo $user["user_akses_id"];?>" > -->
                </div>
            </div>

        </form>
	</div>
</div>
<script type="text/javascript">
    $('#edit').on('click', function() {
        $('#pegawai_handphone').prop('disabled', false);
        $('#pegawai_nama_kontak_darurat').prop('disabled', false);
        $('#pegawai_no_kontak_darurat').prop('disabled', false);
        $('#pegawai_email').prop('disabled', false);
        $('#pegawai_nomor_ktp').prop('disabled', false);
        $('#pegawai_nomor_npwp').prop('disabled', false);
        $('#edit').hide();
        $('#simpan').show();
    });
</script>
