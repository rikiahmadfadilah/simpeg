<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Tugas/Izin Belajar</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nip"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tempat Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_tempat_lahir"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Jabatan</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama_jabatan"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Unit Kerja</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["unit_kerja_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<a href="<?php echo base_url();?>kepegawaian/izin_belajar" class="btn btn-xs btn-warning">Kembali</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Daftar Data Tugas/Izin Belajar</span>
					<div class="col-md-12">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
							<thead>
								<tr>
									<th class="text-center">Status Belajar</th>
									<th class="text-center">Tingkat Pendidikan</th>
									<th class="text-center">Program Studi</th>
									<!-- <th class="text-center">Fakultas</th>
									<th class="text-center">Jurusan</th> -->
									<th class="text-center">No Surat</th>
									<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($history)>0){?>
									<?php foreach ($history as $h) {  ?>
										<tr>
											<td><?php echo $h["izin_belajar_jenis_text"];?></td>
											<td><?php echo $h["pendidikan_nama"];?></td>
											<td><?php echo $h["izin_belajar_prodi_nama"];?></td>
											<!-- <td><?php echo $h["izin_belajar_prodi_nama"];?></td> -->
											<td><?php echo $h["izin_belajar_nomor_surat"];?></td>
											<td>
												<ul class="icons-list">
													<li><a href="javascript:void(0)" onclick="update_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
													<li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>
													<li><a onclick="set_non_aktif(<?php echo $h["izin_belajar_id"];?>,<?php echo $h["izin_belajar_pegawai_id"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
												</ul>
											</td>
										</tr>
									<?php } ?>
								<?php }else{ ?>
									<tr>
										<td class="text-center" colspan="7">Data Belum Tersedia</td>
									</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<td class="text-right" colspan="7"><a href="javascript:void(0)" onclick="tambah_data();" class="btn btn-xs btn-primary btn-tfoot-table">Tambah Data</a>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="col-md-12 form_tambah_data" style="display: none;">
						<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan Data Tugas/Izin Belajar</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Status Belajar</label>
									<div class="col-md-9">
										<select name="izin_belajar_jenis" id="izin_belajar_jenis" data-placeholder="Status Belajar" class="select-size-xs wajib">
											<option></option>
											<option value="1">Tugas Belajar</option>
											<option value="2">Izin Belajar</option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Pendidikan</label>
									<div class="col-md-9">
										<select name="izin_belajar_pendidikan_id" id="izin_belajar_pendidikan_id" data-placeholder="Pendidikan" class="select-size-xs wajib"  onchange="get_tingkat_pendidikan(this,'izin_belajar_tingkat_pendidikan')">
											<option value=""></option>
											<?php foreach ($tingkat as $i) {
												echo '<option value="'.$i["pendidikan_id"].'" data-kode="'.$i["pendidikan_kode"].'">'.$i["pendidikan_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tingkat Pendidikan</label>
									<div class="col-md-9">
										<select name="izin_belajar_tingkat_pendidikan" id="izin_belajar_tingkat_pendidikan" data-placeholder="Tingkat Pendidikan" class="select-size-xs wajib">
											<option value=""></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs" id="sd" style="display: none;">
									<label class="col-md-3 control-label">Nama Sekolah</label>
									<div class="col-md-9">
										<select name="header_sd" id="header_sd" data-default-id="<?php echo $sd["sekolah_sd_id"];?>" data-default-npsn="<?php echo $sd["sekolah_sd_npsn"];?>"  data-placeholder="Pilih Sekolah SD" class="select-size-xs gakwajib">
											<option value=""></option>
											<option value="<?php echo $sd["sekolah_sd_id"];?>" selected="selected"><?php echo $sd["sekolah_sd_nama"];?></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs" id="smp" style="display: none;">
									<label class="col-md-3 control-label">Nama Sekolah</label>
									<div class="col-md-9">
										<select name="header_smp" id="header_smp" data-default-id="<?php echo $smp["sekolah_smp_id"];?>" data-default-npsn="<?php echo $smp["sekolah_smp_npsn"];?>"  data-placeholder="Pilih Sekolah SMP" class="select-size-xs gakwajib">
											<option value=""></option>
											<option value="<?php echo $smp["sekolah_smp_id"];?>" selected="selected"><?php echo $smp["sekolah_smp_nama"];?></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs" id="sma" style="display: none;">
									<label class="col-md-3 control-label">Nama Sekolah</label>
									<div class="col-md-9">
										<select name="header_sma" id="header_sma" data-default-id="<?php echo $sma["sekolah_sma_id"];?>" data-default-npsn="<?php echo $sma["sekolah_sma_npsn"];?>"  data-placeholder="Pilih Sekolah SMA" class="select-size-xs gakwajib">
											<option value=""></option>
											<option value="<?php echo $sma["sekolah_sma_id"];?>" selected="selected"><?php echo $sma["sekolah_sma_nama"];?></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs" id="univ" style="display: none;">
									<label class="col-md-3 control-label">Nama Universitas</label>
									<div class="col-md-9">
										<select name="header_univ" id="header_univ" data-default-id="<?php echo $univ["univ_id"];?>" data-default-npsn="<?php echo $univ["univ_npsn"];?>"  data-placeholder="Pilih Universitas" class="select-size-xs gakwajib">
											<option value=""></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs" id="prodi" style="display: none;">
									<label class="col-md-3 control-label">Program Studi</label>
									<div class="col-md-9">
										<select name="izin_belajar_prodi_id" id="izin_belajar_prodi_id" data-placeholder="Program Studi" class="select-size-xs gakwajib">
											<option value=""></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs" id="show_prodi" style="display: none;">
									<label class="col-md-3 control-label"></label>
									<div class="col-md-9">
										<a class="btn-xs btn-success" onclick="show_prodi()"; >Refresh</a>
									</div>
								</div>
								<!-- <div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Fakultas</label>
									<div class="col-lg-9">
										<select name="izin_belajar_fakultas" id="izin_belajar_fakultas" data-placeholder="Fakultas" class="select-size-xs wajib" onchange="get_jurusan(this,'izin_belajar_jurusan')">
											<option value=""></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Jurusan</label>
									<div class="col-md-9">
										<select name="izin_belajar_jurusan" id="izin_belajar_jurusan" data-placeholder="Jurusan" class="select-size-xs wajib">
											<option value=""></option>
										</select>
									</div>
								</div> -->
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tempat Belajar</label>
									<div class="col-md-9">
										<select name="izin_belajar_negara_tipe" id="izin_belajar_negara_tipe" data-placeholder="Tempat Belajar" class="select-size-xs wajib">
											<option></option>
											<option value="1">Dalam Negeri</option>
											<option value="2">Luar Negeri</option>
											<option value="3">Double Degree - Dalam Negeri &amp; Luar Negeri</option>
										</select>
									</div>
								</div>
								<input type="hidden" name="izin_belajar_nama_sekolah" id="izin_belajar_nama_sekolah" class="form-control input-xs wajib">
								<!-- <div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Nama Sekolah</label>
									<div class="col-md-9">
									</div>
								</div> -->
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Lokasi Kota</label>
									<div class="col-md-9">
										<input type="text" name="izin_belajar_kota_nama" id="izin_belajar_kota_nama" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Lokasi Negara</label>
									<div class="col-md-9">
										<select name="izin_belajar_negara_id" id="izin_belajar_negara_id" data-placeholder="Lokasi Negara" class="select-size-xs wajib">
											<option value=""></option>
												<?php foreach ($negara as $i) {
												echo '<option value="'.$i["negara_id"].'" data-kode="'.$i["negara_id"].'">'.$i["negara_nama"].'</option>';
												}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Status Sekolah</label>
									<div class="col-md-9">
										<select name="izin_belajar_status_sekolah" id="izin_belajar_status_sekolah" data-placeholder="Status Belajar" class="select-size-xs wajib">
											<option></option>
											<option value="1">Negeri</option>
											<option value="2">Swasta</option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Sponsor</label>
									<div class="col-md-9">
										<input type="text" name="izin_belajar_sponsor" id="izin_belajar_sponsor" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">TMT Tugas Belajar/Izin Belajar</label>
									<div class="col-md-9">
										<input type="text" name="izin_belajar_tmt_izin" id="izin_belajar_tmt_izin" class="form-control input-xs wajib pickizinbelajar">
									</div>
								</div>
								<!-- <div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tahun Selesai</label>
									<div class="col-md-9">
										<select name="izin_belajar_tahun_selesai" id="izin_belajar_tahun_selesai" data-placeholder="Tahun Selesai" class="select-size-xs wajib">
											<option value=""></option>
											<?
											for($i=2018;$i>=1950;$i--) {
												echo '<option value="'.$i.'">'.$i.'</option>';
											}
											?>
										</select>
									</div>
								</div> -->
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tgl Mulai</label>
									<div class="col-md-9">
										<input type="text" name="izin_belajar_tanggal_mulai" id="izin_belajar_tanggal_mulai" class="form-control input-xs wajib pickizinbelajar">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tgl Selesai</label>
									<div class="col-md-9">
										<input type="text" name="izin_belajar_tanggal_selesai" id="izin_belajar_tanggal_selesai" class="form-control input-xs wajib pickizinbelajar">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Status Proses</label>
									<div class="col-md-9">
										<select name="izin_belajar_status_proses" id="izin_belajar_status_proses" data-placeholder="Status Proses" class="select-size-xs wajib">
											<option></option>
											<option value="1">DALAM PROSES</option>
											<option value="2">LULUS</option>
											<option value="3">TDK LULUS</option>
											<option value="4">PERPANJANG</option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tgl Surat</label>
									<div class="col-md-9">
										<input type="text" name="izin_belajar_tanggal_surat" id="izin_belajar_tanggal_surat" class="form-control input-xs wajib pickizinbelajar">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Nomor Surat</label>
									<div class="col-md-9">
										<input type="text" name="izin_belajar_nomor_surat" id="izin_belajar_nomor_surat" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Jabatan Pemberi Izin</label>
									<div class="col-md-9">
										<input type="text" name="izin_belajar_jabatan_pemberi_izin" id="izin_belajar_jabatan_pemberi_izin" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<div class="col-md-9 col-md-offset-3">
										<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
										<input type="hidden" name="izin_belajar_pegawai_id" id="izin_belajar_pegawai_id" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_id"];?>">
										<input type="hidden" name="izin_belajar_pegawai_nip" id="izin_belajar_pegawai_nip" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_nip"];?>">
										<input type="hidden" name="isUpdate" id="isUpdate" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="izin_belajar_id" id="izin_belajar_id" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="izin_belajar_jurusan_code_tingkat_temp" id="izin_belajar_jurusan_code_tingkat_temp" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="izin_belajar_jurusan_code_temp" id="izin_belajar_jurusan_code_temp" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="izin_belajar_jurusan_code_fakultas_temp" id="izin_belajar_jurusan_code_fakultas_temp" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="izin_belajar_sekolah_universitas_id" id="izin_belajar_sekolah_universitas_id" class="form-control input-xs gakwajib">
										<input type="hidden" name="izin_belajar_sekolah_universitas_temp" id="izin_belajar_sekolah_universitas_temp" class="form-control input-xs gakwajib" value="">
										<input type="text" name="izin_belajar_prodi_temp" id="izin_belajar_prodi_temp" class="form-control input-xs gakwajib" value="">
										<input type="hidden" name="pro_studi_text" id="pro_studi_text" class="form-control input-xs gakwajib" value="">
										<input type="hidden" name="prodi_jenjang" id="prodi_jenjang" class="form-control input-xs gakwajib" value="">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 form_lihat_data" style="display: none;">
						<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Lihat Data Tugas/Izin Belajar</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Status Belajar</label>
									<div class="col-md-9">
										<div class="form-control-static izin_belajar_jenis_text">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Pendidikan</label>
									<div class="col-md-9">
										<div class="form-control-static pendidikan_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tingkat Pendidikan</label>
									<div class="col-md-9">
										<div class="form-control-static izin_belajar_tingkat_pendidikan_nama">-</div>
									</div>
								</div>
								<!-- <div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Fakultas</label>
									<div class="col-md-9">
										<div class="form-control-static izin_belajar_fakultas_nama">-</div>
									</div>
								</div> -->
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Program Studi</label>
									<div class="col-md-9">
										<div class="form-control-static izin_belajar_prodi_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tempat Belajar</label>
									<div class="col-md-9">
										<div class="form-control-static izin_belajar_negara_tipe_text">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-lg-3 control-label">Nama Sekolah</label>
									<div class="col-lg-9">
										<div class="form-control-static izin_belajar_nama_sekolah">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Lokasi Kota</label>
									<div class="col-md-9">
										<div class="form-control-static izin_belajar_kota_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Lokasi Negara</label>
									<div class="col-md-9">
										<div class="form-control-static negara_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Status Sekolah</label>
									<div class="col-md-9">
										<div class="form-control-static izin_belajar_status_sekolah_text">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Sponsor</label>
									<div class="col-md-9">
										<div class="form-control-static izin_belajar_sponsor">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">TMT Tugas Belajar/Izin Belajar</label>
									<div class="col-md-9">
										<div class="form-control-static izin_belajar_tmt_izin">-</div>
									</div>
								</div>
								<!-- <div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tahun Selesai</label>
									<div class="col-md-9">
										<div class="form-control-static izin_belajar_tahun_selesai">-</div>
									</div>
								</div> -->
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tgl Mulai</label>
									<div class="col-md-9">
										<div class="form-control-static izin_belajar_tanggal_mulai">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tgl Selesai</label>
									<div class="col-md-9">
										<div class="form-control-static izin_belajar_tanggal_selesai">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Status Proses</label>
									<div class="col-md-9">
										<div class="form-control-static izin_belajar_status_proses_text">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tgl Surat</label>
									<div class="col-md-9">
										<div class="form-control-static izin_belajar_tanggal_surat">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Nomor Surat</label>
									<div class="col-md-9">
										<div class="form-control-static izin_belajar_nomor_surat">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Jabatan Pemberi Izin</label>
									<div class="col-md-9">
										<div class="form-control-static izin_belajar_jabatan_pemberi_izin">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<div class="col-md-9 col-md-offset-3">
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>
	<script type="text/javascript">
		function set_non_aktif(id,pegawai_id){
			$('#text_konfirmasi').html('Anda yakin menghapus data ini ?');
			$('#konfirmasipenyimpanan').modal('show');
			$('#setujukonfirmasibutton').unbind();
			$('#setujukonfirmasibutton').on('click', function () {
				$('#konfirmasipenyimpanan').modal('hide');
				$('.angkadoank').inputmask('remove');
				$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
				window.location.href = base_url+'kepegawaian/izin_belajar/hapus/'+id+'/'+pegawai_id;
			});
		}
		$(function(){
			$('#izin_belajar_pendidikan_id').on('change', function() {
				if($(this).val() == "11")
				{
					$("#sd").show();
					$("#smp").hide();
					$("#sma").hide();
					$("#prodi").hide();
					$("#univ").hide();
				}
				else if($(this).val() == "10")
				{
					$("#smp").show();
					$("#sd").hide();
					$("#sma").hide();
					$("#prodi").hide();
					$("#univ").hide();
				}
				else if($(this).val() == "9")
				{
					$("#sma").show();
					$("#sd").hide();
					$("#smp").hide();
					$("#prodi").hide();
					$("#univ").hide();
				}
				else if($(this).val() <= "9")
				{
					$("#prodi").show();
					$("#univ").show();
					$("#sd").hide();
					$("#smp").hide();
					$("#sma").hide();
				}
			});
		});
		function show_prodi(){
			$('#header_univ').change();
		}
		function lihat_data(data){
			$('.form_tambah_data').hide();
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					lihat_data(data);
				});
			}else{
				$('.form_lihat_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_lihat_data").offset().top-60
					}, 500);
					var izin_belajar_id = datajson.izin_belajar_id;
					var izin_belajar_jenis_text = datajson.izin_belajar_jenis_text;
					var izin_belajar_negara_tipe_text = datajson.izin_belajar_negara_tipe_text;
					var pendidikan_nama = datajson.pendidikan_nama;
					var izin_belajar_tingkat_pendidikan_nama = datajson.izin_belajar_tingkat_pendidikan_nama;
					var izin_belajar_nama_sekolah = datajson.izin_belajar_nama_sekolah;
					// var izin_belajar_fakultas_nama = datajson.izin_belajar_fakultas_nama;
					var izin_belajar_prodi_nama = datajson.izin_belajar_prodi_nama;
					var izin_belajar_kota_nama = datajson.izin_belajar_kota_nama;
					var negara_nama = datajson.negara_nama;
					var izin_belajar_status_sekolah_text = datajson.izin_belajar_status_sekolah_text;
					var izin_belajar_sponsor = datajson.izin_belajar_sponsor;
					var izin_belajar_tmt_izin = datajson.izin_belajar_tmt_izin;
					// var izin_belajar_tahun_selesai = datajson.izin_belajar_tahun_selesai;
					var izin_belajar_tanggal_mulai = datajson.izin_belajar_tanggal_mulai;
					var izin_belajar_tanggal_selesai = datajson.izin_belajar_tanggal_selesai;
					var izin_belajar_status_proses_text = datajson.izin_belajar_status_proses_text;
					var izin_belajar_tanggal_surat = datajson.izin_belajar_tanggal_surat;
					var izin_belajar_nomor_surat = datajson.izin_belajar_nomor_surat;
					var izin_belajar_jabatan_pemberi_izin = datajson.izin_belajar_jabatan_pemberi_izin;

					$('.izin_belajar_jenis_text').html(izin_belajar_jenis_text);
					$('.pendidikan_nama').html(pendidikan_nama);
					$('.izin_belajar_tingkat_pendidikan_nama').html(izin_belajar_tingkat_pendidikan_nama);
					$('.izin_belajar_negara_tipe_text').html(izin_belajar_negara_tipe_text);
					$('.izin_belajar_nama_sekolah').html(izin_belajar_nama_sekolah);
					// $('.izin_belajar_fakultas_nama').html(izin_belajar_fakultas_nama);
					$('.izin_belajar_prodi_nama').html(izin_belajar_prodi_nama);
					$('.izin_belajar_kota_nama').html(izin_belajar_kota_nama);
					$('.negara_nama').html(negara_nama);
					$('.izin_belajar_status_sekolah_text').html(izin_belajar_status_sekolah_text);
					$('.izin_belajar_sponsor').html(izin_belajar_sponsor);
					$('.izin_belajar_tmt_izin').html(izin_belajar_tmt_izin);
					// $('.izin_belajar_tahun_selesai').html(izin_belajar_tahun_selesai);
					$('.izin_belajar_tanggal_mulai').html(izin_belajar_tanggal_mulai);
					$('.izin_belajar_tanggal_selesai').html(izin_belajar_tanggal_selesai);
					$('.izin_belajar_status_proses_text').html(izin_belajar_status_proses_text);
					$('.izin_belajar_tanggal_surat').html(izin_belajar_tanggal_surat);
					$('.izin_belajar_nomor_surat').html(izin_belajar_nomor_surat);
					$('.izin_belajar_jabatan_pemberi_izin').html(izin_belajar_jabatan_pemberi_izin);
					
				});
			}
		}
		function update_data(data){
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			$('.form_lihat_data').hide();
			$('.label_aksi_form').html('Form Perubahan Data izin_belajar');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					update_data(data);
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
					$('#isUpdate').val(1);
					$('#show_prodi').show();
					var izin_belajar_id = datajson.izin_belajar_id;
					var izin_belajar_jenis = datajson.izin_belajar_jenis;
					var izin_belajar_pendidikan_id = datajson.izin_belajar_pendidikan_id;
					var izin_belajar_negara_tipe = datajson.izin_belajar_negara_tipe;
					var izin_belajar_nama_sekolah = datajson.izin_belajar_nama_sekolah;
					var izin_belajar_tingkat_pendidikan = datajson.izin_belajar_tingkat_pendidikan;
					// var izin_belajar_fakultas = datajson.izin_belajar_fakultas;
					var izin_belajar_sekolah_universitas_id = datajson.izin_belajar_sekolah_universitas_id;
					var izin_belajar_prodi_id = datajson.izin_belajar_prodi_id;
					var izin_belajar_kota_nama = datajson.izin_belajar_kota_nama;
					var izin_belajar_negara_id = datajson.izin_belajar_negara_id;
					var izin_belajar_status_sekolah = datajson.izin_belajar_status_sekolah;
					var izin_belajar_sponsor = datajson.izin_belajar_sponsor;
					var izin_belajar_tmt_izin = datajson.izin_belajar_tmt_izin;
					var izin_belajar_tahun_selesai = datajson.izin_belajar_tahun_selesai;
					var izin_belajar_tanggal_mulai = datajson.izin_belajar_tanggal_mulai;
					var izin_belajar_tanggal_selesai = datajson.izin_belajar_tanggal_selesai;
					var izin_belajar_status_proses = datajson.izin_belajar_status_proses;
					var izin_belajar_tanggal_surat = datajson.izin_belajar_tanggal_surat;
					var izin_belajar_nomor_surat = datajson.izin_belajar_nomor_surat;
					var izin_belajar_jabatan_pemberi_izin = datajson.izin_belajar_jabatan_pemberi_izin;
					var izin_belajar_prodi_nama = datajson.izin_belajar_prodi_nama;
					var izin_belajar_prodi_jenjang = datajson.izin_belajar_prodi_jenjang;

					$('#izin_belajar_id').val(izin_belajar_id);
					$('#izin_belajar_nama_sekolah').val(izin_belajar_nama_sekolah);
					$('#izin_belajar_kota_nama').val(izin_belajar_kota_nama);
					$('#izin_belajar_sponsor').val(izin_belajar_sponsor);
					$('#izin_belajar_tmt_izin').val(izin_belajar_tmt_izin);
					$('#izin_belajar_tanggal_mulai').val(izin_belajar_tanggal_mulai);
					$('#izin_belajar_tanggal_selesai').val(izin_belajar_tanggal_selesai);
					$('#izin_belajar_tanggal_surat').val(izin_belajar_tanggal_surat);
					$('#izin_belajar_nomor_surat').val(izin_belajar_nomor_surat);
					$('#izin_belajar_jabatan_pemberi_izin').val(izin_belajar_jabatan_pemberi_izin);
					$('#izin_belajar_jurusan_code_tingkat_temp').val(izin_belajar_tingkat_pendidikan);
					// $('#izin_belajar_jurusan_code_fakultas_temp').val(izin_belajar_fakultas);
					$('#izin_belajar_sekolah_universitas_id').val(izin_belajar_sekolah_universitas_id);
					$('#izin_belajar_sekolah_universitas_temp').val(izin_belajar_sekolah_universitas_id);
					$('#izin_belajar_prodi_temp').val(izin_belajar_prodi_id);
					$('#izin_belajar_id').val(datajson.izin_belajar_id);

					if (datajson.izin_belajar_pendidikan_id <= "8") {
						$("#header_univ").append('<option value="' + izin_belajar_sekolah_universitas_id + '">' + izin_belajar_nama_sekolah + '</option>');
						$("#header_univ").val(izin_belajar_sekolah_universitas_id).trigger('change');
					} else if(datajson.izin_belajar_pendidikan_id == "9"){
						$("#header_sma").append('<option value="' + izin_belajar_sekolah_universitas_id + '">' + izin_belajar_nama_sekolah + '</option>');
						$("#header_sma").val(izin_belajar_sekolah_universitas_id).trigger('change');
					}else if(datajson.izin_belajar_pendidikan_id == "10"){
						$("#header_smp").append('<option value="' + izin_belajar_sekolah_universitas_id + '">' + izin_belajar_nama_sekolah + '</option>');
						$("#header_smp").val(izin_belajar_sekolah_universitas_id).trigger('change');
					}else if(datajson.izin_belajar_pendidikan_id == "11"){
						$("#header_sd").append('<option value="' + izin_belajar_sekolah_universitas_id + '">' + izin_belajar_nama_sekolah + '</option>');
						$("#header_sd").val(izin_belajar_sekolah_universitas_id).trigger('change');
					}
					$('#pro_studi_text').val(izin_belajar_prodi_nama);
					$('#prodi_jenjang').val(izin_belajar_prodi_jenjang);

					$('#izin_belajar_jenis').select2('val', '');
					$('#izin_belajar_jenis').val(izin_belajar_jenis);
					$('#izin_belajar_jenis').trigger('change');

					$('#izin_belajar_pendidikan_id').select2('val', '');
					$('#izin_belajar_pendidikan_id').val(izin_belajar_pendidikan_id);
					$('#izin_belajar_pendidikan_id').trigger('change');

					$('#izin_belajar_negara_tipe').select2('val', '');
					$('#izin_belajar_negara_tipe').val(izin_belajar_negara_tipe);
					$('#izin_belajar_negara_tipe').trigger('change');

					$('#izin_belajar_negara_id').select2('val', '');
					$('#izin_belajar_negara_id').val(izin_belajar_negara_id);
					$('#izin_belajar_negara_id').trigger('change');

					$('#izin_belajar_status_sekolah').select2('val',izin_belajar_status_sekolah);

					$('#izin_belajar_tahun_selesai').select2('val', '');
					$('#izin_belajar_tahun_selesai').val(izin_belajar_tahun_selesai);
					$('#izin_belajar_tahun_selesai').trigger('change');

					$('#izin_belajar_status_proses').select2('val', '');
					$('#izin_belajar_status_proses').val(izin_belajar_status_proses);
					$('#izin_belajar_status_proses').trigger('change');

					$('#header_univ').change();
				});
			}
		}
		function tambah_data(){
			$('.form_lihat_data').hide();
			reset_form();
			$('.label_aksi_form').html('Form Penambahan Data izin_belajar');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					$('.form_tambah_data').slideDown('fast',function(){
						$('html, body').animate({
							scrollTop: $(".form_tambah_data").offset().top-60
						}, 500);
						
					});
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
				});
			}
		}
		function batal_data(){
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					reset_form();
				});
			}
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					reset_form();
				});
			}
		}
		function reset_form(){
			$('#izin_belajar_id').val(0);
			$('#izin_belajar_jenis').select2('val',' ');
			$('#izin_belajar_pendidikan_id').select2('val',' ');
			$('#izin_belajar_negara_tipe').select2('val',' ');
			$('#izin_belajar_nama_sekolah').val('');
			$('#izin_belajar_kota_nama').val('');
			$('#izin_belajar_negara_id').select2('val',' ');
			$('#izin_belajar_status_sekolah').select2('val',' ');
			$('#izin_belajar_sponsor').val('');
			$('#izin_belajar_id').val(0);
			$('#izin_belajar_tmt_izin').val('');
			$('#izin_belajar_tahun_selesai').select2('val',' ');
			$('#izin_belajar_tanggal_selesai').val('');
			$('#izin_belajar_status_proses').select2('val',' ');
			$('#izin_belajar_tanggal_surat').val('');
			$('#izin_belajar_nomor_surat').val('');
			$('#izin_belajar_jabatan_pemberi_izin').val('');
			$('.validation-error-label').css('display','none');
		}
		function  get_tingkat_pendidikan(ini,nextopsi) {
			var pendidikan_kode = $('option:selected', ini).attr('data-kode');
			$.ajax({
				url: base_url+'kepegawaian/pendidikan_formal/get_tingkat_pendidikan',
				type: "post",
				dataType: 'json',
				data: { pendidikan_kode: pendidikan_kode},
				success: function (data) {
					var isUpdate = parseInt($('#isUpdate').val());
					var izin_belajar_jurusan_code_tingkat_temp = $('#izin_belajar_jurusan_code_tingkat_temp').val();
					var tingkat_pendidikan = data.tingkat_pendidikan;
					var newoption = "<option value=''></option>";
					for(var i = 0; i < tingkat_pendidikan.length; i ++){
						newoption+='<option value="'+tingkat_pendidikan[i].jurusan_kode+'" data-jenjang="'+tingkat_pendidikan[i].jurusan_jenjang+'" '+((tingkat_pendidikan.length==1 || (isUpdate==1 && izin_belajar_jurusan_code_tingkat_temp == tingkat_pendidikan[i].jurusan_kode))?"selected":"")+'>'+tingkat_pendidikan[i].jurusan_grup+'</option>';
					}
					$('#'+nextopsi).html(newoption);
					$('#'+nextopsi).change();
					// get_fakultas($('#izin_belajar_tingkat_pendidikan'),'izin_belajar_fakultas');
				}
			});
		}
		// function  get_fakultas(ini,nextopsi) {
		// 	var jurusan_kode = $(ini).val();
		// 	$.ajax({
		// 		url: base_url+'kepegawaian/izin_belajar/get_fakultas',
		// 		type: "post",
		// 		dataType: 'json',
		// 		data: { jurusan_kode: jurusan_kode},
		// 		success: function (data) {
		// 			var isUpdate = parseInt($('#isUpdate').val());
		// 			var izin_belajar_jurusan_code_fakultas_temp = $('#izin_belajar_jurusan_code_fakultas_temp').val();

		// 			var jurusan = data.jurusan;
		// 			var newoption = "<option value=''></option>";

		// 			for(var i = 0; i < jurusan.length; i ++){
		// 				newoption+='<option value="'+jurusan[i].jurusan_kode+'" '+(((isUpdate==1 && izin_belajar_jurusan_code_fakultas_temp == jurusan[i].jurusan_kode))?"selected":"")+'>'+jurusan[i].jurusan_nama+'</option>';
		// 			}
		// 			$('#'+nextopsi).html(newoption);
		// 			get_jurusan($('#izin_belajar_fakultas'),'izin_belajar_jurusan');
		// 		}
		// 	});
		// }
		// function  get_jurusan(ini,nextopsi) {
		// 	var jurusan_kode = $(ini).val();
		// 	$.ajax({
		// 		url: base_url+'kepegawaian/izin_belajar/get_jurusan',
		// 		type: "post",
		// 		dataType: 'json',
		// 		data: { jurusan_kode: jurusan_kode},
		// 		success: function (data) {
		// 			var isUpdate = parseInt($('#isUpdate').val());
		// 			var izin_belajar_jurusan_code_temp = $('#izin_belajar_jurusan_code_temp').val();
					
		// 			var jurusan = data.jurusan;
		// 			var newoption = "<option value=''></option>";

		// 			for(var i = 0; i < jurusan.length; i ++){
		// 				if(jurusan[i].jurusan_grup != jurusan[i].jurusan_nama){
		// 					newoption+='<option value="'+jurusan[i].jurusan_kode+'" '+(((isUpdate==1 && izin_belajar_jurusan_code_temp == jurusan[i].jurusan_kode))?"selected":"")+'>'+jurusan[i].jurusan_nama+'</option>';
		// 				}
		// 			}
		// 			$('#'+nextopsi).html(newoption);
		// 		}
		// 	});
		// }
		$(document).ready(function() {
			$('.pickizinbelajar').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
			});
		});

		// function  get_prodi(ini,nextopsi) {
		// 	var univ_id = $(ini).val();
		// 	var oldvalue = $(ini).attr('data-oldvalue');
		// 	var prodinya = ($('#prodinya').val());
		// 	// alert(univ_id);
		// 	$.ajax({
		// 		url: base_url+'kepegawaian/pendidikan_formal/get_prodi',
		// 		type: "post",
		// 		dataType: 'json',
		// 		data: { univ_id: univ_id},
		// 		success: function (data) {
		// 			var isUpdate = parseInt($('#isUpdate').val());
		// 			var univ_temp = $('#univ_temp').val();

		// 			var prodi = data.prodi;
		// 			var newoption = "<option=''></option>";
		// 			for(var i = 0; i < prodi.length; i ++){
		// 				alert(prodi.prodi_nama);
		// 				newoption+='<option value="'+prodi[i].prodi_id+'" '+((prodi.length==1 || (isUpdate==1 && univ_temp == prodi[i].prodi_id))?"selected":"")+'>'+prodi[i].prodi_nama+'</option>';
		// 			}
		// 			$('#'+nextopsi).html(newoption);
		// 			// $('#'+nextopsi).change();
		// 		}
		// 	});
		// }
		$('#header_sd').on('change', function() {
			var sekolah_sd_id = $(this).val();
			$.ajax({
				url: base_url+'kepegawaian/pendidikan_formal/getDataSD',
				type: "post",
				dataType: 'json',
				data: { sekolah_sd_id: sekolah_sd_id},
				success: function (data) {
					if(data.sekolah_sd!=null){
						$('#izin_belajar_sekolah_universitas_id').val(data.sekolah_sd.sekolah_sd_id);
						$('#izin_belajar_nama_sekolah').val(data.sekolah_sd.sekolah_sd_nama);
					}else{
						$('#izin_belajar_nama_sekolah').val('');
						$('#izin_belajar_sekolah_universitas_id').val('');
					}
				}
			});
		});
		$('#header_smp').on('change', function() {
			var sekolah_smp_id = $(this).val();
			$.ajax({
				url: base_url+'kepegawaian/pendidikan_formal/getDataSMP',
				type: "post",
				dataType: 'json',
				data: { sekolah_smp_id: sekolah_smp_id},
				success: function (data) {
					if(data.sekolah_smp!=null){
						$('#izin_belajar_sekolah_universitas_id').val(data.sekolah_smp.sekolah_smp_id);
						$('#izin_belajar_nama_sekolah').val(data.sekolah_smp.sekolah_smp_nama);
					}else{
						$('#izin_belajar_sekolah_universitas_id').val('');
						$('#izin_belajar_nama_sekolah').val('');
					}
				}
			});
		});
		$('#header_sma').on('change', function() {
			var sekolah_sma_id = $(this).val();
			$.ajax({
				url: base_url+'kepegawaian/pendidikan_formal/getDataSMA',
				type: "post",
				dataType: 'json',
				data: { sekolah_sma_id: sekolah_sma_id},
				success: function (data) {
					if(data.sekolah_sma!=null){
						$('#izin_belajar_sekolah_universitas_id').val(data.sekolah_sma.sekolah_sma_id);
						$('#izin_belajar_nama_sekolah').val(data.sekolah_sma.sekolah_sma_nama);
					}else{
						$('#izin_belajar_sekolah_universitas_id').val('');
						$('#izin_belajar_nama_sekolah').val('');
					}
				}
			});
		});
		$('#header_univ').on('change', function() {
			var univ_id = $(this).val();
			var univ_temp = $('#izin_belajar_sekolah_universitas_temp').val();
			// alert(univ_id);
       		var jenjang  = $('#izin_belajar_tingkat_pendidikan option:selected').attr('data-jenjang');
			$.ajax({
				url: base_url+'kepegawaian/pendidikan_formal/get_prodi',
				type: "post",
				dataType: 'json',
				data: { univ_id: univ_id, jenjang: jenjang},
				success: function (data) {
					var isUpdate = parseInt($('#isUpdate').val());
					var izin_belajar_prodi_temp = $('#izin_belajar_prodi_temp').val();
					var pro_studi_text = $('#pro_studi_text').val();
					var prodi_jenjang = $('#prodi_jenjang').val();

					// alert(prodi_jenjang);
					var prodi = data.prodi;
					// if(isUpdate == 1 && izin_belajar_prodi_temp !=''){
					// 	alert(izin_belajar_prodi_temp);
					// var newoption = "<option value='"+izin_belajar_prodi_temp+"'>"+pro_studi_text+" - "+prodi_jenjang+"</option>";
					// for(var i = 0; i < prodi.length; i ++){
					// 	newoption+='<option value="'+prodi[i].prodi_id+'">'+prodi[i].prodi_nama+' - '+prodi[i].prodi_jenjang+'</option>';
					// }
					// }
					// var newoption = "<option value='"+izin_belajar_prodi_temp+"'>"+pro_studi_text+" - "+prodi_jenjang+"</option>";
					var newoption = "<option=''></option>";
					for(var i = 0; i < prodi.length; i ++){
						// alert(prodi[i].prodi_nama);
						newoption+='<option value="'+prodi[i].prodi_id+'" '+((prodi.length==1 || (isUpdate==1 && izin_belajar_prodi_temp == prodi[i].prodi_id))?"selected":"")+'>'+prodi[i].prodi_nama+' - '+prodi[i].prodi_jenjang+'</option>';
					}

					$('#izin_belajar_prodi_id').html(newoption);
					$('#izin_belajar_prodi_id').change();
					// $('#'+nextopsi).html(newoption);
					// $('#'+nextopsi).change();
				}
			});
			$.ajax({
				url: base_url+'kepegawaian/pendidikan_formal/getDataUNIV',
				type: "post",
				dataType: 'json',
				data: { univ_id: univ_id},
				success: function (data) {
					if(data.univ!=null){
						$('#izin_belajar_sekolah_universitas_id').val(data.univ.univ_id);
						$('#izin_belajar_nama_sekolah').val(data.univ.univ_nama);
					}else{
						$('#izin_belajar_sekolah_universitas_id').val('');
						$('#izin_belajar_nama_sekolah').val('a');
					}
				}
			});
		});
		
		$(document).ready(function() {
			$( "#pendidikan_formal_pendidikan_id").rules( "add", {
				required: true,
				remote: {
					url: base_url+'kepegawaian/pendidikan_formal/check_tingat_pendidikan',
					type: "post",
					data: {
						pendidikan_formal_pendidikan_id: function() {
							return $( "#pendidikan_formal_pendidikan_id" ).val();
						},
						pendidikan_formal_pegawai_id: function() {
							return $( "#pendidikan_formal_pegawai_id" ).val();
						},
						isUpdate: function() {
							return $( "#isUpdate" ).val();
						}					
					}
				},
				messages: {
					required: "Wajib Diisi",
					remote: "Pendidikan Telah Digunakan"
				}
			});
		});
      	$("#header_sd").select2({
			ajax: {
				url: base_url+"kepegawaian/pendidikan_formal/getDataListSD",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						q: params.term, 
						page: params.page,
						
					};
				},
				processResults: function (data, params) {
					params.page = params.page || 1;

					return {
						results: data.items,
						pagination: {
							more: (params.page * 5) < data.total_count
						}
					};
				},
				cache: true
			},
			containerCssClass: 'select-xs',
			placeholder: 'Pencarian Sekolah SD',
			escapeMarkup: function (markup) { return markup; }, 
			minimumInputLength: 2,
			templateResult: formatRepoSD,
			templateSelection: formatRepoSDSelection,

		});

		function formatRepoSD (repo) {
			if (repo.loading) {
				return repo.text;
			}

			var markup = "<div class='select2-result-repository clearfix'>" +
			"<div class='select2-result-repository__title'>" + repo.sekolah_sd_nama + "</div>"+
			"<div class='select2-result-repository__description'>NPSN : " + repo.sekolah_sd_npsn + "<br>Alamat : " + repo.sekolah_sd_alamat + "</div>"+
			"</div>";
			
			return markup;
		}
		function formatRepoSDSelection (repo) {
			return repo.sekolah_sd_nama || repo.text;
		}

		$("#header_smp").select2({
			ajax: {
				url: base_url+"kepegawaian/pendidikan_formal/getDataListSMP",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						q: params.term, 
						page: params.page,
						
					};
				},
				processResults: function (data, params) {
					params.page = params.page || 1;

					return {
						results: data.items,
						pagination: {
							more: (params.page * 5) < data.total_count
						}
					};
				},
				cache: true
			},
			containerCssClass: 'select-xs',
			placeholder: 'Pencarian Sekolah SMP',
			escapeMarkup: function (markup) { return markup; }, 
			minimumInputLength: 2,
			templateResult: formatRepoSMP,
			templateSelection: formatRepoSMPSelection,

		});

		function formatRepoSMP (repo) {
			if (repo.loading) {
				return repo.text;
			}

			var markup = "<div class='select2-result-repository clearfix'>" +
			"<div class='select2-result-repository__title'>" + repo.sekolah_smp_nama + "</div>"+
			"<div class='select2-result-repository__description'>NPSN : " + repo.sekolah_smp_npsn + "<br>Alamat : " + repo.sekolah_smp_alamat + "</div>"+
			"</div>";
			
			return markup;
		}
		function formatRepoSMPSelection (repo) {
			return repo.sekolah_smp_nama || repo.text;
		}

		$("#header_sma").select2({
			ajax: {
				url: base_url+"kepegawaian/pendidikan_formal/getDataListSMA",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						q: params.term, 
						page: params.page,
						
					};
				},
				processResults: function (data, params) {
					params.page = params.page || 1;

					return {
						results: data.items,
						pagination: {
							more: (params.page * 5) < data.total_count
						}
					};
				},
				cache: true
			},
			containerCssClass: 'select-xs',
			placeholder: 'Pencarian Sekolah SMA',
			escapeMarkup: function (markup) { return markup; }, 
			minimumInputLength: 2,
			templateResult: formatRepoSMA,
			templateSelection: formatRepoSMASelection,

		});

		function formatRepoSMA (repo) {
			if (repo.loading) {
				return repo.text;
			}

			var markup = "<div class='select2-result-repository clearfix'>" +
			"<div class='select2-result-repository__title'>" + repo.sekolah_sma_nama + "</div>"+
			"<div class='select2-result-repository__description'>NPSN : " + repo.sekolah_sma_npsn + "<br>Alamat : " + repo.sekolah_sma_alamat +' '+ repo.kab + "</div>"+
			"</div>";
			
			return markup;
		}
		function formatRepoSMASelection (repo) {
			return repo.sekolah_sma_nama || repo.text;
		}

		$("#header_univ").select2({
			ajax: {
				url: base_url+"kepegawaian/pendidikan_formal/getDataListUNIV",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						q: params.term, 
						page: params.page,
						
					};
				},
				processResults: function (data, params) {
					params.page = params.page || 1;

					return {
						results: data.items,
						pagination: {
							more: (params.page * 5) < data.total_count
						}
					};
				},
				cache: true
			},
			containerCssClass: 'select-xs',
			placeholder: 'Pencarian Universitas',
			escapeMarkup: function (markup) { return markup; }, 
			minimumInputLength: 2,
			templateResult: formatRepoUNIV,
			templateSelection: formatRepoUNIVSelection,

		});

		function formatRepoUNIV (repo) {
			if (repo.loading) {
				return repo.text;
			}

			var markup = "<div class='select2-result-repository clearfix'>" +
			"<div class='select2-result-repository__title'>" + repo.univ_nama + "</div>"+
			"<div class='select2-result-repository__description'>NPSN : " + repo.univ_npsn + "<br>Alamat : " + repo.univ_alamat + "</div>"+
			"</div>";
			
			return markup;
		}
		function formatRepoUNIVSelection (repo) {
			return repo.univ_nama || repo.text;
		}
	</script>
