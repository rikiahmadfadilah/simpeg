<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Mutasi Unit Kerja</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nip"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tempat Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_tempat_lahir"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Jabatan</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama_jabatan"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Unit Kerja</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["unit_kerja_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<a href="<?php echo base_url();?>kepegawaian/mutasi_uker" class="btn btn-xs btn-warning">Kembali</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Daftar Data Mutasi Unit Kerja</span>
					<div class="col-md-12">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
							<thead>
								<tr>
									<th class="text-center">Tanggal Mutasi Unit Kerja</th>
									<th class="text-center">Mutasi Unit Kerja Sebelum</th>
									<th class="text-center">Mutasi Unit Kerja Sesudah</th>
									<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($history)>0){?>
									<?php foreach ($history as $h) {  ?>
										<tr>
											<td><?php echo $h["mutasi_uker_tanggal"];?></td>
											<td><?php echo $h["mutasi_uker_nama_sebelum"];?></td>
											<td><?php echo $h["mutasi_uker_nama_sesudah"];?></td>
											<td>
												<ul class="icons-list">
													<li><a href="javascript:void(0)" onclick="update_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
													<li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>
													<li><a onclick="set_non_aktif(<?php echo $h["mutasi_uker_id"];?>,<?php echo $h["mutasi_uker_pegawai_id"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
												</ul>
											</td>
										</tr>
									<?php } ?>
								<?php } else{ ?>
									<tr>
										<td class="text-center" colspan="7">Data Belum Tersedia</td>
									</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<td class="text-right" colspan="7"><a href="javascript:void(0)" onclick="tambah_data();" class="btn btn-xs btn-primary btn-tfoot-table">Tambah Data</a>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="col-md-12 form_tambah_data" style="display: none;">
						<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan Data Mutasi Unit Kerja</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tanggal Mutasi Unit Kerja</label>
									<div class="col-md-9">
										<input type="text" name="mutasi_uker_tanggal" id="mutasi_uker_tanggal" class="form-control input-xs wajib pickmutasiuker">
									</div>
								</div>
								<!-- <div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Mutasi Unit Kerja Sebelum</label>
									<div class="col-md-9">
										<select name="mutasi_uker_unit_kerja_id_sebelum" id="mutasi_uker_unit_kerja_id_sebelum" data-placeholder="Mutasi Unit Kerja Sebelum" class="select-size-xs wajib">
											<option value=""></option>
											<?php foreach ($unit_kerja as $i) {
												echo '<option value="'.$i["unit_kerja_id"].'" data-kode="'.$i["unit_kerja_id"].'">'.$i["unit_kerja_nama"].'</option>';
											}?>
										</select>
									</div>
								</div> -->
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Mutasi Unit Kerja Sebelum</label>
									<div class="col-lg-9">
										<input type="text" value="<?php echo $pegawai["unit_kerja_nama"];?>" class="form-control input-xs wajib" readonly="readonly">
										<input type="hidden" id="mutasi_uker_unit_kerja_id_sebelum" name="mutasi_uker_unit_kerja_id_sebelum" value="<?php echo $pegawai["pegawai_unit_kerja_id"];?>" class="form-control input-xs wajib" readonly="readonly">
									</div>
								</div>
								<!-- <div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Mutasi Unit Kerja Sebelum</label>
									<div class="col-lg-9">
										<select name="mutasi_uker_unit_kerja_id_sebelum[]" id="mutasi_uker_unit_kerja_id_sebelum_1" data-placeholder="Mutasi Unit Kerja Sebelum" data-id="0" class="select-size-xs ga_wajib" onchange="get_unit_kerja_hirarki_sebelum(this,1)">
											<option></option>
											<?php foreach ($unit_kerja as $i) {
												echo '<option value="'.$i["unit_kerja_id"].'" parent_id_kode="'.$i["unit_kerja_id_kode"].'">'.$i["unit_kerja_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs mutasi_uker_unit_kerja_id_sebelum_2" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="mutasi_uker_unit_kerja_id_sebelum[]" id="mutasi_uker_unit_kerja_id_sebelum_2" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs"  onchange="get_unit_kerja_hirarki_sebelum(this,2)">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs mutasi_uker_unit_kerja_id_sebelum_3" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="mutasi_uker_unit_kerja_id_sebelum[]" id="mutasi_uker_unit_kerja_id_sebelum_3" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs"  onchange="get_unit_kerja_hirarki_sebelum(this,3)">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs mutasi_uker_unit_kerja_id_sebelum_4" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="mutasi_uker_unit_kerja_id_sebelum[]" id="mutasi_uker_unit_kerja_id_sebelum_4" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs"  onchange="get_unit_kerja_hirarki_sebelum(this,4)">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs mutasi_uker_unit_kerja_id_sebelum_5" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="mutasi_uker_unit_kerja_id_sebelum[]" id="mutasi_uker_unit_kerja_id_sebelum_5" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs" onchange="get_unit_kerja_hirarki_sebelum(this,5)">
											<option></option>
										</select>
									</div>
								</div> -->
								<!-- <div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Mutasi Unit Kerja Sesudah</label>
									<div class="col-md-9">
										<select name="mutasi_uker_unit_kerja_id_sesudah" id="mutasi_uker_unit_kerja_id_sesudah" data-placeholder="Mutasi Unit Kerja Sesudah" class="select-size-xs wajib">
											<option value=""></option>
											<?php foreach ($unit_kerja as $i) {
												echo '<option value="'.$i["unit_kerja_id"].'" data-kode="'.$i["unit_kerja_id"].'">'.$i["unit_kerja_nama"].'</option>';
											}?>
										</select>
									</div>
								</div> -->
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Mutasi Unit Kerja Sesudah</label>
									<div class="col-lg-9">
										<select name="mutasi_uker_unit_kerja_id_sesudah[]" id="mutasi_uker_unit_kerja_id_sesudah_1" data-placeholder="Mutasi Unit Kerja Sesudah" data-id="0" class="select-size-xs ga_wajib" onchange="get_unit_kerja_hirarki_sesudah(this,1)">
											<option></option>
											<?php foreach ($unit_kerja as $i) {
												echo '<option value="'.$i["unit_kerja_id"].'" parent_id_kode="'.$i["unit_kerja_id_kode"].'">'.$i["unit_kerja_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs mutasi_uker_unit_kerja_id_sesudah_2" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="mutasi_uker_unit_kerja_id_sesudah[]" id="mutasi_uker_unit_kerja_id_sesudah_2" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs"  onchange="get_unit_kerja_hirarki_sesudah(this,2)">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs mutasi_uker_unit_kerja_id_sesudah_3" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="mutasi_uker_unit_kerja_id_sesudah[]" id="mutasi_uker_unit_kerja_id_sesudah_3" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs"  onchange="get_unit_kerja_hirarki_sesudah(this,3)">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs mutasi_uker_unit_kerja_id_sesudah_4" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="mutasi_uker_unit_kerja_id_sesudah[]" id="mutasi_uker_unit_kerja_id_sesudah_4" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs"  onchange="get_unit_kerja_hirarki_sesudah(this,4)">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs mutasi_uker_unit_kerja_id_sesudah_5" style="display:none;">
									<div class="col-lg-9 col-lg-offset-3">
										<select name="mutasi_uker_unit_kerja_id_sesudah[]" id="mutasi_uker_unit_kerja_id_sesudah_5" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs" onchange="get_unit_kerja_hirarki_sesudah(this,5)">
											<option></option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<div class="col-md-9 col-md-offset-3">
										<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
										<input type="hidden" name="mutasi_uker_pegawai_id" id="mutasi_uker_pegawai_id" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_id"];?>">
										<input type="hidden" name="mutasi_uker_pegawai_nip" id="mutasi_uker_pegawai_nip" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_nip"];?>">
										<input type="hidden" name="isUpdate" id="isUpdate" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="mutasi_uker_id" id="mutasi_uker_id" class="form-control input-xs wajib" value="0">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 form_lihat_data" style="display: none;">
						<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Lihat Data Mutasi Unit Kerja</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tanggal Mutasi Unit Kerja</label>
									<div class="col-md-9">
										<div class="form-control-static mutasi_uker_tanggal">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Mutasi Unit Kerja Sebelum</label>
									<div class="col-md-9">
										<div class="form-control-static mutasi_uker_nama_sebelum">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Mutasi Unit Kerja Sesudah</label>
									<div class="col-md-9">
										<div class="form-control-static mutasi_uker_nama_sesudah">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<div class="col-md-9 col-md-offset-3">
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>
<script type="text/javascript">
	function set_non_aktif(id,pegawai_id){
		$('#text_konfirmasi').html('Anda yakin menghapus data ini ?');
		$('#konfirmasipenyimpanan').modal('show');
		$('#setujukonfirmasibutton').unbind();
		$('#setujukonfirmasibutton').on('click', function () {
			$('#konfirmasipenyimpanan').modal('hide');
			$('.angkadoank').inputmask('remove');
			$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
			window.location.href = base_url+'kepegawaian/mutasi_uker/hapus/'+id+'/'+pegawai_id;
		});
	}
	function lihat_data(data){
		$('.form_tambah_data').hide();
		var decoded = $("<div/>").html(data).text();
		var datajson = $.parseJSON(decoded);
		reset_form();
		if($('.form_lihat_data').is(":visible")){
			$('.form_lihat_data').slideUp('slow',function(){
				lihat_data(data);
			});
		}else{
			$('.form_lihat_data').slideDown('fast',function(){
				$('html, body').animate({
					scrollTop: $(".form_lihat_data").offset().top-60
				}, 500);
				var mutasi_uker_id = datajson.mutasi_uker_id;
				var mutasi_uker_tanggal = datajson.mutasi_uker_tanggal;
				var mutasi_uker_nama_sebelum = datajson.mutasi_uker_nama_sebelum;
				var mutasi_uker_nama_sesudah = datajson.mutasi_uker_nama_sesudah;

				$('.mutasi_uker_tanggal').html(mutasi_uker_tanggal);
				$('.mutasi_uker_nama_sebelum').html(mutasi_uker_nama_sebelum);
				$('.mutasi_uker_nama_sesudah').html(mutasi_uker_nama_sesudah);
			});
		}
	}
	function update_data(data){
		var decoded = $("<div/>").html(data).text();
		var datajson = $.parseJSON(decoded);
		reset_form();
		$('.form_lihat_data').hide();
		$('.label_aksi_form').html('Form Perubahan Data Kemampuan Bahasa Asing');
		if($('.form_tambah_data').is(":visible")){
			$('.form_tambah_data').slideUp('slow',function(){
				update_data(data);
			});
		}else{
			$('.form_tambah_data').slideDown('fast',function(){
				$('html, body').animate({
					scrollTop: $(".form_tambah_data").offset().top-60
				}, 500);
				$('#isUpdate').val(1);
					var mutasi_uker_unit_kerja_id_sebelum_5 = datajson.mutasi_uker_unit_kerja_id_sebelum_5;
					var mutasi_uker_unit_kerja_id_sebelum_4 = datajson.mutasi_uker_unit_kerja_id_sebelum_4;
					var mutasi_uker_unit_kerja_id_sebelum_3 = datajson.mutasi_uker_unit_kerja_id_sebelum_3;
					var mutasi_uker_unit_kerja_id_sebelum_2 = datajson.mutasi_uker_unit_kerja_id_sebelum_2;
					var mutasi_uker_unit_kerja_id_sebelum = datajson.mutasi_uker_unit_kerja_id_sebelum;
					if(mutasi_uker_unit_kerja_id_sebelum_5 != null && mutasi_uker_unit_kerja_id_sebelum_5 != 1){
						$('#mutasi_uker_unit_kerja_id_sebelum_1').attr('data-id', mutasi_uker_unit_kerja_id_sebelum_4);
						$('#mutasi_uker_unit_kerja_id_sebelum_2').attr('data-id', mutasi_uker_unit_kerja_id_sebelum_3);
						$('#mutasi_uker_unit_kerja_id_sebelum_3').attr('data-id', mutasi_uker_unit_kerja_id_sebelum_2);
						$('#mutasi_uker_unit_kerja_id_sebelum_4').attr('data-id', mutasi_uker_unit_kerja_id_sebelum);

						$("#mutasi_uker_unit_kerja_id_sebelum_1").val(mutasi_uker_unit_kerja_id_sebelum_5);
						$('#mutasi_uker_unit_kerja_id_sebelum_1').trigger('change');						
					}

					else{
						if(mutasi_uker_unit_kerja_id_sebelum_4 != null && mutasi_uker_unit_kerja_id_sebelum_4 != 1){		
							$('#mutasi_uker_unit_kerja_id_sebelum_1').attr('data-id', mutasi_uker_unit_kerja_id_sebelum_3);
							$('#mutasi_uker_unit_kerja_id_sebelum_2').attr('data-id', mutasi_uker_unit_kerja_id_sebelum_2);
							$('#mutasi_uker_unit_kerja_id_sebelum_3').attr('data-id', mutasi_uker_unit_kerja_id_sebelum);

							$("#mutasi_uker_unit_kerja_id_sebelum_1").val(mutasi_uker_unit_kerja_id_sebelum_4);
							$('#mutasi_uker_unit_kerja_id_sebelum_1').trigger('change');
							
						}
						else{
							if(mutasi_uker_unit_kerja_id_sebelum_3 != null && mutasi_uker_unit_kerja_id_sebelum_3 != 1){
								$('#mutasi_uker_unit_kerja_id_sebelum_1').attr('data-id', mutasi_uker_unit_kerja_id_sebelum_2);
								$('#mutasi_uker_unit_kerja_id_sebelum_2').attr('data-id', mutasi_uker_unit_kerja_id_sebelum);

								$("#mutasi_uker_unit_kerja_id_sebelum_1").val(mutasi_uker_unit_kerja_id_sebelum_3);
								$('#mutasi_uker_unit_kerja_id_sebelum_1').trigger('change');
								
							}
							else{
								if(mutasi_uker_unit_kerja_id_sebelum_2 != null && mutasi_uker_unit_kerja_id_sebelum_2 != 1){
									$('#mutasi_uker_unit_kerja_id_sebelum_1').attr('data-id', mutasi_uker_unit_kerja_id_sebelum);

									$("#mutasi_uker_unit_kerja_id_sebelum_1").val(mutasi_uker_unit_kerja_id_sebelum_2);
									$('#mutasi_uker_unit_kerja_id_sebelum_1').trigger('change');
									
								}
								else{
									if(mutasi_uker_unit_kerja_id_sebelum != null && mutasi_uker_unit_kerja_id_sebelum != 1){
										$("#mutasi_uker_unit_kerja_id_sebelum_1").val(mutasi_uker_unit_kerja_id_sebelum);
										$('#mutasi_uker_unit_kerja_id_sebelum_1').trigger('change');
									}
								}
							}
						}
					}
					var mutasi_uker_unit_kerja_id_sesudah_5 = datajson.mutasi_uker_unit_kerja_id_sesudah_5;
					var mutasi_uker_unit_kerja_id_sesudah_4 = datajson.mutasi_uker_unit_kerja_id_sesudah_4;
					var mutasi_uker_unit_kerja_id_sesudah_3 = datajson.mutasi_uker_unit_kerja_id_sesudah_3;
					var mutasi_uker_unit_kerja_id_sesudah_2 = datajson.mutasi_uker_unit_kerja_id_sesudah_2;
					var mutasi_uker_unit_kerja_id_sesudah = datajson.mutasi_uker_unit_kerja_id_sesudah;
					if(mutasi_uker_unit_kerja_id_sesudah_5 != null && mutasi_uker_unit_kerja_id_sesudah_5 != 1){
						$('#mutasi_uker_unit_kerja_id_sesudah_1').attr('data-id', mutasi_uker_unit_kerja_id_sesudah_4);
						$('#mutasi_uker_unit_kerja_id_sesudah_2').attr('data-id', mutasi_uker_unit_kerja_id_sesudah_3);
						$('#mutasi_uker_unit_kerja_id_sesudah_3').attr('data-id', mutasi_uker_unit_kerja_id_sesudah_2);
						$('#mutasi_uker_unit_kerja_id_sesudah_4').attr('data-id', mutasi_uker_unit_kerja_id_sesudah);

						$("#mutasi_uker_unit_kerja_id_sesudah_1").val(mutasi_uker_unit_kerja_id_sesudah_5);
						$('#mutasi_uker_unit_kerja_id_sesudah_1').trigger('change');						
					}

					else{
						if(mutasi_uker_unit_kerja_id_sesudah_4 != null && mutasi_uker_unit_kerja_id_sesudah_4 != 1){		
							$('#mutasi_uker_unit_kerja_id_sesudah_1').attr('data-id', mutasi_uker_unit_kerja_id_sesudah_3);
							$('#mutasi_uker_unit_kerja_id_sesudah_2').attr('data-id', mutasi_uker_unit_kerja_id_sesudah_2);
							$('#mutasi_uker_unit_kerja_id_sesudah_3').attr('data-id', mutasi_uker_unit_kerja_id_sesudah);

							$("#mutasi_uker_unit_kerja_id_sesudah_1").val(mutasi_uker_unit_kerja_id_sesudah_4);
							$('#mutasi_uker_unit_kerja_id_sesudah_1').trigger('change');
							
						}
						else{
							if(mutasi_uker_unit_kerja_id_sesudah_3 != null && mutasi_uker_unit_kerja_id_sesudah_3 != 1){
								$('#mutasi_uker_unit_kerja_id_sesudah_1').attr('data-id', mutasi_uker_unit_kerja_id_sesudah_2);
								$('#mutasi_uker_unit_kerja_id_sesudah_2').attr('data-id', mutasi_uker_unit_kerja_id_sesudah);

								$("#mutasi_uker_unit_kerja_id_sesudah_1").val(mutasi_uker_unit_kerja_id_sesudah_3);
								$('#mutasi_uker_unit_kerja_id_sesudah_1').trigger('change');
								
							}
							else{
								if(mutasi_uker_unit_kerja_id_sesudah_2 != null && mutasi_uker_unit_kerja_id_sesudah_2 != 1){
									$('#mutasi_uker_unit_kerja_id_sesudah_1').attr('data-id', mutasi_uker_unit_kerja_id_sesudah);

									$("#mutasi_uker_unit_kerja_id_sesudah_1").val(mutasi_uker_unit_kerja_id_sesudah_2);
									$('#mutasi_uker_unit_kerja_id_sesudah_1').trigger('change');
									
								}
								else{
									if(mutasi_uker_unit_kerja_id_sesudah != null && mutasi_uker_unit_kerja_id_sesudah != 1){
										$("#mutasi_uker_unit_kerja_id_sesudah_1").val(mutasi_uker_unit_kerja_id_sesudah);
										$('#mutasi_uker_unit_kerja_id_sesudah_1').trigger('change');
									}
								}
							}
						}
					}
				var mutasi_uker_id = datajson.mutasi_uker_id;
				var mutasi_uker_tanggal = datajson.mutasi_uker_tanggal;
				// var mutasi_uker_unit_kerja_id_sebelum = datajson.mutasi_uker_unit_kerja_id_sebelum;
				// var mutasi_uker_unit_kerja_id_sesudah = datajson.mutasi_uker_unit_kerja_id_sesudah;
				
				$('#mutasi_uker_id').val(mutasi_uker_id);
				$('#mutasi_uker_tanggal').val(mutasi_uker_tanggal);
				// $('#mutasi_uker_unit_kerja_id_sebelum').val(mutasi_uker_unit_kerja_id_sebelum);

				// $('#mutasi_uker_unit_kerja_id_sebelum').select2('val', '');
				// $('#mutasi_uker_unit_kerja_id_sebelum').val(mutasi_uker_unit_kerja_id_sebelum);
				// $('#mutasi_uker_unit_kerja_id_sebelum').trigger('change');

				// $('#mutasi_uker_unit_kerja_id_sesudah').select2('val', '');
				// $('#mutasi_uker_unit_kerja_id_sesudah').val(mutasi_uker_unit_kerja_id_sesudah);
				// $('#mutasi_uker_unit_kerja_id_sesudah').trigger('change');

				$('#mutasi_uker_id').val(datajson.mutasi_uker_id);
			});
		}
	}
	function tambah_data(){
		$('.form_lihat_data').hide();
		reset_form();
		$('.label_aksi_form').html('Form Penambahan Data Mutasi Unit Kerja');
		if($('.form_tambah_data').is(":visible")){
			$('.form_tambah_data').slideUp('slow',function(){
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
					
				});
			});
		}else{
			$('.form_tambah_data').slideDown('fast',function(){
				$('html, body').animate({
					scrollTop: $(".form_tambah_data").offset().top-60
				}, 500);
			});
		}
	}
	function batal_data(){
		if($('.form_tambah_data').is(":visible")){
			$('.form_tambah_data').slideUp('slow',function(){
				reset_form();
			});
		}
		if($('.form_lihat_data').is(":visible")){
			$('.form_lihat_data').slideUp('slow',function(){
				reset_form();
			});
		}
	}
	function reset_form(){
		// $('#mutasi_uker_id').val(0);
		$('#mutasi_uker_unit_kerja_id_sebelum_1').attr('data-id', 0);
		$('#mutasi_uker_unit_kerja_id_sebelum_2').attr('data-id', 0);
		$('#mutasi_uker_unit_kerja_id_sebelum_3').attr('data-id', 0);
		$('#mutasi_uker_unit_kerja_id_sebelum_4').attr('data-id', 0);

		$('#mutasi_uker_unit_kerja_id_sesudah_1').attr('data-id', 0);
		$('#mutasi_uker_unit_kerja_id_sesudah_2').attr('data-id', 0);
		$('#mutasi_uker_unit_kerja_id_sesudah_3').attr('data-id', 0);
		$('#mutasi_uker_unit_kerja_id_sesudah_4').attr('data-id', 0);

		$('#mutasi_uker_tanggal').val('');
		$('#mutasi_uker_unit_kerja_id_sebelum_1').select2('val', ' ');
		$('#mutasi_uker_unit_kerja_id_sesudah_1').select2('val', ' ');
		$('.validation-error-label').css('display','none');
	}
	$(document).ready(function() {

		$('.pickmutasiuker').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
	});

	function  get_unit_kerja_hirarki_sebelum(ini,lvl) {
	var attrid = $(ini).attr('data-id');
	//alert(attrid);
	if(lvl==1){
		$('.mutasi_uker_unit_kerja_id_sebelum_5').slideUp('fast',function(){
			$('#mutasi_uker_unit_kerja_id_sebelum_5').select2('val', '');
			$('.mutasi_uker_unit_kerja_id_sebelum_4').slideUp('fast',function(){
				$('#mutasi_uker_unit_kerja_id_sebelum_4').select2('val', '');
				$('.mutasi_uker_unit_kerja_id_sebelum_3').slideUp('fast',function(){
					$('#mutasi_uker_unit_kerja_id_sebelum_3').select2('val', '');
					$('.mutasi_uker_unit_kerja_id_sebelum_2').slideUp('fast',function(){
						$('#mutasi_uker_unit_kerja_id_sebelum_2').select2('val', '');
					});
				});
			});
		});
	}else if(lvl==2){
		$('.mutasi_uker_unit_kerja_id_sebelum_5').slideUp('fast',function(){
			$('#mutasi_uker_unit_kerja_id_sebelum_5').select2('val', '');
			$('.mutasi_uker_unit_kerja_id_sebelum_4').slideUp('fast',function(){
				$('#mutasi_uker_unit_kerja_id_sebelum_4').select2('val', '');
				$('.mutasi_uker_unit_kerja_id_sebelum_3').slideUp('fast',function(){
					$('#mutasi_uker_unit_kerja_id_sebelum_3').select2('val', '');
				});
			});
		});
	}else if(lvl==3){
		$('.mutasi_uker_unit_kerja_id_sebelum_5').slideUp('fast',function(){
			$('#mutasi_uker_unit_kerja_id_sebelum_5').select2('val', '');
			$('.mutasi_uker_unit_kerja_id_sebelum_4').slideUp('fast',function(){
				$('#mutasi_uker_unit_kerja_id_sebelum_4').select2('val', '');
			});
		});
	}else if(lvl==4){
		$('.mutasi_uker_unit_kerja_id_sebelum_5').slideUp('fast',function(){
			$('#mutasi_uker_unit_kerja_id_sebelum_5').select2('val', '');
		});
	}
	var parent_id_kode  = $('option:selected', ini).attr('parent_id_kode');
	// var jabatan_id  = $('option:selected', ini).attr('jabatan_id');

	// $('#unit_kerja_pegawai_jabatan_id').select2('val', '');
	// $('#unit_kerja_pegawai_jabatan_id').val(jabatan_id);
	// $('#unit_kerja_pegawai_jabatan_id').trigger('change');

	$.ajax({
		url: base_url+'kepegawaian/pns/get_unit_kerja_hirarki',
		type: "post",
		dataType: 'json',
		data: { unit_kerja_parent_id_kode: parent_id_kode, unit_kerja_level: (lvl+2)},
		success: function (data) {

			var unit_kerja = data.unit_kerja;
			var newoption = "<option></option>";

			for(var i = 0; i < unit_kerja.length; i ++){
				newoption+='<option value="'+unit_kerja[i].unit_kerja_id+'" parent_id_kode="'+unit_kerja[i].unit_kerja_id_kode+'" '+((unit_kerja[i].unit_kerja_id == attrid)?"selected":"")+'>'+unit_kerja[i].unit_kerja_nama+'</option>';
			}
			$('#mutasi_uker_unit_kerja_id_sebelum_'+(lvl+1)).html(newoption);
			if(unit_kerja.length>0){
				$('.mutasi_uker_unit_kerja_id_sebelum_'+(lvl+1)).slideDown('fast');
			}else{
				$('.mutasi_uker_unit_kerja_id_sebelum_'+(lvl+1)).slideUp('fast');
			}
			if(attrid > 1){
				$('#mutasi_uker_unit_kerja_id_sebelum_'+(lvl+1)).trigger('change');
			}
		}
	});
	}

	function  get_unit_kerja_hirarki_sesudah(ini,lvl) {
	var attrid = $(ini).attr('data-id');
	//alert(attrid);
	if(lvl==1){
		$('.mutasi_uker_unit_kerja_id_sesudah_5').slideUp('fast',function(){
			$('#mutasi_uker_unit_kerja_id_sesudah_5').select2('val', '');
			$('.mutasi_uker_unit_kerja_id_sesudah_4').slideUp('fast',function(){
				$('#mutasi_uker_unit_kerja_id_sesudah_4').select2('val', '');
				$('.mutasi_uker_unit_kerja_id_sesudah_3').slideUp('fast',function(){
					$('#mutasi_uker_unit_kerja_id_sesudah_3').select2('val', '');
					$('.mutasi_uker_unit_kerja_id_sesudah_2').slideUp('fast',function(){
						$('#mutasi_uker_unit_kerja_id_sesudah_2').select2('val', '');
					});
				});
			});
		});
	}else if(lvl==2){
		$('.mutasi_uker_unit_kerja_id_sesudah_5').slideUp('fast',function(){
			$('#mutasi_uker_unit_kerja_id_sesudah_5').select2('val', '');
			$('.mutasi_uker_unit_kerja_id_sesudah_4').slideUp('fast',function(){
				$('#mutasi_uker_unit_kerja_id_sesudah_4').select2('val', '');
				$('.mutasi_uker_unit_kerja_id_sesudah_3').slideUp('fast',function(){
					$('#mutasi_uker_unit_kerja_id_sesudah_3').select2('val', '');
				});
			});
		});
	}else if(lvl==3){
		$('.mutasi_uker_unit_kerja_id_sesudah_5').slideUp('fast',function(){
			$('#mutasi_uker_unit_kerja_id_sesudah_5').select2('val', '');
			$('.mutasi_uker_unit_kerja_id_sesudah_4').slideUp('fast',function(){
				$('#mutasi_uker_unit_kerja_id_sesudah_4').select2('val', '');
			});
		});
	}else if(lvl==4){
		$('.mutasi_uker_unit_kerja_id_sesudah_5').slideUp('fast',function(){
			$('#mutasi_uker_unit_kerja_id_sesudah_5').select2('val', '');
		});
	}
	var parent_id_kode  = $('option:selected', ini).attr('parent_id_kode');
	// var jabatan_id  = $('option:selected', ini).attr('jabatan_id');

	// $('#unit_kerja_pegawai_jabatan_id').select2('val', '');
	// $('#unit_kerja_pegawai_jabatan_id').val(jabatan_id);
	// $('#unit_kerja_pegawai_jabatan_id').trigger('change');

	$.ajax({
		url: base_url+'kepegawaian/pns/get_unit_kerja_hirarki',
		type: "post",
		dataType: 'json',
		data: { unit_kerja_parent_id_kode: parent_id_kode, unit_kerja_level: (lvl+2)},
		success: function (data) {

			var unit_kerja = data.unit_kerja;
			var newoption = "<option></option>";

			for(var i = 0; i < unit_kerja.length; i ++){
				newoption+='<option value="'+unit_kerja[i].unit_kerja_id+'" parent_id_kode="'+unit_kerja[i].unit_kerja_id_kode+'" '+((unit_kerja[i].unit_kerja_id == attrid)?"selected":"")+'>'+unit_kerja[i].unit_kerja_nama+'</option>';
			}
			$('#mutasi_uker_unit_kerja_id_sesudah_'+(lvl+1)).html(newoption);
			if(unit_kerja.length>0){
				$('.mutasi_uker_unit_kerja_id_sesudah_'+(lvl+1)).slideDown('fast');
			}else{
				$('.mutasi_uker_unit_kerja_id_sesudah_'+(lvl+1)).slideUp('fast');
			}
			if(attrid > 1){
				$('#mutasi_uker_unit_kerja_id_sesudah_'+(lvl+1)).trigger('change');
			}
		}
	});
	}

</script>
