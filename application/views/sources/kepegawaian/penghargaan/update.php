<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Penghargaan</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nip"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tempat Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_tempat_lahir"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Jabatan</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama_jabatan"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Unit Kerja</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["unit_kerja_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<a href="<?php echo base_url();?>kepegawaian/penghargaan" class="btn btn-xs btn-warning">Kembali</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Daftar Data Penghargaan</span>
					<div class="col-md-12">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
							<thead>
								<tr>
									<th class="text-center">Nama Penghargaan</th>
									<th class="text-center">Jenis Penghargaan</th>
									<th class="text-center">No SK</th>
									<th class="text-center">TGL SK</th>
									<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($history)>0){?>
									<?php foreach ($history as $h) {  ?>
										<tr>
											<td><?php echo $h["penghargaan_nama"];?></td>
											<td><?php echo $h["penghargaan_jenis_text"];?></td>
											<td><?php echo $h["penghargaan_nomor_sk"];?></td>
											<td><?php echo $h["penghargaan_tanggal_sk"];?></td>
											<td>
												<ul class="icons-list">
													<li><a href="javascript:void(0)" onclick="update_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
													<li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>
													<li><a onclick="set_non_aktif(<?php echo $h["penghargaan_id"];?>,<?php echo $h["penghargaan_pegawai_id"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
												</ul>
											</td>
										</tr>
									<?php } ?>
								<?php }else{ ?>
									<tr>
										<td class="text-center" colspan="7">Data Belum Tersedia</td>
									</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<td class="text-right" colspan="7"><a href="javascript:void(0)" onclick="tambah_data();" class="btn btn-xs btn-primary btn-tfoot-table">Tambah Data</a>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="col-md-12 form_tambah_data" style="display: none;">
						<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan Data penghargaan</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Nama Tanda Penghargaan</label>
									<div class="col-md-9">
										<input type="text" name="penghargaan_nama" id="penghargaan_nama" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Jenis Penghargaan</label>
									<div class="col-md-9">
										<select name="penghargaan_jenis" id="penghargaan_jenis" data-placeholder="Jenis Penghargaan" class="select-size-xs wajib">
											<option value=""></option>
											<option value="1">SLKS 10 THN</option>
											<option value="2">SLKS 20 THN</option>
											<option value="3">SLKS 30 THN</option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">No SK</label>
									<div class="col-md-9">
										<input type="text" name="penghargaan_nomor_sk" id="penghargaan_nomor_sk" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tanggal SK</label>
									<div class="col-md-9">
										<input type="text" name="penghargaan_tanggal_sk" id="penghargaan_tanggal_sk" class="form-control input-xs wajib pickpenghargaan">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tahun</label>
									<div class="col-md-9">
										<select name="penghargaan_tahun" id="penghargaan_tahun" data-placeholder="Tahun" class="select-size-xs wajib">
											<option value=""></option>
											<?php
											for($i=2019;$i>=1950;$i--) {
												echo '<option value="'.$i.'">'.$i.'</option>';
											}
											?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Pemberi</label>
									<div class="col-md-9">
										<input type="text" name="penghargaan_pemberi" id="penghargaan_pemberi" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Jabatan</label>
									<div class="col-md-9">
										<input type="text" name="penghargaan_jabatan" id="penghargaan_jabatan" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<div class="col-md-9 col-md-offset-3">
										<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
										<input type="hidden" name="penghargaan_pegawai_id" id="penghargaan_pegawai_id" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_id"];?>">
										<input type="hidden" name="penghargaan_pegawai_nip" id="penghargaan_pegawai_nip" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_nip"];?>">
										<input type="hidden" name="isUpdate" id="isUpdate" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="penghargaan_id" id="penghargaan_id" class="form-control input-xs wajib" value="0">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 form_lihat_data" style="display: none;">
						<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Lihat Data penghargaan</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Nama Tanda Penghargaan</label>
									<div class="col-md-9">
										<div class="form-control-static penghargaan_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">No SK</label>
									<div class="col-md-9">
										<div class="form-control-static penghargaan_nomor_sk">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tanggal SK</label>
									<div class="col-md-9">
										<div class="form-control-static penghargaan_tanggal_sk">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tahun</label>
									<div class="col-md-9">
										<div class="form-control-static penghargaan_tahun">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-lg-3 control-label">Pemberi</label>
									<div class="col-lg-9">
										<div class="form-control-static penghargaan_pemberi">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Jabatan</label>
									<div class="col-md-9">
										<div class="form-control-static penghargaan_jabatan">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<div class="col-md-9 col-md-offset-3">
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>
	<script type="text/javascript">
		function set_non_aktif(id,pegawai_id){
			$('#text_konfirmasi').html('Anda yakin menghapus data ini ?');
			$('#konfirmasipenyimpanan').modal('show');
			$('#setujukonfirmasibutton').unbind();
			$('#setujukonfirmasibutton').on('click', function () {
				$('#konfirmasipenyimpanan').modal('hide');
				$('.angkadoank').inputmask('remove');
				$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
				window.location.href = base_url+'kepegawaian/penghargaan/hapus/'+id+'/'+pegawai_id;
			});
		}
		function lihat_data(data){
			$('.form_tambah_data').hide();
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					lihat_data(data);
				});
			}else{
				$('.form_lihat_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_lihat_data").offset().top-60
					}, 500);
					var penghargaan_id = datajson.penghargaan_id;
					var penghargaan_nama = datajson.penghargaan_nama;
					var penghargaan_nomor_sk = datajson.penghargaan_nomor_sk;
					var penghargaan_tanggal_sk = datajson.penghargaan_tanggal_sk;
					var penghargaan_tahun = datajson.penghargaan_tahun;
					var penghargaan_pemberi = datajson.penghargaan_pemberi;
					var penghargaan_jabatan = datajson.penghargaan_jabatan;

					$('.penghargaan_nama').html(penghargaan_nama);
					$('.penghargaan_nomor_sk').html(penghargaan_nomor_sk);
					$('.penghargaan_tanggal_sk').html(penghargaan_tanggal_sk);
					$('.penghargaan_tahun').html(penghargaan_tahun);
					$('.penghargaan_pemberi').html(penghargaan_pemberi);
					$('.penghargaan_nama').html(penghargaan_nama);
					$('.penghargaan_jabatan').html(penghargaan_jabatan);
				});
			}
		}
		function update_data(data){
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			$('.form_lihat_data').hide();
			$('.label_aksi_form').html('Form Perubahan Data penghargaan');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					update_data(data);
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
					$('#isUpdate').val(1);
					var penghargaan_id = datajson.penghargaan_id;
					var penghargaan_nama = datajson.penghargaan_nama;
					var penghargaan_nomor_sk = datajson.penghargaan_nomor_sk;
					var penghargaan_tanggal_sk = datajson.penghargaan_tanggal_sk;
					var penghargaan_tahun = datajson.penghargaan_tahun;
					var penghargaan_pemberi = datajson.penghargaan_pemberi;
					var penghargaan_jabatan = datajson.penghargaan_jabatan;
					var penghargaan_jenis = datajson.penghargaan_jenis;

					$('#penghargaan_id').val(penghargaan_id);
					$('#penghargaan_nama').val(penghargaan_nama);
					$('#penghargaan_nomor_sk').val(penghargaan_nomor_sk);
					$('#penghargaan_tanggal_sk').val(penghargaan_tanggal_sk);
					$('#penghargaan_pemberi').val(penghargaan_pemberi);
					$('#penghargaan_nama').val(penghargaan_nama);
					$('#penghargaan_jabatan').val(penghargaan_jabatan);
					$('#penghargaan_id').val(datajson.penghargaan_id);

					$('#penghargaan_tahun').select2('val', '');
					$('#penghargaan_tahun').val(penghargaan_tahun);
					$('#penghargaan_tahun').trigger('change');

					$('#penghargaan_jenis').select2('val', '');
					$('#penghargaan_jenis').val(penghargaan_jenis);
					$('#penghargaan_jenis').trigger('change');

				});
			}
		}
		function tambah_data(){
			$('.form_lihat_data').hide();
			reset_form();
			$('.label_aksi_form').html('Form Penambahan Data penghargaan');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					$('.form_tambah_data').slideDown('fast',function(){
						$('html, body').animate({
							scrollTop: $(".form_tambah_data").offset().top-60
						}, 500);
						
					});
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
				});
			}
		}
		function batal_data(){
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					reset_form();
				});
			}
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					reset_form();
				});
			}
		}
		function reset_form(){
			$('#penghargaan_id').val(0);
			$('#penghargaan_nama').val('');
			$('#penghargaan_nomor_sk').val('');
			$('#penghargaan_tanggal_sk').val('');
			$('#penghargaan_tahun').select2('val', ' ');
			$('#penghargaan_jenis').select2('val', ' ');
			$('#penghargaan_pemberi').val('');
			$('#penghargaan_jabatan').val('');
			$('#penghargaan_id').val(0);
			$('.validation-error-label').css('display','none');
		}

		$(document).ready(function() {
			$('.pickpenghargaan').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
			});
		});
	</script>
