<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title text-center">DATA RIWAYAT HIDUP</h6>
		
	</div>
	<div class="panel-body">
		<form class="need_validation form-horizontal" action="<?php echo base_url('kepegawaian/pegawai_jfu/update') ?>" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px;">I. DATA PRIBADI</span>
                        <table border="0" cellpadding="0" cellspacing="0" class="style2" width="100%">
                        <tbody>
                        <tr>
                          <td width="18%">N A M A</td>
                          <td width="1%">:</td>
                          <td width="60%">
                            <strong><?php echo $data["pegawai_nama_lengkap"];?></strong>
                          </td>
                          <td rowspan="9">
                            <img src="<?php echo base_url();?>assets/images/pegawai/photo/<?php echo $data['pegawai_image_path']; ?>" alt="PHOTO" name="photo" width="120" height="160" align="left" id="photo">
                          </td>
                        </tr>
                        <tr>
                          <td>NIP</td>
                          <td>:</td>
                          <td>
                            <strong><?php echo $data["pegawai_nip"];?></strong>
                          </td>
                        </tr>
                        <tr>
                          <td>KARPEG/KARIS-KARSU/NPWP</td>
                          <td>:</td>
                          <td><?php echo $data["pegawai_nomor_karpeg"];?>/<?php echo $data["pegawai_nomor_karis"];?>/<?php echo $data["pegawai_nomor_npwp"];?></td>
                        </tr>
                        <tr>
                          <td>TEMPAT/TANGGAL LAHIR</td>
                          <td>:</td>
                          <td><?php echo $data["pegawai_tempat_tanggal_lahir"];?></td>
                        </tr>
                        <tr>
                          <td>JENIS KELAMIN</td>
                          <td>:</td>
                          <td><?php echo $data["jenis_kelamin_nama"];?></td>
                        </tr>
                        <tr>
                          <td>AGAMA</td>
                          <td>:</td>
                          <td><?php echo $data["agama_nama"];?></td>
                        </tr>
                        <tr>
                          <td>STATUS KELUARGA</td>
                          <td>:</td>
                          <td><?php echo $data["perkawinan_nama"];?></td>
                        </tr>
                        <tr>
                          <td>PENDIDIKAN AKHIR</td>
                          <td>:</td>
                          <td><?php echo $data["pendidikan_nama"];?></td>
                        </tr>
                        <tr>
                          <td>FAKULTAS/JURUSAN/PRODI</td>
                          <td>:</td>
                          <td><?php echo $data["fakultas_nama"];?>/<?php echo $data["jurusan_nama"];?>/<?php echo $data["pegawai_pendidikan_program_studi"];?></td>
                        </tr>
                        <tr>
                          <td valign="top">UNIT KERJA</td>
                          <td valign="top">:</td>
                          <td>
                            <strong><?php echo $data["unit_kerja_hirarki_name_full"];?></strong>
                          </td>
                        </tr>
                        <tr>
                          <td>DIKLAT PENJENJANGAN</td>
                          <td>:</td>
                          <td>//</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td>-/</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>PANGKAT,GOLONGAN,TMT GOL/TMT CPNS</td>
                          <td>:</td>
                          <td><?php echo $data["golongan_pangkat"];?>, <?php echo $data["golongan_nama"];?>, <?php echo dateEnToId($data["pegawai_tanggal_tmt"], 'd-m-Y');?> / <?php echo dateEnToId($data["pegawai_cpns_tanggal_tmt"], 'd-m-Y');?></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>MASA KERJA GOLONGAN</td>
                          <td>:</td>
                          <?php
                            $masa_kerja_golongan = '';
                            if($data["bulan_kinerja"] < 0 OR $data["bulan_kinerja"] > 11){
                              $masa_kerja_golongan = $data["hasil_tahun"].' Tahun '.$data["hasil_bulan"].' Bulan';
                            }else{
                              $masa_kerja_golongan = $data["tahun_kinerja"].' Tahun '.$data["bulan_kinerja"].' Bulan';
                            } 
                          ?>
                          <td><?php echo $masa_kerja_golongan; ?></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>MASA KERJA KESELURUHAN</td>
                          <td>:</td>
                          <td><?php echo $data["masa_kerja_keseluruhan"];?></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td valign="top">JABATAN STRUKTURAL/TMT</td>
                          <td valign="top">:</td>
                          <td>
                            <strong><?php echo $data["pegawai_nama_jabatan"];?> / <?php echo dateEnToId($data["pegawai_tanggal_tmt_jabatan"], 'd-m-Y');?></strong>
                          </td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>JABATAN FUNGSIONAL</td>
                          <td>:</td>
                          <td><?php echo $data["fungsional_nama"];?></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>BIDANG KEAHLIAN</td>
                          <td>:</td>
                          <td>
                            <?php echo $data["pegawai_jab_fung_bidang_1"];?> / <?php echo $data["pegawai_jab_fung_bidang_2"];?></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>ALAMAT RUMAH</td>
                          <td>:</td>
                          <td><?php echo $data["pegawai_domisili_alamat"];?></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>KOTA/KODE POS</td>
                          <td>:</td>
                          <td><?php if($data["pegawai_domisili_kota_nama"] == ''){ ?><?php echo $data["pegawai_domisili_kota_text"];?><?php }else{?><?php echo $data["pegawai_domisili_kota_nama"];?><?php } ?>/<?php echo $data["pegawai_domisili_kodepos"];?></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>PROPINSI/NEGARA</td>
                          <td>:</td>
                          <td><?php if($data["pegawai_domisili_provinsi_nama"] == ''){ ?><?php echo $data["pegawai_domisili_provinsi_text"];?><?php }else{?><?php echo $data["pegawai_domisili_provinsi_nama"];?><?php } ?>/INDONESIA</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>TELEPHONE RUMAH/HP</td>
                          <td>:</td>
                          <td><?php echo $data["pegawai_telepon"];?>/<?php echo $data["pegawai_handphone"];?></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>ALAMAT KANTOR</td>
                          <td>:</td>
                          <td><?php echo $data["unit_kerja_alamat"];?></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>TELPHONE/EXT</td>
                          <td>:</td>
                          <td><?php echo $data["unit_kerja_telp"];?>/<?php echo $data["unit_kerja_ext"];?></td>
                          <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <br/>
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px">II. DATA PENDIDIKAN FORMAL</span>
                    <table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
                        <thead>
                            <tr>
                                <th class="text-center">TINGKAT</th>
                                <th class="text-center">NAMA SEKOLAH</th>                                  
                                <th class="text-center">FAKULTAS/JURUSAN/PRODI</th>
                                <th class="text-center">LULUS</th>
                                <th class="text-center">LOKASI</th>
                                <th class="text-center">KEPALA/DEKAN</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($pendidikan_formal)>0){?>
                            <?php foreach ($pendidikan_formal as $pf) {  ?>
                            <tr>
                                <td><?php echo $pf["pendidikan_nama"];?></td>
                                <td><?php echo $pf["pendidikan_formal_nama"];?></td>
                                <td><?php echo $pf["pendidikan_formal_fakultas"];?>/<?php echo $pf["pendidikan_formal_jurusan"];?>/<?php echo $pf["pendidikan_formal_pro_studi"];?></td>
                                <td><?php echo $pf["pendidikan_formal_thn_lulus"];?></td>
                                <td><?php echo $pf["pendidikan_formal_lokasi"];?></td>
                                <td><?php echo $pf["pendidikan_formal_nama_kep"];?></td>                                   
                            </tr>
                            <?php } ?>
                            <?php }else{ ?>
                            <tr>
                                <td class="text-center" colspan="9">Data Belum Tersedia</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px">III. DATA PENDIDIKAN INFORMAL</span>
                    <table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
                        <thead>
                            <tr>
                                <th class="text-center">NAMA KURSUS</th>
                                <th class="text-center">LOKASI</th>                                  
                                <th class="text-center">MULAI</th>
                                <th class="text-center">SELESAI</th>
                                <th class="text-center">PENYELENGGARA</th>
                                <th class="text-center">JML JAM</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($pendidikan_nonformal)>0){?>
                            <?php foreach ($pendidikan_nonformal as $pnf) {  ?>
                            <tr>
                                <td><?php echo $pnf["pendidikan_nonformal_nama"];?></td>
                                <td><?php echo $pnf["pendidikan_nonformal_lokasi"];?></td>
                                <td class="text-center"><?php echo dateEnToId($pnf["pendidikan_nonformal_tanggal_mulai"], 'd-m-Y');?></td>
                                <td class="text-center"><?php echo dateEnToId($pnf["pendidikan_nonformal_tanggal_selesai"], 'd-m-Y');?></td>
                                <td><?php echo $pnf["pendidikan_nonformal_penyelenggara"];?></td>
                                <td><?php echo $pnf["pendidikan_nonformal_jumlah_jam"];?></td>                               
                            </tr>
                            <?php } ?>
                            <?php }else{ ?>
                            <tr>
                                <td class="text-center" colspan="9">Data Belum Tersedia</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px">IV. DATA RIWAYAT KEPANGKATAN</span>
                    <table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
                        <thead>
                            <tr>
                                <th class="text-center">GOLONGAN</th>
                                <th class="text-center">TMT GOL</th>                                  
                                <th class="text-center">PEJABAT</th>
                                <th class="text-center">NOMOR SK</th>
                                <th class="text-center">TGL SK</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($kepangkatan)>0){?>
                            <?php foreach ($kepangkatan as $k) {  ?>
                            <tr>
                                <td><?php echo $k["golongan_nama"];?></td>
                                <td><?php echo dateEnToId($k["kepangkatan_tanggal_tmt_golongan"], 'd-m-Y');?></td>
                                <td><?php echo $k["kepangkatan_nama_pejabat"];?></td>
                                <td><?php echo $k["kepangkatan_nomor_sk"];?></td>
                                <td><?php echo dateEnToId($k["kepangkatan_tanggal_sk"], 'd-m-Y');?></td>                            
                            </tr>
                            <?php } ?>
                            <?php }else{ ?>
                            <tr>
                                <td class="text-center" colspan="9">Data Belum Tersedia</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px">V. DATA RIWAYAT JABATAN</span>
                    <table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
                        <thead>
                            <tr>
                                <th class="text-center">ESELON</th>
                                <th class="text-center">NAMA JABATANL</th>                                  
                                <th class="text-center">TGL MULAI</th>
                                <th class="text-center">TGL SELESAI</th>
                                <th class="text-center">NOMOR SK</th>
                                <th class="text-center">TGL SK</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($jabatan)>0){?>
                            <?php foreach ($jabatan as $j) {  ?>
                            <tr>
                                <td><?php echo $j["jabatan_nama"];?></td>
                                <td><?php echo $j["jenjang_jabatan_jabatan_nama"];?></td>
                                <td><?php echo dateEnToId($j["jenjang_jabatan_tanggal_mulai"], 'd-m-Y');?></td>
                                <td><?php
                                      $selesai = dateEnToId($j["jenjang_jabatan_tanggal_selesai"], 'd-m-Y');
                                      $tgl_selesai ='';
                                      if($selesai == '01-01-1900' || $selesai == '01-01-1970'){
                                        $tgl_selesai = '-';
                                      }else{
                                        $tgl_selesai = $selesai;
                                      }
                                      echo $tgl_selesai;
                                     ?>
                                </td>
                                <td><?php echo $j["jenjang_jabatan_nomor_sk"];?></td>
                                <td><?php echo dateEnToId($j["jenjang_jabatan_tanggal_sk"], 'd-m-Y');?></td>                            
                            </tr>
                            <?php } ?>
                            <?php }else{ ?>
                            <tr>
                                <td class="text-center" colspan="9">Data Belum Tersedia</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px">VI. DATA DIKLAT PENJENJANGAN/LEMHANAS</span>
                    <table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
                        <thead>
                            <tr>
                                <th class="text-center">NAMA DIKLAT</th>
                                <th class="text-center">LOKASI</th>                                  
                                <th class="text-center">TGL MULAI</th>
                                <th class="text-center">TGL SELESAI</th>
                                <th class="text-center">PREDIKAT</th>
                                <th class="text-center">PENYELENGGARA</th>
                                <th class="text-center">JML JAM</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($diklatp)>0){?>
                            <?php foreach ($diklatp as $dp) {  ?>
                            <tr>
                                <td><?php echo $dp["diklat_nama"];?></td>
                                <td><?php echo $dp["diklat_lokasi"];?></td>
                                <td><?php echo dateEnToId($dp["diklat_tanggal_mulai"], 'd-m-Y');?></td>
                                <td><?php echo dateEnToId($dp["diklat_tanggal_selesai"], 'd-m-Y');?></td>
                                <td><?php echo $dp["diklat_predikat"];?></td>
                                <td><?php echo $dp["diklat_penyelenggara"];?></td>
                                <td><?php echo $dp["diklat_jumlah_jam"];?></td>                            
                            </tr>
                            <?php } ?>
                            <?php }else{ ?>
                            <tr>
                                <td class="text-center" colspan="9">Data Belum Tersedia</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px">VII. DATA ISTRI/SUAMI</span>
                    <table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
                        <thead>
                            <tr>
                                <th class="text-center">NAMA ISTRI/SUAMI</th>
                                <th class="text-center">NOMOR KARIS/KARSU</th>                                  
                                <th class="text-center">TGL LAHIR</th>
                                <th class="text-center">TGL NIKAH</th>
                                <th class="text-center">PENDIDIKAN</th>
                                <th class="text-center">PEKERJAAN</th>
                                <th class="text-center">KETERANGAN</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($suamiistri)>0){?>
                            <?php foreach ($suamiistri as $si) {  ?>
                            <tr>
                                <td><?php echo $si["suami_istri_nama"];?></td>
                                <td><?php echo $si["suami_istri_no_karis_karsu"];?></td>
                                <td><?php echo dateEnToId($si["suami_istri_tanggal_lahir"], 'd-m-Y');?></td>
                                <td><?php echo dateEnToId($si["suami_istri_tanggal_nikah"], 'd-m-Y');?></td>
                                <td><?php echo $si["pendidikan_nama"];?></td>
                                <td><?php echo $si["suami_istri_pekerjaan"];?></td>
                                <td>-</td>                            
                            </tr>
                            <?php } ?>
                            <?php }else{ ?>
                            <tr>
                                <td class="text-center" colspan="9">Data Belum Tersedia</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px">VIII. DATA ANAK</span>
                    <table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
                        <thead>
                            <tr>
                                <th class="text-center">NAMA ANAK</th>
                                <th class="text-center">JNS KELAMIN</th>                                  
                                <th class="text-center">TMP LAHIR</th>
                                <th class="text-center">TGL LAHIR</th>
                                <th class="text-center">PENDIDIKAN</th>
                                <th class="text-center">KONDISI</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($anak)>0){?>
                            <?php foreach ($anak as $a) {  ?>
                            <tr>
                                <td><?php echo $a["anak_pegawai_nama"];?></td>
                                <td><?php echo $a["jenis_kelamin_nama"];?></td>
                                <td><?php echo $a["anak_tempat_lahir"];?></td>
                                <td><?php echo dateEnToId($a["anak_tanggal_lahir"], 'd-m-Y');?></td>
                                <td><?php echo $a["pendidikan_nama"];?></td>
                                <td><?php echo $a["anak_kondisi_nama"];?></td>                            
                            </tr>
                            <?php } ?>
                            <?php }else{ ?>
                            <tr>
                                <td class="text-center" colspan="9">Data Belum Tersedia</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px">IX. DATA KELUARGA</span>
                    <table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
                        <thead>
                            <tr>
                                <th class="text-center">NAMA KELUARGA</th>
                                <th class="text-center">JNS KELAMIN</th>                                  
                                <th class="text-center">TGL LAHIR</th>
                                <th class="text-center">HUBUNGAN</th>
                                <th class="text-center">PEKERJAAN</th>
                                <th class="text-center">KONDISI</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($keluarga)>0){?>
                            <?php foreach ($keluarga as $k) {  ?>
                            <tr>
                                <td><?php echo $k["keluarga_nama"];?></td>
                                <td><?php echo $k["jenis_kelamin_nama"];?></td>
                                <td><?php echo dateEnToId($k["keluarga_tanggal_lahir"], 'd-m-Y');?></td>
                                <td><?php echo $k["hubungan_keluarga_nama"];?></td>
                                <td><?php echo $k["keluarga_pekerjaan"];?></td>
                                <td><?php echo $k["keluarga_kondisi_nama"];?></td>                            
                            </tr>
                            <?php } ?>
                            <?php }else{ ?>
                            <tr>
                                <td class="text-center" colspan="9">Data Belum Tersedia</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px">X. DATA SEMINAR/LOKAKARYA</span>
                    <table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
                        <thead>
                            <tr>
                                <th class="text-center">NAMA SEMINAR/LOKAKARYA</th>
                                <th class="text-center">LOKASI</th>                                  
                                <th class="text-center">PENYELENGGARA</th>
                                <th class="text-center">TAHUN</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($seminar)>0){?>
                            <?php foreach ($seminar as $s) {  ?>
                            <tr>
                                <td><?php echo $s["seminar_nama_kegiatan"];?></td>
                                <td><?php echo $s["seminar_lokasi"];?></td>
                                <td><?php echo $s["seminar_penyelenggara"];?></td>
                                <td><?php echo $s["seminar_tahun"];?></td>                            
                            </tr>
                            <?php } ?>
                            <?php }else{ ?>
                            <tr>
                                <td class="text-center" colspan="9">Data Belum Tersedia</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px">XI. DATA TANDA JASA</span>
                    <table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
                        <thead>
                            <tr>
                                <th class="text-center">NAMA TANDA JASA</th>
                                <th class="text-center">TGL PEROLEH</th>                                  
                                <th class="text-center">NOMOR SK</th>
                                <th class="text-center">PEMBERI</th>
                                <th class="text-center">JABATAN</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($penghargaan)>0){?>
                            <?php foreach ($penghargaan as $p) {  ?>
                            <tr>
                                <td><?php echo $p["penghargaan_nama"];?></td>
                                <td><?php echo dateEnToId($p["penghargaan_tanggal_sk"], 'd-m-Y');?></td>
                                <td><?php echo $p["penghargaan_nomor_sk"];?></td>
                                <td><?php echo $p["penghargaan_pemberi"];?></td>
                                <td><?php echo $p["penghargaan_jabatan"];?></td>                          
                            </tr>
                            <?php } ?>
                            <?php }else{ ?>
                            <tr>
                                <td class="text-center" colspan="9">Data Belum Tersedia</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px">XII. HUKUMAN DISIPLIN</span>
                    <table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
                        <thead>
                            <tr>
                                <th class="text-center">KODE HUKUM</th>
                                <th class="text-center">NOMOR SK</th>                                  
                                <th class="text-center">TGL SK</th>
                                <th class="text-center">TGL BERLAKU</th>
                                <th class="text-center">PEJABAT</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($hukuman_disiplin)>0){?>
                            <?php foreach ($hukuman_disiplin as $hd) {  ?>
                            <tr>
                                <td><?php echo $hd["hukuman_disiplin_kode_hukuman_lain"];?></td>
                                <td><?php echo $hd["hukuman_disiplin_nomor_sk"];?></td>
                                <td><?php echo dateEnToId($hd["hukuman_disiplin_tanggal_sk"], 'd-m-Y');?></td>
                                <td><?php echo dateEnToId($hd["hukuman_disiplin_tanggal_berlaku"], 'd-m-Y');?></td>
                                <td><?php echo $hd["jenis_pejabat_berwenang_nama"];?></td>                          
                            </tr>
                            <?php } ?>
                            <?php }else{ ?>
                            <tr>
                                <td class="text-center" colspan="9">Data Belum Tersedia</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px">XIII. DATA DP3</span>
                    <table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
                        <thead>
                            <tr>
                                <th class="text-center">TAHUN</th>
                                <th class="text-center">KESETIAAN</th>                                  
                                <th class="text-center">PRESTASI</th>
                                <th class="text-center">TANG. JAWAB</th>
                                <th class="text-center">KETAATAN</th>
                                <th class="text-center">KEJUJURAN</th>
                                <th class="text-center">KERJASAMA</th>
                                <th class="text-center">PRAKARSA</th>
                                <th class="text-center">KEPEMIMPINAN</th>
                                <th class="text-center">RATA-RATA</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($dp3 as $datadp3) {  ?>
                            <tr>
                              <td><?php echo $datadp3["dp3_tahun_penilaian"];?></td>
                              <td><?php echo $datadp3["dp3_kesetiaan"];?></td>
                              <td><?php echo $datadp3["dp3_prestasi"];?></td>
                              <td><?php echo $datadp3["dp3_tanggung_jawab"];?></td>
                              <td><?php echo $datadp3["dp3_ketaatan"];?></td>
                              <td><?php echo $datadp3["dp3_kejujuran"];?></td>
                              <td><?php echo $datadp3["dp3_kerjasama"];?></td>
                              <td><?php echo $datadp3["dp3_prakarsa"];?></td>
                              <td><?php echo $datadp3["dp3_kepemimpinan"];?></td>
                              <td><?php echo $datadp3["dp3_rata_rata"];?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px">XIV. PENILAIAN KINERJA PEGAWAI</span>
                    <table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
                        <thead>
                            <tr>
                                <th class="text-center" rowspan="2">TAHUN</th>
                                <th class="text-center" rowspan="2">SKP 60%</th>                                  
                                <th class="text-center" colspan="7">PENILAIAN PERILAKU</th>
                                <th class="text-center" colspan="3">NILAI KINERJA (SKP 60% + PERILAKU 40%)</th>
                            </tr>
                            <tr>
                                <th class="text-center">ORIENTASI PELAYANAN</th>
                                <th class="text-center">INTEGRITAS</th>
                                <th class="text-center">KOMITMEN</th>
                                <th class="text-center">DISIPLIN</th>
                                <th class="text-center">KERJASAMA</th>
                                <th class="text-center">KEPEMIMPINAN</th>
                                <th class="text-center">RATA-RATA</th>
                                <th class="text-center">SKP 60%</th>
                                <th class="text-center">PERILAKU 40%</th>
                                <th class="text-center">TOTAL NILAI</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center" colspan="12">Data Belum Tersedia</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left; font-size: 15px">XV. URUTAN BERDASARKAN DAFTAR URUT KEPANGKATAN</span>
                    <table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
                        <thead>
                            <tr>
                                <th class="text-center">LINGKUP UNIT KERJA</th>
                                <th class="text-center">LINGKUP ESELON I</th>                                  
                                <th class="text-center">LINGKUP KKP</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">KE - <?php echo $urut['NO_URUT_UNITKERJA']; ?></td>
                                <td class="text-center">KE - <?php echo $urut['NO_URUT_ESELON']; ?></td>
                                <td class="text-center">KE - <?php echo $urut['NO_URUT_KKP']; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
	</div>
</div>
