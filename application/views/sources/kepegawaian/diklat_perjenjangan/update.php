<style type="text/css">
	.control-label{
		font-weight: bold;
	}
</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Data Diklat Penjenjangan</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nip"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tempat Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_tempat_lahir"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Jabatan</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama_jabatan"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Unit Kerja</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["unit_kerja_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<a href="<?php echo base_url();?>kepegawaian/pendidikan_formal" class="btn btn-xs btn-warning">Kembali</a></td>

							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Daftar Diklat Penjenjangan</span>
					<div class="col-md-12">
						<table class="table table-xxs table-bordered table-striped table-hover datatable-responsive epagawai_table" >
							<thead>
								<tr>
									<th rowspan="2" class="text-center">Nama Diklat</th>
									<th rowspan="2" class="text-center">Jenis Diklat</th>
									<th rowspan="2" class="text-center">Lokasi</th>
									<th colspan="2" class="text-center">Tanggal</th>
									<th rowspan="2" class="text-center">Predikat</th>
									<th rowspan="2" class="text-center">Penyelenggara</th>
									<th rowspan="2" class="text-center">Jumlah Jam</th>
									<th rowspan="2" class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
								</tr>
								<tr>
									<th class="text-center">Mulai</th>
									<th class="text-center">Selesai</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($history)>0){?>
								<?php foreach ($history as $h) {  ?>
								<tr>
									<td><?php echo $h["diklat_nama"];?></td>
									<td><?php echo $h["jenis_diklat_nama"];?></td>
									<td><?php echo $h["diklat_lokasi"];?></td>
									<td class="text-center"><?php echo $h["diklat_tanggal_mulai"];?></td>
									<td class="text-center"><?php echo $h["diklat_tanggal_selesai"];?></td>
									<td><?php echo $h["diklat_predikat"];?></td>									
									<td><?php echo $h["diklat_penyelenggara"];?></td>
									<td><?php echo $h["diklat_jumlah_jam"];?></td>
									<td>
										<ul class="icons-list">
											<li><a href="javascript:void(0)" onclick="update_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
											<li><a href="javascript:void(0)" onclick="lihat_data('<?php echo htmlentities(json_encode($h),ENT_QUOTES);?>')" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>
											<li><a onclick="set_non_aktif(<?php echo $h["diklat_id"];?>,<?php echo $h["diklat_pegawai_id"];?>)" class="hapus_data" data-popup="tooltip" title="Hapus" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
										</ul>
									</td>
								</tr>
								<?php } ?>
								<?php }else{ ?>
								<tr>
									<td class="text-center" colspan="9">Data Belum Tersedia</td>
								</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<td class="text-right" colspan="9"><a href="javascript:void(0)" onclick="tambah_data();" class="btn btn-xs btn-primary btn-tfoot-table">Tambah Data</a>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="col-md-12 form_tambah_data" style="display: none;">
						<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Form Penambahan Diklat Penjenjangan</span>
						<div class="col-md-12">

							<div class="col-md-12">
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Jenis Diklat</label>
									<div class="col-md-9">
										<select name="diklat_jenis_id" id="diklat_jenis_id" data-placeholder="Jenis Diklat" class="select-size-xs wajib" >
											<option value=""></option>
											<?php foreach ($jenisdiklat as $i) {
												echo '<option value="'.$i["jenis_diklat_id"].'" >'.$i["jenis_diklat_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Angkatan</label>
									<div class="col-md-9">
										<input type="text" name="diklat_angkatan" id="diklat_angkatan" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Nama Diklat Perjenjangan</label>
									<div class="col-md-9">
										<input type="text" name="diklat_nama" id="diklat_nama" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Penyelenggara</label>
									<div class="col-md-9">
										<input type="text" name="diklat_penyelenggara" id="diklat_penyelenggara" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Durasi</label>
									<div class="col-md-9">
										<input class="form-control input-xs wajib" type="number" name="diklat_jumlah_jam" id="diklat_jumlah_jam">
										<!-- <input type="text" name="diklat_jumlah_jam" id="diklat_jumlah_jam" class="form-control input-xs wajib"> -->
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Tanggal Mulai</label>
									<div class="col-lg-2">
										<input type="text" name="diklat_tanggal_mulai" id="diklat_tanggal_mulai" class="form-control input-xs wajib pickttlstart">
									</div>
									<label class="col-md-2 control-label">Tanggal Selesai</label>
									<div class="col-md-2">
										<input type="text" name="diklat_tanggal_selesai" id="diklat_tanggal_selesai" class="form-control input-xs wajib pickttlend">
									</div>
								</div>					
								
								
								<div class="form-group form-group-xs">
									<label class="col-lg-3 control-label">Predikat</label>
									<div class="col-lg-9">
										<input type="text" name="diklat_predikat" id="diklat_predikat" class="form-control input-xs wajib">
									</div>
								</div>								
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Lokasi</label>
									<div class="col-md-9">
										<input type="text" name="diklat_lokasi" id="diklat_lokasi" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Nomor Sertifikat</label>
									<div class="col-md-9">
										<input type="text" name="diklat_nomor_sertifikat" id="diklat_nomor_sertifikat" class="form-control input-xs wajib">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tanggal Sertifikat</label>
									<div class="col-md-9">
										<input type="text" name="diklat_tanggal_sertifikat" id="diklat_tanggal_sertifikat" class="form-control input-xs wajib pickttl">
									</div>
								</div>
								<div class="form-group form-group-xs">
									<div class="col-md-9 col-md-offset-3">
										<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
										<input type="hidden" name="diklat_pegawai_id" id="diklat_pegawai_id" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_id"];?>">
										<input type="hidden" name="diklat_pegawai_nip" id="diklat_pegawai_nip" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_nip"];?>">
										<input type="hidden" name="isUpdate" id="isUpdate" class="form-control input-xs wajib" value="0">
										<input type="hidden" name="diklat_id" id="diklat_id" class="form-control input-xs wajib" value="0">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 form_lihat_data" style="display: none;">
						<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Lihat Diklat Penjenjangan</span>
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Jenis Diklat</label>
									<div class="col-md-9">
										<div class="form-control-static jenis_diklat_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Angkatan</label>
									<div class="col-md-9">
										<div class="form-control-static diklat_angkatan">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Nama Diklat Perjenjangan</label>
									<div class="col-md-9">
										<div class="form-control-static diklat_nama">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-lg-3 control-label">Penyelenggara</label>
									<div class="col-lg-9">
										<div class="form-control-static diklat_penyelenggara">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Durasi</label>
									<div class="col-md-9">
										<div class="form-control-static diklat_jumlah_jam">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-lg-3 control-label">Tanggal Mulai</label>
									<div class="col-lg-2">
										<div class="form-control-static diklat_tanggal_mulai">-</div>
									</div>
									<label class="col-md-2 control-label">Tanggal Selesai</label>
									<div class="col-md-2">
										<div class="form-control-static diklat_tanggal_selesai">-</div>
									</div>
								</div>	
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Predikat</label>
									<div class="col-md-9">
										<div class="form-control-static diklat_predikat">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Lokasi</label>
									<div class="col-md-9">
										<div class="form-control-static diklat_lokasi">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Nomor Sertifikat</label>
									<div class="col-md-9">
										<div class="form-control-static diklat_nomor_sertifikat">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<label class="col-md-3 control-label">Tanggal Sertifikat</label>
									<div class="col-md-9">
										<div class="form-control-static diklat_tanggal_sertifikat">-</div>
									</div>
								</div>
								<div class="form-group form-group-xs lihat_data_statik">
									<div class="col-md-9 col-md-offset-3">
										<a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>
	<script type="text/javascript">
		function set_non_aktif(id,pegawai_id){
			$('#text_konfirmasi').html('Anda yakin menghapus data ini ?');
			$('#konfirmasipenyimpanan').modal('show');
			$('#setujukonfirmasibutton').unbind();
			$('#setujukonfirmasibutton').on('click', function () {
				$('#konfirmasipenyimpanan').modal('hide');
				$('.angkadoank').inputmask('remove');
				$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
				window.location.href = base_url+'kepegawaian/diklat_perjenjangan/hapus/'+id+'/'+pegawai_id;
			});
		}
		function lihat_data(data){
			$('.form_tambah_data').hide();
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					lihat_data(data);
				});
			}else{
				$('.form_lihat_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_lihat_data").offset().top-60
					}, 500);

					var diklat_id = datajson.diklat_id;
					var diklat_nama = datajson.diklat_nama;
					var diklat_angkatan = datajson.diklat_angkatan;
					var diklat_lokasi = datajson.diklat_lokasi;
					var diklat_predikat = datajson.diklat_predikat;
					var diklat_penyelenggara = datajson.diklat_penyelenggara;
					var diklat_jumlah_jam = datajson.diklat_jumlah_jam;
					var diklat_tanggal_mulai = datajson.diklat_tanggal_mulai;
					var diklat_tanggal_selesai = datajson.diklat_tanggal_selesai;
					var diklat_nomor_sertifikat = datajson.diklat_nomor_sertifikat;
					var diklat_tanggal_sertifikat = datajson.diklat_tanggal_sertifikat;
					var diklat_jenis_id = datajson.diklat_jenis_id;
					var jenis_diklat_nama = datajson.jenis_diklat_nama;

					$(".diklat_id").html(diklat_id);
					$(".diklat_nama").html(diklat_nama);
					$(".diklat_angkatan").html(diklat_angkatan);
					$(".diklat_lokasi").html(diklat_lokasi);
					$(".diklat_predikat").html(diklat_predikat);
					$(".diklat_penyelenggara").html(diklat_penyelenggara);
					$(".diklat_jumlah_jam").html(diklat_jumlah_jam);
					$(".diklat_tanggal_mulai").html(diklat_tanggal_mulai);
					$(".diklat_tanggal_selesai").html(diklat_tanggal_selesai);
					$(".diklat_nomor_sertifikat").html(diklat_nomor_sertifikat);
					$(".diklat_tanggal_sertifikat").html(diklat_tanggal_sertifikat);
					$(".jenis_diklat_nama").html(jenis_diklat_nama);
					
				});
			}
		}
		function update_data(data){
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			
			$('.form_lihat_data').hide();
			$('.label_aksi_form').html('Form Perubahan Diklat Penjenjangan');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					update_data(data);
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
					$('#isUpdate').val(1);
					var diklat_id = datajson.diklat_id;
					var diklat_nama = datajson.diklat_nama;
					var diklat_angkatan = datajson.diklat_angkatan;
					var diklat_lokasi = datajson.diklat_lokasi;
					var diklat_predikat = datajson.diklat_predikat;
					var diklat_penyelenggara = datajson.diklat_penyelenggara;
					var diklat_jumlah_jam = datajson.diklat_jumlah_jam;
					var diklat_tanggal_mulai = datajson.diklat_tanggal_mulai;
					var diklat_tanggal_selesai = datajson.diklat_tanggal_selesai;
					var diklat_nomor_sertifikat = datajson.diklat_nomor_sertifikat;
					var diklat_tanggal_sertifikat = datajson.diklat_tanggal_sertifikat;
					var diklat_jenis_id = datajson.diklat_jenis_id;

					$("#diklat_id").val(diklat_id);
					$("#diklat_nama").val(diklat_nama);
					$("#diklat_angkatan").val(diklat_angkatan);
					$("#diklat_lokasi").val(diklat_lokasi);
					$("#diklat_predikat").val(diklat_predikat);
					$("#diklat_penyelenggara").val(diklat_penyelenggara);
					$("#diklat_jumlah_jam").val(diklat_jumlah_jam);
					$("#diklat_tanggal_mulai").val(diklat_tanggal_mulai);
					$("#diklat_tanggal_selesai").val(diklat_tanggal_selesai);
					$("#diklat_nomor_sertifikat").val(diklat_nomor_sertifikat);
					$("#diklat_tanggal_sertifikat").val(diklat_tanggal_sertifikat);

					//alert(diklat_jenis_id);
					$('#diklat_jenis_id').val(diklat_jenis_id);
					$('#diklat_jenis_id').trigger('change');
				});
			}
		}

		function tambah_data(){
			$('.form_lihat_data').hide();
			reset_form();
			$('.label_aksi_form').html('Form Penambahan Diklat Penjenjangan');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					$('.form_tambah_data').slideDown('fast',function(){
						$('html, body').animate({
							scrollTop: $(".form_tambah_data").offset().top-60
						}, 500);
						
					});
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
				});
			}
		}
		function batal_data(){
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					reset_form();
				});
			}
			if($('.form_lihat_data').is(":visible")){
				$('.form_lihat_data').slideUp('slow',function(){
					reset_form();
				});
			}
		}
		function reset_form(){
			$("#diklat_id").val(0);
			$("#diklat_nama").val('');
			$("#diklat_angkatan").val('');
			$("#diklat_lokasi").val('');
			$("#diklat_predikat").val('');
			$("#diklat_penyelenggara").val('');
			$("#diklat_jumlah_jam").val('');
			$("#diklat_tanggal_mulai").val('');
			$("#diklat_tanggal_selesai").val('');
			$("#diklat_nomor_sertifikat").val('');
			$("#diklat_tanggal_sertifikat").val('');
			$('#diklat_jenis_id').val('');
			$('#diklat_jenis_id').trigger('change');
			$('.validation-error-label').css('display','none');
		}
		
		$(document).ready(function() {

			var d = new Date();

		$('.pickttl').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});
		$('.pickttlstart').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});

		$('.pickttlend').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
		});

		// On value change
	    // $(".pickttlstart").change(function(e) {
	    //     try {
	    //         var fromDay = rangeDemoConv.parse($("#rangeDemoStart").val()).getTime();
	    //         var dayLater = new Date(fromDay+oneDay);
	    //             dayLater.setHours(0,0,0,0);
	    //         var ninetyDaysLater = new Date(fromDay+(90*oneDay));
	    //             ninetyDaysLater.setHours(23,59,59,999);

	    //         // End date
	    //         $("#rangeDemoFinish")
	    //         .AnyTime_noPicker()
	    //         .removeAttr("disabled")
	    //         .val(rangeDemoConv.format(dayLater))
	    //         .AnyTime_picker({
	    //             earliest: dayLater,
	    //             format: rangeDemoFormat,
	    //             latest: ninetyDaysLater
	    //         });
	    //     }
	    //     catch(e) {
	    //         // Disable End date field
	    //         $("#rangeDemoFinish").val("").attr("disabled","disabled");
	    //     }
	    // });

		});
	</script>
