<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Mutasi Pensiun</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<span class="label border-left-primary label-striped" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Pegawai</span>

					<div class="col-md-12">
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NIP</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nip"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">NAMA</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Tempat Lahir</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_tempat_lahir"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Jabatan</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["pegawai_nama_jabatan"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs lihat_data_statik">
							<label class="col-md-3 control-label">Unit Kerja</label>
							<div class="col-md-9">
								<div class="form-control-static"><?php echo $pegawai["unit_kerja_nama"];?></div>
							</div>
						</div>
						<div class="form-group form-group-xs">
							<div class="col-md-9 col-md-offset-3">
								<a href="<?php echo base_url();?>kepegawaian/mutasi_pensiun" class="btn btn-xs btn-warning">Kembali</a>
							</div>
						</div>
					</div>
				</div>
					<div class="col-md-12 form_tambah_data">
						<span class="label border-left-primary label-striped label_aksi_form" style="width: 100%;margin-bottom: 10px;text-align: left;">Data Mutasi Pensiun</span>
						<div class="col-md-12">
							<div class="col-md-12">
							<?php if(count($history)>0){?>
								<?php foreach ($history as $h) {  ?>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Jenis Pensiun</label>
									<div class="col-md-9">
										<select name="pegawai_jenis_pensiun_id" id="pegawai_jenis_pensiun_id" data-placeholder="Pilih jenis pensiun" class="select-size-xs wajib">
											<option value="99" value="99">AKTIF</option>
											<?php foreach ($jenis_pensiun as $i) {
												echo '<option value="'.$i["jenis_pensiun_id"].'" '.(($h["pegawai_jenis_pensiun_id"]==$i["jenis_pensiun_id"])?"selected":"").'>'.$i["jenis_pensiun_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tanggal Pensiun/Pindah/Wafat</label>
									<div class="col-md-9">
											<input type="text" name="pegawai_tanggal_pensiun" id="pegawai_tanggal_pensiun" class="form-control input-xs wajib pickmutasipensiun" placeholder="" value="<?php echo $h["pegawai_tanggal_pensiun"];?>">
									</div>
								</div>
								<?php 
									if ($h['pegawai_jenis_pensiun_id'] == 2){
										echo '<div class="form-group form-group-xs" id="wafat" >';
										echo '<label class="col-md-3 control-label">Faktor Penyebab Wafat</label>';
										echo '<div class="col-md-9">';
										echo '<input type="text" name="pegawai_faktor_wafat" id="pegawai_faktor_wafat" class="form-control input-xs" value="'.$h["pegawai_faktor_wafat"].'">';
										echo '</div>';
										echo '</div>';
										echo '<div class="form-group form-group-xs" style="display: none;" id="pindah">';
										echo '<label class="col-md-3 control-label">Ke Departemen</label>';
										echo '<div class="col-md-9">';
										echo '<input type="text" name="mutasi_departemen_ke" id="mutasi_departemen_ke" class="form-control input-xs" value="'.$h["mutasi_departemen_ke"].'">';
										echo '</div>';
										echo '</div>';
									}
									else if($h['pegawai_jenis_pensiun_id'] == 3){
										echo '<div class="form-group form-group-xs" id="pindah">';
										echo '<label class="col-md-3 control-label">Ke Departemen</label>';
										echo '<div class="col-md-9">';
										echo '<input type="text" name="mutasi_departemen_ke" id="mutasi_departemen_ke" class="form-control input-xs" value="'.$h["mutasi_departemen_ke"].'">';
										echo '</div>';
										echo '</div>';
										echo '<div class="form-group form-group-xs" style="display: none;" id="wafat" >';
										echo '<label class="col-md-3 control-label">Faktor Penyebab Wafat</label>';
										echo '<div class="col-md-9">';
										echo '<input type="text" name="pegawai_faktor_wafat" id="pegawai_faktor_wafat" class="form-control input-xs" value="'.$h["pegawai_faktor_wafat"].'">';
										echo '</div>';
										echo '</div>';
									}
								?>
								<!-- <div class="form-group form-group-xs" style="display: none;" id="wafat" >
									<label class="col-md-3 control-label">Faktor Penyebab Wafat</label>
									<div class="col-md-9">
										<input type="text" name="pegawai_faktor_wafat" id="pegawai_faktor_wafat" class="form-control input-xs" placeholder="" value="<?php echo $h["pegawai_faktor_wafat"];?>">
									</div>
								</div>
								<div class="form-group form-group-xs" style="display: none;" id="pindah">
									<label class="col-md-3 control-label">Ke Departemen</label>
									<div class="col-md-9">
											<input type="text" name="mutasi_departemen_ke" id="mutasi_departemen_ke" class="form-control input-xs" placeholder="" value="<?php echo $h["mutasi_departemen_ke"];?>">
									</div>
								</div> -->
								<!-- <div class="form-group form-group-xs" style="display: none;" id="pindah">
									<label class="col-md-3 control-label">Ke Departemen</label>
									<div class="col-md-9">
											<input type="text" name="mutasi_departemen_ke" id="mutasi_departemen_ke" class="form-control input-xs" placeholder="" value="<?php echo $h["mutasi_departemen_ke"];?>">
									</div>
								</div> -->
								<!-- <?php if($h['pegawai_jenis_pensiun_id'] = '4'){ ?>
								<div class="form-group form-group-xs" id="wafat">
									<label class="col-md-3 control-label">Faktor Penyebab Wafat</label>
									<div class="col-md-9">
										<input type="text" name="pegawai_faktor_wafat" id="pegawai_faktor_wafat" class="form-control input-xs" placeholder="" value="<?php echo $h["pegawai_faktor_wafat"];?>">
									</div>
								</div>
								<?php } else if($h['pegawai_jenis_pensiun_id'] = '5'){?>
									<div class="form-group form-group-xs" id="pindah">
									<label class="col-md-3 control-label">Ke Departemen</label>
									<div class="col-md-9">
											<input type="text" name="mutasi_departemen_ke" id="mutasi_departemen_ke" class="form-control input-xs" placeholder="" value="<?php echo $h["mutasi_departemen_ke"];?>">
									</div>
								</div>
								<?php }?> -->
								<?php } ?>
							<?php } else{ ?>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Jenis Pensiun</label>
									<div class="col-md-9">
										<select name="pegawai_jenis_pensiun_id" id="pegawai_jenis_pensiun_id" data-placeholder="Pilih jenis pensiun" class="select-size-xs wajib">
											<option value="99">AKTIF</option>
											<?php foreach ($jenis_pensiun as $i) {
												echo '<option value="'.$i["jenis_pensiun_id"].'" '.(($h["jenis_pensiun_id"]==$i["jenis_pensiun_id"])?"selected":"").'>'.$i["jenis_pensiun_nama"].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="form-group form-group-xs">
									<label class="col-md-3 control-label">Tanggal Pensiun/Pindah/Wafat</label>
									<div class="col-md-9">
											<input type="text" name="pegawai_tanggal_pensiun" id="pegawai_tanggal_pensiun" class="form-control input-xs wajib pickmutasipensiun" placeholder="" >
									</div>
								</div>
								<div class="form-group form-group-xs" style="display: none;" id="pindah">
									<label class="col-md-3 control-label">Ke Departemen</label>
									<div class="col-md-9">
											<input type="text" name="mutasi_departemen_ke" id="mutasi_departemen_ke" class="form-control input-xs" placeholder="" >
									</div>
								</div>
								<div class="form-group form-group-xs" style="display: none;" id="wafat">
									<label class="col-md-3 control-label">Faktor Penyebab Wafat</label>
									<div class="col-md-9">
											<input type="text" name="pegawai_faktor_wafat" id="pegawai_faktor_wafat" class="form-control input-xs" placeholder="" >
									</div>
								</div>
							<?php } ?>
								<div class="form-group form-group-xs">
									<div class="col-md-9 col-md-offset-3">
										<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
										<!-- <a href="javascript:void(0)" onclick="batal_data();" class="btn btn-xs btn-danger">Batal</a> -->
										<input type="hidden" name="pegawai_id" id="pegawai_id" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_id"];?>">
										<input type="hidden" name="pegawai_nip" id="pegawai_nip" class="form-control input-xs wajib" value="<?php echo $pegawai["pegawai_nip"];?>">
										<input type="hidden" name="isUpdate" id="isUpdate" class="form-control input-xs wajib" value="0">
										<!-- <input type="hidden" name="pegawai_id" id="pegawai_id" class="form-control input-xs wajib" value="0"> -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>
	<script type="text/javascript">
		// $(document).ready(function() {
		// 	var jenis = ('#pegawai_jenis_pensiun_id').val();
		// 	if(jenis == 2){
		// 		$('#wafat').show();
		// 		$('#pindah').hide();
		// 	}
		// 	else if(jenis == 3){
		// 		$('#pindah').show();
		// 		$('#wafat').hide();
		// 	}
		// 	else{
		// 		$('#pindah').hide();
		// 		$('#wafat').hide();
		// 	}
		// });
		function update_data(data){
			var decoded = $("<div/>").html(data).text();
			var datajson = $.parseJSON(decoded);
			reset_form();
			$('.form_lihat_data').hide();
			$('.label_aksi_form').html('Form Perubahan Data Kemampuan Bahasa Asing');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					update_data(data);
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
					$('#isUpdate').val(1);
					var pegawai_id = datajson.pegawai_id;
					var pegawai_tanggal = datajson.pegawai_tanggal;
					var pegawai_gaji_sebelum = datajson.pegawai_gaji_sebelum;
					var pegawai_gaji_sesudah = datajson.pegawai_gaji_sesudah;
					
					$('#pegawai_id').val(pegawai_id);
					$('#pegawai_tanggal').val(pegawai_tanggal);
					$('#pegawai_gaji_sebelum').val(pegawai_gaji_sebelum);
					$('#pegawai_gaji_sesudah').val(pegawai_gaji_sesudah);
					$('#pegawai_id').val(datajson.pegawai_id);
				});
			}
		}
		function tambah_data(){
			$('.form_lihat_data').hide();
			reset_form();
			$('.label_aksi_form').html('Form Penambahan Data Kenaikan Gaji Berkala');
			if($('.form_tambah_data').is(":visible")){
				$('.form_tambah_data').slideUp('slow',function(){
					$('.form_tambah_data').slideDown('fast',function(){
						$('html, body').animate({
							scrollTop: $(".form_tambah_data").offset().top-60
						}, 500);
						
					});
				});
			}else{
				$('.form_tambah_data').slideDown('fast',function(){
					$('html, body').animate({
						scrollTop: $(".form_tambah_data").offset().top-60
					}, 500);
				});
			}
		}
		
		$(document).ready(function() {
			$('.pickmutasipensiun').pickadate({
			monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
			weekdaysShort: ['Mng','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
			selectMonths: true,
			selectYears: 80,
			max: true,
			today:'Hari ini',
			clear: 'Hapus',
			close: 'Keluar',
			format: 'dd mmm yyyy',
			formatSubmit: 'yyyy-mm-dd'
			});
		});

		$('#pegawai_jenis_pensiun_id').on('change', function() {
			var jenis = $('#pegawai_jenis_pensiun_id option:selected').val();
			if(jenis == 2){
				$('#wafat').show();
				$('#pindah').hide();
			}
			else if(jenis == 3){
				$('#pindah').show();
				$('#wafat').hide();
			}
			else{
				$('#pindah').hide();
				$('#wafat').hide();
			}
		});
	</script>
