
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Ubah Password</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Password Lama</label>
						<div class="col-md-9">
							<input type="password" name="konfir_password_lama" id="konfir_password_lama"  class="form-control input-xs wajib" type="text">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Password Baru</label>
						<div class="col-md-9">
							<input type="password" name="password_baru" id="password_baru"  class="form-control input-xs wajib" type="text">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Konfirmasi Password Baru</label>
						<div class="col-md-9">
							<input type="password" name="konfir_password_baru" id="konfir_password_baru"  class="form-control input-xs wajib" type="text">
							<label id="confirm-error" class="validation-error-label" style="display: none;">Password baru dan Konfirmasi Password baru tidak sama</label>
							<label id="confirm-good" class="validation-error-label validation-valid-label" for="with_icon" style="display: none;">Konfirmasi password berhasil.</label>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" id="submit_pass" class="btn btn-xs btn-primary">Simpan</button>
					<input type="hidden" name="password_lama" id="password_lama"  class="form-control input-xs wajib" type="text" value="<?php echo $password_lama["password"] ?>">
					<!-- <input type="hidden" name="bahasa_id" id="bahasa_id"  class="form-control input-xs wajib" type="text" value="<?php echo 'bb';?>" > -->
				</div>
			</div>

		</form>
	</div>
</div>
<script type="text/javascript">
	function chekcode(ini){
		
	}

	$('#konfir_password_baru').on('keyup keypress blur change', function(e) {
		var pass   	 = $('#password_baru').val();
		var confpass  = $('#konfir_password_baru').val();
		// console.log(confpass);

		if(pass!=confpass){
			$('#confirm-error').show();
			$('#confirm-good').hide();
			$( "#submit_pass" ).prop( "disabled", true );
		}else{
			$('#confirm-good').show();
			$('#confirm-error').hide();
			$( "#submit_pass" ).prop( "disabled", false );
		}
	});

	// $("#password_lama").keyup(function(){
	// 	var input = $('#password_lama');
	// 	var md5 = md5($('#password_lama'));
	// 	alert(md5);
	// });
	// $(document).ready(function() {

	// 	$( "#password_lama" ).rules( "add", {
	// 		required: true,
	// 		remote: {
	// 			url: base_url+'password/ganti_password/ubah',
	// 			type: "post",
	// 			data: {
	// 				password_lama: function() {
	// 					return $( "#password_lama" ).val();
	// 				}
	// 			}
	// 		},
	// 		messages: {
	// 			required: "Wajib Diisi",
	// 			remote: "Password Lama yang anda masukkan salah"
	// 		}
	// 	});
	// });
</script>
