
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Perubahan Data Kota</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Pilih Provinsi</label>
						<div class="col-md-9">
							<select name="KODE_PROVINSI" id="KODE_PROVINSI" data-placeholder="Pilih Provinsi" class="select-size-xs wajib" onchange="setKode(this)">
								<option></option>
								<?php foreach ($provinsi as $i) {
									echo '<option value="'.$i["KODE"].'" data-kode="'.$i["KODE"].'" '.(($kota["KODE_PROVINSI"]==$i["KODE"])?"selected":"").'>'.$i["PROVINSI"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Kode Kota</label>
						<div class="col-md-1">
							<input type="text" name="KODE" id="KODE"  class="form-control input-xs wajib" type="text" value="<?php echo $kota["KODE"];?>" >
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Nama Kota</label>
						<div class="col-md-9">
							<input type="text" name="KOTA" id="KOTA"  class="form-control input-xs wajib" type="text" value="<?php echo $kota["KOTA"];?>" >
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
					<a href="<?php echo base_url();?>master/kota" class="btn btn-xs btn-warning">Kembali</a>
					<input type="hidden" name="ID" id="ID"  class="form-control input-xs wajib" type="text" value="<?php echo $kota["ID"];?>" >
					
				</div>
			</div>

		</form>
	</div>
</div>
<script type="text/javascript">
	
	$(document).ready(function() {
		

	});
	function setKode(ini)
	{
		var TenderRUPId = $('option:selected', ini).attr('data-TenderRUPId');
		var kode = $('option:selected', ini).attr('data-kode');

		$('#KODE').val("").val(kode);
	}
</script>
