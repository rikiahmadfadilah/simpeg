
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Tambah Data Provinsi</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Kode Provinsi</label>
						<div class="col-md-1">
							<input type="text" name="KODE" id="KODE"  class="form-control input-xs wajib" type="text">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Nama Provinsi</label>
						<div class="col-md-9">
							<input type="text" name="PROVINSI" id="PROVINSI"  class="form-control input-xs wajib" type="text">
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
					<a href="<?php echo base_url();?>master/provinsi" class="btn btn-xs btn-warning">Kembali</a>
					
				</div>
			</div>

		</form>
	</div>
</div>
<script type="text/javascript">
	function chekcode(ini){
		
	}
	$(document).ready(function() {
		$( "#KODE" ).rules( "add", {
			required: true,
			remote: {
				url: base_url+'master/provinsi/checkKodeProvinsi',
				type: "post",
				data: {
					KODE: function() {
						return $( "#KODE" ).val();
					}
				}
			},
			messages: {
				required: "Wajib Diisi",
				remote: "Kode Provinsi Telah Digunakan"
			}
		});
		$( "#PROVINSI" ).rules( "add", {
			required: true,
			remote: {
				url: base_url+'master/provinsi/checkNamaProvinsi',
				type: "post",
				data: {
					PROVINSI: function() {
						return $( "#PROVINSI" ).val();
					}
				}

			},
			messages: {
				required: "Wajib Diisi",
				remote: "Nama Provinsi Sudah Tersedia"
			}
		});
		
	});
</script>
