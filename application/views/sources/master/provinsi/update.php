
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Perubahan Data Provinsi</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Kode Provinsi</label>
						<div class="col-md-1">
							<input type="text" name="KODE" id="KODE"  class="form-control input-xs wajib" type="text" value="<?php echo $provinsi["KODE"];?>" >
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Nama Provinsi</label>
						<div class="col-md-9">
							<input type="text" name="PROVINSI" id="PROVINSI"  class="form-control input-xs wajib" type="text" value="<?php echo $provinsi["PROVINSI"];?>" >
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
					<a href="<?php echo base_url();?>master/provinsi" class="btn btn-xs btn-warning">Kembali</a>
					<input type="hidden" name="ID" id="ID"  class="form-control input-xs wajib" type="text" value="<?php echo $provinsi["ID"];?>" >
					
				</div>
			</div>

		</form>
	</div>
</div>
