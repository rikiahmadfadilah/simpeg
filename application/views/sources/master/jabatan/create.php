
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Tambah Data Jabatan</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Kode Jabatan</label>
						<div class="col-md-2">
							<input type="text" name="jabatan_kode" id="jabatan_kode"  data-placeholder="Kode Jabatan" class="form-control input-xs wajib" type="text">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Nama Jabatan</label>
						<div class="col-md-9">
							<input type="text" name="jabatan_nama" id="jabatan_nama"  data-placeholder="Nama Jabatan" class="form-control input-xs wajib" type="text">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Tipe Jabatan</label>
						<div class="col-md-9">
							<select name="jabatan_tipe" id="jabatan_tipe" data-placeholder="Tipe Jabatan" class="select-size-xs wajib">
								<option value=""></option>
								<option value="1">Fungsional Tertentu</option>
								<option value="2">Fungsional Umum</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
					<a href="<?php echo base_url();?>master/jabatan" class="btn btn-xs btn-warning">Kembali</a>
					
				</div>
			</div>
		</form>
	</div>
</div>