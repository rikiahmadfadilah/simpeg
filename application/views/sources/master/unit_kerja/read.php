
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Lihat Data Unit Kerja</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Induk Unit Kerja</label>
						<div class="col-md-9">
							<input type="text" name="nama_unker_induk" id="nama_unker_induk"  class="form-control input-xs wajib" type="text" value="<?php echo $unit_kerja["nama_unker_induk"];?>" disabled="disabled">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Kode Unit Kerja</label>
						<div class="col-md-2">
							<input type="text" name="kode_unker" id="kode_unker"  class="form-control input-xs wajib" type="text" value="<?php echo $unit_kerja["kode_unker"];?>" disabled="disabled">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Nama Unit Kerja</label>
						<div class="col-md-9">
							<input type="text" name="nama_unker" id="nama_unker"  class="form-control input-xs wajib" type="text" value="<?php echo $unit_kerja["nama_unker"];?>" disabled="disabled">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Nama Jabatan Struktural</label>
						<div class="col-md-9">
							<input type="text" name="nama_jab_struk" id="nama_jab_struk"  class="form-control input-xs wajib" type="text" value="<?php echo $unit_kerja["nama_jab_struk"];?>" disabled="disabled">
						</div>
					</div>
					<!-- <div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Jabatan/Eselon</label>
						<div class="col-md-9">
							<input type="text" name="unit_kerja_nama" id="unit_kerja_nama"  class="form-control input-xs wajib" type="text" value="<?php echo $unit_kerja["jabatan_nama"];?>" disabled="disabled">
						</div>
					</div> -->
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<a href="<?php echo base_url();?>master/unit_kerja" class="btn btn-xs btn-warning">Kembali</a>
					<input type="hidden" name="idunker" id="idunker"  class="form-control input-xs wajib" type="text" value="<?php echo $unit_kerja["idunker"];?>" >
					
				</div>
			</div>

		</form>
	</div>
</div>
<script type="text/javascript">
	
	$(document).ready(function() {
		

	});
</script>
