
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Perubahan Data Unit Kerja</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Induk Unit Kerja</label>
						<div class="col-md-9">
							<select name="idparent" id="idparent" data-placeholder="Unit Kerja" data-id="0" class="select-size-xs wajib">
								<option></option>
								<?php foreach ($unit_kerja as $i) {
									echo '<option value="'.$i["idunker"].'" eselon="'.$i["eselon_unker"].'" '.(($data["idunker_induk"]==$i["idunker"])?"selected":"").'>'.$i["nama_unker"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Kode Unit Kerja</label>
						<div class="col-md-2">
							<input type="text" name="kode_unker" id="kode_unker"  class="form-control input-xs wajib" type="text" value="<?php echo $data["kode_unker"];?>" >
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Nama Unit Kerja</label>
						<div class="col-md-9">
							<input type="text" name="nama_unker" id="nama_unker"  class="form-control input-xs wajib" type="text" value="<?php echo $data["nama_unker"];?>" >
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Pilih Eselon</label>
						<div class="col-md-2">
							<select name="eselon_unker" id="eselon_unker" data-placeholder="Eselon" class="select-size-xs wajib">
								<option></option>
								<?php foreach ($eselon as $i) {
									echo '<option value="'.$i["KODE_ESL"].'" data-kode="'.$i["KODE_ESL"].'" '.(($data["eselon_unker"]==$i["KODE_ESL"])?"selected":"").'>'.$i["ESELON"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Nama Jabatan Struktural</label>
						<div class="col-md-9">
							<input type="text" name="nama_jab_struk" id="nama_jab_struk"  class="form-control input-xs wajib" value="<?php echo $data["nama_jab_struk"];?>">
						</div>
					</div>
					<!-- <div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Pusat/UPT</label>
						<div class="col-md-2">
							<select name="unit_kerja_pusat_upt" id="unit_kerja_pusat_upt" data-placeholder="Eselon" class="select-size-xs wajib">
								<option value="P" <?php echo ($data["unit_kerja_pusat_upt"]=='P'?'selected':'')?>>Pusat</option>
								<option value="U" <?php echo ($data["unit_kerja_pusat_upt"]=='U'?'selected':'')?>>UPT</option>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">No Telpon</label>
						<div class="col-md-5">
							<input type="text" name="unit_kerja_telp" id="unit_kerja_telp"  class="form-control input-xs wajib" type="text" value="<?php echo $data["unit_kerja_telp"];?>" >
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Ext</label>
						<div class="col-md-5">
							<input type="text" name="unit_kerja_ext" id="unit_kerja_ext"  class="form-control input-xs gakwajib" type="text" value="<?php echo $data["unit_kerja_ext"];?>" >
						</div>
					</div>
					<div class="form-group form-group-xs dalam_negeri">
						<label class="col-md-3 control-label">Provinsi</label>
						<div class="col-md-9">
							<select name="unit_kerja_provinsi_id" id="unit_kerja_provinsi_id" data-placeholder="Pilih Provinsi" class="select-size-xs wajib" data-oldvalue="<?php echo $data["unit_kerja_kota_id"];?>" onchange="get_kota(this,'unit_kerja_kota_id')">
								<option></option>
								<?php foreach ($provinsi as $i) {
									echo '<option value="'.$i["provinsi_kode"].'" '.(($data["unit_kerja_provinsi_id"]==$i["provinsi_kode"])?"selected":"").'>'.$i["provinsi_nama"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs dalam_negeri">
						<label class="col-md-3 control-label">Kota</label>
						<div class="col-md-9">
							<select name="unit_kerja_kota_id" id="unit_kerja_kota_id" data-placeholder="Pilih Kota" class="select-size-xs wajib" data-oldvalue="<?php echo $data["unit_kerja_kec_id"];?>" onchange="get_kecamatan(this,'unit_kerja_kec_id')">
								<option></option>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs dalam_negeri">
						<label class="col-md-3 control-label">Kecamatan</label>
						<div class="col-md-9">
							<select name="unit_kerja_kec_id" id="unit_kerja_kec_id" data-placeholder="Pilih Kecamatan" class="select-size-xs gakwajib" data-oldvalue="<?php echo $data["unit_kerja_kel_id"];?>" onchange="get_kelurahan(this,'unit_kerja_kel_id')">
								<option></option>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs dalam_negeri">
						<label class="col-md-3 control-label">Kelurahan</label>
						<div class="col-md-9">
							<select name="unit_kerja_kel_id" id="unit_kerja_kel_id" data-placeholder="Pilih Kelurahan" class="select-size-xs gakwajib">
								<option></option>
							</select>
						</div>
					</div> -->
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
					<a href="<?php echo base_url();?>master/unit_kerja" class="btn btn-xs btn-warning">Kembali</a>
					<input type="hidden" name="idunker" id="idunker"  class="form-control input-xs wajib" type="text" value="<?php echo $data["idunker"];?>" >
					<!-- <input type="hidden" name="unit_kerja_id_kode_old" id="unit_kerja_id_kode_old"  class="form-control input-xs wajib" type="text" value="<?php echo $data["unit_kerja_id_kode"];?>" >
					<input type="hidden" name="unit_kerja_kode_old" id="unit_kerja_kode_old"  class="form-control input-xs wajib" type="text" value="<?php echo $data["unit_kerja_kode"];?>" >
					<input type="hidden" id="unit_kerja_id_kode_1" value="<?php echo $data["unit_kerja_parent_id_kode"];?>">
					<input type="hidden" id="unit_kerja_id_kode_2" value="<?php echo $data["unit_kerja_parent_id_kode_2"];?>">
					<input type="hidden" id="unit_kerja_id_kode_3" value="<?php echo $data["unit_kerja_parent_id_kode_3"];?>">
					<input type="hidden" id="jenis_input" name="jenis_input" value="1"> -->
				</div>
			</div>

		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#kode_unker').formatter({
		pattern: '{{9999999999}}'
		});
	});
</script>
