
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Tambah Data Unit Kerja</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Induk Unit Kerja</label>
						<div class="col-md-9">
							<select name="idparent" id="idparent" data-placeholder="Unit Kerja" class="select-size-xs wajib">
								<option></option>
								<?php foreach ($unit_kerja as $i) {
									echo '<option value="'.$i["idunker"].'" eselon="'.$i["eselon_unker"].'">'.$i["nama_unker"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Kode Unit Kerja</label>
						<div class="col-md-2">
							<input type="text" name="kode_unker" id="kode_unker"  class="form-control input-xs wajib" type="text">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Nama Unit Kerja</label>
						<div class="col-md-9">
							<input type="text" name="nama_unker" id="nama_unker"  class="form-control input-xs wajib" type="text">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Pilih Eselon</label>
						<div class="col-md-2">
							<select name="eselon_unker" id="eselon_unker" data-placeholder="Pilih Eselon" class="select-size-xs wajib">
								<option></option>
								<?php foreach ($eselon as $i) {
									echo '<option value="'.$i["KODE_ESL"].'">'.$i["ESELON"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Nama Jabatan Struktural</label>
						<div class="col-md-9">
							<input type="text" name="nama_jab_struk" id="nama_jab_struk"  class="form-control input-xs wajib" type="text">
						</div>
					</div>
					<!-- <div class="form-group form-group-xs">
						<label class="col-md-3 control-label">No Telp. dan No Fax</label>
						<div class="col-md-5">
							<input type="text" name="unit_kerja_telp" id="unit_kerja_telp"  class="form-control input-xs wajib" type="text" value="">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">No Ext</label>
						<div class="col-md-5">
							<input type="text" name="unit_kerja_ext" id="unit_kerja_ext"  class="form-control input-xs wajib" type="text" value="">
						</div>
					</div>
					<div class="form-group form-group-xs dalam_negeri">
						<label class="col-md-3 control-label">Provinsi</label>
						<div class="col-md-9">
							<select name="unit_kerja_provinsi_id" id="unit_kerja_provinsi_id" data-placeholder="Pilih Provinsi" class="select-size-xs wajib" onchange="get_kota(this,'unit_kerja_kota_id')">
								<option></option>
								<?php foreach ($provinsi as $i) {
									echo '<option value="'.$i["provinsi_kode"].'" kode="'.$i["provinsi_kode"].'">'.$i["provinsi_nama"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs dalam_negeri">
						<label class="col-md-3 control-label">Kota</label>
						<div class="col-md-9">
							<select name="unit_kerja_kota_id" id="unit_kerja_kota_id" data-placeholder="Pilih Kota" class="select-size-xs wajib" onchange="get_kecamatan(this,'unit_kerja_kec_id')">
								<option></option>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs dalam_negeri">
						<label class="col-md-3 control-label">Kecamatan</label>
						<div class="col-md-9">
							<select name="unit_kerja_kec_id" id="unit_kerja_kec_id" data-placeholder="Pilih Kecamatan" class="select-size-xs gakwajib" onchange="get_kelurahan(this,'unit_kerja_kel_id')">
								<option></option>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs dalam_negeri">
						<label class="col-md-3 control-label">Kelurahan</label>
						<div class="col-md-9">
							<select name="unit_kerja_kel_id" id="unit_kerja_kel_id" data-placeholder="Pilih Kelurahan" class="select-size-xs gakwajib">
								<option></option>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Alamat Unit Kerja</label>
						<div class="col-md-9">
							<textarea class="form-control" name="unit_kerja_alamat" id="unit_kerja_alamat" style="min-height: 100px;" value=""></textarea>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Unker Root</label>
						<div class="col-md-9">
							<select name="unker_root" id="unker_root" data-placeholder="Unit Kerja Root" class="select-size-xs">
								<option></option>
								<?php foreach ($satker as $i) {
									echo '<option value="'.$i["unit_kerja_kode"].'" parent_id_kode="'.$i["unit_kerja_id_kode"].'" jabatan_id="'.$i["jabatan_id"].'">'.$i["unit_kerja_nama"].'</option>';
								}?>
							</select>
						</div>
					</div> -->
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
					<a href="<?php echo base_url();?>master/unit_kerja" class="btn btn-xs btn-warning">Kembali</a>
					
				</div>
			</div>

		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$( "#kode_unker" ).rules( "add", {
			required: true,
			remote: {
				url: base_url+'master/unit_kerja/checkKodeUnitKerja',
				type: "post",
				data: {
					kode_unker: function() {
						return $( "#kode_unker" ).val();
					}
				}
			},
			messages: {
				required: "Wajib Diisi",
				remote: "Kode Unit Kerja Telah Digunakan"
			}
		});
	});

	$('#kode_unker').formatter({
		pattern: '{{9999999999}}'
	});

	function  get_kota(ini,nextopsi) {
		var provinsi_kode = $(ini).val();
		$.ajax({
			url: base_url+'kepegawaian/pns/get_kota',
			type: "post",
			dataType: 'json',
			data: { provinsi_kode: provinsi_kode},
			success: function (data) {
				var kota = data.kota;
				var newoption = "<option></option>";
				for(var i = 0; i < kota.length; i ++){
					newoption+='<option value="'+kota[i].kota_kode+'">'+kota[i].kota_nama+'</option>';
				}
				$('#'+nextopsi).html(newoption);
			}
		});
	}
	function  get_kecamatan(ini,nextopsi) {
		var kota_kode = $(ini).val();
		$.ajax({
			url: base_url+'kepegawaian/pns/get_kecamatan',
			type: "post",
			dataType: 'json',
			data: { kota_kode: kota_kode},
			success: function (data) {
				var kecamatan = data.kecamatan;
				var newoption = "<option></option>";
				for(var i = 0; i < kecamatan.length; i ++){
					newoption+='<option value="'+kecamatan[i].kecamatan_kode+'">'+kecamatan[i].kecamatan_nama+'</option>';
				}
				$('#'+nextopsi).html(newoption);
			}
		});
	}
	function  get_kelurahan(ini,nextopsi) {
		var kecamatan_kode = $(ini).val();
		$.ajax({
			url: base_url+'kepegawaian/pns/get_kelurahan',
			type: "post",
			dataType: 'json',
			data: { kecamatan_kode: kecamatan_kode},
			success: function (data) {
				var kelurahan = data.kelurahan;
				var newoption = "<option></option>";
				for(var i = 0; i < kelurahan.length; i ++){
					newoption+='<option value="'+kelurahan[i].kelurahan_kode+'">'+kelurahan[i].kelurahan_nama+'</option>';
				}
				$('#'+nextopsi).html(newoption);
			}
		});
	}
</script>