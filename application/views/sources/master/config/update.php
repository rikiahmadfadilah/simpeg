
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Perubahan Data Konfigurasi</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Kode</label>
						<div class="col-md-2">
							<input type="text" readonly name="CONFIG_CODE" id="CONFIG_CODE"  class="form-control input-xs wajib" type="text" value="<?php echo $config["CONFIG_CODE"];?>" >
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Nama Konfigurasi</label>
						<div class="col-md-9">
							<input type="text" name="CONFIG_NAME" id="CONFIG_NAME"  class="form-control input-xs wajib" type="text" value="<?php echo $config["CONFIG_NAME"];?>" >
						</div>
					</div>
					<?php if($config['CONFIG_TYPE'] == 0){ ?>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Isi Konfigurasi</label>
						<div class="col-md-9">
							<input type="text" name="CONFIG_VALUE" id="CONFIG_VALUE"  class="form-control input-xs wajib" type="text" value="<?php echo $config["CONFIG_VALUE"];?>">
						</div>
					</div>
					<?php }else{ ?>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Isi Konfigurasi</label>
						<div class="col-md-9">
							<img src="<?php echo base_url($config['CONFIG_VALUE']) ?>" height="150px;">
							<input type="file" name="PATH" id="PATH" class="form-control input-xs" style="padding-top: 0px !important;padding-bottom: 0px !important;" accept="image/*">
							<input type="hidden" name="PATH_OLD" id="PATH_OLD" value="<?php echo $config["CONFIG_VALUE"]; ?>">
						</div>
					</div>
					<?php } ?>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Deskripsi Konfigurasi</label>
						<div class="col-md-9">
							<textarea class="form-control input-xs wajib" name="CONFIG_DESCRIPTION" id="CONFIG_DESCRIPTION" value="<?php echo $config["CONFIG_DESCRIPTION"];?>" style="height: 60px"><?php echo $config["CONFIG_DESCRIPTION"];?></textarea>
						</div>
					</div>
					<input type="hidden" name="CONFIG_TYPE" value="<?php echo $config["CONFIG_TYPE"] ?>">
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
					<a href="<?php echo base_url();?>master/config" class="btn btn-xs btn-warning">Kembali</a>
				</div>
			</div>

		</form>
	</div>
</div>
