<style type="text/css">

</style>
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Data Kecamatan</h6>
		<div class="heading-elements">
			<div class="heading-btn">
				<a href="<?php echo base_url().'master/kecamatan/create';?>" class="btn btn-primary btn-labeled btn-xs"><b><i class=" icon-plus3"></i></b> Tambah Data Kecamatan</a>
			</div>
		</div>
	</div>

	<div class="panel-body">
		<table class="table table-xxs datatable-basic table-bordered table-striped table-hover datatable-responsive ropeg_table" id="list_data">
			<thead>
				<tr>
					<th class="text-center" width="10%">No</th>
					<th class="text-center" width="50%">Kecamatan</th>
					<th class="text-center" width="50%">Kota</th>
					<th class="text-center" style="width: 150px;min-width: 150px;">Status</th>
					<th class="text-center" style="width: 80px;min-width: 80px;">Aksi</th>
				</tr>
			</thead>
			
		</table>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#list_data').dataTable( {
			"processing": true,
			"serverSide": true,
			"bServerSide": true,
			"sAjaxSource": base_url+"master/kecamatan/list_data",
			"aaSorting": [],
			"order": [],
			"iDisplayLength": 10,
			"aoColumns": [
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-left" },
			{ "bSortable": false, "sClass": "text-center" },
			{ "bSortable": false, "sClass": "text-center" },
			],
			"fnDrawCallback": function () {
				set_default_datatable();
			},
		});
	});
	function set_aktif(id){
		$('#text_konfirmasi').html('Anda yakin mengaktifkan data ini ?');
		$('#konfirmasipenyimpanan').modal('show');
		$('#setujukonfirmasibutton').unbind();
		$('#setujukonfirmasibutton').on('click', function () {
			$('#konfirmasipenyimpanan').modal('hide');
			$('.angkadoank').inputmask('remove');
			$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
			window.location.href = base_url+'master/kecamatan/aktif/'+id;
		});
	}
	function set_non_aktif(id){
		$('#text_konfirmasi').html('Anda yakin nonaktifkan data ini ?');
		$('#konfirmasipenyimpanan').modal('show');
		$('#setujukonfirmasibutton').unbind();
		$('#setujukonfirmasibutton').on('click', function () {
			$('#konfirmasipenyimpanan').modal('hide');
			$('.angkadoank').inputmask('remove');
			$('#text_konfirmasi').html('Anda yakin dengan data ini ?');
			window.location.href = base_url+'master/kecamatan/non_aktif/'+id;
		});
	}
</script>
