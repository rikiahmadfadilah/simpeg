
<div class="panel panel-flat border-top-info border-bottom-info panel-default">
	<div class="panel-heading">
		<h6 class="panel-title">Tambah Data Kecamatan</h6>
	</div>

	<div class="panel-body">
		<form class="need_validation form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Pilih Kota/Kabupaten</label>
						<div class="col-md-9">
							<select name="KODE_KOTA" id="KODE_KOTA" data-placeholder="Pilih Kota/Kabupaten" class="select-size-xs wajib" onchange="setKode(this)">
								<option></option>
								<?php foreach ($kota as $i) {

									echo '<option value="'.$i["KODE"].'" data-kode="'.$i["KODE"].'">'.$i["KOTA"].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Kode Kecamatan</label>
						<div class="col-md-1">
							<input type="text" name="KODE" id="KODE"  class="form-control input-xs wajib kode" type="text">
						</div>
					</div>
					<div class="form-group form-group-xs">
						<label class="col-md-3 control-label">Nama Kecamatan</label>
						<div class="col-md-9">
							<input type="text" name="KECAMATAN" id="KECAMATAN"  class="form-control input-xs wajib" type="text">
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-xs btn-primary">Simpan</button> 
					<a href="<?php echo base_url();?>master/kecamatan" class="btn btn-xs btn-warning">Kembali</a>
					
				</div>
			</div>

		</form>
	</div>
</div>
<script type="text/javascript">
	function chekcode(ini){
		
	}
	$(document).ready(function() {
		$( "#kecamatan_kode" ).rules( "add", {
			required: true,
			remote: {
				url: base_url+'master/kecamatan/checkKodekecamatan',
				type: "post",
				data: {
					kecamatan_kode: function() {
						return $( "#kecamatan_kode" ).val();
					}
				}
			},
			messages: {
				required: "Wajib Diisi",
				remote: "Kode kecamatan Telah Digunakan"
			}
		});
		$( "#kecamatan_nama" ).rules( "add", {
			required: true,
			remote: {
				url: base_url+'master/kecamatan/checkNamakecamatan',
				type: "post",
				data: {
					kecamatan_nama: function() {
						return $( "#kecamatan_nama" ).val();
					}
				}

			},
			messages: {
				required: "Wajib Diisi",
				remote: "Nama kecamatan Sudah Tersedia"
			}
		});
		
	});
	function setKode(ini)
	{
		var TenderRUPId = $('option:selected', ini).attr('data-TenderRUPId');
		var kode = $('option:selected', ini).attr('data-kode');

		$('.kode').val("").val(kode);
	}
</script>
