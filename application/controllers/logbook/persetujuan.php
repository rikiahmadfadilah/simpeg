<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Persetujuan extends CI_Controller {

	public function __construct(){
		parent::__construct();
		// $this->load->model('auth/auth_model','',TRUE);
		// $this->load->model('kepegawaian/pns/pns_model','',TRUE);
		$this->load->model('logbook/persetujuan_model','',TRUE);
		//$this->load->model('logbook/persetujuan/pns_model','',TRUE);

		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index()
	{
		if($this->access->permission('approve')){
		$data = array();
		$data["unit_kerja_nama"] = $this->session->userdata('unit_kerja_nama');
		$data["pegawai_nama_jabatan"] = $this->session->userdata('pegawai_nama_jabatan');
		// echo "<pre>";
		// print_r($this->session->userdata());die;
		$this->template->display('logbook/persetujuan/index', $data);
	}	
	}
	function verifikasi($jadwal_kegiatan_id){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];

					$dataupdate = array(
						'jadwal_kegiatan_status' => 3
					);
					$this->persetujuan_model->update_jadwal_kegiatan($dataupdate, $jadwal_kegiatan_id);

					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan Data Pendidikan Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);
					redirect(base_url().'logbook/persetujuan');
			}
			$pegawai_unit_kerja_id = $this->session->userdata('pegawai_unit_kerja_id');
			$parent_unit_kerja_id = $this->session->userdata('parent_unit_kerja_id');
			$atasan_parent_unit_kerja_id = $this->session->userdata('atasan_parent_unit_kerja_id');
			$data = array();
			//$data['pegawai']  	= $this->persetujuan_model->getPegawai($pegawai_id)->row_array();
			$data['atasan_pegawai'] = $this->persetujuan_model->getDataPegawaiByUnitKerja($parent_unit_kerja_id)->row_array();
			$data['atasan_pegawais'] = $this->persetujuan_model->getDataPegawaiByUnitKerja($parent_unit_kerja_id)->result_array();
			// echo "<pre/>";
			// print_r($data['atasan_pegawais']);die;
			$data['history']  	= $this->persetujuan_model->getkegiatanHarian($jadwal_kegiatan_id)->result_array();
			// echo "<pre/>";
			// print_r($data['history']);die;			
			$data["getSKP"] 	= $this->persetujuan_model->getSKP()->result_array();			
			$this->template->display('logbook/persetujuan/detail', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	
	public function list_data_aktif(){
		$default_order = "";
		$limit = 10;
		// $where = "jadwal_kegiatan_status = 2";
		$where = "";
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama',
			'pegawai_nama_jabatan',
			'unit_kerja_nama',
			'unit_kerja_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->persetujuan_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->persetujuan_model->get_count_all_data($search,$field_name, $where);

		$tanggal = '2018-10-09';
		$aaData = array();
		$getData 	= $this->persetujuan_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$no,
				$row["pemilik"],
				$row["jadwal_kegiatan_tanggal"],
				($row["jumlah_menit"] != "")?$row["jumlah_menit"].' Menit':"-",
				$row["jumlah_kegiatan"],
				$row["jadwal_kegiatan_status_nama"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'logbook/persetujuan/verifikasi/'.urlencode($row["jadwal_kegiatan_id"]).'" class="lihat_data" data-popup="tooltip" title="Permintaan Verifikasi" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>
				</ul>'
				// <li><a href="'.base_url().'logbook/persetujuan/read/'.urlencode($row["pegawai_id"]).'" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>
				// <li><a href="'.base_url().'logbook/persetujuan/verifikasi/'.urlencode($row["jadwal_kegiatan_id"]).'" class="create_data" data-popup="tooltip" title="Verifikasi" data-placement="bottom"><i class="icon-file-check" style="font-size: 13px;"></i></a></li>
				
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}

	public function get_unit_kerja_hirarki()
	{
		$unit_kerja_parent_id_kode 	= $this->input->post('unit_kerja_parent_id_kode');
		$unit_kerja_level 	= $this->input->post('unit_kerja_level');
		$data["unit_kerja"] = $this->persetujuan_model->getUnitKerja($unit_kerja_parent_id_kode,$unit_kerja_level)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function getkegiatan($id){
		$data = $this->persetujuan_model->getkegiatan($id)->row_array();
		return $this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function aktif($jadwal_kegiatan_id = 0){

        $jadwal_kegiatan_idFilter = filter_var($jadwal_kegiatan_id, FILTER_SANITIZE_NUMBER_INT);
        if($this->access->permission('update')) {
            if($jadwal_kegiatan_id==$jadwal_kegiatan_idFilter) {

                $dataupdate = array(
                    'jadwal_kegiatan_status'   => 3
                );

                $del = $this->persetujuan_model->update_jadwal_kegiatan($dataupdate,$jadwal_kegiatan_id);
                $notify = array(
                    'title'     => 'Berhasil!',
                    'message'   => 'persetujuan Berhasil diaktifkan',
                    'status'    => 'success'
                );
                $this->session->set_flashdata('notify', $notify);

                redirect(base_url().'logbook/persetujuan');
            } else {
                $notify = array(
                    'title'     => 'Gagal!',
                    'message'   => 'persetujuan Gagal diaktifkan',
                    'status'    => 'error'
                );
                $this->session->set_flashdata('notify', $notify);
                redirect(base_url().'logbook/persetujuan');
            }
        } else {
            $notify = array(
                'title'     => 'Gagal!',
                'message'   => 'persetujuan Gagal diaktifkan',
                'status'    => 'error'
            );
            $this->session->set_flashdata('notify', $notify);
            redirect(base_url().'logbook/persetujuan');
        }
    }
}
