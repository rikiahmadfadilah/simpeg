<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Elogbook extends CI_Controller {

	public function __construct(){
		parent::__construct();
		// $this->load->model('auth/auth_model','',TRUE);
		// $this->load->model('kepegawaian/pns/pns_model','',TRUE);
		$this->load->model('logbook/elogbook_model','',TRUE);
		//$this->load->model('logbook/elogbook/pns_model','',TRUE);

		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index()
	{
		$data = array();
		$data["UNIT_KERJA"] = $this->session->userdata('UNIT_KERJA');
		$data["NAMA_JAB"] = $this->session->userdata('NAMA_JAB');
		// echo "<pre>";
		// print_r($this->session->userdata());die;
		$this->template->display('logbook/elogbook/index', $data);	
	}

	public function update($pegawai_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];

				//print_r($post['jenjang_jabatan_unit_kerja_id']);die;
				$jenjang_jabatan_unit_kerja_id = $post['jenjang_jabatan_unit_kerja_id'];
				$unit_kerja_id = '';
				for ($i=0; $i < count($jenjang_jabatan_unit_kerja_id); $i++) { 
					if($jenjang_jabatan_unit_kerja_id[$i] != ''){
						$unit_kerja_id = $jenjang_jabatan_unit_kerja_id[$i];
					}
				}
				$jenjang_jabatan_pegawai_id = $post['jenjang_jabatan_pegawai_id'];
				$jenjang_jabatan_pegawai_nip = $post['jenjang_jabatan_pegawai_nip'];
				$jenjang_jabatan_jabatan_id = $post['jenjang_jabatan_jabatan_id'];
				$jenjang_jabatan_jabatan_nama = $post['jenjang_jabatan_jabatan_nama'];
				//$jenjang_jabatan_unit_kerja_id = $post['jenjang_jabatan_unit_kerja_id	
				$jenjang_jabatan_tanggal_mulai = $post['jenjang_jabatan_tanggal_mulai'];
				$jenjang_jabatan_tanggal_selesai = $post['jenjang_jabatan_tanggal_selesai'];
				$jenjang_jabatan_nomor_sk = $post['jenjang_jabatan_nomor_sk'];
				$jenjang_jabatan_tanggal_sk = $post['jenjang_jabatan_tanggal_sk'];
				// $jenjang_jabatan_create_date = $post['jenjang_jabatan_create_date'];
				// $jenjang_jabatan_create_by = $post['jenjang_jabatan_create_by'];
				$jenjang_jabatan_status = 1;

				if($isUpdate == 0){
					$datacreate = array(
						'jenjang_jabatan_pegawai_id' => $jenjang_jabatan_pegawai_id, 
						'jenjang_jabatan_pegawai_nip' => $jenjang_jabatan_pegawai_nip, 
						'jenjang_jabatan_jabatan_id' => $jenjang_jabatan_jabatan_id, 
						'jenjang_jabatan_jabatan_nama' => $jenjang_jabatan_jabatan_nama, 
						'jenjang_jabatan_unit_kerja_id' => $unit_kerja_id, 
						'jenjang_jabatan_tanggal_mulai' => $jenjang_jabatan_tanggal_mulai, 
						'jenjang_jabatan_tanggal_selesai' => $jenjang_jabatan_tanggal_selesai, 
						'jenjang_jabatan_nomor_sk' => $jenjang_jabatan_nomor_sk, 
						'jenjang_jabatan_tanggal_sk' => $jenjang_jabatan_tanggal_sk, 
						'jenjang_jabatan_create_date' => date('Y-m-d H:i:s'), 
						'jenjang_jabatan_create_by' => $this->session->userdata('user_id'), 
						'jenjang_jabatan_status' => $jenjang_jabatan_status
					);					
					
					$insDb = $this->elogbook_model->create_jenjang_jabatan($datacreate);

					if($insDb > 0){
						$notify = array(
							'title' 	=> 'Berhasil!',
							'message' 	=> 'Penambahan Data Pendidikan Berhasil',
							'status' 	=> 'success'
						);
						$this->session->set_flashdata('notify', $notify);

						redirect(base_url().'logbook/elogbook/update/'.$jenjang_jabatan_pegawai_id);
					}else{
						$notify = array(
							'title' 	=> 'Gagal!',
							'message'	=> 'Penambahan Data Pendidikan Gagal, silahkan coba lagi',
							'status' 	=> 'error'
						);
						$this->session->set_flashdata('notify', $notify);
						redirect(base_url().'logbook/elogbook/update/'.$jenjang_jabatan_pegawai_id);
					}
				}else{
					$dataupdate = array(
						'jenjang_jabatan_pegawai_id' => $jenjang_jabatan_pegawai_id, 
						'jenjang_jabatan_pegawai_nip' => $jenjang_jabatan_pegawai_nip, 
						'jenjang_jabatan_jabatan_id' => $jenjang_jabatan_jabatan_id, 
						'jenjang_jabatan_jabatan_nama' => $jenjang_jabatan_jabatan_nama, 
						'jenjang_jabatan_unit_kerja_id' => $unit_kerja_id, 
						'jenjang_jabatan_tanggal_mulai' => $jenjang_jabatan_tanggal_mulai, 
						'jenjang_jabatan_tanggal_selesai' => $jenjang_jabatan_tanggal_selesai, 
						'jenjang_jabatan_nomor_sk' => $jenjang_jabatan_nomor_sk, 
						'jenjang_jabatan_tanggal_sk' => $jenjang_jabatan_tanggal_sk, 
						'jenjang_jabatan_create_date' => date('Y-m-d H:i:s'), 
						'jenjang_jabatan_create_by' => $this->session->userdata('user_id'), 
						'jenjang_jabatan_status' => $jenjang_jabatan_status
					);
					$this->elogbook_model->update_jenjang_jabatan($dataupdate, $jabatan_id);

					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan Data Pendidikan Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'logbook/elogbook/update/'.$jenjang_jabatan_pegawai_id);
				}
			}

			$data = array();
			$data['pegawai']  	= $this->elogbook_model->getPegawai($pegawai_id)->row_array();
			$data['history']  	= $this->elogbook_model->getJenjangJabatan($pegawai_id)->result_array();
			$data['golongan']  	= $this->elogbook_model->getGolongan()->result_array();
			$data["unit_kerja"] = $this->elogbook_model->getUnitKerja(1,2)->result_array();
			$data["jabatan"] 	= $this->elogbook_model->getJabatan()->result_array();
			$this->template->display('logbook/elogbook/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}

	public function kegiatan($jadwal_kegiatan_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];

				$id = $post["kegiatan_id"];
				$kegiatan_jadwal_kegiatan_id = $post["kegiatan_jadwal_kegiatan_id"];
				$kegiatan_waktu_mulai = $post["kegiatan_waktu_mulai"];
				$kegiatan_waktu_akhir = $post["kegiatan_waktu_akhir"];
				$kegiatan_skp = $post["kegiatan_skp"];
				$kegiatan_harian_nama = $post["kegiatan_harian_nama"];
				$kegiatan_harian_keterangan = $post["kegiatan_harian_keterangan"];				
				$kegiatan_status = 1;

				if($isUpdate == 0){
					$datacreate = array(
						'kegiatan_jadwal_kegiatan_id' => $jadwal_kegiatan_id,
						'kegiatan_waktu_mulai' => $kegiatan_waktu_mulai,
						'kegiatan_waktu_akhir' => $kegiatan_waktu_akhir,
						'kegiatan_skp' => $kegiatan_skp,
						'kegiatan_harian_nama' => $kegiatan_harian_nama,
						'kegiatan_harian_keterangan' => $kegiatan_harian_keterangan,
						'kegiatan_create_date' => date('Y-m-d H:i:s'),
						'kegiatan_create_by' => $this->session->userdata('user_id'),
						'kegiatan_status' => $kegiatan_status
					);
												
					$insDb = $this->elogbook_model->create_kegiatan($datacreate);

					if($insDb > 0){
						$notify = array(
							'title' 	=> 'Berhasil!',
							'message' 	=> 'Penambahan Data Pendidikan Berhasil',
							'status' 	=> 'success'
						);
						$this->session->set_flashdata('notify', $notify);

						redirect(base_url().'logbook/elogbook/kegiatan/'.$jadwal_kegiatan_id);
					}else{
						$notify = array(
							'title' 	=> 'Gagal!',
							'message'	=> 'Penambahan Data Pendidikan Gagal, silahkan coba lagi',
							'status' 	=> 'error'
						);
						$this->session->set_flashdata('notify', $notify);
						redirect(base_url().'logbook/elogbook/kegiatan/'.$jadwal_kegiatan_id);
					}
				}else{
					$dataupdate = array(
						'kegiatan_jadwal_kegiatan_id' => $jadwal_kegiatan_id,
						'kegiatan_waktu_mulai' => $kegiatan_waktu_mulai,
						'kegiatan_waktu_akhir' => $kegiatan_waktu_akhir,
						'kegiatan_skp' => $kegiatan_skp,
						'kegiatan_harian_nama' => $kegiatan_harian_nama,
						'kegiatan_harian_keterangan' => $kegiatan_harian_keterangan,
						'kegiatan_create_date' => date('Y-m-d H:i:s'),
						'kegiatan_create_by' => $this->session->userdata('user_id'),
						'kegiatan_status' => $kegiatan_status,
					);
					$this->elogbook_model->update_kegiatan($dataupdate, $id);

					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan Data Pendidikan Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'logbook/elogbook/kegiatan/'.$jadwal_kegiatan_id);
				}
			}

			$data = array();
			//$data['pegawai']  	= $this->elogbook_model->getPegawai($pegawai_id)->row_array();
			// $data['kegiatan'] = $this->elogbook_model->getkegiatanHarian($jadwal_kegiatan_id)->row_array();
			$data['history']  	= $this->elogbook_model->getkegiatanHarian($jadwal_kegiatan_id)->result_array();
			// echo "<pre/>";	
			// print_r($data['history']);die;		
			$data["getSKP"] 	= $this->elogbook_model->getSKP()->result_array();
			$this->template->display('logbook/elogbook/kegiatan', $data);
		}else{
			$this->access->redirect('404');
		}
	}

	function insertjadwal($id, $jadwal){
		if($this->access->permission('create')) {
			if($id > 0){
				$dataupdate = array(
					'jadwal_kegiatan_tanggal' => $jadwal,
					'jadwal_kegiatan_pegawai_id' => $this->session->userdata('ID'),
					'jadwal_kegiatan_pegawai_nip' => $this->session->userdata('NIP'),
					'jadwal_kegiatan_status' => 1,
					'jadwal_kegiatan_create_by' => $this->session->userdata('user_id'),
					'jadwal_kegiatan_create_date' => date('Y-m-d H:i:s')
				);
				$this->elogbook_model->update_jadwal($dataupdate, $id);

				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Perubahan jadwal kegiatan Berhasil',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'logbook/elogbook');
			}
			else{
				$datacreate = array(
					'jadwal_kegiatan_tanggal' => $jadwal,
					'jadwal_kegiatan_pegawai_id' => $this->session->userdata('ID'),
					'jadwal_kegiatan_pegawai_nip' => $this->session->userdata('NIP'),
					'jadwal_kegiatan_status' => 1,
					'jadwal_kegiatan_create_by' => $this->session->userdata('user_id'),
					'jadwal_kegiatan_create_date' => date('Y-m-d H:i:s')
				);

				$insDb = $this->elogbook_model->create_jadwal($datacreate);
				if($insDb > 0){
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Penambahan jadwal kegiatan Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'logbook/elogbook');
				}else{
					$notify = array(
						'title' 	=> 'Gagal!',
						'message'	=> 'Penambahan jadwal kegiatan Gagal, silahkan coba lagi',
						'status' 	=> 'error'
					);
					$this->session->set_flashdata('notify', $notify);
					redirect(base_url().'logbook/elogbook');
				}
			}

			
		}
	}

	function permintaanverifikasi($jadwal_kegiatan_id){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];

					$dataupdate = array(
						'jadwal_kegiatan_status' => 2
					);
					$this->elogbook_model->update_jadwal_kegiatan($dataupdate, $jadwal_kegiatan_id);

					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan Data Pendidikan Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'logbook/elogbook');
			}
			$pegawai_unit_kerja_id = $this->session->userdata('ID_UNKER');
			$parent_unit_kerja_id = $this->session->userdata('ID_PARENT_UNKER');
			// $atasan_parent_unit_kerja_id = $this->session->userdata('atasan_parent_unit_kerja_id');
			$data = array();
			//$data['pegawai']  	= $this->elogbook_model->getPegawai($pegawai_id)->row_array();
			$data['atasan_pegawai'] = $this->elogbook_model->getDataPegawaiByUnitKerja($parent_unit_kerja_id)->row_array();
			$data['atasan_pegawais'] = $this->elogbook_model->getDataPegawaiByUnitKerja($parent_unit_kerja_id)->result_array();
			// echo "<pre/>";
			// print_r($data['atasan_pegawais']);die;
			$data['history']  	= $this->elogbook_model->getkegiatanHarian($jadwal_kegiatan_id)->result_array();
			// echo "<pre/>";
			// print_r($data['history']);die;			
			$data["getSKP"] 	= $this->elogbook_model->getSKP()->result_array();			
			$this->template->display('logbook/elogbook/permintaanverifikasi', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	
	public function list_data_aktif(){
		$default_order = "jadwal_kegiatan_tanggal desc";
		$limit = 10;
		$pegawai_akses = $this->session->userdata('user_akses_id');
		$pegawai_id = $this->session->userdata('pegawai_id');
		if($pegawai_akses == 3){
			$where = "jadwal_kegiatan_pegawai_id =".$pegawai_id;
		}else{
			$where = "";
		}
		$field_name 	= array(
			'pemilik'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->elogbook_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->elogbook_model->get_count_all_data($search,$field_name, $where);

		$tanggal = '2018-10-09';
		$aaData = array();
		$getData 	= $this->elogbook_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$tombol = '';
            if($row["jadwal_kegiatan_status"]==3){
                $tombol = '<ul class="icons-list"></ul>';
            }else if($row["jadwal_kegiatan_status"]==2){
                $tombol = '<ul class="icons-list">
				<li><a href="'.base_url().'logbook/elogbook/kegiatan/'.urlencode($row["jadwal_kegiatan_id"]).'" class="create_data" data-popup="tooltip" title="kegiatan" data-placement="bottom"><i class="icon-plus-circle2" style="font-size: 13px;"></i></a></li>
				<li><a href="javascript:void(0)" onclick="ubah_jadwal(this)" data-tanggal="'.$row["jadwal_kegiatan_tanggal"].'" data-id="'.$row["jadwal_kegiatan_id"].'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>';
            }else{
                $tombol = '<ul class="icons-list">
				<li><a href="'.base_url().'logbook/elogbook/kegiatan/'.urlencode($row["jadwal_kegiatan_id"]).'" class="create_data" data-popup="tooltip" title="kegiatan" data-placement="bottom"><i class="icon-plus-circle2" style="font-size: 13px;"></i></a></li>
				<li><a href="javascript:void(0)" onclick="ubah_jadwal(this)" data-tanggal="'.$row["jadwal_kegiatan_tanggal"].'" data-id="'.$row["jadwal_kegiatan_id"].'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				<li><a href="'.base_url().'logbook/elogbook/permintaanverifikasi/'.urlencode($row["jadwal_kegiatan_id"]).'" class="lihat_data" data-popup="tooltip" title="Permintaan Verifikasi" data-placement="bottom"><i class="icon-enter3" style="font-size: 13px;"></i></a></li>				
				</ul>';
            }
			$aaData[] = array(
				$no,
				$row["pemilik"],
				dateEnToId($row["jadwal_kegiatan_tanggal"], 'd-m-Y'),
				($row["jumlah_menit"] != "")?$row["jumlah_menit"].' Menit':"-",
				$row["jumlah_kegiatan"],
				$row["jadwal_kegiatan_status_nama"],
				$tombol
				// '<ul class="icons-list">
				// <li><a href="'.base_url().'logbook/elogbook/kegiatan/'.urlencode($row["jadwal_kegiatan_id"]).'" class="create_data" data-popup="tooltip" title="kegiatan" data-placement="bottom"><i class="icon-plus-circle2" style="font-size: 13px;"></i></a></li>
				// <li><a href="javascript:void(0)" onclick="ubah_jadwal(this)" data-tanggal="'.$row["jadwal_kegiatan_tanggal"].'" data-id="'.$row["jadwal_kegiatan_id"].'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				// <li><a href="'.base_url().'logbook/elogbook/permintaanverifikasi/'.urlencode($row["jadwal_kegiatan_id"]).'" class="lihat_data" data-popup="tooltip" title="Permintaan Verifikasi" data-placement="bottom"><i class="icon-enter3" style="font-size: 13px;"></i></a></li>				
				// </ul>'
				// <li><a href="'.base_url().'logbook/elogbook/read/'.urlencode($row["pegawai_id"]).'" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>
				
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
	public function hapus($id = 0,$kegiatan_jadwal_kegiatan_id = 0){

		$kegiatan_idFilter = filter_var($id, FILTER_SANITIZE_NUMBER_INT);

		if($this->access->permission('delete')) {
			if($id==$kegiatan_idFilter) {

				$dataupdate = array(
					'kegiatan_status'  => 0,
					'kegiatan_create_by' 			=> $this->session->userdata('user_id'),
					'kegiatan_create_date' 			=> date('Y-m-d H:i:s')
				);
				//print_r($dataupdate);die;
				$del = $this->elogbook_model->update_kegiatan($dataupdate,$id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Pendidikan Formal Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'logbook/elogbook/kegiatan/'.$kegiatan_jadwal_kegiatan_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Pendidikan Formal Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'logbook/elogbook/kegiatan/'.$kegiatan_jadwal_kegiatan_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Pendidikan Formal Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'logbook/elogbook/kegiatan/'.$kegiatan_jadwal_kegiatan_id);
		}
	}

	public function get_unit_kerja_hirarki()
	{
		$unit_kerja_parent_id_kode 	= $this->input->post('unit_kerja_parent_id_kode');
		$unit_kerja_level 	= $this->input->post('unit_kerja_level');
		$data["unit_kerja"] = $this->elogbook_model->getUnitKerja($unit_kerja_parent_id_kode,$unit_kerja_level)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function getkegiatan($id){
		$data = $this->elogbook_model->getkegiatan($id)->row_array();
		return $this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
}
