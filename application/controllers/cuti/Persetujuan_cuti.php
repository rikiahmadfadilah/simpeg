<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Persetujuan_cuti extends CI_Controller{

    function __construct(){
        parent:: __construct();
        $this->load->model('cuti/persetujuan_cuti/persetujuan_cuti_model');
        $this->load->model('logbook/elogbook_model','',TRUE);
        //$this->load->library('access');
        $this->load->helper('xml');
        $this->load->helper('text');
    }

    public function index(){
        if($this->access->permission('read')){
            $data = array();
            $parent_unit_kerja_id = $this->session->userdata('unit_kerja_id_kode');
            $this->template->display('cuti/persetujuan_cuti/index', $data);
        }else{
            $this->access->redirect('404');
        }
        
    }
    public function update($cuti_detail_id = 0){
        if($this->access->permission('update')){
            // $a = $this->session->userdata('pegawai_nip');
            // echo "<pre/>";
            // print_r($a);die;
            if($post = $this->input->post()){
                $cuti_detail_id = $post['cuti_detail_id'];
                $dataupdate = array(
                    // 'cuti_detail_pegawai_nip'    => isset($post['cuti_detail_pegawai_nip'])?$post['cuti_detail_pegawai_nip']:'',
                    // 'cuti_detail_jenis_id'       => isset($post['cuti_detail_jenis_id'])?$post['cuti_detail_jenis_id']:'',
                    // 'cuti_detail_alasan'         => isset($post['cuti_detail_alasan'])?$post['cuti_detail_alasan']:'',
                    // 'cuti_detail_tanggal_mulai'  => isset($post['cuti_detail_tanggal_mulai'])?$post['cuti_detail_tanggal_mulai']:'',
                    // 'cuti_detail_tanggal_selesai'=> isset($post['cuti_detail_tanggal_selesai'])?$post['cuti_detail_tanggal_selesai']:'',
                    // 'cuti_detail_tahun'          => isset($post['cuti_detail_tahun'])?$post['cuti_detail_tahun']:'',
                    // 'cuti_detail_alamat'         => isset($post['cuti_detail_alamat'])?$post['cuti_detail_alamat']:'',
                    // 'cuti_detail_telp'           => isset($post['cuti_detail_telp'])?$post['cuti_detail_telp']:'',
                    'cuti_detail_atasan_nip'        => $this->session->userdata('pegawai_nip'),
                    'cuti_detail_status_atasan'     => isset($post['cuti_detail_status_atasan'])?$post['cuti_detail_status_atasan']:'',
                    'cuti_detail_keterangan_atasan' => isset($post['cuti_detail_keterangan_atasan'])?$post['cuti_detail_keterangan_atasan']:'',
                    // 'cuti_detail_pejabat_nip'       => isset($post['cuti_detail_pejabat_nip'])?$post['cuti_detail_pejabat_nip']:'',
                    // 'cuti_detail_status_pejabat'    => isset($post['cuti_detail_status_pejabat'])?$post['cuti_detail_status_pejabat']:'',
                    // 'cuti_detail_keterangan_pejabat'=> isset($post['cuti_detail_keterangan_pejabat'])?$post['cuti_detail_keterangan_pejabat']:'',
                    'cuti_detail_create_by'         => $this->session->userdata('user_id'),
                    'cuti_detail_creete_date'       => date('Y-m-d H:i:s'),
                    'cuti_detail_status'            => 1,
                    // 'cuti_detail_cuti_id'        =>$cuti_id
                );

                
                $insDb = $this->persetujuan_cuti_model->updateCutiDetail($dataupdate, $cuti_detail_id);

                if($insDb > 0){
                    $notify = array(
                        'title'     => 'Berhasil!',
                        'message'   => 'Perubahan Pengajuan Cuti Berhasil',
                        'status'    => 'success'
                    );
                    $this->session->set_flashdata('notify', $notify);

                    redirect(base_url().'cuti/persetujuan_cuti');
                }else{
                    $notify = array(
                        'title'     => 'Gagal!',
                        'message'   => 'Perubahan Pengajuan Cuti gagal, silahkan coba lagi',
                        'status'    => 'error'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    redirect(base_url().'cuti/persetujuan_cuti');
                }
            }

            $data = array();
            $data['pegawai']   = $this->persetujuan_cuti_model->getPegawai()->result_array();
            // $data['pegawai']   = $this->persetujuan_cuti_model->getListPegawai()->result_array();
            $data['jenis_cuti']   = $this->persetujuan_cuti_model->getJenisCuti()->result_array();
            $data['cuti'] = $this->persetujuan_cuti_model->getCutiDetail($cuti_detail_id)->row_array();
            $this->template->display('cuti/persetujuan_cuti/update', $data);
        }else{
            $this->access->redirect('404');
        }
    }

    
    public function read($cuti_detail_id=0){
        if($this->access->permission('read')){
            $data['cuti']       = $this->persetujuan_cuti_model->getCutiDetail($cuti_detail_id)->row_array();
            $this->template->display('cuti/persetujuan_cuti/read', $data);
        }else{
            $this->access->redirect('404');
        }
    }

    public function list_data_aktif(){
        $default_order = "cuti_detail_id DESC";
        $limit = 10;
        $where = "cuti_detail_status_atasan = 0";
        $parent_unit_kerja_id = $this->session->userdata('unit_kerja_id_kode');
        if($parent_unit_kerja_id)$where .= " and atasan_unit_kerja_id_kode =". $parent_unit_kerja_id;
        $field_name     = array(
            'pegawai_nama',
            'jenis_cuti_name'
        );
        $iSortingCols   = ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
        $ordertextarr = array();
        for ($i = 0;$i<$iSortingCols;$i++){
            $iSortCol   = ($this->input->get('iSortCol_'.$i));
            $sSortDir   = (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
            $ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
        }
        
        $ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
        $search     = (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
        $limit      = (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
        $start      = (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
        $data['sEcho'] = $this->input->get('sEcho');
        $data['iTotalRecords'][] = $this->persetujuan_cuti_model->get_count_all_data($search,$field_name, $where);
        $data['iTotalDisplayRecords'][] = $this->persetujuan_cuti_model->get_count_all_data($search,$field_name, $where);


        $aaData = array();
        $getData    = $this->persetujuan_cuti_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
        $no = (($start == 0) ? 1 : $start + 1);
        foreach ($getData as $row) {
            $tombol_aktif = '';
            // if($row["cuti_detail_status"]==1){
            //     $tombol_aktif = '<li><a onclick="set_non_aktif('.$row["cuti_detail_id"].')" class="lihat_data" data-popup="tooltip" title="Non Aktifkan" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>';
            // }else{
            //     $tombol_aktif = '<li><a onclick="set_aktif('.$row["cuti_detail_id"].')" class="lihat_data" data-popup="tooltip" title="Aktifkan" data-placement="bottom"><i class="icon-file-check" style="font-size: 13px;"></i></a></li>';
            // }
            $aaData[] = array(
                $row["pegawai_nama"],
                $row["jenis_cuti_name"],
                $row["cuti_detail_alasan"],
                dateEnToID($row["cuti_detail_tanggal_mulai"], 'd M Y'),
                (($row["cuti_detail_status_atasan"]==0)?'<span>'.$row["cuti_detail_status_atasan_name"].'</span>':'<span>'.$row["cuti_detail_status_atasan_name"].'</span>'),
                '<ul class="icons-list">
                <li><a href="'.base_url().'cuti/persetujuan_cuti/update/'.urlencode($row["cuti_detail_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
                <li><a href="'.base_url().'cuti/persetujuan_cuti/read/'.urlencode($row["cuti_detail_id"]).'" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>'.
                $tombol_aktif.'
                </ul>'
            );
            $no++;
        }
        $data['aaData'] = $aaData;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));

    }
}