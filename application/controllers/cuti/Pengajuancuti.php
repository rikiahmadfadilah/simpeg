<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pengajuancuti extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('auth/auth_model','',TRUE);
		$this->load->model('cuti/cuti_model','',TRUE);
	}

	public function index()
	{
		$data = array();
		$this->template->display('cuti/pengajuancuti/index', $data);	
	}
	public function create()
	{
		$data = array();
		$this->template->display('cuti/pengajuancuti/create', $data);	
	}
	
	public function list_data(){
		$default_order = "pegawai_esel_kini ASC, pegawai_kode_unit_kerja ASC";
		$limit = 10;
		$where = "(pegawai_kode_pensiun = '' OR pegawai_kode_pensiun IS NULL)";
		$field_name 	= array(
			'pegawai_nip_fix',
			'pegawai_nama',
			'pegawai_tempat_tanggal_lahir',
			'pegawai_nama_jabatan',
			'pegawai_nama_unit_kerja'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->pns_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->pns_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->pns_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip_fix"],
				$row["pegawai_nama"],
				$row["pegawai_tempat_tanggal_lahir"],
				$row["pegawai_nama_jabatan"],
				$row["pegawai_nama_unit_kerja"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'cuti/pengajuancuti/update/'.urlencode($row["pegawai_id"]).'" class="update_data"><i class="icon-pencil7"></i></a></li>
				<li><a href="#"><i class="icon-file-text"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
}
