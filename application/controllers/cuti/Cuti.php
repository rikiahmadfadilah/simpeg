<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cuti extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('auth/auth_model','',TRUE);
		$this->load->model('cuti/cuti/cuti_model','',TRUE);
	}
	public function tes(){
		$dataIdDasarSimpeg = 100046;
		$dataIdDasarEpegawai = 100046;
		$sameValues = array_intersect($dataIdDasarSimpeg,$dataIdDasarEpegawai);
		echo $sameValues;
	}
	public function index()
	{
		$data = array();
		$this->template->display('cuti/cuti/index', $data);	
	}
	public function create()
	{
		$data["cutipegawainip"] = $this->cuti_model->getCutiPegawai()->result_array();	
		$data["pegawai"] = $this->cuti_model->getPegawai()->result_array();
		$data["jeniscuti"] = $this->cuti_model->getJenisCuti()->result_array();
		$this->template->display('cuti/cuti/create', $data);
	}
	public function create_action()
	{
		//echo "<pre>"; print_r($this->input->post()); die;
		$cuti_detail_pegawai_nip = $this->input->post('cuti_detail_pegawai_nip');
		$cuti_detail_jenis_id = $this->input->post('cuti_detail_jenis_id');
		$cuti_detail_alasan = $this->input->post('cuti_detail_alasan');
		$cuti_detail_tanggal_mulai = $this->input->post('cuti_detail_tanggal_mulai_submit');
		$cuti_detail_tanggal_selesai = $this->input->post('cuti_detail_tanggal_selesai_submit');
		$cuti_detail_tahun = $this->input->post('cuti_detail_tahun');
		$cuti_detail_alamat = $this->input->post('cuti_detail_alamat');
		$cuti_detail_telp = $this->input->post('cuti_detail_telp');
		$cuti_detail_atasan_nip = $this->input->post('cuti_detail_atasan_nip');
		$cuti_detail_keterangan_atasan = $this->input->post('cuti_detail_keterangan_atasan');
		$cuti_detail_status_atasan = $this->input->post('cuti_detail_status_atasan');

		$data = array(
			'cuti_pegawai_nip' => $cuti_detail_pegawai_nip,
			'cuti_tahun' => $cuti_detail_tahun,
			);

		$cuti = $this->cuti_model->insertCuti($data,'dbo.trx_cuti');
 
		$data = array(
			'cuti_detail_pegawai_nip' => $cuti_detail_pegawai_nip,
			'cuti_detail_jenis_id' => $cuti_detail_jenis_id,
			'cuti_detail_alasan' => $cuti_detail_alasan,
			'cuti_detail_tanggal_mulai' => $cuti_detail_tanggal_mulai,
			'cuti_detail_tanggal_selesai' => $cuti_detail_tanggal_selesai,
			'cuti_detail_tahun' => $cuti_detail_tahun,
			'cuti_detail_alamat' => $cuti_detail_alamat,
			'cuti_detail_telp' => $cuti_detail_telp,
			'cuti_detail_atasan_nip' => $cuti_detail_atasan_nip,
			'cuti_detail_keterangan_atasan' => $cuti_detail_keterangan_atasan,
			'cuti_detail_status_atasan' => $cuti_detail_status_atasan,
			'cuti_detail_create_by'=>$this->session->userdata('user_id'),
			'cuti_detail_cuti_id'=>$cuti,
			'cuti_detail_status'=>1
			);

		$this->cuti_model->insertCutidetail($data,'dbo.trx_cuti_detail');

		redirect('cuti/cuti');
	}
	public function update($id)
	{
	$where = array('cuti_detail_id' => $id);
	$data['cuti'] = $this->cuti_model->getCuti($where,'dbo.trx_cuti_detail')->result();
	$data["jeniscuti"] = $this->cuti_model->getJenisCuti()->result_array();
	$data["pegawai"] = $this->cuti_model->getPegawai()->result_array();
	$data['getPegawai'] = $this->cuti_model->getCutidetail($where,'dbo.trx_cuti_detail')->row_array();
	$data['getJenisCuti'] = $this->cuti_model->getCutidetail($where,'dbo.trx_cuti_detail')->row_array();
	$data['getAtasan'] = $this->cuti_model->getCutidetail($where,'dbo.trx_cuti_detail')->row_array();
	$data['getStatusAtasan'] = $this->cuti_model->getCutidetail($where,'dbo.trx_cuti_detail')->row_array();
	$this->template->display('cuti/cuti/update', $data);
	}
	public function update_action()
	{
	$ids = $this->input->post('cuti_id');
	$idc = $this->input->post('cuti_detail_cuti_id');
	$id = $this->input->post('cuti_detail_id');
	$cuti_detail_pegawai_nip = $this->input->post('cuti_detail_pegawai_nip');
	$cuti_detail_jenis_id = $this->input->post('cuti_detail_jenis_id');
	$cuti_detail_alasan = $this->input->post('cuti_detail_alasan');
	$cuti_detail_tanggal_mulai = $this->input->post('cuti_detail_tanggal_mulai');
	$cuti_detail_tanggal_selesai = $this->input->post('cuti_detail_tanggal_selesai');
	$cuti_detail_tahun = $this->input->post('cuti_detail_tahun');
	$cuti_detail_alamat = $this->input->post('cuti_detail_alamat');
	$cuti_detail_telp = $this->input->post('cuti_detail_telp');
	$cuti_detail_atasan_nip = $this->input->post('cuti_detail_atasan_nip');
	$cuti_detail_keterangan_atasan = $this->input->post('cuti_detail_keterangan_atasan');
	$cuti_detail_status_atasan = $this->input->post('cuti_detail_status_atasan');
 
	// $data = array(
	// 		'cuti_pegawai_nip' => $cuti_detail_pegawai_nip,
	// 		'cuti_tahun' => $cuti_detail_tahun,
	// );
 
	// $where = array(
	// 		'cuti_id' => $ids
	// );
 
	// $cuti = $this->cuti_model->updateCuti($where,$data,'dbo.trx_cuti');

	$data = array(
			'cuti_detail_pegawai_nip' => $cuti_detail_pegawai_nip,
			'cuti_detail_jenis_id' => $cuti_detail_jenis_id,
			'cuti_detail_alasan' => $cuti_detail_alasan,
			'cuti_detail_tanggal_mulai' => $cuti_detail_tanggal_mulai,
			'cuti_detail_tanggal_selesai' => $cuti_detail_tanggal_selesai,
			'cuti_detail_tahun' => $cuti_detail_tahun,
			'cuti_detail_alamat' => $cuti_detail_alamat,
			'cuti_detail_telp' => $cuti_detail_telp,
			'cuti_detail_atasan_nip' => $cuti_detail_atasan_nip,
			'cuti_detail_keterangan_atasan' => $cuti_detail_keterangan_atasan,
			'cuti_detail_status_atasan' => $cuti_detail_status_atasan,	
			'cuti_detail_create_by'=>$this->session->userdata('user_id'),
			'cuti_detail_cuti_id'=>$idc,
			'cuti_detail_status'=>1
	);
	$where = array(
			'cuti_detail_id' => $id
	);

	$this->cuti_model->updateCutidetail($where,$data,'dbo.trx_cuti_detail');

	redirect('cuti/cuti');
	}
	public function detail($id)
	{
	$where = array('cuti_detail_id' => $id);
	$data['cuti'] = $this->cuti_model->detailCuti($where,'dbo.view_cuti')->result();
	$this->template->display('cuti/cuti/detail', $data);
	}
	public function delete($id){
		$type_id = urlencode($id);
		$status = $this->cuti_model->statusCuti($type_id)->row_array();
		if ($status['cuti_detail_status']==1) {
			$update = array(
				'cuti_detail_status' => 0
			);
			$update_cuti = $this->cuti_model->updateCutis($update, $type_id);
			if ($update_cuti) {
				$notify = array(
	                'title' 	=> 'Berhasil!',
	                'message' 	=> 'Pengajuan Cuti berhasil di non aktifkan',
	                'status' 	=> 'success'
	            );
	            $this->session->set_flashdata('notify', $notify);
				redirect(base_url().'cuti/cuti');
			}else{
	            $notify = array(
	                'title' 	=> 'Gagal!',
	                'message' 	=> 'Pengajuan Cuti gagal di non aktifkan, silahkan coba lagi',
	                'status' 	=> 'error'
	            );
	            $this->session->set_flashdata('notify', $notify);
	            redirect(base_url().'cuti/cuti');
	        }
		} else {
	        $update = array(
				'cuti_detail_status' => 1
			);
			$update_type = $this->cuti_model->updateCutis($update, $type_id);
			if ($update_type) {
				$notify = array(
	                'title' 	=> 'Berhasil!',
	                'message' 	=> 'Pengajuan Cuti berhasil di aktifkan',
	                'status' 	=> 'success'
	            );
	            $this->session->set_flashdata('notify', $notify);
				redirect(base_url().'cuti/cuti');
			}else{
	            $notify = array(
	                'title' 	=> 'Gagal!',
	                'message' 	=> 'Pengajuan Cuti gagal di aktifkan, silahkan coba lagi',
	                'status' 	=> 'error'
	            );
	            $this->session->set_flashdata('notify', $notify);
	            redirect(base_url().'cuti/cuti');
	        }
		}	
	}
	
	public function list_data(){
		$default_order = "pegawai_nama ASC, jenis_cuti_name ASC";
		$limit = 10;
		$where = "(cuti_detail_status = '1')";
		$field_name 	= array(
			'pegawai_nama',
			'jenis_cuti_name',
			'cuti_detail_alasan',
			'cuti_detail_tanggal_mulai',
			'cuti_detail_tanggal_selesai'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->cuti_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->cuti_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->cuti_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nama"],
				$row["jenis_cuti_name"],
				$row["cuti_detail_alasan"],
				$row["cuti_detail_tanggal_mulai"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'cuti/cuti/update/'.urlencode($row["cuti_detail_id"]).'" class="update_data"><i class="icon-pencil7"></i></a></li>
				<li><a href="'.base_url().'cuti/cuti/detail/'.urlencode($row["cuti_detail_id"]).'" class="detail_data"><i class="icon-file-text"></i></a></li>
				<li><a href="'.base_url().'cuti/cuti/delete/'.urlencode($row["cuti_detail_id"]).'" class="hapus"><i class="icon-warning"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
}
