<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengajuan_cuti extends CI_Controller{

    function __construct(){
        parent:: __construct();
        $this->load->model('cuti/pengajuan_cuti/pengajuan_cuti_model');
        // $this->load->library('access');
        $this->load->helper('xml');
        $this->load->helper('text');
    }

    public function index(){
        if($this->access->permission('read')){
            $data = array();
            $this->template->display('cuti/pengajuan_cuti/index', $data);
        }else{
            $this->access->redirect('404');
        }
        
    }
    public function get_atasan()
    {
        $post = $this->input->post();
        $atasan = $post['unit_parrent'];
        $atasan = $this->pengajuan_cuti_model->get_atasan($atasan)->result_array();
        if ($atasan > 0) {
            echo json_encode($atasan);
        }
    }

    public function create(){
        
        if($this->access->permission('create')){
            $operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');
            if($post = $this->input->post()){

                $cuti = array(
                    'cuti_pegawai_nip'    => isset($post['cuti_detail_pegawai_nip'])?$post['cuti_detail_pegawai_nip']:'',
                    'cuti_tahun'          => dateEnToID($post['cuti_detail_tanggal_mulai'], 'Y'),
                );
                // echo "<pre>"; print_r($cuti); die;
                $cuti_id = $this->pengajuan_cuti_model->addCuti($cuti);

                $pengajuan_cuti = array(
                    'cuti_detail_pegawai_nip'    => isset($post['cuti_detail_pegawai_nip'])?$post['cuti_detail_pegawai_nip']:'',
                    'cuti_detail_jenis_id'       => isset($post['cuti_detail_jenis_id'])?$post['cuti_detail_jenis_id']:'',
                    'cuti_detail_alasan'         => isset($post['cuti_detail_alasan'])?$post['cuti_detail_alasan']:'',
                    'cuti_detail_waktu'          => isset($post['cuti_detail_waktu'])?$post['cuti_detail_waktu']:'',
                    'cuti_detail_satuan_waktu'   => isset($post['cuti_detail_satuan_waktu'])?$post['cuti_detail_satuan_waktu']:'',
                    'cuti_detail_tanggal_mulai'  => isset($post['cuti_detail_tanggal_mulai'])?dateEnToID($post['cuti_detail_tanggal_mulai'], 'Y-m-d'):'',
                    'cuti_detail_tanggal_selesai'=> isset($post['cuti_detail_tanggal_selesai'])?dateEnToID($post['cuti_detail_tanggal_selesai'], 'Y-m-d'):'',
                    'cuti_detail_tahun'          => dateEnToID($post['cuti_detail_tanggal_mulai'], 'Y'),
                    'cuti_detail_alamat'         => isset($post['cuti_detail_alamat'])?$post['cuti_detail_alamat']:'',
                    'cuti_detail_telp'           => isset($post['cuti_detail_telp'])?$post['cuti_detail_telp']:'',
                    'cuti_detail_atasan_nip'     => isset($post['cuti_detail_atasan_nip'])?$post['cuti_detail_atasan_nip']:'',
                    'cuti_detail_create_by'      => $this->session->userdata('user_id'),
                    'cuti_detail_creete_date'    => date('Y-m-d H:i:s'),
                    'cuti_detail_status'         => 1,
                    'cuti_detail_cuti_id'        =>$cuti_id,
                    'cuti_detail_anak_ke'        => isset($post['cuti_detail_anak_ke'])?$post['cuti_detail_anak_ke']:'',
                );
                // echo "<pre>"; print_r($pengajuan_cuti); die;

                $cuti_detail_id = $this->pengajuan_cuti_model->addCutiDetail($pengajuan_cuti);
                if($cuti_detail_id > 0){
                    $notify = array(
                        'title'     => 'Berhasil!',
                        'message'   => 'Tambah Pengajuan Cuti Berhasil',
                        'status'    => 'success'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    
                    redirect(base_url().'cuti/pengajuan_cuti');
                }else{
                    $notify = array(
                        'title'     => 'Gagal!',
                        'message'   => 'Tambah Pengajuan Cuti gagal, silahkan coba lagi',
                        'status'    => 'error'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    
                    redirect(base_url().'cuti/pengajuan_cuti');
                }
            }
            
            $data = array();
            $data['pegawai']   = $this->pengajuan_cuti_model->getPegawai()->result_array();
            $data['jenis_cuti']   = $this->pengajuan_cuti_model->getJenisCuti()->result_array();
            $this->template->display('cuti/pengajuan_cuti/create', $data);
        } else {
            $this->access->redirect('404');
        }
    }
    public function update($cuti_detail_id = 0){
        if($this->access->permission('update')){

            if($post = $this->input->post()){
                $cuti_detail_id = $post['cuti_detail_id'];
                $dataupdate = array(
                    'cuti_detail_pegawai_nip'    => isset($post['cuti_detail_pegawai_nip'])?$post['cuti_detail_pegawai_nip']:'',
                    'cuti_detail_jenis_id'       => isset($post['cuti_detail_jenis_id'])?$post['cuti_detail_jenis_id']:'',
                    'cuti_detail_alasan'         => isset($post['cuti_detail_alasan'])?$post['cuti_detail_alasan']:'',
                    'cuti_detail_waktu'          => isset($post['cuti_detail_waktu'])?$post['cuti_detail_waktu']:'',
                    'cuti_detail_satuan_waktu'   => isset($post['cuti_detail_satuan_waktu'])?$post['cuti_detail_satuan_waktu']:'',
                    'cuti_detail_tanggal_mulai'  => isset($post['cuti_detail_tanggal_mulai'])?$post['cuti_detail_tanggal_mulai']:'',
                    'cuti_detail_tanggal_selesai'=> isset($post['cuti_detail_tanggal_selesai'])?$post['cuti_detail_tanggal_selesai']:'',
                    'cuti_detail_tahun'          => dateEnToID($post['cuti_detail_tanggal_mulai'], 'Y'),
                    'cuti_detail_alamat'         => isset($post['cuti_detail_alamat'])?$post['cuti_detail_alamat']:'',
                    'cuti_detail_telp'           => isset($post['cuti_detail_telp'])?$post['cuti_detail_telp']:'',
                    'cuti_detail_atasan_nip'     => isset($post['cuti_detail_atasan_nip'])?$post['cuti_detail_atasan_nip']:'',
                    'cuti_detail_create_by'      => $this->session->userdata('user_id'),
                    'cuti_detail_creete_date'    => date('Y-m-d H:i:s'),
                    'cuti_detail_status'         => 1,
                    // 'cuti_detail_cuti_id'        =>$cuti_id
                );

                
                $insDb = $this->pengajuan_cuti_model->updateCutiDetail($dataupdate, $cuti_detail_id);

                if($insDb > 0){
                    $notify = array(
                        'title'     => 'Berhasil!',
                        'message'   => 'Perubahan Pengajuan Cuti Berhasil',
                        'status'    => 'success'
                    );
                    $this->session->set_flashdata('notify', $notify);

                    redirect(base_url().'cuti/pengajuan_cuti');
                }else{
                    $notify = array(
                        'title'     => 'Gagal!',
                        'message'   => 'Perubahan Pengajuan Cuti gagal, silahkan coba lagi',
                        'status'    => 'error'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    redirect(base_url().'cuti/pengajuan_cuti');
                }
            }

            $data = array();
            $data['pegawai']   = $this->pengajuan_cuti_model->getPegawai()->result_array();
            $data['jenis_cuti']   = $this->pengajuan_cuti_model->getJenisCuti()->result_array();
            $data['cuti'] = $this->pengajuan_cuti_model->getCutiDetail($cuti_detail_id)->row_array();
            $this->template->display('cuti/pengajuan_cuti/update', $data);
        }else{
            $this->access->redirect('404');
        }
    }

    
    public function read($cuti_detail_id=0){
        if($this->access->permission('read')){
            $data['cuti']       = $this->pengajuan_cuti_model->getCutiDetail($cuti_detail_id)->row_array();
            $this->template->display('cuti/pengajuan_cuti/read', $data);
        }else{
            $this->access->redirect('404');
        }
    }

    public function list_data_aktif(){
        $default_order = "cuti_detail_id DESC";
        $limit = 10;
        $pegawai_akses = $this->session->userdata('user_akses_id');
        $pegawai_nip = $this->session->userdata('username');
        if($pegawai_akses == 3){
            $where = "cuti_detail_status = 1 and cuti_detail_pegawai_nip like '". $pegawai_nip."%'";
        }else{
            $where = "cuti_detail_status = 1";
        }
        $field_name     = array(
            'NIP',
            'NAMA',
            'jenis_cuti_name'
        );
        $iSortingCols   = ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
        $ordertextarr = array();
        for ($i = 0;$i<$iSortingCols;$i++){
            $iSortCol   = ($this->input->get('iSortCol_'.$i));
            $sSortDir   = (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
            $ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
        }
        
        $ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
        $search     = (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
        $limit      = (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
        $start      = (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
        $data['sEcho'] = $this->input->get('sEcho');
        $data['iTotalRecords'][] = $this->pengajuan_cuti_model->get_count_all_data($search,$field_name, $where);
        $data['iTotalDisplayRecords'][] = $this->pengajuan_cuti_model->get_count_all_data($search,$field_name, $where);


        $aaData = array();
        $getData    = $this->pengajuan_cuti_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
        $no = (($start == 0) ? 1 : $start + 1);
        foreach ($getData as $row) {
            $tombol_aktif = '';
            if($row["cuti_detail_status_atasan"]!=1){
                $tombol_aktif = '<li><a onclick="set_non_aktif('.$row["cuti_detail_id"].')" class="lihat_data" data-popup="tooltip" title="Non Aktifkan" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>
                                <li><a href="'.base_url().'cuti/pengajuan_cuti/update/'.urlencode($row["cuti_detail_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>';
            }
           
            $aaData[] = array(
                $row["NIP"],
                $row["NAMA"],
                $row["jenis_cuti_name"],
                $row["cuti_detail_alasan"],
                dateEnToID($row["cuti_detail_tanggal_mulai"], 'd-m-Y'),
                dateEnToID($row["cuti_detail_tanggal_selesai"], 'd-m-Y'),
                // (($row["cuti_detail_status_atasan"]==0)?'<span class"label label-sm label-success">'.$row["cuti_detail_status_atasan_name"].'</span>':'<span class"label label-sm label-success">'.$row["cuti_detail_status_atasan_name"].'</span>'),
                '<ul class="icons-list">
                <li><a href="'.base_url().'cuti/pengajuan_cuti/read/'.urlencode($row["cuti_detail_id"]).'" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>'.
                $tombol_aktif.'
                </ul>'
            );
            $no++;
        }
        $data['aaData'] = $aaData;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));

    }

    public function non_aktif($cuti_detail_id = 0){

        $cuti_detail_idFilter = filter_var($cuti_detail_id, FILTER_SANITIZE_NUMBER_INT);
        if($this->access->permission('delete')) {
            if($cuti_detail_id==$cuti_detail_idFilter) {

                $dataupdate = array(
                    'cuti_detail_status'               => 0,
                    'cuti_detail_create_by'            => $this->session->userdata('user_id'),
                    'cuti_detail_creete_date'          => date('Y-m-d H:i:s')
                );

                $del = $this->pengajuan_cuti_model->updateCutiDetail($dataupdate,$cuti_detail_id);
                $notify = array(
                    'title'     => 'Berhasil!',
                    'message'   => 'Pengajuan Cuti dinonaktifkan',
                    'status'    => 'success'
                );
                $this->session->set_flashdata('notify', $notify);

                redirect(base_url().'cuti/pengajuan_cuti');
            } else {
                $notify = array(
                    'title'     => 'Gagal!',
                    'message'   => 'Pengajuan Cuti gagal dinonaktifkan',
                    'status'    => 'error'
                );
                $this->session->set_flashdata('notify', $notify);
                redirect(base_url().'cuti/pengajuan_cuti');
            }
        } else {
            $notify = array(
                'title'     => 'Gagal!',
                'message'   => 'Pengajuan Cuti gagal dinonaktifkan',
                'status'    => 'error'
            );
            $this->session->set_flashdata('notify', $notify);
            redirect(base_url().'cuti/pengajuan_cuti');
        }
    }
    public function getDataListPegawai($page=0)
    {
        $unit_kerja_parent_id = $this->session->userdata('ID');
        $search = $this->input->get('q');
        $page = $this->input->get('page');
        $pegawai_id_selected = $this->input->get('pegawai_id_selected');
        $unit_kerja_id_selected = $this->input->get('unit_kerja_id_selected');
        $unit_kerja_parent_id_selected = $this->input->get('unit_kerja_parent_id_selected');
        
        $data = array();
        $data['total_count'] = count($this->pengajuan_cuti_model->getDataListPegawaiCount($unit_kerja_parent_id_selected,$search,$pegawai_id_selected)->result_array());
        $data['incomplete_results'] = false;
        $getDataListPegawai = $this->pengajuan_cuti_model->getDataListPegawai($unit_kerja_parent_id_selected,$search,$page,$pegawai_id_selected)->result_array();
        $data['items'] = array();
        $index = 0;
        foreach ($getDataListPegawai as $dlp) {
            $data['items'][$index]["id"] = $dlp["ID"];
            $data['items'][$index]["pegawai_id"] = $dlp["ID"];
            $data['items'][$index]["pegawai_nip"] = $dlp["NIP"];
            $data['items'][$index]["pegawai_nama"] = $dlp["NAMA"];
            $data['items'][$index]["pegawai_nama_jabatan"] = $dlp["NAMA_JAB"];
            $data['items'][$index]["unit_kerja_nama"] = $dlp["UNIT_KERJA"];
            $data['items'][$index]["golongan_nama"] = $dlp["GOLONGAN_NAMA"];
            $data['items'][$index]["golongan_pangkat"] = $dlp["PANGKAT"];
            $data['items'][$index]["pegawai_image_path"] = $dlp["PHOTO"];
            
            $index++;
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    public function getDataPegawai()
    {
        $pegawai_id = $this->input->post('pegawai_id');
        $data = array();
        $data['pegawai'] = $this->pengajuan_cuti_model->getDataPegawai($pegawai_id)->row_array();
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    // public function aktif($cuti_detail_id = 0){

    //     $cuti_detail_idFilter = filter_var($cuti_detail_id, FILTER_SANITIZE_NUMBER_INT);
    //     if($this->access->permission('update')) {
    //         if($cuti_detail_id==$cuti_detail_idFilter) {

    //             $dataupdate = array(
    //                 'cuti_detail_status'               => 1,
    //                 'pengajuan_cuti_create_by'            => $this->session->userdata('user_id'),
    //                 'pengajuan_cuti_create_date'          => date('Y-m-d H:i:s')
    //             );

    //             $del = $this->pengajuan_cuti_model->updatepengajuan_cuti($dataupdate,$cuti_detail_id);
    //             $notify = array(
    //                 'title'     => 'Berhasil!',
    //                 'message'   => 'pengajuan_cuti Berhasil diaktifkan',
    //                 'status'    => 'success'
    //             );
    //             $this->session->set_flashdata('notify', $notify);

    //             redirect(base_url().'pengajuan_cuti/pengajuan_cuti');
    //         } else {
    //             $notify = array(
    //                 'title'     => 'Gagal!',
    //                 'message'   => 'pengajuan_cuti Gagal diaktifkan',
    //                 'status'    => 'error'
    //             );
    //             $this->session->set_flashdata('notify', $notify);
    //             redirect(base_url().'pengajuan_cuti/pengajuan_cuti');
    //         }
    //     } else {
    //         $notify = array(
    //             'title'     => 'Gagal!',
    //             'message'   => 'pengajuan_cuti Gagal diaktifkan',
    //             'status'    => 'error'
    //         );
    //         $this->session->set_flashdata('notify', $notify);
    //         redirect(base_url().'pengajuan_cuti/pengajuan_cuti');
    //     }
    // }
}