<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rekap_cuti extends CI_Controller{

    function __construct(){
        parent:: __construct();
        $this->load->model('cuti/rekap_cuti/rekap_cuti_model');
        // $this->load->library('access');
        $this->load->helper('xml');
        $this->load->helper('text');
    }

    public function index(){
        if($this->access->permission('read')){
            $data = array();
            $this->template->display('cuti/rekap_cuti/index', $data);
        }else{
            $this->access->redirect('404');
        }
        
    }

    public function read($cuti_detail_id=0){
        if($this->access->permission('read')){
            $data['cuti']  = $this->rekap_cuti_model->getCutiDetail($cuti_detail_id)->row_array();
            $tgl_mulai = dateEnToID($data['cuti']['cuti_detail_tanggal_mulai'], 'd-m-Y');
            $tgl_selesai = dateEnToID($data['cuti']['cuti_detail_tanggal_selesai'], 'd-m-Y');
            $data['jumlah_cuti'] = hitungcuti($tgl_mulai, $tgl_selesai,"-");
            $h = $data['jumlah_cuti'];
            $thn = $h/365;
            $t = $h%365;
            $bln = $t/30;
            $a = $t%30;
            $mng = $a/7;
            $hr = $a%7;
            $data['thn'] = substr($thn, 0,1);
            $data['bln'] = substr($bln, 0,1);
            $data['mng'] = substr($mng, 0,1);
            $data['hr'] = $hr;
            // echo "<pre>"; print_r($data['thn']); echo "<pre>"; print_r($data['bln']); echo "<pre>"; print_r($data['mng']); echo "<pre>"; print_r($data['hr']);die;
            $this->template->display('cuti/rekap_cuti/read', $data);
        }else{
            $this->access->redirect('404');
        }
    }

    public function print($cuti_detail_id=0){
        if($this->access->permission('read')){
            $data['cuti'] = $this->rekap_cuti_model->getCutiDetail($cuti_detail_id)->row_array();
            $this->load->view('sources/cuti/rekap_cuti/print', $data);
        }else{
            $this->access->redirect('404');
        }
    }

    public function list_data(){
        $default_order = "cuti_detail_id DESC";
        $limit = 10;
        $pegawai_akses = $this->session->userdata('user_akses_id');
        $pegawai_nip = $this->session->userdata('NIP');
        if($pegawai_akses == 3){
            $where = "cuti_detail_status = 1 and cuti_detail_pegawai_nip like '". $pegawai_nip."%'";
        }else{
            $where = "cuti_detail_status = 1";
        }
        $field_name     = array(
            'NAMA',
            'jenis_cuti_name'
        );
        $iSortingCols   = ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
        $ordertextarr = array();
        for ($i = 0;$i<$iSortingCols;$i++){
            $iSortCol   = ($this->input->get('iSortCol_'.$i));
            $sSortDir   = (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
            $ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
        }
        
        $ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
        $search     = (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
        $limit      = (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
        $start      = (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
        $data['sEcho'] = $this->input->get('sEcho');
        $data['iTotalRecords'][] = $this->rekap_cuti_model->get_count_all_data($search,$field_name, $where);
        $data['iTotalDisplayRecords'][] = $this->rekap_cuti_model->get_count_all_data($search,$field_name, $where);


        $aaData = array();
        $getData    = $this->rekap_cuti_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
        $no = (($start == 0) ? 1 : $start + 1);
        foreach ($getData as $row) {
            $cetak = '<ul class="icons-list">
                      <li><a href="'.base_url().'cuti/rekap_cuti/read/'.urlencode($row["cuti_detail_id"]).'" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li></ul>';
            // if($row["cuti_detail_status_atasan"] == 1){
            //     $cetak = '<ul class="icons-list">
            //               <li><a href="'.base_url().'cuti/rekap_cuti/print/'.urlencode($row["cuti_detail_id"]).'" class="lihat_data" data-popup="tooltip" title="Cetak Form Cuti" data-placement="bottom" target="_blank"><i class="icon-printer2" style="font-size: 13px;"></i></a></li>
            //               <li><a href="'.base_url().'cuti/rekap_cuti/read/'.urlencode($row["cuti_detail_id"]).'" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li></ul>';
            // }else{
            //     $cetak = '<ul class="icons-list">
            //               <li><a href="'.base_url().'cuti/rekap_cuti/print/'.urlencode($row["cuti_detail_id"]).'" class="lihat_data" data-popup="tooltip" title="Cetak Form Cuti" data-placement="bottom" target="_blank"><i class="icon-printer2" style="font-size: 13px;"></i></a></li>
            //               <li><a href="'.base_url().'cuti/rekap_cuti/read/'.urlencode($row["cuti_detail_id"]).'" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li></ul>';
            // }
            $aaData[] = array(
                $row["NAMA"],
                $row["jenis_cuti_name"],
                $row["cuti_detail_alasan"],
                dateEnToID($row["cuti_detail_tanggal_mulai"], 'd M Y'),
                // (($row["cuti_detail_status_atasan"]==0)?'<span>'.$row["cuti_detail_status_atasan_name"].'</span>':'<span>'.$row["cuti_detail_status_atasan_name"].'</span>'),
                $cetak,
            );
            $no++;
        }
        $data['aaData'] = $aaData;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));

    }
}