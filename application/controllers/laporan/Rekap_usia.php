<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap_usia extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('laporan/rekap_usia_model','',TRUE);
	}

	public function index()
	{
		if($this->session->userdata('user_akses_id') == 3){
		}else{
			if($this->access->permission('read')){
				$data = array();
				$data ['rekap_usia'] = $this->rekap_usia_model->getRekapUsia()->result_array();
				// echo "<pre>"; print_r($data); die;
				$this->template->display('laporan/rekap_usia/index', $data);
			}else{
				$this->access->redirect('404');
			}
		}
	}

	public function preview(){
		$data = array();
		$data ['rekap_usia'] = $this->rekap_usia_model->getRekapUsia()->result_array();
		$this->load->view('sources/laporan/rekap_usia/preview', $data);
	}
}
