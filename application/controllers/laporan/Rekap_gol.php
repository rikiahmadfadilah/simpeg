<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap_gol extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('laporan/rekap_gol_model','',TRUE);
	}

	public function index()
	{
		if($this->session->userdata('user_akses_id') == 3){
		}else{
			if($this->access->permission('read')){
				$data = array();
				$data ['rekap_gol'] = $this->rekap_gol_model->getRekapGol()->result_array();
				// echo "<pre>"; print_r($data); die;
				$this->template->display('laporan/rekap_gol/index', $data);
			}else{
				$this->access->redirect('404');
			}
		}
	}

	public function preview(){
		$data = array();
		$data ['rekap_gol'] = $this->rekap_gol_model->getRekapGol()->result_array();
		$this->load->view('sources/laporan/rekap_gol/preview', $data);
	}
}
