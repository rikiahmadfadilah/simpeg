<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Daftar_naik_pangkat extends CI_Controller{

    function __construct(){
        parent:: __construct();
        //$this->load->library('access');
        $this->load->model('dt_dasar/dt_dasar_model','',TRUE);
        $this->load->model('laporan/daftar_naik_pangkat_model','',TRUE);
        $this->load->helper('xml');
        $this->load->helper('text');
    }

    public function index(){
        if($this->access->permission('read')){
            $data = array();
            $data['unker']     = $this->dt_dasar_model->getUnitKerja()->result_array();
            $data['eselon']   = $this->dt_dasar_model->getEselon()->result_array();
            $data["golongan"]     = $this->dt_dasar_model->getGolongan()->result_array();

            $this->template->display('laporan/daftar_naik_pangkat/index',$data);
        }else{
            $this->access->redirect('404');
        }
    }
    public function list_data(){
        $unker1      = (!$this->input->get('unker1'))?'':strtoupper($this->input->get('unker1'));
        $unker2      = (!$this->input->get('unker2'))?'':strtoupper($this->input->get('unker2'));
        $periode      = (!$this->input->get('periode'))?'':strtoupper($this->input->get('periode'));
        $tahun      = (!$this->input->get('tahun'))?'':strtoupper($this->input->get('tahun'));
        $eselon2      = (!$this->input->get('eselon2'))?'':strtoupper($this->input->get('eselon2'));
        $golongan1      = (!$this->input->get('golongan1'))?'':strtoupper($this->input->get('golongan1'));
        $golongan2      = (!$this->input->get('golongan2'))?'':strtoupper($this->input->get('golongan2'));

        $default_order = "GOLONGAN desc, TMT_GOL asc, TMT_JAB asc";
        $limit = 10;

        $where = "STATUS = 1 and STATUS_PEGAWAI = 1";
        if($unker1)$where .= " and KODE_UNKER >=". $unker1;
        if($unker2)$where .= " and KODE_UNKER <=". $unker2;
        if($periode == '04'){
            $where .= " and BULAN > 10";
            $where .= " and BULAN < 5";
        }else{
            $where .= " and BULAN > 4";
            $where .= " and BULAN < 11";
        }
        if($tahun)$where .= " and YEAR(TMT_GOL) = ".$tahun."";
        
        $field_name     = array(
            'NIP',
            'NAMA'
        );
        
        
        $iSortingCols   = ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
        $ordertextarr = array();
        for ($i = 0;$i<$iSortingCols;$i++){
            $iSortCol   = ($this->input->get('iSortCol_'.$i));
            $sSortDir   = (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
            // $ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
        }
        
        $ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
        $search     = (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
        $limit      = (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
        $start      = (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
        $data['sEcho'] = $this->input->get('sEcho');
        $data['iTotalRecords'][] = $this->daftar_naik_pangkat_model->get_count_all_data($search,$field_name, $where);
        $data['iTotalDisplayRecords'][] = $this->daftar_naik_pangkat_model->get_count_all_data($search,$field_name, $where);


        $aaData = array();
        $getData    = $this->daftar_naik_pangkat_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
        // echo "<pre>"; print_r($getData); die;
        $no = (($start == 0) ? 1 : $start + 1);
        foreach ($getData as $row) {
            $masa_kerja_golongan = 2;
            $aaData[] = array(
                $no,
                $row["NAMA"].'<br>'.$row["TMP_TGL_LAHIR"],
                $row["NIP"].'<br>'.$row["AGAMA_NAMA"],

                $row["GOLONGAN"],
                dateEnToId($row["TMT_GOL"], 'd-m-Y'),

                $row["NAMA_JAB"],
                dateEnToId($row["TMT_JAB"], 'd-m-Y'),

                $row["MASA_KERJA_THN"],
                $row["MASA_KERJA_BLN"],
                
                $row["NAMA_DIKLAT"],
                $row["THN_DIKLAT"],

                new_pangkat($row["GOLONGAN"]),
                dateEnToId($row["TMT_GOL"], 'd-m-').''.(dateEnToId($row["TMT_GOL"], 'Y') + 4),

                '<img src="'.base_url().'assets/images/pegawai/'.urlencode($row["PHOTO"]).'" width="100" height="100" align="center">',
            );
            $no++;
        }
        $data['aaData'] = $aaData;
        //print_r($data['aaData']);die;
        
        $this->output->set_content_type('application/json')->set_output(json_encode($data));

    }
    public function preview(){
        $data = array();
        $unker1      = (!$this->input->get('unker1'))?'':strtoupper($this->input->get('unker1'));
        $unker2      = (!$this->input->get('unker2'))?'':strtoupper($this->input->get('unker2'));
        $periode      = (!$this->input->get('periode'))?'':strtoupper($this->input->get('periode'));
        $tahun      = (!$this->input->get('tahun'))?'':strtoupper($this->input->get('tahun'));
        $eselon2      = (!$this->input->get('eselon2'))?'':strtoupper($this->input->get('eselon2'));
        $golongan1      = (!$this->input->get('golongan1'))?'':strtoupper($this->input->get('golongan1'));
        $golongan2      = (!$this->input->get('golongan2'))?'':strtoupper($this->input->get('golongan2'));

        $default_order = "GOLONGAN desc, TMT_GOL asc, TMT_JAB asc";

        $where = "STATUS = 1 and STATUS_PEGAWAI = 1";
        if($unker1)$where .= " and KODE_UNKER >=". $unker1;
        if($unker2)$where .= " and KODE_UNKER <=". $unker2;
        if($periode == '04'){
            $where .= " and BULAN > 10";
            $where .= " and BULAN < 5";
        }else{
            $where .= " and BULAN > 4";
            $where .= " and BULAN < 11";
        }
        if($tahun)$where .= " and YEAR(TMT_GOL) = ".($tahun-4)."";

        $data["preview"] = $this->daftar_naik_pangkat_model->preview($where,$default_order)->result_array();
        $data["periode"] = $periode;
        $data["tahun"] = $tahun;
        // echo "<pre>"; print_r($data["preview"]); die;

        $this->load->view('sources/laporan/daftar_naik_pangkat/preview', $data);
    }
}