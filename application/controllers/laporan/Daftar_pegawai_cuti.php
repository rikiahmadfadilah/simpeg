<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Daftar_pegawai_cuti extends CI_Controller{

    function __construct(){
        parent:: __construct();
        //$this->load->library('access');
        $this->load->model('dt_dasar/dt_dasar_model','',TRUE);
        $this->load->model('laporan/daftar_pegawai_cuti_model','',TRUE);
        $this->load->helper('xml');
        $this->load->helper('text');
    }

    public function index(){
        if($this->access->permission('read')){
            $data = array();
            $data['unker']     = $this->dt_dasar_model->getUnitKerja()->result_array();

            $this->template->display('laporan/daftar_pegawai_cuti/index',$data);
        }else{
            $this->access->redirect('404');
        }
    }
    public function list_data(){
        $unker1      = (!$this->input->get('unker1'))?'':strtoupper($this->input->get('unker1'));
        $unker2      = (!$this->input->get('unker2'))?'':strtoupper($this->input->get('unker2'));
        $nip_nama      = (!$this->input->get('nip_nama'))?'':strtoupper($this->input->get('nip_nama'));
        $tahun      = (!$this->input->get('tahun'))?'':strtoupper($this->input->get('tahun'));

        $default_order = "KODE_UNKER asc";
        $limit = 10;

        $where = "aktif = 1";
        if($unker1)$where .= " and KODE_UNKER >=". $unker1;
        if($unker2)$where .= " and KODE_UNKER <=". $unker2;
        if($nip_nama != 'UNDEFINED')$where .= " and (NIP LIKE '%".$nip_nama."%' OR NAMA LIKE '%".$nip_nama."%')";
        if($tahun)$where .= " and tahun_cuti = '".$tahun."'";
        
        $field_name     = array(
            'NAMA'
        );
        
        
        $iSortingCols   = ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
        $ordertextarr = array();
        for ($i = 0;$i<$iSortingCols;$i++){
            $iSortCol   = ($this->input->get('iSortCol_'.$i));
            $sSortDir   = (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
            // $ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
        }
        
        $ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
        $search     = (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
        $limit      = (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
        $start      = (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
        $data['sEcho'] = $this->input->get('sEcho');
        $data['iTotalRecords'][] = $this->daftar_pegawai_cuti_model->get_count_all_data($search,$field_name, $where);
        $data['iTotalDisplayRecords'][] = $this->daftar_pegawai_cuti_model->get_count_all_data($search,$field_name, $where);


        $aaData = array();
        $getData    = $this->daftar_pegawai_cuti_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
        // echo "<pre>"; print_r($getData); die;
        $no = (($start == 0) ? 1 : $start + 1);
        foreach ($getData as $row) {
            $masa_kerja_golongan = 2;
            $aaData[] = array(
                $no,
                $row["NIP"].'/'.$row["NAMA"].'/'.$row["GOLONGAN_NAMA"].' - '.$row["PANGKAT"],
                $row["NAMA_JAB"],
                $row["UNIT_KERJA"],
                $row["JENIS_CUTI"],
                dateEnToId($row["tgl_start"], 'd-m-Y').' s/d<br>'.dateEnToId($row["tgl_end"], 'd-m-Y'),
                $row["jml_hari_nama"].' '.$row['satuan_jml_nama'],
                $row["nama_kepala"],
                $row["nama_unker_kepala"],
            );
            $no++;
        }
        $data['aaData'] = $aaData;
        //print_r($data['aaData']);die;
        
        $this->output->set_content_type('application/json')->set_output(json_encode($data));

    }
    public function preview(){
        $data = array();
        $unker1      = (!$this->input->get('unker1'))?'':strtoupper($this->input->get('unker1'));
        $unker2      = (!$this->input->get('unker2'))?'':strtoupper($this->input->get('unker2'));
        $nip_nama      = (!$this->input->get('nip_nama'))?'':strtoupper($this->input->get('nip_nama'));
        $tahun      = (!$this->input->get('tahun'))?'':strtoupper($this->input->get('tahun'));
        $default_order = "KODE_UNKER asc";

        $where = "aktif = 1";
        if($unker1)$where .= " and KODE_UNKER >=". $unker1;
        if($unker2)$where .= " and KODE_UNKER <=". $unker2;
        if($nip_nama != 'UNDEFINED')$where .= " and (NIP LIKE '%".$nip_nama."%' OR NAMA LIKE '%".$nip_nama."%')";
        if($tahun)$where .= " and tahun_cuti = '".$tahun."'";

        $data["preview"] = $this->daftar_pegawai_cuti_model->preview($where,$default_order)->result_array();
        // echo "<pre>"; print_r($data["preview"]); die;

        $this->load->view('sources/laporan/daftar_pegawai_cuti/preview', $data);
    }
}