<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Daftar_penerima_slks extends CI_Controller{

    function __construct(){
        parent:: __construct();
        //$this->load->library('access');
        $this->load->model('dt_dasar/dt_dasar_model','',TRUE);
        $this->load->model('laporan/daftar_penerima_slks_model','',TRUE);
        $this->load->helper('xml');
        $this->load->helper('text');
    }

    public function index(){
        if($this->access->permission('read')){
            $data = array();
            $data['unker']     = $this->dt_dasar_model->getUnitKerja()->result_array();
            $data['eselon']   = $this->dt_dasar_model->getEselon()->result_array();
            $data["golongan"]     = $this->dt_dasar_model->getGolongan()->result_array();
            // echo "<pre>"; print_r($data['unker']); die;
            $this->template->display('laporan/daftar_penerima_slks/index',$data);
        }else{
            $this->access->redirect('404');
        }
    }
    public function list_data(){
        $usia_pensiun = 58;
        $data_filter  = explode('-',$this->uri->segment(4));
        $data_unit    = explode('_',$data_filter[0]);
        $unker1  = $data_unit[0];
        
        $masa_kerja_keseluruhan = explode('_',$data_filter[1]);     
        $masa_kerja_1           = $masa_kerja_keseluruhan[0];
        $masa_kerja_2           = $masa_kerja_keseluruhan[1];

        // $unker1      = (!$this->input->get('unker1'))?'':strtoupper($this->input->get('unker1'));
        // $masa_kerja_1 = (!$this->input->get('masa_kerja_1'))?'':strtoupper($this->input->get('masa_kerja_1'));
        // $masa_kerja_2 = (!$this->input->get('masa_kerja_2'))?'':strtoupper($this->input->get('masa_kerja_2'));

        $default_order = "KODE_UNKER asc, ESELON asc";
        $limit = 10;

        $where = "STATUS = 1";
        if($unker1)$where .= " and KODE_UNKER >=". $unker1;
        if($unker1)$where .= " and KODE_UNKER <=". $unker1;
        if($masa_kerja_1)$where .= " and MASA_KERJA_KESELURUHAN >=". $masa_kerja_1;
        if($masa_kerja_2)$where .= " and MASA_KERJA_KESELURUHAN <=". $masa_kerja_2;
        
        $field_name     = array(
            'NAMA'
        );
        
        
        $iSortingCols   = ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
        $ordertextarr = array();
        for ($i = 0;$i<$iSortingCols;$i++){
            $iSortCol   = ($this->input->get('iSortCol_'.$i));
            $sSortDir   = (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
            // $ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
        }
        
        $ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
        $search     = (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
        $limit      = (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
        $start      = (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
        $data['sEcho'] = $this->input->get('sEcho');
        $data['iTotalRecords'][] = $this->daftar_penerima_slks_model->get_count_all_data($search,$field_name, $where);
        $data['iTotalDisplayRecords'][] = $this->daftar_penerima_slks_model->get_count_all_data($search,$field_name, $where);


        $aaData = array();
        $getData    = $this->daftar_penerima_slks_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
        // echo "<pre>"; print_r($getData); die;
        $no = (($start == 0) ? 1 : $start + 1);
        foreach ($getData as $row) {
            // $data_penghargaan = $this->daftar_penerima_slks_model->get_penghargaan($row['NIP'])->result_array();
            $masa_kerja_golongan = 2;
            $aaData[] = array(
                $no,
                $row["NAMA"].'<br>'.$row["TMP_TGL_LAHIR"],
                $row["NIP"],
                
                $row["GOLONGAN_NAMA"].'<br>'.dateEnToId($row["TMT_GOL"], 'd-m-Y'),
                
                $row["PANGKAT"].'<br>'.dateEnToId($row["TMT_GOL"], 'd-m-Y'),
                
                $row["NAMA_JAB"].'<br>'.$row["NAMA_JABFUNG"],
                $row["TMT_JAB"].'<br>'.$row["TMT_JABFUNG"],
                
                $row["MASA_KERJA_KESELURUHAN_NAME"],
                // foreach ($data_penghargaan as $dp) {
                //     $dp["NAMA_TANDA"].'<br>'.$dp['THN_OLEH']
                // }
                ''
            );
            $no++;
        }
        $data['aaData'] = $aaData;
        //print_r($data['aaData']);die;
        
        $this->output->set_content_type('application/json')->set_output(json_encode($data));

    }
    public function preview(){
        $usia_pensiun = 58;
        $data_filter  = explode('-',$this->uri->segment(4));
        $data_unit    = explode('_',$data_filter[0]);
        $unker1  = $data_unit[0];
        
        $masa_kerja_keseluruhan = explode('_',$data_filter[1]);     
        $masa_kerja_1           = $masa_kerja_keseluruhan[0];
        $masa_kerja_2           = $masa_kerja_keseluruhan[1];
        
        $where = "STATUS = 1";
        if($unker1)$where .= " and KODE_UNKER >=". $unker1;
        if($unker1)$where .= " and KODE_UNKER <=". $unker1;
        if($masa_kerja_1)$where .= " and MASA_KERJA_KESELURUHAN >=". $masa_kerja_1;
        if($masa_kerja_2)$where .= " and MASA_KERJA_KESELURUHAN <=". $masa_kerja_2;

        $data["preview"] = $this->daftar_penerima_slks_model->preview($where)->result_array();
        // echo "<pre>"; print_r($data["preview"]); die;

        $this->load->view('sources/laporan/daftar_penerima_slks/preview', $data);
    }
}