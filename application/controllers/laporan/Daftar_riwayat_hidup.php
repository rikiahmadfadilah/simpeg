<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar_riwayat_hidup extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('dt_dasar/dt_dasar_model','',TRUE);
		$this->load->model('riwayat/riwayat_model','',TRUE);
	}

	public function index()
	{
		if($this->session->userdata('user_akses_id') == 3){
			redirect('laporan/daftar_riwayat_hidup/read/'.$this->session->userdata('ID'));
		}else{
			if($this->access->permission('read')){
				$data = array();
				$this->template->display('laporan/daftar_riwayat_hidup/index', $data);
			}else{
				$this->access->redirect('404');
			}
		}
		// $data = array();
		// $this->template->display('skp/kinerjapegawai/index', $data);	
	}

	public function list_data(){
        $default_order = "KODE_UNKER asc, ESELON asc";
		$limit = 10;
		$where = "STATUS = 1";
		$field_name 	= array(
			'NIP',
			'NAMA'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->dt_dasar_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->dt_dasar_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->dt_dasar_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["NIP"],
				$row["NAMA"],
				$row["TMP_LAHIR"],
				$row["NAMA_JAB"],
				$row["UNIT_KERJA"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'laporan/daftar_riwayat_hidup/read/'.urlencode($row["ID"]).'" target="_blank" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}

	public function read($id){
		$data["pegawai"] = $this->dt_dasar_model->getDetailPegawai($id)->row_array();
		$data['dt_pendidikan'] = $this->dt_dasar_model->getDataPendidikan($data['pegawai']['NIP'])->result_array();
		$data['dt_kepangkatan'] = $this->dt_dasar_model->getDataKepangkatan($data['pegawai']['NIP'])->result_array();
		$data['dt_jabatan'] = $this->dt_dasar_model->getDataJabatan($data['pegawai']['NIP'])->result_array();
		$data['dt_diklat'] = $this->dt_dasar_model->getDataDiklat($data['pegawai']['NIP'])->result_array();
		$data['dt_skp'] = $this->dt_dasar_model->getDataSKP($data['pegawai']['NIP'])->result_array();
		$data['dt_penghargaan'] = $this->dt_dasar_model->getDataPenghargaan($data['pegawai']['NIP'])->result_array();
		$data['dt_kgb'] = $this->dt_dasar_model->getDataKgb($data['pegawai']['NIP'])->result_array();
		$data['dt_cuti'] = $this->dt_dasar_model->getDataCuti($id)->result_array();
		$data['dt_keluarga'] = $this->dt_dasar_model->getDataKeluarga($data['pegawai']['NIP'])->result_array();
		$data['dt_assessment'] = $this->dt_dasar_model->getDataAssessment($data['pegawai']['NIP'])->result_array();
		$data['dt_hukdis'] = $this->dt_dasar_model->getDataHukdis($data['pegawai']['NIP'])->result_array();
		$data['dt_assessment'] = $this->dt_dasar_model->getDataAssessment($data['pegawai']['NIP'])->result_array();
		// echo "<pre>"; print_r($data['dt_assessment']); die;
		$this->load->view('sources/laporan/daftar_riwayat_hidup/read', $data);
	}
}
