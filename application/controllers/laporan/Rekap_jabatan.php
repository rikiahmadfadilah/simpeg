<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap_jabatan extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('laporan/rekap_jab_model','',TRUE);
	}

	public function index()
	{
		// if($this->session->userdata('user_akses_id') == 3){
		// }else{
			// if($this->access->permission('read')){
				$data = array();
				$data ['rekap_jab'] = $this->rekap_jab_model->getRekapJab()->result_array();
				// echo "<pre>"; print_r($data); die;
				$this->template->display('laporan/rekap_jab/index', $data);
			// }else{
				// $this->access->redirect('404');
			// }
		// }
	}

	public function preview(){
		$data = array();
		$data ['rekap_jab'] = $this->rekap_jab_model->getRekapJab()->result_array();
		$this->load->view('sources/laporan/rekap_jab/preview', $data);
	}
}
