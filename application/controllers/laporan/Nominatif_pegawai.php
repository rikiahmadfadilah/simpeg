<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nominatif_pegawai extends CI_Controller{

    function __construct(){
        parent:: __construct();
        //$this->load->library('access');
        $this->load->model('dt_dasar/dt_dasar_model','',TRUE);
        $this->load->model('laporan/nominatif_pegawai_model','',TRUE);
        $this->load->helper('xml');
        $this->load->helper('text');
    }

    public function index(){
        if($this->access->permission('read')){
            $data = array();
            $data['unker']     = $this->dt_dasar_model->getUnitKerja()->result_array();
            $data['eselon']   = $this->dt_dasar_model->getEselon()->result_array();
            $data["golongan"]     = $this->dt_dasar_model->getGolongan()->result_array();

            $this->template->display('laporan/nominatif_pegawai/index',$data);
        }else{
            $this->access->redirect('404');
        }
    }
    public function list_data(){
        $unker1      = (!$this->input->get('unker1'))?'':strtoupper($this->input->get('unker1'));
        $unker2      = (!$this->input->get('unker2'))?'':strtoupper($this->input->get('unker2'));
        $eselon1      = (!$this->input->get('eselon1'))?'':strtoupper($this->input->get('eselon1'));
        $eselon2      = (!$this->input->get('eselon2'))?'':strtoupper($this->input->get('eselon2'));
        $golongan1      = (!$this->input->get('golongan1'))?'':strtoupper($this->input->get('golongan1'));
        $golongan2      = (!$this->input->get('golongan2'))?'':strtoupper($this->input->get('golongan2'));

        $default_order = "KODE_UNKER asc, ESELON asc";
        $limit = 10;

        $where = "STATUS = 1";
        if($unker1)$where .= " and KODE_UNKER >=". $unker1;
        if($unker2)$where .= " and KODE_UNKER <=". $unker2;
        if($eselon1)$where .= " and ESELON >= '".$eselon1."'";
        if($eselon2)$where .= " and ESELON <= '".$eselon2."'";
        if($golongan1)$where .= " and GOLONGAN >= '".$golongan1."'";
        if($golongan2)$where .= " and GOLONGAN <= '".$golongan2."'";
        
        $field_name     = array(
            'pegawai_nama'
        );
        
        
        $iSortingCols   = ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
        $ordertextarr = array();
        for ($i = 0;$i<$iSortingCols;$i++){
            $iSortCol   = ($this->input->get('iSortCol_'.$i));
            $sSortDir   = (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
            // $ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
        }
        
        $ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
        $search     = (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
        $limit      = (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
        $start      = (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
        $data['sEcho'] = $this->input->get('sEcho');
        $data['iTotalRecords'][] = $this->nominatif_pegawai_model->get_count_all_data($search,$field_name, $where);
        $data['iTotalDisplayRecords'][] = $this->nominatif_pegawai_model->get_count_all_data($search,$field_name, $where);


        $aaData = array();
        $getData    = $this->nominatif_pegawai_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
        // echo "<pre>"; print_r($getData); die;
        $no = (($start == 0) ? 1 : $start + 1);
        foreach ($getData as $row) {
            $masa_kerja_golongan = 2;
            $aaData[] = array(
                $no,
                $row["NAMA"].'<br>'.$row["NIP"].'<br>'.$row["TMP_TGL_LAHIR"].
                '<br>'.$row["NO_KARPEG"].'<br>'.$row["NO_KARIS"],
                $row["JENIS_KELAMIN"].'<br>'.$row["STATUS_KAWIN"].'<br>'.$row["AGAMA_NAMA"] ,
                
                $row["GOLONGAN_NAMA"].'<br>'.dateEnToId($row["TMT_GOL"], 'd-m-Y').'<br>'.$masa_kerja_golongan.
                '<br>'.$row["GOLONGAN_NAMA"],
                
                $row["NAMA_JAB"].'<br>'.dateEnToId($row["TMT_JAB"], 'd-m-Y').'<br>'.'2'.
                '<br>'.$row["NAMA_JABFUNG"].'<br>'.$row["TMT_JABFUNG"],
                
                $row["NAMA_PENDIDIKAN"].'/'.$row["THN_LULUS"].'<br>'.$row["NAMA_SEK_PT"].
                '<br>'.$row["PRO_STUDI"],
                
                $row["KATEGORI_DIKLAT_TEXT"].'<br>'.$row["NAMA_DIKLAT"].'<br>'.dateEnToId($row["TGL_DIKLAT"], 'Y').'<br>'.''.
                ' / '.'',
                dateEnToId($row["TMT_CPNS"], 'd-m-Y').'<br>'.''.' <br>'.$row["STAT_KEPEG"],
                '<img src="'.base_url().'assets/images/pegawai/'.urlencode($row["PHOTO"]).'" width="100" height="100" align="center">',
            );
            $no++;
        }
        $data['aaData'] = $aaData;
        //print_r($data['aaData']);die;
        
        $this->output->set_content_type('application/json')->set_output(json_encode($data));

    }
    public function preview(){
        $data = array();
        $unker1      = (!$this->input->get('unker1'))?'':strtoupper($this->input->get('unker1'));
        $unker2      = (!$this->input->get('unker2'))?'':strtoupper($this->input->get('unker2'));
        $eselon1      = (!$this->input->get('eselon1'))?'':strtoupper($this->input->get('eselon1'));
        $eselon2      = (!$this->input->get('eselon2'))?'':strtoupper($this->input->get('eselon2'));
        $golongan1      = (!$this->input->get('golongan1'))?'':strtoupper($this->input->get('golongan1'));
        $golongan2      = (!$this->input->get('golongan2'))?'':strtoupper($this->input->get('golongan2'));
        // $data['pegawai'] = $this->drh_tanpa_ttd_model->getDetailPegawai($pegawai_id)->row_array();
        $default_order = "KODE_UNKER asc, ESELON asc";

        $where = "STATUS = 1";
        if($unker1)$where .= " and KODE_UNKER >=". $unker1;
        if($unker2)$where .= " and KODE_UNKER <=". $unker2;
        if($eselon1)$where .= " and ESELON >= '".$eselon1."'";
        if($eselon2)$where .= " and ESELON <= '".$eselon2."'";
        if($golongan1)$where .= " and GOLONGAN >= '".$golongan1."'";
        if($golongan2)$where .= " and GOLONGAN <= '".$golongan2."'";

        $data["preview"] = $this->nominatif_pegawai_model->preview($where,$default_order)->result_array();
        // echo "<pre>"; print_r($data["preview"]); die;

        $this->load->view('sources/laporan/nominatif_pegawai/preview', $data);
    }
}