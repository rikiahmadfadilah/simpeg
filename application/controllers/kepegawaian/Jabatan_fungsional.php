<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan_fungsional extends CI_Controller {

	public function __construct(){
		parent::__construct();
		// $this->load->model('auth/auth_model','',TRUE);
		// $this->load->model('kepegawaian/pns/pns_model','',TRUE);
		$this->load->model('kepegawaian/jabatan_fungsional_model','',TRUE);
		//$this->load->model('kepegawaian/jabatan_fungsional/pns_model','',TRUE);

		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index()
	{
		$data = array();
		$this->template->display('kepegawaian/jabatan_fungsional/index', $data);	
	}

	public function update($pegawai_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];

				//print_r($post['jenjang_jabatan_unit_kerja_id']);die;
				$jenjang_jabatan_fungsional_id = $post['jenjang_jabatan_fungsional_id'];
				$jenjang_jabatan_fungsional_pegawai_id = $post['jenjang_jabatan_fungsional_pegawai_id'];
				$jenjang_jabatan_fungsional_pegawai_nip = $post['jenjang_jabatan_fungsional_pegawai_nip'];
				//$jenjang_jabatan_fungsional_jabatan_id = $post['jenjang_jabatan_fungsional_jabatan_id'];
				$jenjang_jabatan_fungsional_unit_kerja = $post['jenjang_jabatan_fungsional_unit_kerja'];
				$jenjang_jabatan_fungsional_fungsional_id = $post['jenjang_jabatan_fungsional_fungsional_id'];
				$jenjang_jabatan_fungsional_tmt_jab_fung = $post['jenjang_jabatan_fungsional_tmt_jab_fung'];
				$jenjang_jabatan_fungsional_nomor_sk = $post['jenjang_jabatan_fungsional_nomor_sk'];
				$jenjang_jabatan_fungsional_tanggal_sk = $post['jenjang_jabatan_fungsional_tanggal_sk'];
				$jenjang_jabatan_fungsional_angka_kredit = $post['jenjang_jabatan_fungsional_angka_kredit'];
				// $jenjang_jabatan_fungsional_create_date = $post['jenjang_jabatan_fungsional_create_date'];
				// $jenjang_jabatan_fungsional_create_by = $post['jenjang_jabatan_fungsional_create_by'];
				$jenjang_jabatan_fungsional_status = 1;


				if($isUpdate == 0){
					$datacreate = array(
						'jenjang_jabatan_fungsional_pegawai_id' => $jenjang_jabatan_fungsional_pegawai_id,
						'jenjang_jabatan_fungsional_pegawai_nip' => $jenjang_jabatan_fungsional_pegawai_nip,
						//'jenjang_jabatan_fungsional_jabatan_id' => $jenjang_jabatan_fungsional_jabatan_id,
						'jenjang_jabatan_fungsional_unit_kerja' => $jenjang_jabatan_fungsional_unit_kerja,
						'jenjang_jabatan_fungsional_fungsional_id' => $jenjang_jabatan_fungsional_fungsional_id,
						'jenjang_jabatan_fungsional_tmt_jab_fung' => $jenjang_jabatan_fungsional_tmt_jab_fung,
						'jenjang_jabatan_fungsional_nomor_sk' => $jenjang_jabatan_fungsional_nomor_sk,
						'jenjang_jabatan_fungsional_tanggal_sk' => $jenjang_jabatan_fungsional_tanggal_sk,
						'jenjang_jabatan_fungsional_angka_kredit' => $jenjang_jabatan_fungsional_angka_kredit,
						'jenjang_jabatan_fungsional_create_date' => date('Y-m-d H:i:s'),
						'jenjang_jabatan_fungsional_create_by' => $this->session->userdata('user_id'),
						'jenjang_jabatan_fungsional_status' => $jenjang_jabatan_fungsional_status
					);					
					
					$insDb = $this->jabatan_fungsional_model->create_jenjang_jabatan_fungsional($datacreate);

					if($insDb > 0){
						$notify = array(
							'title' 	=> 'Berhasil!',
							'message' 	=> 'Penambahan Data Pendidikan Berhasil',
							'status' 	=> 'success'
						);
						$this->session->set_flashdata('notify', $notify);

						redirect(base_url().'kepegawaian/jabatan_fungsional/update/'.$jenjang_jabatan_fungsional_pegawai_id);
					}else{
						$notify = array(
							'title' 	=> 'Gagal!',
							'message'	=> 'Penambahan Data Pendidikan Gagal, silahkan coba lagi',
							'status' 	=> 'error'
						);
						$this->session->set_flashdata('notify', $notify);
						redirect(base_url().'kepegawaian/jabatan_fungsional/update/'.$jenjang_jabatan_fungsional_pegawai_id);
					}
				}else{
					$dataupdate = array(
						'jenjang_jabatan_fungsional_pegawai_id' => $jenjang_jabatan_fungsional_pegawai_id,
						'jenjang_jabatan_fungsional_pegawai_nip' => $jenjang_jabatan_fungsional_pegawai_nip,
						//'jenjang_jabatan_fungsional_jabatan_id' => $jenjang_jabatan_fungsional_jabatan_id,
						'jenjang_jabatan_fungsional_unit_kerja' => $jenjang_jabatan_fungsional_unit_kerja,
						'jenjang_jabatan_fungsional_fungsional_id' => $jenjang_jabatan_fungsional_fungsional_id,
						'jenjang_jabatan_fungsional_tmt_jab_fung' => $jenjang_jabatan_fungsional_tmt_jab_fung,
						'jenjang_jabatan_fungsional_nomor_sk' => $jenjang_jabatan_fungsional_nomor_sk,
						'jenjang_jabatan_fungsional_tanggal_sk' => $jenjang_jabatan_fungsional_tanggal_sk,
						'jenjang_jabatan_fungsional_angka_kredit' => $jenjang_jabatan_fungsional_angka_kredit,
						'jenjang_jabatan_fungsional_create_date' => date('Y-m-d H:i:s'),
						'jenjang_jabatan_fungsional_create_by' => $this->session->userdata('user_id'),
						'jenjang_jabatan_fungsional_status' => $jenjang_jabatan_fungsional_status
					);
					$this->jabatan_fungsional_model->update_jenjang_jabatan_fungsional($dataupdate, $jenjang_jabatan_fungsional_id);

					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan Data Pendidikan Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/jabatan_fungsional/update/'.$jenjang_jabatan_fungsional_pegawai_id);
				}
			}

			$data = array();
			$data['pegawai']  	= $this->jabatan_fungsional_model->getPegawai($pegawai_id)->row_array();
			$data['history']  	= $this->jabatan_fungsional_model->getJenjangJabatan($pegawai_id)->result_array();
			$data['golongan']  	= $this->jabatan_fungsional_model->getGolongan()->result_array();
			$data["unit_kerja"] = $this->jabatan_fungsional_model->getUnitKerja(1,2)->result_array();
			$data["jabatan"] 	= $this->jabatan_fungsional_model->getJabatan()->result_array();
			$data["jabatanfungsional"] = $this->jabatan_fungsional_model->getjabatanfungsional()->result_array();
			$data["jabatanfungsionalchild"] = $this->jabatan_fungsional_model->getjabatanfungsionalLevel()->result_array();
			
			$this->template->display('kepegawaian/jabatan_fungsional/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	
	public function list_data_aktif(){
		$default_order = "";
		$limit = 10;
		$where = "";
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama',
			'pegawai_nama_jabatan',
			'unit_kerja_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->jabatan_fungsional_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->jabatan_fungsional_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->jabatan_fungsional_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_nama_jabatan"],
				$row["unit_kerja_nama"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/jabatan_fungsional/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
	public function hapus($jenjang_jabatan_fungsional_id = 0,$pegawai_id = 0){

		$jenjang_jabatan_fungsional_idFilter = filter_var($jenjang_jabatan_fungsional_id, FILTER_SANITIZE_NUMBER_INT);

		if($this->access->permission('delete')) {
			if($jenjang_jabatan_fungsional_id==$jenjang_jabatan_fungsional_idFilter) {

				$dataupdate = array(
					'jenjang_jabatan_fungsional_status'  => 0,
					'jenjang_jabatan_fungsional_create_by' 			=> $this->session->userdata('user_id'),
					'jenjang_jabatan_fungsional_create_date' 			=> date('Y-m-d H:i:s')
				);
				//print_r($dataupdate);die;
				$del = $this->jabatan_fungsional_model->update_jenjang_jabatan_fungsional($dataupdate,$jenjang_jabatan_fungsional_id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Pendidikan Formal Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'kepegawaian/jabatan_fungsional/update/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Pendidikan Formal Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'kepegawaian/jabatan_fungsional/update/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Pendidikan Formal Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'kepegawaian/jabatan_fungsional/update/'.$pegawai_id);
		}
	}

	public function get_unit_kerja_hirarki()
	{
		$unit_kerja_parent_id_kode 	= $this->input->post('unit_kerja_parent_id_kode');
		$unit_kerja_level 	= $this->input->post('unit_kerja_level');
		$data["unit_kerja"] = $this->jabatan_fungsional_model->getUnitKerja($unit_kerja_parent_id_kode,$unit_kerja_level)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
}
