<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unit_kerja extends CI_Controller {

	public function __construct(){
		parent::__construct();
		// $this->load->model('auth/auth_model','',TRUE);
		$this->load->model('kepegawaian/pns/pns_model','',TRUE);
		$this->load->model('kepegawaian/unit_kerja_model','',TRUE);
		//$this->load->model('kepegawaian/unit_kerja/pns_model','',TRUE);

		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index()
	{
		$data = array();
		$this->template->display('kepegawaian/unit_kerja/index', $data);	
	}

	public function update($pegawai_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];

				//print_r($post['unit_kerja_pegawai_unit_kerja_id']);die;
				$unit_kerja_pegawai_unit_kerja_id = $post['unit_kerja_pegawai_unit_kerja_id'];
				$unit_kerja_id = '';
				for ($i=0; $i < count($unit_kerja_pegawai_unit_kerja_id); $i++) { 
					if($unit_kerja_pegawai_unit_kerja_id[$i] != ''){
						$unit_kerja_id = $unit_kerja_pegawai_unit_kerja_id[$i];
					}
				}
				$unit_kerja_pegawai_id = $post['unit_kerja_pegawai_id'];
				$unit_kerja_pegawai_pegawai_id = $post['unit_kerja_pegawai_pegawai_id'];
				$unit_kerja_pegawai_pegawai_nip = $post['unit_kerja_pegawai_pegawai_nip'];
				$unit_kerja_pegawai_jabatan_id = $post['unit_kerja_pegawai_jabatan_id'];
				// $unit_kerja_pegawai_jabatan_nama = $post['unit_kerja_pegawai_jabatan_nama'];
				$unit_kerja_pegawai_tanggal_mulai = $post['unit_kerja_pegawai_tanggal_mulai'];
				$unit_kerja_pegawai_tanggal_selesai = $post['unit_kerja_pegawai_tanggal_selesai'];
				$unit_kerja_pegawai_nomor_sk = $post['unit_kerja_pegawai_nomor_sk'];
				$unit_kerja_pegawai_tanggal_sk = $post['unit_kerja_pegawai_tanggal_sk'];
				$unit_kerja_pegawai_status = 1;

				if($isUpdate == 0){
					$datacreate = array(
						'unit_kerja_pegawai_pegawai_id' => $unit_kerja_pegawai_pegawai_id, 
						'unit_kerja_pegawai_pegawai_nip' => $unit_kerja_pegawai_pegawai_nip, 
						'unit_kerja_pegawai_jabatan_id' => $unit_kerja_pegawai_jabatan_id, 
						// 'unit_kerja_pegawai_jabatan_nama' => $unit_kerja_pegawai_jabatan_nama, 
						'unit_kerja_pegawai_unit_kerja_id' => $unit_kerja_id, 
						'unit_kerja_pegawai_tanggal_mulai' => $unit_kerja_pegawai_tanggal_mulai, 
						'unit_kerja_pegawai_tanggal_selesai' => $unit_kerja_pegawai_tanggal_selesai, 
						'unit_kerja_pegawai_nomor_sk' => $unit_kerja_pegawai_nomor_sk, 
						'unit_kerja_pegawai_tanggal_sk' => $unit_kerja_pegawai_tanggal_sk, 
						'unit_kerja_pegawai_create_date' => date('Y-m-d H:i:s'), 
						'unit_kerja_pegawai_create_by' => $this->session->userdata('user_id'), 
						'unit_kerja_pegawai_status' => $unit_kerja_pegawai_status
					);					
					
					$insDb = $this->unit_kerja_model->create_unit_kerja_pegawai($datacreate);

					if($insDb > 0){
						$unit_kerja_last = $this->unit_kerja_model->getUnit_kerjaLast($pegawai_id)->row_array();
						$update_pegawai = array(
							'pegawai_unit_kerja_id' => $unit_kerja_last['unit_kerja_pegawai_unit_kerja_id'],
							'pegawai_jabatan_id' => $unit_kerja_last['unit_kerja_pegawai_jabatan_id'],
							'pegawai_tanggal_tmt_jabatan' => $unit_kerja_last['unit_kerja_pegawai_tanggal_mulai']
						);
						$this->pns_model->updatepegawai($update_pegawai, $pegawai_id);
						$notify = array(
							'title' 	=> 'Berhasil!',
							'message' 	=> 'Penambahan Data Pendidikan Berhasil',
							'status' 	=> 'success'
						);
						$this->session->set_flashdata('notify', $notify);

						redirect(base_url().'kepegawaian/unit_kerja/update/'.$pegawai_id);
					}else{
						$notify = array(
							'title' 	=> 'Gagal!',
							'message'	=> 'Penambahan Data Pendidikan Gagal, silahkan coba lagi',
							'status' 	=> 'error'
						);
						$this->session->set_flashdata('notify', $notify);
						redirect(base_url().'kepegawaian/unit_kerja/update/'.$pegawai_id);
					}
				}else{
					$dataupdate = array(
						// 'unit_kerja_pegawai_pegawai_id' => $unit_kerja_pegawai_pegawai_id, 
						// 'unit_kerja_pegawai_pegawai_nip' => $unit_kerja_pegawai_pegawai_nip, 
						'unit_kerja_pegawai_jabatan_id' => $unit_kerja_pegawai_jabatan_id, 
						// 'unit_kerja_pegawai_jabatan_nama' => $unit_kerja_pegawai_jabatan_nama, 
						'unit_kerja_pegawai_unit_kerja_id' => $unit_kerja_id, 
						'unit_kerja_pegawai_tanggal_mulai' => $unit_kerja_pegawai_tanggal_mulai, 
						'unit_kerja_pegawai_tanggal_selesai' => $unit_kerja_pegawai_tanggal_selesai, 
						'unit_kerja_pegawai_nomor_sk' => $unit_kerja_pegawai_nomor_sk, 
						'unit_kerja_pegawai_tanggal_sk' => $unit_kerja_pegawai_tanggal_sk, 
						'unit_kerja_pegawai_create_date' => date('Y-m-d H:i:s'), 
						'unit_kerja_pegawai_create_by' => $this->session->userdata('user_id'), 
						'unit_kerja_pegawai_status' => $unit_kerja_pegawai_status
					);
					$this->unit_kerja_model->update_unit_kerja_pegawai($dataupdate, $unit_kerja_pegawai_id);
					$unit_kerja_last = $this->unit_kerja_model->getUnit_kerjaLast($pegawai_id)->row_array();
					$update_pegawai = array(
						'pegawai_unit_kerja_id' => $unit_kerja_last['unit_kerja_pegawai_unit_kerja_id'],
						'pegawai_jabatan_id' => $unit_kerja_last['unit_kerja_pegawai_jabatan_id'],
						'pegawai_tanggal_tmt_jabatan' => $unit_kerja_last['unit_kerja_pegawai_tanggal_mulai']
					);
					$this->pns_model->updatepegawai($update_pegawai, $pegawai_id);
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan Data Pendidikan Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/unit_kerja/update/'.$pegawai_id);
				}
			}

			$data = array();
			$data['pegawai']  	= $this->unit_kerja_model->getPegawai($pegawai_id)->row_array();
			$data['history']  	= $this->unit_kerja_model->getUnitKerjaPegawai($pegawai_id)->result_array();
			$data['golongan']  	= $this->unit_kerja_model->getGolongan()->result_array();
			$data["unit_kerja"] = $this->unit_kerja_model->getUnitKerja(1,2)->result_array();
			$data["jabatan"] 	= $this->unit_kerja_model->getJabatan()->result_array();
			$this->template->display('kepegawaian/unit_kerja/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	
	public function list_data_aktif(){
		$default_order = "";
		$limit = 10;
		$operator_unit_kerja = $this->session->userdata('user_operator_unit_kerja_id');
		$operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');

		if($this->session->userdata('user_akses_id') == '2'){
        	$where = "pegawai_status = 1 and pegawai_is_pns = 1 and (pegawai_jenis_pensiun_id is null or pegawai_jenis_pensiun_id = 0) and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
		}
		else{
			$where = "pegawai_status = 1 AND pegawai_is_pns = 1";
		}
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->unit_kerja_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->unit_kerja_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->unit_kerja_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_nama_jabatan"],
				$row["unit_kerja_hirarki_name_full"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/unit_kerja/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
	public function hapus($unit_kerja_pegawai_id = 0,$pegawai_id = 0){

		$unit_kerja_pegawai_idFilter = filter_var($unit_kerja_pegawai_id, FILTER_SANITIZE_NUMBER_INT);

		if($this->access->permission('delete')) {
			if($unit_kerja_pegawai_id==$unit_kerja_pegawai_idFilter) {

				$dataupdate = array(
					'unit_kerja_pegawai_status'  => 0,
					'unit_kerja_pegawai_create_by' 			=> $this->session->userdata('user_id'),
					'unit_kerja_pegawai_create_date' 			=> date('Y-m-d H:i:s')
				);
				//print_r($dataupdate);die;
				//
				$del = $this->unit_kerja_model->update_unit_kerja_pegawai($dataupdate,$unit_kerja_pegawai_id);
				$unit_kerja_last = $this->unit_kerja_model->getUnit_kerjaLast($pegawai_id)->row_array();
				$update_pegawai = array(
					'pegawai_unit_kerja_id' => $unit_kerja_last['unit_kerja_pegawai_unit_kerja_id'],
					'pegawai_jabatan_id' => $unit_kerja_last['unit_kerja_pegawai_jabatan_id'],
					'pegawai_tanggal_tmt_jabatan' => $unit_kerja_last['unit_kerja_pegawai_tanggal_mulai']
				);
				$this->pns_model->updatepegawai($update_pegawai, $pegawai_id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Pendidikan Formal Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'kepegawaian/unit_kerja/update/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Pendidikan Formal Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'kepegawaian/unit_kerja/update/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Pendidikan Formal Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'kepegawaian/unit_kerja/update/'.$pegawai_id);
		}
	}

	public function get_unit_kerja_hirarki()
	{
		$unit_kerja_parent_id_kode 	= $this->input->post('unit_kerja_parent_id_kode');
		$unit_kerja_level 	= $this->input->post('unit_kerja_level');
		$data["unit_kerja"] = $this->unit_kerja_model->getUnitKerja($unit_kerja_parent_id_kode,$unit_kerja_level)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
}
