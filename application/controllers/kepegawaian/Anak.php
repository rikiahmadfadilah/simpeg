<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anak extends CI_Controller {

	public function __construct(){
		parent::__construct();
		// $this->load->model('auth/auth_model','',TRUE);
		// $this->load->model('kepegawaian/pns/pns_model','',TRUE);
		$this->load->model('kepegawaian/anak_model','',TRUE);
		//$this->load->model('kepegawaian/anak/pns_model','',TRUE);

		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index()
	{
		if($this->session->userdata('user_akses_id') == 3){
			redirect('kepegawaian/anak/update/'.$this->session->userdata('pegawai_id'));
		}else{
			if($this->access->permission('read')){
				$data = array();
				$this->template->display('kepegawaian/anak/index', $data);
			}else{
				$this->access->redirect('404');
			}
		}
		// $data = array();
		// $this->template->display('kepegawaian/anak/index', $data);	
	}

	public function update($pegawai_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];

				$anak_id = $post['anak_id'];

				$anak_pegawai_id = $post['anak_pegawai_id'];
				$anak_pegawai_nip = $post['anak_pegawai_nip'];
				$anak_pegawai_nama = $post['anak_pegawai_nama'];
				$anak_jenis_kelamin_id = $post['anak_jenis_kelamin_id'];
				$anak_tempat_lahir = $post['anak_tempat_lahir'];
				$anak_tanggal_lahir = $post['anak_tanggal_lahir'];
				// $anak_usia_tahun = $post['anak_usia_tahun'];
				// $anak_usia_bulan = $post['anak_usia_bulan'];
				$anak_jenis_anak = $post['anak_jenis_anak'];
				$anak_pekerjaan = $post['anak_pekerjaan'];
				// $anak_pendidikan_id = $post['anak_pendidikan_id'];
				$anak_masuk_kp4 = $post['anak_masuk_kp4'];
				$anak_kondisi = $post['anak_kondisi'];

				if($isUpdate == 0){
					$datacreate = array(
						'anak_pegawai_id' => $anak_pegawai_id,
						'anak_pegawai_nip' => $anak_pegawai_nip,
						'anak_pegawai_nama' => $anak_pegawai_nama,
						'anak_jenis_kelamin_id' => $anak_jenis_kelamin_id,
						'anak_tempat_lahir' => $anak_tempat_lahir,
						'anak_tanggal_lahir' => $anak_tanggal_lahir,
						// 'anak_usia_tahun' => $anak_usia_tahun,
						// 'anak_usia_bulan' => $anak_usia_bulan,
						'anak_jenis_anak' => $anak_jenis_anak,
						'anak_pekerjaan' => $anak_pekerjaan,
						// 'anak_pendidikan_id' => $anak_pendidikan_id,
						'anak_masuk_kp4' => $anak_masuk_kp4,
						'anak_kondisi' => $anak_kondisi,
						'anak_create_by' => $this->session->userdata('user_id'),
						'anak_create_date' => date('Y-m-d H:i:s'),
						'anak_status' => 1,
						'anak_sinkronisasi' => 0
					);									
					
					$insDb = $this->anak_model->create_anak($datacreate);

					if($insDb > 0){
						$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["anak_pegawai_nip"])."','2','1B')");
						$notify = array(
							'title' 	=> 'Berhasil!',
							'message' 	=> 'Penambahan Data Pendidikan Berhasil',
							'status' 	=> 'success'
						);
						$this->session->set_flashdata('notify', $notify);

						redirect(base_url().'kepegawaian/anak/update/'.$anak_pegawai_id);
					}else{
						$notify = array(
							'title' 	=> 'Gagal!',
							'message'	=> 'Penambahan Data Pendidikan Gagal, silahkan coba lagi',
							'status' 	=> 'error'
						);
						$this->session->set_flashdata('notify', $notify);
						redirect(base_url().'kepegawaian/anak/update/'.$anak_pegawai_id);
					}
				}else{
					$dataupdate = array(
						'anak_pegawai_id' => $anak_pegawai_id,
						'anak_pegawai_nip' => $anak_pegawai_nip,
						'anak_pegawai_nama' => $anak_pegawai_nama,
						'anak_jenis_kelamin_id' => $anak_jenis_kelamin_id,
						'anak_tempat_lahir' => $anak_tempat_lahir,
						'anak_usia_tahun' => $anak_usia_tahun,
						'anak_usia_bulan' => $anak_usia_bulan,
						'anak_tanggal_lahir' => $anak_tanggal_lahir,
						'anak_jenis_anak' => $anak_jenis_anak,
						'anak_pekerjaan' => $anak_pekerjaan,
						// 'anak_pendidikan_id' => $anak_pendidikan_id,
						'anak_masuk_kp4' => $anak_masuk_kp4,
						'anak_kondisi' => $anak_kondisi,
						'anak_create_by' => $this->session->userdata('user_id'),
						'anak_create_date' => date('Y-m-d H:i:s'),
						'anak_status' => 1,
						'anak_sinkronisasi' => 0
					);
					$this->anak_model->update_anak($dataupdate, $anak_id);
					$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["anak_pegawai_nip"])."','23','2B')");
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan Data Pendidikan Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/anak/update/'.$anak_pegawai_id);
				}
			}

			$data = array();
			$data['pegawai']  	= $this->anak_model->getPegawai($pegawai_id)->row_array();
			$data['history']  	= $this->anak_model->getAnak($pegawai_id)->result_array();
			$data['listjk']  	= $this->anak_model->GetJenisKelamin()->result_array();
			$data['pendidikan'] = $this->anak_model->getPendidikan()->result_array();
			$this->template->display('kepegawaian/anak/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	
	public function list_data_aktif(){
		$default_order = "";
		$limit = 10;
		$operator_unit_kerja = $this->session->userdata('user_operator_unit_kerja_id');
		$operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');

		if($this->session->userdata('user_akses_id') == '2'){
        	$where = "pegawai_status = 1 and pegawai_is_pns = 1 and (pegawai_jenis_pensiun_id is null or pegawai_jenis_pensiun_id = 0) and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
		}
		else{
			$where = "pegawai_status = 1 AND pegawai_is_pns = 1";
		}
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->anak_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->anak_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->anak_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_nama_jabatan"],
				$row["unit_kerja_hirarki_name_full"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/anak/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
	public function hapus($anak_id = 0,$pegawai_id = 0){

		$anak_idFilter = filter_var($anak_id, FILTER_SANITIZE_NUMBER_INT);

		if($this->access->permission('delete')) {
			if($anak_id==$anak_idFilter) {

				$dataupdate = array(
					'anak_status'  => 0,
					'anak_create_by' 			=> $this->session->userdata('user_id'),
					'anak_create_date' 			=> date('Y-m-d H:i:s')
				);
				//print_r($dataupdate);die;
				$del = $this->anak_model->update_anak($dataupdate,$anak_id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Pendidikan Formal Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'kepegawaian/anak/update/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Pendidikan Formal Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'kepegawaian/anak/update/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Pendidikan Formal Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'kepegawaian/anak/update/'.$pegawai_id);
		}
	}
}
