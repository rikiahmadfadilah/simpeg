<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penghargaan extends CI_Controller{

	function __construct(){
		parent:: __construct();
		$this->load->model('kepegawaian/penghargaan_model');
		//$this->load->library('access');
		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index(){
		if($this->access->permission('read')){
			$data = array();
			$this->template->display('kepegawaian/penghargaan/index', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function update($pegawai_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];
				$penghargaan_id = $post['penghargaan_id'];
				$penghargaan_nama = $post['penghargaan_nama'];
				$penghargaan_nomor_sk = $post['penghargaan_nomor_sk'];
				$penghargaan_tanggal_sk = $post['penghargaan_tanggal_sk'];
				$penghargaan_tahun = $post['penghargaan_tahun'];
				$penghargaan_pemberi = $post['penghargaan_pemberi'];
				$penghargaan_jabatan = $post['penghargaan_jabatan'];
				$penghargaan_pegawai_id = $post['penghargaan_pegawai_id'];
				$penghargaan_pegawai_nip = $post['penghargaan_pegawai_nip'];
				$penghargaan_jenis = $post['penghargaan_jenis'];

				if($isUpdate == 0){
					$datacreate = array(
						'penghargaan_nama' 				=> $penghargaan_nama,
						'penghargaan_nomor_sk' 			=> $penghargaan_nomor_sk,
						'penghargaan_tanggal_sk' 		=> $penghargaan_tanggal_sk,
						'penghargaan_tahun' 			=> $penghargaan_tahun,
						'penghargaan_pemberi' 			=> $penghargaan_pemberi,
						'penghargaan_jabatan' 			=> $penghargaan_jabatan,
						'penghargaan_status' 			=> 1,
						'penghargaan_create_by' 		=> $this->session->userdata('user_id'),
						'penghargaan_create_date' 		=> date('Y-m-d H:i:s'),
						'penghargaan_pegawai_id' 		=> $penghargaan_pegawai_id,
						'penghargaan_pegawai_nip' 		=> $penghargaan_pegawai_nip,
						'penghargaan_jenis' 			=> $penghargaan_jenis
					);

					$insDb = $this->penghargaan_model->create_penghargaan($datacreate);

					if($insDb > 0){
						$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["penghargaan_pegawai_nip"])."','5','1E')");
						$notify = array(
							'title' 	=> 'Berhasil!',
							'message' 	=> 'Penambahan Data Penghargaan Berhasil',
							'status' 	=> 'success'
						);
						$this->session->set_flashdata('notify', $notify);

						redirect(base_url().'kepegawaian/penghargaan/update/'.$penghargaan_pegawai_id);
					}else{
						$notify = array(
							'title' 	=> 'Gagal!',
							'message'	=> 'Penambahan Data Penghargaan Gagal, silahkan coba lagi',
							'status' 	=> 'error'
						);
						$this->session->set_flashdata('notify', $notify);
						redirect(base_url().'kepegawaian/penghargaan/update/'.$penghargaan_pegawai_id);
					}
				}else{
					$dataupdate = array(
						'penghargaan_nama' 				=> $penghargaan_nama,
						'penghargaan_nomor_sk' 			=> $penghargaan_nomor_sk,
						'penghargaan_tanggal_sk' 		=> $penghargaan_tanggal_sk,
						'penghargaan_tahun' 			=> $penghargaan_tahun,
						'penghargaan_pemberi' 			=> $penghargaan_pemberi,
						'penghargaan_jabatan' 			=> $penghargaan_jabatan,
						'penghargaan_status' 			=> 1,
						'penghargaan_create_by' 		=> $this->session->userdata('user_id'),
						'penghargaan_create_date' 		=> date('Y-m-d H:i:s'),
						'penghargaan_jenis' 			=> $penghargaan_jenis
					);
					$this->penghargaan_model->update_penghargaan($dataupdate, $penghargaan_id);
					$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["penghargaan_pegawai_nip"])."','26','2E')");
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan Data Penghargaan Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/penghargaan/update/'.$penghargaan_pegawai_id);
				}
			}

			$data = array();
			$data['pegawai']  	= $this->penghargaan_model->getPegawai($pegawai_id)->row_array();
			$data['history']  	= $this->penghargaan_model->getListPenghargaan($pegawai_id)->result_array();
			$this->template->display('kepegawaian/penghargaan/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function list_data_aktif(){
		$default_order = "";
		$limit = 10;
		$operator_unit_kerja = $this->session->userdata('user_operator_unit_kerja_id');
		$operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');

		if($this->session->userdata('user_akses_id') == '2'){
        	$where = "pegawai_status = 1 and pegawai_is_pns = 1 and (pegawai_jenis_pensiun_id is null or pegawai_jenis_pensiun_id = 0) and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
		}
		else{
			$where = "pegawai_status = 1 AND pegawai_is_pns = 1";
		}
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->penghargaan_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->penghargaan_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->penghargaan_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_nama_jabatan"],
				$row["unit_kerja_hirarki_name_full"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/penghargaan/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
	public function hapus($penghargaan_id = 0,$pegawai_id = 0){

		$penghargaan_idFilter = filter_var($penghargaan_id, FILTER_SANITIZE_NUMBER_INT);
		if($this->access->permission('delete')) {
			if($penghargaan_id==$penghargaan_idFilter) {

				$dataupdate = array(
					'penghargaan_status'  => 0,
					'penghargaan_create_by' 			=> $this->session->userdata('user_id'),
					'penghargaan_create_date' 			=> date('Y-m-d H:i:s')
				);

				$del = $this->penghargaan_model->update_penghargaan($dataupdate,$penghargaan_id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Penghargaan Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'kepegawaian/penghargaan/update/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Penghargaan Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'kepegawaian/penghargaan/update/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Penghargaan Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'kepegawaian/penghargaan/update/'.$pegawai_id);
		}
	}

	
}