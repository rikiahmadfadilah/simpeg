<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Organisasi extends CI_Controller{

	function __construct(){
		parent:: __construct();
		$this->load->model('kepegawaian/organisasi_model');
		//$this->load->library('access');
		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index(){
		if($this->access->permission('read')){
			$data = array();
			$this->template->display('kepegawaian/organisasi/index', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function update($pegawai_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];
				$organisasi_id = $post['organisasi_id'];
				$organisasi_nama = $post['organisasi_nama'];
				$organisasi_kedudukan = $post['organisasi_kedudukan'];
				$organisasi_tanggal_mulai = $post['organisasi_tanggal_mulai'];
				$organisasi_tanggal_selesai = $post['organisasi_tanggal_selesai'];
				$organisasi_nomor_sk = $post['organisasi_nomor_sk'];
				$organisasi_nama_jabatan = $post['organisasi_nama_jabatan'];
				$organisasi_tahun = $post['organisasi_tahun'];
				$organisasi_pegawai_id = $post['organisasi_pegawai_id'];
				$organisasi_pegawai_nip = $post['organisasi_pegawai_nip'];
				
				if($isUpdate == 0){
					$datacreate = array(
						'organisasi_nama' 				=> $organisasi_nama,
						'organisasi_kedudukan' 			=> $organisasi_kedudukan,
						'organisasi_tanggal_mulai' 		=> $organisasi_tanggal_mulai,
						'organisasi_tanggal_selesai' 	=> $organisasi_tanggal_selesai,
						'organisasi_nomor_sk' 			=> $organisasi_nomor_sk,
						'organisasi_nama_jabatan' 		=> $organisasi_nama_jabatan,
						'organisasi_tahun' 				=> $organisasi_tahun,
						'organisasi_pegawai_id' 		=> $organisasi_pegawai_id,
						'organisasi_pegawai_nip' 		=> $organisasi_pegawai_nip,
						'organisasi_status' 			=> 1,
						'organisasi_create_by' 			=> $this->session->userdata('user_id'),
						'organisasi_create_date' 		=> date('Y-m-d H:i:s')
					);


					$insDb = $this->organisasi_model->create_organisasi($datacreate);

					if($insDb > 0){
						$notify = array(
							'title' 	=> 'Berhasil!',
							'message' 	=> 'Penambahan Data organisasi Berhasil',
							'status' 	=> 'success'
						);
						$this->session->set_flashdata('notify', $notify);

						redirect(base_url().'kepegawaian/organisasi/update/'.$organisasi_pegawai_id);
					}else{
						$notify = array(
							'title' 	=> 'Gagal!',
							'message'	=> 'Penambahan Data organisasi Gagal, silahkan coba lagi',
							'status' 	=> 'error'
						);
						$this->session->set_flashdata('notify', $notify);
						redirect(base_url().'kepegawaian/organisasi/update/'.$organisasi_pegawai_id);
					}
				}else{
					$dataupdate = array(
						'organisasi_nama' 				=> $organisasi_nama,
						'organisasi_kedudukan' 			=> $organisasi_kedudukan,
						'organisasi_tanggal_mulai' 		=> $organisasi_tanggal_mulai,
						'organisasi_tanggal_selesai' 	=> $organisasi_tanggal_selesai,
						'organisasi_nomor_sk' 			=> $organisasi_nomor_sk,
						'organisasi_nama_jabatan' 		=> $organisasi_nama_jabatan,
						'organisasi_tahun' 				=> $organisasi_tahun,
						'organisasi_pegawai_id' 		=> $organisasi_pegawai_id,
						'organisasi_pegawai_nip' 		=> $organisasi_pegawai_nip,
						'organisasi_status' 			=> 1,
						'organisasi_create_by' 			=> $this->session->userdata('user_id'),
						'organisasi_create_date' 		=> date('Y-m-d H:i:s')
					);
					$this->organisasi_model->update_organisasi($dataupdate, $organisasi_id);

					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan Data Organisasi Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/organisasi/update/'.$organisasi_pegawai_id);
				}
			}

			$data = array();
			$data['pegawai']  	= $this->organisasi_model->getPegawai($pegawai_id)->row_array();
			$data['history']  	= $this->organisasi_model->getListOrganisasi($pegawai_id)->result_array();
			$this->template->display('kepegawaian/organisasi/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	
	public function list_data_aktif(){
		$default_order = "";
		$limit = 10;
		$operator_unit_kerja = $this->session->userdata('user_operator_unit_kerja_id');
		$operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');

		if($this->session->userdata('user_akses_id') == '2'){
        	$where = "pegawai_status = 1 and pegawai_is_pns = 1 and (pegawai_jenis_pensiun_id is null or pegawai_jenis_pensiun_id = 0) and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
		}
		else{
			$where = "pegawai_status = 1 AND pegawai_is_pns = 1";
		}
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->organisasi_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->organisasi_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->organisasi_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_nama_jabatan"],
				$row["unit_kerja_hirarki_name_full"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/organisasi/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
	public function hapus($organisasi_id = 0,$pegawai_id = 0){

		$organisasi_idFilter = filter_var($organisasi_id, FILTER_SANITIZE_NUMBER_INT);
		if($this->access->permission('delete')) {
			if($organisasi_id==$organisasi_idFilter) {

				$dataupdate = array(
					'organisasi_status'  => 0,
					'organisasi_create_by' 			=> $this->session->userdata('user_id'),
					'organisasi_create_date' 			=> date('Y-m-d H:i:s')
				);

				$del = $this->organisasi_model->update_organisasi($dataupdate,$organisasi_id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Organisasi  Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'kepegawaian/organisasi/update/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Organisasi  Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'kepegawaian/organisasi/update/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Organisasi  Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'kepegawaian/organisasi/update/'.$pegawai_id);
		}
	}

	
}