<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NonPns extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('auth/auth_model','',TRUE);
		$this->load->model('kepegawaian/pns/pns_model','',TRUE);
		$this->load->model('kepegawaian/jabatan_model','',TRUE);
		$this->load->model('kepegawaian/kepangkatan_model','',TRUE);
	}

	public function index()
	{
		$data = array();
		$this->template->display('kepegawaian/nonpns/index', $data);	
	}
	public function create()
	{
		if($this->access->permission('read')){
			if($post = $this->input->post()){
				$pegawai_unit_kerja_code = "";
				foreach($post["pegawai_unit_kerja_id"] as $field => $value) {
					if($value!=""){
						$pegawai_unit_kerja_code = $value;
					}
				}
				$datauker = $this->db->query("SELECT * FROM ms_unit_kerja WHERE unit_kerja_kode = '".$pegawai_unit_kerja_code."' AND unit_kerja_status = 1")->row_array();
				$pegawai_unit_kerja_id_fix = $datauker["unit_kerja_id"];
				$format="";//format file name
	            $strf=(string)strftime($format);
	            $config['file_name'] = '';
	            $config['upload_path']      = './assets/images/pegawai/photo';
	            $config['allowed_types']    = 'gif|jpg|png|svg|jpeg';
	            $config['max_size']         = '20000'; // KB    
	            $this->load->library('upload', $config);
	            $file_name = "";
	            if($this->upload->do_upload('pegawai_photo')) {
	                $upload_data = array('uploads' =>$this->upload->data());
	                $file_name   =  $upload_data['uploads']['file_name'];
	            }else{
	            	$file_name = '';
	            }
	                $datainsert = array(
					'pegawai_nip' => str_replace(' ', '', $post["pegawai_nip"]),
					'pegawai_nip_lama' => str_replace(' ', '', $post["pegawai_nip"]),
					'pegawai_unit_kerja_id' => $pegawai_unit_kerja_id_fix,
					'pegawai_unit_kerja_code' => $pegawai_unit_kerja_code,
					'pegawai_nomor_ktp' => str_replace(' ', '', $post["pegawai_nomor_ktp"]),
					'pegawai_nomor_npwp' => str_replace(' ', '', $post["pegawai_nomor_npwp"]),
					'pegawai_gelar_depan' => $post["pegawai_gelar_depan"],
					'pegawai_gelar_belakang' => $post["pegawai_gelar_belakang"],
					'pegawai_nama' => $post["pegawai_nama"],
					'pegawai_tempat_lahir' => $post["pegawai_tempat_lahir"],
					'pegawai_tanggal_lahir' => $post["pegawai_tanggal_lahir_submit"],
					'pegawai_jenis_kelamin_id' => $post["pegawai_jenis_kelamin_id"],
					'pegawai_perkawinan_id' => $post["pegawai_perkawinan_id"],
					'pegawai_agama_id' => $post["pegawai_agama_id"],
					'pegawai_golongan_darah_id' => $post["pegawai_golongan_darah_id"],
					'pegawai_domisili_negara_id' => $post["pegawai_domisili_negara_id"],
					'pegawai_domisili_provinsi_id' => $post["pegawai_domisili_provinsi_id"],
					'pegawai_domisili_kota_id' => $post["pegawai_domisili_kota_id"],
					'pegawai_domisili_kec_id' => $post["pegawai_domisili_kec_id"],
					'pegawai_domisili_kel_id' => $post["pegawai_domisili_kel_id"],
					'pegawai_domisili_alamat' => $post["pegawai_domisili_alamat"],
					'pegawai_domisili_kodepos' => $post["pegawai_domisili_kodepos"],
					'pegawai_ktp_provinsi_id' => $post["pegawai_ktp_provinsi_id"],
					'pegawai_ktp_kota_id' => $post["pegawai_ktp_kota_id"],
					'pegawai_ktp_kec_id' => $post["pegawai_ktp_kec_id"],
					'pegawai_ktp_kel_id' => $post["pegawai_ktp_kel_id"],
					'pegawai_ktp_alamat' => $post["pegawai_ktp_alamat"],
					'pegawai_ktp_kodepos' => $post["pegawai_ktp_kodepos"],
					'pegawai_email_lain' => $post["pegawai_email_lain"],
					'pegawai_handphone' => $post["pegawai_handphone"],
					'pegawai_cpns_tanggal_tmt' => $post["pegawai_cpns_tanggal_tmt_submit"],
					'pegawai_nama_jabatan' => $post["pegawai_nama_jabatan"],
					'pegawai_image_path' => $file_name,
					'pegawai_pendidikan_terakhir_id' => $post["pegawai_pendidikan_terakhir_id"],
					// 'pegawai_pendidikan_tingkat_id' => $post["pegawai_pendidikan_tingkat_id"],
					'pegawai_pendidikan_program_studi' => $post["pegawai_pendidikan_program_studi"],
					'pegawai_pendidikan_tahun_masuk' => $post["pegawai_pendidikan_tahun_masuk"],
					'pegawai_pendidikan_tahun_lulus' => $post["pegawai_pendidikan_tahun_lulus"],
					'pegawai_pendidikan_nama' => $post["pegawai_pendidikan_nama"],
					'pegawai_pendidikan_tempat' => $post["pegawai_pendidikan_tempat"],
					'pegawai_pendidikan_ipk' => $post["pegawai_pendidikan_ipk"],
					'pegawai_nilai_toefl' => $post["pegawai_nilai_toefl"],
					'pegawai_nilai_ielt' => $post["pegawai_nilai_ielt"],
					'pegawai_catatan' => $post["pegawai_catatan"],
					'pegawai_is_non_pns' => '0',
					'pegawai_is_pns' => '0',
					'pegawai_create_by' => $this->session->userdata('user_id'),
					'pegawai_create_date' => date('Y-m-d H:i:s'),
					'pegawai_status' => 1,
					'pegawai_sinkronisasi' => 0
				);
				// echo "<pre>"; print_r($datainsert); die;
				$pegawai_id = $this->pns_model->createpegawai($datainsert);//
				if($pegawai_id > 0){
					$this->db->query("UPDATE ms_pegawai SET pegawai_nip = '".$pegawai_id."', pegawai_nip_lama = '".$pegawai_id."' WHERE pegawai_id = '".$pegawai_id."'");
					$this->db->query("INSERT INTO  [dbo].[ms_user] (user_nip,username,password,user_status,user_akses_id,user_default_akses_id) VALUES('".$pegawai_id."','".$pegawai_id."','".md5($post["pegawai_nomor_ktp"])."',1,3,3)");
					$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["anak_pegawai_nip"])."','65','1X')");
					
					$datacreate = array(
						'jenjang_jabatan_pegawai_id' => $pegawai_id,
						'jenjang_jabatan_pegawai_nip' => str_replace(' ', '', $post["pegawai_nip"]),
						// 'jenjang_jabatan_jabatan_id' => $post["pegawai_jabatan_id"],
						'jenjang_jabatan_jabatan_nama' => $post["pegawai_nama_jabatan"],
						'jenjang_jabatan_unit_kerja_id' => $pegawai_unit_kerja_id_fix,
						'jenjang_jabatan_tanggal_mulai' => $post["pegawai_cpns_tanggal_tmt_submit"],
						'jenjang_jabatan_create_by' => $this->session->userdata('user_id'),
						'jenjang_jabatan_create_date' => date('Y-m-d H:i:s'),
						'jenjang_jabatan_status' => 1
					);
					$this->jabatan_model->create_jenjang_jabatan($datacreate);
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Penambahan Pegawai Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/nonpns');
				}else{
					$notify = array(
						'title' 	=> 'Gagal!',
						'message'	=> 'Penambahan Pegawai gagal, silahkan coba lagi',
						'status' 	=> 'error'
					);
					$this->session->set_flashdata('notify', $notify);
					redirect(base_url().'kepegawaian/nonpns');
				}
			}

			$data = array();
			$data["provinsi"] = $this->pns_model->getProvinsi()->result_array();
			$data["jenis_kelamin"] = $this->pns_model->getJenisKelamin()->result_array();
			$data["status_perkawinan"] = $this->pns_model->getStatusKawin()->result_array();
			$data["agama"] = $this->pns_model->getAgama()->result_array();
			$data["golongan_darah"] = $this->pns_model->getGolonganDarah()->result_array();
			$data["pendidikan"] = $this->pns_model->getPendidikan()->result_array();
			$data["negara"] = $this->pns_model->getNegara()->result_array();
			$data["unit_kerja"] = $this->pns_model->getUnitKerja(1,2)->result_array();
			$data["diklat"] = $this->pns_model->getJenisDiklat()->result_array();
			$data["status_pegawai"] = $this->pns_model->getStatusPegawai()->result_array();
			$data["fungsional_tertentu"] = $this->pns_model->getFungsionalTertentu()->result_array();
			$data["fungsional_tertentu_formasi"] = $this->pns_model->getFormasiFungsionalTertentu()->result_array();
			$data["golongan"] = $this->pns_model->getGolongan()->result_array();
			$data["jabatan"] = $this->pns_model->getJabatan()->result_array();
			$this->template->display('kepegawaian/nonpns/create', $data);	

		}else{
			$this->access->redirect('404');
		}
	}
	public function update($pegawai_id)
	{
		if($this->access->permission('read')){
			if($post = $this->input->post()){
				$pegawai_id = $post["pegawai_id"];
				$pegawai_status = $post["pegawai_status"];
				$pegawai_unit_kerja_code = "";
				foreach($post["pegawai_unit_kerja_id"] as $field => $value) {
					if($value!=""){
						$pegawai_unit_kerja_code = $value;
					}
				}
				$datauker = $this->db->query("SELECT * FROM ms_unit_kerja WHERE unit_kerja_kode = '".$pegawai_unit_kerja_code."' AND unit_kerja_status = 1")->row_array();
				$pegawai_unit_kerja_id_fix = $datauker["unit_kerja_id"];
				$format="";//format file name
	            $strf=(string)strftime($format);
	            $config['file_name'] = $strf;
	            $config['upload_path']      = './assets/images/pegawai/photo';
	            $config['allowed_types']    = 'gif|jpg|png|svg|jpeg';
	            $config['max_size']         = '20000'; // KB    
	            $this->load->library('upload', $config);
	            $file_name = "";
	            if($this->upload->do_upload('pegawai_photo')) {
	                $upload_data = array('uploads' =>$this->upload->data());
	                $file_name   =  $upload_data['uploads']['file_name'];
	            }else{
	            	$file_name = '';
	            }
	            // echo "<pre>";
	            // print_r($upload_data);die;
				$dataupdate = array(
					'pegawai_nip' => str_replace(' ', '', $post["pegawai_nip"]),
					'pegawai_unit_kerja_id' => $pegawai_unit_kerja_id_fix,
					'pegawai_unit_kerja_code' => $pegawai_unit_kerja_code,
					'pegawai_nomor_ktp' => str_replace(' ', '', $post["pegawai_nomor_ktp"]),
					'pegawai_nomor_npwp' => str_replace(' ', '', $post["pegawai_nomor_npwp"]),
					'pegawai_gelar_depan' => $post["pegawai_gelar_depan"],
					'pegawai_gelar_belakang' => $post["pegawai_gelar_belakang"],
					'pegawai_nama' => $post["pegawai_nama"],
					'pegawai_tempat_lahir' => $post["pegawai_tempat_lahir"],
					'pegawai_tanggal_lahir' => $post["pegawai_tanggal_lahir_submit"],
					'pegawai_jenis_kelamin_id' => $post["pegawai_jenis_kelamin_id"],
					'pegawai_perkawinan_id' => $post["pegawai_perkawinan_id"],
					'pegawai_agama_id' => $post["pegawai_agama_id"],
					'pegawai_golongan_darah_id' => $post["pegawai_golongan_darah_id"],
					'pegawai_domisili_negara_id' => $post["pegawai_domisili_negara_id"],
					'pegawai_domisili_provinsi_id' => $post["pegawai_domisili_provinsi_id"],
					'pegawai_domisili_kota_id' => $post["pegawai_domisili_kota_id"],
					'pegawai_domisili_kec_id' => $post["pegawai_domisili_kec_id"],
					'pegawai_domisili_kel_id' => $post["pegawai_domisili_kel_id"],
					'pegawai_domisili_alamat' => $post["pegawai_domisili_alamat"],
					'pegawai_domisili_kodepos' => $post["pegawai_domisili_kodepos"],
					'pegawai_ktp_provinsi_id' => $post["pegawai_ktp_provinsi_id"],
					'pegawai_ktp_kota_id' => $post["pegawai_ktp_kota_id"],
					'pegawai_ktp_kec_id' => $post["pegawai_ktp_kec_id"],
					'pegawai_ktp_kel_id' => $post["pegawai_ktp_kel_id"],
					'pegawai_ktp_alamat' => $post["pegawai_ktp_alamat"],
					'pegawai_ktp_kodepos' => $post["pegawai_ktp_kodepos"],
					'pegawai_email_lain' => $post["pegawai_email_lain"],
					'pegawai_handphone' => $post["pegawai_handphone"],
					'pegawai_cpns_tanggal_tmt' => $post["pegawai_cpns_tanggal_tmt_submit"],
					'pegawai_nama_jabatan' => $post["pegawai_nama_jabatan"],
					'pegawai_image_path' => $file_name,
					'pegawai_pendidikan_terakhir_id' => $post["pegawai_pendidikan_terakhir_id"],
					// 'pegawai_pendidikan_tingkat_id' => $post["pegawai_pendidikan_tingkat_id"],
					'pegawai_pendidikan_program_studi' => $post["pegawai_pendidikan_program_studi"],
					'pegawai_pendidikan_tahun_masuk' => $post["pegawai_pendidikan_tahun_masuk"],
					'pegawai_pendidikan_tahun_lulus' => $post["pegawai_pendidikan_tahun_lulus"],
					'pegawai_pendidikan_nama' => $post["pegawai_pendidikan_nama"],
					'pegawai_pendidikan_tempat' => $post["pegawai_pendidikan_tempat"],
					'pegawai_pendidikan_ipk' => $post["pegawai_pendidikan_ipk"],
					'pegawai_nilai_toefl' => $post["pegawai_nilai_toefl"],
					'pegawai_nilai_ielt' => $post["pegawai_nilai_ielt"],
					'pegawai_catatan' => $post["pegawai_catatan"],
					'pegawai_is_non_pns' => '0',
					'pegawai_is_pns' => '0',
					'pegawai_create_by' => $this->session->userdata('user_id'),
					'pegawai_create_date' => date('Y-m-d H:i:s'),
					'pegawai_status' => 1,
					'pegawai_sinkronisasi' => 0
				);
					// echo "<pre>"; print_r($dataupdate); die;
					if($pegawai_status == 1){
						$pegawai_id = $this->pns_model->updatepegawai($dataupdate, $pegawai_id);
					}else{
						$update_pegawai = array(
							'pegawai_tanggal_pensiun' => $post["pegawai_tanggal_pensiun_submit"],
							'pegawai_catatan' => $post["pegawai_catatan"],
							'pegawai_status' => 0
						);
						$this->pns_model->updatepegawai($update_pegawai, $pegawai_id);
						// $this->db->query("UPDATE ms_pegawai SET pegawai_status = 0 WHERE pegawai_id = ".$pegawai_id);
					}

				if($pegawai_id > 0){
					$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["anak_pegawai_nip"])."','66','22')");
					$this->load->model('kepegawaian/pendidikan_formal_model');
					$this->load->model('kepegawaian/diklat_perjenjangan_model');
					$pendidikan_formal_lokasi = "";
					$pendidikan_formal_no_ijazah = "";
					$pendidikan_formal_nama_kep = "";
					$pendidikan_formal_pegawai_id = $pegawai_id;
					$pendidikan_formal_pegawai_nip = str_replace(' ', '', $post["pegawai_nip"]);

					$datacreate = array(
						'pendidikan_formal_lokasi' 				=> $pendidikan_formal_lokasi,
						'pendidikan_formal_no_ijazah' 			=> $pendidikan_formal_no_ijazah,
						'pendidikan_formal_nama_kep' 			=> $pendidikan_formal_nama_kep,
						'pendidikan_formal_pegawai_id' 			=> $pendidikan_formal_pegawai_id,
						'pendidikan_formal_pegawai_nip' 		=> $pendidikan_formal_pegawai_nip,
						'pendidikan_formal_status' 				=> 1,
						'pendidikan_formal_create_by' 			=> $this->session->userdata('user_id'),
						'pendidikan_formal_create_date' 		=> date('Y-m-d H:i:s')
					);
					$this->pendidikan_formal_model->create_pendidikan($datacreate);


					$diklat_pegawai_id = $pegawai_id;
					$diklat_pegawai_nip = str_replace(' ', '', $post["pegawai_nip"]);

					$diklat_type = 1;
					$datacreate = array(
						'diklat_pegawai_id' => $diklat_pegawai_id,
						'diklat_pegawai_nip' => $diklat_pegawai_nip,
						'diklat_type' => $diklat_type,
						'diklat_create_by' => $this->session->userdata('user_id'),
						'diklat_create_date' => date('Y-m-d H:i:s'),
						'diklat_status' => 1,
					);
					$this->diklat_perjenjangan_model->create_diklat($datacreate);
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Penambahan Pegawai Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/nonpns');
				}else{
					$notify = array(
						'title' 	=> 'Gagal!',
						'message'	=> 'Penambahan Pegawai gagal, silahkan coba lagi',
						'status' 	=> 'error'
					);
					$this->session->set_flashdata('notify', $notify);
					redirect(base_url().'kepegawaian/nonpns');
				}
			}

			$data = array();
			$data["pegawai"] = $this->pns_model->getPegawai($pegawai_id)->row_array();
			$data["provinsi"] = $this->pns_model->getProvinsi()->result_array();
			$data["jenis_kelamin"] = $this->pns_model->getJenisKelamin()->result_array();
			$data["status_perkawinan"] = $this->pns_model->getStatusKawin()->result_array();
			$data["agama"] = $this->pns_model->getAgama()->result_array();
			$data["golongan_darah"] = $this->pns_model->getGolonganDarah()->result_array();
			$data["pendidikan"] = $this->pns_model->getPendidikan()->result_array();
			$data["negara"] = $this->pns_model->getNegara()->result_array();
			$data["unit_kerja"] = $this->pns_model->getUnitKerja(1,2)->result_array();
			$data["diklat"] = $this->pns_model->getJenisDiklat()->result_array();
			$data["status_pegawai"] = $this->pns_model->getStatusPegawai()->result_array();
			$data["fungsional_tertentu"] = $this->pns_model->getFungsionalTertentu()->result_array();
			$data["fungsional_tertentu_formasi"] = $this->pns_model->getFormasiFungsionalTertentu()->result_array();
			$data["golongan"] = $this->pns_model->getGolongan()->result_array();
			$data["jabatan"] = $this->pns_model->getJabatan()->result_array();
			$this->template->display('kepegawaian/nonpns/update', $data);	

		}else{
			$this->access->redirect('404');
		}
	}
	public function get_kota()
	{
		$provinsi_kode 	= $this->input->post('provinsi_kode');
		$data["kota"] = $this->pns_model->getKota($provinsi_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_tingkat_pendidikan()
	{
		$pendidikan_kode 	= $this->input->post('pendidikan_kode');
		$data["tingkat_pendidikan"] = $this->pns_model->getTingkatPendidikan($pendidikan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_jurusan()
	{
		$jurusan_kode 	= $this->input->post('jurusan_kode');
		$data["jurusan"] = $this->pns_model->getJurusan($jurusan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function list_data($status_nonpns){
		$default_order = "pegawai_id";
		$limit = 10;
		$operator_unit_kerja = $this->session->userdata('user_operator_unit_kerja_id');
		$operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');

		if($this->session->userdata('user_akses_id') == '2'){
			if($status_nonpns != '99'){
        		$where = "pegawai_status = ".$status_nonpns." and pegawai_is_pns = 0 and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
			}else{
        		$where = "pegawai_status = 1 and pegawai_is_pns = 0 and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
			}
		}
		else{
			if($status_nonpns != '99'){
				$where = "pegawai_status = ".$status_nonpns." AND pegawai_is_pns = 0";
			}else{
				$where = "pegawai_is_pns = 0";
			}
		}
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}

		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->pns_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->pns_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->pns_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_tempat_tanggal_lahir"],
				$row["pegawai_nama_jabatan"],
				// $row["unit_kerja_nama"],
				$row["unit_kerja_hirarki_name_full"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/nonpns/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
	// public function list_data_nonaktif(){
	// 	$default_order = "pegawai_id";
	// 	$limit = 10;
	// 	$operator_unit_kerja = $this->session->userdata('user_operator_unit_kerja_id');
	// 	$operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');

	// 	if($this->session->userdata('user_akses_id') == '2'){
 //        	$where = "pegawai_status = 0 and pegawai_is_pns = 0 and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
	// 	}
	// 	else{
	// 		$where = "pegawai_status = 0 AND pegawai_is_pns = 0";
	// 	}
	// 	$field_name 	= array(
	// 		'pegawai_nip',
	// 		'pegawai_nama'
	// 	);
	// 	$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
	// 	$ordertextarr = array();
	// 	for ($i = 0;$i<$iSortingCols;$i++){
	// 		$iSortCol 	= ($this->input->get('iSortCol_'.$i));
	// 		$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
	// 		$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
	// 	}

	// 	$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
	// 	$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
	// 	$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
	// 	$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
	// 	$data['sEcho'] = $this->input->get('sEcho');
	// 	$data['iTotalRecords'][] = $this->pns_model->get_count_all_data($search,$field_name, $where);
	// 	$data['iTotalDisplayRecords'][] = $this->pns_model->get_count_all_data($search,$field_name, $where);


	// 	$aaData = array();
	// 	$getData 	= $this->pns_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
	// 	$no = (($start == 0) ? 1 : $start + 1);
	// 	foreach ($getData as $row) {
	// 		$aaData[] = array(
	// 			$row["pegawai_nip"],
	// 			$row["pegawai_nama"],
	// 			$row["pegawai_tempat_tanggal_lahir"],
	// 			$row["pegawai_nama_jabatan"],
	// 			// $row["unit_kerja_nama"],
	// 			$row["unit_kerja_hirarki_name_full"],
	// 			'<ul class="icons-list">
	// 			<li><a href="'.base_url().'kepegawaian/nonpns/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
	// 			</ul>'
	// 		);
	// 		$no++;
	// 	}
	// 	$data['aaData'] = $aaData;
	// 	$this->output->set_content_type('application/json')->set_output(json_encode($data));

	// }
	// public function list_data_aktifnonaktif(){
	// 	$default_order = "pegawai_id";
	// 	$limit = 10;
	// 	$operator_unit_kerja = $this->session->userdata('user_operator_unit_kerja_id');
	// 	$operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');

	// 	if($this->session->userdata('user_akses_id') == '2'){
 //        	$where = "pegawai_is_pns = 0 and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
	// 	}
	// 	else{
	// 		$where = "pegawai_is_pns = 0";
	// 	}
	// 	$field_name 	= array(
	// 		'pegawai_nip',
	// 		'pegawai_nama'
	// 	);
	// 	$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
	// 	$ordertextarr = array();
	// 	for ($i = 0;$i<$iSortingCols;$i++){
	// 		$iSortCol 	= ($this->input->get('iSortCol_'.$i));
	// 		$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
	// 		$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
	// 	}

	// 	$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
	// 	$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
	// 	$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
	// 	$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
	// 	$data['sEcho'] = $this->input->get('sEcho');
	// 	$data['iTotalRecords'][] = $this->pns_model->get_count_all_data($search,$field_name, $where);
	// 	$data['iTotalDisplayRecords'][] = $this->pns_model->get_count_all_data($search,$field_name, $where);


	// 	$aaData = array();
	// 	$getData 	= $this->pns_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
	// 	$no = (($start == 0) ? 1 : $start + 1);
	// 	foreach ($getData as $row) {
	// 		$aaData[] = array(
	// 			$row["pegawai_nip"],
	// 			$row["pegawai_nama"],
	// 			$row["pegawai_tempat_tanggal_lahir"],
	// 			$row["pegawai_nama_jabatan"],
	// 			// $row["unit_kerja_nama"],
	// 			$row["unit_kerja_hirarki_name_full"],
	// 			'<ul class="icons-list">
	// 			<li><a href="'.base_url().'kepegawaian/nonpns/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
	// 			</ul>'
	// 		);
	// 		$no++;
	// 	}
	// 	$data['aaData'] = $aaData;
	// 	$this->output->set_content_type('application/json')->set_output(json_encode($data));

	// }
}
