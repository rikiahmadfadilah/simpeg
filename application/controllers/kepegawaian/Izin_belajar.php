<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Izin_belajar extends CI_Controller{

	function __construct(){
		parent:: __construct();
		$this->load->model('kepegawaian/izin_belajar_model');
		//$this->load->library('access');
		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index(){
		if($this->access->permission('read')){
			$data = array();
			$this->template->display('kepegawaian/izin_belajar/index', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function update($pegawai_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];
				$izin_belajar_id = $post['izin_belajar_id'];
				$izin_belajar_jenis = $post['izin_belajar_jenis'];
				$izin_belajar_pendidikan_id = $post['izin_belajar_pendidikan_id'];
				$izin_belajar_tingkat_pendidikan = $post['izin_belajar_tingkat_pendidikan'];
				$izin_belajar_sekolah_universitas_id = $post['izin_belajar_sekolah_universitas_id'];
				$izin_belajar_prodi_id = $post['izin_belajar_prodi_id'];
				$izin_belajar_negara_tipe = $post['izin_belajar_negara_tipe'];
				$izin_belajar_nama_sekolah = $post['izin_belajar_nama_sekolah'];
				// $izin_belajar_fakultas = $post['izin_belajar_fakultas'];
				// $izin_belajar_jurusan = $post['izin_belajar_jurusan'];
				$izin_belajar_kota_nama = $post['izin_belajar_kota_nama'];
				$izin_belajar_negara_id = $post['izin_belajar_negara_id'];
				$izin_belajar_status_sekolah = $post['izin_belajar_status_sekolah'];
				$izin_belajar_sponsor = $post['izin_belajar_sponsor'];
				$izin_belajar_tmt_izin = $post['izin_belajar_tmt_izin'];
				// $izin_belajar_tahun_selesai = $post['izin_belajar_tahun_selesai'];
				$izin_belajar_tanggal_mulai = $post['izin_belajar_tanggal_mulai'];
				$izin_belajar_tanggal_selesai = $post['izin_belajar_tanggal_selesai'];
				$izin_belajar_status_proses = $post['izin_belajar_status_proses'];
				$izin_belajar_tanggal_surat = $post['izin_belajar_tanggal_surat'];
				$izin_belajar_nomor_surat = $post['izin_belajar_nomor_surat'];
				$izin_belajar_jabatan_pemberi_izin = $post['izin_belajar_jabatan_pemberi_izin'];
				$izin_belajar_pegawai_id = $post['izin_belajar_pegawai_id'];
				$izin_belajar_pegawai_nip = $post['izin_belajar_pegawai_nip'];
			
				if($isUpdate == 0){
					$datacreate = array(
						'izin_belajar_jenis' 				=> $izin_belajar_jenis,
						'izin_belajar_pendidikan_id' 		=> $izin_belajar_pendidikan_id,
						'izin_belajar_tingkat_pendidikan' 	=> $izin_belajar_tingkat_pendidikan,
						'izin_belajar_sekolah_universitas_id' => $izin_belajar_sekolah_universitas_id,
						'izin_belajar_prodi_id' 			=> $izin_belajar_prodi_id,
						'izin_belajar_negara_tipe' 			=> $izin_belajar_negara_tipe,
						'izin_belajar_nama_sekolah' 		=> $izin_belajar_nama_sekolah,
						// 'izin_belajar_fakultas' 			=> $izin_belajar_fakultas,
						// 'izin_belajar_jurusan' 				=> $izin_belajar_jurusan,
						'izin_belajar_kota_nama' 			=> $izin_belajar_kota_nama,
						'izin_belajar_negara_id' 			=> $izin_belajar_negara_id,
						'izin_belajar_status_sekolah' 		=> $izin_belajar_status_sekolah,
						'izin_belajar_sponsor' 				=> $izin_belajar_sponsor,
						'izin_belajar_tmt_izin' 			=> $izin_belajar_tmt_izin,
						'izin_belajar_tahun_selesai' 		=> substr($izin_belajar_tanggal_selesai,7),
						'izin_belajar_tanggal_mulai' 		=> $izin_belajar_tanggal_mulai,
						'izin_belajar_tanggal_selesai' 		=> $izin_belajar_tanggal_selesai,
						'izin_belajar_status_proses' 		=> $izin_belajar_status_proses,
						'izin_belajar_tanggal_surat' 		=> $izin_belajar_tanggal_surat,
						'izin_belajar_nomor_surat' 			=> $izin_belajar_nomor_surat,
						'izin_belajar_jabatan_pemberi_izin' => $izin_belajar_jabatan_pemberi_izin,
						'izin_belajar_pegawai_id' 			=> $izin_belajar_pegawai_id,
						'izin_belajar_pegawai_nip' 			=> $izin_belajar_pegawai_nip,
						'izin_belajar_status' 				=> 1,
						'izin_belajar_create_by' 			=> $this->session->userdata('user_id'),
						'izin_belajar_create_date' 			=> date('Y-m-d H:i:s')
					);
					// echo "<pre>"; print_r($datacreate); die;

					$insDb = $this->izin_belajar_model->create_izin_belajar($datacreate);

					if($insDb > 0){
						$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["anak_pegawai_nip"])."','7','1G')");
						$notify = array(
							'title' 	=> 'Berhasil!',
							'message' 	=> 'Penambahan Data Izin Belajar Berhasil',
							'status' 	=> 'success'
						);
						$this->session->set_flashdata('notify', $notify);

						redirect(base_url().'kepegawaian/izin_belajar/update/'.$izin_belajar_pegawai_id);
					}else{
						$notify = array(
							'title' 	=> 'Gagal!',
							'message'	=> 'Penambahan Data izin_belajar Gagal, silahkan coba lagi',
							'status' 	=> 'error'
						);
						$this->session->set_flashdata('notify', $notify);
						redirect(base_url().'kepegawaian/izin_belajar/update/'.$izin_belajar_pegawai_id);
					}
				}else{
					$dataupdate = array(
						'izin_belajar_pendidikan_id' 		=> $izin_belajar_pendidikan_id,
						'izin_belajar_tingkat_pendidikan' 	=> $izin_belajar_tingkat_pendidikan,
						'izin_belajar_sekolah_universitas_id' => $izin_belajar_sekolah_universitas_id,
						'izin_belajar_prodi_id' 			=> $izin_belajar_prodi_id,
						'izin_belajar_negara_tipe' 			=> $izin_belajar_negara_tipe,
						'izin_belajar_nama_sekolah' 		=> $izin_belajar_nama_sekolah,
						// 'izin_belajar_fakultas' 			=> $izin_belajar_fakultas,
						// 'izin_belajar_jurusan' 				=> $izin_belajar_jurusan,
						'izin_belajar_kota_nama' 			=> $izin_belajar_kota_nama,
						'izin_belajar_negara_id' 			=> $izin_belajar_negara_id,
						'izin_belajar_status_sekolah' 		=> $izin_belajar_status_sekolah,
						'izin_belajar_sponsor' 				=> $izin_belajar_sponsor,
						'izin_belajar_tmt_izin' 			=> $izin_belajar_tmt_izin,
						'izin_belajar_tahun_selesai' 		=> substr($izin_belajar_tanggal_selesai,7),
						'izin_belajar_tanggal_mulai' 		=> $izin_belajar_tanggal_mulai,
						'izin_belajar_tanggal_selesai' 		=> $izin_belajar_tanggal_selesai,
						'izin_belajar_status_proses' 		=> $izin_belajar_status_proses,
						'izin_belajar_tanggal_surat' 		=> $izin_belajar_tanggal_surat,
						'izin_belajar_nomor_surat' 			=> $izin_belajar_nomor_surat,
						'izin_belajar_jabatan_pemberi_izin' => $izin_belajar_jabatan_pemberi_izin,
						'izin_belajar_status' 				=> 1,
						'izin_belajar_create_by' 			=> $this->session->userdata('user_id'),
						'izin_belajar_create_date' 			=> date('Y-m-d H:i:s')
					);
					echo "<pre>"; print_r($dataupdate);
					$this->izin_belajar_model->update_izin_belajar($dataupdate, $izin_belajar_id);
					$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["anak_pegawai_nip"])."','36','2O')");
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan Data Izin Belajar Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/izin_belajar/update/'.$izin_belajar_pegawai_id);
				}
			}

			$data = array();
			$data['pegawai']  	= $this->izin_belajar_model->getPegawai($pegawai_id)->row_array();
			$data['history']  	= $this->izin_belajar_model->getListIzin_belajar($pegawai_id)->result_array();
			$data['tingkat']  	= $this->izin_belajar_model->getTingkat()->result_array();
			$data['negara']  	= $this->izin_belajar_model->getNegara()->result_array();
			$this->template->display('kepegawaian/izin_belajar/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function get_tingkat_pendidikan()
	{
		$pendidikan_kode 	= $this->input->post('pendidikan_kode');
		$data["tingkat_pendidikan"] = $this->izin_belajar_model->getTingkatPendidikan($pendidikan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_fakultas()
	{
		$jurusan_kode 	= $this->input->post('jurusan_kode');
		$data["jurusan"] = $this->izin_belajar_model->getFakultas($jurusan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_jurusan()
	{
		$jurusan_kode 	= $this->input->post('jurusan_kode');
		$data["jurusan"] = $this->izin_belajar_model->getJurusan($jurusan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	
	public function list_data_aktif(){
		$default_order = "";
		$limit = 10;
		$operator_unit_kerja = $this->session->userdata('user_operator_unit_kerja_id');
		$operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');

		if($this->session->userdata('user_akses_id') == '2'){
        	$where = "pegawai_status = 1 and pegawai_is_pns = 1 and (pegawai_jenis_pensiun_id is null or pegawai_jenis_pensiun_id = 0) and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
		}
		else{
			$where = "pegawai_status = 1 AND pegawai_is_pns = 1";
		}
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->izin_belajar_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->izin_belajar_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->izin_belajar_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_nama_jabatan"],
				$row["unit_kerja_hirarki_name_full"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/izin_belajar/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
	public function hapus($izin_belajar_id = 0,$pegawai_id = 0){

		$izin_belajar_idFilter = filter_var($izin_belajar_id, FILTER_SANITIZE_NUMBER_INT);
		if($this->access->permission('delete')) {
			if($izin_belajar_id==$izin_belajar_idFilter) {

				$dataupdate = array(
					'izin_belajar_status'  => 0,
					'izin_belajar_create_by' 			=> $this->session->userdata('user_id'),
					'izin_belajar_create_date' 			=> date('Y-m-d H:i:s')
				);

				$del = $this->izin_belajar_model->update_izin_belajar($dataupdate,$izin_belajar_id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'izin_belajar Formal Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'kepegawaian/izin_belajar/update/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'izin_belajar Formal Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'kepegawaian/izin_belajar/update/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'izin_belajar Formal Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'kepegawaian/izin_belajar/update/'.$pegawai_id);
		}
	}

	
}