<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dp3 extends CI_Controller{

	function __construct(){
		parent:: __construct();
		$this->load->model('kepegawaian/dp3_model');
		//$this->load->library('access');
		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index(){
		if($this->access->permission('read')){
			$data = array();
			$this->template->display('kepegawaian/dp3/index', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function update($pegawai_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];
				$dp3_id = $post['dp3_id'];
				$dp3_penilai_nip = $post['dp3_penilai_nip'];
				$dp3_penilai_nama = $post['dp3_penilai_nama'];
				$dp3_penilai_jabatan = $post['dp3_penilai_jabatan'];
				$dp3_penilai_golongan = $post['dp3_penilai_golongan'];
				$dp3_penilai_unit_kerja = $post['dp3_penilai_unit_kerja'];
				$dp3_atasan_nip = $post['dp3_atasan_nip'];
				$dp3_atasan_nama = $post['dp3_atasan_nama'];
				$dp3_atasan_jabatan = $post['dp3_atasan_jabatan'];
				$dp3_atasan_golongan = $post['dp3_atasan_golongan'];
				$dp3_atasan_unit_kerja = $post['dp3_atasan_unit_kerja'];
				$dp3_tahun_penilaian = $post['dp3_tahun_penilaian'];
				$dp3_kesetiaan = $post['dp3_kesetiaan'];
				$dp3_prestasi = $post['dp3_prestasi'];
				$dp3_tanggung_jawab = $post['dp3_tanggung_jawab'];
				$dp3_ketaatan = $post['dp3_ketaatan'];
				$dp3_kejujuran = $post['dp3_kejujuran'];
				$dp3_kerjasama = $post['dp3_kerjasama'];
				$dp3_prakarsa = $post['dp3_prakarsa'];
				$dp3_kepemimpinan = $post['dp3_kepemimpinan'];
				$dp3_rata_rata = $post['dp3_rata_rata'];
				$dp3_pegawai_id = $post['dp3_pegawai_id'];
				$dp3_pegawai_nip = $post['dp3_pegawai_nip'];

				if($isUpdate == 0){
					$datacreate = array(
						'dp3_penilai_nip' 		=> $dp3_penilai_nip,
						'dp3_penilai_nama' 		=> $dp3_penilai_nama,
						'dp3_penilai_jabatan' 	=> $dp3_penilai_jabatan,
						'dp3_penilai_golongan' 	=> $dp3_penilai_golongan,
						'dp3_penilai_unit_kerja'=> $dp3_penilai_unit_kerja,
						'dp3_atasan_nip' 		=> $dp3_atasan_nip,
						'dp3_atasan_nama' 		=> $dp3_atasan_nama,
						'dp3_atasan_jabatan' 	=> $dp3_atasan_jabatan,
						'dp3_atasan_golongan' 	=> $dp3_atasan_golongan,
						'dp3_atasan_unit_kerja' => $dp3_atasan_unit_kerja,
						'dp3_tahun_penilaian' 	=> $dp3_tahun_penilaian,
						'dp3_kesetiaan' 		=> $dp3_kesetiaan,
						'dp3_prestasi' 			=> $dp3_prestasi,
						'dp3_tanggung_jawab' 	=> $dp3_tanggung_jawab,
						'dp3_ketaatan' 			=> $dp3_ketaatan,
						'dp3_kejujuran' 		=> $dp3_kejujuran,
						'dp3_kerjasama' 		=> $dp3_kerjasama,
						'dp3_prakarsa' 			=> $dp3_prakarsa,
						'dp3_kepemimpinan' 		=> $dp3_kepemimpinan,
						'dp3_rata_rata' 		=> $dp3_rata_rata,
						'dp3_pegawai_id' 		=> $dp3_pegawai_id,
						'dp3_pegawai_nip' 		=> $dp3_pegawai_nip,
						'dp3_status' 			=> 1,
						'dp3_create_by' 		=> $this->session->userdata('user_id'),
						'dp3_create_date' 		=> date('Y-m-d H:i:s')
					);


					$insDb = $this->dp3_model->create_dp3($datacreate);

					if($insDb > 0){
						$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["dp3_penilai_nip"])."','9','1I')");
						$notify = array(
							'title' 	=> 'Berhasil!',
							'message' 	=> 'Penambahan Data dp3 Berhasil',
							'status' 	=> 'success'
						);
						$this->session->set_flashdata('notify', $notify);

						redirect(base_url().'kepegawaian/dp3/update/'.$dp3_pegawai_id);
					}else{
						$notify = array(
							'title' 	=> 'Gagal!',
							'message'	=> 'Penambahan Data dp3 Gagal, silahkan coba lagi',
							'status' 	=> 'error'
						);
						$this->session->set_flashdata('notify', $notify);
						redirect(base_url().'kepegawaian/dp3/update/'.$dp3_pegawai_id);
					}
				}else{
					$dataupdate = array(
						'dp3_penilai_nip' 		=> $dp3_penilai_nip,
						'dp3_penilai_nama' 		=> $dp3_penilai_nama,
						'dp3_penilai_jabatan' 	=> $dp3_penilai_jabatan,
						'dp3_penilai_golongan' 	=> $dp3_penilai_golongan,
						'dp3_penilai_unit_kerja'=> $dp3_penilai_unit_kerja,
						'dp3_atasan_nip' 		=> $dp3_atasan_nip,
						'dp3_atasan_nama' 		=> $dp3_atasan_nama,
						'dp3_atasan_jabatan' 	=> $dp3_atasan_jabatan,
						'dp3_atasan_golongan' 	=> $dp3_atasan_golongan,
						'dp3_atasan_unit_kerja' => $dp3_atasan_unit_kerja,
						'dp3_tahun_penilaian' 	=> $dp3_tahun_penilaian,
						'dp3_kesetiaan' 		=> $dp3_kesetiaan,
						'dp3_prestasi' 			=> $dp3_prestasi,
						'dp3_tanggung_jawab' 	=> $dp3_tanggung_jawab,
						'dp3_ketaatan' 			=> $dp3_ketaatan,
						'dp3_kejujuran' 		=> $dp3_kejujuran,
						'dp3_kerjasama' 		=> $dp3_kerjasama,
						'dp3_prakarsa' 			=> $dp3_prakarsa,
						'dp3_kepemimpinan' 		=> $dp3_kepemimpinan,
						'dp3_rata_rata' 		=> $dp3_rata_rata,
						'dp3_status' 			=> 1,
						'dp3_create_by' 		=> $this->session->userdata('user_id'),
						'dp3_create_date' 		=> date('Y-m-d H:i:s')
					);
					$this->dp3_model->update_dp3($dataupdate, $dp3_id);
					$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["dp3_penilai_nip"])."','30','2I')");
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan Data dp3 Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/dp3/update/'.$dp3_pegawai_id);
				}
			}

			$data = array();
			$data['pegawai']  	= $this->dp3_model->getPegawai($pegawai_id)->row_array();
			// $data['atasan']  	= $this->dp3_model->getAtasan()->result_array();
			$data['history']  	= $this->dp3_model->getListdp3($pegawai_id)->result_array();
			$this->template->display('kepegawaian/dp3/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function list_data(){
		$default_order = "";
		$limit = 10;
		$operator_unit_kerja = $this->session->userdata('user_operator_unit_kerja_id');
		$operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');

		if($this->session->userdata('user_akses_id') == '2'){
        	$where = "pegawai_status = 1 and pegawai_is_pns = 1 and (pegawai_jenis_pensiun_id is null or pegawai_jenis_pensiun_id = 0) and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
		}
		else{
			$where = "pegawai_status = 1 AND pegawai_is_pns = 1";
		}
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->dp3_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->dp3_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->dp3_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_nama_jabatan"],
				$row["unit_kerja_hirarki_name_full"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/dp3/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
	public function hapus($dp3_id = 0,$pegawai_id = 0){

		$dp3_idFilter = filter_var($dp3_id, FILTER_SANITIZE_NUMBER_INT);
		if($this->access->permission('delete')) {
			if($dp3_id==$dp3_idFilter) {

				$dataupdate = array(
					'dp3_status'  => 0,
					'dp3_create_by' 			=> $this->session->userdata('user_id'),
					'dp3_create_date' 			=> date('Y-m-d H:i:s')
				);

				$del = $this->dp3_model->update_dp3($dataupdate,$dp3_id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'dp3 Formal Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'kepegawaian/dp3/update/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'dp3 Formal Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'kepegawaian/dp3/update/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'dp3 Formal Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'kepegawaian/dp3/update/'.$pegawai_id);
		}
	}

	public function getDataPegawai()
	{
		$pegawai_id = $this->input->post('pegawai_id');
		$data = array();
		$data['pegawai_penilai'] = $this->dp3_model->getDataPegawai($pegawai_id)->row_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}	
	public function getDataListPegawai($page=0)
	{
		$search = $this->input->get('q');
		$page = $this->input->get('page');
		$pegawai_id_selected = $this->input->get('pegawai_id_selected');
		
		$data = array();
		$data['total_count'] = count($this->dp3_model->getDataListPegawaiCount($search,$pegawai_id_selected)->result_array());
		$data['incomplete_results'] = false;
		$getDataListPegawai = $this->dp3_model->getDataListPegawai($search,$page,$pegawai_id_selected)->result_array();
		$data['items'] = array();
		$index = 0;
		foreach ($getDataListPegawai as $dlp) {
			$data['items'][$index]["id"] = $dlp["pegawai_id"];
			$data['items'][$index]["pegawai_id"] = $dlp["pegawai_id"];
			$data['items'][$index]["pegawai_nip"] = $dlp["pegawai_nip"];
			$data['items'][$index]["pegawai_nama_lengkap"] = $dlp["pegawai_nama_lengkap"];
			$data['items'][$index]["pegawai_nama_jabatan"] = $dlp["pegawai_nama_jabatan"];
			$data['items'][$index]["golongan_nama"] = $dlp["golongan_nama"];
			$data['items'][$index]["unit_kerja_nama"] = $dlp["unit_kerja_nama"];
			$data['items'][$index]["pegawai_image_path"] = $dlp["pegawai_image_path"];

			$index++;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}	

}