<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wafat extends CI_Controller{

	function __construct(){
		parent:: __construct();
		$this->load->model('kepegawaian/wafat_model');
		//$this->load->library('access');
		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index(){
		if($this->access->permission('read')){
			$data = array();
			$this->template->display('kepegawaian/wafat/index', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function update($pegawai_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];
				$pegawai_tanggal_wafat = $post['pegawai_tanggal_wafat'];
				$pegawai_faktor_wafat = $post['pegawai_faktor_wafat'];
				$pegawai_id = $post['pegawai_id'];
				$pegawai_nip = $post['pegawai_nip'];
				
				if($isUpdate == 0){
					$dataupdate = array(
						'pegawai_tanggal_wafat' 			=> $pegawai_tanggal_wafat,
						'pegawai_faktor_wafat' 				=> $pegawai_faktor_wafat,
						'pegawai_telah_wafat' 				=> 1,
						'pegawai_status_pegawai_id' 		=> 1,
						'pegawai_create_by' 				=> $this->session->userdata('user_id'),
						'pegawai_create_date' 				=> date('Y-m-d H:i:s')
					);

					$insDb = $this->wafat_model->update_wafat($dataupdate, $pegawai_id);

					if($insDb > 0){
						$notify = array(
							'title' 	=> 'Berhasil!',
							'message' 	=> 'Penambahan Data Kemampuan Bahasa Berhasil',
							'status' 	=> 'success'
						);
						$this->session->set_flashdata('notify', $notify);

						redirect(base_url().'kepegawaian/wafat/update/'.$pegawai_id);
					}else{
						$notify = array(
							'title' 	=> 'Gagal!',
							'message'	=> 'Penambahan Data Kemampuan Bahasa Gagal, silahkan coba lagi',
							'status' 	=> 'error'
						);
						$this->session->set_flashdata('notify', $notify);
						redirect(base_url().'kepegawaian/wafat/update/'.$pegawai_id);
					}
				}
			}

			$data = array();
			$data['pegawai']  	= $this->wafat_model->getPegawai($pegawai_id)->row_array();
			$data['history']  	= $this->wafat_model->getListwafat($pegawai_id)->result_array();
			$this->template->display('kepegawaian/wafat/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	
	public function list_data_aktif(){
		$default_order = "";
		$limit = 10;
		$where = $this->session->userdata('filter_where');
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama',
			'pegawai_nama_jabatan',
			'unit_kerja_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->wafat_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->wafat_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->wafat_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_nama_jabatan"],
				$row["unit_kerja_nama"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/wafat/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
}
