<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pegawai_jfu extends CI_Controller{

	function __construct(){
		parent:: __construct();
		$this->load->model('kepegawaian/data_pribadi_model');
		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index(){
		if($this->access->permission('read')){
			$pegawai_id = $this->session->userdata('pegawai_id');
			$data['data'] = $this->data_pribadi_model->getDataPribadi($pegawai_id)->row_array();
			// echo "<pre>";
			// print_r($data);die;
			$this->template->display('kepegawaian/pegawai_jfu/index', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function update(){
            if($post = $this->input->post()){
				$pegawai_id = $this->session->userdata('pegawai_id');
                $dataupdate = array(
                    'pegawai_handphone'     => isset($post['pegawai_handphone'])?$post['pegawai_handphone']:'',
                    'pegawai_nama_kontak_darurat'     => isset($post['pegawai_nama_kontak_darurat'])?$post['pegawai_nama_kontak_darurat']:'',
                    'pegawai_no_kontak_darurat'     => isset($post['pegawai_no_kontak_darurat'])?$post['pegawai_no_kontak_darurat']:'',
                    'pegawai_email'      	=> isset($post['pegawai_email'])?$post['pegawai_email']:'',
                    'pegawai_nomor_ktp'     => isset($post['pegawai_nomor_ktp'])?$post['pegawai_nomor_ktp']:'',
                    'pegawai_nomor_npwp'    => isset($post['pegawai_nomor_npwp'])?$post['pegawai_nomor_npwp']:'',
                    'pegawai_nomor_npwp'    => isset($post['pegawai_nomor_npwp'])?$post['pegawai_nomor_npwp']:'',
					'pegawai_sinkronisasi' => 0
                );

                
                $insDb = $this->data_pribadi_model->updatedatapribadi($dataupdate, $pegawai_id);

                if($insDb > 0){
                    $notify = array(
                        'title'     => 'Berhasil!',
                        'message'   => 'Perubahan Data Pribadi Berhasil',
                        'status'    => 'success'
                    );
                    $this->session->set_flashdata('notify', $notify);

                    redirect(base_url().'kepegawaian/pegawai_jfu/index');
                }else{
                    $notify = array(
                        'title'     => 'Gagal!',
                        'message'   => 'Perubahan Data Pribadi gagal, silahkan coba lagi',
                        'status'    => 'error'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    redirect(base_url().'kepegawaian/pegawai_jfu/index');
                }
            }
    }

    public function downloadapk(){
        $this->load->helper('download');
        $file = 'assets/elayar.apk';
        force_download($file, NULL);
    }
	
}