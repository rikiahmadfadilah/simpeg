<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suami_istri extends CI_Controller {

	public function __construct(){
		parent::__construct();
		// $this->load->model('auth/auth_model','',TRUE);
		// $this->load->model('kepegawaian/pns/pns_model','',TRUE);
		$this->load->model('kepegawaian/suami_istri_model','',TRUE);
		//$this->load->model('kepegawaian/suami_istri/pns_model','',TRUE);

		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index()
	{
		if($this->session->userdata('user_akses_id') == 3){
			redirect('kepegawaian/suami_istri/update/'.$this->session->userdata('pegawai_id'));
		}else{
			if($this->access->permission('read')){
				$data = array();
				$this->template->display('kepegawaian/suami_istri/index', $data);
			}else{
				$this->access->redirect('404');
			}
		}
		// $data = array();
		// $this->template->display('kepegawaian/suami_istri/index', $data);	
	}

	public function update($pegawai_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];

				$suami_istri_id	= $post['suami_istri_id'];
				$suami_istri_pegawai_id	= $post['suami_istri_pegawai_id'];
				$suami_istri_pegawai_nip	= $post['suami_istri_pegawai_nip'];
				$suami_istri_jenis	= $post['suami_istri_jenis'];
				$suami_istri_nama	= $post['suami_istri_nama'];
				$suami_istri_no_karis_karsu	= $post['suami_istri_no_karis_karsu'];
				$suami_istri_tanggal_lahir	= $post['suami_istri_tanggal_lahir'];
				$suami_istri_tanggal_nikah	= $post['suami_istri_tanggal_nikah'];
				$suami_istri_pendidikan_id	= $post['suami_istri_pendidikan_id'];
				$suami_istri_pekerjaan	= $post['suami_istri_pekerjaan'];
				$suami_istri_jenis_status	= $post['suami_istri_jenis_status'];
				$suami_istri_tanggal_status	= isset($post['suami_istri_tanggal_status'])?$post['suami_istri_tanggal_status']:null;
				$suami_istri_sk_cerai_wafat	= isset($post['suami_istri_sk_cerai_wafat'])?$post['suami_istri_sk_cerai_wafat']:null;
				// $suami_istri_create_by	= $post['suami_istri_create_by'];
				// $suami_istri_create_date	= $post['suami_istri_create_date'];
				$suami_istri_status	= 1;

				if($isUpdate == 0){
					$datacreate = array(					
					'suami_istri_pegawai_id' => $suami_istri_pegawai_id,
					'suami_istri_pegawai_nip' => $suami_istri_pegawai_nip,
					'suami_istri_jenis' => $suami_istri_jenis,
					'suami_istri_nama' => $suami_istri_nama,
					'suami_istri_no_karis_karsu' => $suami_istri_no_karis_karsu,
					'suami_istri_tanggal_lahir' => $suami_istri_tanggal_lahir,
					'suami_istri_tanggal_nikah' => $suami_istri_tanggal_nikah,
					'suami_istri_pendidikan_id' => $suami_istri_pendidikan_id,
					'suami_istri_pekerjaan' => $suami_istri_pekerjaan,
					'suami_istri_jenis_status' => $suami_istri_jenis_status,
					'suami_istri_tanggal_status' => ($suami_istri_tanggal_status != '')?$suami_istri_tanggal_status:null,
					'suami_istri_sk_cerai_wafat' => ($suami_istri_sk_cerai_wafat != '')?$suami_istri_sk_cerai_wafat:null,
					'suami_istri_create_by' => $this->session->userdata('user_id'),
					'suami_istri_create_date' => date('Y-m-d H:i:s'),
					'suami_istri_status' => $suami_istri_status,
					'suami_istri_sinkronisasi' => 0
					);
								
					
					$insDb = $this->suami_istri_model->create_suami_istri($datacreate);

					if($insDb > 0){
						$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["suami_istri_pegawai_nip"])."','3','1C')");
						$notify = array(
							'title' 	=> 'Berhasil!',
							'message' 	=> 'Penambahan Data Pendidikan Berhasil',
							'status' 	=> 'success'
						);
						$this->session->set_flashdata('notify', $notify);

						redirect(base_url().'kepegawaian/suami_istri/update/'.$suami_istri_pegawai_id);
					}else{
						$notify = array(
							'title' 	=> 'Gagal!',
							'message'	=> 'Penambahan Data Pendidikan Gagal, silahkan coba lagi',
							'status' 	=> 'error'
						);
						$this->session->set_flashdata('notify', $notify);
						redirect(base_url().'kepegawaian/suami_istri/update/'.$suami_istri_pegawai_id);
					}
				}else{
					$dataupdate = array(
					// 'suami_istri_pegawai_id' => $suami_istri_pegawai_id,
					// 'suami_istri_pegawai_nip' => $suami_istri_pegawai_nip,
					'suami_istri_jenis' => $suami_istri_jenis,
					'suami_istri_nama' => $suami_istri_nama,
					'suami_istri_no_karis_karsu' => $suami_istri_no_karis_karsu,
					'suami_istri_tanggal_lahir' => $suami_istri_tanggal_lahir,
					'suami_istri_tanggal_nikah' => $suami_istri_tanggal_nikah,
					'suami_istri_pendidikan_id' => $suami_istri_pendidikan_id,
					'suami_istri_pekerjaan' => $suami_istri_pekerjaan,
					'suami_istri_jenis_status' => $suami_istri_jenis_status,
					'suami_istri_tanggal_status' => ($suami_istri_tanggal_status != '')?$suami_istri_tanggal_status:null,
					'suami_istri_sk_cerai_wafat' => ($suami_istri_sk_cerai_wafat != '')?$suami_istri_sk_cerai_wafat:null,
					'suami_istri_create_by' => $this->session->userdata('user_id'),
					'suami_istri_create_date' => date('Y-m-d H:i:s'),
					'suami_istri_status' => $suami_istri_status,
					'suami_istri_sinkronisasi' => 0
					);
					
					$this->suami_istri_model->update_suami_istri($dataupdate, $suami_istri_id);
					$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["suami_istri_pegawai_nip"])."','24','2C')");
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan Data Pendidikan Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/suami_istri/update/'.$suami_istri_pegawai_id);
				}
			}

			$data = array();
			$data['pegawai']  	= $this->suami_istri_model->getPegawai($pegawai_id)->row_array();
			$data['history']  	= $this->suami_istri_model->getSuamiIstri($pegawai_id)->result_array();
			$data['pendidikan']  	= $this->suami_istri_model->getPendidikan()->result_array();
			$this->template->display('kepegawaian/suami_istri/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	
	public function list_data_aktif(){
		$default_order = "";
		$limit = 10;
		$operator_unit_kerja = $this->session->userdata('user_operator_unit_kerja_id');
		$operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');

		if($this->session->userdata('user_akses_id') == '2'){
        	$where = "pegawai_status = 1 and pegawai_is_pns = 1 and (pegawai_jenis_pensiun_id is null or pegawai_jenis_pensiun_id = 0) and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
		}
		else{
			$where = "pegawai_status = 1 AND pegawai_is_pns = 1";
		}
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->suami_istri_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->suami_istri_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->suami_istri_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_nama_jabatan"],
				$row["unit_kerja_hirarki_name_full"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/suami_istri/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
	public function hapus($suami_istri_id = 0,$pegawai_id = 0){

		$suami_istri_idFilter = filter_var($suami_istri_id, FILTER_SANITIZE_NUMBER_INT);

		if($this->access->permission('delete')) {
			if($suami_istri_id==$suami_istri_idFilter) {

				$dataupdate = array(
					'suami_istri_status'  => 0,
					'suami_istri_create_by' 			=> $this->session->userdata('user_id'),
					'suami_istri_create_date' 			=> date('Y-m-d H:i:s')
				);
				//print_r($dataupdate);die;
				$del = $this->suami_istri_model->update_suami_istri($dataupdate,$suami_istri_id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Pendidikan Formal Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'kepegawaian/suami_istri/update/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Pendidikan Formal Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'kepegawaian/suami_istri/update/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Pendidikan Formal Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'kepegawaian/suami_istri/update/'.$pegawai_id);
		}
	}
}
