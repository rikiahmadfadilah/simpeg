<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kemampuan_bahasa extends CI_Controller{

	function __construct(){
		parent:: __construct();
		$this->load->model('kepegawaian/kemampuan_bahasa_model');
		//$this->load->library('access');
		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index(){
		if($this->access->permission('read')){
			$data = array();
			$this->template->display('kepegawaian/kemampuan_bahasa/index', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function update($pegawai_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];
				$kemampuan_bahasa_id = $post['kemampuan_bahasa_id'];
				$kemampuan_bahasa_bahasa_id = $post['kemampuan_bahasa_bahasa_id'];
				$kemampuan_bahasa_lain = $post['kemampuan_bahasa_lain'];
				$kemampuan_bahasa_kategori = $post['kemampuan_bahasa_kategori'];
				$kemampuan_bahasa_pegawai_id = $post['kemampuan_bahasa_pegawai_id'];
				$kemampuan_bahasa_pegawai_nip = $post['kemampuan_bahasa_pegawai_nip'];
				
				if($isUpdate == 0){
					$datacreate = array(
						'kemampuan_bahasa_bahasa_id' 			=> $kemampuan_bahasa_bahasa_id,
						'kemampuan_bahasa_lain' 				=> $kemampuan_bahasa_lain,
						'kemampuan_bahasa_kategori' 			=> $kemampuan_bahasa_kategori,
						'kemampuan_bahasa_pegawai_id' 			=> $kemampuan_bahasa_pegawai_id,
						'kemampuan_bahasa_pegawai_nip' 			=> $kemampuan_bahasa_pegawai_nip,
						'kemampuan_bahasa_status' 				=> 1,
						'kemampuan_bahasa_create_by' 			=> $this->session->userdata('user_id'),
						'kemampuan_bahasa_create_date' 			=> date('Y-m-d H:i:s')
					);


					$insDb = $this->kemampuan_bahasa_model->create_kemampuan_bahasa($datacreate);

					if($insDb > 0){
						$notify = array(
							'title' 	=> 'Berhasil!',
							'message' 	=> 'Penambahan Data Kemampuan Bahasa Berhasil',
							'status' 	=> 'success'
						);
						$this->session->set_flashdata('notify', $notify);

						redirect(base_url().'kepegawaian/kemampuan_bahasa/update/'.$kemampuan_bahasa_pegawai_id);
					}else{
						$notify = array(
							'title' 	=> 'Gagal!',
							'message'	=> 'Penambahan Data Kemampuan Bahasa Gagal, silahkan coba lagi',
							'status' 	=> 'error'
						);
						$this->session->set_flashdata('notify', $notify);
						redirect(base_url().'kepegawaian/kemampuan_bahasa/update/'.$kemampuan_bahasa_pegawai_id);
					}
				}else{
					$dataupdate = array(
						'kemampuan_bahasa_bahasa_id' 			=> $kemampuan_bahasa_bahasa_id,
						'kemampuan_bahasa_lain' 				=> $kemampuan_bahasa_lain,
						'kemampuan_bahasa_kategori' 			=> $kemampuan_bahasa_kategori,
						'kemampuan_bahasa_pegawai_id' 			=> $kemampuan_bahasa_pegawai_id,
						'kemampuan_bahasa_pegawai_nip' 			=> $kemampuan_bahasa_pegawai_nip,
						'kemampuan_bahasa_status' 				=> 1,
						'kemampuan_bahasa_create_by' 			=> $this->session->userdata('user_id'),
						'kemampuan_bahasa_create_date' 			=> date('Y-m-d H:i:s')
					);
					
					$this->kemampuan_bahasa_model->update_kemampuan_bahasa($dataupdate, $kemampuan_bahasa_id);

					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan Data Kemampuan Bahasa Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/kemampuan_bahasa/update/'.$kemampuan_bahasa_pegawai_id);
				}
			}

			$data = array();
			$data['pegawai']  	= $this->kemampuan_bahasa_model->getPegawai($pegawai_id)->row_array();
			$data['history']  	= $this->kemampuan_bahasa_model->getListkemampuan_bahasa($pegawai_id)->result_array();
			$data['bahasa']  	= $this->kemampuan_bahasa_model->getBahasa()->result_array();
			$this->template->display('kepegawaian/kemampuan_bahasa/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function get_jurusan()
	{
		$jurusan_kode 	= $this->input->post('jurusan_kode');
		$data["jurusan"] = $this->kemampuan_bahasa_model->getJurusan($jurusan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	

	
	public function list_data_aktif(){
		$default_order = "";
		$limit = 10;
		$operator_unit_kerja = $this->session->userdata('user_operator_unit_kerja_id');
		$operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');

		if($this->session->userdata('user_akses_id') == '2'){
        	$where = "pegawai_status = 1 and pegawai_is_pns = 1 and (pegawai_jenis_pensiun_id is null or pegawai_jenis_pensiun_id = 0) and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
		}
		else{
			$where = "pegawai_status = 1 AND pegawai_is_pns = 1";
		}
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->kemampuan_bahasa_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->kemampuan_bahasa_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->kemampuan_bahasa_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_nama_jabatan"],
				$row["unit_kerja_hirarki_name_full"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/kemampuan_bahasa/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
	public function hapus($kemampuan_bahasa_id = 0,$pegawai_id = 0){

		$kemampuan_bahasa_idFilter = filter_var($kemampuan_bahasa_id, FILTER_SANITIZE_NUMBER_INT);
		if($this->access->permission('delete')) {
			if($kemampuan_bahasa_id==$kemampuan_bahasa_idFilter) {

				$dataupdate = array(
					'kemampuan_bahasa_status'  => 0,
					'kemampuan_bahasa_create_by' 			=> $this->session->userdata('user_id'),
					'kemampuan_bahasa_create_date' 			=> date('Y-m-d H:i:s')
				);

				$del = $this->kemampuan_bahasa_model->update_kemampuan_bahasa($dataupdate,$kemampuan_bahasa_id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'kemampuan_bahasa Formal Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'kepegawaian/kemampuan_bahasa/update/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'kemampuan_bahasa Formal Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'kepegawaian/kemampuan_bahasa/update/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'kemampuan_bahasa Formal Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'kepegawaian/kemampuan_bahasa/update/'.$pegawai_id);
		}
	}

	
}