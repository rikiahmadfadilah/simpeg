<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keluarga extends CI_Controller {

	public function __construct(){
		parent::__construct();
		// $this->load->model('auth/auth_model','',TRUE);
		// $this->load->model('kepegawaian/pns/pns_model','',TRUE);
		$this->load->model('kepegawaian/keluarga_model','',TRUE);
		//$this->load->model('kepegawaian/keluarga/pns_model','',TRUE);

		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index()
	{
		if($this->session->userdata('user_akses_id') == 3){
			redirect('kepegawaian/keluarga/update/'.$this->session->userdata('pegawai_id'));
		}else{
			if($this->access->permission('read')){
				$data = array();
				$this->template->display('kepegawaian/keluarga/index', $data);
			}else{
				$this->access->redirect('404');
			}
		}
		// $data = array();
		// $this->template->display('kepegawaian/keluarga/index', $data);	
	}

	public function update($pegawai_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];

				$keluarga_id =$post['keluarga_id'];
				$keluarga_pegawai_id = $post['keluarga_pegawai_id'];
				$keluarga_pegawai_nip = $post['keluarga_pegawai_nip'];
				$keluarga_nama = $post['keluarga_nama'];
				$keluarga_jenis_kelamin_id = $post['keluarga_jenis_kelamin_id'];
				$keluarga_tanggal_lahir = $post['keluarga_tanggal_lahir'];
				$keluarga_hubungan_keluarga_id = $post['keluarga_hubungan_keluarga_id'];
				$keluarga_pekerjaan = $post['keluarga_pekerjaan'];
				$keluarga_kondisi = $post['keluarga_kondisi'];
				// $keluarga_create_by = $post['keluarga_create_by'];
				// $keluarga_create_date = $post['keluarga_create_date'];
				$keluarga_status = 1;

		

				if($isUpdate == 0){
					$datacreate = array(
						'keluarga_pegawai_id' => $keluarga_pegawai_id,
						'keluarga_pegawai_nip' => $keluarga_pegawai_nip,
						'keluarga_nama' => $keluarga_nama,
						'keluarga_jenis_kelamin_id' => $keluarga_jenis_kelamin_id,
						'keluarga_tanggal_lahir' => $keluarga_tanggal_lahir,
						'keluarga_hubungan_keluarga_id' => $keluarga_hubungan_keluarga_id,
						'keluarga_pekerjaan' => $keluarga_pekerjaan,
						'keluarga_kondisi' => $keluarga_kondisi,
						'keluarga_create_by' => $this->session->userdata('user_id'),
						'keluarga_create_date' => date('Y-m-d H:i:s'),
						'keluarga_status' => $keluarga_status,
						'keluarga_sinkronisasi' => 0
					);				
					
					$insDb = $this->keluarga_model->create_keluarga($datacreate);

					if($insDb > 0){
						$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["keluarga_pegawai_nip"])."','15','1O')");
						$notify = array(
							'title' 	=> 'Berhasil!',
							'message' 	=> 'Penambahan Data Pendidikan Berhasil',
							'status' 	=> 'success'
						);
						$this->session->set_flashdata('notify', $notify);

						redirect(base_url().'kepegawaian/keluarga/update/'.$keluarga_pegawai_id);
					}else{
						$notify = array(
							'title' 	=> 'Gagal!',
							'message'	=> 'Penambahan Data Pendidikan Gagal, silahkan coba lagi',
							'status' 	=> 'error'
						);
						$this->session->set_flashdata('notify', $notify);
						redirect(base_url().'kepegawaian/keluarga/update/'.$keluarga_pegawai_id);
					}
				}else{
					$dataupdate = array(
						// 'keluarga_pegawai_id' => $keluarga_pegawai_id,
						// 'keluarga_pegawai_nip' => $keluarga_pegawai_nip,
						'keluarga_nama' => $keluarga_nama,
						'keluarga_jenis_kelamin_id' => $keluarga_jenis_kelamin_id,
						'keluarga_tanggal_lahir' => $keluarga_tanggal_lahir,
						'keluarga_hubungan_keluarga_id' => $keluarga_hubungan_keluarga_id,
						'keluarga_pekerjaan' => $keluarga_pekerjaan,
						'keluarga_kondisi' => $keluarga_kondisi,
						'keluarga_create_by' => $this->session->userdata('user_id'),
						'keluarga_create_date' => date('Y-m-d H:i:s'),
						'keluarga_status' => $keluarga_status,
						'keluarga_sinkronisasi' => 0

					);	
					$this->keluarga_model->update_keluarga($dataupdate, $keluarga_id);
					$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["keluarga_pegawai_nip"])."','37','2P')");
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan Data Pendidikan Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/keluarga/update/'.$keluarga_pegawai_id);
				}
			}

			$data = array();
			$data['pegawai']  	= $this->keluarga_model->getPegawai($pegawai_id)->row_array();
			$data['history']  	= $this->keluarga_model->getKeluarga($pegawai_id)->result_array();
			$data['listjk']  	= $this->keluarga_model->GetJenisKelamin()->result_array();
			$data['listhk']  	= $this->keluarga_model->GetHubunganKeluarga()->result_array();
			$this->template->display('kepegawaian/keluarga/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	
	public function list_data_aktif(){
		$default_order = "";
		$limit = 10;
		$pegawai = $this->session->userdata('pegawai_nip');
		$operator_unit_kerja = $this->session->userdata('user_operator_unit_kerja_id');
		$operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');

		if($this->session->userdata('user_akses_id') == '2'){
        	$where = "pegawai_status = 1 and pegawai_is_pns = 1 and (pegawai_jenis_pensiun_id is null or pegawai_jenis_pensiun_id = 0) and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
		}
		else{
			$where = "pegawai_status = 1 AND pegawai_is_pns = 1";
		}
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->keluarga_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->keluarga_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->keluarga_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_nama_jabatan"],
				$row["unit_kerja_hirarki_name_full"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/keluarga/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
	public function check_hubungan_keluarga()
    {   
		$data = $this->keluarga_model->check_hubungan_keluarga($this->input->post('keluarga_hubungan_keluarga_id'),$this->input->post('keluarga_pegawai_id'))->result_array();
		$res = true;
		if($this->input->post('isUpdate') == 0){ 
			if($this->input->post('keluarga_hubungan_keluarga_id') <= '3'){
				if(count($data)>0){
					$res = false;
				}
			}
		}
		echo json_encode($res);
    }
	public function hapus($keluarga_id = 0,$pegawai_id = 0){

		$keluarga_idFilter = filter_var($keluarga_id, FILTER_SANITIZE_NUMBER_INT);

		if($this->access->permission('delete')) {
			if($keluarga_id==$keluarga_idFilter) {

				$dataupdate = array(
					'keluarga_status'  => 0,
					'keluarga_create_by' 			=> $this->session->userdata('user_id'),
					'keluarga_create_date' 			=> date('Y-m-d H:i:s')
				);
				//print_r($dataupdate);die;
				$del = $this->keluarga_model->update_keluarga($dataupdate,$keluarga_id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Pendidikan Formal Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'kepegawaian/keluarga/update/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Pendidikan Formal Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'kepegawaian/keluarga/update/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Pendidikan Formal Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'kepegawaian/keluarga/update/'.$pegawai_id);
		}
	}
}
