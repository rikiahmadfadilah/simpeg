<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mutasi_uker extends CI_Controller{

	function __construct(){
		parent:: __construct();
		$this->load->model('kepegawaian/mutasi_uker_model');
		//$this->load->library('access');
		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index(){
		if($this->access->permission('read')){
			$data = array();
			$this->template->display('kepegawaian/mutasi_uker/index', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function update($pegawai_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];
				// $mutasi_uker_unit_kerja_id_sebelum = $post['mutasi_uker_unit_kerja_id_sebelum'];
				// $unit_kerja_id_sebelum = '';
				// for ($i=0; $i < count($mutasi_uker_unit_kerja_id_sebelum); $i++) { 
				// 	if($mutasi_uker_unit_kerja_id_sebelum[$i] != ''){
				// 		$unit_kerja_id_sebelum = $mutasi_uker_unit_kerja_id_sebelum[$i];
				// 	}
				// }
				$mutasi_uker_unit_kerja_id_sesudah = $post['mutasi_uker_unit_kerja_id_sesudah'];
				$unit_kerja_id_sesudah = '';
				for ($i=0; $i < count($mutasi_uker_unit_kerja_id_sesudah); $i++) { 
					if($mutasi_uker_unit_kerja_id_sesudah[$i] != ''){
						$unit_kerja_id_sesudah = $mutasi_uker_unit_kerja_id_sesudah[$i];
					}
				}
				$mutasi_uker_id = $post['mutasi_uker_id'];
				$mutasi_uker_tanggal = $post['mutasi_uker_tanggal'];
				$mutasi_uker_unit_kerja_id_sebelum = $post['mutasi_uker_unit_kerja_id_sebelum'];
				// $mutasi_uker_unit_kerja_id_sesudah = $post['mutasi_uker_unit_kerja_id_sesudah'];
				$mutasi_uker_pegawai_id = $post['mutasi_uker_pegawai_id'];
				$mutasi_uker_pegawai_nip = $post['mutasi_uker_pegawai_nip'];
				
				if($isUpdate == 0){
					$datacreate = array(
						'mutasi_uker_tanggal' 				=> $mutasi_uker_tanggal,
						'mutasi_uker_unit_kerja_id_sebelum' => $mutasi_uker_unit_kerja_id_sebelum,
						'mutasi_uker_unit_kerja_id_sesudah' => $unit_kerja_id_sesudah,
						'mutasi_uker_pegawai_id' 			=> $mutasi_uker_pegawai_id,
						'mutasi_uker_pegawai_nip' 			=> $mutasi_uker_pegawai_nip,
						'mutasi_uker_status' 				=> 1,
						'mutasi_uker_create_by' 			=> $this->session->userdata('user_id'),
						'mutasi_uker_create_date' 			=> date('Y-m-d H:i:s')
					);


					$insDb = $this->mutasi_uker_model->create_mutasi_uker($datacreate);

					$updatepegawai = array(
						'pegawai_unit_kerja_id' 			=> $unit_kerja_id_sesudah,
						'pegawai_status_pegawai_id' 		=> 1,
						'pegawai_create_by' 				=> $this->session->userdata('user_id'),
						'pegawai_create_date' 				=> date('Y-m-d H:i:s')
					);

					$this->mutasi_uker_model->update_pegawai($updatepegawai, $mutasi_uker_pegawai_id);

					if($insDb > 0){
						$notify = array(
							'title' 	=> 'Berhasil!',
							'message' 	=> 'Penambahan Data Kemampuan Bahasa Berhasil',
							'status' 	=> 'success'
						);
						$this->session->set_flashdata('notify', $notify);

						redirect(base_url().'kepegawaian/mutasi_uker/update/'.$mutasi_uker_pegawai_id);
					}else{
						$notify = array(
							'title' 	=> 'Gagal!',
							'message'	=> 'Penambahan Data Kemampuan Bahasa Gagal, silahkan coba lagi',
							'status' 	=> 'error'
						);
						$this->session->set_flashdata('notify', $notify);
						redirect(base_url().'kepegawaian/mutasi_uker/update/'.$mutasi_uker_pegawai_id);
					}
				}else{
					$dataupdate = array(
						'mutasi_uker_tanggal' 				=> $mutasi_uker_tanggal,
						'mutasi_uker_unit_kerja_id_sebelum' => $mutasi_uker_unit_kerja_id_sebelum,
						'mutasi_uker_unit_kerja_id_sesudah' => $unit_kerja_id_sesudah,
						'mutasi_uker_pegawai_id' 			=> $mutasi_uker_pegawai_id,
						'mutasi_uker_pegawai_nip' 			=> $mutasi_uker_pegawai_nip,
						'mutasi_uker_status' 				=> 1,
						'mutasi_uker_create_by' 			=> $this->session->userdata('user_id'),
						'mutasi_uker_create_date' 			=> date('Y-m-d H:i:s')
					);
					
					$this->mutasi_uker_model->update_mutasi_uker($dataupdate, $mutasi_uker_id);

					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan Data Kemampuan Bahasa Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/mutasi_uker/update/'.$mutasi_uker_pegawai_id);
				}
			}

			$data = array();
			$data['pegawai']  	= $this->mutasi_uker_model->getPegawai($pegawai_id)->row_array();
			$data['history']  	= $this->mutasi_uker_model->getListmutasi_uker($pegawai_id)->result_array();
			$data["unit_kerja"] = $this->mutasi_uker_model->getUnitKerja(1,2)->result_array();
			$data["jabatan"] 	= $this->mutasi_uker_model->getJabatan()->result_array();
			// $data['unit_kerja'] = $this->mutasi_uker_model->getUnitKerja()->result_array();
			$this->template->display('kepegawaian/mutasi_uker/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}

	// public function get_unit_kerja_hirarki()
	// {
	// 	$unit_kerja_parent_id_kode 	= $this->input->post('unit_kerja_parent_id_kode');
	// 	$unit_kerja_level 	= $this->input->post('unit_kerja_level');
	// 	$data['unit_kerja'] = $this->mutasi_uker_model->getUnitKerja($unit_kerja_parent_id_kode,$unit_kerja_level)->result_array();
	// 	$this->output->set_content_type('application/json')->set_output(json_encode($data));
	// }
	
	public function list_data_aktif(){
		$default_order = "";
		$limit = 10;
		$operator_unit_kerja = $this->session->userdata('user_operator_unit_kerja_id');
		$operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');

		if($this->session->userdata('user_akses_id') == '2'){
        	$where = "pegawai_status = 1 and pegawai_is_pns = 1 and (pegawai_jenis_pensiun_id is null or pegawai_jenis_pensiun_id = 0) and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
		}
		else{
			$where = "pegawai_status = 1 AND pegawai_is_pns = 1";
		}
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->mutasi_uker_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->mutasi_uker_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->mutasi_uker_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_nama_jabatan"],
				$row["unit_kerja_hirarki_name_full"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/mutasi_uker/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
	public function hapus($mutasi_uker_id = 0,$pegawai_id = 0){

		$mutasi_uker_idFilter = filter_var($mutasi_uker_id, FILTER_SANITIZE_NUMBER_INT);
		if($this->access->permission('delete')) {
			if($mutasi_uker_id==$mutasi_uker_idFilter) {

				$dataupdate = array(
					'mutasi_uker_status'  => 0,
					'mutasi_uker_create_by' 			=> $this->session->userdata('user_id'),
					'mutasi_uker_create_date' 			=> date('Y-m-d H:i:s')
				);

				$del = $this->mutasi_uker_model->update_mutasi_uker($dataupdate,$mutasi_uker_id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'mutasi_uker Formal Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'kepegawaian/mutasi_uker/update/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'mutasi_uker Formal Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'kepegawaian/mutasi_uker/update/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'mutasi_uker Formal Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'kepegawaian/mutasi_uker/update/'.$pegawai_id);
		}
	}

	public function get_unit_kerja_hirarki()
	{
		$unit_kerja_parent_id_kode 	= $this->input->post('unit_kerja_parent_id_kode');
		$unit_kerja_level 	= $this->input->post('unit_kerja_level');
		$data["unit_kerja"] = $this->mutasi_uker_model->getUnitKerja($unit_kerja_parent_id_kode,$unit_kerja_level)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}

	
}