<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Diklat_ujiandinas extends CI_Controller{

	function __construct(){
		parent:: __construct();
		$this->load->model('kepegawaian/diklat_ujiandinas_model');
		//$this->load->library('access');
		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index(){
		if($this->access->permission('read')){
			$data = array();
			$this->template->display('kepegawaian/diklat_ujiandinas/index', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function update($pegawai_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];
				$diklat_id = $post['diklat_id'];

				$diklat_pegawai_id = $post['diklat_pegawai_id'];
				$diklat_pegawai_nip = $post['diklat_pegawai_nip'];

				$diklat_pegawai_id = $post['diklat_pegawai_id'];
				$diklat_pegawai_nip = $post['diklat_pegawai_nip'];
				$diklat_type = 2;//$post['diklat_type'];
				$diklat_jenis_id = $post['diklat_jenis_id'];
				$diklat_angkatan = $post['diklat_angkatan'];
				$diklat_penyelenggara = $post['diklat_penyelenggara'];
				$diklat_jumlah_jam = $post['diklat_jumlah_jam'];
				$diklat_tanggal_mulai = $post['diklat_tanggal_mulai'];
				$diklat_tanggal_selesai = $post['diklat_tanggal_selesai'];
				$diklat_predikat = $post['diklat_predikat'];
				$diklat_lokasi = $post['diklat_lokasi'];
				$diklat_nomor_sertifikat = $post['diklat_nomor_sertifikat'];
				$diklat_tanggal_sertifikat = $post['diklat_tanggal_sertifikat'];
				// $diklat_create_by = $post['diklat_create_by'];
				// $diklat_create_date = $post['diklat_create_date'];
				// $diklat_status = $post['diklat_status'];
				$diklat_nama = $post['diklat_nama'];
							
				if($isUpdate == 0){
					$datacreate = array(
						'diklat_pegawai_id' => $diklat_pegawai_id,
						'diklat_pegawai_nip' => $diklat_pegawai_nip,
						'diklat_type' => $diklat_type,
						'diklat_jenis_id' => $diklat_jenis_id,
						'diklat_angkatan' => $diklat_angkatan,
						'diklat_penyelenggara' => $diklat_penyelenggara,
						'diklat_jumlah_jam' => $diklat_jumlah_jam,
						'diklat_tanggal_mulai' => $diklat_tanggal_mulai,
						'diklat_tanggal_selesai' => $diklat_tanggal_selesai,
						'diklat_predikat' => $diklat_predikat,
						'diklat_lokasi' => $diklat_lokasi,
						'diklat_nomor_sertifikat' => $diklat_nomor_sertifikat,
						'diklat_tanggal_sertifikat' => $diklat_tanggal_sertifikat,
						'diklat_create_by' => $this->session->userdata('user_id'),
						'diklat_create_date' => date('Y-m-d H:i:s'),
						'diklat_status' => 1,
						'diklat_nama' => $diklat_nama
					);
					
					$insDb = $this->diklat_ujiandinas_model->create_diklat($datacreate);

					if($insDb > 0){
						$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["diklat_pegawai_nip"])."','8','1H')");
						$notify = array(
							'title' 	=> 'Berhasil!',
							'message' 	=> 'Penambahan Data Pendidikan Berhasil',
							'status' 	=> 'success'
						);
						$this->session->set_flashdata('notify', $notify);

						redirect(base_url().'kepegawaian/diklat_ujiandinas/update/'.$diklat_pegawai_id);
					}else{
						$notify = array(
							'title' 	=> 'Gagal!',
							'message'	=> 'Penambahan Data Pendidikan Gagal, silahkan coba lagi',
							'status' 	=> 'error'
						);
						$this->session->set_flashdata('notify', $notify);
						redirect(base_url().'kepegawaian/diklat_ujiandinas/update/'.$diklat_pegawai_id);
					}
				}else{
					$dataupdate = array(
						'diklat_pegawai_id' => $diklat_pegawai_id,
						'diklat_pegawai_nip' => $diklat_pegawai_nip,
						'diklat_type' => $diklat_type,
						'diklat_jenis_id' => $diklat_jenis_id,
						'diklat_angkatan' => $diklat_angkatan,
						'diklat_penyelenggara' => $diklat_penyelenggara,
						'diklat_jumlah_jam' => $diklat_jumlah_jam,
						'diklat_tanggal_mulai' => $diklat_tanggal_mulai,
						'diklat_tanggal_selesai' => $diklat_tanggal_selesai,
						'diklat_predikat' => $diklat_predikat,
						'diklat_lokasi' => $diklat_lokasi,
						'diklat_nomor_sertifikat' => $diklat_nomor_sertifikat,
						'diklat_tanggal_sertifikat' => $diklat_tanggal_sertifikat,
						'diklat_create_by' => $this->session->userdata('user_id'),
						'diklat_create_date' => date('Y-m-d H:i:s'),
						'diklat_status' => 1,
						'diklat_nama' => $diklat_nama
					);
					$this->diklat_ujiandinas_model->update_diklat($dataupdate, $diklat_id);
					$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["diklat_pegawai_nip"])."','45','2X')");
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan Data Pendidikan Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/diklat_ujiandinas/update/'.$diklat_pegawai_id);
				}
			}

			$data = array();
			$data['pegawai']  	= $this->diklat_ujiandinas_model->getPegawai($pegawai_id)->row_array();
			$data['history']  	= $this->diklat_ujiandinas_model->getDiklatUjianDinas($pegawai_id)->result_array();
			$data['jenisdiklat'] = $this->diklat_ujiandinas_model->getJenisDiklat($pegawai_id)->result_array();
			//$data['pendidikan']  	= $this->diklat_ujiandinas_model->getPendidikan()->result_array();
			$this->template->display('kepegawaian/diklat_ujiandinas/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function get_tingkat_pendidikan()
	{
		$pendidikan_kode 	= $this->input->post('pendidikan_kode');
		$data["tingkat_pendidikan"] = $this->diklat_ujiandinas_model->getTingkatPendidikan($pendidikan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_fakultas()
	{
		$jurusan_kode 	= $this->input->post('jurusan_kode');
		$data["jurusan"] = $this->diklat_ujiandinas_model->getFakultas($jurusan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_jurusan()
	{
		$jurusan_kode 	= $this->input->post('jurusan_kode');
		$data["jurusan"] = $this->diklat_ujiandinas_model->getJurusan($jurusan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	
	public function list_data_aktif(){
		$default_order = "";
		$limit = 10;
		$operator_unit_kerja = $this->session->userdata('user_operator_unit_kerja_id');
		$operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');

		if($this->session->userdata('user_akses_id') == '2'){
        	$where = "pegawai_status = 1 and pegawai_is_pns = 1 and (pegawai_jenis_pensiun_id is null or pegawai_jenis_pensiun_id = 0) and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
		}
		else{
			$where = "pegawai_status = 1 AND pegawai_is_pns = 1";
		}
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->diklat_ujiandinas_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->diklat_ujiandinas_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->diklat_ujiandinas_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_nama_jabatan"],
				$row["unit_kerja_hirarki_name_full"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/diklat_ujiandinas/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
	public function hapus($diklat_id = 0,$pegawai_id = 0){

		$diklat_idFilter = filter_var($diklat_id, FILTER_SANITIZE_NUMBER_INT);

		if($this->access->permission('delete')) {
			if($diklat_id==$diklat_idFilter) {

				$dataupdate = array(
					'diklat_status'  => 0,
					'diklat_create_by' 			=> $this->session->userdata('user_id'),
					'diklat_create_date' 			=> date('Y-m-d H:i:s')
				);
				//print_r($dataupdate);die;
				$del = $this->diklat_ujiandinas_model->update_diklat($dataupdate,$diklat_id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Pendidikan Formal Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'kepegawaian/diklat_ujiandinas/update/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Pendidikan Formal Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'kepegawaian/diklat_ujiandinas/update/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Pendidikan Formal Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'kepegawaian/diklat_ujiandinas/update/'.$pegawai_id);
		}
	}

	
}