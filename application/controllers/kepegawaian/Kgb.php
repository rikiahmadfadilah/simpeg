<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kgb extends CI_Controller{

	function __construct(){
		parent:: __construct();
		$this->load->model('kepegawaian/kgb_model');
		//$this->load->library('access');
		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index(){
		if($this->access->permission('read')){
			$data = array();
			$this->template->display('kepegawaian/kgb/index', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function update($pegawai_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];
				$kgb_id = $post['kgb_id'];
				$kgb_tanggal = $post['kgb_tanggal'];
				$kgb_gaji_sebelum = $post['kgb_gaji_sebelum'];
				$kgb_gaji_sesudah = $post['kgb_gaji_sesudah'];
				$kgb_pegawai_id = $post['kgb_pegawai_id'];
				$kgb_pegawai_nip = $post['kgb_pegawai_nip'];
				
				if($isUpdate == 0){
					$datacreate = array(
						'kgb_tanggal' 				=> $kgb_tanggal,
						'kgb_gaji_sebelum' 			=> $kgb_gaji_sebelum,
						'kgb_gaji_sesudah' 			=> $kgb_gaji_sesudah,
						'kgb_pegawai_id' 			=> $kgb_pegawai_id,
						'kgb_pegawai_nip' 			=> $kgb_pegawai_nip,
						'kgb_status' 				=> 1,
						'kgb_create_by' 			=> $this->session->userdata('user_id'),
						'kgb_create_date' 			=> date('Y-m-d H:i:s')
					);


					$insDb = $this->kgb_model->create_kgb($datacreate);

					if($insDb > 0){
						$notify = array(
							'title' 	=> 'Berhasil!',
							'message' 	=> 'Penambahan Data Kemampuan Bahasa Berhasil',
							'status' 	=> 'success'
						);
						$this->session->set_flashdata('notify', $notify);

						redirect(base_url().'kepegawaian/kgb/update/'.$kgb_pegawai_id);
					}else{
						$notify = array(
							'title' 	=> 'Gagal!',
							'message'	=> 'Penambahan Data Kemampuan Bahasa Gagal, silahkan coba lagi',
							'status' 	=> 'error'
						);
						$this->session->set_flashdata('notify', $notify);
						redirect(base_url().'kepegawaian/kgb/update/'.$kgb_pegawai_id);
					}
				}else{
					$dataupdate = array(
						'kgb_tanggal' 				=> $kgb_tanggal,
						'kgb_gaji_sebelum' 			=> $kgb_gaji_sebelum,
						'kgb_gaji_sesudah' 			=> $kgb_gaji_sesudah,
						'kgb_pegawai_id' 			=> $kgb_pegawai_id,
						'kgb_pegawai_nip' 			=> $kgb_pegawai_nip,
						'kgb_status' 				=> 1,
						'kgb_create_by' 			=> $this->session->userdata('user_id'),
						'kgb_create_date' 			=> date('Y-m-d H:i:s')
					);
					
					$this->kgb_model->update_kgb($dataupdate, $kgb_id);

					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan Data Kemampuan Bahasa Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/kgb/update/'.$kgb_pegawai_id);
				}
			}

			$data = array();
			$data['pegawai']  	= $this->kgb_model->getPegawai($pegawai_id)->row_array();
			$data['history']  	= $this->kgb_model->getListkgb($pegawai_id)->result_array();
			$this->template->display('kepegawaian/kgb/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	
	public function list_data_aktif(){
		$default_order = "";
		$limit = 10;
		$operator_unit_kerja = $this->session->userdata('user_operator_unit_kerja_id');
		$operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');

		if($this->session->userdata('user_akses_id') == '2'){
        	$where = "pegawai_status = 1 and pegawai_is_pns = 1 and (pegawai_jenis_pensiun_id is null or pegawai_jenis_pensiun_id = 0) and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
		}
		else{
			$where = "pegawai_status = 1 AND pegawai_is_pns = 1";
		}
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->kgb_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->kgb_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->kgb_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_nama_jabatan"],
				$row["unit_kerja_hirarki_name_full"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/kgb/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
	public function hapus($kgb_id = 0,$pegawai_id = 0){

		$kgb_idFilter = filter_var($kgb_id, FILTER_SANITIZE_NUMBER_INT);
		if($this->access->permission('delete')) {
			if($kgb_id==$kgb_idFilter) {

				$dataupdate = array(
					'kgb_status'  => 0,
					'kgb_create_by' 			=> $this->session->userdata('user_id'),
					'kgb_create_date' 			=> date('Y-m-d H:i:s')
				);

				$del = $this->kgb_model->update_kgb($dataupdate,$kgb_id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'kgb Formal Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'kepegawaian/kgb/update/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'kgb Formal Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'kepegawaian/kgb/update/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'kgb Formal Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'kepegawaian/kgb/update/'.$pegawai_id);
		}
	}

	
}