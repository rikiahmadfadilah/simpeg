<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kepangkatan extends CI_Controller {

	public function __construct(){
		parent::__construct();
		// $this->load->model('auth/auth_model','',TRUE);
		$this->load->model('kepegawaian/pns/pns_model','',TRUE);
		$this->load->model('kepegawaian/kepangkatan_model','',TRUE);
		//$this->load->model('kepegawaian/kepangkatan/pns_model','',TRUE);

		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index()
	{
		$data = array();
		$this->template->display('kepegawaian/kepangkatan/index', $data);	
	}

	public function update($pegawai_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];

				$kepangkatan_id = $post['kepangkatan_id'];
				$kepangkatan_pegawai_id = $post['kepangkatan_pegawai_id'];
				$kepangkatan_pegawai_nip = $post['kepangkatan_pegawai_nip'];
				$kepangkatan_golongan_id = $post['kepangkatan_golongan_id'];
				$kepangkatan_tanggal_tmt_golongan = $post['kepangkatan_tanggal_tmt_golongan'];
				$kepangkatan_nama_pejabat = $post['kepangkatan_nama_pejabat'];
				$kepangkatan_nomor_sk = $post['kepangkatan_nomor_sk'];
				$kepangkatan_tanggal_sk = $post['kepangkatan_tanggal_sk'];
				$kepangkatan_tahun_masa_kerja_golongan = $post['kepangkatan_tahun_masa_kerja_golongan'];
				$kepangkatan_bulan_masa_kerja_golongan = $post['kepangkatan_bulan_masa_kerja_golongan'];
				$kepangkatan_jenis_sk = $post['kepangkatan_jenis_sk'];
				// $pegawai_cpns_golongan_id = $post['pegawai_cpns_golongan_id'];
				// $pegawai_cpns_tanggal_tmt = $post['pegawai_cpns_tanggal_tmt'];
				$kepangkatan_status = 1;

				if($isUpdate == 0){
					$datacreate = array(
						'kepangkatan_pegawai_id' => $kepangkatan_pegawai_id,
						'kepangkatan_pegawai_nip' => $kepangkatan_pegawai_nip,
						'kepangkatan_golongan_id' => $kepangkatan_golongan_id,
						'kepangkatan_tanggal_tmt_golongan' => $kepangkatan_tanggal_tmt_golongan,
						'kepangkatan_nama_pejabat' => $kepangkatan_nama_pejabat,
						'kepangkatan_nomor_sk' => $kepangkatan_nomor_sk,
						'kepangkatan_tanggal_sk' => $kepangkatan_tanggal_sk,
						'kepangkatan_tahun_masa_kerja_golongan' => $kepangkatan_tahun_masa_kerja_golongan,
						'kepangkatan_bulan_masa_kerja_golongan' => $kepangkatan_bulan_masa_kerja_golongan,
						'kepangkatan_create_date' => date('Y-m-d H:i:s'),
						'kepangkatan_create_by' => $this->session->userdata('user_id'),
						'kepangkatan_status' => $kepangkatan_status,
						'kepangkatan_jenis_sk' => $kepangkatan_jenis_sk
					);					
					
					$insDb = $this->kepangkatan_model->create_kepangkatan($datacreate);
					$kepangkatan_last = $this->kepangkatan_model->getKepangkatanLast($pegawai_id)->row_array();
					$update_pegawai = array(
						'pegawai_golongan_id' => $kepangkatan_last['kepangkatan_golongan_id'],
						'pegawai_tanggal_tmt' => $kepangkatan_last['kepangkatan_tanggal_tmt_golongan'],
						'pegawai_tahun_masa_kerja_golongan' => $kepangkatan_last['kepangkatan_tahun_masa_kerja_golongan'],
						'pegawai_bulan_masa_kerja_golongan' => $kepangkatan_last['kepangkatan_bulan_masa_kerja_golongan'],
						// 'pegawai_cpns_golongan_id' => $pegawai_cpns_golongan_id,
						// 'pegawai_cpns_tanggal_tmt' => $pegawai_cpns_tanggal_tmt,
						'pegawai_sinkronisasi' => 0
					);
					if($kepangkatan_jenis_sk == '1'){
						$this->db->query("UPDATE ms_pegawai SET pegawai_cpns_tanggal_tmt=".$kepangkatan_tanggal_tmt_golongan.", pegawai_cpns_golongan_id=".$kepangkatan_golongan_id." WHERE pegawai_id =".$kepangkatan_pegawai_id);
					}else{}
					$this->pns_model->updatepegawai($update_pegawai, $pegawai_id);
					if($insDb > 0){
						$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["kepangkatan_pegawai_nip"])."','13','1M')");
						$notify = array(
							'title' 	=> 'Berhasil!',
							'message' 	=> 'Penambahan Data Pendidikan Berhasil',
							'status' 	=> 'success'
						);
						$this->session->set_flashdata('notify', $notify);

						redirect(base_url().'kepegawaian/kepangkatan/update/'.$kepangkatan_pegawai_id);
					}else{
						$notify = array(
							'title' 	=> 'Gagal!',
							'message'	=> 'Penambahan Data Pendidikan Gagal, silahkan coba lagi',
							'status' 	=> 'error'
						);
						$this->session->set_flashdata('notify', $notify);
						redirect(base_url().'kepegawaian/kepangkatan/update/'.$kepangkatan_pegawai_id);
					}
				}else{
					$dataupdate = array(
						'kepangkatan_pegawai_id' => $kepangkatan_pegawai_id,
						'kepangkatan_pegawai_nip' => $kepangkatan_pegawai_nip,
						'kepangkatan_golongan_id' => $kepangkatan_golongan_id,
						'kepangkatan_tanggal_tmt_golongan' => $kepangkatan_tanggal_tmt_golongan,
						'kepangkatan_nama_pejabat' => $kepangkatan_nama_pejabat,
						'kepangkatan_nomor_sk' => $kepangkatan_nomor_sk,
						'kepangkatan_tanggal_sk' => $kepangkatan_tanggal_sk,
						'kepangkatan_tahun_masa_kerja_golongan' => $kepangkatan_tahun_masa_kerja_golongan,
						'kepangkatan_bulan_masa_kerja_golongan' => $kepangkatan_bulan_masa_kerja_golongan,
						'kepangkatan_create_date' => date('Y-m-d H:i:s'),
						'kepangkatan_create_by' => $this->session->userdata('user_id'),
						'kepangkatan_status' => $kepangkatan_status,
						'kepangkatan_jenis_sk' => $kepangkatan_jenis_sk
					);
					$this->kepangkatan_model->update_kepangkatan($dataupdate, $kepangkatan_id);
					$kepangkatan_last = $this->kepangkatan_model->getKepangkatanLast($pegawai_id)->row_array();
					$update_pegawai = array(
						'pegawai_golongan_id' => $kepangkatan_last['kepangkatan_golongan_id'],
						'pegawai_tanggal_tmt' => $kepangkatan_last['kepangkatan_tanggal_tmt_golongan'],
						'pegawai_tahun_masa_kerja_golongan' => $kepangkatan_last['kepangkatan_tahun_masa_kerja_golongan'],
						'pegawai_bulan_masa_kerja_golongan' => $kepangkatan_last['kepangkatan_bulan_masa_kerja_golongan'],
						// 'pegawai_cpns_golongan_id' => $pegawai_cpns_golongan_id,
						// 'pegawai_cpns_tanggal_tmt' => $pegawai_cpns_tanggal_tmt,
						'pegawai_sinkronisasi' => 0
					);
					// echo "<pre>";
					// print_r($kepangkatan_jenis_sk);
					// die;
					if($kepangkatan_jenis_sk == '1'){
						$this->db->query("UPDATE ms_pegawai SET pegawai_cpns_tanggal_tmt='".$kepangkatan_tanggal_tmt_golongan."', pegawai_cpns_golongan_id=".$kepangkatan_golongan_id." WHERE pegawai_id =".$kepangkatan_pegawai_id);
					}else{}
					$this->pns_model->updatepegawai($update_pegawai, $pegawai_id);
					$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["kepangkatan_pegawai_nip"])."','29','2H')");
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan Data Pendidikan Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/kepangkatan/update/'.$kepangkatan_pegawai_id);
				}
			}

			$data = array();
			$data['pegawai']  	= $this->kepangkatan_model->getPegawai($pegawai_id)->row_array();
			$data['history']  	= $this->kepangkatan_model->getKepangkatan($pegawai_id)->result_array();
			$data['golongan']  	= $this->kepangkatan_model->getGolongan()->result_array();
			$this->template->display('kepegawaian/kepangkatan/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	
	public function list_data_aktif(){
		$default_order = "";
		$limit = 10;
		$operator_unit_kerja = $this->session->userdata('user_operator_unit_kerja_id');
		$operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');

		if($this->session->userdata('user_akses_id') == '2'){
        	$where = "pegawai_status = 1 and pegawai_is_pns = 1 and (pegawai_jenis_pensiun_id is null or pegawai_jenis_pensiun_id = 0) and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
		}
		else{
			$where = "pegawai_status = 1 AND pegawai_is_pns = 1";
		}
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->kepangkatan_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->kepangkatan_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->kepangkatan_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_nama_jabatan"],
				$row["unit_kerja_hirarki_name_full"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/kepangkatan/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
	public function hapus($kepangkatan_id = 0,$pegawai_id = 0){

		$kepangkatan_idFilter = filter_var($kepangkatan_id, FILTER_SANITIZE_NUMBER_INT);

		if($this->access->permission('delete')) {
			if($kepangkatan_id==$kepangkatan_idFilter) {

				$dataupdate = array(
					'kepangkatan_status'  => 0,
					'kepangkatan_create_by' 			=> $this->session->userdata('user_id'),
					'kepangkatan_create_date' 			=> date('Y-m-d H:i:s')
				);
				//print_r($dataupdate);die;
				$del = $this->kepangkatan_model->update_kepangkatan($dataupdate,$kepangkatan_id);
				$kepangkatan_last = $this->kepangkatan_model->getKepangkatanLast($pegawai_id)->row_array();
				$update_pegawai = array(
					'pegawai_golongan_id' => $kepangkatan_last['kepangkatan_golongan_id'],
					'pegawai_tanggal_tmt' => $kepangkatan_last['kepangkatan_tanggal_tmt_golongan'],
					'pegawai_tahun_masa_kerja_golongan' => $kepangkatan_last['kepangkatan_tahun_masa_kerja_golongan'],
					'pegawai_bulan_masa_kerja_golongan' => $kepangkatan_last['kepangkatan_bulan_masa_kerja_golongan'],
					'pegawai_sinkronisasi' => 0
				);
				$this->pns_model->updatepegawai($update_pegawai, $pegawai_id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Pendidikan Formal Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'kepegawaian/kepangkatan/update/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Pendidikan Formal Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'kepegawaian/kepangkatan/update/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Pendidikan Formal Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'kepegawaian/kepangkatan/update/'.$pegawai_id);
		}
	}
}
