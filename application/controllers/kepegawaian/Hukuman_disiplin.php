<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hukuman_disiplin extends CI_Controller{

	function __construct(){
		parent:: __construct();
		$this->load->model('kepegawaian/hukuman_disiplin_model');
		//$this->load->library('access');
		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index(){
		if($this->access->permission('read')){
			$data = array();
			$this->template->display('kepegawaian/hukuman_disiplin/index', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function update($pegawai_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];
				$hukuman_disiplin_id = $post['hukuman_disiplin_id'];
				$hukuman_disiplin_pp_pelanggaran_id = $post['hukuman_disiplin_pp_pelanggaran_id'];
				$hukuman_disiplin_jenis_hukuman_id = $post['hukuman_disiplin_jenis_hukuman_id'];
				$hukuman_disiplin_kode_hukuman_lain = $post['hukuman_disiplin_kode_hukuman_lain'];
				$hukuman_disiplin_nomor_sk = $post['hukuman_disiplin_nomor_sk'];
				$hukuman_disiplin_tanggal_sk = $post['hukuman_disiplin_tanggal_sk'];
				$hukuman_disiplin_tanggal_berlaku = $post['hukuman_disiplin_tanggal_berlaku'];
				$hukuman_disiplin_jenis_pejabat_berwenang_id = $post['hukuman_disiplin_jenis_pejabat_berwenang_id'];
				$hukuman_disiplin_ket_pejabatan = $post['hukuman_disiplin_ket_pejabatan'];
				$hukuman_disiplin_peny_kep_huk_id = $post['hukuman_disiplin_peny_kep_huk_id'];
				$hukuman_disiplin_upaya_adm_hukuman_id = $post['hukuman_disiplin_upaya_adm_hukuman_id'];
				$hukuman_disiplin_status_hukuman_id = $post['hukuman_disiplin_status_hukuman_id'];
				$hukuman_disiplin_catatan = $post['hukuman_disiplin_catatan'];
				$hukuman_disiplin_pegawai_id = $post['hukuman_disiplin_pegawai_id'];
				$hukuman_disiplin_pegawai_nip = $post['hukuman_disiplin_pegawai_nip'];
			
				if($isUpdate == 0){
					$datacreate = array(
						'hukuman_disiplin_pp_pelanggaran_id' 	=> $hukuman_disiplin_pp_pelanggaran_id,
						'hukuman_disiplin_jenis_hukuman_id' 	=> $hukuman_disiplin_jenis_hukuman_id,
						'hukuman_disiplin_kode_hukuman_lain' 	=> $hukuman_disiplin_kode_hukuman_lain,
						'hukuman_disiplin_nomor_sk' 			=> $hukuman_disiplin_nomor_sk,
						'hukuman_disiplin_tanggal_sk' 			=> $hukuman_disiplin_tanggal_sk,
						'hukuman_disiplin_tanggal_berlaku' 		=> $hukuman_disiplin_tanggal_berlaku,
						'hukuman_disiplin_jenis_pejabat_berwenang_id' => $hukuman_disiplin_jenis_pejabat_berwenang_id,
						'hukuman_disiplin_ket_pejabatan' 		=> $hukuman_disiplin_ket_pejabatan,
						'hukuman_disiplin_peny_kep_huk_id' 		=> $hukuman_disiplin_peny_kep_huk_id,
						'hukuman_disiplin_upaya_adm_hukuman_id' => $hukuman_disiplin_upaya_adm_hukuman_id,
						'hukuman_disiplin_status_hukuman_id' 	=> $hukuman_disiplin_status_hukuman_id,
						'hukuman_disiplin_catatan' 				=> $hukuman_disiplin_catatan,
						'hukuman_disiplin_pegawai_id' 			=> $hukuman_disiplin_pegawai_id,
						'hukuman_disiplin_pegawai_nip' 			=> $hukuman_disiplin_pegawai_nip,
						'hukuman_disiplin_status' 				=> 1,
						'hukuman_disiplin_create_by' 			=> $this->session->userdata('user_id'),
						'hukuman_disiplin_create_date' 			=> date('Y-m-d H:i:s')
					);


					$insDb = $this->hukuman_disiplin_model->create_hukuman_disiplin($datacreate);

					if($insDb > 0){
						$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["anak_pegawai_nip"])."','6','1F')");
						$notify = array(
							'title' 	=> 'Berhasil!',
							'message' 	=> 'Penambahan Data hukuman_disiplin Berhasil',
							'status' 	=> 'success'
						);
						$this->session->set_flashdata('notify', $notify);

						redirect(base_url().'kepegawaian/hukuman_disiplin/update/'.$hukuman_disiplin_pegawai_id);
					}else{
						$notify = array(
							'title' 	=> 'Gagal!',
							'message'	=> 'Penambahan Data hukuman_disiplin Gagal, silahkan coba lagi',
							'status' 	=> 'error'
						);
						$this->session->set_flashdata('notify', $notify);
						redirect(base_url().'kepegawaian/hukuman_disiplin/update/'.$hukuman_disiplin_pegawai_id);
					}
				}else{
					$dataupdate = array(
						'hukuman_disiplin_pp_pelanggaran_id' 	=> $hukuman_disiplin_pp_pelanggaran_id,
						'hukuman_disiplin_jenis_hukuman_id' 	=> $hukuman_disiplin_jenis_hukuman_id,
						'hukuman_disiplin_kode_hukuman_lain' 	=> $hukuman_disiplin_kode_hukuman_lain,
						'hukuman_disiplin_nomor_sk' 			=> $hukuman_disiplin_nomor_sk,
						'hukuman_disiplin_tanggal_sk' 			=> $hukuman_disiplin_tanggal_sk,
						'hukuman_disiplin_tanggal_berlaku' 		=> $hukuman_disiplin_tanggal_berlaku,
						'hukuman_disiplin_jenis_pejabat_berwenang_id'=> $hukuman_disiplin_jenis_pejabat_berwenang_id,
						'hukuman_disiplin_ket_pejabatan' 		=> $hukuman_disiplin_ket_pejabatan,
						'hukuman_disiplin_peny_kep_huk_id' 		=> $hukuman_disiplin_peny_kep_huk_id,
						'hukuman_disiplin_upaya_adm_hukuman_id' => $hukuman_disiplin_upaya_adm_hukuman_id,
						'hukuman_disiplin_status_hukuman_id' 	=> $hukuman_disiplin_status_hukuman_id,
						'hukuman_disiplin_catatan' 				=> $hukuman_disiplin_catatan,
						'hukuman_disiplin_status' 				=> 1,
						'hukuman_disiplin_create_by' 			=> $this->session->userdata('user_id'),
						'hukuman_disiplin_create_date' 			=> date('Y-m-d H:i:s')
					);
					$this->hukuman_disiplin_model->update_hukuman_disiplin($dataupdate, $hukuman_disiplin_id);
					$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["anak_pegawai_nip"])."','27','2F')");
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan Data hukuman_disiplin Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/hukuman_disiplin/update/'.$hukuman_disiplin_pegawai_id);
				}
			}

			$data = array();
			$data['pegawai']  	= $this->hukuman_disiplin_model->getPegawai($pegawai_id)->row_array();
			$data['history']  	= $this->hukuman_disiplin_model->getListHukuman_disiplin($pegawai_id)->result_array();
			$data['pp_pelanggaran']  	= $this->hukuman_disiplin_model->getPelanggaranPP()->result_array();
			$data['jenis_hukuman']  	= $this->hukuman_disiplin_model->getJenisHukuman()->result_array();
			$data['jenis_pejabat']  	= $this->hukuman_disiplin_model->getJenisPejabat()->result_array();
			$data['jenis_peny_keputusan']  	= $this->hukuman_disiplin_model->getJenisPenyKeputusan()->result_array();
			$data['upaya_adm']  	= $this->hukuman_disiplin_model->getUpayaAdm()->result_array();
			$data['status_hukuman']  	= $this->hukuman_disiplin_model->getStatusHukuman()->result_array();
			$this->template->display('kepegawaian/hukuman_disiplin/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function get_tingkat_hukuman_disiplin()
	{
		$hukuman_disiplin_kode 	= $this->input->post('hukuman_disiplin_kode');
		$data["tingkat_hukuman_disiplin"] = $this->hukuman_disiplin_model->getTingkathukuman_disiplin($hukuman_disiplin_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_fakultas()
	{
		$jurusan_kode 	= $this->input->post('jurusan_kode');
		$data["jurusan"] = $this->hukuman_disiplin_model->getFakultas($jurusan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_jurusan()
	{
		$jurusan_kode 	= $this->input->post('jurusan_kode');
		$data["jurusan"] = $this->hukuman_disiplin_model->getJurusan($jurusan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	

	
	public function list_data_aktif(){
		$default_order = "";
		$limit = 10;
		$operator_unit_kerja = $this->session->userdata('user_operator_unit_kerja_id');
		$operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');

		if($this->session->userdata('user_akses_id') == '2'){
        	$where = "pegawai_status = 1 and pegawai_is_pns = 1 and (pegawai_jenis_pensiun_id is null or pegawai_jenis_pensiun_id = 0) and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
		}
		else{
			$where = "pegawai_status = 1 AND pegawai_is_pns = 1";
		}
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->hukuman_disiplin_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->hukuman_disiplin_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->hukuman_disiplin_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_nama_jabatan"],
				$row["unit_kerja_hirarki_name_full"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/hukuman_disiplin/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
	public function hapus($hukuman_disiplin_id = 0,$pegawai_id = 0){

		$hukuman_disiplin_idFilter = filter_var($hukuman_disiplin_id, FILTER_SANITIZE_NUMBER_INT);
		if($this->access->permission('delete')) {
			if($hukuman_disiplin_id==$hukuman_disiplin_idFilter) {

				$dataupdate = array(
					'hukuman_disiplin_status'  => 0,
					'hukuman_disiplin_create_by' 			=> $this->session->userdata('user_id'),
					'hukuman_disiplin_create_date' 			=> date('Y-m-d H:i:s')
				);

				$del = $this->hukuman_disiplin_model->update_hukuman_disiplin($dataupdate,$hukuman_disiplin_id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'hukuman_disiplin Formal Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'kepegawaian/hukuman_disiplin/update/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'hukuman_disiplin Formal Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'kepegawaian/hukuman_disiplin/update/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'hukuman_disiplin Formal Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'kepegawaian/hukuman_disiplin/update/'.$pegawai_id);
		}
	}

	
}