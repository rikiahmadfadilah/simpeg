<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan extends CI_Controller {

	public function __construct(){
		parent::__construct();
		// $this->load->model('auth/auth_model','',TRUE);
		// $this->load->model('kepegawaian/pns/pns_model','',TRUE);
		$this->load->model('kepegawaian/jabatan_model','',TRUE);
		//$this->load->model('kepegawaian/jabatan/pns_model','',TRUE);

		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index()
	{
		$data = array();
		$this->template->display('kepegawaian/jabatan/index', $data);	
	}

	public function update($pegawai_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];

				//print_r($post['jenjang_jabatan_unit_kerja_id']);die;
				$jenjang_jabatan_unit_kerja_id = $post['jenjang_jabatan_unit_kerja_id'];
				$unit_kerja_id = '';
				for ($i=0; $i < count($jenjang_jabatan_unit_kerja_id); $i++) { 
					if($jenjang_jabatan_unit_kerja_id[$i] != ''){
						$unit_kerja_id = $jenjang_jabatan_unit_kerja_id[$i];
					}
				}
				$datauker = $this->db->query("SELECT * FROM ms_unit_kerja WHERE unit_kerja_id = '".$unit_kerja_id."' AND unit_kerja_status = 1")->row_array();
				$unit_kerja_kode = $datauker["unit_kerja_kode"];
				// echo "<pre>"; print_r($unit_kerja_kode);die;
				$jenjang_jabatan_id = $post['jenjang_jabatan_id'];
				$jabatan_fix_id = $post['jabatan_fix_id'];
				$jenjang_jabatan_pegawai_id = $post['jenjang_jabatan_pegawai_id'];
				$jenjang_jabatan_pegawai_nip = $post['jenjang_jabatan_pegawai_nip'];
				// $jenjang_jabatan_jabatan_id = $post['jenjang_jabatan_jabatan_id'];
				$jenjang_jabatan_tipe = $post['jenjang_jabatan_tipe'];
				if($jenjang_jabatan_tipe == 1){
					$jenjang_jabatan_jabatan_nama = $post['jenjang_js_nama'];
					$jenjang_jabatan_jabatan_id = $post['jenjang_jabatan_jabatan_id'];
				}else if($jenjang_jabatan_tipe == 2){
					$jenjang_jabatan_jabatan_nama = $post['jenjang_jabatan_jft_nama'];
					$jenjang_jabatan_jabatan_id = $post['jenjang_jabatan_jft_id'];
				}else if($jenjang_jabatan_tipe == 3){
					$jenjang_jabatan_jabatan_nama = $post['jenjang_jabatan_jfu_nama'];
					$jenjang_jabatan_jabatan_id = $post['jenjang_jabatan_jfu_id'];
				}
				// $jenjang_jabatan_jabatan_nama = $post['jenjang_jabatan_jabatan_nama'];
				//$jenjang_jabatan_unit_kerja_id = $post['jenjang_jabatan_unit_kerja_id'];
				$jenjang_jabatan_tanggal_mulai = $post['jenjang_jabatan_tanggal_mulai'];
				$jenjang_jabatan_tanggal_selesai = $post['jenjang_jabatan_tanggal_selesai'];
				$jenjang_jabatan_nomor_sk = $post['jenjang_jabatan_nomor_sk'];
				$jenjang_jabatan_tanggal_sk = $post['jenjang_jabatan_tanggal_sk'];
				$jabatan_tipe = isset($post['cuti_detail_pegawai_nip'])?$post['cuti_detail_pegawai_nip']:'';
				$jenjang_jabatan_angka_kredit_old = isset($post['jenjang_jabatan_angka_kredit_old'])?$post['jenjang_jabatan_angka_kredit_old']:'';
				$jenjang_jabatan_angka_kredit = isset($post['jenjang_jabatan_angka_kredit'])?$post['jenjang_jabatan_angka_kredit']:'';
				$jenjang_jabatan_unit_kerja_lama_nama = isset($post['jenjang_jabatan_unit_kerja_lama_nama'])?$post['jenjang_jabatan_unit_kerja_lama_nama']:'';
				// echo "<pre>";
				// print_r($post);die;
				if($isUpdate == 0){
					$datacreate = array(
						'jenjang_jabatan_pegawai_id' => $jenjang_jabatan_pegawai_id, 
						'jenjang_jabatan_pegawai_nip' => $jenjang_jabatan_pegawai_nip, 
						'jenjang_jabatan_jabatan_id' => $jenjang_jabatan_jabatan_id, 
						'jenjang_jabatan_jabatan_nama' => $jenjang_jabatan_jabatan_nama, 
						'jenjang_jabatan_unit_kerja_id' => $unit_kerja_id, 
						'jenjang_jabatan_tanggal_mulai' => $jenjang_jabatan_tanggal_mulai, 
						'jenjang_jabatan_tanggal_selesai' => $jenjang_jabatan_tanggal_selesai, 
						'jenjang_jabatan_nomor_sk' => $jenjang_jabatan_nomor_sk, 
						'jenjang_jabatan_tanggal_sk' => $jenjang_jabatan_tanggal_sk, 
						'jenjang_jabatan_create_date' => date('Y-m-d H:i:s'), 
						'jenjang_jabatan_create_by' => $this->session->userdata('user_id'), 
						'jenjang_jabatan_status' => 1,
						'jenjang_jabatan_tipe' => $jenjang_jabatan_tipe,
						'jenjang_jabatan_angka_kredit' => $jenjang_jabatan_angka_kredit ,
						'jenjang_jabatan_unit_kerja_lama_nama' => $jenjang_jabatan_unit_kerja_lama_nama
					);
					// echo "<pre>"; print_r(dateEnToId($jenjang_jabatan_tanggal_mulai, 'Y-m-d'));die;
					$insDb = $this->jabatan_model->create_jenjang_jabatan($datacreate);
					
					$jabatan_last = $this->jabatan_model->getJenjangJabatanLast($pegawai_id)->row_array();
					// echo "<pre>"; print_r(count($jabatan_last));die;
						if(count($jabatan_last) < 1){
							$this->db->query("UPDATE ms_pegawai SET pegawai_sinkronisasi = 0, pegawai_unit_kerja_id = '".$unit_kerja_id."', pegawai_unit_kerja_code = '".$unit_kerja_kode."', pegawai_nama_jabatan = '".$jenjang_jabatan_jabatan_nama."', pegawai_tanggal_tmt_jabatan = '".$jenjang_jabatan_tanggal_mulai."', pegawai_jabatan_id = '".$jenjang_jabatan_jabatan_id."' WHERE pegawai_id = ".$jenjang_jabatan_pegawai_id);
						}else{
							$this->db->query("UPDATE ms_pegawai SET pegawai_sinkronisasi = 0, pegawai_unit_kerja_id = '".$jabatan_last['jenjang_jabatan_unit_kerja_id']."', pegawai_unit_kerja_code = '".$jabatan_last['unit_kerja_kode']."', pegawai_nama_jabatan = '".$jabatan_last['jenjang_jabatan_jabatan_nama']."', pegawai_tanggal_tmt_jabatan = '".$jabatan_last['jenjang_jabatan_tanggal_mulai']."', pegawai_jabatan_id = '".$jenjang_jabatan_jabatan_id."' WHERE pegawai_id = ".$jenjang_jabatan_pegawai_id);
							// $this->db->query("UPDATE trx_jenjang_jabatan set jenjang_jabatan_tanggal_selesai = '".dateEnToId($jenjang_jabatan_tanggal_mulai,'Y-m-d')."' WHERE jenjang_jabatan_id = ".$jabatan_last['jenjang_jabatan_id']);
						}
					
					if($insDb > 0){
						if($jenjang_jabatan_tipe == 2 && $jenjang_jabatan_angka_kredit != ''){
							$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["jenjang_jabatan_pegawai_nip"])."','19','1S')");
						}else{

						} 
						if($jenjang_jabatan_tipe != 2){
							$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["jenjang_jabatan_pegawai_nip"])."','14','1N')");
						}else{
							$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["jenjang_jabatan_pegawai_nip"])."','20','1T')");
						}
						$notify = array(
							'title' 	=> 'Berhasil!',
							'message' 	=> 'Penambahan Data Pendidikan Berhasil',
							'status' 	=> 'success'
						);
						$this->session->set_flashdata('notify', $notify);

						redirect(base_url().'kepegawaian/jabatan/update/'.$jenjang_jabatan_pegawai_id);
					}else{
						$notify = array(
							'title' 	=> 'Gagal!',
							'message'	=> 'Penambahan Data Pendidikan Gagal, silahkan coba lagi',
							'status' 	=> 'error'
						);
						$this->session->set_flashdata('notify', $notify);
						redirect(base_url().'kepegawaian/jabatan/update/'.$jenjang_jabatan_pegawai_id);
					}
				}else{
					$dataupdate = array(
						// 'jenjang_jabatan_pegawai_id' => $jenjang_jabatan_pegawai_id, 
						// 'jenjang_jabatan_pegawai_nip' => $jenjang_jabatan_pegawai_nip, 
						'jenjang_jabatan_jabatan_id' => $jenjang_jabatan_jabatan_id, 
						'jenjang_jabatan_jabatan_nama' => $jenjang_jabatan_jabatan_nama, 
						'jenjang_jabatan_unit_kerja_id' => $unit_kerja_id, 
						'jenjang_jabatan_tanggal_mulai' => $jenjang_jabatan_tanggal_mulai, 
						'jenjang_jabatan_tanggal_selesai' => $jenjang_jabatan_tanggal_selesai, 
						'jenjang_jabatan_nomor_sk' => $jenjang_jabatan_nomor_sk, 
						'jenjang_jabatan_tanggal_sk' => $jenjang_jabatan_tanggal_sk, 
						'jenjang_jabatan_create_date' => date('Y-m-d H:i:s'), 
						'jenjang_jabatan_create_by' => $this->session->userdata('user_id'), 
						'jenjang_jabatan_status' => 1,
						'jenjang_jabatan_tipe' => $jenjang_jabatan_tipe,
						'jenjang_jabatan_angka_kredit' => $jenjang_jabatan_angka_kredit,
						'jenjang_jabatan_unit_kerja_lama_nama' => $jenjang_jabatan_unit_kerja_lama_nama 
					);
					// echo "<pre>";
					// print_r($dataupdate);die;
					$this->jabatan_model->update_jenjang_jabatan($dataupdate, $jenjang_jabatan_id);
					$jabatan_last = $this->jabatan_model->getJenjangJabatanLast($pegawai_id)->row_array();
					// echo "<pre>";
					// print_r($jabatan_last['jenjang_jabatan_unit_kerja_id']);die;
					$this->db->query("UPDATE ms_pegawai SET pegawai_sinkronisasi = 0, pegawai_unit_kerja_id = '".$jabatan_last['jenjang_jabatan_unit_kerja_id']."', pegawai_unit_kerja_code = '".$jabatan_last['unit_kerja_kode']."', pegawai_nama_jabatan = '".$jabatan_last['jenjang_jabatan_jabatan_nama']."', pegawai_tanggal_tmt_jabatan = '".$jabatan_last['jenjang_jabatan_tanggal_mulai']."', pegawai_jabatan_id = '".$jenjang_jabatan_jabatan_id."' WHERE pegawai_id = ".$jenjang_jabatan_pegawai_id);
					// $this->db->query("UPDATE ms_pegawai SET pegawai_sinkronisasi = 0, pegawai_unit_kerja_id = '".$unit_kerja_id."', pegawai_unit_kerja_code = '".$unit_kerja_kode."', pegawai_nama_jabatan = '".$jabatan_last['jenjang_jabatan_jabatan_nama']."', pegawai_tanggal_tmt_jabatan = '".$jabatan_last['jenjang_jabatan_tanggal_mulai']."', pegawai_jabatan_id = '".$jabatan_fix_id."' WHERE pegawai_id = ".$jenjang_jabatan_pegawai_id);
					if($jenjang_jabatan_tipe == 2 && $jenjang_jabatan_angka_kredit != $jenjang_jabatan_angka_kredit_old){
						$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["jenjang_jabatan_pegawai_nip"])."','42','2U')");
					}else{

					} 
					if($jenjang_jabatan_tipe != 2){
						$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["jenjang_jabatan_pegawai_nip"])."','28','2G')");
					}else{
						$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["jenjang_jabatan_pegawai_nip"])."','43','2V')");
					}
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan Data Pendidikan Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/jabatan/update/'.$jenjang_jabatan_pegawai_id);
				}
			}

			// $data = array();
			$data['pegawai']  	= $this->jabatan_model->getPegawai($pegawai_id)->row_array();
			$data['history']  	= $this->jabatan_model->getJenjangJabatan($pegawai_id)->result_array();
			$data['golongan']  	= $this->jabatan_model->getGolongan()->result_array();
			$data["unit_kerja"] = $this->jabatan_model->getUnitKerja(1,2)->result_array();
			$data["jabatan"] 	= $this->jabatan_model->getJabatan()->result_array();
			$data["struktural"] = $this->jabatan_model->getStruktural()->result_array();
			$data["fungsional_tertentu"] = $this->jabatan_model->getFungsionalTertentu()->result_array();
			// echo "<pre>"; print_r($data["fungsional_tertentu"]); die;
			$data["fungsional_umum"] = $this->jabatan_model->getFungsionalUmum()->result_array();
			$this->template->display('kepegawaian/jabatan/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	
	public function list_data_aktif(){
		$default_order = "";
		$limit = 10;
		$operator_unit_kerja = $this->session->userdata('user_operator_unit_kerja_id');
		$operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');

		if($this->session->userdata('user_akses_id') == '2'){
			$where = "pegawai_status = 1 and pegawai_is_pns = 1 and (pegawai_jenis_pensiun_id is null or pegawai_jenis_pensiun_id = 0) and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
		}
		else{
			$where = "pegawai_status = 1 AND pegawai_is_pns = 1";
		}
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->jabatan_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->jabatan_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->jabatan_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_nama_jabatan"],
				$row["unit_kerja_hirarki_name_full"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/jabatan/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
	public function hapus($jabatan_id = 0,$pegawai_id = 0){

		$jabatan_idFilter = filter_var($jabatan_id, FILTER_SANITIZE_NUMBER_INT);

		if($this->access->permission('delete')) {
			if($jabatan_id==$jabatan_idFilter) {

				$dataupdate = array(
					'jenjang_jabatan_status'  => 0,
					'jenjang_jabatan_create_by' 			=> $this->session->userdata('user_id'),
					'jenjang_jabatan_create_date' 			=> date('Y-m-d H:i:s')
				);

				//print_r($dataupdate);die;
				$del = $this->jabatan_model->update_jenjang_jabatan($dataupdate,$jabatan_id);
				$jabatan_last = $this->jabatan_model->getJenjangJabatanLast($pegawai_id)->row_array();
				if($jabatan_last["jenjang_jabatan_unit_kerja_id"] != ''){
				$this->db->query("UPDATE ms_pegawai SET pegawai_sinkronisasi = 0, pegawai_unit_kerja_id = '".$jabatan_last['jenjang_jabatan_unit_kerja_id']."', pegawai_unit_kerja_code = '".$jabatan_last['unit_kerja_kode']."', pegawai_nama_jabatan = '".$jabatan_last['jenjang_jabatan_jabatan_nama']."', pegawai_tanggal_tmt_jabatan = '".$jabatan_last['jenjang_jabatan_tanggal_mulai']."' WHERE pegawai_id = ".$pegawai_id);

				$jenjang_jabatan = $this->db->query("SELECT * FROM view_trx_jenjang_jabatan a WHERE a.jenjang_jabatan_id = (SELECT TOP 1 x.jenjang_jabatan_id FROM [dbo].[trx_jenjang_jabatan] x WHERE x.[jenjang_jabatan_pegawai_id] = '".$pegawai_id."' AND jenjang_jabatan_status = 1 ORDER BY x.[jenjang_jabatan_tanggal_mulai] DESC)")->row_array();
				$jenjang_jabatan_jabatan_id = $jabatan_last["jenjang_jabatan_jabatan_id"];
				$jenjang_jabatan_jabatan_nama = $jabatan_last["jenjang_jabatan_jabatan_nama"];
				$unit_kerja_id = $jabatan_last["jenjang_jabatan_unit_kerja_id"];
				$jenjang_jabatan_tanggal_mulai = $jabatan_last["jenjang_jabatan_tanggal_mulai"];
				$jenjang_jabatan_tanggal_selesai = $jabatan_last["jenjang_jabatan_tanggal_selesai"];
				$jenjang_jabatan_nomor_sk = $jabatan_last["jenjang_jabatan_nomor_sk"];
				$jenjang_jabatan_tanggal_sk = $jabatan_last["jenjang_jabatan_tanggal_sk"];
				$jenjang_jabatan_angka_kredit = $jabatan_last["jenjang_jabatan_angka_kredit"];
				$jenjang_jabatan_unit_kerja_lama_nama = $jabatan_last["jenjang_jabatan_unit_kerja_lama_nama"];
				$jenjang_jabatan_tipe = $jabatan_last["jenjang_jabatan_tipe"];


				$datauker = $this->db->query("SELECT * FROM ms_unit_kerja WHERE unit_kerja_id = '".$unit_kerja_id."' AND unit_kerja_status = 1")->row_array();
				$unit_kerja_kode = $datauker["unit_kerja_kode"];

			
				$jabatan_last = $this->jabatan_model->getJenjangJabatanLast($pegawai_id)->row_array();
				$this->db->query("UPDATE ms_pegawai SET pegawai_sinkronisasi = 0, pegawai_unit_kerja_id = '".$unit_kerja_id."', pegawai_unit_kerja_code = '".$unit_kerja_kode."', pegawai_nama_jabatan = '".$jabatan_last['jenjang_jabatan_jabatan_nama']."', pegawai_tanggal_tmt_jabatan = '".$jabatan_last['jenjang_jabatan_tanggal_mulai']."', pegawai_jabatan_id = '".$jenjang_jabatan["jabatan_fix_id"]."' WHERE pegawai_id = ".$pegawai_id);

				}else{}
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Pendidikan Formal Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'kepegawaian/jabatan/update/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Pendidikan Formal Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'kepegawaian/jabatan/update/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Pendidikan Formal Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'kepegawaian/jabatan/update/'.$pegawai_id);
		}
	}

	public function get_unit_kerja_hirarki()
	{
		$unit_kerja_parent_id_kode 	= $this->input->post('unit_kerja_parent_id_kode');
		$unit_kerja_level 	= $this->input->post('unit_kerja_level');
		$data["unit_kerja"] = $this->jabatan_model->getUnitKerja($unit_kerja_parent_id_kode,$unit_kerja_level)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}

	public function get_jabatan()
	{
		$post = $this->input->post();
		$jabatan_tipe = $post['jabatan_tipe'];
		$jabatan_tipe = $this->jabatan_model->get_jabatan($jabatan_tipe)->result_array();
		if ($jabatan_tipe > 0) {
			echo json_encode($jabatan_tipe);
		}
	}
}
