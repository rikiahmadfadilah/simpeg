<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pns extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('auth/auth_model','',TRUE);
		$this->load->model('kepegawaian/pns/pns_model','',TRUE);
		$this->load->model('kepegawaian/jabatan_model','',TRUE);
		$this->load->model('kepegawaian/kepangkatan_model','',TRUE);
		$this->load->model('kepegawaian/unit_kerja_model','',TRUE);
	}

	public function index()
	{
		$data = array();
		$this->template->display('kepegawaian/pns/index', $data);	
	}
	public function create()
	{
		if($this->access->permission('read')){
			if($post = $this->input->post()){
				$pegawai_nip = str_replace(' ', '', $post["pegawai_nip"]);
				$pegawai_unit_kerja_code = "";
				foreach($post["pegawai_unit_kerja_id"] as $field => $value) {
					if($value!=""){
						$pegawai_unit_kerja_code = $value;
					}
				}
				$datauker = $this->db->query("SELECT * FROM ms_unit_kerja WHERE unit_kerja_kode = '".$pegawai_unit_kerja_code."' AND unit_kerja_status = 1")->row_array();
				$pegawai_unit_kerja_id_fix = $datauker["unit_kerja_id"];
				$format="";//format file name
	            $strf=(string)strftime($format);
	            $config['file_name'] = '';
	            $config['upload_path']      = './assets/images/pegawai/photo';
	            $config['allowed_types']    = 'gif|jpg|png|svg|jpeg';
	            $config['max_size']         = '20000'; // KB    
	            $this->load->library('upload', $config);
	            $file_name = "";
	            if($this->upload->do_upload('pegawai_photo')) {
	                $upload_data = array('uploads' =>$this->upload->data());
	                $file_name   =  $upload_data['uploads']['file_name'];
	            }else{
	            	$file_name = '';
	            }
				$datainsert = array(
					'pegawai_nip' => str_replace(' ', '', $post["pegawai_nip"]),
					'pegawai_unit_kerja_id' => $pegawai_unit_kerja_id_fix,
					'pegawai_unit_kerja_code' => $pegawai_unit_kerja_code,
					'pegawai_nomor_ktp' => str_replace(' ', '', $post["pegawai_nomor_ktp"]),
					'pegawai_nomor_npwp' => str_replace(' ', '', $post["pegawai_nomor_npwp"]),
					'pegawai_gelar_depan' => $post["pegawai_gelar_depan"],
					'pegawai_gelar_belakang' => $post["pegawai_gelar_belakang"],
					'pegawai_nama' => $post["pegawai_nama"],
					'pegawai_tempat_lahir' => $post["pegawai_tempat_lahir"],
					'pegawai_tanggal_lahir' => $post["pegawai_tanggal_lahir_submit"],
					'pegawai_jenis_kelamin_id' => $post["pegawai_jenis_kelamin_id"],
					'pegawai_perkawinan_id' => $post["pegawai_perkawinan_id"],
					'pegawai_agama_id' => $post["pegawai_agama_id"],
					'pegawai_golongan_darah_id' => $post["pegawai_golongan_darah_id"],
					'pegawai_domisili_negara_id' => $post["pegawai_domisili_negara_id"],
					'pegawai_domisili_provinsi_id' => $post["pegawai_domisili_provinsi_id"],
					'pegawai_domisili_kota_id' => $post["pegawai_domisili_kota_id"],
					'pegawai_domisili_kec_id' => $post["pegawai_domisili_kec_id"],
					'pegawai_domisili_kel_id' => $post["pegawai_domisili_kel_id"],
					'pegawai_domisili_alamat' => $post["pegawai_domisili_alamat"],
					'pegawai_domisili_kodepos' => $post["pegawai_domisili_kodepos"],
					'pegawai_ktp_provinsi_id' => $post["pegawai_ktp_provinsi_id"],
					'pegawai_ktp_kota_id' => $post["pegawai_ktp_kota_id"],
					'pegawai_ktp_kec_id' => $post["pegawai_ktp_kec_id"],
					'pegawai_ktp_kel_id' => $post["pegawai_ktp_kel_id"],
					'pegawai_ktp_alamat' => $post["pegawai_ktp_alamat"],
					'pegawai_ktp_kodepos' => $post["pegawai_ktp_kodepos"],
					'pegawai_email_lain' => $post["pegawai_email_lain"],
					'pegawai_email' => $post["pegawai_email"],
					'pegawai_handphone' => $post["pegawai_handphone"],
					'pegawai_nomor_karpeg' => $post["pegawai_nomor_karpeg"],
					'pegawai_nomor_askes' => $post["pegawai_nomor_askes"],
					'pegawai_nomor_karis' => $post["pegawai_nomor_karis"],
					// 'pegawai_pendidikan_terakhir_id' => $post["pegawai_pendidikan_terakhir_id"],
					// 'pegawai_pendidikan_tingkat_id' => $post["pegawai_pendidikan_tingkat_id"],
					// 'pegawai_pendidikan_fakultas_id' => $post["pegawai_pendidikan_fakultas_id"],
					// 'pegawai_pendidikan_jurusan_id' => $post["pegawai_pendidikan_jurusan_id"],
					// 'pegawai_pendidikan_program_studi' => $post["pegawai_pendidikan_program_studi"],
					// 'pegawai_pendidikan_tahun_masuk' => $post["pegawai_pendidikan_tahun_masuk"],
					// 'pegawai_pendidikan_tahun_lulus' => $post["pegawai_pendidikan_tahun_lulus"],
					// 'pegawai_pendidikan_nama' => $post["pegawai_pendidikan_nama"],
					// 'pegawai_pendidikan_tempat' => $post["pegawai_pendidikan_tempat"],
					// 'pegawai_pendidikan_ipk' => $post["pegawai_pendidikan_ipk"],
					// 'pegawai_nilai_toefl' => $post["pegawai_nilai_toefl"],
					// 'pegawai_nilai_ielt' => $post["pegawai_nilai_ielt"],
					// 'pegawai_catatan' => $post["pegawai_catatan"],
					// 'pegawai_diklat_jenis_id' => $post["pegawai_diklat_jenis_id"],
					// 'pegawai_diklat_angkatan' => $post["pegawai_diklat_angkatan"],
					// 'pegawai_diklat_nama' => $post["pegawai_diklat_nama"],
					// 'pegawai_diklat_penyelenggara' => $post["pegawai_diklat_penyelenggara"],
					// 'pegawai_diklat_durasi' => $post["pegawai_diklat_durasi"],
					// 'pegawai_diklat_tanggal_mulai' => $post["pegawai_diklat_tanggal_mulai_submit"],
					// 'pegawai_diklat_tanggal_selesai' => $post["pegawai_diklat_tanggal_selesai_submit"],
					// 'pegawai_diklat_predikat' => $post["pegawai_diklat_predikat"],
					// 'pegawai_diklat_lokasi' => $post["pegawai_diklat_lokasi"],
					// 'pegawai_diklat_nomor_sertifikat' => $post["pegawai_diklat_nomor_sertifikat"],
					// 'pegawai_diklat_tanggal_sertifikat' => $post["pegawai_diklat_tanggal_sertifikat_submit"],
					// 'pegawai_diklat_lemhanas' => $post["pegawai_diklat_lemhanas"],
					'pegawai_status_pegawai_id' => $post["pegawai_status_pegawai_id"],
					'pegawai_instansi_asal' => $post["pegawai_instansi_asal"],
					'pegawai_cpns_formasi_id' => $post["pegawai_cpns_formasi_id"],
					'pegawai_cpns_nama_formasi' => $post["pegawai_cpns_nama_formasi"],
					'pegawai_cpns_status_formasi' => $post["pegawai_cpns_status_formasi"],
					'pegawai_cpns_fungsional_catatan' => $post["pegawai_cpns_fungsional_catatan"],
					'pegawai_cpns_golongan_id' => $post["pegawai_cpns_golongan_id"],
					'pegawai_cpns_tanggal_tmt' => $post["pegawai_cpns_tanggal_tmt_submit"],
					'pegawai_golongan_id' => $post["pegawai_golongan_id"],
					'pegawai_tanggal_tmt' => $post["pegawai_tanggal_tmt_submit"],
					'pegawai_tahun_masa_kerja_golongan' => $post["pegawai_tahun_masa_kerja_golongan"],
					'pegawai_bulan_masa_kerja_golongan' => $post["pegawai_bulan_masa_kerja_golongan"],
					'pegawai_tahun_masa_kerja_berkala' => $post["pegawai_tahun_masa_kerja_berkala"],
					'pegawai_bulan_masa_kerja_berkala' => $post["pegawai_bulan_masa_kerja_berkala"],
					'pegawai_tanggal_tmt_berkala' => $post["pegawai_tanggal_tmt_berkala_submit"],
					'pegawai_jabatan_id' => $post["pegawai_jabatan_id"],
					'pegawai_jabatan_fungsional_id' => $post["pegawai_jabatan_fungsional_id"],
					'pegawai_tanggal_tmt_jabfung' => $post["pegawai_tanggal_tmt_jabfung_submit"],
					'pegawai_fungsional_kredit' => $post["pegawai_fungsional_kredit"],
					'pegawai_jab_fung_bidang_1' => $post["pegawai_jab_fung_bidang_1"],
					'pegawai_jab_fung_bidang_2' => $post["pegawai_jab_fung_bidang_2"],
					'pegawai_tanggal_pembebasan_tmt_jab_fung' => $post["pegawai_tanggal_pembebasan_tmt_jab_fung_submit"],
					'pegawai_tanggal_aktif_tmt_jab_fung' => $post["pegawai_tanggal_aktif_tmt_jab_fung_submit"],
					'pegawai_nama_jabatan' => $post["pegawai_nama_jabatan"],
					'pegawai_tanggal_tmt_jabatan' => $post["pegawai_tanggal_tmt_jabatan_submit"],
					'pegawai_telah_pra_jabatan' => $post["pegawai_telah_pra_jabatan"],
					'pegawai_tahun_pra_jabatan' => $post["pegawai_tahun_pra_jabatan"],
					'pegawai_telah_sumpah_jabatan' => $post["pegawai_telah_sumpah_jabatan"],
					'pegawai_tahun_sumpah_jabatan' => $post["pegawai_tahun_sumpah_jabatan"],
					'pegawai_telah_test_kesehatan' => $post["pegawai_telah_test_kesehatan"],
					'pegawai_tahun_test_kesehatan' => $post["pegawai_tahun_test_kesehatan"],
					'pegawai_cpns_fungsional_tipe' => $post["pegawai_cpns_fungsional_tipe"],
					'pegawai_nomor_pendidikan' => $post["pegawai_nomor_pendidikan"],
					'pegawai_image_path' => $file_name,
					'pegawai_create_by' => $this->session->userdata('user_id'),
					'pegawai_create_date' => date('Y-m-d H:i:s'),
					'pegawai_status' => 1,
					'pegawai_is_non_pns' => 1,
					'pegawai_is_pns' => 1,
					'pegawai_sinkronisasi' => 0
				);

				$pegawai_id = $this->pns_model->createpegawai($datainsert);//
				if($pegawai_id > 0){

					$this->db->query("INSERT INTO  [dbo].[ms_user] (user_nip,username,password,user_status,user_akses_id,user_default_akses_id) VALUES('".str_replace(' ', '', $post["pegawai_nip"])."','".str_replace(' ', '', $post["pegawai_nip"])."','".md5($post["pegawai_nomor_ktp"])."',1,3,3)");
					$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["pegawai_nip"])."','1','1A')");
					$this->load->model('kepegawaian/pendidikan_formal_model');
					$this->load->model('kepegawaian/diklat_perjenjangan_model');
					// $pendidikan_formal_nama = $post['pegawai_pendidikan_nama'];
					// $pendidikan_formal_pendidikan_id = $post['pegawai_pendidikan_terakhir_id'];
					// $pendidikan_formal_jurusan_code_tingkat = $post['pegawai_pendidikan_tingkat_id'];
					// $pendidikan_formal_jurusan_code_fakultas = $post['pegawai_pendidikan_fakultas_id'];
					// $pendidikan_formal_jurusan_code = $post['pegawai_pendidikan_jurusan_id'];
					// $pendidikan_formal_pro_studi = $post['pegawai_pendidikan_program_studi'];
					// $pendidikan_formal_thn_masuk = $post['pegawai_pendidikan_tahun_masuk'];
					// $pendidikan_formal_thn_lulus = $post['pegawai_pendidikan_tahun_lulus'];
					// $pendidikan_formal_tmp_belajar = $post['pegawai_pendidikan_tempat'];
					$pendidikan_formal_lokasi = "";
					$pendidikan_formal_no_ijazah = "";
					$pendidikan_formal_nama_kep = "";
					$pendidikan_formal_pegawai_id = $pegawai_id;
					$pendidikan_formal_pegawai_nip = str_replace(' ', '', $post["pegawai_nip"]);


					// $Tingkat = $this->pendidikan_formal_model->getJurusanData($pendidikan_formal_jurusan_code_tingkat)->row_array();
					// $pendidikan_formal_jurusan_id_tingkat = $Tingkat["jurusan_id"];
					// $pendidikan_formal_tingkat = $Tingkat["jurusan_grup"];
					// $Fakultas = $this->pendidikan_formal_model->getJurusanData($pendidikan_formal_jurusan_code_fakultas)->row_array();
					// $pendidikan_formal_jurusan_id_fakultas = $Fakultas["jurusan_id"];
					// $pendidikan_formal_fakultas = $Fakultas["jurusan_nama"];
					// $Jurusan = $this->pendidikan_formal_model->getJurusanData($pendidikan_formal_jurusan_code)->row_array();
					// $pendidikan_formal_jurusan_id = $Jurusan["jurusan_id"];
					// $pendidikan_formal_jurusan = $Jurusan["jurusan_nama"];
					// $Pendidikan = $this->pendidikan_formal_model->getPendidikanData($pendidikan_formal_pendidikan_id)->row_array();
					// $pendidikan_formal_pendidikan = $Pendidikan["pendidikan_nama"];
					$datacreate = array(
						// 'pendidikan_formal_nama' 				=> $pendidikan_formal_nama,
						// 'pendidikan_formal_pendidikan_id' 		=> $pendidikan_formal_pendidikan_id,
						// 'pendidikan_formal_pendidikan' 			=> $pendidikan_formal_pendidikan,
						// 'pendidikan_formal_jurusan_id_tingkat' 	=> $pendidikan_formal_jurusan_id_tingkat,
						// 'pendidikan_formal_tingkat' 			=> $pendidikan_formal_tingkat,
						// 'pendidikan_formal_jurusan_id_fakultas' => $pendidikan_formal_jurusan_id_fakultas,
						// 'pendidikan_formal_fakultas' 			=> $pendidikan_formal_fakultas,
						// 'pendidikan_formal_jurusan_id' 			=> $pendidikan_formal_jurusan_id,
						// 'pendidikan_formal_jurusan' 			=> $pendidikan_formal_jurusan,
						// 'pendidikan_formal_pro_studi' 			=> $pendidikan_formal_pro_studi,
						// 'pendidikan_formal_thn_masuk' 			=> $pendidikan_formal_thn_masuk,
						// 'pendidikan_formal_thn_lulus' 			=> $pendidikan_formal_thn_lulus,
						// 'pendidikan_formal_tmp_belajar' 		=> $pendidikan_formal_tmp_belajar,
						'pendidikan_formal_lokasi' 				=> $pendidikan_formal_lokasi,
						'pendidikan_formal_no_ijazah' 			=> $pendidikan_formal_no_ijazah,
						'pendidikan_formal_nama_kep' 			=> $pendidikan_formal_nama_kep,
						'pendidikan_formal_pegawai_id' 			=> $pendidikan_formal_pegawai_id,
						'pendidikan_formal_pegawai_nip' 		=> $pendidikan_formal_pegawai_nip,
						'pendidikan_formal_status' 				=> 1,
						'pendidikan_formal_create_by' 			=> $this->session->userdata('user_id'),
						'pendidikan_formal_create_date' 		=> date('Y-m-d H:i:s')
					);
					$this->pendidikan_formal_model->create_pendidikan($datacreate);


					// $diklat_pegawai_id = $pegawai_id;
					// $diklat_pegawai_nip = str_replace(' ', '', $post["pegawai_nip"]);

					// $diklat_type = 1;
					// $diklat_jenis_id = $post['pegawai_diklat_jenis_id'];
					// $diklat_angkatan = $post['pegawai_diklat_angkatan'];
					// $diklat_penyelenggara = $post['pegawai_diklat_penyelenggara'];
					// $diklat_jumlah_jam = $post['pegawai_diklat_durasi'];
					// $diklat_tanggal_mulai = $post['pegawai_diklat_tanggal_mulai_submit'];
					// $diklat_tanggal_selesai = $post['pegawai_diklat_tanggal_selesai_submit'];
					// $diklat_predikat = $post['pegawai_diklat_predikat'];
					// $diklat_lokasi = $post['pegawai_diklat_lokasi'];
					// $diklat_nomor_sertifikat = $post['pegawai_diklat_nomor_sertifikat'];
					// $diklat_tanggal_sertifikat = $post['pegawai_diklat_tanggal_sertifikat_submit'];
					// $diklat_status = 1;
					// $diklat_nama = $post['pegawai_diklat_nama'];
					
					// $datacreate = array(
					// 	'diklat_pegawai_id' => $diklat_pegawai_id,
					// 	'diklat_pegawai_nip' => $diklat_pegawai_nip,
					// 	'diklat_type' => $diklat_type,
						// 'diklat_jenis_id' => $diklat_jenis_id,
						// 'diklat_angkatan' => $diklat_angkatan,
						// 'diklat_penyelenggara' => $diklat_penyelenggara,
						// 'diklat_jumlah_jam' => $diklat_jumlah_jam,
						// 'diklat_tanggal_mulai' => $diklat_tanggal_mulai,
						// 'diklat_tanggal_selesai' => $diklat_tanggal_selesai,
						// 'diklat_predikat' => $diklat_predikat,
						// 'diklat_lokasi' => $diklat_lokasi,
						// 'diklat_nomor_sertifikat' => $diklat_nomor_sertifikat,
						// 'diklat_tanggal_sertifikat' => $diklat_tanggal_sertifikat,
					// 	'diklat_create_by' => $this->session->userdata('user_id'),
					// 	'diklat_create_date' => date('Y-m-d H:i:s'),
					// 	'diklat_status' => 1,
					// 	'diklat_nama' => $diklat_nama
					// );
					// $this->diklat_perjenjangan_model->create_diklat($datacreate);

					$datacreate = array(
						'jenjang_jabatan_pegawai_id' => $pegawai_id,
						'jenjang_jabatan_pegawai_nip' => str_replace(' ', '', $post["pegawai_nip"]),
						'jenjang_jabatan_jabatan_id' => $post["pegawai_jabatan_id"],
						'jenjang_jabatan_jabatan_nama' => $post["pegawai_nama_jabatan"],
						'jenjang_jabatan_unit_kerja_id' => $pegawai_unit_kerja_id_fix,
						'jenjang_jabatan_tanggal_mulai' => $post["pegawai_tanggal_tmt_jabatan_submit"],
						'jenjang_jabatan_create_by' => $this->session->userdata('user_id'),
						'jenjang_jabatan_create_date' => date('Y-m-d H:i:s'),
						'jenjang_jabatan_status' => 1
					);
					$this->jabatan_model->create_jenjang_jabatan($datacreate);
					$datacreate = array(
						'kepangkatan_pegawai_id' => $pegawai_id,
						'kepangkatan_pegawai_nip' => str_replace(' ', '', $post["pegawai_nip"]),
						'kepangkatan_golongan_id' => $post["pegawai_golongan_id"],
						'kepangkatan_tanggal_tmt_golongan' => $post["pegawai_tanggal_tmt_jabatan_submit"],
						'kepangkatan_create_by' => $this->session->userdata('user_id'),
						'kepangkatan_create_date' => date('Y-m-d H:i:s'),
						'kepangkatan_status' => 1
					);
					$this->kepangkatan_model->create_kepangkatan($datacreate);
					$datacreate = array(
						'unit_kerja_pegawai_pegawai_id' => $pegawai_id,
						'unit_kerja_pegawai_pegawai_nip' => str_replace(' ', '', $post["pegawai_nip"]),
						'unit_kerja_pegawai_jabatan_id' => $post["pegawai_jabatan_id"],
						'unit_kerja_pegawai_unit_kerja_id' => $pegawai_unit_kerja_id_fix,
						'unit_kerja_pegawai_tanggal_mulai' => $post["pegawai_tanggal_tmt_jabatan_submit"],
						'unit_kerja_pegawai_create_by' => $this->session->userdata('user_id'),
						'unit_kerja_pegawai_create_date' => date('Y-m-d H:i:s'),
						'unit_kerja_pegawai_status' => 1
					);
					$this->unit_kerja_model->create_unit_kerja_pegawai($datacreate);
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Penambahan Pegawai Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/pns');
				}else{
					$notify = array(
						'title' 	=> 'Gagal!',
						'message'	=> 'Penambahan Pegawai gagal, silahkan coba lagi',
						'status' 	=> 'error'
					);
					$this->session->set_flashdata('notify', $notify);
					redirect(base_url().'kepegawaian/pns');
				}
			}

			$data = array();
			$data["provinsi"] = $this->pns_model->getProvinsi()->result_array();
			$data["jenis_kelamin"] = $this->pns_model->getJenisKelamin()->result_array();
			$data["status_perkawinan"] = $this->pns_model->getStatusKawin()->result_array();
			$data["agama"] = $this->pns_model->getAgama()->result_array();
			$data["golongan_darah"] = $this->pns_model->getGolonganDarah()->result_array();
			$data["pendidikan"] = $this->pns_model->getPendidikan()->result_array();
			$data["negara"] = $this->pns_model->getNegara()->result_array();
			$data["unit_kerja"] = $this->pns_model->getUnitKerja(1,2)->result_array();
			$data["diklat"] = $this->pns_model->getJenisDiklat()->result_array();
			$data["status_pegawai"] = $this->pns_model->getStatusPegawai()->result_array();
			$data["fungsional_tertentu"] = $this->pns_model->getFungsionalTertentu()->result_array();
			$data["fungsional_tertentu_formasi"] = $this->pns_model->getFormasiFungsionalTertentu()->result_array();
			$data["golongan"] = $this->pns_model->getGolongan()->result_array();
			$data["jabatan"] = $this->pns_model->getJabatan()->result_array();
			$this->template->display('kepegawaian/pns/create', $data);	

		}else{
			$this->access->redirect('404');
		}
	}
	public function update($pegawai_id)
	{
		if($this->access->permission('read')){
			if($post = $this->input->post()){
				$pegawai_id = $post["pegawai_id"];
				// $pegawai_unit_kerja_code = "";
				// foreach($post["pegawai_unit_kerja_id"] as $field => $value) {
				// 	if($value!=""){
				// 		$pegawai_unit_kerja_code = $value;
				// 	}
				// }
				// $datauker = $this->db->query("SELECT * FROM ms_unit_kerja WHERE unit_kerja_kode = '".$pegawai_unit_kerja_code."' AND unit_kerja_status = 1")->row_array();
				// $pegawai_unit_kerja_id_fix = $datauker["unit_kerja_id"];
				$format="";//format file name
	            $strf=(string)strftime($format);
	            $config['file_name'] = '';
	            $config['upload_path']      = './assets/images/pegawai/photo';
	            $config['allowed_types']    = 'gif|jpg|png|svg|jpeg';
	            $config['max_size']         = '20000'; // KB    
	            $this->load->library('upload', $config);
	            $file_name = "";
	            if($this->upload->do_upload('pegawai_photo')) {
	                $upload_data = array('uploads' =>$this->upload->data());
	                $file_name   =  $upload_data['uploads']['file_name'];
	            }else{
	            	$file_name = $post["pegawai_image_path"];
	            }
				$dataupdate = array(
					'pegawai_nip' => str_replace(' ', '', $post["pegawai_nip"]),
					// 'pegawai_unit_kerja_id' => $pegawai_unit_kerja_id_fix,
					// 'pegawai_unit_kerja_code' => $pegawai_unit_kerja_code,
					'pegawai_nomor_ktp' => str_replace(' ', '', $post["pegawai_nomor_ktp"]),
					'pegawai_nomor_npwp' => str_replace(' ', '', $post["pegawai_nomor_npwp"]),
					'pegawai_gelar_depan' => $post["pegawai_gelar_depan"],
					'pegawai_gelar_belakang' => $post["pegawai_gelar_belakang"],
					'pegawai_nama' => $post["pegawai_nama"],
					'pegawai_tempat_lahir' => $post["pegawai_tempat_lahir"],
					'pegawai_tanggal_lahir' => $post["pegawai_tanggal_lahir_submit"],
					'pegawai_jenis_kelamin_id' => $post["pegawai_jenis_kelamin_id"],
					'pegawai_perkawinan_id' => $post["pegawai_perkawinan_id"],
					'pegawai_agama_id' => $post["pegawai_agama_id"],
					'pegawai_golongan_darah_id' => $post["pegawai_golongan_darah_id"],
					'pegawai_domisili_negara_id' => $post["pegawai_domisili_negara_id"],
					'pegawai_domisili_provinsi_id' => $post["pegawai_domisili_provinsi_id"],
					'pegawai_domisili_kota_id' => $post["pegawai_domisili_kota_id"],
					'pegawai_domisili_kec_id' => $post["pegawai_domisili_kec_id"],
					'pegawai_domisili_kel_id' => $post["pegawai_domisili_kel_id"],
					'pegawai_domisili_alamat' => $post["pegawai_domisili_alamat"],
					'pegawai_domisili_kodepos' => $post["pegawai_domisili_kodepos"],
					'pegawai_ktp_provinsi_id' => $post["pegawai_ktp_provinsi_id"],
					'pegawai_ktp_kota_id' => $post["pegawai_ktp_kota_id"],
					'pegawai_ktp_kec_id' => $post["pegawai_ktp_kec_id"],
					'pegawai_ktp_kel_id' => $post["pegawai_ktp_kel_id"],
					'pegawai_ktp_alamat' => $post["pegawai_ktp_alamat"],
					'pegawai_ktp_kodepos' => $post["pegawai_ktp_kodepos"],
					'pegawai_email_lain' => $post["pegawai_email_lain"],
					'pegawai_email' => $post["pegawai_email"],
					'pegawai_handphone' => $post["pegawai_handphone"],
					'pegawai_nomor_karpeg' => $post["pegawai_nomor_karpeg"],
					'pegawai_nomor_askes' => $post["pegawai_nomor_askes"],
					'pegawai_nomor_karis' => $post["pegawai_nomor_karis"],
					// 'pegawai_pendidikan_terakhir_id' => $post["pegawai_pendidikan_terakhir_id"],
					// 'pegawai_pendidikan_tingkat_id' => $post["pegawai_pendidikan_tingkat_id"],
					// 'pegawai_pendidikan_fakultas_id' => $post["pegawai_pendidikan_fakultas_id"],
					// 'pegawai_pendidikan_jurusan_id' => $post["pegawai_pendidikan_jurusan_id"],
					// 'pegawai_pendidikan_program_studi' => $post["pegawai_pendidikan_program_studi"],
					// 'pegawai_pendidikan_tahun_masuk' => $post["pegawai_pendidikan_tahun_masuk"],
					// 'pegawai_pendidikan_tahun_lulus' => $post["pegawai_pendidikan_tahun_lulus"],
					// 'pegawai_pendidikan_nama' => $post["pegawai_pendidikan_nama"],
					// 'pegawai_pendidikan_tempat' => $post["pegawai_pendidikan_tempat"],
					// 'pegawai_pendidikan_ipk' => $post["pegawai_pendidikan_ipk"],
					// 'pegawai_nilai_toefl' => $post["pegawai_nilai_toefl"],
					// 'pegawai_nilai_ielt' => $post["pegawai_nilai_ielt"],
					// 'pegawai_catatan' => $post["pegawai_catatan"],
					// 'pegawai_diklat_jenis_id' => $post["pegawai_diklat_jenis_id"],
					// 'pegawai_diklat_angkatan' => $post["pegawai_diklat_angkatan"],
					// 'pegawai_diklat_nama' => $post["pegawai_diklat_nama"],
					// 'pegawai_diklat_penyelenggara' => $post["pegawai_diklat_penyelenggara"],
					// 'pegawai_diklat_durasi' => $post["pegawai_diklat_durasi"],
					// 'pegawai_diklat_tanggal_mulai' => $post["pegawai_diklat_tanggal_mulai_submit"],
					// 'pegawai_diklat_tanggal_selesai' => $post["pegawai_diklat_tanggal_selesai_submit"],
					// 'pegawai_diklat_predikat' => $post["pegawai_diklat_predikat"],
					// 'pegawai_diklat_lokasi' => $post["pegawai_diklat_lokasi"],
					// 'pegawai_diklat_nomor_sertifikat' => $post["pegawai_diklat_nomor_sertifikat"],
					// 'pegawai_diklat_tanggal_sertifikat' => $post["pegawai_diklat_tanggal_sertifikat_submit"],
					// 'pegawai_diklat_lemhanas' => $post["pegawai_diklat_lemhanas"],
					'pegawai_status_pegawai_id' => $post["pegawai_status_pegawai_id"],
					'pegawai_instansi_asal' => $post["pegawai_instansi_asal"],
					'pegawai_cpns_formasi_id' => $post["pegawai_cpns_formasi_id"],
					'pegawai_cpns_nama_formasi' => $post["pegawai_cpns_nama_formasi"],
					'pegawai_cpns_status_formasi' => $post["pegawai_cpns_status_formasi"],
					'pegawai_cpns_fungsional_catatan' => $post["pegawai_cpns_fungsional_catatan"],
					'pegawai_cpns_golongan_id' => $post["pegawai_cpns_golongan_id"],
					'pegawai_cpns_tanggal_tmt' => $post["pegawai_cpns_tanggal_tmt_submit"],
					// 'pegawai_golongan_id' => $post["pegawai_golongan_id"],
					// 'pegawai_tanggal_tmt' => $post["pegawai_tanggal_tmt_submit"],
					// 'pegawai_tahun_masa_kerja_golongan' => $post["pegawai_tahun_masa_kerja_golongan"],
					// 'pegawai_bulan_masa_kerja_golongan' => $post["pegawai_bulan_masa_kerja_golongan"],
					'pegawai_tahun_masa_kerja_berkala' => $post["pegawai_tahun_masa_kerja_berkala"],
					'pegawai_bulan_masa_kerja_berkala' => $post["pegawai_bulan_masa_kerja_berkala"],
					'pegawai_tanggal_tmt_berkala' => $post["pegawai_tanggal_tmt_berkala_submit"],
					// 'pegawai_jabatan_id' => $post["pegawai_jabatan_id"],
					'pegawai_jabatan_fungsional_id' => $post["pegawai_jabatan_fungsional_id"],
					'pegawai_tanggal_tmt_jabfung' => $post["pegawai_tanggal_tmt_jabfung_submit"],
					'pegawai_fungsional_kredit' => $post["pegawai_fungsional_kredit"],
					'pegawai_jab_fung_bidang_1' => $post["pegawai_jab_fung_bidang_1"],
					'pegawai_jab_fung_bidang_2' => $post["pegawai_jab_fung_bidang_2"],
					'pegawai_tanggal_pembebasan_tmt_jab_fung' => $post["pegawai_tanggal_pembebasan_tmt_jab_fung_submit"],
					'pegawai_tanggal_aktif_tmt_jab_fung' => $post["pegawai_tanggal_aktif_tmt_jab_fung_submit"],
					// 'pegawai_nama_jabatan' => $post["pegawai_nama_jabatan"],
					'pegawai_tanggal_tmt_jabatan' => $post["pegawai_tanggal_tmt_jabatan_submit"],
					'pegawai_telah_pra_jabatan' => $post["pegawai_telah_pra_jabatan"],
					'pegawai_tahun_pra_jabatan' => $post["pegawai_tahun_pra_jabatan"],
					'pegawai_telah_sumpah_jabatan' => $post["pegawai_telah_sumpah_jabatan"],
					'pegawai_tahun_sumpah_jabatan' => $post["pegawai_tahun_sumpah_jabatan"],
					'pegawai_telah_test_kesehatan' => $post["pegawai_telah_test_kesehatan"],
					'pegawai_tahun_test_kesehatan' => $post["pegawai_tahun_test_kesehatan"],
					'pegawai_nomor_pendidikan' => $post["pegawai_nomor_pendidikan"],
					'pegawai_cpns_fungsional_tipe' => $post["pegawai_cpns_fungsional_tipe"],
					'pegawai_image_path' => $file_name,
					'pegawai_create_by' => $this->session->userdata('user_id'),
					'pegawai_create_date' => date('Y-m-d H:i:s'),
					'pegawai_log_id' => $pegawai_id,
					'pegawai_status' => 1,
					'pegawai_is_non_pns' => 1,
					'pegawai_is_pns' => 1,
					'pegawai_sinkronisasi' => 0
				);
				// $this->db->query("UPDATE ms_pegawai SET pegawai_status = 0 WHERE pegawai_id = ".$pegawai_id);
				// echo "<pre>"; print_r($dataupdate); die;
				$pegawai_id = $this->pns_model->updatepegawai($dataupdate, $pegawai_id);//
				//$pegawai_id = $this->pns_model->updatepegawai($datainsert,$pegawai_id);//

				if($pegawai_id > 0){
					$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["pegawai_nip"])."',22,2A)");
					$this->load->model('kepegawaian/pendidikan_formal_model');
					$this->load->model('kepegawaian/diklat_perjenjangan_model');
					// $pendidikan_formal_nama = $post['pegawai_pendidikan_nama'];
					// $pendidikan_formal_pendidikan_id = $post['pegawai_pendidikan_terakhir_id'];
					// $pendidikan_formal_jurusan_code_tingkat = $post['pegawai_pendidikan_tingkat_id'];
					// $pendidikan_formal_jurusan_code_fakultas = $post['pegawai_pendidikan_fakultas_id'];
					// $pendidikan_formal_jurusan_code = $post['pegawai_pendidikan_jurusan_id'];
					// $pendidikan_formal_pro_studi = $post['pegawai_pendidikan_program_studi'];
					// $pendidikan_formal_thn_masuk = $post['pegawai_pendidikan_tahun_masuk'];
					// $pendidikan_formal_thn_lulus = $post['pegawai_pendidikan_tahun_lulus'];
					// $pendidikan_formal_tmp_belajar = $post['pegawai_pendidikan_tempat'];
					$pendidikan_formal_lokasi = "";
					$pendidikan_formal_no_ijazah = "";
					$pendidikan_formal_nama_kep = "";
					$pendidikan_formal_pegawai_id = $pegawai_id;
					$pendidikan_formal_pegawai_nip = str_replace(' ', '', $post["pegawai_nip"]);


					// $Tingkat = $this->pendidikan_formal_model->getJurusanData($pendidikan_formal_jurusan_code_tingkat)->row_array();
					// $pendidikan_formal_jurusan_id_tingkat = $Tingkat["jurusan_id"];
					// $pendidikan_formal_tingkat = $Tingkat["jurusan_grup"];
					// $Fakultas = $this->pendidikan_formal_model->getJurusanData($pendidikan_formal_jurusan_code_fakultas)->row_array();
					// $pendidikan_formal_jurusan_id_fakultas = $Fakultas["jurusan_id"];
					// $pendidikan_formal_fakultas = $Fakultas["jurusan_nama"];
					// $Jurusan = $this->pendidikan_formal_model->getJurusanData($pendidikan_formal_jurusan_code)->row_array();
					// $pendidikan_formal_jurusan_id = $Jurusan["jurusan_id"];
					// $pendidikan_formal_jurusan = $Jurusan["jurusan_nama"];
					// $Pendidikan = $this->pendidikan_formal_model->getPendidikanData($pendidikan_formal_pendidikan_id)->row_array();
					// $pendidikan_formal_pendidikan = $Pendidikan["pendidikan_nama"];
					$datacreate = array(
						// 'pendidikan_formal_nama' 				=> $pendidikan_formal_nama,
						// 'pendidikan_formal_pendidikan_id' 		=> $pendidikan_formal_pendidikan_id,
						// 'pendidikan_formal_pendidikan' 			=> $pendidikan_formal_pendidikan,
						// 'pendidikan_formal_jurusan_id_tingkat' 	=> $pendidikan_formal_jurusan_id_tingkat,
						// 'pendidikan_formal_tingkat' 			=> $pendidikan_formal_tingkat,
						// 'pendidikan_formal_jurusan_id_fakultas' => $pendidikan_formal_jurusan_id_fakultas,
						// 'pendidikan_formal_fakultas' 			=> $pendidikan_formal_fakultas,
						// 'pendidikan_formal_jurusan_id' 			=> $pendidikan_formal_jurusan_id,
						// 'pendidikan_formal_jurusan' 			=> $pendidikan_formal_jurusan,
						// 'pendidikan_formal_pro_studi' 			=> $pendidikan_formal_pro_studi,
						// 'pendidikan_formal_thn_masuk' 			=> $pendidikan_formal_thn_masuk,
						// 'pendidikan_formal_thn_lulus' 			=> $pendidikan_formal_thn_lulus,
						// 'pendidikan_formal_tmp_belajar' 		=> $pendidikan_formal_tmp_belajar,
						'pendidikan_formal_lokasi' 				=> $pendidikan_formal_lokasi,
						'pendidikan_formal_no_ijazah' 			=> $pendidikan_formal_no_ijazah,
						'pendidikan_formal_nama_kep' 			=> $pendidikan_formal_nama_kep,
						'pendidikan_formal_pegawai_id' 			=> $pendidikan_formal_pegawai_id,
						'pendidikan_formal_pegawai_nip' 		=> $pendidikan_formal_pegawai_nip,
						'pendidikan_formal_status' 				=> 1,
						'pendidikan_formal_create_by' 			=> $this->session->userdata('user_id'),
						'pendidikan_formal_create_date' 		=> date('Y-m-d H:i:s')
					);
					$this->pendidikan_formal_model->create_pendidikan($datacreate);


					$diklat_pegawai_id = $pegawai_id;
					$diklat_pegawai_nip = str_replace(' ', '', $post["pegawai_nip"]);

					$diklat_type = 1;
					// $diklat_jenis_id = $post['pegawai_diklat_jenis_id'];
					// $diklat_angkatan = $post['pegawai_diklat_angkatan'];
					// $diklat_penyelenggara = $post['pegawai_diklat_penyelenggara'];
					// $diklat_jumlah_jam = $post['pegawai_diklat_durasi'];
					// $diklat_tanggal_mulai = $post['pegawai_diklat_tanggal_mulai_submit'];
					// $diklat_tanggal_selesai = $post['pegawai_diklat_tanggal_selesai_submit'];
					// $diklat_predikat = $post['pegawai_diklat_predikat'];
					// $diklat_lokasi = $post['pegawai_diklat_lokasi'];
					// $diklat_nomor_sertifikat = $post['pegawai_diklat_nomor_sertifikat'];
					// $diklat_tanggal_sertifikat = $post['pegawai_diklat_tanggal_sertifikat_submit'];
					// $diklat_status = 1;
					// $diklat_nama = $post['pegawai_diklat_nama'];
					
					$datacreate = array(
						'diklat_pegawai_id' => $diklat_pegawai_id,
						'diklat_pegawai_nip' => $diklat_pegawai_nip,
						'diklat_type' => $diklat_type,
						// 'diklat_jenis_id' => $diklat_jenis_id,
						// 'diklat_angkatan' => $diklat_angkatan,
						// 'diklat_penyelenggara' => $diklat_penyelenggara,
						// 'diklat_jumlah_jam' => $diklat_jumlah_jam,
						// 'diklat_tanggal_mulai' => $diklat_tanggal_mulai,
						// 'diklat_tanggal_selesai' => $diklat_tanggal_selesai,
						// 'diklat_predikat' => $diklat_predikat,
						// 'diklat_lokasi' => $diklat_lokasi,
						// 'diklat_nomor_sertifikat' => $diklat_nomor_sertifikat,
						// 'diklat_tanggal_sertifikat' => $diklat_tanggal_sertifikat,
						'diklat_create_by' => $this->session->userdata('user_id'),
						'diklat_create_date' => date('Y-m-d H:i:s'),
						'diklat_status' => 1,
						// 'diklat_nama' => $diklat_nama
					);
					$this->diklat_perjenjangan_model->create_diklat($datacreate);
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Penambahan Pegawai Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/pns');
				}else{
					$notify = array(
						'title' 	=> 'Gagal!',
						'message'	=> 'Penambahan Pegawai gagal, silahkan coba lagi',
						'status' 	=> 'error'
					);
					$this->session->set_flashdata('notify', $notify);
					redirect(base_url().'kepegawaian/pns');
				}
			}

			$data = array();
			$data["pegawai"] = $this->pns_model->getPegawai($pegawai_id)->row_array();
			$data["provinsi"] = $this->pns_model->getProvinsi()->result_array();
			$data["jenis_kelamin"] = $this->pns_model->getJenisKelamin()->result_array();
			$data["status_perkawinan"] = $this->pns_model->getStatusKawin()->result_array();
			$data["agama"] = $this->pns_model->getAgama()->result_array();
			$data["golongan_darah"] = $this->pns_model->getGolonganDarah()->result_array();
			$data["pendidikan"] = $this->pns_model->getPendidikan()->result_array();
			$data["negara"] = $this->pns_model->getNegara()->result_array();
			$data["unit_kerja"] = $this->pns_model->getUnitKerja(1,2)->result_array();
			$data["diklat"] = $this->pns_model->getJenisDiklat()->result_array();
			$data["status_pegawai"] = $this->pns_model->getStatusPegawai()->result_array();
			$data["fungsional_tertentu"] = $this->pns_model->getFungsionalTertentu()->result_array();
			$data["fungsional_tertentu_formasi"] = $this->pns_model->getFormasiFungsionalTertentu()->result_array();
			$data["golongan"] = $this->pns_model->getGolongan()->result_array();
			$data["jabatan"] = $this->pns_model->getJabatan()->result_array();
			$this->template->display('kepegawaian/pns/update', $data);	

		}else{
			$this->access->redirect('404');
		}
	}
	public function checkNipExist()
	{
		$pegawai_nip = $this->input->post('pegawai_nip');
		$kota = $this->pns_model->checkNipExist($pegawai_nip)->result_array();
		$res = true;
		if(count($kota)>0){
			$res = false;
		}
		echo json_encode($res);
	}
	public function checkKTPExist()
	{
		$pegawai_nomor_ktp = $this->input->post('pegawai_nomor_ktp');
		$kota = $this->pns_model->checkKTPExist($pegawai_nomor_ktp)->result_array();
		$res = true;
		if(count($kota)>0){
			$res = false;
		}
		echo json_encode($res);
	}
	public function checkNPWPExist()
	{
		$pegawai_nomor_npwp = $this->input->post('pegawai_nomor_npwp');
		$kota = $this->pns_model->checkNPWPExist($pegawai_nomor_npwp)->result_array();
		$res = true;
		if(count($kota)>0){
			$res = false;
		}
		echo json_encode($res);
	}



	public function createbackup()
	{
		$data = array();
		$data["provinsi"] = $this->pns_model->getProvinsi()->result_array();
		$data["jenis_kelamin"] = $this->pns_model->getJenisKelamin()->result_array();
		$data["status_perkawinan"] = $this->pns_model->getStatusKawin()->result_array();
		$data["agama"] = $this->pns_model->getAgama()->result_array();
		$data["golongan_darah"] = $this->pns_model->getGolonganDarah()->result_array();
		$data["pendidikan"] = $this->pns_model->getPendidikan()->result_array();
		$data["negara"] = $this->pns_model->getNegara()->result_array();
		$data["unit_kerja"] = $this->pns_model->getUnitKerja('','')->result_array();


		$this->template->display('kepegawaian/pns/createbackup', $data);	
	}

	public function get_kota()
	{
		$provinsi_kode 	= $this->input->post('provinsi_kode');
		$data["kota"] = $this->pns_model->getKota($provinsi_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_kecamatan()
	{
		$kota_kode 	= $this->input->post('kota_kode');
		$data["kecamatan"] = $this->pns_model->getKecamatan($kota_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_kelurahan()
	{
		$kecamatan_kode 	= $this->input->post('kecamatan_kode');
		$data["kelurahan"] = $this->pns_model->getKelurahan($kecamatan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_unit_kerja_hirarki()
	{
		$unit_kerja_parent_id_kode 	= $this->input->post('unit_kerja_parent_id_kode');
		$unit_kerja_level 	= $this->input->post('unit_kerja_level');
		$data["unit_kerja"] = $this->pns_model->getUnitKerja($unit_kerja_parent_id_kode,$unit_kerja_level)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}

	public function get_tingkat_pendidikan()
	{
		$pendidikan_kode 	= $this->input->post('pendidikan_kode');
		$data["tingkat_pendidikan"] = $this->pns_model->getTingkatPendidikan($pendidikan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_jurusan()
	{
		$jurusan_kode 	= $this->input->post('jurusan_kode');
		$data["jurusan"] = $this->pns_model->getJurusan($jurusan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function list_data(){
		$default_order = "pegawai_id ASC";
		$limit = 10;
		$operator_unit_kerja_id = $this->session->userdata('user_operator_unit_kerja_id');
		$operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');
		
		if($this->session->userdata('user_akses_id') == '2'){
			// $where = "pegawai_status = 1"
        	$where = "pegawai_status = 1 and pegawai_is_pns = 1 and (pegawai_jenis_pensiun_id is null or pegawai_jenis_pensiun_id = 0) and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
			// $where = "unit_kerja_id_2 =". $operator_unit_kerja;
			// $where = "unit_kerja_id_3 =". $operator_unit_kerja;
			// $where = " and pegawai_jenis_pensiun_id is null";
			// $where = " and pegawai_telah_wafat is null";
			// if($operator_unit_kerja)$where .= " or unit_kerja_id_2 =". $operator_unit_kerja;
			// if($operator_unit_kerja)$where .= " or unit_kerja_id_3 =". $operator_unit_kerja;
			// if($operator_unit_kerja)$where .= " or unit_kerja_id_4 =". $operator_unit_kerja;
			// if($operator_unit_kerja)$where .= " or unit_kerja_id_5 =". $operator_unit_kerja;
		}
		else{
			$where = "pegawai_status = 1 AND pegawai_is_pns = 1";
		}
		
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}

		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->pns_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->pns_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->pns_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_tempat_tanggal_lahir"],
				$row["pegawai_nama_jabatan"],
				// $row["unit_kerja_nama"],
				$row["unit_kerja_hirarki_name_full"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/pns/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
}
