<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Seminar extends CI_Controller{

	function __construct(){
		parent:: __construct();
		$this->load->model('kepegawaian/seminar_model');
		//$this->load->library('access');
		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index(){
		if($this->access->permission('read')){
			$data = array();
			$this->template->display('kepegawaian/seminar/index', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function update($pegawai_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];
				$seminar_id = $post['seminar_id'];
				$seminar_nama_kegiatan = $post['seminar_nama_kegiatan'];
				$seminar_lokasi = $post['seminar_lokasi'];
				$seminar_tempat = $post['seminar_tempat'];
				$seminar_penyelenggara = $post['seminar_penyelenggara'];
				$seminar_tahun = $post['seminar_tahun'];
				$seminar_kedudukan_id = $post['seminar_kedudukan_id'];
				$seminar_lokasi = $post['seminar_lokasi'];
				$seminar_no_ijazah = $post['seminar_no_ijazah'];
				$seminar_pegawai_id = $post['seminar_pegawai_id'];
				$seminar_pegawai_nip = $post['seminar_pegawai_nip'];
			
				if($isUpdate == 0){
					$datacreate = array(
						'seminar_nama_kegiatan' 	=> $seminar_nama_kegiatan,
						'seminar_lokasi' 			=> $seminar_lokasi,
						'seminar_tempat' 			=> $seminar_tempat,
						'seminar_penyelenggara' 	=> $seminar_penyelenggara,
						'seminar_tahun' 			=> $seminar_tahun,
						'seminar_kedudukan_id' 		=> $seminar_kedudukan_id,
						'seminar_pegawai_id' 		=> $seminar_pegawai_id,
						'seminar_pegawai_nip' 		=> $seminar_pegawai_nip,
						'seminar_status' 			=> 1,
						'seminar_create_by' 		=> $this->session->userdata('user_id'),
						'seminar_create_date' 		=> date('Y-m-d H:i:s')
					);


					$insDb = $this->seminar_model->create_seminar($datacreate);

					if($insDb > 0){
						$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["seminar_pegawai_nip"])."','12','1L')");
						$notify = array(
							'title' 	=> 'Berhasil!',
							'message' 	=> 'Penambahan Data seminar Berhasil',
							'status' 	=> 'success'
						);
						$this->session->set_flashdata('notify', $notify);

						redirect(base_url().'kepegawaian/seminar/update/'.$seminar_pegawai_id);
					}else{
						$notify = array(
							'title' 	=> 'Gagal!',
							'message'	=> 'Penambahan Data seminar Gagal, silahkan coba lagi',
							'status' 	=> 'error'
						);
						$this->session->set_flashdata('notify', $notify);
						redirect(base_url().'kepegawaian/seminar/update/'.$seminar_pegawai_id);
					}
				}else{
					$dataupdate = array(
						'seminar_nama_kegiatan' 	=> $seminar_nama_kegiatan,
						'seminar_lokasi' 			=> $seminar_lokasi,
						'seminar_tempat' 			=> $seminar_tempat,
						'seminar_penyelenggara' 	=> $seminar_penyelenggara,
						'seminar_tahun' 			=> $seminar_tahun,
						'seminar_kedudukan_id' 		=> $seminar_kedudukan_id,
						'seminar_status' 			=> 1,
						'seminar_create_by' 		=> $this->session->userdata('user_id'),
						'seminar_create_date' 		=> date('Y-m-d H:i:s')
					);
					$this->seminar_model->update_seminar($dataupdate, $seminar_id);
					$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["seminar_pegawai_nip"])."','33','2L')");
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan Data seminar Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/seminar/update/'.$seminar_pegawai_id);
				}
			}

			$data = array();
			$data['pegawai']  	= $this->seminar_model->getPegawai($pegawai_id)->row_array();
			$data['history']  	= $this->seminar_model->getListseminar($pegawai_id)->result_array();
			$data['kedudukan']  	= $this->seminar_model->getKedudukan()->result_array();
			$this->template->display('kepegawaian/seminar/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function get_tingkat_seminar()
	{
		$seminar_kode 	= $this->input->post('seminar_kode');
		$data["tingkat_seminar"] = $this->seminar_model->getTingkatseminar($seminar_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_fakultas()
	{
		$jurusan_kode 	= $this->input->post('jurusan_kode');
		$data["jurusan"] = $this->seminar_model->getFakultas($jurusan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_jurusan()
	{
		$jurusan_kode 	= $this->input->post('jurusan_kode');
		$data["jurusan"] = $this->seminar_model->getJurusan($jurusan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	

	
	public function list_data_aktif(){
		$default_order = "";
		$limit = 10;
		$operator_unit_kerja = $this->session->userdata('user_operator_unit_kerja_id');
		$operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');

		if($this->session->userdata('user_akses_id') == '2'){
        	$where = "pegawai_status = 1 and pegawai_is_pns = 1 and (pegawai_jenis_pensiun_id is null or pegawai_jenis_pensiun_id = 0) and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
		}
		else{
			$where = "pegawai_status = 1 AND pegawai_is_pns = 1";
		}
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->seminar_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->seminar_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->seminar_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_nama_jabatan"],
				$row["unit_kerja_hirarki_name_full"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/seminar/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
	public function hapus($seminar_id = 0,$pegawai_id = 0){

		$seminar_idFilter = filter_var($seminar_id, FILTER_SANITIZE_NUMBER_INT);
		if($this->access->permission('delete')) {
			if($seminar_id==$seminar_idFilter) {

				$dataupdate = array(
					'seminar_status'  => 0,
					'seminar_create_by' 			=> $this->session->userdata('user_id'),
					'seminar_create_date' 			=> date('Y-m-d H:i:s')
				);

				$del = $this->seminar_model->update_seminar($dataupdate,$seminar_id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'seminar Formal Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'kepegawaian/seminar/update/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'seminar Formal Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'kepegawaian/seminar/update/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'seminar Formal Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'kepegawaian/seminar/update/'.$pegawai_id);
		}
	}

	
}