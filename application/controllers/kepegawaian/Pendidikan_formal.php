<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pendidikan_Formal extends CI_Controller{

	function __construct(){
		parent:: __construct();
		$this->load->model('kepegawaian/pendidikan_formal_model');
		$this->load->model('skp/skp_model','',TRUE);
		//$this->load->library('access');
		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index(){
		if($this->access->permission('read')){
			$data = array();
			$this->template->display('kepegawaian/pendidikan_formal/index', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function read($data_id=0){
		if($this->access->permission('read')){
			$data = array();
			$data['pegawai']  	= $this->pendidikan_formal_model->getPegawai($data_id)->row_array();
			$data['history']  	= $this->pendidikan_formal_model->getListPendidikan($data_id)->result_array();
			$data['pendidikan'] = $this->pendidikan_formal_model->getPendidikan()->result_array();
			$this->template->display('kepegawaian/pendidikan_formal/read', $data);
		}else{
			$this->access->redirect('404');
		}
	}	
	public function update($pegawai_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];
				$pendidikan_formal_id = $post['pendidikan_formal_id'];
				$pendidikan_formal_nama = $post['pendidikan_formal_nama'];
				$pendidikan_formal_pendidikan_id = $post['pendidikan_formal_pendidikan_id'];
				$pendidikan_formal_jurusan_code_tingkat = $post['pendidikan_formal_jurusan_id_tingkat'];
				$pendidikan_formal_pro_studi = $post['pendidikan_formal_pro_studi'];
				$pendidikan_formal_thn_masuk = $post['pendidikan_formal_thn_masuk'];
				$pendidikan_formal_thn_lulus = $post['pendidikan_formal_thn_lulus'];
				$pendidikan_formal_tmp_belajar = $post['pendidikan_formal_tmp_belajar'];
				$pendidikan_formal_lokasi = $post['pendidikan_formal_lokasi'];
				$pendidikan_formal_no_ijazah = $post['pendidikan_formal_no_ijazah'];
				$pendidikan_formal_nama_kep = $post['pendidikan_formal_nama_kep'];
				$pendidikan_formal_pegawai_id = $post['pendidikan_formal_pegawai_id'];
				$pendidikan_formal_pegawai_nip = $post['pendidikan_formal_pegawai_nip'];
				$pegawai_pendidikan_ipk = isset($post['pegawai_pendidikan_ipk'])?$post['pegawai_pendidikan_ipk']:'';
				$pendidikan_sekolah_universitas_id = $post['pendidikan_sekolah_universitas_id'];

				$Tingkat = $this->pendidikan_formal_model->getJurusanData($pendidikan_formal_jurusan_code_tingkat)->row_array();
				$pendidikan_formal_jurusan_id_tingkat = $Tingkat["jurusan_id"];
				$pendidikan_formal_tingkat = $Tingkat["jurusan_grup"];
				$Pendidikan = $this->pendidikan_formal_model->getPendidikanData($pendidikan_formal_pendidikan_id)->row_array();
				$pendidikan_formal_pendidikan = $Pendidikan["pendidikan_nama"];

			    // tambahan muklis
				$update_data_sort  = $this->pendidikan_formal_model->get_short_id($post['pendidikan_formal_pendidikan_id'])->result_array();
				$update_data_sort  = $update_data_sort[0]['pendidikan_sort'];                    

				$last_pendidikan        = $this->pendidikan_formal_model->get_last_pendidikan($pendidikan_formal_pegawai_id)->result_array();
				$tahun_last_pendidikan  = $last_pendidikan[0]['pendidikan_formal_thn_lulus'];
				$id_last_pendidikan     = $last_pendidikan[0]['pendidikan_formal_pendidikan_id'];
				
				$last_data_sort         = $this->pendidikan_formal_model->get_short_id($id_last_pendidikan)->result_array();
				$last_data_sort         = $last_data_sort[0]['pendidikan_sort'];
				
				$update_pegawai = 0;
				if($update_data_sort > $last_data_sort){
					$update_pegawai = 1;
				}
				else if($update_data_sort == $last_data_sort){
					if($post['pendidikan_formal_thn_lulus'] >= $tahun_last_pendidikan){
						$update_pegawai = 1;
					}					
				}
				if($update_pegawai == 1){
					$update_pendidikan_last = array(	
						'pegawai_pendidikan_terakhir_id' 		=> $post['pendidikan_formal_pendidikan_id'],
						'pegawai_pendidikan_tingkat_id' 		=> $post['pendidikan_formal_jurusan_id_tingkat'],
						'pegawai_pendidikan_program_studi' 	=> $post['pendidikan_formal_pro_studi'],
						'pegawai_pendidikan_tahun_masuk' 		=> $post['pendidikan_formal_thn_masuk'],
						'pegawai_pendidikan_tahun_lulus' 		=> $post['pendidikan_formal_thn_lulus'],
						'pegawai_pendidikan_nama' 			=> $post['pendidikan_formal_tmp_belajar'],
						'pegawai_pendidikan_tempat' 			=> $post['pendidikan_formal_lokasi']
					);					  
				}							

				if($isUpdate == 0){
					$datacreate = array(
						'pendidikan_formal_nama' 				=> $pendidikan_formal_nama,
						'pendidikan_formal_pendidikan_id' 		=> $pendidikan_formal_pendidikan_id,
						'pendidikan_formal_pendidikan' 			=> $pendidikan_formal_pendidikan,
						'pendidikan_formal_jurusan_id_tingkat' 	=> $pendidikan_formal_jurusan_id_tingkat,
						'pendidikan_formal_tingkat' 			=> $pendidikan_formal_tingkat,
						'pendidikan_formal_pro_studi' 			=> $pendidikan_formal_pro_studi,
						'pendidikan_formal_thn_masuk' 			=> $pendidikan_formal_thn_masuk,
						'pendidikan_formal_thn_lulus' 			=> $pendidikan_formal_thn_lulus,
						'pendidikan_formal_tmp_belajar' 		=> $pendidikan_formal_tmp_belajar,
						'pendidikan_formal_lokasi' 				=> $pendidikan_formal_lokasi,
						'pendidikan_formal_no_ijazah' 			=> $pendidikan_formal_no_ijazah,
						'pendidikan_formal_nama_kep' 			=> $pendidikan_formal_nama_kep,
						'pendidikan_formal_pegawai_id' 			=> $pendidikan_formal_pegawai_id,
						'pendidikan_formal_pegawai_nip' 		=> $pendidikan_formal_pegawai_nip,
						'pendidikan_sekolah_universitas_id' 	=> $pendidikan_sekolah_universitas_id,
						'pendidikan_formal_status' 				=> 1,
						'pendidikan_formal_create_by' 			=> $this->session->userdata('user_id'),
						'pendidikan_formal_create_date' 		=> date('Y-m-d H:i:s'),
						'pendidikan_formal_ipk' 				=> $post['pegawai_pendidikan_ipk']
					);
					// echo "<pre>"; print_r($datacreate); die;
					$insDb = $this->pendidikan_formal_model->create_pendidikan($datacreate);

					if($update_pegawai == 1){ //print_r($update_pendidikan_last);print $pendidikan_formal_pegawai_id;exit;
						$update_pendidikan_last = $this->pendidikan_formal_model->update_pendidikan_last($update_pendidikan_last,$pendidikan_formal_pegawai_id);					
					}
					
					if($insDb > 0){
						$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["pendidikan_formal_pegawai_nip"])."','10','1J')");
						$notify = array(
							'title' 	=> 'Berhasil!',
							'message' 	=> 'Penambahan Data Pendidikan Berhasil',
							'status' 	=> 'success'
						);
						$this->session->set_flashdata('notify', $notify);

						redirect(base_url().'kepegawaian/pendidikan_formal/update/'.$pendidikan_formal_pegawai_id);
					}else{
						$notify = array(
							'title' 	=> 'Gagal!',
							'message'	=> 'Penambahan Data Pendidikan Gagal, silahkan coba lagi',
							'status' 	=> 'error'
						);
						$this->session->set_flashdata('notify', $notify);
						redirect(base_url().'kepegawaian/pendidikan_formal/update/'.$pendidikan_formal_pegawai_id);
					}
				}else{
					$dataupdate = array(
						'pendidikan_formal_nama' 				=> $pendidikan_formal_nama,
						'pendidikan_formal_pendidikan_id' 		=> $pendidikan_formal_pendidikan_id,
						'pendidikan_formal_pendidikan' 			=> $pendidikan_formal_pendidikan,
						'pendidikan_formal_jurusan_id_tingkat' 	=> $pendidikan_formal_jurusan_id_tingkat,
						'pendidikan_formal_tingkat' 			=> $pendidikan_formal_tingkat,
						'pendidikan_formal_pro_studi' 			=> $pendidikan_formal_pro_studi,
						'pendidikan_formal_thn_masuk' 			=> $pendidikan_formal_thn_masuk,
						'pendidikan_formal_thn_lulus' 			=> $pendidikan_formal_thn_lulus,
						'pendidikan_formal_tmp_belajar' 		=> $pendidikan_formal_tmp_belajar,
						'pendidikan_formal_lokasi' 				=> $pendidikan_formal_lokasi,
						'pendidikan_formal_no_ijazah' 			=> $pendidikan_formal_no_ijazah,
						'pendidikan_formal_nama_kep' 			=> $pendidikan_formal_nama_kep,
						'pendidikan_sekolah_universitas_id' 	=> $pendidikan_sekolah_universitas_id,
						'pendidikan_formal_status' 				=> 1,
						'pendidikan_formal_create_by' 			=> $this->session->userdata('user_id'),
						'pendidikan_formal_create_date' 		=> date('Y-m-d H:i:s'),
						'pendidikan_formal_ipk' 				=> $post['pegawai_pendidikan_ipk'],
					);
					$this->pendidikan_formal_model->update_pendidikan($dataupdate, $pendidikan_formal_id);

					if($update_pegawai == 1){
						$update_pendidikan_last = $this->pendidikan_formal_model->update_pendidikan_last($update_pendidikan_last,$pendidikan_formal_pegawai_id);					
					}					
					$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["pendidikan_formal_pegawai_nip"])."','31','2J')");
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan Data Pendidikan Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/pendidikan_formal/update/'.$pendidikan_formal_pegawai_id);
				}
			}

			$data = array();
			$data['pegawai']  	= $this->pendidikan_formal_model->getPegawai($pegawai_id)->row_array();
			$data['history']  	= $this->pendidikan_formal_model->getListPendidikan($pegawai_id)->result_array();
			$data['pendidikan'] = $this->pendidikan_formal_model->getPendidikan()->result_array();
			$data['sd'] = $this->pendidikan_formal_model->getSekolahSD()->result_array();
			$data['smp'] = $this->pendidikan_formal_model->getSekolahSMP()->result_array();
			$data['sma'] = $this->pendidikan_formal_model->getSekolahSMA()->result_array();
			$data['prodi'] = $this->pendidikan_formal_model->getProdi()->result_array();
			$data['univ'] = $this->pendidikan_formal_model->getUniv()->result_array();
			// echo "<pre>";
			// print_r($data['sd']); die;
			$this->template->display('kepegawaian/pendidikan_formal/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function get_tingkat_pendidikan()
	{
		$pendidikan_kode 	= $this->input->post('pendidikan_kode');
		$data["tingkat_pendidikan"] = $this->pendidikan_formal_model->getTingkatPendidikan($pendidikan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_fakultas()
	{
		$jurusan_kode 	= $this->input->post('jurusan_kode');
		$data["jurusan"] = $this->pendidikan_formal_model->getFakultas($jurusan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_jurusan()
	{
		$jurusan_kode 	= $this->input->post('jurusan_kode');
		$data["jurusan"] = $this->pendidikan_formal_model->getJurusan($jurusan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_prodi_univ()
	{
		$univ_id 	= $this->input->post('pendidikan_formal_univ');
		$data["tingkat_univ"] = $this->pendidikan_formal_model->getProdiUniv($univ_id)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	
	public function list_data_aktif(){
		$default_order = "";
		$limit = 10;
		$operator_unit_kerja = $this->session->userdata('user_operator_unit_kerja_id');
		$operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');
		
		if($this->session->userdata('user_akses_id') == '2'){
			$where = "pegawai_status = 1 and pegawai_is_pns = 1 and (pegawai_jenis_pensiun_id is null or pegawai_jenis_pensiun_id = 0) and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
		}
		else{
			$where = "pegawai_status = 1 AND pegawai_is_pns = 1";
		}
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->pendidikan_formal_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->pendidikan_formal_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->pendidikan_formal_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_nama_jabatan"],
				$row["unit_kerja_hirarki_name_full"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/pendidikan_formal/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
	public function hapus($pendidikan_formal_id = 0,$pegawai_id = 0){

		$pendidikan_formal_idFilter = filter_var($pendidikan_formal_id, FILTER_SANITIZE_NUMBER_INT);
		if($this->access->permission('delete')) {
			if($pendidikan_formal_id==$pendidikan_formal_idFilter) {

				$dataupdate = array(
					'pendidikan_formal_status'  => 0,
					'pendidikan_formal_create_by' 			=> $this->session->userdata('user_id'),
					'pendidikan_formal_create_date' 			=> date('Y-m-d H:i:s')
				);

				$del = $this->pendidikan_formal_model->update_pendidikan($dataupdate,$pendidikan_formal_id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Pendidikan Formal Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'kepegawaian/pendidikan_formal/update/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Pendidikan Formal Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'kepegawaian/pendidikan_formal/update/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Pendidikan Formal Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'kepegawaian/pendidikan_formal/update/'.$pegawai_id);
		}
	}

   //tambahan muklis	
	public function check_tingat_pendidikan()
	{   
	    // 9  pendidikan SLTA
		$data = $this->pendidikan_formal_model->check_tingat_pendidikan($this->input->post('pendidikan_formal_pendidikan_id'),$this->input->post('pendidikan_formal_pegawai_id'))->result_array();
		$res = true;
		if($this->input->post('isUpdate') == 0){ 
			if($this->input->post('pendidikan_formal_pendidikan_id') > 8){
				if(count($data)>0){
					$res = false;
				}
			}
		}
		echo json_encode($res);
	}
	public function get_prodi()
	{
		$univ_id 	= $this->input->post('univ_id');
		$jenjang 	= $this->input->post('jenjang');
		$data["prodi"] = $this->pendidikan_formal_model->get_prodi($univ_id,$jenjang)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}

	public function get_sd()
	{
		$sd = $this->pendidikan_formal_model->getSekolahSD()->result_array();
		echo json_encode($sd);
	}

	public function data_sekolah_sd(){
		$data = $this->pendidikan_formal_model->get_sekolah_sd()->result_array();
		return $this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function cari_data_sekolah()
	{
		$json = [];


		$this->load->database();
		
		if(!empty($this->input->get("q"))){
			$this->db->like('sekolah_sd_nama', $this->input->get("q"));
			$query = $this->db->select('sekolah_sd_id,sekolah_sd_nama as text')
			->limit(10)
			->get("dbo.ms_sekolah");
			$json = $query->result();
		}
		
		echo json_encode($json);
	}

	public function getDataListPenilai($page=0)
	{
		$unit_kerja_parent_id = $this->session->userdata('unit_kerja_parent_id');
		$search = $this->input->get('q');
		$page = $this->input->get('page');
		$pegawai_id_selected = $this->input->get('pegawai_id_selected');
		$unit_kerja_id_selected = $this->input->get('unit_kerja_id_selected');
		$unit_kerja_parent_id_selected = $this->input->get('unit_kerja_parent_id_selected');
		
		$data = array();
		$data['total_count'] = count($this->skp_model->getDataListPenilaiCount($unit_kerja_parent_id_selected,$search,$pegawai_id_selected)->result_array());
		$data['incomplete_results'] = false;
		$getDataListPenilai = $this->skp_model->getDataListPenilai($unit_kerja_parent_id_selected,$search,$page,$pegawai_id_selected)->result_array();
		$data['items'] = array();
		$index = 0;
		foreach ($getDataListPenilai as $dlp) {
			$data['items'][$index]["id"] = $dlp["pegawai_id"];
			$data['items'][$index]["pegawai_id"] = $dlp["pegawai_id"];
			$data['items'][$index]["pegawai_nip"] = $dlp["pegawai_nip"];
			$data['items'][$index]["pegawai_nama"] = $dlp["pegawai_nama"];
			$data['items'][$index]["pegawai_nama_jabatan"] = $dlp["pegawai_nama_jabatan"];
			$data['items'][$index]["unit_kerja_nama"] = $dlp["unit_kerja_nama"];
			$data['items'][$index]["golongan_kode"] = $dlp["golongan_kode"];
			$data['items'][$index]["golongan_nama"] = $dlp["golongan_nama"];
			$data['items'][$index]["golongan_pangkat"] = $dlp["golongan_pangkat"];
			$data['items'][$index]["pegawai_image_path"] = $dlp["pegawai_image_path"];
			
			$index++;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function getDataSD()
	{
		$sekolah_sd_id = $this->input->post('sekolah_sd_id');
		$data = array();
		$data['sekolah_sd'] = $this->pendidikan_formal_model->getDataSD($sekolah_sd_id)->row_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function getDataListSD($page=0)
	{
		$search = $this->input->get('q');
		$page = $this->input->get('page');
		$sekolah_sd_id_selected = $this->input->get('sekolah_sd_id_selected');
		// $unit_kerja_id_selected = $this->input->get('unit_kerja_id_selected');
		// $unit_kerja_parent_id_selected = $this->input->get('unit_kerja_parent_id_selected');
		
		$data = array();
		$data['total_count'] = count($this->pendidikan_formal_model->getDataListSDCount($search,$sekolah_sd_id_selected)->result_array());
		$data['incomplete_results'] = false;
		$getDataListSD = $this->pendidikan_formal_model->getDataListSD($search,$page,$sekolah_sd_id_selected)->result_array();
		$data['items'] = array();
		$index = 0;
		foreach ($getDataListSD as $dlp) {
			$data['items'][$index]["id"] = $dlp["sekolah_sd_id"];
			$data['items'][$index]["sekolah_sd_id"] = $dlp["sekolah_sd_id"];
			$data['items'][$index]["sekolah_sd_npsn"] = $dlp["sekolah_sd_npsn"];
			$data['items'][$index]["sekolah_sd_nama"] = $dlp["sekolah_sd_nama"];
			$data['items'][$index]["sekolah_sd_alamat"] = $dlp["sekolah_sd_alamat"];

			$index++;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function getDataSMP()
	{
		$sekolah_smp_id = $this->input->post('sekolah_smp_id');
		$data = array();
		$data['sekolah_smp'] = $this->pendidikan_formal_model->getDataSMP($sekolah_smp_id)->row_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}	
	public function getDataListSMP($page=0)
	{
		$search = $this->input->get('q');
		$page = $this->input->get('page');
		$sekolah_smp_id_selected = $this->input->get('sekolah_smp_id_selected');
		
		$data = array();
		$data['total_count'] = count($this->pendidikan_formal_model->getDataListSMPCount($search,$sekolah_smp_id_selected)->result_array());
		$data['incomplete_results'] = false;
		$getDataListSMP = $this->pendidikan_formal_model->getDataListSMP($search,$page,$sekolah_smp_id_selected)->result_array();
		$data['items'] = array();
		$index = 0;
		foreach ($getDataListSMP as $dlp) {
			$data['items'][$index]["id"] = $dlp["sekolah_smp_id"];
			$data['items'][$index]["sekolah_smp_id"] = $dlp["sekolah_smp_id"];
			$data['items'][$index]["sekolah_smp_npsn"] = $dlp["sekolah_smp_npsn"];
			$data['items'][$index]["sekolah_smp_nama"] = $dlp["sekolah_smp_nama"];
			$data['items'][$index]["sekolah_smp_alamat"] = $dlp["sekolah_smp_alamat"];

			$index++;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function getDataSMA()
	{
		$sekolah_sma_id = $this->input->post('sekolah_sma_id');
		$data = array();
		$data['sekolah_sma'] = $this->pendidikan_formal_model->getDataSMA($sekolah_sma_id)->row_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}	
	public function getDataListSMA($page=0)
	{
		$search = $this->input->get('q');
		$page = $this->input->get('page');
		$sekolah_sma_id_selected = $this->input->get('sekolah_sma_id_selected');
		
		$data = array();
		$data['total_count'] = count($this->pendidikan_formal_model->getDataListSMACount($search,$sekolah_sma_id_selected)->result_array());
		$data['incomplete_results'] = false;
		$getDataListSMA = $this->pendidikan_formal_model->getDataListSMA($search,$page,$sekolah_sma_id_selected)->result_array();
		$data['items'] = array();
		$index = 0;
		foreach ($getDataListSMA as $dlp) {
			$data['items'][$index]["id"] = $dlp["sekolah_sma_id"];
			$data['items'][$index]["sekolah_sma_id"] = $dlp["sekolah_sma_id"];
			$data['items'][$index]["sekolah_sma_npsn"] = $dlp["sekolah_sma_npsn"];
			$data['items'][$index]["sekolah_sma_nama"] = $dlp["sekolah_sma_nama"];
			$data['items'][$index]["sekolah_sma_alamat"] = strtoupper($dlp["sekolah_sma_alamat"]);
			$data['items'][$index]["kab"] = strtoupper($dlp["kab"]);
			$index++;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function getDataUNIV()
	{
		$univ_id = $this->input->post('univ_id');
		$data = array();
		$data['univ'] = $this->pendidikan_formal_model->getDataUNIV($univ_id)->row_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}	
	public function getDataListUNIV($page=0)
	{
		$search = $this->input->get('q');
		$page = $this->input->get('page');
		$univ_id_selected = $this->input->get('univ_id_selected');

		$data = array();
		$data['total_count'] = count($this->pendidikan_formal_model->getDataListUNIVCount($search,$univ_id_selected)->result_array());
		$data['incomplete_results'] = false;
		$getDataListUNIV = $this->pendidikan_formal_model->getDataListUNIV($search,$page,$univ_id_selected)->result_array();
		$data['items'] = array();
		$index = 0;
		foreach ($getDataListUNIV as $dlp) {
			$data['items'][$index]["id"] = $dlp["univ_id"];
			$data['items'][$index]["univ_id"] = $dlp["univ_id"];
			$data['items'][$index]["univ_npsn"] = $dlp["univ_npsn"];
			$data['items'][$index]["univ_nama"] = $dlp["univ_nama"];
			$data['items'][$index]["univ_alamat"] = $dlp["univ_alamat"];

			$index++;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}				
}