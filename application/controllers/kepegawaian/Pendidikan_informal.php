<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pendidikan_informal extends CI_Controller{

	function __construct(){
		parent:: __construct();
		$this->load->model('kepegawaian/pendidikan_informal_model');
		//$this->load->library('access');
		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index(){
		if($this->session->userdata('user_akses_id') == 3){
			redirect('kepegawaian/pendidikan_informal/update/'.$this->session->userdata('pegawai_id'));
		}else{
			if($this->access->permission('read')){
				$data = array();
				$this->template->display('kepegawaian/pendidikan_informal/index', $data);
			}else{
				$this->access->redirect('404');
			}
		}
		// if($this->access->permission('read')){
		// 	$data = array();
		// 	$this->template->display('kepegawaian/pendidikan_informal/index', $data);
		// }else{
		// 	$this->access->redirect('404');
		// }
	}
	public function update($pegawai_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];
				$pendidikan_nonformal_id = $post['pendidikan_nonformal_id'];
				$pendidikan_nonformal_nama = $post['pendidikan_nonformal_nama'];
				$pendidikan_nonformal_tempat = $post['pendidikan_nonformal_tempat'];
				$pendidikan_nonformal_kelompok = $post['pendidikan_nonformal_kelompok'];
				$pendidikan_nonformal_lokasi = $post['pendidikan_nonformal_lokasi'];
				$pendidikan_nonformal_tanggal_mulai = $post['pendidikan_nonformal_tanggal_mulai'];
				$pendidikan_nonformal_tanggal_selesai = $post['pendidikan_nonformal_tanggal_selesai'];
				$pendidikan_nonformal_penyelenggara = $post['pendidikan_nonformal_penyelenggara'];
				$pendidikan_nonformal_jumlah_jam = $post['pendidikan_nonformal_jumlah_jam'];
				$pendidikan_nonformal_pegawai_id = $post['pendidikan_nonformal_pegawai_id'];
				$pendidikan_nonformal_pegawai_nip = $post['pendidikan_nonformal_pegawai_nip'];

				if($isUpdate == 0){
					$datacreate = array(
						'pendidikan_nonformal_nama' 				=> $pendidikan_nonformal_nama,
						'pendidikan_nonformal_tempat' 				=> $pendidikan_nonformal_tempat,
						'pendidikan_nonformal_kelompok' 			=> $pendidikan_nonformal_kelompok,
						'pendidikan_nonformal_lokasi' 				=> $pendidikan_nonformal_lokasi,
						'pendidikan_nonformal_tanggal_mulai' 		=> $pendidikan_nonformal_tanggal_mulai,
						'pendidikan_nonformal_tanggal_selesai' 		=> $pendidikan_nonformal_tanggal_selesai,
						'pendidikan_nonformal_penyelenggara' 		=> $pendidikan_nonformal_penyelenggara,
						'pendidikan_nonformal_jumlah_jam' 			=> $pendidikan_nonformal_jumlah_jam,
						'pendidikan_nonformal_status' 				=> 1,
						'pendidikan_nonformal_create_by' 			=> $this->session->userdata('user_id'),
						'pendidikan_nonformal_create_date' 			=> date('Y-m-d H:i:s'),
						'pendidikan_nonformal_pegawai_id' 			=> $pendidikan_nonformal_pegawai_id,
						'pendidikan_nonformal_pegawai_nip' 			=> $pendidikan_nonformal_pegawai_nip
					);


					$insDb = $this->pendidikan_informal_model->create_pendidikan($datacreate);

					if($insDb > 0){
						$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["pendidikan_nonformal_pegawai_nip"])."','11','1K')");
						$notify = array(
							'title' 	=> 'Berhasil!',
							'message' 	=> 'Penambahan Data Pendidikan Berhasil',
							'status' 	=> 'success'
						);
						$this->session->set_flashdata('notify', $notify);

						redirect(base_url().'kepegawaian/pendidikan_informal/update/'.$pendidikan_nonformal_pegawai_id);
					}else{
						$notify = array(
							'title' 	=> 'Gagal!',
							'message'	=> 'Penambahan Data Pendidikan Gagal, silahkan coba lagi',
							'status' 	=> 'error'
						);
						$this->session->set_flashdata('notify', $notify);
						redirect(base_url().'kepegawaian/pendidikan_informal/update/'.$pendidikan_nonformal_pegawai_id);
					}
				}else{
					$dataupdate = array(
						'pendidikan_nonformal_nama' 				=> $pendidikan_nonformal_nama,
						'pendidikan_nonformal_tempat' 				=> $pendidikan_nonformal_tempat,
						'pendidikan_nonformal_kelompok' 			=> $pendidikan_nonformal_kelompok,
						'pendidikan_nonformal_lokasi' 				=> $pendidikan_nonformal_lokasi,
						'pendidikan_nonformal_tanggal_mulai' 		=> $pendidikan_nonformal_tanggal_mulai,
						'pendidikan_nonformal_tanggal_selesai' 		=> $pendidikan_nonformal_tanggal_selesai,
						'pendidikan_nonformal_penyelenggara' 		=> $pendidikan_nonformal_penyelenggara,
						'pendidikan_nonformal_jumlah_jam' 			=> $pendidikan_nonformal_jumlah_jam,
						'pendidikan_nonformal_status' 				=> 1,
						'pendidikan_nonformal_create_by' 			=> $this->session->userdata('user_id'),
						'pendidikan_nonformal_create_date' 			=> date('Y-m-d H:i:s'),
						'pendidikan_nonformal_pegawai_id' 			=> $pendidikan_nonformal_pegawai_id,
						'pendidikan_nonformal_pegawai_nip' 			=> $pendidikan_nonformal_pegawai_nip
					);
					$this->pendidikan_informal_model->update_pendidikan($dataupdate, $pendidikan_nonformal_id);
					$this->db->query("INSERT INTO [dbo].[tabel_login] (login_username,login_tanggal_login,login_waktu_login,login_nip_edit,login_logtype_id,login_logtype_kode) VALUES('".$this->session->userdata('username')."','".date('Y-m-d')."','".date('H:i:s')."','".str_replace(' ', '', $post["pendidikan_nonformal_pegawai_nip"])."','32','2K')");
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan Data Pendidikan Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/pendidikan_informal/update/'.$pendidikan_nonformal_pegawai_id);
				}
			}

			$data = array();
			$data['pegawai']  	= $this->pendidikan_informal_model->getPegawai($pegawai_id)->row_array();
			$data['history']  	= $this->pendidikan_informal_model->getListPendidikan($pegawai_id)->result_array();
			$this->template->display('kepegawaian/pendidikan_informal/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}

	public function list_data_aktif(){
		$default_order = "";
		$limit = 10;
		$operator_unit_kerja = $this->session->userdata('user_operator_unit_kerja_id');
		$operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');

		if($this->session->userdata('user_akses_id') == '2'){
        	$where = "pegawai_status = 1 and pegawai_is_pns = 1 and (pegawai_jenis_pensiun_id is null or pegawai_jenis_pensiun_id = 0) and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
		}
		else{
			$where = "pegawai_status = 1 AND pegawai_is_pns = 1";
		}
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->pendidikan_informal_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->pendidikan_informal_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->pendidikan_informal_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_nama_jabatan"],
				$row["unit_kerja_hirarki_name_full"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/pendidikan_informal/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
	public function hapus($pendidikan_nonformal_id = 0,$pegawai_id = 0){

		$pendidikan_nonformal_idFilter = filter_var($pendidikan_nonformal_id, FILTER_SANITIZE_NUMBER_INT);
		if($this->access->permission('delete')) {
			if($pendidikan_nonformal_id==$pendidikan_nonformal_idFilter) {

				$dataupdate = array(
					'pendidikan_nonformal_status'  => 0,
					'pendidikan_nonformal_create_by' 			=> $this->session->userdata('user_id'),
					'pendidikan_nonformal_create_date' 			=> date('Y-m-d H:i:s')
				);

				$del = $this->pendidikan_informal_model->update_pendidikan($dataupdate,$pendidikan_nonformal_id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Pendidikan Informal Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'kepegawaian/pendidikan_informal/update/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Pendidikan Informal Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'kepegawaian/pendidikan_informal/update/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Pendidikan Informal Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'kepegawaian/pendidikan_informal/update/'.$pegawai_id);
		}
	}

	
}