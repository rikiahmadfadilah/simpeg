<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Berkas_elektronik extends CI_Controller{

	function __construct(){
		parent:: __construct();
		$this->load->model('kepegawaian/berkas_elektronik_model');
		//$this->load->library('access');
		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index(){
		if($this->session->userdata('user_akses_id') == 3){
			redirect('kepegawaian/berkas_elektronik/update/'.$this->session->userdata('pegawai_id'));
		}else{
			if($this->access->permission('read')){
				$data = array();
				$this->template->display('kepegawaian/berkas_elektronik/index', $data);
			}else{
				$this->access->redirect('404');
			}
		}
	}
	public function upload_berkas_elektronik($pegawai_id=0){
		if($this->access->permission('create')){
			if($post = $this->input->post()){
				$pegawai_nip = $post['pegawai_nip'];
				$berkas_nama_dokumen = $post['berkas_nama_dokumen'];
				$berkas_jenis_dokumen_id = $post['berkas_jenis_dokumen_id'];
				$format="%Y_%m_%d_j%Hm%Md%S";//format file name
				$strf=(string)strftime($format);
				$config['file_name'] = '';
				$config['upload_path']      = FCPATH. 'assets/berkas_elektronik/';
				// echo "<pre>"; print_r($config['upload_path']); die;	
				$config['allowed_types']    = 'pdf|jpg|jpeg|png|gif';
				$config['max_size']         = '3000'; // KB    
				$this->load->library('upload', $config);
				$file_name = "";
				if($this->upload->do_upload('berkas_path')) {
				    $upload_data = array('uploads' =>$this->upload->data());
				    $file_name   =  $upload_data['uploads']['client_name'];
				    $full_path   =  $upload_data['uploads']['file_name'];
				    $file_ext    =  $upload_data['uploads']['file_ext'];
				}else{
					print_r( $this->upload->display_errors() );die;
					$file_name = '';
					$file_ext  = '';
					$full_path  = '';
				}
				// echo "<pre>"; print_r($upload_data); die;
				$datacreate = array(
					'berkas_pegawai_id' => $pegawai_id,
					'berkas_pegawai_nip' => $pegawai_nip,
					'berkas_nama_dokumen' => $berkas_nama_dokumen,
					'berkas_jenis_dokumen_id' => $berkas_jenis_dokumen_id,
					'berkas_file' => $file_name,
					'berkas_path' => $full_path,
					'berkas_create_by' 	  => $this->session->userdata('user_id'),
					'berkas_create_date'   => date('Y-m-d H:i:s', time()),
					'berkas_status' 		  => 1
				);
				// echo "<pre>"; print_r($datacreate); die;

				$insDb = $this->berkas_elektronik_model->create_berkas($datacreate);

				if($insDb > 0){
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Upload SKP Final Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/berkas_elektronik/update/'.$pegawai_id);
				}else{
					$notify = array(
						'title' 	=> 'Gagal!',
						'message'	=> 'Upload SKP Final Gagal, silahkan coba lagi',
						'status' 	=> 'error'
					);
					$this->session->set_flashdata('notify', $notify);
					redirect(base_url().'kepegawaian/berkas_elektronik/update/'.$pegawai_id);
				}
			}
			$data = array();
			$data['jenis_dokumen'] = $this->berkas_elektronik_model->getJenisDokumen()->result_array();
			$data['pegawai'] = $this->berkas_elektronik_model->getPegawai($pegawai_id)->row_array();
			$this->template->display('kepegawaian/berkas_elektronik/tambah_berkas', $data);	
		}else{
			$this->access->redirect('404');
		}
	}

	public function update($pegawai_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];
				$berkas_id = $post['berkas_id'];
				$berkas_nama_dokumen = $post['berkas_nama_dokumen'];
				$berkas_jenis_dokumen_id = $post['berkas_jenis_dokumen_id'];

				if($isUpdate == 1){
					$dataupdate = array(
						'berkas_nama_dokumen' 		=> $berkas_nama_dokumen,
						'berkas_jenis_dokumen_id' 	=> $berkas_jenis_dokumen_id,
						'berkas_create_by' 			=> $this->session->userdata('user_id'),
						'berkas_create_date' 		=> date('Y-m-d H:i:s'),
					);
					$this->berkas_elektronik_model->update_berkas($dataupdate, $berkas_id);

					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan Data Berkas Elektronik Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/berkas_elektronik/update/'.$pegawai_id);
				}else{}
			}

			$data = array();
			$data['jenis_dokumen'] = $this->berkas_elektronik_model->getJenisDokumen()->result_array();
			$data['pegawai']  	= $this->berkas_elektronik_model->getPegawai($pegawai_id)->row_array();
			$data['history']  	= $this->berkas_elektronik_model->getBerkasElektronik($pegawai_id)->result_array();
			$this->template->display('kepegawaian/berkas_elektronik/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}

	public function list_data_aktif(){
		$default_order = "";
		$limit = 10;
		$operator_unit_kerja = $this->session->userdata('user_operator_unit_kerja_id');
		$operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');

		if($this->session->userdata('user_akses_id') == '2'){
        	$where = "pegawai_status = 1 and pegawai_is_pns = 1 and (pegawai_jenis_pensiun_id is null or pegawai_jenis_pensiun_id = 0) and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
		}
		else{
			$where = "pegawai_status = 1 AND pegawai_is_pns = 1";
		}
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->berkas_elektronik_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->berkas_elektronik_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->berkas_elektronik_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_nama_jabatan"],
				$row["unit_kerja_hirarki_name_full"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/berkas_elektronik/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
	public function hapus($berkas_id = 0,$pegawai_id = 0){

		$berkas_idFilter = filter_var($berkas_id, FILTER_SANITIZE_NUMBER_INT);
		if($this->access->permission('delete')) {
			if($berkas_id==$berkas_idFilter) {

				$dataupdate = array(
					'berkas_status'     => 0,
					'berkas_create_by'  => $this->session->userdata('user_id'),
					'berkas_create_date'=> date('Y-m-d H:i:s')
				);

				$del = $this->berkas_elektronik_model->update_berkas($dataupdate,$berkas_id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Berkas Elektronik Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'kepegawaian/berkas_elektronik/update/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Berkas Elektronik Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'kepegawaian/berkas_elektronik/update/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Berkas Elektronik Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'kepegawaian/berkas_elektronik/update/'.$pegawai_id);
		}
	}
  
	public function get_folder_berkas_elektronik($pegawai_nip){
		$full_path = FCPATH . '/efile/'.base64_decode($pegawai_nip);
		$data['data_folder'] = array();
		if(file_exists($full_path)){
			$data['data_folder'] = scandir($full_path);
		}
		
		$data['nip'] = base64_decode($pegawai_nip);
		$this->template->display('kepegawaian/berkas_elektronik/list_folder_efile', $data);	
	}	
}