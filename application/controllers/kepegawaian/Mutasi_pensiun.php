<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mutasi_pensiun extends CI_Controller{

	function __construct(){
		parent:: __construct();
		$this->load->model('kepegawaian/mutasi_pensiun_model');
		//$this->load->library('access');
		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index(){
		if($this->access->permission('read')){
			$data = array();
			$this->template->display('kepegawaian/mutasi_pensiun/index', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function update($pegawai_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];
				$pegawai_jenis_pensiun_id = $post['pegawai_jenis_pensiun_id'];
				$pegawai_tanggal_pensiun = $post['pegawai_tanggal_pensiun'];
				$pegawai_tanggal_wafat = $post['pegawai_tanggal_pensiun'];
				$pegawai_faktor_wafat = $post['pegawai_faktor_wafat'];
				$mutasi_departemen_tanggal = $post['pegawai_tanggal_pensiun'];
				$mutasi_departemen_ke = $post['mutasi_departemen_ke'];
				$pegawai_id = $post['pegawai_id'];
				$pegawai_nip = $post['pegawai_nip'];
				if($pegawai_jenis_pensiun_id == 99){
					$jenis_pensiun = '';
					$pegawai_status = '1';
				}else{
					$jenis_pensiun = $post['pegawai_jenis_pensiun_id'];
					$pegawai_status = '0';
				}
				
				if($isUpdate == 0){
					$dataupdate = array(
						'pegawai_jenis_pensiun_id' 			=> $jenis_pensiun,
						'pegawai_tanggal_pensiun' 			=> $pegawai_tanggal_pensiun,
						'pegawai_tanggal_wafat' 			=> $pegawai_tanggal_pensiun,
						'pegawai_faktor_wafat' 				=> $pegawai_faktor_wafat,
						// 'mutasi_departemen_tanggal' 		=> $pegawai_tanggal_pensiun,
						'mutasi_departemen_ke' 				=> $mutasi_departemen_ke,
						'pegawai_status' 					=> $pegawai_status,
						'pegawai_create_by' 				=> $this->session->userdata('user_id'),
						'pegawai_create_date' 				=> date('Y-m-d H:i:s')
					);
					// echo "<pre>";
					// print_r($dataupdate);die;
					$insDb = $this->mutasi_pensiun_model->update_mutasi_pensiun($dataupdate, $pegawai_id);

					if($insDb > 0){
						$this->db->query("UPDATE ms_pegawai SET pegawai_sinkronisasi = 0 WHERE pegawai_id = ".$pegawai_id);
						$notify = array(
							'title' 	=> 'Berhasil!',
							'message' 	=> 'Penambahan Data Kemampuan Bahasa Berhasil',
							'status' 	=> 'success'
						);
						$this->session->set_flashdata('notify', $notify);

						redirect(base_url().'kepegawaian/mutasi_pensiun/update/'.$pegawai_id);
					}else{
						$notify = array(
							'title' 	=> 'Gagal!',
							'message'	=> 'Penambahan Data Kemampuan Bahasa Gagal, silahkan coba lagi',
							'status' 	=> 'error'
						);
						$this->session->set_flashdata('notify', $notify);
						redirect(base_url().'kepegawaian/mutasi_pensiun/update/'.$pegawai_id);
					}
				}
			}

			$data = array();
			$data['pegawai']  	= $this->mutasi_pensiun_model->getPegawai($pegawai_id)->row_array();
			$data['history']  	= $this->mutasi_pensiun_model->getListmutasi_pensiun($pegawai_id)->result_array();
			$data['jenis_pensiun']  	= $this->mutasi_pensiun_model->getJenisPensiun()->result_array();
			$this->template->display('kepegawaian/mutasi_pensiun/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	
	public function list_data_aktif(){
		$default_order = "";
		$limit = 10;
		$operator_unit_kerja = $this->session->userdata('user_operator_unit_kerja_id');
		$operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');

		if($this->session->userdata('user_akses_id') == '2'){
        	$where = "pegawai_is_pns = 1 and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
		}
		else{
			$where = "pegawai_is_pns = 1";
		}
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->mutasi_pensiun_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->mutasi_pensiun_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->mutasi_pensiun_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				// $pensiun = dateEnToID($row["pegawai_tanggal_pensiun"], 'd-m-Y');
				// if($pensiun = '01-01-1970'){
				// 	$tmt_pensiun = '';
				// }else{
				// 	$tmt_pensiun = $pensiun;
				// }
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_nama_jabatan"],
				$row["unit_kerja_hirarki_name_full"],
				$row["jenis_pensiun_nama"],
				$row["pegawai_tanggal_pensiun"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/mutasi_pensiun/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
}