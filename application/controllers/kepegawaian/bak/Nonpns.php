<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NonPns extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('auth/auth_model','',TRUE);
		$this->load->model('kepegawaian/pns/pns_model','',TRUE);
	}

	public function index()
	{
		$data = array();
		$this->template->display('kepegawaian/nonpns/index', $data);	
	}
	public function create()
	{
		if($this->access->permission('read')){
			if($post = $this->input->post()){
				/*echo "<pre>";
				print_r($post);
				die();*/
				$pegawai_unit_kerja_code = "";
				foreach($post["pegawai_unit_kerja_id"] as $field => $value) {
					if($value!=""){
						$pegawai_unit_kerja_code = $value;
					}
				}
				$datauker = $this->db->query("SELECT * FROM ms_unit_kerja WHERE unit_kerja_kode = '".$pegawai_unit_kerja_code."' AND unit_kerja_status = 1")->row_array();
				$pegawai_unit_kerja_id_fix = $datauker["unit_kerja_id"];
				$datainsert = array(
					'pegawai_nip' => str_replace(' ', '', $post["pegawai_nip"]),
					'pegawai_unit_kerja_id' => $pegawai_unit_kerja_id_fix,
					'pegawai_unit_kerja_code' => $pegawai_unit_kerja_code,
					'pegawai_nomor_ktp' => str_replace(' ', '', $post["pegawai_nomor_ktp"]),
					'pegawai_nomor_npwp' => str_replace(' ', '', $post["pegawai_nomor_npwp"]),
					'pegawai_gelar_depan' => $post["pegawai_gelar_depan"],
					'pegawai_gelar_belakang' => $post["pegawai_gelar_belakang"],
					'pegawai_nama' => $post["pegawai_nama"],
					'pegawai_tempat_lahir' => $post["pegawai_tempat_lahir"],
					'pegawai_tanggal_lahir' => $post["pegawai_tanggal_lahir_submit"],
					'pegawai_jenis_kelamin_id' => $post["pegawai_jenis_kelamin_id"],
					'pegawai_perkawinan_id' => $post["pegawai_perkawinan_id"],
					'pegawai_agama_id' => $post["pegawai_agama_id"],
					'pegawai_golongan_darah_id' => $post["pegawai_golongan_darah_id"],
					'pegawai_domisili_provinsi_id' => $post["pegawai_domisili_provinsi_id"],
					'pegawai_domisili_kota_id' => $post["pegawai_domisili_kota_id"],
					'pegawai_domisili_kec_id' => $post["pegawai_domisili_kec_id"],
					'pegawai_domisili_kel_id' => $post["pegawai_domisili_kel_id"],
					'pegawai_domisili_alamat' => $post["pegawai_domisili_alamat"],
					'pegawai_domisili_kodepos' => $post["pegawai_domisili_kodepos"],
					'pegawai_ktp_provinsi_id' => $post["pegawai_ktp_provinsi_id"],
					'pegawai_ktp_kota_id' => $post["pegawai_ktp_kota_id"],
					'pegawai_ktp_kec_id' => $post["pegawai_ktp_kec_id"],
					'pegawai_ktp_kel_id' => $post["pegawai_ktp_kel_id"],
					'pegawai_ktp_alamat' => $post["pegawai_ktp_alamat"],
					'pegawai_ktp_kodepos' => $post["pegawai_ktp_kodepos"],
					'pegawai_email_lain' => $post["pegawai_email_lain"],
					'pegawai_handphone' => $post["pegawai_handphone"],
					'pegawai_pendidikan_terakhir_id' => $post["pegawai_pendidikan_terakhir_id"],
					'pegawai_pendidikan_tingkat_id' => $post["pegawai_pendidikan_tingkat_id"],
					'pegawai_pendidikan_fakultas_id' => $post["pegawai_pendidikan_fakultas_id"],
					'pegawai_pendidikan_jurusan_id' => $post["pegawai_pendidikan_jurusan_id"],
					'pegawai_pendidikan_program_studi' => $post["pegawai_pendidikan_program_studi"],
					'pegawai_pendidikan_tahun_masuk' => $post["pegawai_pendidikan_tahun_masuk"],
					'pegawai_pendidikan_tahun_lulus' => $post["pegawai_pendidikan_tahun_lulus"],
					'pegawai_pendidikan_nama' => $post["pegawai_pendidikan_nama"],
					'pegawai_pendidikan_tempat' => $post["pegawai_pendidikan_tempat"],
					'pegawai_pendidikan_ipk' => $post["pegawai_pendidikan_ipk"],
					'pegawai_nilai_toefl' => $post["pegawai_nilai_toefl"],
					'pegawai_nilai_ielt' => $post["pegawai_nilai_ielt"],
					'pegawai_catatan' => $post["pegawai_catatan"],
					'pegawai_cpns_tanggal_tmt' => $post["pegawai_cpns_tanggal_tmt_submit"],
					'pegawai_nama_jabatan' => $post["pegawai_nama_jabatan"],
					'pegawai_image_path' => '950002236.jpg',
					'pegawai_is_non_pns' => '0',
					'pegawai_create_by' => $this->session->userdata('user_id'),
					'pegawai_create_date' => date('Y-m-d H:i:s'),
					'pegawai_status' => 1
				);

				$pegawai_id = $this->pns_model->createpegawai($datainsert);//
				if($pegawai_id > 0){
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Penambahan Pegawai Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/pns');
				}else{
					$notify = array(
						'title' 	=> 'Gagal!',
						'message'	=> 'Penambahan Pegawai gagal, silahkan coba lagi',
						'status' 	=> 'error'
					);
					$this->session->set_flashdata('notify', $notify);
					redirect(base_url().'kepegawaian/pns');
				}
			}

			$data = array();
			$data["provinsi"] = $this->pns_model->getProvinsi()->result_array();
			$data["jenis_kelamin"] = $this->pns_model->getJenisKelamin()->result_array();
			$data["status_perkawinan"] = $this->pns_model->getStatusKawin()->result_array();
			$data["agama"] = $this->pns_model->getAgama()->result_array();
			$data["golongan_darah"] = $this->pns_model->getGolonganDarah()->result_array();
			$data["pendidikan"] = $this->pns_model->getPendidikan()->result_array();
			$data["negara"] = $this->pns_model->getNegara()->result_array();
			$data["unit_kerja"] = $this->pns_model->getUnitKerja(1,2)->result_array();
			$data["diklat"] = $this->pns_model->getJenisDiklat()->result_array();
			$data["status_pegawai"] = $this->pns_model->getStatusPegawai()->result_array();
			$data["fungsional_tertentu"] = $this->pns_model->getFungsionalTertentu()->result_array();
			$data["fungsional_tertentu_formasi"] = $this->pns_model->getFormasiFungsionalTertentu()->result_array();
			$data["golongan"] = $this->pns_model->getGolongan()->result_array();
			$data["jabatan"] = $this->pns_model->getJabatan()->result_array();
			$this->template->display('kepegawaian/nonpns/create', $data);	

		}else{
			$this->access->redirect('404');
		}
	}
	public function get_kota()
	{
		$provinsi_kode 	= $this->input->post('provinsi_kode');
		$data["kota"] = $this->pns_model->getKota($provinsi_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_tingkat_pendidikan()
	{
		$pendidikan_kode 	= $this->input->post('pendidikan_kode');
		$data["tingkat_pendidikan"] = $this->pns_model->getTingkatPendidikan($pendidikan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_jurusan()
	{
		$jurusan_kode 	= $this->input->post('jurusan_kode');
		$data["jurusan"] = $this->pns_model->getJurusan($jurusan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function list_data(){
		$default_order = "pegawai_id";
		$limit = 10;
		$where = "pegawai_status = 1 AND pegawai_is_pns = 0 ".(($this->session->userdata('filter_where') != "")?"AND ".$this->session->userdata('filter_where'):"");
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama',
			'pegawai_tempat_tanggal_lahir',
			'pegawai_nama_jabatan',
			'unit_kerja_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}

		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->pns_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->pns_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->pns_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_tempat_tanggal_lahir"],
				$row["pegawai_nama_jabatan"],
				$row["unit_kerja_nama"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/nonpns/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
}
