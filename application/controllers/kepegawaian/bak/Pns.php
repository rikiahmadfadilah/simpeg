<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pns extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('auth/auth_model','',TRUE);
		$this->load->model('kepegawaian/pns/pns_model','',TRUE);
	}

	public function index()
	{
		$data = array();
		$this->template->display('kepegawaian/pns/index', $data);
	}
	public function create()
	{
		if($this->access->permission('read')){
			if($post = $this->input->post()){
				$pegawai_unit_kerja_code = "";
				foreach($post["pegawai_unit_kerja_id"] as $field => $value) {
					if($value!=""){
						$pegawai_unit_kerja_code = $value;
					}
				}
				$datauker = $this->db->query("SELECT * FROM ms_unit_kerja WHERE unit_kerja_kode = '".$pegawai_unit_kerja_code."' AND unit_kerja_status = 1")->row_array();
				$pegawai_unit_kerja_id_fix = $datauker["unit_kerja_id"];
				$datainsert = array(
					'pegawai_nip' => str_replace(' ', '', $post["pegawai_nip"]),
					'pegawai_unit_kerja_id' => $pegawai_unit_kerja_id_fix,
					'pegawai_unit_kerja_code' => $pegawai_unit_kerja_code,
					'pegawai_nomor_ktp' => str_replace(' ', '', $post["pegawai_nomor_ktp"]),
					'pegawai_nomor_npwp' => str_replace(' ', '', $post["pegawai_nomor_npwp"]),
					'pegawai_gelar_depan' => $post["pegawai_gelar_depan"],
					'pegawai_gelar_belakang' => $post["pegawai_gelar_belakang"],
					'pegawai_nama' => $post["pegawai_nama"],
					'pegawai_tempat_lahir' => $post["pegawai_tempat_lahir"],
					'pegawai_tanggal_lahir' => $post["pegawai_tanggal_lahir_submit"],
					'pegawai_jenis_kelamin_id' => $post["pegawai_jenis_kelamin_id"],
					'pegawai_perkawinan_id' => $post["pegawai_perkawinan_id"],
					'pegawai_agama_id' => $post["pegawai_agama_id"],
					'pegawai_golongan_darah_id' => $post["pegawai_golongan_darah_id"],
					'pegawai_domisili_negara_id' => $post["pegawai_domisili_negara_id"],
					'pegawai_domisili_provinsi_id' => $post["pegawai_domisili_provinsi_id"],
					'pegawai_domisili_kota_id' => $post["pegawai_domisili_kota_id"],
					'pegawai_domisili_kec_id' => $post["pegawai_domisili_kec_id"],
					'pegawai_domisili_kel_id' => $post["pegawai_domisili_kel_id"],
					'pegawai_domisili_alamat' => $post["pegawai_domisili_alamat"],
					'pegawai_domisili_kodepos' => $post["pegawai_domisili_kodepos"],
					'pegawai_ktp_provinsi_id' => $post["pegawai_ktp_provinsi_id"],
					'pegawai_ktp_kota_id' => $post["pegawai_ktp_kota_id"],
					'pegawai_ktp_kec_id' => $post["pegawai_ktp_kec_id"],
					'pegawai_ktp_kel_id' => $post["pegawai_ktp_kel_id"],
					'pegawai_ktp_alamat' => $post["pegawai_ktp_alamat"],
					'pegawai_ktp_kodepos' => $post["pegawai_ktp_kodepos"],
					'pegawai_email_lain' => $post["pegawai_email_lain"],
					'pegawai_email' => $post["pegawai_email"],
					'pegawai_handphone' => $post["pegawai_handphone"],
					'pegawai_nomor_karpeg' => $post["pegawai_nomor_karpeg"],
					'pegawai_nomor_askes' => $post["pegawai_nomor_askes"],
					'pegawai_nomor_karis' => $post["pegawai_nomor_karis"],
					'pegawai_pendidikan_terakhir_id' => $post["pegawai_pendidikan_terakhir_id"],
					'pegawai_pendidikan_tingkat_id' => $post["pegawai_pendidikan_tingkat_id"],
					'pegawai_pendidikan_fakultas_id' => $post["pegawai_pendidikan_fakultas_id"],
					'pegawai_pendidikan_jurusan_id' => $post["pegawai_pendidikan_jurusan_id"],
					'pegawai_pendidikan_program_studi' => $post["pegawai_pendidikan_program_studi"],
					'pegawai_pendidikan_tahun_masuk' => $post["pegawai_pendidikan_tahun_masuk"],
					'pegawai_pendidikan_tahun_lulus' => $post["pegawai_pendidikan_tahun_lulus"],
					'pegawai_pendidikan_nama' => $post["pegawai_pendidikan_nama"],
					'pegawai_pendidikan_tempat' => $post["pegawai_pendidikan_tempat"],
					'pegawai_pendidikan_ipk' => $post["pegawai_pendidikan_ipk"],
					'pegawai_nilai_toefl' => $post["pegawai_nilai_toefl"],
					'pegawai_nilai_ielt' => $post["pegawai_nilai_ielt"],
					'pegawai_catatan' => $post["pegawai_catatan"],
					'pegawai_diklat_jenis_id' => $post["pegawai_diklat_jenis_id"],
					'pegawai_diklat_angkatan' => $post["pegawai_diklat_angkatan"],
					'pegawai_diklat_nama' => $post["pegawai_diklat_nama"],
					'pegawai_diklat_penyelenggara' => $post["pegawai_diklat_penyelenggara"],
					'pegawai_diklat_durasi' => $post["pegawai_diklat_durasi"],
					'pegawai_diklat_tanggal_mulai' => $post["pegawai_diklat_tanggal_mulai_submit"],
					'pegawai_diklat_tanggal_selesai' => $post["pegawai_diklat_tanggal_selesai_submit"],
					'pegawai_diklat_predikat' => $post["pegawai_diklat_predikat"],
					'pegawai_diklat_lokasi' => $post["pegawai_diklat_lokasi"],
					'pegawai_diklat_nomor_sertifikat' => $post["pegawai_diklat_nomor_sertifikat"],
					'pegawai_diklat_tanggal_sertifikat' => $post["pegawai_diklat_tanggal_sertifikat_submit"],
					'pegawai_diklat_lemhanas' => $post["pegawai_diklat_lemhanas"],
					'pegawai_status_pegawai_id' => $post["pegawai_status_pegawai_id"],
					'pegawai_instansi_asal' => $post["pegawai_instansi_asal"],
					'pegawai_cpns_formasi_id' => $post["pegawai_cpns_formasi_id"],
					'pegawai_cpns_nama_formasi' => $post["pegawai_cpns_nama_formasi"],
					'pegawai_cpns_status_formasi' => $post["pegawai_cpns_status_formasi"],
					'pegawai_cpns_fungsional_catatan' => $post["pegawai_cpns_fungsional_catatan"],
					'pegawai_cpns_golongan_id' => $post["pegawai_cpns_golongan_id"],
					'pegawai_cpns_tanggal_tmt' => $post["pegawai_cpns_tanggal_tmt_submit"],
					'pegawai_golongan_id' => $post["pegawai_golongan_id"],
					'pegawai_tanggal_tmt' => $post["pegawai_tanggal_tmt_submit"],
					'pegawai_tahun_masa_kerja_golongan' => $post["pegawai_tahun_masa_kerja_golongan"],
					'pegawai_bulan_masa_kerja_golongan' => $post["pegawai_bulan_masa_kerja_golongan"],
					'pegawai_tahun_masa_kerja_berkala' => $post["pegawai_tahun_masa_kerja_berkala"],
					'pegawai_bulan_masa_kerja_berkala' => $post["pegawai_bulan_masa_kerja_berkala"],
					'pegawai_tanggal_tmt_berkala' => $post["pegawai_tanggal_tmt_berkala_submit"],
					'pegawai_jabatan_id' => $post["pegawai_jabatan_id"],
					'pegawai_jabatan_fungsional_id' => $post["pegawai_jabatan_fungsional_id"],
					'pegawai_tanggal_tmt_jabfung' => $post["pegawai_tanggal_tmt_jabfung_submit"],
					'pegawai_fungsional_kredit' => $post["pegawai_fungsional_kredit"],
					'pegawai_jab_fung_bidang_1' => $post["pegawai_jab_fung_bidang_1"],
					'pegawai_jab_fung_bidang_2' => $post["pegawai_jab_fung_bidang_2"],
					'pegawai_tanggal_pembebasan_tmt_jab_fung' => $post["pegawai_tanggal_pembebasan_tmt_jab_fung_submit"],
					'pegawai_tanggal_aktif_tmt_jab_fung' => $post["pegawai_tanggal_aktif_tmt_jab_fung_submit"],
					'pegawai_nama_jabatan' => $post["pegawai_nama_jabatan"],
					'pegawai_tanggal_tmt_jabatan' => $post["pegawai_tanggal_tmt_jabatan_submit"],
					'pegawai_telah_pra_jabatan' => $post["pegawai_telah_pra_jabatan"],
					'pegawai_tahun_pra_jabatan' => $post["pegawai_tahun_pra_jabatan"],
					'pegawai_telah_sumpah_jabatan' => $post["pegawai_telah_sumpah_jabatan"],
					'pegawai_tahun_sumpah_jabatan' => $post["pegawai_tahun_sumpah_jabatan"],
					'pegawai_telah_test_kesehatan' => $post["pegawai_telah_test_kesehatan"],
					'pegawai_tahun_test_kesehatan' => $post["pegawai_tahun_test_kesehatan"],
					'pegawai_cpns_fungsional_tipe' => $post["pegawai_cpns_fungsional_tipe"],
					'pegawai_nomor_pendidikan' => $post["pegawai_nomor_pendidikan"],
					'pegawai_image_path' => '950002236.jpg',
					'pegawai_create_by' => $this->session->userdata('user_id'),
					'pegawai_create_date' => date('Y-m-d H:i:s'),
					'pegawai_status' => 1
				);

				$pegawai_id = $this->pns_model->createpegawai($datainsert);//
				if($pegawai_id > 0){

					$this->db->query("INSERT INTO  [Master].[user] (user_nip,username,password,user_status,user_akses_id,user_default_akses_id) VALUES('".str_replace(' ', '', $post["pegawai_nip"])."','".str_replace(' ', '', $post["pegawai_nip"])."','5f4dcc3b5aa765d61d8327deb882cf99',1,1,1)");
					$this->load->model('kepegawaian/pendidikan_formal_model');
					$this->load->model('kepegawaian/diklat_perjenjangan_model');
					$pendidikan_formal_nama = $post['pegawai_pendidikan_nama'];
					$pendidikan_formal_pendidikan_id = $post['pegawai_pendidikan_terakhir_id'];
					$pendidikan_formal_jurusan_code_tingkat = $post['pegawai_pendidikan_tingkat_id'];
					$pendidikan_formal_jurusan_code_fakultas = $post['pegawai_pendidikan_fakultas_id'];
					$pendidikan_formal_jurusan_code = $post['pegawai_pendidikan_jurusan_id'];
					$pendidikan_formal_pro_studi = $post['pegawai_pendidikan_program_studi'];
					$pendidikan_formal_thn_masuk = $post['pegawai_pendidikan_tahun_masuk'];
					$pendidikan_formal_thn_lulus = $post['pegawai_pendidikan_tahun_lulus'];
					$pendidikan_formal_tmp_belajar = $post['pegawai_pendidikan_tempat'];
					$pendidikan_formal_lokasi = "";
					$pendidikan_formal_no_ijazah = "";
					$pendidikan_formal_nama_kep = "";
					$pendidikan_formal_pegawai_id = $pegawai_id;
					$pendidikan_formal_pegawai_nip = str_replace(' ', '', $post["pegawai_nip"]);


					$Tingkat = $this->pendidikan_formal_model->getJurusanData($pendidikan_formal_jurusan_code_tingkat)->row_array();
					$pendidikan_formal_jurusan_id_tingkat = $Tingkat["jurusan_id"];
					$pendidikan_formal_tingkat = $Tingkat["jurusan_grup"];
					$Fakultas = $this->pendidikan_formal_model->getJurusanData($pendidikan_formal_jurusan_code_fakultas)->row_array();
					$pendidikan_formal_jurusan_id_fakultas = $Fakultas["jurusan_id"];
					$pendidikan_formal_fakultas = $Fakultas["jurusan_nama"];
					$Jurusan = $this->pendidikan_formal_model->getJurusanData($pendidikan_formal_jurusan_code)->row_array();
					$pendidikan_formal_jurusan_id = $Jurusan["jurusan_id"];
					$pendidikan_formal_jurusan = $Jurusan["jurusan_nama"];
					$Pendidikan = $this->pendidikan_formal_model->getPendidikanData($pendidikan_formal_pendidikan_id)->row_array();
					$pendidikan_formal_pendidikan = $Pendidikan["pendidikan_nama"];
					$datacreate = array(
						'pendidikan_formal_nama' 				=> $pendidikan_formal_nama,
						'pendidikan_formal_pendidikan_id' 		=> $pendidikan_formal_pendidikan_id,
						'pendidikan_formal_pendidikan' 			=> $pendidikan_formal_pendidikan,
						'pendidikan_formal_jurusan_id_tingkat' 	=> $pendidikan_formal_jurusan_id_tingkat,
						'pendidikan_formal_tingkat' 			=> $pendidikan_formal_tingkat,
						'pendidikan_formal_jurusan_id_fakultas' => $pendidikan_formal_jurusan_id_fakultas,
						'pendidikan_formal_fakultas' 			=> $pendidikan_formal_fakultas,
						'pendidikan_formal_jurusan_id' 			=> $pendidikan_formal_jurusan_id,
						'pendidikan_formal_jurusan' 			=> $pendidikan_formal_jurusan,
						'pendidikan_formal_pro_studi' 			=> $pendidikan_formal_pro_studi,
						'pendidikan_formal_thn_masuk' 			=> $pendidikan_formal_thn_masuk,
						'pendidikan_formal_thn_lulus' 			=> $pendidikan_formal_thn_lulus,
						'pendidikan_formal_tmp_belajar' 		=> $pendidikan_formal_tmp_belajar,
						'pendidikan_formal_lokasi' 				=> $pendidikan_formal_lokasi,
						'pendidikan_formal_no_ijazah' 			=> $pendidikan_formal_no_ijazah,
						'pendidikan_formal_nama_kep' 			=> $pendidikan_formal_nama_kep,
						'pendidikan_formal_pegawai_id' 			=> $pendidikan_formal_pegawai_id,
						'pendidikan_formal_pegawai_nip' 		=> $pendidikan_formal_pegawai_nip,
						'pendidikan_formal_status' 				=> 1,
						'pendidikan_formal_create_by' 			=> $this->session->userdata('user_id'),
						'pendidikan_formal_create_date' 		=> date('Y-m-d H:i:s')
					);
					$this->pendidikan_formal_model->create_pendidikan($datacreate);


					$diklat_pegawai_id = $pegawai_id;
					$diklat_pegawai_nip = str_replace(' ', '', $post["pegawai_nip"]);

					$diklat_type = 1;
					$diklat_jenis_id = $post['pegawai_diklat_jenis_id'];
					$diklat_angkatan = $post['pegawai_diklat_angkatan'];
					$diklat_penyelenggara = $post['pegawai_diklat_penyelenggara'];
					$diklat_jumlah_jam = $post['pegawai_diklat_durasi'];
					$diklat_tanggal_mulai = $post['pegawai_diklat_tanggal_mulai_submit'];
					$diklat_tanggal_selesai = $post['pegawai_diklat_tanggal_selesai_submit'];
					$diklat_predikat = $post['pegawai_diklat_predikat'];
					$diklat_lokasi = $post['pegawai_diklat_lokasi'];
					$diklat_nomor_sertifikat = $post['pegawai_diklat_nomor_sertifikat'];
					$diklat_tanggal_sertifikat = $post['pegawai_diklat_tanggal_sertifikat_submit'];
					$diklat_status = 1;
					$diklat_nama = $post['pegawai_diklat_nama'];
					
					$datacreate = array(
						'diklat_pegawai_id' => $diklat_pegawai_id,
						'diklat_pegawai_nip' => $diklat_pegawai_nip,
						'diklat_type' => $diklat_type,
						'diklat_jenis_id' => $diklat_jenis_id,
						'diklat_angkatan' => $diklat_angkatan,
						'diklat_penyelenggara' => $diklat_penyelenggara,
						'diklat_jumlah_jam' => $diklat_jumlah_jam,
						'diklat_tanggal_mulai' => $diklat_tanggal_mulai,
						'diklat_tanggal_selesai' => $diklat_tanggal_selesai,
						'diklat_predikat' => $diklat_predikat,
						'diklat_lokasi' => $diklat_lokasi,
						'diklat_nomor_sertifikat' => $diklat_nomor_sertifikat,
						'diklat_tanggal_sertifikat' => $diklat_tanggal_sertifikat,
						'diklat_create_by' => $this->session->userdata('user_id'),
						'diklat_create_date' => date('Y-m-d H:i:s'),
						'diklat_status' => 1,
						'diklat_nama' => $diklat_nama
					);
					$this->diklat_perjenjangan_model->create_diklat($datacreate);
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Penambahan Pegawai Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/pns');
				}else{
					$notify = array(
						'title' 	=> 'Gagal!',
						'message'	=> 'Penambahan Pegawai gagal, silahkan coba lagi',
						'status' 	=> 'error'
					);
					$this->session->set_flashdata('notify', $notify);
					redirect(base_url().'kepegawaian/pns');
				}
			}

			$data = array();
			$data["provinsi"] = $this->pns_model->getProvinsi()->result_array();
			$data["jenis_kelamin"] = $this->pns_model->getJenisKelamin()->result_array();
			$data["status_perkawinan"] = $this->pns_model->getStatusKawin()->result_array();
			$data["agama"] = $this->pns_model->getAgama()->result_array();
			$data["golongan_darah"] = $this->pns_model->getGolonganDarah()->result_array();
			$data["pendidikan"] = $this->pns_model->getPendidikan()->result_array();
			$data["negara"] = $this->pns_model->getNegara()->result_array();
			$data["unit_kerja"] = $this->pns_model->getUnitKerja(1,2)->result_array();
			$data["diklat"] = $this->pns_model->getJenisDiklat()->result_array();
			$data["status_pegawai"] = $this->pns_model->getStatusPegawai()->result_array();
			$data["fungsional_tertentu"] = $this->pns_model->getFungsionalTertentu()->result_array();
			$data["fungsional_tertentu_formasi"] = $this->pns_model->getFormasiFungsionalTertentu()->result_array();
			$data["golongan"] = $this->pns_model->getGolongan()->result_array();
			$data["jabatan"] = $this->pns_model->getJabatan()->result_array();
			$this->template->display('kepegawaian/pns/create', $data);	

		}else{
			$this->access->redirect('404');
		}
	}
	public function update($pegawai_id)
	{
		if($this->access->permission('read')){
			if($post = $this->input->post()){
				$pegawai_id = $post["pegawai_id"];
				$pegawai_unit_kerja_code = "";
				foreach($post["pegawai_unit_kerja_id"] as $field => $value) {
					if($value!=""){
						$pegawai_unit_kerja_code = $value;
					}
				}
				$datauker = $this->db->query("SELECT * FROM ms_unit_kerja WHERE unit_kerja_kode = '".$pegawai_unit_kerja_code."' AND unit_kerja_status = 1")->row_array();
				$pegawai_unit_kerja_id_fix = $datauker["unit_kerja_id"];
				$datainsert = array(
					'pegawai_nip' => str_replace(' ', '', $post["pegawai_nip"]),
					'pegawai_unit_kerja_id' => $pegawai_unit_kerja_id_fix,
					'pegawai_unit_kerja_code' => $pegawai_unit_kerja_code,
					'pegawai_nomor_ktp' => str_replace(' ', '', $post["pegawai_nomor_ktp"]),
					'pegawai_nomor_npwp' => str_replace(' ', '', $post["pegawai_nomor_npwp"]),
					'pegawai_gelar_depan' => $post["pegawai_gelar_depan"],
					'pegawai_gelar_belakang' => $post["pegawai_gelar_belakang"],
					'pegawai_nama' => $post["pegawai_nama"],
					'pegawai_tempat_lahir' => $post["pegawai_tempat_lahir"],
					'pegawai_tanggal_lahir' => $post["pegawai_tanggal_lahir_submit"],
					'pegawai_jenis_kelamin_id' => $post["pegawai_jenis_kelamin_id"],
					'pegawai_perkawinan_id' => $post["pegawai_perkawinan_id"],
					'pegawai_agama_id' => $post["pegawai_agama_id"],
					'pegawai_golongan_darah_id' => $post["pegawai_golongan_darah_id"],
					'pegawai_domisili_negara_id' => $post["pegawai_domisili_negara_id"],
					'pegawai_domisili_provinsi_id' => $post["pegawai_domisili_provinsi_id"],
					'pegawai_domisili_kota_id' => $post["pegawai_domisili_kota_id"],
					'pegawai_domisili_kec_id' => $post["pegawai_domisili_kec_id"],
					'pegawai_domisili_kel_id' => $post["pegawai_domisili_kel_id"],
					'pegawai_domisili_alamat' => $post["pegawai_domisili_alamat"],
					'pegawai_domisili_kodepos' => $post["pegawai_domisili_kodepos"],
					'pegawai_ktp_provinsi_id' => $post["pegawai_ktp_provinsi_id"],
					'pegawai_ktp_kota_id' => $post["pegawai_ktp_kota_id"],
					'pegawai_ktp_kec_id' => $post["pegawai_ktp_kec_id"],
					'pegawai_ktp_kel_id' => $post["pegawai_ktp_kel_id"],
					'pegawai_ktp_alamat' => $post["pegawai_ktp_alamat"],
					'pegawai_ktp_kodepos' => $post["pegawai_ktp_kodepos"],
					'pegawai_email_lain' => $post["pegawai_email_lain"],
					'pegawai_email' => $post["pegawai_email"],
					'pegawai_handphone' => $post["pegawai_handphone"],
					'pegawai_nomor_karpeg' => $post["pegawai_nomor_karpeg"],
					'pegawai_nomor_askes' => $post["pegawai_nomor_askes"],
					'pegawai_nomor_karis' => $post["pegawai_nomor_karis"],
					'pegawai_pendidikan_terakhir_id' => $post["pegawai_pendidikan_terakhir_id"],
					'pegawai_pendidikan_tingkat_id' => $post["pegawai_pendidikan_tingkat_id"],
					'pegawai_pendidikan_fakultas_id' => $post["pegawai_pendidikan_fakultas_id"],
					'pegawai_pendidikan_jurusan_id' => $post["pegawai_pendidikan_jurusan_id"],
					'pegawai_pendidikan_program_studi' => $post["pegawai_pendidikan_program_studi"],
					'pegawai_pendidikan_tahun_masuk' => $post["pegawai_pendidikan_tahun_masuk"],
					'pegawai_pendidikan_tahun_lulus' => $post["pegawai_pendidikan_tahun_lulus"],
					'pegawai_pendidikan_nama' => $post["pegawai_pendidikan_nama"],
					'pegawai_pendidikan_tempat' => $post["pegawai_pendidikan_tempat"],
					'pegawai_pendidikan_ipk' => $post["pegawai_pendidikan_ipk"],
					'pegawai_nilai_toefl' => $post["pegawai_nilai_toefl"],
					'pegawai_nilai_ielt' => $post["pegawai_nilai_ielt"],
					'pegawai_catatan' => $post["pegawai_catatan"],
					'pegawai_diklat_jenis_id' => $post["pegawai_diklat_jenis_id"],
					'pegawai_diklat_angkatan' => $post["pegawai_diklat_angkatan"],
					'pegawai_diklat_nama' => $post["pegawai_diklat_nama"],
					'pegawai_diklat_penyelenggara' => $post["pegawai_diklat_penyelenggara"],
					'pegawai_diklat_durasi' => $post["pegawai_diklat_durasi"],
					'pegawai_diklat_tanggal_mulai' => $post["pegawai_diklat_tanggal_mulai_submit"],
					'pegawai_diklat_tanggal_selesai' => $post["pegawai_diklat_tanggal_selesai_submit"],
					'pegawai_diklat_predikat' => $post["pegawai_diklat_predikat"],
					'pegawai_diklat_lokasi' => $post["pegawai_diklat_lokasi"],
					'pegawai_diklat_nomor_sertifikat' => $post["pegawai_diklat_nomor_sertifikat"],
					'pegawai_diklat_tanggal_sertifikat' => $post["pegawai_diklat_tanggal_sertifikat_submit"],
					'pegawai_diklat_lemhanas' => $post["pegawai_diklat_lemhanas"],
					'pegawai_status_pegawai_id' => $post["pegawai_status_pegawai_id"],
					'pegawai_instansi_asal' => $post["pegawai_instansi_asal"],
					'pegawai_cpns_formasi_id' => $post["pegawai_cpns_formasi_id"],
					'pegawai_cpns_nama_formasi' => $post["pegawai_cpns_nama_formasi"],
					'pegawai_cpns_status_formasi' => $post["pegawai_cpns_status_formasi"],
					'pegawai_cpns_fungsional_catatan' => $post["pegawai_cpns_fungsional_catatan"],
					'pegawai_cpns_golongan_id' => $post["pegawai_cpns_golongan_id"],
					'pegawai_cpns_tanggal_tmt' => $post["pegawai_cpns_tanggal_tmt_submit"],
					'pegawai_golongan_id' => $post["pegawai_golongan_id"],
					'pegawai_tanggal_tmt' => $post["pegawai_tanggal_tmt_submit"],
					'pegawai_tahun_masa_kerja_golongan' => $post["pegawai_tahun_masa_kerja_golongan"],
					'pegawai_bulan_masa_kerja_golongan' => $post["pegawai_bulan_masa_kerja_golongan"],
					'pegawai_tahun_masa_kerja_berkala' => $post["pegawai_tahun_masa_kerja_berkala"],
					'pegawai_bulan_masa_kerja_berkala' => $post["pegawai_bulan_masa_kerja_berkala"],
					'pegawai_tanggal_tmt_berkala' => $post["pegawai_tanggal_tmt_berkala_submit"],
					'pegawai_jabatan_id' => $post["pegawai_jabatan_id"],
					'pegawai_jabatan_fungsional_id' => $post["pegawai_jabatan_fungsional_id"],
					'pegawai_tanggal_tmt_jabfung' => $post["pegawai_tanggal_tmt_jabfung_submit"],
					'pegawai_fungsional_kredit' => $post["pegawai_fungsional_kredit"],
					'pegawai_jab_fung_bidang_1' => $post["pegawai_jab_fung_bidang_1"],
					'pegawai_jab_fung_bidang_2' => $post["pegawai_jab_fung_bidang_2"],
					'pegawai_tanggal_pembebasan_tmt_jab_fung' => $post["pegawai_tanggal_pembebasan_tmt_jab_fung_submit"],
					'pegawai_tanggal_aktif_tmt_jab_fung' => $post["pegawai_tanggal_aktif_tmt_jab_fung_submit"],
					'pegawai_nama_jabatan' => $post["pegawai_nama_jabatan"],
					'pegawai_tanggal_tmt_jabatan' => $post["pegawai_tanggal_tmt_jabatan_submit"],
					'pegawai_telah_pra_jabatan' => $post["pegawai_telah_pra_jabatan"],
					'pegawai_tahun_pra_jabatan' => $post["pegawai_tahun_pra_jabatan"],
					'pegawai_telah_sumpah_jabatan' => $post["pegawai_telah_sumpah_jabatan"],
					'pegawai_tahun_sumpah_jabatan' => $post["pegawai_tahun_sumpah_jabatan"],
					'pegawai_telah_test_kesehatan' => $post["pegawai_telah_test_kesehatan"],
					'pegawai_tahun_test_kesehatan' => $post["pegawai_tahun_test_kesehatan"],
					'pegawai_nomor_pendidikan' => $post["pegawai_nomor_pendidikan"],
					'pegawai_cpns_fungsional_tipe' => $post["pegawai_cpns_fungsional_tipe"],
					'pegawai_create_by' => $this->session->userdata('user_id'),
					'pegawai_create_date' => date('Y-m-d H:i:s'),
					'pegawai_log_id' => $pegawai_id,
					'pegawai_status' => 1
				);
				$this->db->query("UPDATE ms_pegawai SET pegawai_status = 0 WHERE pegawai_id = ".$pegawai_id);
				$pegawai_id = $this->pns_model->createpegawai($datainsert);//
				//$pegawai_id = $this->pns_model->updatepegawai($datainsert,$pegawai_id);//

				if($pegawai_id > 0){
					$this->load->model('kepegawaian/pendidikan_formal_model');
					$this->load->model('kepegawaian/diklat_perjenjangan_model');
					$pendidikan_formal_nama = $post['pegawai_pendidikan_nama'];
					$pendidikan_formal_pendidikan_id = $post['pegawai_pendidikan_terakhir_id'];
					$pendidikan_formal_jurusan_code_tingkat = $post['pegawai_pendidikan_tingkat_id'];
					$pendidikan_formal_jurusan_code_fakultas = $post['pegawai_pendidikan_fakultas_id'];
					$pendidikan_formal_jurusan_code = $post['pegawai_pendidikan_jurusan_id'];
					$pendidikan_formal_pro_studi = $post['pegawai_pendidikan_program_studi'];
					$pendidikan_formal_thn_masuk = $post['pegawai_pendidikan_tahun_masuk'];
					$pendidikan_formal_thn_lulus = $post['pegawai_pendidikan_tahun_lulus'];
					$pendidikan_formal_tmp_belajar = $post['pegawai_pendidikan_tempat'];
					$pendidikan_formal_lokasi = "";
					$pendidikan_formal_no_ijazah = "";
					$pendidikan_formal_nama_kep = "";
					$pendidikan_formal_pegawai_id = $pegawai_id;
					$pendidikan_formal_pegawai_nip = str_replace(' ', '', $post["pegawai_nip"]);


					$Tingkat = $this->pendidikan_formal_model->getJurusanData($pendidikan_formal_jurusan_code_tingkat)->row_array();
					$pendidikan_formal_jurusan_id_tingkat = $Tingkat["jurusan_id"];
					$pendidikan_formal_tingkat = $Tingkat["jurusan_grup"];
					$Fakultas = $this->pendidikan_formal_model->getJurusanData($pendidikan_formal_jurusan_code_fakultas)->row_array();
					$pendidikan_formal_jurusan_id_fakultas = $Fakultas["jurusan_id"];
					$pendidikan_formal_fakultas = $Fakultas["jurusan_nama"];
					$Jurusan = $this->pendidikan_formal_model->getJurusanData($pendidikan_formal_jurusan_code)->row_array();
					$pendidikan_formal_jurusan_id = $Jurusan["jurusan_id"];
					$pendidikan_formal_jurusan = $Jurusan["jurusan_nama"];
					$Pendidikan = $this->pendidikan_formal_model->getPendidikanData($pendidikan_formal_pendidikan_id)->row_array();
					$pendidikan_formal_pendidikan = $Pendidikan["pendidikan_nama"];
					$datacreate = array(
						'pendidikan_formal_nama' 				=> $pendidikan_formal_nama,
						'pendidikan_formal_pendidikan_id' 		=> $pendidikan_formal_pendidikan_id,
						'pendidikan_formal_pendidikan' 			=> $pendidikan_formal_pendidikan,
						'pendidikan_formal_jurusan_id_tingkat' 	=> $pendidikan_formal_jurusan_id_tingkat,
						'pendidikan_formal_tingkat' 			=> $pendidikan_formal_tingkat,
						'pendidikan_formal_jurusan_id_fakultas' => $pendidikan_formal_jurusan_id_fakultas,
						'pendidikan_formal_fakultas' 			=> $pendidikan_formal_fakultas,
						'pendidikan_formal_jurusan_id' 			=> $pendidikan_formal_jurusan_id,
						'pendidikan_formal_jurusan' 			=> $pendidikan_formal_jurusan,
						'pendidikan_formal_pro_studi' 			=> $pendidikan_formal_pro_studi,
						'pendidikan_formal_thn_masuk' 			=> $pendidikan_formal_thn_masuk,
						'pendidikan_formal_thn_lulus' 			=> $pendidikan_formal_thn_lulus,
						'pendidikan_formal_tmp_belajar' 		=> $pendidikan_formal_tmp_belajar,
						'pendidikan_formal_lokasi' 				=> $pendidikan_formal_lokasi,
						'pendidikan_formal_no_ijazah' 			=> $pendidikan_formal_no_ijazah,
						'pendidikan_formal_nama_kep' 			=> $pendidikan_formal_nama_kep,
						'pendidikan_formal_pegawai_id' 			=> $pendidikan_formal_pegawai_id,
						'pendidikan_formal_pegawai_nip' 		=> $pendidikan_formal_pegawai_nip,
						'pendidikan_formal_status' 				=> 1,
						'pendidikan_formal_create_by' 			=> $this->session->userdata('user_id'),
						'pendidikan_formal_create_date' 		=> date('Y-m-d H:i:s')
					);
					$this->pendidikan_formal_model->create_pendidikan($datacreate);


					$diklat_pegawai_id = $pegawai_id;
					$diklat_pegawai_nip = str_replace(' ', '', $post["pegawai_nip"]);

					$diklat_type = 1;
					$diklat_jenis_id = $post['pegawai_diklat_jenis_id'];
					$diklat_angkatan = $post['pegawai_diklat_angkatan'];
					$diklat_penyelenggara = $post['pegawai_diklat_penyelenggara'];
					$diklat_jumlah_jam = $post['pegawai_diklat_durasi'];
					$diklat_tanggal_mulai = $post['pegawai_diklat_tanggal_mulai_submit'];
					$diklat_tanggal_selesai = $post['pegawai_diklat_tanggal_selesai_submit'];
					$diklat_predikat = $post['pegawai_diklat_predikat'];
					$diklat_lokasi = $post['pegawai_diklat_lokasi'];
					$diklat_nomor_sertifikat = $post['pegawai_diklat_nomor_sertifikat'];
					$diklat_tanggal_sertifikat = $post['pegawai_diklat_tanggal_sertifikat_submit'];
					$diklat_status = 1;
					$diklat_nama = $post['pegawai_diklat_nama'];
					
					$datacreate = array(
						'diklat_pegawai_id' => $diklat_pegawai_id,
						'diklat_pegawai_nip' => $diklat_pegawai_nip,
						'diklat_type' => $diklat_type,
						'diklat_jenis_id' => $diklat_jenis_id,
						'diklat_angkatan' => $diklat_angkatan,
						'diklat_penyelenggara' => $diklat_penyelenggara,
						'diklat_jumlah_jam' => $diklat_jumlah_jam,
						'diklat_tanggal_mulai' => $diklat_tanggal_mulai,
						'diklat_tanggal_selesai' => $diklat_tanggal_selesai,
						'diklat_predikat' => $diklat_predikat,
						'diklat_lokasi' => $diklat_lokasi,
						'diklat_nomor_sertifikat' => $diklat_nomor_sertifikat,
						'diklat_tanggal_sertifikat' => $diklat_tanggal_sertifikat,
						'diklat_create_by' => $this->session->userdata('user_id'),
						'diklat_create_date' => date('Y-m-d H:i:s'),
						'diklat_status' => 1,
						'diklat_nama' => $diklat_nama
					);
					$this->diklat_perjenjangan_model->create_diklat($datacreate);
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Penambahan Pegawai Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/pns');
				}else{
					$notify = array(
						'title' 	=> 'Gagal!',
						'message'	=> 'Penambahan Pegawai gagal, silahkan coba lagi',
						'status' 	=> 'error'
					);
					$this->session->set_flashdata('notify', $notify);
					redirect(base_url().'kepegawaian/pns');
				}
			}

			$data = array();
			$data["pegawai"] = $this->pns_model->getPegawai($pegawai_id)->row_array();
			$data["provinsi"] = $this->pns_model->getProvinsi()->result_array();
			$data["jenis_kelamin"] = $this->pns_model->getJenisKelamin()->result_array();
			$data["status_perkawinan"] = $this->pns_model->getStatusKawin()->result_array();
			$data["agama"] = $this->pns_model->getAgama()->result_array();
			$data["golongan_darah"] = $this->pns_model->getGolonganDarah()->result_array();
			$data["pendidikan"] = $this->pns_model->getPendidikan()->result_array();
			$data["negara"] = $this->pns_model->getNegara()->result_array();
			$data["unit_kerja"] = $this->pns_model->getUnitKerja(1,2)->result_array();
			$data["diklat"] = $this->pns_model->getJenisDiklat()->result_array();
			$data["status_pegawai"] = $this->pns_model->getStatusPegawai()->result_array();
			$data["fungsional_tertentu"] = $this->pns_model->getFungsionalTertentu()->result_array();
			$data["fungsional_tertentu_formasi"] = $this->pns_model->getFormasiFungsionalTertentu()->result_array();
			$data["golongan"] = $this->pns_model->getGolongan()->result_array();
			$data["jabatan"] = $this->pns_model->getJabatan()->result_array();
			$this->template->display('kepegawaian/pns/update', $data);	

		}else{
			$this->access->redirect('404');
		}
	}
	public function checkNipExist()
	{
		$pegawai_nip = $this->input->post('pegawai_nip');
		$kota = $this->pns_model->checkNipExist($pegawai_nip)->result_array();
		$res = true;
		if(count($kota)>0){
			$res = false;
		}
		echo json_encode($res);
	}
	public function checkKTPExist()
	{
		$pegawai_nomor_ktp = $this->input->post('pegawai_nomor_ktp');
		$kota = $this->pns_model->checkKTPExist($pegawai_nomor_ktp)->result_array();
		$res = true;
		if(count($kota)>0){
			$res = false;
		}
		echo json_encode($res);
	}
	public function checkNPWPExist()
	{
		$pegawai_nomor_npwp = $this->input->post('pegawai_nomor_npwp');
		$kota = $this->pns_model->checkNPWPExist($pegawai_nomor_npwp)->result_array();
		$res = true;
		if(count($kota)>0){
			$res = false;
		}
		echo json_encode($res);
	}
	public function createbackup()
	{
		$data = array();
		$data["provinsi"] = $this->pns_model->getProvinsi()->result_array();
		$data["jenis_kelamin"] = $this->pns_model->getJenisKelamin()->result_array();
		$data["status_perkawinan"] = $this->pns_model->getStatusKawin()->result_array();
		$data["agama"] = $this->pns_model->getAgama()->result_array();
		$data["golongan_darah"] = $this->pns_model->getGolonganDarah()->result_array();
		$data["pendidikan"] = $this->pns_model->getPendidikan()->result_array();
		$data["negara"] = $this->pns_model->getNegara()->result_array();
		$data["unit_kerja"] = $this->pns_model->getUnitKerja('','')->result_array();


		$this->template->display('kepegawaian/pns/createbackup', $data);	
	}
	public function get_kota()
	{
		$provinsi_kode 	= $this->input->post('provinsi_kode');
		$data["kota"] = $this->pns_model->getKota($provinsi_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_kecamatan()
	{
		$kota_kode 	= $this->input->post('kota_kode');
		$data["kecamatan"] = $this->pns_model->getKecamatan($kota_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_kelurahan()
	{
		$kecamatan_kode 	= $this->input->post('kecamatan_kode');
		$data["kelurahan"] = $this->pns_model->getKelurahan($kecamatan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_unit_kerja_hirarki()
	{
		$unit_kerja_parent_id_kode 	= $this->input->post('unit_kerja_parent_id_kode');
		$unit_kerja_level 	= $this->input->post('unit_kerja_level');
		$data["unit_kerja"] = $this->pns_model->getUnitKerja($unit_kerja_parent_id_kode,$unit_kerja_level)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_tingkat_pendidikan()
	{
		$pendidikan_kode 	= $this->input->post('pendidikan_kode');
		$data["tingkat_pendidikan"] = $this->pns_model->getTingkatPendidikan($pendidikan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_jurusan()
	{
		$jurusan_kode 	= $this->input->post('jurusan_kode');
		$data["jurusan"] = $this->pns_model->getJurusan($jurusan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function list_data(){
		$default_order = "pegawai_id";
		$limit = 10;
		$where = "pegawai_status = 1 AND pegawai_is_pns = 1 ".(($this->session->userdata('filter_where') != "")?"AND ".$this->session->userdata('filter_where'):"");
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama',
			'pegawai_tempat_lahir',
			'pegawai_tanggal_lahir',
			'pegawai_nama_jabatan',
			'unit_kerja_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}

		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->pns_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->pns_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->pns_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_tempat_lahir"].'/'.$row["pegawai_tanggal_lahir"],
				$row["pegawai_nama_jabatan"],
				$row["unit_kerja_nama"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/pns/update/'.urlencode($row['pegawai_id']).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
}
