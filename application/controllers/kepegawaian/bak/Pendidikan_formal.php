	<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pendidikan_Formal extends CI_Controller{

	function __construct(){
		parent:: __construct();
		$this->load->model('kepegawaian/pendidikan_formal_model');
		//$this->load->library('access');
		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index(){
		if($this->access->permission('read')){
			$data = array();
			if($this->session->userdata(""))
			$this->template->display('kepegawaian/pendidikan_formal/index', $data);
		}else{
			$this->access->redirect('404');
		}
	}
    public function read($data_id=0){
        if($this->access->permission('read')){
			$data = array();
			$data['pegawai']  	= $this->pendidikan_formal_model->getPegawai($data_id)->row_array();
			$data['history']  	= $this->pendidikan_formal_model->getListPendidikan($data_id)->result_array();
			$data['pendidikan'] = $this->pendidikan_formal_model->getPendidikan()->result_array();
			$this->template->display('kepegawaian/pendidikan_formal/read', $data);
       }else{
            $this->access->redirect('404');
        }
    }	
	public function update($pegawai_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$isUpdate = $post['isUpdate'];
				$pendidikan_formal_id = $post['pendidikan_formal_id'];
				$pendidikan_formal_nama = $post['pendidikan_formal_nama'];
				$pendidikan_formal_pendidikan_id = $post['pendidikan_formal_pendidikan_id'];
				$pendidikan_formal_jurusan_code_tingkat = $post['pendidikan_formal_jurusan_id_tingkat'];
				$pendidikan_formal_jurusan_code_fakultas = $post['pendidikan_formal_jurusan_id_fakultas'];
				$pendidikan_formal_jurusan_code = $post['pendidikan_formal_jurusan_id'];
				$pendidikan_formal_pro_studi = $post['pendidikan_formal_pro_studi'];
				$pendidikan_formal_thn_masuk = $post['pendidikan_formal_thn_masuk'];
				$pendidikan_formal_thn_lulus = $post['pendidikan_formal_thn_lulus'];
				$pendidikan_formal_tmp_belajar = $post['pendidikan_formal_tmp_belajar'];
				$pendidikan_formal_lokasi = $post['pendidikan_formal_lokasi'];
				$pendidikan_formal_no_ijazah = $post['pendidikan_formal_no_ijazah'];
				$pendidikan_formal_nama_kep = $post['pendidikan_formal_nama_kep'];
				$pendidikan_formal_pegawai_id = $post['pendidikan_formal_pegawai_id'];
				$pendidikan_formal_pegawai_nip = $post['pendidikan_formal_pegawai_nip'];
				$pegawai_pendidikan_ipk = $post['pegawai_pendidikan_ipk'];
				
				
				$Tingkat = $this->pendidikan_formal_model->getJurusanData($pendidikan_formal_jurusan_code_tingkat)->row_array();
				$pendidikan_formal_jurusan_id_tingkat = $Tingkat["jurusan_id"];
				$pendidikan_formal_tingkat = $Tingkat["jurusan_grup"];
				$Fakultas = $this->pendidikan_formal_model->getJurusanData($pendidikan_formal_jurusan_code_fakultas)->row_array();
				$pendidikan_formal_jurusan_id_fakultas = $Fakultas["jurusan_id"];
				$pendidikan_formal_fakultas = $Fakultas["jurusan_nama"];
				$Jurusan = $this->pendidikan_formal_model->getJurusanData($pendidikan_formal_jurusan_code)->row_array();
				$pendidikan_formal_jurusan_id = $Jurusan["jurusan_id"];
				$pendidikan_formal_jurusan = $Jurusan["jurusan_nama"];
				$Pendidikan = $this->pendidikan_formal_model->getPendidikanData($pendidikan_formal_pendidikan_id)->row_array();
				$pendidikan_formal_pendidikan = $Pendidikan["pendidikan_nama"];
			
			
			    // tambahan muklis
				$update_data_sort  = $this->pendidikan_formal_model->get_short_id($post['pendidikan_formal_pendidikan_id'])->result_array();
				$update_data_sort  = $update_data_sort[0]['pendidikan_sort'];                    
				 
				$last_pendidikan        = $this->pendidikan_formal_model->get_last_pendidikan($pendidikan_formal_pegawai_id)->result_array();
				$tahun_last_pendidikan  = $last_pendidikan[0]['pendidikan_formal_thn_lulus'];
				$id_last_pendidikan     = $last_pendidikan[0]['pendidikan_formal_pendidikan_id'];
				
				$last_data_sort         = $this->pendidikan_formal_model->get_short_id($id_last_pendidikan)->result_array();
				$last_data_sort         = $last_data_sort[0]['pendidikan_sort'];
				
				//print "<br>update_data_sort =".$update_data_sort."==tahun_last_pendidikan=".$tahun_last_pendidikan."==<br>";
				//print "<br>last_data_sort =".$last_data_sort;exit;
				
				// update data pegawai
				$update_pegawai = 0;
				if($update_data_sort > $last_data_sort){
					$update_pegawai = 1;
				}
				else if($update_data_sort == $last_data_sort){
					if($post['pendidikan_formal_thn_lulus'] >= $tahun_last_pendidikan){
						$update_pegawai = 1;
					}					
				}
				if($update_pegawai == 1){
				  $update_pendidikan_last = array(	
					  'pegawai_pendidikan_terakhir_id' 		=> $post['pendidikan_formal_pendidikan_id'],
					  'pegawai_pendidikan_tingkat_id' 		=> $post['pendidikan_formal_jurusan_id_tingkat'],
					  'pegawai_pendidikan_fakultas_id' 		=> $post['pendidikan_formal_jurusan_id_fakultas'],
					  'pegawai_pendidikan_jurusan_id' 		=> $post['pendidikan_formal_jurusan_id'],
					  'pegawai_pendidikan_program_studi' 	=> $post['pendidikan_formal_pro_studi'],
					  'pegawai_pendidikan_tahun_masuk' 		=> $post['pendidikan_formal_thn_masuk'],
					  'pegawai_pendidikan_tahun_lulus' 		=> $post['pendidikan_formal_thn_lulus'],
					  'pegawai_pendidikan_nama' 			=> $post['pendidikan_formal_tmp_belajar'],
					  'pegawai_pendidikan_tempat' 			=> $post['pendidikan_formal_lokasi'],
					  'pegawai_pendidikan_ipk' 				=> $post['pegawai_pendidikan_ipk']
				  );					  
				}							
			
				if($isUpdate == 0){
					$datacreate = array(
						'pendidikan_formal_nama' 				=> $pendidikan_formal_nama,
						'pendidikan_formal_pendidikan_id' 		=> $pendidikan_formal_pendidikan_id,
						'pendidikan_formal_pendidikan' 			=> $pendidikan_formal_pendidikan,
						'pendidikan_formal_jurusan_id_tingkat' 	=> $pendidikan_formal_jurusan_id_tingkat,
						'pendidikan_formal_tingkat' 			=> $pendidikan_formal_tingkat,
						'pendidikan_formal_jurusan_id_fakultas' => $pendidikan_formal_jurusan_id_fakultas,
						'pendidikan_formal_fakultas' 			=> $pendidikan_formal_fakultas,
						'pendidikan_formal_jurusan_id' 			=> $pendidikan_formal_jurusan_id,
						'pendidikan_formal_jurusan' 			=> $pendidikan_formal_jurusan,
						'pendidikan_formal_pro_studi' 			=> $pendidikan_formal_pro_studi,
						'pendidikan_formal_thn_masuk' 			=> $pendidikan_formal_thn_masuk,
						'pendidikan_formal_thn_lulus' 			=> $pendidikan_formal_thn_lulus,
						'pendidikan_formal_tmp_belajar' 		=> $pendidikan_formal_tmp_belajar,
						'pendidikan_formal_lokasi' 				=> $pendidikan_formal_lokasi,
						'pendidikan_formal_no_ijazah' 			=> $pendidikan_formal_no_ijazah,
						'pendidikan_formal_nama_kep' 			=> $pendidikan_formal_nama_kep,
						'pendidikan_formal_pegawai_id' 			=> $pendidikan_formal_pegawai_id,
						'pendidikan_formal_pegawai_nip' 		=> $pendidikan_formal_pegawai_nip,
						'pendidikan_formal_status' 				=> 1,
						'pendidikan_formal_create_by' 			=> $this->session->userdata('user_id'),
						'pendidikan_formal_create_date' 		=> date('Y-m-d H:i:s'),
					    'pendidikan_formal_ipk' 				=> $post['pegawai_pendidikan_ipk']
					);


					$insDb = $this->pendidikan_formal_model->create_pendidikan($datacreate);
                    
					if($update_pegawai == 1){ //print_r($update_pendidikan_last);print $pendidikan_formal_pegawai_id;exit;
						$update_pendidikan_last = $this->pendidikan_formal_model->update_pendidikan_last($update_pendidikan_last,$pendidikan_formal_pegawai_id);					
					}
					
					if($insDb > 0){
						$notify = array(
							'title' 	=> 'Berhasil!',
							'message' 	=> 'Penambahan Data Pendidikan Berhasil',
							'status' 	=> 'success'
						);
						$this->session->set_flashdata('notify', $notify);

						redirect(base_url().'kepegawaian/pendidikan_formal/update/'.$pendidikan_formal_pegawai_id);
					}else{
						$notify = array(
							'title' 	=> 'Gagal!',
							'message'	=> 'Penambahan Data Pendidikan Gagal, silahkan coba lagi',
							'status' 	=> 'error'
						);
						$this->session->set_flashdata('notify', $notify);
						redirect(base_url().'kepegawaian/pendidikan_formal/update/'.$pendidikan_formal_pegawai_id);
					}
				}else{
					$dataupdate = array(
						'pendidikan_formal_nama' 				=> $pendidikan_formal_nama,
						'pendidikan_formal_pendidikan_id' 		=> $pendidikan_formal_pendidikan_id,
						'pendidikan_formal_pendidikan' 			=> $pendidikan_formal_pendidikan,
						'pendidikan_formal_jurusan_id_tingkat' 	=> $pendidikan_formal_jurusan_id_tingkat,
						'pendidikan_formal_tingkat' 			=> $pendidikan_formal_tingkat,
						'pendidikan_formal_jurusan_id_fakultas' => $pendidikan_formal_jurusan_id_fakultas,
						'pendidikan_formal_fakultas' 			=> $pendidikan_formal_fakultas,
						'pendidikan_formal_jurusan_id' 			=> $pendidikan_formal_jurusan_id,
						'pendidikan_formal_jurusan' 			=> $pendidikan_formal_jurusan,
						'pendidikan_formal_pro_studi' 			=> $pendidikan_formal_pro_studi,
						'pendidikan_formal_thn_masuk' 			=> $pendidikan_formal_thn_masuk,
						'pendidikan_formal_thn_lulus' 			=> $pendidikan_formal_thn_lulus,
						'pendidikan_formal_tmp_belajar' 		=> $pendidikan_formal_tmp_belajar,
						'pendidikan_formal_lokasi' 				=> $pendidikan_formal_lokasi,
						'pendidikan_formal_no_ijazah' 			=> $pendidikan_formal_no_ijazah,
						'pendidikan_formal_nama_kep' 			=> $pendidikan_formal_nama_kep,
						'pendidikan_formal_status' 				=> 1,
						'pendidikan_formal_create_by' 			=> $this->session->userdata('user_id'),
						'pendidikan_formal_create_date' 		=> date('Y-m-d H:i:s'),
					    'pendidikan_formal_ipk' 				=> $post['pegawai_pendidikan_ipk']
					);
					$this->pendidikan_formal_model->update_pendidikan($dataupdate, $pendidikan_formal_id);

                    if($update_pegawai == 1){
						$update_pendidikan_last = $this->pendidikan_formal_model->update_pendidikan_last($update_pendidikan_last,$pendidikan_formal_pegawai_id);					
					}					
										
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan Data Pendidikan Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'kepegawaian/pendidikan_formal/update/'.$pendidikan_formal_pegawai_id);
				}
			}

			$data = array();
			$data['pegawai']  	= $this->pendidikan_formal_model->getPegawai($pegawai_id)->row_array();
			$data['history']  	= $this->pendidikan_formal_model->getListPendidikan($pegawai_id)->result_array();
			$data['pendidikan'] = $this->pendidikan_formal_model->getPendidikan()->result_array();
			$this->template->display('kepegawaian/pendidikan_formal/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function get_tingkat_pendidikan()
	{
		$pendidikan_kode 	= $this->input->post('pendidikan_kode');
		$data["tingkat_pendidikan"] = $this->pendidikan_formal_model->getTingkatPendidikan($pendidikan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_fakultas()
	{
		$jurusan_kode 	= $this->input->post('jurusan_kode');
		$data["jurusan"] = $this->pendidikan_formal_model->getFakultas($jurusan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function get_jurusan()
	{
		$jurusan_kode 	= $this->input->post('jurusan_kode');
		$data["jurusan"] = $this->pendidikan_formal_model->getJurusan($jurusan_kode)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	

	
	public function list_data_aktif(){
		$default_order = "";
		$limit = 10;
		$where = $this->session->userdata('filter_where');
		$field_name 	= array(
			'pegawai_nip',
			'pegawai_nama',
			'pegawai_nama_jabatan',
			'unit_kerja_nama'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->pendidikan_formal_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->pendidikan_formal_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->pendidikan_formal_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["pegawai_nip"],
				$row["pegawai_nama"],
				$row["pegawai_nama_jabatan"],
				$row["unit_kerja_nama"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'kepegawaian/pendidikan_formal/update/'.urlencode($row["pegawai_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
	public function hapus($pendidikan_formal_id = 0,$pegawai_id = 0){

		$pendidikan_formal_idFilter = filter_var($pendidikan_formal_id, FILTER_SANITIZE_NUMBER_INT);
		if($this->access->permission('delete')) {
			if($pendidikan_formal_id==$pendidikan_formal_idFilter) {

				$dataupdate = array(
					'pendidikan_formal_status'  => 0,
					'pendidikan_formal_create_by' 			=> $this->session->userdata('user_id'),
					'pendidikan_formal_create_date' 			=> date('Y-m-d H:i:s')
				);

				$del = $this->pendidikan_formal_model->update_pendidikan($dataupdate,$pendidikan_formal_id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Pendidikan Formal Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'kepegawaian/pendidikan_formal/update/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Pendidikan Formal Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'kepegawaian/pendidikan_formal/update/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Pendidikan Formal Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'kepegawaian/pendidikan_formal/update/'.$pegawai_id);
		}
	}

   //tambahan muklis	
    public function check_tingat_pendidikan()
    {   
	    // 9  pendidikan SLTA
		$data = $this->pendidikan_formal_model->check_tingat_pendidikan($this->input->post('pendidikan_formal_pendidikan_id'),$this->input->post('pendidikan_formal_pegawai_id'))->result_array();
		$res = true;
		if($this->input->post('isUpdate') == 0){ 
			if($this->input->post('pendidikan_formal_pendidikan_id') > 8){
				if(count($data)>0){
					$res = false;
				}
			}
		}
		echo json_encode($res);
    }	
}
