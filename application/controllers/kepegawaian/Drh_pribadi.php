<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Drh_pribadi extends CI_Controller{

	function __construct(){
		parent:: __construct();
		$this->load->model('kepegawaian/drh_pribadi_model');
		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index(){
		if($this->access->permission('read')){
			$pegawai_id = $this->session->userdata('pegawai_id');
            $pegawai_nip = $this->session->userdata('pegawai_nip');
			$data['data'] = $this->drh_pribadi_model->getPegawai($pegawai_id)->row_array();
            $data['pendidikan_formal'] = $this->drh_pribadi_model->getPendidikanFormal($pegawai_id)->result_array();
            $data['pendidikan_nonformal'] = $this->drh_pribadi_model->getPendidikanNonFormal($pegawai_id)->result_array();
            $data['kepangkatan'] = $this->drh_pribadi_model->getKepangkatan($pegawai_id)->result_array();
            $data['jabatan'] = $this->drh_pribadi_model->getJabatan($pegawai_id)->result_array();
            $data['diklatp'] = $this->drh_pribadi_model->getDiklatP($pegawai_id)->result_array();
            $data['suamiistri'] = $this->drh_pribadi_model->getSuamiIstri($pegawai_id)->result_array();
            $data['anak'] = $this->drh_pribadi_model->getAnak($pegawai_id)->result_array();
            $data['keluarga'] = $this->drh_pribadi_model->getKeluarga($pegawai_id)->result_array();
            $data['seminar'] = $this->drh_pribadi_model->getSeminar($pegawai_id)->result_array();
            $data['penghargaan'] = $this->drh_pribadi_model->getPenghargaan($pegawai_id)->result_array();
            $data['hukuman_disiplin'] = $this->drh_pribadi_model->getHukumanDisiplin($pegawai_id)->result_array();
            $data['dp3'] = $this->drh_pribadi_model->getDp3($pegawai_id)->result_array();
            $data['perilaku'] = $this->drh_pribadi_model->getPerilaku($pegawai_id)->result_array();
            $data['urut'] = $this->drh_pribadi_model->getUrut($pegawai_nip)->row_array();
			$this->template->display('kepegawaian/drh_pribadi/index', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function update(){
            if($post = $this->input->post()){
				$pegawai_id = $this->session->userdata('pegawai_id');
                $dataupdate = array(
                    'pegawai_handphone'           => isset($post['pegawai_handphone'])?$post['pegawai_handphone']:'',
                    'pegawai_nama_kontak_darurat' => isset($post['pegawai_nama_kontak_darurat'])?$post['pegawai_nama_kontak_darurat']:'',
                    'pegawai_no_kontak_darurat'   => isset($post['pegawai_no_kontak_darurat'])?$post['pegawai_no_kontak_darurat']:'',
                    'pegawai_email'      	      => isset($post['pegawai_email'])?$post['pegawai_email']:'',
                    'pegawai_nomor_ktp'           => isset($post['pegawai_nomor_ktp'])?$post['pegawai_nomor_ktp']:'',
                    'pegawai_nomor_npwp'          => isset($post['pegawai_nomor_npwp'])?$post['pegawai_nomor_npwp']:'',
                    'pegawai_nomor_npwp'          => isset($post['pegawai_nomor_npwp'])?$post['pegawai_nomor_npwp']:'',
					'pegawai_sinkronisasi'        => 0
                );

                
                $insDb = $this->drh_pribadi_model->updatedatapribadi($dataupdate, $pegawai_id);

                if($insDb > 0){
                    $notify = array(
                        'title'     => 'Berhasil!',
                        'message'   => 'Perubahan Data Pribadi Berhasil',
                        'status'    => 'success'
                    );
                    $this->session->set_flashdata('notify', $notify);

                    redirect(base_url().'kepegawaian/drh_pribadi/index');
                }else{
                    $notify = array(
                        'title'     => 'Gagal!',
                        'message'   => 'Perubahan Data Pribadi gagal, silahkan coba lagi',
                        'status'    => 'error'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    redirect(base_url().'kepegawaian/drh_pribadi/index');
                }
            }
    }
	
}