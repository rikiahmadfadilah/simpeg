<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perdin extends CI_Controller{

    function __construct(){
        parent:: __construct();
        $this->load->model('perdin/perdin/perdin_model');
        //$this->load->library('access');
        $this->load->helper('xml');
        $this->load->helper('text');
    }

    public function index(){
        if($this->access->permission('read')){
            $data = array();
            $this->template->display('perdin/perdin/index', $data);
        }else{
            $this->access->redirect('404');
        }
        
    }
    public function create(){
        
        if($this->access->permission('create')){
            if($post = $this->input->post()){
            $tgl_mulai = dateEnToID($post['perdin_tanggal_mulai'], 'd-m-Y');
            $tgl_selesai = dateEnToID($post['perdin_tanggal_selesai'], 'd-m-Y');
                for($i=0;$i<count($post['perdin_ketua_nip']);$i++){
                $perdin = array(
                    'perdin_no_sp'              => isset($post['perdin_no_sp'])?$post['perdin_no_sp']:'',
                    'perdin_ppk_nip'            => isset($post['perdin_ppk_nip'])?$post['perdin_ppk_nip']:'',
                    'perdin_ketua_nip'          => isset($post['perdin_ketua_nip'][$i])?$post['perdin_ketua_nip'][$i]:'',
                    'perdin_ketua_nama'         => isset($post['perdin_ketua_nama'][$i])?$post['perdin_ketua_nama'][$i]:'',
                    'perdin_ketua_jabatan_nama' => isset($post['perdin_ketua_jabatan_nama'][$i])?$post['perdin_ketua_jabatan_nama'][$i]:'',
                    'perdin_maksud_tujuan'      => isset($post['perdin_maksud_tujuan'])?$post['perdin_maksud_tujuan']:'',
                    'perdin_berangkat_negara_kode'      => isset($post['perdin_berangkat_negara_kode'])?$post['perdin_berangkat_negara_kode']:'',
                    'perdin_berangkat_provinsi_kode'      => isset($post['perdin_berangkat_provinsi_kode'])?$post['perdin_berangkat_provinsi_kode']:'',
                    'perdin_berangkat_kota_kode'      => isset($post['perdin_berangkat_kota_kode'])?$post['perdin_berangkat_kota_kode']:'',
                    'perdin_berangkat_kecamatan_kode'      => isset($post['perdin_berangkat_kecamatan_kode'])?$post['perdin_berangkat_kecamatan_kode']:'',
                    'perdin_tujuan_negara_kode'      => isset($post['perdin_tujuan_negara_kode'])?$post['perdin_tujuan_negara_kode']:'',
                    'perdin_tujuan_provinsi_kode'      => isset($post['perdin_tujuan_provinsi_kode'])?$post['perdin_tujuan_provinsi_kode']:'',
                    'perdin_tujuan_kota_kode'      => isset($post['perdin_tujuan_kota_kode'])?$post['perdin_tujuan_kota_kode']:'',
                    'perdin_tujuan_kecamatan_kode'      => isset($post['perdin_tujuan_kecamatan_kode'])?$post['perdin_tujuan_kecamatan_kode']:'',
                    'perdin_tanggal_mulai'      => isset($post['perdin_tanggal_mulai'])?dateEnToID($post['perdin_tanggal_mulai'], 'Y-m-d'):'',
                    'perdin_tanggal_selesai'    => isset($post['perdin_tanggal_selesai'])?dateEnToID($post['perdin_tanggal_selesai'], 'Y-m-d'):'',
                    'perdin_create_by'          => $this->session->userdata('user_id'),
                    'perdin_create_date'        => date('Y-m-d H:i:s'),
                    'perdin_status'             => 1,
                    'perdin_lama_waktu'              => hitungcuti($tgl_mulai, $tgl_selesai,"-")
                );
                // echo "<pre>"; print_r($perdin);die;
                $perdin_id = $this->perdin_model->addPerdin($perdin);
                }

                // for($i=0;$i<count($post['pengikut_nip']);$i++){

                // $perdin = array(
                //     'perdin_detail_pegawai_nip' => isset($post['pengikut_nip'][$i])?$post['pengikut_nip'][$i]:'',
                //     'perdin_detail_status'      => 1,
                //     'perdin_detail_perdin_id'   =>$perdin_id
                // );

                // $perdindetail = $this->perdin_model->addPerdinDetail($perdin);
                // }
                if($perdin_id > 0){
                    $notify = array(
                        'title'     => 'Berhasil!',
                        'message'   => 'Tambah Pengajuan perdin Berhasil',
                        'status'    => 'success'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    
                    redirect(base_url().'perdin/perdin');
                }else{
                    $notify = array(
                        'title'     => 'Gagal!',
                        'message'   => 'Tambah Pengajuan perdin gagal, silahkan coba lagi',
                        'status'    => 'error'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    
                    redirect(base_url().'perdin/perdin');
                }
            }
            
            $data = array();
            $data['pegawai']   = $this->perdin_model->getPegawai()->result_array();
            $data['negara']   = $this->perdin_model->getNegara()->result_array();
            $data['provinsi']   = $this->perdin_model->getProvinsi()->result_array();
            $data["pangkatgolongan"] = $this->perdin_model->getPangkatGolongan()->result_array();
            $data["jabataninstansi"] = $this->perdin_model->getJabatanInstansi()->result_array();
            $this->template->display('perdin/perdin/create', $data);
        } else {
            $this->access->redirect('404');
        }
    }
    public function update($perdin_id = 0){
        if($this->access->permission('update')){

            if($post = $this->input->post()){
                $perdin_id = $post['perdin_id'];
                $dataupdate = array(
                    'perdin_ppk_nip'            => isset($post['perdin_ppk_nip'])?$post['perdin_ppk_nip']:'',
                    'perdin_ketua_nip'          => isset($post['perdin_ketua_nip'])?$post['perdin_ketua_nip']:'',
                    'perdin_ketua_golongan_id'  => isset($post['perdin_ketua_golongan_id'])?$post['perdin_ketua_golongan_id']:'',
                    'perdin_ketua_jabatan_id'   => isset($post['perdin_ketua_jabatan_id'])?$post['perdin_ketua_jabatan_id']:'',
                    'perdin_tingkat_biaya'      => isset($post['perdin_tingkat_biaya'])?$post['perdin_tingkat_biaya']:'',
                    'perdin_maksud_tujuan'      => isset($post['perdin_maksud_tujuan'])?$post['perdin_maksud_tujuan']:'',
                    'perdin_alat_angkut'        => isset($post['perdin_alat_angkut'])?$post['perdin_alat_angkut']:'',
                    'perdin_tempat_berangkat'   => isset($post['perdin_tempat_berangkat'])?$post['perdin_tempat_berangkat']:'',
                    'perdin_tempat_tujuan'      => isset($post['perdin_tempat_tujuan'])?$post['perdin_tempat_tujuan']:'',
                    'perdin_tanggal_mulai'      => isset($post['perdin_tanggal_mulai'])?$post['perdin_tanggal_mulai']:'',
                    'perdin_tanggal_selesai'    => isset($post['perdin_tanggal_selesai'])?$post['perdin_tanggal_selesai']:'',
                    'perdin_anggaran_instansi'  => isset($post['perdin_anggaran_instansi'])?$post['perdin_anggaran_instansi']:'',
                    'perdin_anggaran_akun'      => isset($post['perdin_anggaran_akun'])?$post['perdin_anggaran_akun']:'',
                    'perdin_anggaran_keterangan'=> isset($post['perdin_anggaran_keterangan'])?$post['perdin_anggaran_keterangan']:'',
                    'perdin_create_by'          => $this->session->userdata('user_id'),
                    'perdin_create_date'        => date('Y-m-d H:i:s'),
                    'perdin_status'             => 1,
                    'perdin_lama_waktu'              => hitungcuti($tgl_mulai, $tgl_selesai,"-")

                );

                
                $insDb = $this->perdin_model->updatePerdin($dataupdate, $perdin_id);

                if($insDb > 0){
                    $notify = array(
                        'title'     => 'Berhasil!',
                        'message'   => 'Perubahan Pengajuan perdin Berhasil',
                        'status'    => 'success'
                    );
                    $this->session->set_flashdata('notify', $notify);

                    redirect(base_url().'perdin/perdin');
                }else{
                    $notify = array(
                        'title'     => 'Gagal!',
                        'message'   => 'Perubahan Pengajuan perdin gagal, silahkan coba lagi',
                        'status'    => 'error'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    redirect(base_url().'perdin/perdin');
                }
            }

            $data = array();
            $data['pegawai']   = $this->perdin_model->getPegawai()->result_array();
            $data["pangkatgolongan"] = $this->perdin_model->getPangkatGolongan()->result_array();
            $data["jabataninstansi"] = $this->perdin_model->getJabatanInstansi()->result_array();
            $data['perdin'] = $this->perdin_model->getPerdin($perdin_id)->row_array();
            $this->template->display('perdin/perdin/update', $data);
        }else{
            $this->access->redirect('404');
        }
    }
    public function read($perdin_id=0){
        if($this->access->permission('read')){
            $data['perdin'] = $this->perdin_model->readPerdin($perdin_id)->row_array();
            $tgl_mulai = dateEnToID($data['perdin']['perdin_tanggal_mulai'], 'd-m-Y');
            $tgl_selesai = dateEnToID($data['perdin']['perdin_tanggal_selesai'], 'd-m-Y');
            $data['jumlah_perdin'] = hitungcuti($tgl_mulai, $tgl_selesai,"-");
            
            $this->template->display('perdin/perdin/read', $data);
        }else{
            $this->access->redirect('404');
        }
    }

    public function list_data_aktif(){
        $default_order = "perdin_id DESC";
        $limit = 10;
        if($this->session->userdata('user_akses_id') == '3'){
            $where = "perdin_ketua_nip = ".$this->session->userdata('pegawai_nip');
        }
        else{
            $where = "perdin_status = 1";
        }
        $field_name     = array(
            'pegawai_nip',
            'pegawai_nama'
        );
        $iSortingCols   = ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
        $ordertextarr = array();
        for ($i = 0;$i<$iSortingCols;$i++){
            $iSortCol   = ($this->input->get('iSortCol_'.$i));
            $sSortDir   = (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
            $ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
        }
        
        $ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
        $search     = (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
        $limit      = (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
        $start      = (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
        $data['sEcho'] = $this->input->get('sEcho');
        $data['iTotalRecords'][] = $this->perdin_model->get_count_all_data($search,$field_name, $where);
        $data['iTotalDisplayRecords'][] = $this->perdin_model->get_count_all_data($search,$field_name, $where);


        $aaData = array();
        $getData    = $this->perdin_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
        $no = (($start == 0) ? 1 : $start + 1);
        foreach ($getData as $row) {
            $tombol_aktif = '';
            if($row["perdin_status"]==1){
                $tombol_aktif = '<li><a onclick="set_non_aktif('.$row["perdin_id"].')" class="lihat_data" data-popup="tooltip" title="Non Aktifkan" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>';
            }else{
                $tombol_aktif = '<li><a onclick="set_aktif('.$row["perdin_id"].')" class="lihat_data" data-popup="tooltip" title="Aktifkan" data-placement="bottom"><i class="icon-file-check" style="font-size: 13px;"></i></a></li>';
            }
            $aaData[] = array(
                $row["perdin_no_sp"],
                $row["pegawai_nama"],
                $row["pegawai_nip"],
                $row["perdin_tujuan_kota_nama"],
                dateEnToID($row["perdin_tanggal_mulai"], 'd M Y'),
                dateEnToID($row["perdin_tanggal_selesai"], 'd M Y'),
                // (($row["perdin_status"]==1)?'<span class="label label-success">'.$row["perdin_status_text"].'</span>':'<span class="label label-danger">'.$row["perdin_status_text"].'</span>'),
                '<ul class="icons-list">
                <li><a href="'.base_url().'perdin/perdin/update/'.urlencode($row["perdin_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
                <li><a href="'.base_url().'perdin/perdin/read/'.urlencode($row["perdin_id"]).'" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>'.
                $tombol_aktif.'
                </ul>'
            );
            $no++;
        }
        $data['aaData'] = $aaData;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));

    }

    public function non_aktif($perdin_id = 0){

        $perdin_idFilter = filter_var($perdin_id, FILTER_SANITIZE_NUMBER_INT);
        if($this->access->permission('delete')) {
            if($perdin_id==$perdin_idFilter) {

                $dataupdate = array(
                    'perdin_status'               => 0,
                    'perdin_create_by'     => $this->session->userdata('user_id'),
                    'perdin_create_date'   => date('Y-m-d H:i:s')
                );

                $del = $this->perdin_model->updatePerdin($dataupdate,$perdin_id);
                $notify = array(
                    'title'     => 'Berhasil!',
                    'message'   => 'Pengajuan perdin dinonaktifkan',
                    'status'    => 'success'
                );
                $this->session->set_flashdata('notify', $notify);

                redirect(base_url().'perdin/perdin');
            } else {
                $notify = array(
                    'title'     => 'Gagal!',
                    'message'   => 'Pengajuan perdin gagal dinonaktifkan',
                    'status'    => 'error'
                );
                $this->session->set_flashdata('notify', $notify);
                redirect(base_url().'perdin/perdin');
            }
        } else {
            $notify = array(
                'title'     => 'Gagal!',
                'message'   => 'Pengajuan perdin gagal dinonaktifkan',
                'status'    => 'error'
            );
            $this->session->set_flashdata('notify', $notify);
            redirect(base_url().'perdin/perdin');
        }
    }

    public function aktif($perdin_id = 0){

        $perdin_idFilter = filter_var($perdin_id, FILTER_SANITIZE_NUMBER_INT);
        if($this->access->permission('update')) {
            if($perdin_id==$perdin_idFilter) {

                $dataupdate = array(
                    'perdin_status'               => 1,
                    'perdin_create_by'            => $this->session->userdata('user_id'),
                    'perdin_create_date'          => date('Y-m-d H:i:s')
                );

                $del = $this->perdin_model->updatePerdin($dataupdate,$perdin_id);
                $notify = array(
                    'title'     => 'Berhasil!',
                    'message'   => 'perdin Berhasil diaktifkan',
                    'status'    => 'success'
                );
                $this->session->set_flashdata('notify', $notify);

                redirect(base_url().'perdin/perdin');
            } else {
                $notify = array(
                    'title'     => 'Gagal!',
                    'message'   => 'perdin Gagal diaktifkan',
                    'status'    => 'error'
                );
                $this->session->set_flashdata('notify', $notify);
                redirect(base_url().'perdin/perdin');
            }
        } else {
            $notify = array(
                'title'     => 'Gagal!',
                'message'   => 'perdin Gagal diaktifkan',
                'status'    => 'error'
            );
            $this->session->set_flashdata('notify', $notify);
            redirect(base_url().'perdin/perdin');
        }
    }

    public function get_kota()
    {
        $provinsi_kode  = $this->input->post('provinsi_kode');
        $data["kota"] = $this->perdin_model->getKota($provinsi_kode)->result_array();
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }
    public function get_kecamatan()
    {
        $kota_kode  = $this->input->post('kota_kode');
        $data["kecamatan"] = $this->perdin_model->getKecamatan($kota_kode)->result_array();
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }
}