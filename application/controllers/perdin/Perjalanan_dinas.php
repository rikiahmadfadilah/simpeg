<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perjalanan_dinas extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('auth/auth_model','',TRUE);
		$this->load->model('perjalanan_dinas/perjalanan_dinas/perjalanan_dinas_model','',TRUE);
	}

	public function index()
	{
		$data = array();
		$this->template->display('perjalanan_dinas/perjalanan_dinas/index', $data);	
	}
	public function create()
	{
		$data["pegawai"] = $this->perjalanan_dinas_model->getPegawai()->result_array();
		$data["pangkatgolongan"] = $this->perjalanan_dinas_model->getPangkatGolongan()->result_array();
		$data["jabataninstansi"] = $this->perjalanan_dinas_model->getJabatanInstansi()->result_array();
		$this->template->display('perjalanan_dinas/perjalanan_dinas/create', $data);
	}
	public function create_action()
	{
		$perdin_detail_pegawai_nip = $this->input->post('perdin_detail_pegawai_nip');
		$perdin_ketua_golongan_id = $this->input->post('perdin_ketua_golongan_id');
		$perdin_ketua_jabatan_id = $this->input->post('perdin_ketua_jabatan_id');
		$perdin_tingkat_biaya = $this->input->post('perdin_tingkat_biaya');
		$perdin_maksud_tujuan = $this->input->post('perdin_maksud_tujuan');
		$perdin_alat_angkut = $this->input->post('perdin_alat_angkut');
		$perdin_tempat_berangkat = $this->input->post('perdin_tempat_berangkat');
		$perdin_tempat_tujuan = $this->input->post('perdin_tempat_tujuan');
		$perdin_tanggal_mulai = $this->input->post('perdin_tanggal_mulai');
		$perdin_tanggal_selesai = $this->input->post('perdin_tanggal_selesai');
		$perdin_anggaran_instansi = $this->input->post('perdin_anggaran_instansi');
		$perdin_anggaran_akun = $this->input->post('perdin_anggaran_akun');
		$perdin_anggaran_keterangan = $this->input->post('perdin_anggaran_keterangan');

		$data = array(
			'perdin_ppk_nip' => $perdin_detail_pegawai_nip,
			'perdin_ketua_golongan_id' => $perdin_ketua_golongan_id,
			'perdin_ketua_jabatan_id' => $perdin_ketua_jabatan_id,
			'perdin_tingkat_biaya' => $perdin_tingkat_biaya,
			'perdin_maksud_tujuan' => $perdin_maksud_tujuan,
			'perdin_alat_angkut' => $perdin_alat_angkut,
			'perdin_tempat_berangkat' => $perdin_tempat_berangkat,
			'perdin_tempat_tujuan' => $perdin_tempat_tujuan,
			'perdin_tanggal_mulai' => $perdin_tanggal_mulai,
			'perdin_tanggal_selesai' => $perdin_tanggal_selesai,
			'perdin_anggaran_instansi' => $perdin_anggaran_instansi,
			'perdin_anggaran_akun' => $perdin_anggaran_akun,
			'perdin_anggaran_keterangan' => $perdin_anggaran_keterangan,
			'perdin_create_by'=>$this->session->userdata('user_id'),
			'perdin_status'=>1
			);

		$perdin = $this->perjalanan_dinas_model->insertPerdin($data,'dbo.trx_perdin');
 
		$data = array(
			'perdin_detail_perdin_id'=>$perdin,
			'perdin_detail_pegawai_nip' => $perdin_detail_pegawai_nip,
			'perdin_detail_status'=>1
			);

		$this->perjalanan_dinas_model->insertPerdindetail($data,'dbo.trx_perdin_detail');
		redirect('perjalanan_dinas/perjalanan_dinas');

		// $data = array();
		// $data["pegawai"] = $this->cuti_model->getPegawai()->result_array();
		// $data["jeniscuti"] = $this->cuti_model->getJenisCuti()->result_array();
		
		
		// $this->template->display('cuti/cuti/create', $data);	
	}
	public function update($id)
	{
	$where = array('perdin_id' => $id);
	$data['perdin'] = $this->perjalanan_dinas_model->getPerdin($where,'dbo.trx_perdin')->result();
	$data["pegawai"] = $this->perjalanan_dinas_model->getPegawai()->result_array();
	$data["pangkatgolongan"] = $this->perjalanan_dinas_model->getPangkatGolongan()->result_array();
	$data["jabataninstansi"] = $this->perjalanan_dinas_model->getJabatanInstansi()->result_array();
	$data['getPegawai'] = $this->perjalanan_dinas_model->getPerdin($where,'dbo.trx_perdin')->row_array();
	$data['getPangkatGolongan'] = $this->perjalanan_dinas_model->getPerdin($where,'dbo.trx_perdin')->row_array();
	$data['getJabatanInstansi'] = $this->perjalanan_dinas_model->getPerdin($where,'dbo.trx_perdin')->row_array();
	$this->template->display('perjalanan_dinas/perjalanan_dinas/update', $data);
	}
	public function update_action()
	{
		$id = $this->input->post('perdin_id');
		$perdin_detail_pegawai_nip = $this->input->post('perdin_detail_pegawai_nip');
		$perdin_ketua_golongan_id = $this->input->post('perdin_ketua_golongan_id');
		$perdin_ketua_jabatan_id = $this->input->post('perdin_ketua_jabatan_id');
		$perdin_tingkat_biaya = $this->input->post('perdin_tingkat_biaya');
		$perdin_maksud_tujuan = $this->input->post('perdin_maksud_tujuan');
		$perdin_alat_angkut = $this->input->post('perdin_alat_angkut');
		$perdin_tempat_berangkat = $this->input->post('perdin_tempat_berangkat');
		$perdin_tempat_tujuan = $this->input->post('perdin_tempat_tujuan');
		$perdin_tanggal_mulai = $this->input->post('perdin_tanggal_mulai');
		$perdin_tanggal_selesai = $this->input->post('perdin_tanggal_selesai');
		$perdin_anggaran_instansi = $this->input->post('perdin_anggaran_instansi');
		$perdin_anggaran_akun = $this->input->post('perdin_anggaran_akun');
		$perdin_anggaran_keterangan = $this->input->post('perdin_anggaran_keterangan');

	$data = array(
			'perdin_ppk_nip' => $perdin_detail_pegawai_nip,
			'perdin_ketua_golongan_id' => $perdin_ketua_golongan_id,
			'perdin_ketua_jabatan_id' => $perdin_ketua_jabatan_id,
			'perdin_tingkat_biaya' => $perdin_tingkat_biaya,
			'perdin_maksud_tujuan' => $perdin_maksud_tujuan,
			'perdin_alat_angkut' => $perdin_alat_angkut,
			'perdin_tempat_berangkat' => $perdin_tempat_berangkat,
			'perdin_tempat_tujuan' => $perdin_tempat_tujuan,
			'perdin_tanggal_mulai' => $perdin_tanggal_mulai,
			'perdin_tanggal_selesai' => $perdin_tanggal_selesai,
			'perdin_anggaran_instansi' => $perdin_anggaran_instansi,
			'perdin_anggaran_akun' => $perdin_anggaran_akun,
			'perdin_anggaran_keterangan' => $perdin_anggaran_keterangan,
			'perdin_create_by'=>$this->session->userdata('user_id'),
			// 'perdin_detail_perdin_id'=>$idc,
			'perdin_status'=>1
	);
	$where = array(
			'perdin_id' => $id
	);

	$this->perjalanan_dinas_model->updatePerdin($where,$data,'dbo.trx_perdin');

	redirect('perjalanan_dinas/perjalanan_dinas');
	}
	public function delete($id){
		$type_id = urlencode($id);
		$status = $this->perjalanan_dinas_model->statusPerdin($type_id)->row_array();
		if ($status['perdin_status']==1) {
			$update = array(
				'perdin_status' => 0
			);
			$update_perdin = $this->perjalanan_dinas_model->updateSPerdin($update, $type_id);
			if ($update_perdin) {
				$notify = array(
	                'title' 	=> 'Berhasil!',
	                'message' 	=> 'Perjalanan Dinas berhasil di non aktifkan',
	                'status' 	=> 'success'
	            );
	            $this->session->set_flashdata('notify', $notify);
				redirect(base_url().'perjalanan_dinas/perjalanan_dinas');
			}else{
	            $notify = array(
	                'title' 	=> 'Gagal!',
	                'message' 	=> 'Perjalanan Dinas gagal di non aktifkan, silahkan coba lagi',
	                'status' 	=> 'error'
	            );
	            $this->session->set_flashdata('notify', $notify);
	            redirect(base_url().'perjalanan_dinas/perjalanan_dinas');
	        }
		} else {
	        $update = array(
				'perdin_status' => 1
			);
			$update_type = $this->perjalanan_dinas_model->updateSPerdin($update, $type_id);
			if ($update_type) {
				$notify = array(
	                'title' 	=> 'Berhasil!',
	                'message' 	=> 'Perjalanan Dinas berhasil di aktifkan',
	                'status' 	=> 'success'
	            );
	            $this->session->set_flashdata('notify', $notify);
				redirect(base_url().'perjalanan_dinas/perjalanan_dinas');
			}else{
	            $notify = array(
	                'title' 	=> 'Gagal!',
	                'message' 	=> 'Perjalanan Dinas gagal di aktifkan, silahkan coba lagi',
	                'status' 	=> 'error'
	            );
	            $this->session->set_flashdata('notify', $notify);
	            redirect(base_url().'perjalanan_dinas/perjalanan_dinas');
	        }
		}	
	}

	public function detail($id){
	$where = array('perdin_id' => $id);
	$data['perdin'] = $this->perjalanan_dinas_model->detailPerdin($where,'dbo.view_perjalanan_dinas')->result();
	$this->template->display('perjalanan_dinas/perjalanan_dinas/detail', $data);
	}
	
	public function list_data(){
		$default_order = "pegawai_nama ASC";
		$limit = 10;
		$where = "(perdin_status = '1')";
		$field_name 	= array(
			'perdin_detail_pegawai_nip',
			'pegawai_nama',
			'perdin_maksud_tujuan',
			'perdin_tanggal_mulai'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->perjalanan_dinas_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->perjalanan_dinas_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->perjalanan_dinas_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["perdin_detail_pegawai_nip"],
				$row["pegawai_nama"],
				$row["perdin_maksud_tujuan"],
				$row["perdin_tanggal_mulai"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'perjalanan_dinas/perjalanan_dinas/update/'.urlencode($row["perdin_id"]).'" class="update_data"><i class="icon-pencil7"></i></a></li>
				<li><a href="'.base_url().'perjalanan_dinas/perjalanan_dinas/detail/'.urlencode($row["perdin_id"]).'" class="detail_data"><i class="icon-file-text"></i></a></li>
				<li><a href="'.base_url().'perjalanan_dinas/perjalanan_dinas/delete/'.urlencode($row["perdin_id"]).'" class="delete_data"><i class="icon-warning"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}
}
