<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rekap_perdin extends CI_Controller{

    function __construct(){
        parent:: __construct();
        $this->load->model('perdin/perdin/rekap_perdin_model');
        //$this->load->library('access');
        $this->load->helper('xml');
        $this->load->helper('text');
    }

    public function index(){
        if($this->session->userdata('user_akses_id') == 3){
            redirect('perdin/rekap_perdin/read/'.$this->session->userdata('pegawai_nip'));
        }else{
            if($this->access->permission('read')){
                $data = array();
                $this->template->display('perdin/rekap_perdin/index', $data);
            }else{
                $this->access->redirect('404');
            }
        }
        
    }
    
    public function read($perdin_id=0){
        if($this->access->permission('read')){
            $data['pegawai'] = $this->rekap_perdin_model->getPegawai($perdin_id)->row_array();
            $banyak_perdin = $this->db->query("select count(*) banyak from trx_perdin where perdin_status = 1 and perdin_ketua_nip =".$perdin_id)->row_array();
            $data['banyak_perdin'] = $banyak_perdin['banyak'];
            $lama_perdin = $this->db->query("select * from trx_perdin where perdin_status = 1 and perdin_ketua_nip =".$perdin_id)->result_array();
            foreach ($lama_perdin as $lp) {
                $lama_p[] = ($lp['perdin_lama_waktu']);
            }
            $data['lama_perdin'] = array_sum($lama_p);
            $h = $data['lama_perdin'];
            $bln = $h/30;
            $a = $h%30;
            $mng = $a/7;
            $hr = $a%7;
            $data['bln'] = substr($bln, 0,1);
            $data['mng'] = substr($mng, 0,1);
            $data['hr'] = $hr;
            // echo "<pre>"; print_r($data['lama_perdin']);die;
            $this->template->display('perdin/rekap_perdin/read', $data);
        }else{
            $this->access->redirect('404');
        }
    }

    public function list_data_aktif(){
        $default_order = "pegawai_nama asc";
        $limit = 10;
        $where = "pegawai_status = 1 and pegawai_nip IN (SELECT perdin_ketua_nip FROM trx_perdin)";
        $field_name     = array(
            'pegawai_nip',
            'pegawai_nama'
        );
        $iSortingCols   = ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
        $ordertextarr = array();
        for ($i = 0;$i<$iSortingCols;$i++){
            $iSortCol   = ($this->input->get('iSortCol_'.$i));
            $sSortDir   = (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
            $ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
        }
        
        $ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
        $search     = (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
        $limit      = (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
        $start      = (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
        $data['sEcho'] = $this->input->get('sEcho');
        $data['iTotalRecords'][] = $this->rekap_perdin_model->get_count_all_data($search,$field_name, $where);
        $data['iTotalDisplayRecords'][] = $this->rekap_perdin_model->get_count_all_data($search,$field_name, $where);


        $aaData = array();
        $getData    = $this->rekap_perdin_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
        $no = (($start == 0) ? 1 : $start + 1);
        foreach ($getData as $row) {
            $aaData[] = array(
                $row["pegawai_nip"],
                $row["pegawai_nama"],
                $row["pegawai_nama_jabatan"],
                $row["unit_kerja_hirarki_name_full"],
                '<ul class="icons-list">
                <li><a href="'.base_url().'perdin/rekap_perdin/read/'.urlencode($row["pegawai_nip"]).'" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>'.
               '</ul>'
            );
            $no++;
        }
        $data['aaData'] = $aaData;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));

    }

}