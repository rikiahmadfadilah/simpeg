<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller{

	function __construct(){
		parent:: __construct();
		$this->load->model('pengaturan/user_model');
        //$this->load->library('access');
		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index(){
		if($this->access->permission('read')){
			$data = array();
            // $sesi = $this->session->userdata();
            // echo "<pre>";
            // print_r($sesi);die;
			$this->template->display('pengaturan/user/index', $data);
		}else{
			$this->access->redirect('404');
		}
		
	}
	public function create(){

		if($this->access->permission('create')){
			if($post = $this->input->post()){
				$datacreate = array(
					'user_nip'                 => isset($post['user_nip'])?$post['user_nip']:'',
					'username'                 => isset($post['user_nip'])?$post['user_nip']:'',
					'password'                 => md5(isset ($post['user_nip'])?$post['user_nip']:''),
					'user_create_by'           => $this->session->userdata('user_id'),
					'user_create_date'         => date('Y-m-d H:i:s'),
					'user_status'              => 1,
					'user_akses_id'          => isset($post['role_is_default'])?$post['role_is_default']:'',
					'user_default_akses_id'  => isset($post['role_is_default'])?$post['role_is_default']:'',
				);
				$user_akses_id = $post['user_akses_id'];

                // echo "<pre>"; print_r($datacreate); die;
				$pegawai_id = $this->user_model->addUser($datacreate);
				$user_id = $pegawai_id;
				for ($i=0; $i < count($user_akses_id); $i++) { 
					$role_is_default = (($post['role_is_default'] == $user_akses_id[$i])?1:0);
					$this->db->query("INSERT INTO ms_roles (role_user_id,role_akses_id,role_create_by,role_create_date,role_status,role_is_default) VALUES ('".$user_id."','".$user_akses_id[$i]."','".$this->session->userdata('user_id')."','".date('Y-m-d H:i:s')."','1','".$role_is_default."')");
				}
				if($pegawai_id > 0){
					$notify = array(
						'title'     => 'Berhasil!',
						'message'   => 'Tambah user Berhasil',
						'status'    => 'success'
					);
					$this->session->set_flashdata('notify', $notify);
					
					redirect(base_url().'pengaturan/user');
				}else{
					$notify = array(
						'title'     => 'Gagal!',
						'message'   => 'Tambah user gagal, silahkan coba lagi',
						'status'    => 'error'
					);
					$this->session->set_flashdata('notify', $notify);
					
					redirect(base_url().'pengaturan/user');
				}
			}
			
			$data = array();
			$data['pegawai']  = $this->user_model->getPegawai()->result_array();
			$data['akses']    = $this->user_model->getAkses()->result_array();
            // echo "<pre>"; print_r($data['akses']); die;
			// $data['unitkerja'] = $this->user_model->GetUnitKerja()->result_array();
			$this->template->display('pengaturan/user/create', $data);
		} else {
			$this->access->redirect('404');
		}
	}
	public function update($user_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$user_id = $post['user_id'];


				$user_id = $post['user_id'];
				$pass = $post['password'];
				if($pass == '#@$%%%$#@@'){
					$dataupdate = array(
						'user_nip'               => isset($post['user_nip'])?$post['user_nip']:'',
						'username'               => isset($post['username'])?$post['username']:'',
						'user_akses_id'          => isset($post['role_is_default'])?$post['role_is_default']:'',
						'user_default_akses_id'  => isset($post['role_is_default'])?$post['role_is_default']:'',
						'user_update_by'         => $this->session->userdata('user_id'),
						'user_update_date'       => date('Y-m-d H:i:s'),
					);
				}else{
					$dataupdate = array(
						'user_nip'               => isset($post['user_nip'])?$post['user_nip']:'',
						'username'               => isset($post['username'])?$post['username']:'',
						'password'               => md5(isset($post['password'])?$post['password']:''),
						'user_akses_id'          => isset($post['role_is_default'])?$post['role_is_default']:'',
						'user_default_akses_id'  => isset($post['role_is_default'])?$post['role_is_default']:'',
						'user_update_by'         => $this->session->userdata('user_id'),
						'user_update_date'       => date('Y-m-d H:i:s'),
					);
				}


				$user_akses_id = $post['user_akses_id'];
				$this->db->query("UPDATE ms_roles SET role_status = 0, role_is_default = 0 WHERE  role_user_id = '".$user_id."'");

				for ($i=0; $i < count($user_akses_id); $i++) { 
					$cekExist = $this->db->query("SELECT COUNT(*) Jumlah FROM ms_roles WHERE role_user_id = '".$user_id."' AND role_akses_id = '".$user_akses_id[$i]."'")->row_array();
					$role_is_default = (($post['role_is_default'] == $user_akses_id[$i])?1:0);

					if($cekExist["Jumlah"]>0){
						$this->db->query("UPDATE ms_roles SET role_status = 1, role_is_default = '".$role_is_default."' WHERE  role_user_id = '".$user_id."' AND role_akses_id = '".$user_akses_id[$i]."'");
					}else{
						$this->db->query("INSERT INTO ms_roles (role_user_id,role_akses_id,role_create_by,role_create_date,role_status,role_is_default) VALUES ('".$user_id."','".$user_akses_id[$i]."','".$this->session->userdata('user_id')."','".date('Y-m-d H:i:s')."','1','".$role_is_default."')");
					}
				}
              /*  echo "<pre>";
                print_r($post);die;
                */
                $insDb = $this->user_model->updateUser($dataupdate, $user_id);

                if($insDb > 0){
                	$notify = array(
                		'title'     => 'Berhasil!',
                		'message'   => 'Perubahan user Berhasil',
                		'status'    => 'success'
                	);
                	$this->session->set_flashdata('notify', $notify);

                	redirect(base_url().'pengaturan/user');
                }else{
                	$notify = array(
                		'title'     => 'Gagal!',
                		'message'   => 'Perubahan user gagal, silahkan coba lagi',
                		'status'    => 'error'
                	);
                	$this->session->set_flashdata('notify', $notify);
                	redirect(base_url().'pengaturan/user');
                }
            }

            $data = array();
            $data['users']      = $this->user_model->getPegawai()->result_array();
            $data['akses']     = $this->user_model->getAkses()->result_array();
            $data['roles']     = $this->user_model->getRoles($user_id)->result_array();
			$data['user']       = $this->user_model->getUser($user_id)->row_array();
			$operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');
			if($this->session->userdata('user_akses_id') == '7'){
				$data['unitkerja'] = $this->user_model->GetUnitKerja($operator_unit_kerja_kode)->result_array();
			}
			else{
				$data['unitkerja'] = $this->user_model->GetUnitKerjaAdmin()->result_array();
			}
            $this->template->display('pengaturan/user/update', $data);
        }else{
        	$this->access->redirect('404');
        }
    }

    
    public function read($user_id=0){
    	if($this->access->permission('read')){
    		$data['user']       = $this->user_model->getDetailUser($user_id)->row_array();
    		$data['akses']     = $this->user_model->getAkses()->result_array();
    		$data['roles']     = $this->user_model->getRoles($user_id)->result_array();
    		$this->template->display('pengaturan/user/read', $data);
    	}else{
    		$this->access->redirect('404');
    	}
    }

    public function list_data(){
    	$default_order = "user_id ASC";
    	$limit = 10;
        $operator_unit_kerja = $this->session->userdata('user_operator_unit_kerja_id');
        $operator_unit_kerja_kode = $this->session->userdata('user_operator_unit_kerja_kode');
        $where = "";
    	// if($this->session->userdata('user_akses_id') == '7'){
     //        $where = "user_status = 1 and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
     //    }
     //    else{
     //        $where = "user_status = 1";
     //    }
    	$field_name     = array(
		'pegawai_nip',
    		'pegawai_nama'
    	);
    	$iSortingCols   = ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
    	$ordertextarr = array();
    	for ($i = 0;$i<$iSortingCols;$i++){
    		$iSortCol   = ($this->input->get('iSortCol_'.$i));
    		$sSortDir   = (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
    		$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
    	}
    	
    	$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
    	$search     = (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
    	$limit      = (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
    	$start      = (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
    	$data['sEcho'] = $this->input->get('sEcho');
    	$data['iTotalRecords'][] = $this->user_model->get_count_all_data($search,$field_name, $where);
    	$data['iTotalDisplayRecords'][] = $this->user_model->get_count_all_data($search,$field_name, $where);


    	$aaData = array();
    	$getData    = $this->user_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
    	$no = (($start == 0) ? 1 : $start + 1);
    	foreach ($getData as $row) {
    		$tombol_aktif = '';
    		if($row["user_status"]==1){
    			$tombol_aktif = '<li><a onclick="set_non_aktif('.$row["user_id"].')" class="lihat_data" data-popup="tooltip" title="Non Aktifkan" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>';
    		}else{
    			$tombol_aktif = '<li><a onclick="set_aktif('.$row["user_id"].')" class="lihat_data" data-popup="tooltip" title="Aktifkan" data-placement="bottom"><i class="icon-file-check" style="font-size: 13px;"></i></a></li>';
    		}
    		$aaData[] = array(
			$row["pegawai_nip"],
    			$row["pegawai_nama"],
    			$row["user_default_akses_nama"],
    			(($row["user_status"]==1)?'<span class="label label-success">'.$row["user_status_text"].'</span>':'<span class="label label-danger">'.$row["user_status_text"].'</span>'),
    			'<ul class="icons-list">
    			<li><a href="'.base_url().'pengaturan/user/update/'.urlencode($row["user_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
    			<li><a href="'.base_url().'pengaturan/user/read/'.urlencode($row["user_id"]).'" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>'.
    			$tombol_aktif.'
    			</ul>'
    		);
    		$no++;
    	}
    	$data['aaData'] = $aaData;
    	$this->output->set_content_type('application/json')->set_output(json_encode($data));

    }

    public function non_aktif($user_id = 0){

    	$user_idFilter = filter_var($user_id, FILTER_SANITIZE_NUMBER_INT);
    	if($this->access->permission('delete')) {
    		if($user_id==$user_idFilter) {

    			$dataupdate = array(
    				'user_status'               => 0,
                    // 'user_create_by'            => $this->session->userdata('user_id'),
                    // 'user_create_date'          => date('Y-m-d H:i:s')
    			);

    			$del = $this->user_model->updateUser($dataupdate,$user_id);
    			$notify = array(
    				'title'     => 'Berhasil!',
    				'message'   => 'user dinonaktifkan',
    				'status'    => 'success'
    			);
    			$this->session->set_flashdata('notify', $notify);

    			redirect(base_url().'pengaturan/user');
    		} else {
    			$notify = array(
    				'title'     => 'Gagal!',
    				'message'   => 'user gagal dinonaktifkan',
    				'status'    => 'error'
    			);
    			$this->session->set_flashdata('notify', $notify);
    			redirect(base_url().'pengaturan/user');
    		}
    	} else {
    		$notify = array(
    			'title'     => 'Gagal!',
    			'message'   => 'user gagal dinonaktifkan',
    			'status'    => 'error'
    		);
    		$this->session->set_flashdata('notify', $notify);
    		redirect(base_url().'pengaturan/user');
    	}
    }

    public function aktif($user_id = 0){

    	$user_idFilter = filter_var($user_id, FILTER_SANITIZE_NUMBER_INT);
    	if($this->access->permission('update')) {
    		if($user_id==$user_idFilter) {

    			$dataupdate = array(
    				'user_status'               => 1,
                    // 'user_create_by'            => $this->session->userdata('user_id'),
                    // 'user_create_date'          => date('Y-m-d H:i:s')
    			);

    			$del = $this->user_model->updateUser($dataupdate,$user_id);
    			$notify = array(
    				'title'     => 'Berhasil!',
    				'message'   => 'user Berhasil diaktifkan',
    				'status'    => 'success'
    			);
    			$this->session->set_flashdata('notify', $notify);

    			redirect(base_url().'pengaturan/user');
    		} else {
    			$notify = array(
    				'title'     => 'Gagal!',
    				'message'   => 'user Gagal diaktifkan',
    				'status'    => 'error'
    			);
    			$this->session->set_flashdata('notify', $notify);
    			redirect(base_url().'pengaturan/user');
    		}
    	} else {
    		$notify = array(
    			'title'     => 'Gagal!',
    			'message'   => 'user Gagal diaktifkan',
    			'status'    => 'error'
    		);
    		$this->session->set_flashdata('notify', $notify);
    		redirect(base_url().'pengaturan/user');
    	}
    }
    public function checkKodeuser()
    {
    	$user_kode = $this->input->post('user_kode');
    	$user = $this->user_model->checkKodeuser($user_kode)->result_array();
    	$res = true;
    	if(count($user)>0){
    		$res = false;
    	}
    	echo json_encode($res);
    }
    public function checkNamauser()
    {
    	$pegawai_nama = $this->input->post('pegawai_nama');
    	$user = $this->user_model->checkNamauser($pegawai_nama)->result_array();
    	$res = true;
    	if(count($user)>0){
    		$res = false;
    	}
    	echo json_encode($res);
    }
}