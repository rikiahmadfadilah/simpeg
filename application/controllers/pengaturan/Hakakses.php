<?php //if ( ! defined('basepath')) exit('no direct script access allowed');

class hakakses extends ci_controller{

    function __construct(){
        parent:: __construct();
        $this->load->model($this->uri->segment(1).'/'.$this->uri->segment(2).'_model');
        //$this->load->library('access');
        $this->load->helper('xml');
        $this->load->helper('text');
    }

    public function index(){
        if($this->access->permission('read')){
            $data = array();
            $this->template->display($this->uri->segment(1).'/'.$this->uri->segment(2).'/index', $data);
        }else{
            $this->access->redirect('404');
        }
        
    }
    public function create(){
        if($this->access->permission('create')){
            if($post = $this->input->post()){
				
				$menu_id      = $this->input->post('menu_id');
				$can_read     = $this->input->post('read');
				$can_create   = $this->input->post('create');
				$can_update   = $this->input->post('update');
				$can_delete   = $this->input->post('delete');
				$can_approve  = $this->input->post('approve');
				$can_print   = $this->input->post('print');

				$jml_menu_id  = count($menu_id);
	          				  
				
                $data_insert = array(
                    'akses_nama'                 => isset($post['akses_nama'])?$post['akses_nama']:'',
                    'akses_status'               => 1,
					'akses_default_menu_id'      => 1,
					'akses_create_by'			 => $this->session->userdata('user_id'),
					'akses_create_date'			 => date('Y-m-d H:i:s')
                );

                $data_insert_id = $this->hakakses_model->add_hakakses($data_insert);

				
                if($data_insert_id > 0){
					for ($i=0;$i<$jml_menu_id;$i++){ 
						$data_insert_detail = array(
							'akses_detail_akses_id'  	=> $data_insert_id,
							'akses_detail_menu_id'   	=> $menu_id[$i],
							'akses_detail_type'      	=> 1,
							'akses_detail_status'		=> 1,
							/*
							'akses_detail_can_read' 	=> ($can_read[$i]?$can_read[$i]:'0'),
							'akses_detail_can_create' 	=> ($can_create[$i]?$can_create[$i]:'0'),
							'akses_detail_can_update' 	=> ($can_update[$i]?$can_update[$i]:'0'),
							'akses_detail_can_delete' 	=> ($can_delete[$i]?$can_delete[$i]:'0'),
							'akses_detail_can_approve' 	=> ($can_approve[$i]?$can_approve[$i]:'0'),
							'akses_detail_can_print'	=> ($can_print[$i]?$can_print[$i]:'0')						
                            */
							
							'akses_detail_can_read' 	=> ($this->input->post('re_'.$menu_id[$i])?$this->input->post('re_'.$menu_id[$i]):'0'),
							'akses_detail_can_create' 	=> ($this->input->post('cr_'.$menu_id[$i])?$this->input->post('cr_'.$menu_id[$i]):'0'),
							'akses_detail_can_update' 	=> ($this->input->post('up_'.$menu_id[$i])?$this->input->post('up_'.$menu_id[$i]):'0'),
							'akses_detail_can_delete' 	=> ($this->input->post('de_'.$menu_id[$i])?$this->input->post('de_'.$menu_id[$i]):'0'),
							'akses_detail_can_approve' 	=> ($this->input->post('ap_'.$menu_id[$i])?$this->input->post('ap_'.$menu_id[$i]):'0'),
							'akses_detail_can_print'	=> ($this->input->post('pr_'.$menu_id[$i])?$this->input->post('pr_'.$menu_id[$i]):'0')						

							);

						$data_insert_detail_id = $this->hakakses_model->add_hakakses_detail($data_insert_detail);
                        //print_r($data_insert_detail);exit;
					}

					$notify = array(
                        'title'     => 'berhasil!',
                        'message'   => 'tambah hak akses berhasil',
                        'status'    => 'success'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    
                    redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
                }else{
                    $notify = array(
                        'title'     => 'gagal!',
                        'message'   => 'tambah hak akses gagal, silahkan coba lagi',
                        'status'    => 'error'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    
                    redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
                }
            }
            
            $data = array();
            $data['data']   = $this->hakakses_model->get_menu()->result_array();
            $this->template->display($this->uri->segment(1).'/'.$this->uri->segment(2).'/create', $data);
		}   
         else 
        {
            $this->access->redirect('404');
        }
    }
	
    public function update($data_id = 0){
        if($this->access->permission('update')){
            if($post = $this->input->post()){
                $data_akses = array(
                    'akses_nama'                 => isset($post['akses_nama'])?$post['akses_nama']:'',
					'akses_update_by'			 => $this->session->userdata('user_id'),
					'akses_update_date'			 => date('Y-m-d H:i:s')
                );
								
                $akses_update = $this->hakakses_model->update_hakakses($data_akses,$post['akses_id']);

				$menu_id      = $this->input->post('menu_id');
				
				$can_read     = $this->input->post('read');
				$can_create   = $this->input->post('create');
				$can_update   = $this->input->post('update');
				$can_delete   = $this->input->post('delete');
				$can_approve  = $this->input->post('approve');
				$can_print    = $this->input->post('print');
				
				$jml_menu_id  = count($menu_id);
				for ($i=0;$i<$jml_menu_id;$i++){ 
					$data_akses_detail = array(
						'akses_detail_menu_id'   	=> $menu_id[$i],
						'akses_detail_type'      	=> 1,
						'akses_detail_status'		=> 1,
						'akses_detail_update_by'	=> $this->session->userdata('user_id'),
						'akses_detail_update_date'	=> date('Y-m-d H:i:s'),
						/*
						'akses_detail_can_read' 	=> ($can_read[$i]?$can_read[$i]:'0'),
						'akses_detail_can_create' 	=> ($can_create[$i]?$can_create[$i]:'0'),
						'akses_detail_can_update' 	=> ($can_update[$i]?$can_update[$i]:'0'),
						'akses_detail_can_delete' 	=> ($can_delete[$i]?$can_delete[$i]:'0'),
						'akses_detail_can_approve' 	=> ($can_approve[$i]?$can_approve[$i]:'0'),
						'akses_detail_can_print'	=> ($can_print[$i]?$can_print[$i]:'0')	
						*/
						'akses_detail_can_read' 	=> ($this->input->post('re_'.$menu_id[$i])?$this->input->post('re_'.$menu_id[$i]):'0'),
						'akses_detail_can_create' 	=> ($this->input->post('cr_'.$menu_id[$i])?$this->input->post('cr_'.$menu_id[$i]):'0'),
						'akses_detail_can_update' 	=> ($this->input->post('up_'.$menu_id[$i])?$this->input->post('up_'.$menu_id[$i]):'0'),
						'akses_detail_can_delete' 	=> ($this->input->post('de_'.$menu_id[$i])?$this->input->post('de_'.$menu_id[$i]):'0'),
						'akses_detail_can_approve' 	=> ($this->input->post('ap_'.$menu_id[$i])?$this->input->post('ap_'.$menu_id[$i]):'0'),
						'akses_detail_can_print'	=> ($this->input->post('pr_'.$menu_id[$i])?$this->input->post('pr_'.$menu_id[$i]):'0')						
					);
                    //print_r($data_akses_detail);exit;
					$akses_detail_update = $this->hakakses_model->update_hakakses_detail($data_akses_detail,$post['akses_detail_id'][$i]);
				}
				
				
                if($akses_update > 0){
                    $notify = array(
                        'title'     => 'berhasil!',
                        'message'   => 'perubahan hak akses berhasil',
                        'status'    => 'success'
                    );
                    $this->session->set_flashdata('notify', $notify);

                    redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
                }else{
                    $notify = array(
                        'title'     => 'gagal!',
                        'message'   => 'perubahan hak akses gagal, silahkan coba lagi',
                        'status'    => 'error'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
                }
            }
            
			$data_hakakses   = $this->hakakses_model->check_data_hakakses($data_id,1)->row_array();
            $data['akses']   = $this->hakakses_model->get_hakakses_nama($data_id,1)->row_array();
			
			if(count($data_hakakses)>0){
				$data['data']    = $this->hakakses_model->get_hakakses_detail($data_id,1)->result_array();
                $this->template->display($this->uri->segment(1).'/'.$this->uri->segment(2).'/update', $data);
            }
			else {
				$data['data']    = $this->hakakses_model->get_menu()->result_array();
                $this->template->display($this->uri->segment(1).'/'.$this->uri->segment(2).'/create', $data);
			}
            //print "==".count($data_hakakses);exit;
			
        }else{
            $this->access->redirect('404');
        }
    }

    
    public function read($data_id=0){
        if($this->access->permission('read')){
            $data = array();
            $data['akses']   = $this->hakakses_model->get_hakakses_nama($data_id,1)->row_array();
            $data['data']    = $this->hakakses_model->get_hakakses_detail($data_id,1)->result_array();

			//print_r($data);exit;	
            $this->template->display($this->uri->segment(1).'/'.$this->uri->segment(2).'/read', $data);
       }else{
            $this->access->redirect('404');
        }
    }

    public function list_data_aktif(){
        $default_order = "akses_id desc";
        $limit = 10;
        $where = "";
        $field_name     = array(
            'akses_nama'
        );
        $iSortingCols   = ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
        $ordertextarr = array();
        for ($i = 0;$i<$iSortingCols;$i++){
            $iSortCol   = ($this->input->get('iSortCol_'.$i));
            $sSortDir   = (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
            $ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
        }
        
        $ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
        $search     = (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
        $limit      = (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
        $start      = (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
        
		$data['sEcho'] = $this->input->get('sEcho');
        $data['iTotalRecords'][] = $this->hakakses_model->get_count_all_data($search,$field_name, $where);
        $data['iTotalDisplayRecords'][] = $this->hakakses_model->get_count_all_data($search,$field_name, $where);


        $aaData = array();
        $getData    = $this->hakakses_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
        $no = (($start == 0) ? 1 : $start + 1);
        foreach ($getData as $row) {
            $tombol_aktif = '';
            if($row["akses_status"]==1){
                $tombol_aktif = '<li><a onclick="set_non_aktif('.$row["akses_id"].')" class="lihat_data" data-popup="tooltip" title="Non Aktifkan" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>';
            }else{
                $tombol_aktif = '<li><a onclick="set_aktif('.$row["akses_id"].')" class="lihat_data" data-popup="tooltip" title="Aktifkan" data-placement="bottom"><i class="icon-file-check" style="font-size: 13px;"></i></a></li>';
            }
            $aaData[] = array(
                $row["akses_nama"],
                (($row["akses_status"]==1)?'<span class="label label-success">'.$row["akses_status_text"].'</span>':'<span class="label label-danger">'.$row["akses_status_text"].'</span>'),
                '<ul class="icons-list">
                <li><a href="'.base_url().'pengaturan/hakakses/update/'.urlencode($row["akses_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
                <li><a href="'.base_url().'pengaturan/hakakses/read/'.urlencode($row["akses_id"]).'" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>'.
                $tombol_aktif.'</ul>'
            );
            $no++;
        }
        $data['aaData'] = $aaData;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    public function non_aktif($data_id = 0){
        
        $filter_id = filter_var($data_id, FILTER_SANITIZE_NUMBER_INT);
        if($this->access->permission('delete')) {
           if($data_id==$filter_id) {
                $data = array(
                    'akses_status'               => 0,
					'akses_update_by'			 => $this->session->userdata('user_id'),
					'akses_update_date'			 => date('Y-m-d H:i:s')
                );

                $del = $this->hakakses_model->update_hakakses($data,$data_id);
				
                $notify = array(
                    'title'     => 'berhasil!',
                    'message'   => 'hak akses dinonaktifkan',
                    'status'    => 'success'
                );
                $this->session->set_flashdata('notify', $notify);

                redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
            } else {
                $notify = array(
                    'title'     => 'gagal!',
                    'message'   => 'hak akses gagal dinonaktifkan',
                    'status'    => 'error'
                );
                $this->session->set_flashdata('notify', $notify);
                redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
            }
        } else {
            $notify = array(
                'title'     => 'gagal!',
                'message'   => 'hak akses gagal dinonaktifkan',
                'status'    => 'error'
            );
            $this->session->set_flashdata('notify', $notify);
            redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
        }
    }

    public function aktif($data_id = 0){

        $filter_id = filter_var($data_id, FILTER_SANITIZE_NUMBER_INT);
        if($this->access->permission('update')) {
            if($data_id==$filter_id) {

                $data = array(
                    'akses_status'               => 1,
					'akses_update_by'			 => $this->session->userdata('user_id'),
					'akses_update_date'			 => date('Y-m-d H:i:s')
                );

                $del = $this->hakakses_model->update_hakakses($data,$data_id);
                $notify = array(
                    'title'     => 'berhasil!',
                    'message'   => 'hak akses berhasil diaktifkan',
                    'status'    => 'success'
                );
                $this->session->set_flashdata('notify', $notify);

                redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
            } else {
                $notify = array(
                    'title'     => 'gagal!',
                    'message'   => 'hak akses gagal diaktifkan',
                    'status'    => 'error'
                );
                $this->session->set_flashdata('notify', $notify);
                redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
            }
        } else {
            $notify = array(
                'title'     => 'gagal!',
                'message'   => 'hak akses gagal diaktifkan',
                'status'    => 'error'
            );
            $this->session->set_flashdata('notify', $notify);
            redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
        }
    }
	
    public function check_nama_hakakses()
    {
        $data = $this->hakakses_model->check_nama_hakakses($this->input->post('akses_nama'))->result_array();
        $res = true;
        if(count($data)>0){
            $res = false;
        }
        echo json_encode($res);
    }
}