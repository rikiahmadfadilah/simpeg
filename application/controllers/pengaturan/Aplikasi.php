<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aplikasi extends CI_Controller{

    function __construct(){
        parent:: __construct();
        $this->load->model('pengaturan/aplikasi_model');
        //$this->load->library('access');
        $this->load->helper('xml');
        $this->load->helper('text');
    }

    public function index(){
        if($this->access->permission('read')){
            $data = array();
            $data['getConfig'] = $this->aplikasi_model->getConfig()->result_array();
            $this->template->display('pengaturan/aplikasi/update', $data);
        }else{
            $this->access->redirect('404');
        }
        
    }
    public function update($config_id = 0){
        if($this->access->permission('update')){

            if($post = $this->input->post()){
                    $kode   = isset($post['kode'])?$post['kode']:'';
                    $value  = isset($post['value'])?$post['value']:'';

                $updated = 0;

                for ($i=0; $i < count($kode); $i++){
                    $updated = 1;

                $insDb = $this->aplikasi_model->updateConfig($kode[$i], $value[$i]);

                }
                
                if($updated > 0){
                    $notify = array(
                        'title'     => 'Berhasil!',
                        'message'   => 'Perubahan aplikasi Berhasil',
                        'status'    => 'success'
                    );
                    $this->session->set_flashdata('notify', $notify);

                    redirect(base_url().'pengaturan/aplikasi');
                }else{
                    $notify = array(
                        'title'     => 'Gagal!',
                        'message'   => 'Perubahan aplikasi gagal, silahkan coba lagi',
                        'status'    => 'error'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    redirect(base_url().'pengaturan/aplikasi');
                }
            }
            $this->template->display('pengaturan/aplikasi/update', $data);
        }else{
            $this->access->redirect('404');
        }
    }
}