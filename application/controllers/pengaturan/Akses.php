<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Akses extends CI_Controller{

    function __construct(){
        parent:: __construct();
        $this->load->model('pengaturan/akses_model');
        //$this->load->library('access');
        $this->load->helper('xml');
        $this->load->helper('text');
    }

    public function index(){
        if($this->access->permission('read')){
            $data = array();
            $this->template->display('pengaturan/akses/index', $data);
        }else{
            $this->access->redirect('404');
        }
        
    }
    public function create(){

        if($this->access->permission('create')){
            if($post = $this->input->post()){

                $create  = $this->input->post('create');
                $read    = $this->input->post('read');
                $update  = $this->input->post('update');
                $delete  = $this->input->post('delete');
                $approve = $this->input->post('approve');
                $print   = $this->input->post('print');

                $akses = array(
                    'akses_nama'                 => isset($post['akses_nama'])?$post['akses_nama']:'',
                    'akses_create_by'            => $this->session->userdata('user_id'),
                    'akses_create_date'          => date('Y-m-d H:i:s'),
                    'akses_default_menu_id'      => 1,
                    'akses_status'               => 1
                );

                $akses_id = $this->akses_model->addakses($akses);

                $akses_detail   = array();
                $newarray       = array_slice($post, 1, -1);
                $keyarr         = array_keys($newarray);
                $x=0;

                for ($i=0; $i < count($read); $i++) {
                    $akses_detail = array(
                        'akses_detail_menu_id'       => $read[$i],
                        'akses_detail_akses_id'      => $akses_id,
                        'akses_detail_can_read'      => 1,
                        'akses_Detail_status'        => 1,
                    );
                    $this->akses_model->addDetail($akses_detail);
                }

                for ($i=0; $i < count($create); $i++) {
                    $dataupdate = array(
                        'akses_detail_can_create'    => 1
                    );
                    $this->akses_model->updateDetail($create[$i],$akses_id,$dataupdate);
                }

                for ($i=0; $i < count($update); $i++) {
                    $dataupdate = array(
                        'akses_detail_can_update'    => 1
                    );
                    $this->akses_model->updateDetail($update[$i],$akses_id,$dataupdate);
                }

                for ($i=0; $i < count($delete); $i++) {
                    $dataupdate = array(
                        'akses_detail_can_delete'    => 1
                    );
                    $this->akses_model->updateDetail($delete[$i],$akses_id,$dataupdate);
                }

                for ($i=0; $i < count($approve); $i++) {
                    $dataupdate = array(
                        'akses_detail_can_approve'   => 1
                    );
                    $this->akses_model->updateDetail($approve[$i],$akses_id,$dataupdate);
                }

                for ($i=0; $i < count($print); $i++) {
                    $dataupdate = array(
                        'akses_detail_can_print'    => 1
                    );
                    $this->akses_model->updateDetail($print[$i],$akses_id,$dataupdate);
                }

                if($akses_id > 0){
                    $notify = array(
                        'title'     => 'Berhasil!',
                        'message'   => 'Tambah akses Berhasil',
                        'status'    => 'success'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    
                    redirect(base_url().'pengaturan/akses');
                }else{
                    $notify = array(
                        'title'     => 'Gagal!',
                        'message'   => 'Tambah akses gagal, silahkan coba lagi',
                        'status'    => 'error'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    
                    redirect(base_url().'pengaturan/akses');
                }
            }
            
            $data = array();
            $datamenus = $this->akses_model->get_menu()->result_array();

            $data['datamenus']      = $datamenus;
            $data['akses']   = $this->akses_model->getakses()->result_array();
            $this->template->display('pengaturan/akses/create', $data);
        } else {
            $this->access->redirect('404');
        }
    }
    public function update($idakses = 0){
        $idFilter = filter_var(urlencode($idakses), FILTER_SANITIZE_NUMBER_INT);
            // echo "<pre>";
            // print_r($this->input->post());
            // die();
        if($post = $this->input->post()){
            
            $default_menu  = isset($post['default_menu'])?$post['default_menu']:'1';
            $create  = $this->input->post('create');
            $read    = $this->input->post('read');
            $update  = $this->input->post('update');
            $delete  = $this->input->post('delete');
            $approve = $this->input->post('approve');
            $print   = $this->input->post('print');

            $akses_id   = $this->input->post('akses_id');

            $dataupdate = array(
                'akses_nama'           => isset($post['akses_namas'])?$post['akses_namas']:'',
                'akses_default_menu_id' => $default_menu,
                'akses_update_by'      => $this->session->userdata('user_id'),
                'akses_update_date'    => date('Y-m-d H:i:s'),
            );

            $id = $this->akses_model->updateakses($dataupdate, $akses_id);

            $akses_detail  = array();
            $x=0;
            $this->akses_model->delete_detail($idFilter);
            for ($i=0; $i < count($read); $i++) {
                $akses_detail = array(
                    'akses_detail_menu_id'       => $read[$i],
                    'akses_detail_akses_id'      => $idFilter,
                    'akses_detail_can_read'      => 1,
                    'akses_detail_status'        => 1,
                );
                $this->akses_model->addDetail($akses_detail);
            }

            for ($i=0; $i < count($create); $i++) {
                $dataupdate = array(
                    'akses_detail_can_create'    => 1
                );
                $this->akses_model->updateDetail($create[$i],$idFilter,$dataupdate);
            }

            for ($i=0; $i < count($update); $i++) {
                $dataupdate = array(
                    'akses_detail_can_update'    => 1
                );
                $this->akses_model->updateDetail($update[$i],$idFilter,$dataupdate);
            }

            for ($i=0; $i < count($delete); $i++) {
                $dataupdate = array(
                    'akses_detail_can_delete'    => 1
                );
                $this->akses_model->updateDetail($delete[$i],$idFilter,$dataupdate);
            }

            for ($i=0; $i < count($approve); $i++) {
                $dataupdate = array(
                    'akses_detail_can_approve'    => 1
                );
                $this->akses_model->updateDetail($approve[$i],$idFilter,$dataupdate);
            }

            for ($i=0; $i < count($print); $i++) {
                $dataupdate = array(
                    'akses_detail_can_print'    => 1
                );
                $this->akses_model->updateDetail($print[$i],$idFilter,$dataupdate);
            }

            if($idFilter > 0){
                $notify = array(
                    'title'     => 'Berhasil!',
                    'message'   => 'Perubahan akses Berhasil',
                    'status'    => 'success'
                );
                $this->session->set_flashdata('notify', $notify);

                redirect(base_url().'pengaturan/akses');
            }else{
                $notify = array(
                    'title'     => 'Gagal!',
                    'message'   => 'Perubahan akses gagal, silahkan coba lagi',
                    'status'    => 'error'
                );
                $this->session->set_flashdata('notify', $notify);
                redirect(base_url().'pengaturan/akses');
            }
        }

        $data = array();
        $datamenus = $this->akses_model->get_menu()->result_array();

        $data['datamenus']      = $datamenus;
        $data['getDetail']      = $this->akses_model->getDetailhakakses($idFilter)->row_array();
        $data['dataakses']      = $this->akses_model->getDetailakses($idFilter)->result_array();
        $data['listakses']      = $this->akses_model->getListhakakses()->result_array();
        $this->template->display('pengaturan/akses/update', $data);
    }

    
    public function read($idakses=0){
        if($this->access->permission('read')){
            $idFilter = filter_var(urlencode($idakses), FILTER_SANITIZE_NUMBER_INT);
            $datamenus = $this->akses_model->get_menu()->result_array();
            //array_push($datamenus, $menuinduk);

            $data['datamenus']      = $datamenus;
            $data['getDetail']      = $this->akses_model->getDetailhakakses($idFilter)->row_array();
            $data['dataakses']      = $this->akses_model->getDetailakses($idFilter)->result_array();
            $this->template->display('pengaturan/akses/read', $data);
        }else{
            $this->access->redirect('404');
        }
    }

    public function list_data(){
        $default_order = "akses_id DESC";
        $limit = 10;
        $where = "";
        $field_name     = array(
            'akses_nama',
            'jml_pengguna',
            'total_menu',
            'akses_status_text'
        );
        $iSortingCols   = ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
        $ordertextarr = array();
        for ($i = 0;$i<$iSortingCols;$i++){
            $iSortCol   = ($this->input->get('iSortCol_'.$i));
            $sSortDir   = (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
            $ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
        }
        
        $ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
        $search     = (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
        $limit      = (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
        $start      = (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
        $data['sEcho'] = $this->input->get('sEcho');
        $data['iTotalRecords'][] = $this->akses_model->get_count_all_data($search,$field_name, $where);
        $data['iTotalDisplayRecords'][] = $this->akses_model->get_count_all_data($search,$field_name, $where);


        $aaData = array();
        $getData    = $this->akses_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
        $no = (($start == 0) ? 1 : $start + 1);
        foreach ($getData as $row) {
            $tombol_aktif = '';
            if($row["akses_status"]==1){
                $tombol_aktif = '<li><a onclick="set_non_aktif('.$row["akses_id"].')" class="lihat_data" data-popup="tooltip" title="Non Aktifkan" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>';
            }else{
                $tombol_aktif = '<li><a onclick="set_aktif('.$row["akses_id"].')" class="lihat_data" data-popup="tooltip" title="Aktifkan" data-placement="bottom"><i class="icon-file-check" style="font-size: 13px;"></i></a></li>';
            }
            $aaData[] = array(
                $row["akses_nama"],
                $row["jml_pengguna"],
                $row["total_menu"],
                (($row["akses_status"]==1)?'<span class="label label-success">'.$row["akses_status_text"].'</span>':'<span class="label label-danger">'.$row["akses_status_text"].'</span>'),
                '<ul class="icons-list">
                <li><a href="'.base_url().'pengaturan/akses/update/'.urlencode($row["akses_id"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
                <li><a href="'.base_url().'pengaturan/akses/read/'.urlencode($row["akses_id"]).'" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>'.
                $tombol_aktif.'
                </ul>'
            );
            $no++;
        }
        $data['aaData'] = $aaData;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));

    }

    public function non_aktif($akses_id = 0){

        $akses_idFilter = filter_var($akses_id, FILTER_SANITIZE_NUMBER_INT);
        if($this->access->permission('delete')) {
            if($akses_id==$akses_idFilter) {

                $dataupdate = array(
                    'akses_status'               => 0,
                    // 'akses_create_by'            => $this->session->userdata('user_id'),
                    // 'akses_create_date'          => date('Y-m-d H:i:s')
                );

                $del = $this->akses_model->updateakses($dataupdate,$akses_id);
                $notify = array(
                    'title'     => 'Berhasil!',
                    'message'   => 'akses dinonaktifkan',
                    'status'    => 'success'
                );
                $this->session->set_flashdata('notify', $notify);

                redirect(base_url().'pengaturan/akses');
            } else {
                $notify = array(
                    'title'     => 'Gagal!',
                    'message'   => 'akses gagal dinonaktifkan',
                    'status'    => 'error'
                );
                $this->session->set_flashdata('notify', $notify);
                redirect(base_url().'pengaturan/akses');
            }
        } else {
            $notify = array(
                'title'     => 'Gagal!',
                'message'   => 'akses gagal dinonaktifkan',
                'status'    => 'error'
            );
            $this->session->set_flashdata('notify', $notify);
            redirect(base_url().'pengaturan/akses');
        }
    }

    public function aktif($akses_id = 0){

        $akses_idFilter = filter_var($akses_id, FILTER_SANITIZE_NUMBER_INT);
        if($this->access->permission('update')) {
            if($akses_id==$akses_idFilter) {

                $dataupdate = array(
                    'akses_status'               => 1,
                    // 'akses_create_by'            => $this->session->userdata('user_id'),
                    // 'akses_create_date'          => date('Y-m-d H:i:s')
                );

                $del = $this->akses_model->updateakses($dataupdate,$akses_id);
                $notify = array(
                    'title'     => 'Berhasil!',
                    'message'   => 'akses Berhasil diaktifkan',
                    'status'    => 'success'
                );
                $this->session->set_flashdata('notify', $notify);

                redirect(base_url().'pengaturan/akses');
            } else {
                $notify = array(
                    'title'     => 'Gagal!',
                    'message'   => 'akses Gagal diaktifkan',
                    'status'    => 'error'
                );
                $this->session->set_flashdata('notify', $notify);
                redirect(base_url().'pengaturan/akses');
            }
        } else {
            $notify = array(
                'title'     => 'Gagal!',
                'message'   => 'akses Gagal diaktifkan',
                'status'    => 'error'
            );
            $this->session->set_flashdata('notify', $notify);
            redirect(base_url().'pengaturan/akses');
        }
    }
    public function checkKodeakses()
    {
        $akses_kode = $this->input->post('akses_kode');
        $akses = $this->akses_model->checkKodeakses($akses_kode)->result_array();
        $res = true;
        if(count($akses)>0){
            $res = false;
        }
        echo json_encode($res);
    }
    public function checkNamaakses()
    {
        $akses_nama = $this->input->post('akses_nama');
        $akses = $this->akses_model->checkNamaakses($akses_nama)->result_array();
        $res = true;
        if(count($akses)>0){
            $res = false;
        }
        echo json_encode($res);
    }
}
