<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Import extends CI_Controller {

	public function __construct(){
		parent::__construct();
        //$this->load->model('auth/auth_model','',TRUE);
	}
	
	public function update_sinkron()
	{        
		$simpeg_db = $this->load->database('simpeg_db', TRUE); 

		$getDataDasar = $this->db->query("SELECT  pegawai_id, pegawai_old_id FROM ms_pegawai")->result_array();
		foreach ($getDataDasar as $UI) {
			echo 'UPDATE DATA DASAR SINKRON: '.$UI["pegawai_id"]."\r\n";
			$simpeg_db->query("UPDATE datadasar SET data_source_id = '".$UI["pegawai_id"]."' WHERE ID = '".$UI["pegawai_old_id"]."'");
		}

		die();

	}
	public function izin()
	{

		//$this->db->query("TRUNCATE TABLE trx_izin;");
		$simpeg_db = $this->load->database('simpeg_db', TRUE); 

		$getDataIzin = $simpeg_db->query('SELECT * FROM SEND_TO_NEW_EPEG_IZIN WHERE idpeg_leave > 1008193 ORDER BY idpeg_leave ASC')->result_array();

		foreach ($getDataIzin as $UI) {
			echo 'INSERT DATA IZIN: '.$UI["idpeg_leave"]."\r\n";
			$riwayat_jabatan_pegawai_nip = $this->db->query("SELECT TOP 1 pegawai_id, pegawai_nip FROM ms_pegawai WHERE pegawai_abs_id = '".$UI["userid"]."'")->row_array();

			$this->db->query(str_replace("''","NULL","SET IDENTITY_INSERT trx_izin ON; INSERT INTO trx_izin (
				izin_id,
				izin_jenis_id,
				izin_pegawai_id,
				izin_pegawai_nip,
				izin_tanggal_awal,
				izin_tanggal_akhir,
				izin_anak_ke,
				izin_keterangan,
				izin_status
				) VALUES (
				'".$UI["idpeg_leave"]."',
				'".$UI["idleave_group"]."',
				'".$riwayat_jabatan_pegawai_nip["pegawai_id"]."',
				'".$riwayat_jabatan_pegawai_nip["pegawai_nip"]."',
				TRY_CONVERT(datetime2, '".(($UI["tgl_start"] == '0000-00-00 00:00:00' )?null:$UI["tgl_start"])."'),
				TRY_CONVERT(datetime2, '".(($UI["tgl_end"] == '0000-00-00 00:00:00' )?null:$UI["tgl_end"])."'),		
				'".$UI["anak_ke"]."',
				'".$UI["leave_notes"]."',
				1
			) SET IDENTITY_INSERT trx_izin OFF;"));
		}

		die();

	}
	public function skp_header()
	{	
		$skp_db = $this->load->database('skp_db', TRUE); 

		// $this->db->query("TRUNCATE TABLE trx_skp_header");
		$getDataSKP = $skp_db->query('SELECT * FROM SEND_TO_NEW_EPEG_SKP_HEADER')->result_array();
		foreach ($getDataSKP as $skp) {
			echo 'INSERT DATA SKP HEADER: '.$skp["skp_header_pegawai_nama"]."\r\n";
			$skp_pegawai = $this->db->query("SELECT TOP 1 pegawai_id, pegawai_nip FROM ms_pegawai WHERE pegawai_nip_lama = '".$skp["skp_header_pegawai_nip"]."'")->row_array();
			$skp_penilai = $this->db->query("SELECT TOP 1 pegawai_id, pegawai_nip FROM ms_pegawai WHERE pegawai_nip_lama = '".$skp["skp_header_penilai_pegawai_nip"]."'")->row_array();
			$skp_penilai_atasan = $this->db->query("SELECT TOP 1 pegawai_id, pegawai_nip FROM ms_pegawai WHERE pegawai_nip_lama = '".$skp["skp_header_penilai_atasan_pegawai_id"]."'")->row_array();
			if($skp_pegawai_nip!=null){
				$this->db->query(str_replace("''","NULL","INSERT INTO trx_skp_header (
					skp_header_tanggal_mulai,
					skp_header_tanggal_selesai, 
					skp_header_keberatan_pegawai, 
					skp_header_keberatan_pegawai_tanggal, 
					skp_header_tanggapan_penilai, 
					skp_header_tanggapan_penilai_tanggal, 
					skp_header_keputusan_atasan, 
					skp_header_keputusan_atasan_tanggal,
					skp_header_rekomendasi, 
					skp_header_keterangan, 
					skp_header_is_utama, 
					skp_header_status, 
					skp_header_create_by, 
					skp_header_create_date, 
					skp_header_update_by, 
					skp_header_update_date, 
					skp_header_pegawai_id, 
					skp_header_pegawai_nip, 
					skp_header_pegawai_nama, 
					skp_header_pegawai_gol, 
					skp_header_pegawai_jabatan, 
					skp_header_pegawai_eselon, 
					skp_header_pegawai_unit_kerja, 
					skp_header_penilai_pegawai_id, 
					skp_header_penilai_pegawai_nip, 
					skp_header_penilai_pegawai_nama, 
					skp_header_penilai_pegawai_gol, 
					skp_header_penilai_pegawai_jabatan,
					skp_header_penilai_pegawai_eselon, 
					skp_header_penilai_pegawai_unit_kerja, 
					skp_header_penilai_atasan_pegawai_id, 
					skp_header_penilai_atasan_pegawai_nip, 
					skp_header_penilai_atasan_pegawai_nama, 
					skp_header_penilai_atasan_pegawai_gol, 
					skp_header_penilai_atasan_pegawai_jabatan,
					skp_header_penilai_atasan_pegawai_eselon, 
					skp_header_penilai_atasan_pegawai_unit_kerja, 
					skp_header_status_pegawai,
					skp_header_old_id 
					) VALUES (
					'".$skp["skp_header_tanggal_mulai"]."',
					'".$skp["skp_header_tanggal_selesai"]."',
					'".$skp["skp_header_keberatan_pegawai"]."',
					'".$skp["skp_header_keberatan_pegawai_tanggal"]."',
					'".$skp["skp_header_tanggapan_penilai"]."',
					'".$skp["skp_header_tanggapan_penilai_tanggal"]."',
					'".$skp["skp_header_keputusan_atasan"]."',
					'".$skp["skp_header_keputusan_atasan_tanggal"]."',
					'".$skp["skp_header_rekomendasi"]."',
					'".$skp["skp_header_keterangan"]."',
					'".$skp["skp_header_is_utama"]."',
					'".$skp["skp_header_status"]."',
					'".$skp["skp_header_create_by"]."',
					'".$skp["skp_header_create_date"]."',
					'".$skp["skp_header_update_by"]."',
					'".$skp["skp_header_update_date"]."',
					'".$skp_pegawai["pegawai_id"]."',
					'".str_replace(' ','',$skp_pegawai["pegawai_nip"])."',
					'".$skp["skp_header_pegawai_nama"]."',
					'".$skp["skp_header_pegawai_gol"]."',
					'".$skp["skp_header_pegawai_jabatan"]."',
					'".$skp["skp_header_pegawai_eselon"]."',
					'".$skp["skp_header_pegawai_unit_kerja"]."',
					'".$skp_penilai["pegawai_id"]."',
					'".str_replace(' ','',$skp_penilai["pegawai_nip"])."',
					'".$skp["skp_header_penilai_pegawai_nama"]."',
					'".$skp["skp_header_penilai_pegawai_gol"]."',
					'".$skp["skp_header_penilai_pegawai_jabatan"]."',
					'".$skp["skp_header_penilai_pegawai_eselon"]."',
					'".$skp["skp_header_penilai_pegawai_unit_kerja"]."',
					'".$skp_penilai_atasan["pegawai_id"]."',
					'".str_replace(' '.'',$skp_penilai_atasan["pegawai_nip"])."',
					'".$skp["skp_header_penilai_atasan_pegawai_nama"]."',
					'".$skp["skp_header_penilai_atasan_pegawai_gol"]."',
					'".$skp["skp_header_penilai_atasan_pegawai_jabatan"]."',
					'".$skp["skp_header_penilai_atasan_pegawai_eselon"]."',
					'".$skp["skp_header_penilai_atasan_pegawai_unit_kerja"]."',
					'".$skp["skp_header_status_pegawai"]."'
				)"));
			}
		}
		die();
	}
	public function userinfo()
	{        
		$adms_db = $this->load->database('adms_db', TRUE); 

		$getUserInfo = $adms_db->query('SELECT userid,iddatadasar,name FROM userinfo WHERE iddatadasar > 0')->result_array();
		foreach ($getUserInfo as $UI) {
			echo 'INSERT DATA USER INFO: '.$UI["name"]."\r\n";
			$this->db->query("UPDATE ms_pegawai SET pegawai_abs_id = '".$UI["userid"]."' WHERE pegawai_old_id = '".$UI["iddatadasar"]."'");
		}

		die();

	}
	public function jabatan()
	{

		$this->db->query("UPDATE a SET a.pegawai_jabatan_fungsional_id = c.jabatan_id FROM ms_pegawai a INNER JOIN ms_jabatan_fix c ON a.pegawai_nama_jabatan = c.jabatan_nama WHERE a.pegawai_jabatan_id = 11 AND c.jabatan_tipe = 3 AND a.pegawai_is_pns = 1;");

		$this->db->query("UPDATE a SET a.pegawai_nama_jabatan = c.jabatan_nama FROM ms_pegawai a INNER JOIN ms_unit_kerja b ON a.pegawai_unit_kerja_id = b.unit_kerja_id INNER JOIN ms_jabatan_fix c ON a.pegawai_unit_kerja_id = c.jabatan_unit_kerja_id WHERE (a.pegawai_jabatan_id < 10 AND a.pegawai_jabatan_id IS NOT NULL) AND a.pegawai_is_pns = 1;");
		die();
	}
	public function unker_lokasi(){
		$this->db->query("TRUNCATE TABLE trx_jenjang_jabatan_fungsional;");
		$simpeg_db = $this->load->database('simpeg_db', TRUE); 

		$getDataRiwayatJabatan = $simpeg_db->query('SELECT * FROM tabel_unit_kerja')->result_array();
		foreach ($getDataRiwayatJabatan as $riwayat_jabatan) {
			echo 'UPDATE UNIT KERJA : '.$riwayat_jabatan["nama_unker"]."\r\n";
			$getUnitKerjaId = $this->db->query("SELECT TOP 1 * FROM [ms_unit_kerja] WHERE unit_kerja_kode = '".$riwayat_jabatan["kode_unker_1"]."'")->row_array();
			if($getUnitKerjaId!=null){
				$this->db->query(str_replace("''","NULL","UPDATE ms_unit_kerja SET unit_kerja_provinsi_id = '".$riwayat_jabatan["idpropinsi"]."', unit_kerja_kota_id = '".$riwayat_jabatan["idkota"]."', unit_kerja_kec_id = '".$riwayat_jabatan["idkecamatan"]."' WHERE unit_kerja_id = '".$getUnitKerjaId["unit_kerja_id"]."'"));
			}
		}
		
	}
	public function jabatan_fungsional(){
		$this->db->query("TRUNCATE TABLE trx_jenjang_jabatan_fungsional;");
		$simpeg_db = $this->load->database('simpeg_db', TRUE); 

		$getDataRiwayatJabatan = $simpeg_db->query('SELECT * FROM SEND_TO_NEW_EPEG_RIWAYAT_JABATAN_FUNGSIONAL')->result_array();
		foreach ($getDataRiwayatJabatan as $riwayat_jabatan) {
			echo 'INSERT DATA RIWAYAT JABATAN FUNGSIONAL : '.$riwayat_jabatan["jenjang_jabatan_fungsional_fungsional_kode"]."\r\n";
			$riwayat_jabatan_pegawai_nip = $this->db->query("SELECT TOP 1 pegawai_id, pegawai_nip FROM ms_pegawai WHERE pegawai_nip_lama = '".$riwayat_jabatan["jenjang_jabatan_fungsional_pegawai_nip"]."'")->row_array();
			if($riwayat_jabatan_pegawai_nip!=null){
				$this->db->query(str_replace("''","NULL","INSERT INTO trx_jenjang_jabatan (
					jenjang_jabatan_pegawai_id,
					jenjang_jabatan_pegawai_nip,
					jenjang_jabatan_jabatan_nama,
					jenjang_jabatan_tipe,
					jenjang_jabatan_jabatan_id,
					jenjang_jabatan_nomor_sk,
					jenjang_jabatan_tanggal_sk,
					jenjang_jabatan_tanggal_mulai,
					jenjang_jabatan_angka_kredit,
					jenjang_jabatan_unit_kerja_nama,
					jenjang_jabatan_status
					) VALUES (
					'".$riwayat_jabatan_pegawai_nip["pegawai_id"]."',
					'".$riwayat_jabatan_pegawai_nip["pegawai_nip"]."',
					'".$riwayat_jabatan["jenjang_jabatan_fungsional_fungsional_kode"]."',
					'2',
					'11',
					'".$riwayat_jabatan["jenjang_jabatan_fungsional_nomor_sk"]."',
					'".$riwayat_jabatan["jenjang_jabatan_fungsional_tanggal_sk"]."',
					'".$riwayat_jabatan["jenjang_jabatan_fungsional_tmt_jab_fung"]."',				
					'".$riwayat_jabatan["jenjang_jabatan_fungsional_angka_kredit"]."',
					'".$riwayat_jabatan["jenjang_jabatan_fungsional_unit_kerja_nama"]."',
					1
				)"));
			}
		}
		$this->db->query("UPDATE a SET a.jenjang_jabatan_jabatan_nama = b.fungsional_jenjang FROM trx_jenjang_jabatan a INNER JOIN ms_fungsional b ON a.jenjang_jabatan_jabatan_nama = b.fungsional_kode WHERE jenjang_jabatan_tipe = 2");
	}
	public function riwayat_jabatan (){
		$this->db->query("TRUNCATE TABLE trx_jenjang_jabatan;");
		$adms_db = $this->load->database('adms_db', TRUE); 
		$simpeg_db = $this->load->database('simpeg_db', TRUE); 
		$epegawai_db = $this->load->database('epegawai_db', TRUE); 

		$getDataRiwayatJabatan = $simpeg_db->query('SELECT * FROM SEND_TO_NEW_EPEG_RIWAYAT_JABATAN')->result_array();
		foreach ($getDataRiwayatJabatan as $riwayat_jabatan) {
			echo 'INSERT DATA RIWAYAT JABATAN : '.$riwayat_jabatan["jenjang_jabatan_jabatan_nama"]."\r\n";
			$riwayat_jabatan_pegawai_nip = $this->db->query("SELECT TOP 1 pegawai_id, pegawai_nip FROM ms_pegawai WHERE pegawai_nip_lama = '".$riwayat_jabatan["jenjang_jabatan_pegawai_nip"]."'")->row_array();
			if($riwayat_jabatan_pegawai_nip!=null){
				$this->db->query(str_replace("''","NULL","INSERT INTO trx_jenjang_jabatan (
					jenjang_jabatan_pegawai_id,
					jenjang_jabatan_pegawai_nip,
					jenjang_jabatan_jabatan_id,
					jenjang_jabatan_jabatan_nama,
					jenjang_jabatan_tanggal_mulai,
					jenjang_jabatan_tanggal_selesai,
					jenjang_jabatan_nomor_sk,
					jenjang_jabatan_tanggal_sk,
					jenjang_jabatan_unit_kerja_nama,
					jenjang_jabatan_tipe,
					jenjang_jabatan_status
					) VALUES (
					'".$riwayat_jabatan_pegawai_nip["pegawai_id"]."',
					'".$riwayat_jabatan_pegawai_nip["pegawai_nip"]."',
					'".$riwayat_jabatan["jenjang_jabatan_jabatan_id"]."',
					'".$riwayat_jabatan["jenjang_jabatan_jabatan_nama"]."',
					'".$riwayat_jabatan["jenjang_jabatan_tanggal_mulai"]."',
					'".$riwayat_jabatan["jenjang_jabatan_tanggal_selesai"]."',
					'".$riwayat_jabatan["jenjang_jabatan_nomor_sk"]."',
					'".$riwayat_jabatan["jenjang_jabatan_tanggal_sk"]."',
					'".$riwayat_jabatan["jenjang_jabatan_unit_kerja_nama"]."',
					'".$riwayat_jabatan["jenjang_jabatan_tipe"]."',
					1
				)"));
			}
		}
		die();
	}
	public function seminar()
	{	
		$adms_db = $this->load->database('adms_db', TRUE); 
		$simpeg_db = $this->load->database('simpeg_db', TRUE); 
		$epegawai_db = $this->load->database('epegawai_db', TRUE); 

		$this->db->query("TRUNCATE TABLE trx_seminar");
		$getDataKeluarga = $simpeg_db->query('SELECT * FROM SEND_TO_NEW_EPEG_SEMINAR')->result_array();
		foreach ($getDataKeluarga as $kel) {
			echo 'INSERT DATA SEMINAR: '.$kel["seminar_nama_kegiatan"]."\r\n";
			$kel_pegawai_nip = $this->db->query("SELECT TOP 1 pegawai_id, pegawai_nip FROM ms_pegawai WHERE pegawai_nip_lama = '".$kel["seminar_pegawai_nip"]."'")->row_array();
			if($kel_pegawai_nip!=null){
				$this->db->query(str_replace("''","NULL","INSERT INTO trx_seminar (
					seminar_pegawai_id,
					seminar_pegawai_nip, 
					seminar_nama_kegiatan, 
					seminar_lokasi, 
					seminar_tempat, 
					seminar_penyelenggara, 
					seminar_tahun, 
					seminar_kedudukan_id,
					seminar_status
					) VALUES (
					'".$kel_pegawai_nip["pegawai_id"]."',
					'".$kel_pegawai_nip["pegawai_nip"]."',
					'".$kel["seminar_nama_kegiatan"]."',
					'".$kel["seminar_lokasi"]."',
					'".$kel["seminar_tempat"]."',
					'".$kel["seminar_penyelenggara"]."',
					'".$kel["seminar_tahun"]."',
					'".$kel["seminar_kedudukan_id"]."',
					1
				)"));
			}
		}
		die();
	}
	public function dp3 (){
		$this->db->query("TRUNCATE TABLE trx_dp3;");
		$adms_db = $this->load->database('adms_db', TRUE); 
		$simpeg_db = $this->load->database('simpeg_db', TRUE); 
		$epegawai_db = $this->load->database('epegawai_db', TRUE); 

		$getDataRiwayatJabatan = $simpeg_db->query('SELECT * FROM SEND_TO_NEW_EPEG_DP3')->result_array();
		foreach ($getDataRiwayatJabatan as $dp3) {
			echo 'INSERT DATA DP3 : '.$dp3["dp3_pegawai_nip"]."\r\n";
			$dp3_pegawai_nip = $this->db->query("SELECT TOP 1 pegawai_id, pegawai_nip FROM ms_pegawai WHERE pegawai_nip_lama = '".$dp3["dp3_pegawai_nip"]."'")->row_array();
			if($dp3_pegawai_nip!=null){
				$this->db->query(str_replace("''","NULL","INSERT INTO trx_dp3 (
					dp3_pegawai_id,
					dp3_pegawai_nip,
					dp3_tahun_penilaian,
					dp3_kesetiaan,
					dp3_prestasi,
					dp3_tanggung_jawab,
					dp3_ketaatan,
					dp3_kejujuran,
					dp3_kerjasama,
					dp3_prakarsa,
					dp3_kepemimpinan,
					dp3_rata_rata,
					dp3_atasan_nip,
					dp3_penilai_nip,
					dp3_penilai_nama,
					dp3_penilai_jabatan,
					dp3_penilai_golongan,
					dp3_penilai_unit_kerja,
					dp3_atasan_nama,
					dp3_atasan_jabatan,
					dp3_atasan_golongan,
					dp3_atasan_unit_kerja,
					dp3_status
					) VALUES (
					'".$dp3_pegawai_nip["pegawai_id"]."',
					'".$dp3_pegawai_nip["pegawai_nip"]."',
					'".$dp3["dp3_tahun_penilaian"]."',
					'".$dp3["dp3_kesetiaan"]."',
					'".$dp3["dp3_prestasi"]."',
					'".$dp3["dp3_tanggung_jawab"]."',
					'".$dp3["dp3_ketaatan"]."',
					'".$dp3["dp3_kejujuran"]."',
					'".$dp3["dp3_kerjasama"]."',
					'".$dp3["dp3_prakarsa"]."',
					'".$dp3["dp3_kepemimpinan"]."',
					'".$dp3["dp3_rata_rata"]."',
					'".$dp3["dp3_atasan_nip"]."',
					'".$dp3["dp3_penilai_nip"]."',
					'".$dp3["dp3_penilai_nama"]."',
					'".$dp3["dp3_penilai_jabatan"]."',
					'".$dp3["dp3_penilai_golongan"]."',
					'".$dp3["dp3_penilai_unit_kerja"]."',
					'".$dp3["dp3_atasan_nama"]."',
					'".$dp3["dp3_atasan_jabatan"]."',
					'".$dp3["dp3_atasan_golongan"]."',
					'".$dp3["dp3_atasan_unit_kerja"]."',
					1
				)"));
			}
		}
		die();
	}
	public function penghargaan (){
		$this->db->query("TRUNCATE TABLE trx_penghargaan;");
		$adms_db = $this->load->database('adms_db', TRUE); 
		$simpeg_db = $this->load->database('simpeg_db', TRUE); 
		$epegawai_db = $this->load->database('epegawai_db', TRUE); 

		$getDataRiwayatJabatan = $simpeg_db->query('SELECT * FROM SEND_TO_NEW_EPEG_PENGHARGAAN')->result_array();
		foreach ($getDataRiwayatJabatan as $penghargaan) {
			echo 'INSERT DATA PENGHARGAAN : '.$penghargaan["penghargaan_pegawai_nip"]."\r\n";
			$penghargaan_pegawai_nip = $this->db->query("SELECT TOP 1 pegawai_id, pegawai_nip FROM ms_pegawai WHERE pegawai_nip_lama = '".$penghargaan["penghargaan_pegawai_nip"]."'")->row_array();
			if($penghargaan_pegawai_nip!=null){
				$this->db->query(str_replace("''","NULL","INSERT INTO trx_penghargaan (
					penghargaan_pegawai_id,
					penghargaan_pegawai_nip,
					penghargaan_nama,
					penghargaan_nomor_sk,
					penghargaan_tanggal_sk,
					penghargaan_tahun,
					penghargaan_pemberi,
					penghargaan_jabatan,
					penghargaan_status
					) VALUES (
					'".$penghargaan_pegawai_nip["pegawai_id"]."',
					'".$penghargaan_pegawai_nip["pegawai_nip"]."',
					'".$penghargaan["penghargaan_nama"]."',
					'".$penghargaan["penghargaan_nomor_sk"]."',
					'".$penghargaan["penghargaan_tanggal_sk"]."',
					'".$penghargaan["penghargaan_tahun"]."',
					'".$penghargaan["penghargaan_pemberi"]."',
					'".$penghargaan["penghargaan_jabatan"]."',
					1
				)"));
			}
		}
		die();
	}
	public function hukuman (){
		$this->db->query("TRUNCATE TABLE trx_hukuman_disiplin;");
		$adms_db = $this->load->database('adms_db', TRUE); 
		$simpeg_db = $this->load->database('simpeg_db', TRUE); 
		$epegawai_db = $this->load->database('epegawai_db', TRUE); 

		$getDataHukuman = $simpeg_db->query('SELECT * FROM SEND_TO_NEW_EPEG_HUKUMAN')->result_array();
		foreach ($getDataHukuman as $huk) {
			echo 'INSERT DATA HUKUMAN : '.$huk["hukuman_disiplin_pegawai_nip"]."\r\n";
			$huk_pegawai_nip = $this->db->query("SELECT TOP 1 pegawai_id, pegawai_nip FROM ms_pegawai WHERE pegawai_nip_lama = '".$huk["hukuman_disiplin_pegawai_nip"]."'")->row_array();
			if($huk_pegawai_nip!=null){
				$hukuman_disiplin_pp_pelanggaran_id = $this->db->query("SELECT TOP 1 pp_pelanggaran_id FROM ms_pp_pelanggaran WHERE pp_pelanggaran_kode = '".$huk["hukuman_disiplin_pp_pelanggaran_id"]."'")->row_array();
				$hukuman_disiplin_jenis_hukuman_id = $this->db->query("SELECT TOP 1 jenis_hukuman_id FROM ms_jenis_hukuman WHERE jenis_hukuman_kode = '".$huk["hukuman_disiplin_jenis_hukuman_id"]."'")->row_array();
				$hukuman_disiplin_jenis_pejabat_berwenang_id = $this->db->query("SELECT TOP 1 jenis_pejabat_berwenang_id FROM ms_jenis_pejabat_berwenang WHERE jenis_pejabat_berwenang_kode = '".$huk["hukuman_disiplin_jenis_pejabat_berwenang_id"]."'")->row_array();
				$hukuman_disiplin_peny_kep_huk_id = $this->db->query("SELECT TOP 1 peny_keputusan_hukuman_id FROM ms_jenis_penyampaian_keputusan_hukuman WHERE peny_keputusan_hukuman_kode = '".$huk["hukuman_disiplin_peny_kep_huk_id"]."'")->row_array();
				$hukuman_disiplin_upaya_adm_hukuman_id = $this->db->query("SELECT TOP 1 upaya_adm_hukuman_id FROM ms_upaya_adm_hukuman WHERE upaya_adm_hukuman_kode = '".$huk["hukuman_disiplin_upaya_adm_hukuman_id"]."'")->row_array();
				$hukuman_disiplin_status_hukuman_id = $this->db->query("SELECT TOP 1 status_hukuman_id FROM ms_status_hukuman WHERE status_hukuman_kode = '".$huk["hukuman_disiplin_status_hukuman_id"]."'")->row_array();
				
				$this->db->query(str_replace("''","NULL","INSERT INTO trx_hukuman_disiplin (
					hukuman_disiplin_pegawai_id,
					hukuman_disiplin_pegawai_nip,
					hukuman_disiplin_pp_pelanggaran_id,
					hukuman_disiplin_jenis_hukuman_id,
					hukuman_disiplin_kode_hukuman_lain,
					hukuman_disiplin_nomor_sk,
					hukuman_disiplin_tanggal_sk,
					hukuman_disiplin_tanggal_berlaku,
					hukuman_disiplin_jenis_pejabat_berwenang_id,
					hukuman_disiplin_ket_pejabatan,
					hukuman_disiplin_peny_kep_huk_id,
					hukuman_disiplin_upaya_adm_hukuman_id,
					hukuman_disiplin_status_hukuman_id,
					hukuman_disiplin_catatan,
					hukuman_disiplin_status
					) VALUES (
					'".$huk_pegawai_nip["pegawai_id"]."',
					'".$huk_pegawai_nip["pegawai_nip"]."',
					'".$hukuman_disiplin_pp_pelanggaran_id["pp_pelanggaran_id"]."',
					'".$hukuman_disiplin_jenis_hukuman_id["jenis_hukuman_id"]."',
					'".$huk["hukuman_disiplin_kode_hukuman_lain"]."',
					'".$huk["hukuman_disiplin_nomor_sk"]."',
					'".$huk["hukuman_disiplin_tanggal_sk"]."',
					'".$huk["hukuman_disiplin_tanggal_berlaku"]."',
					'".$hukuman_disiplin_jenis_pejabat_berwenang_id["jenis_pejabat_berwenang_id"]."',
					'".$huk["hukuman_disiplin_ket_pejabatan"]."',
					'".$hukuman_disiplin_peny_kep_huk_id["peny_keputusan_hukuman_id"]."',
					'".$hukuman_disiplin_upaya_adm_hukuman_id["upaya_adm_hukuman_id"]."',
					'".$hukuman_disiplin_status_hukuman_id["status_hukuman_id"]."',
					'".$huk["hukuman_disiplin_catatan"]."',
					1
				)"));
			}
		}
		die();
	}
	public function pendidikan_formal (){
		$this->db->query("TRUNCATE TABLE trx_pendidikan_formal;");
		$adms_db = $this->load->database('adms_db', TRUE); 
		$simpeg_db = $this->load->database('simpeg_db', TRUE); 
		$epegawai_db = $this->load->database('epegawai_db', TRUE); 

		$getDataHukuman = $simpeg_db->query('SELECT * FROM SEND_TO_NEW_EPEG_PENDIDIKAN_FORMAL')->result_array();
		foreach ($getDataHukuman as $huk) {
			echo 'INSERT DATA PENDIDIKAN FORMAL : '.$huk["pendidikan_formal_pegawai_nip"]."\r\n";
			$huk_pegawai_nip = $this->db->query("SELECT TOP 1 pegawai_id, pegawai_nip FROM ms_pegawai WHERE pegawai_nip_lama = '".$huk["pendidikan_formal_pegawai_nip"]."'")->row_array();
			if($huk_pegawai_nip!=null){
				$pendidikan_formal_pendidikan_id = $this->db->query("SELECT TOP 1 pendidikan_id,pendidikan_kode,pendidikan_nama FROM ms_pendidikan WHERE pendidikan_kode = '".$huk["pendidikan_formal_tingkat"]."'")->row_array();

				$this->db->query(str_replace("''","NULL","INSERT INTO trx_pendidikan_formal (
					pendidikan_formal_pegawai_id,
					pendidikan_formal_pegawai_nip,
					pendidikan_formal_pendidikan_id,
					pendidikan_formal_pendidikan,
					pendidikan_formal_nama,
					pendidikan_formal_thn_masuk,
					pendidikan_formal_thn_lulus,
					pendidikan_formal_tmp_belajar,
					pendidikan_formal_lokasi,
					pendidikan_formal_no_ijazah,
					pendidikan_formal_nama_kep,
					pendidikan_formal_fakultas,
					pendidikan_formal_jurusan,
					pendidikan_formal_pro_studi_text,
					pendidikan_formal_status
					) VALUES (
					'".$huk_pegawai_nip["pegawai_id"]."',
					'".$huk_pegawai_nip["pegawai_nip"]."',
					'".$pendidikan_formal_pendidikan_id["pendidikan_id"]."',
					'".$pendidikan_formal_pendidikan_id["pendidikan_nama"]."',
					'".$huk["pendidikan_formal_nama"]."',
					'".$huk["pendidikan_formal_thn_masuk"]."',
					'".$huk["pendidikan_formal_thn_lulus"]."',
					'".$huk["pendidikan_formal_tmp_belajar"]."',
					'".$huk["pendidikan_formal_lokasi"]."',
					'".$huk["pendidikan_formal_no_ijazah"]."',
					'".$huk["pendidikan_formal_nama_kep"]."',
					'".$huk["pendidikan_formal_fakultas"]."',
					'".$huk["pendidikan_formal_jurusan"]."',
					'".$huk["pendidikan_formal_pro_studi_text"]."',
					1
				)"));
			}
		}
		die();
	}
	public function tahap2()
	{
		$adms_db = $this->load->database('adms_db', TRUE); 
		$simpeg_db = $this->load->database('simpeg_db', TRUE); 
		$epegawai_db = $this->load->database('epegawai_db', TRUE); 

		$getUserInfo = $adms_db->query('SELECT userid,iddatadasar,name FROM userinfo WHERE iddatadasar > 0')->result_array();
		foreach ($getUserInfo as $UI) {
			echo 'INSERT DATA USER INFO: '.$UI["name"]."\r\n";
			$this->db->query("UPDATE ms_pegawai SET pegawai_abs_id = '".$UI["userid"]."' WHERE pegawai_old_id = '".$UI["iddatadasar"]."'");
		}
		die();
	}
	public function tahap3()
	{
		$adms_db = $this->load->database('adms_db', TRUE); 
		$simpeg_db = $this->load->database('simpeg_db', TRUE); 
		$epegawai_db = $this->load->database('epegawai_db', TRUE); 

		$this->db->query("TRUNCATE TABLE trx_keluarga");
		$getDataKeluarga = $simpeg_db->query('SELECT * FROM SEND_TO_NEW_EPEG_KELUARGA')->result_array();
		foreach ($getDataKeluarga as $kel) {
			echo 'INSERT DATA KELUARGA: '.$kel["keluarga_nama"]."\r\n";
			$kel_pegawai_nip = $this->db->query("SELECT TOP 1 pegawai_id, pegawai_nip FROM ms_pegawai WHERE pegawai_nip_lama = '".$kel["keluarga_pegawai_nip"]."'")->row_array();
			if($kel_pegawai_nip!=null){
				$this->db->query(str_replace("''","NULL","INSERT INTO trx_keluarga (
					keluarga_pegawai_id,
					keluarga_pegawai_nip, 
					keluarga_nama, 
					keluarga_jenis_kelamin_id, 
					keluarga_tanggal_lahir, 
					keluarga_hubungan_keluarga_id, 
					keluarga_pekerjaan, 
					keluarga_kondisi,
					keluarga_status
					) VALUES (
					'".$kel_pegawai_nip["pegawai_id"]."',
					'".$kel_pegawai_nip["pegawai_nip"]."',
					'".$kel["keluarga_nama"]."',
					'".$kel["keluarga_jenis_kelamin_id"]."',
					'".$kel["keluarga_tanggal_lahir"]."',
					'".$kel["keluarga_hubungan_keluarga_id"]."',
					'".$kel["keluarga_pekerjaan"]."',
					'".$kel["keluarga_kondisi"]."',
					1
				)"));
			}
		}
		die();
	}
	public function tahap5()
	{
		$adms_db = $this->load->database('adms_db', TRUE); 
		$simpeg_db = $this->load->database('simpeg_db', TRUE); 
		$epegawai_db = $this->load->database('epegawai_db', TRUE); 

		$this->db->query("TRUNCATE TABLE trx_kepangkatan");
		$getDataKepangkatan = $simpeg_db->query('SELECT * FROM SEND_TO_NEW_EPEG_RIWAYAT_KEPANGKATAN')->result_array();
		foreach ($getDataKepangkatan as $kel) {
			echo 'INSERT DATA KEPANGKATAN: '.$kel["kepangkatan_pegawai_nip"]."\r\n";
			$kel_pegawai_nip = $this->db->query("SELECT TOP 1 pegawai_id, pegawai_nip FROM ms_pegawai WHERE pegawai_nip_lama = '".$kel["kepangkatan_pegawai_nip"]."'")->row_array();
			if($kel_pegawai_nip!=null){
				$this->db->query(str_replace("''","NULL","INSERT INTO trx_kepangkatan (
					kepangkatan_pegawai_id,
					kepangkatan_pegawai_nip, 
					kepangkatan_golongan_id, 
					kepangkatan_tanggal_tmt_golongan, 
					kepangkatan_nama_pejabat, 
					kepangkatan_nomor_sk, 
					kepangkatan_tanggal_sk, 
					kepangkatan_status
					) VALUES (
					'".$kel_pegawai_nip["pegawai_id"]."',
					'".$kel_pegawai_nip["pegawai_nip"]."',
					'".$kel["kepangkatan_golongan_id"]."',
					'".$kel["kepangkatan_tanggal_tmt_golongan"]."',
					'".$kel["kepangkatan_nama_pejabat"]."',
					'".$kel["kepangkatan_nomor_sk"]."',
					'".$kel["kepangkatan_tanggal_sk"]."',
					1
				)"));
			}
		}
		die();
	}
	public function tahap4()
	{
		$adms_db = $this->load->database('adms_db', TRUE); 
		$simpeg_db = $this->load->database('simpeg_db', TRUE); 
		$epegawai_db = $this->load->database('epegawai_db', TRUE); 

		

		$this->db->query("TRUNCATE TABLE trx_pendidikan_nonformal");
		$getDataPendidikanNonFormal = $simpeg_db->query('SELECT * FROM SEND_TO_NEW_EPEG_PENDIDIKAN_NON_FORMAL')->result_array();
		foreach ($getDataPendidikanNonFormal as $pen_non_formal) {
			echo 'INSERT DATA PENDIDIKAN NON FORMAL : '.$pen_non_formal["pendidikan_nonformal_nama"]."\r\n";
			$pen_non_formal_pegawai_nip = $this->db->query("SELECT TOP 1 pegawai_id, pegawai_nip FROM ms_pegawai WHERE pegawai_nip_lama = '".$pen_non_formal["pendidikan_nonformal_pegawai_nip"]."'")->row_array();
			if($pen_non_formal_pegawai_nip!=null){
				$this->db->query(str_replace("''","NULL","INSERT INTO trx_pendidikan_nonformal (
					pendidikan_nonformal_pegawai_id,
					pendidikan_nonformal_pegawai_nip,
					pendidikan_nonformal_nama,
					pendidikan_nonformal_tempat,
					pendidikan_nonformal_kelompok,
					pendidikan_nonformal_lokasi,
					pendidikan_nonformal_tanggal_mulai,
					pendidikan_nonformal_tanggal_selesai,
					pendidikan_nonformal_penyelenggara,
					pendidikan_nonformal_jumlah_jam,
					pendidikan_nonformal_status
					) VALUES (
					'".$pen_non_formal_pegawai_nip["pegawai_id"]."',
					'".$pen_non_formal_pegawai_nip["pegawai_nip"]."',
					'".$pen_non_formal["pendidikan_nonformal_nama"]."',
					'".$pen_non_formal["pendidikan_nonformal_tempat"]."',
					'".$pen_non_formal["pendidikan_nonformal_kelompok"]."',
					'".$pen_non_formal["pendidikan_nonformal_lokasi"]."',
					'".$pen_non_formal["pendidikan_nonformal_tanggal_mulai"]."',
					'".$pen_non_formal["pendidikan_nonformal_tanggal_selesai"]."',
					'".$pen_non_formal["pendidikan_nonformal_penyelenggara"]."',
					'".$pen_non_formal["pendidikan_nonformal_jumlah_jam"]."',
					1
				)"));
			}
		}
		$this->db->query("TRUNCATE TABLE trx_diklat");
		$getDataPendidikanNonFormal = $simpeg_db->query('SELECT * FROM SEND_TO_NEW_EPEG_DIKLAT_PERJENJANGAN')->result_array();
		foreach ($getDataPendidikanNonFormal as $pen_non_formal) {
			echo 'INSERT DATA DIKLAT PERJENJANGAN : '.$pen_non_formal["diklat_nama"]."\r\n";
			$pen_non_formal_pegawai_nip = $this->db->query("SELECT TOP 1 pegawai_id, pegawai_nip FROM ms_pegawai WHERE pegawai_nip_lama = '".$pen_non_formal["diklat_pegawai_nip"]."'")->row_array();
			if($pen_non_formal_pegawai_nip!=null){
				$this->db->query(str_replace("''","NULL","INSERT INTO trx_diklat (
					diklat_pegawai_id,
					diklat_pegawai_nip,
					diklat_type,
					diklat_jenis_id,
					diklat_angkatan,
					diklat_penyelenggara,
					diklat_jumlah_jam,
					diklat_tanggal_mulai,
					diklat_tanggal_selesai,
					diklat_predikat,
					diklat_lokasi,
					diklat_nomor_sertifikat,
					diklat_tanggal_sertifikat,
					diklat_nama,
					diklat_status
					) VALUES (
					'".$pen_non_formal_pegawai_nip["pegawai_id"]."',
					'".$pen_non_formal_pegawai_nip["pegawai_nip"]."',
					'".$pen_non_formal["diklat_type"]."',
					'".$pen_non_formal["diklat_jenis_id"]."',
					'".$pen_non_formal["diklat_angkatan"]."',
					'".$pen_non_formal["diklat_penyelenggara"]."',
					'".$pen_non_formal["diklat_jumlah_jam"]."',
					'".$pen_non_formal["diklat_tanggal_mulai"]."',
					'".$pen_non_formal["diklat_tanggal_selesai"]."',
					'".$pen_non_formal["diklat_predikat"]."',
					'".$pen_non_formal["diklat_lokasi"]."',
					'".$pen_non_formal["diklat_nomor_sertifikat"]."',
					'".$pen_non_formal["diklat_tanggal_sertifikat"]."',
					'".$pen_non_formal["diklat_nama"]."',
					1
				)"));
			}
		}



/*
		
		$this->db->query("TRUNCATE TABLE trx_jenjang_jabatan;");
		$simpeg_db = $this->load->database('simpeg_db', TRUE); 

		$getDataRiwayatJabatan = $simpeg_db->query('SELECT * FROM SEND_TO_NEW_EPEG_RIWAYAT_JABATAN')->result_array();
		foreach ($getDataRiwayatJabatan as $riwayat_jabatan) {
			echo 'INSERT DATA RIWAYAT JABATAN : '.$riwayat_jabatan["jenjang_jabatan_jabatan_nama"]."\r\n";
			$riwayat_jabatan_pegawai_nip = $this->db->query("SELECT TOP 1 pegawai_id, pegawai_nip FROM ms_pegawai WHERE pegawai_nip_lama = '".$riwayat_jabatan["jenjang_jabatan_pegawai_nip"]."'")->row_array();
			if($riwayat_jabatan_pegawai_nip!=null){
				$this->db->query(str_replace("''","NULL","INSERT INTO trx_jenjang_jabatan (
					jenjang_jabatan_pegawai_id,
					jenjang_jabatan_pegawai_nip,
					jenjang_jabatan_jabatan_id,
					jenjang_jabatan_jabatan_nama,
					jenjang_jabatan_tanggal_mulai,
					jenjang_jabatan_tanggal_selesai,
					jenjang_jabatan_nomor_sk,
					jenjang_jabatan_tanggal_sk,
					jenjang_jabatan_unit_kerja_nama,
					jenjang_jabatan_status
					) VALUES (
					'".$riwayat_jabatan_pegawai_nip["pegawai_id"]."',
					'".$riwayat_jabatan_pegawai_nip["pegawai_nip"]."',
					'".$riwayat_jabatan["jenjang_jabatan_jabatan_id"]."',
					'".$riwayat_jabatan["jenjang_jabatan_jabatan_nama"]."',
					'".$riwayat_jabatan["jenjang_jabatan_tanggal_mulai"]."',
					'".$riwayat_jabatan["jenjang_jabatan_tanggal_selesai"]."',
					'".$riwayat_jabatan["jenjang_jabatan_nomor_sk"]."',
					'".$riwayat_jabatan["jenjang_jabatan_tanggal_sk"]."',
					'".$riwayat_jabatan["jenjang_jabatan_unit_kerja_nama"]."',
					1
				)"));
			}
		}

		$this->db->query("TRUNCATE TABLE trx_jenjang_jabatan_fungsional;");
		$simpeg_db = $this->load->database('simpeg_db', TRUE); 

		$getDataRiwayatJabatan = $simpeg_db->query('SELECT * FROM SEND_TO_NEW_EPEG_RIWAYAT_JABATAN_FUNGSIONAL')->result_array();
		foreach ($getDataRiwayatJabatan as $riwayat_jabatan) {
			echo 'INSERT DATA RIWAYAT JABATAN FUNGSIONAL : '.$riwayat_jabatan["jenjang_jabatan_fungsional_fungsional_kode"]."\r\n";
			$riwayat_jabatan_pegawai_nip = $this->db->query("SELECT TOP 1 pegawai_id, pegawai_nip FROM ms_pegawai WHERE pegawai_nip_lama = '".$riwayat_jabatan["jenjang_jabatan_fungsional_pegawai_nip"]."'")->row_array();
			if($riwayat_jabatan_pegawai_nip!=null){
				$this->db->query(str_replace("''","NULL","INSERT INTO trx_jenjang_jabatan_fungsional (
					jenjang_jabatan_fungsional_pegawai_id,
					jenjang_jabatan_fungsional_pegawai_nip,
					jenjang_jabatan_fungsional_fungsional_kode,
					jenjang_jabatan_fungsional_tmt_jab_fung,
					jenjang_jabatan_fungsional_nomor_sk,
					jenjang_jabatan_fungsional_tanggal_sk,
					jenjang_jabatan_fungsional_angka_kredit,
					jenjang_jabatan_fungsional_unit_kerja_nama,
					jenjang_jabatan_fungsional_status
					) VALUES (
					'".$riwayat_jabatan_pegawai_nip["pegawai_id"]."',
					'".$riwayat_jabatan_pegawai_nip["pegawai_nip"]."',
					'".$riwayat_jabatan["jenjang_jabatan_fungsional_fungsional_kode"]."',
					'".$riwayat_jabatan["jenjang_jabatan_fungsional_tmt_jab_fung"]."',
					'".$riwayat_jabatan["jenjang_jabatan_fungsional_nomor_sk"]."',
					'".$riwayat_jabatan["jenjang_jabatan_fungsional_tanggal_sk"]."',
					'".$riwayat_jabatan["jenjang_jabatan_fungsional_angka_kredit"]."',
					'".$riwayat_jabatan["jenjang_jabatan_fungsional_unit_kerja_nama"]."',
					1
				)"));
			}
		}
		$this->db->query("UPDATE a SET a.jenjang_jabatan_fungsional_fungsional_id = b.fungsional_id FROM trx_jenjang_jabatan_fungsional a INNER JOIN ms_fungsional b ON a.jenjang_jabatan_fungsional_fungsional_kode = b.fungsional_kode");
*/
		die();
	}

	public function tahap1()
	{
		$this->db->query("TRUNCATE TABLE ms_pegawai;TRUNCATE TABLE ms_user;TRUNCATE TABLE trx_anak; TRUNCATE TABLE trx_suami_istri; ");

		$simpeg_db = $this->load->database('simpeg_db', TRUE); 
		$epegawai_db = $this->load->database('epegawai_db', TRUE); 

		$getDataDasar = $simpeg_db->query('SELECT * FROM SEND_TO_NEW_EPEG')->result_array();
		foreach ($getDataDasar as $dsr) {
			echo 'INSERT DATA DASAR: '.$dsr["pegawai_nama"]."\r\n";
			$pegawai_domisili_provinsi_id = $this->db->query("SELECT TOP 1 * FROM [ms_provinsi] WHERE provinsi_kode = '".$dsr["pegawai_domisili_provinsi_id"]."' AND provinsi_status = 1")->row_array();
			$pegawai_jenis_pensiun_id = $this->db->query("SELECT TOP 1 * FROM [ms_jenis_pensiun] WHERE jenis_pensiun_kode = '".$dsr["pegawai_jenis_pensiun_id"]."' AND jenis_pensiun_status = 1")->row_array();

			$this->db->query(str_replace("''","NULL","INSERT INTO ms_pegawai (
				pegawai_nip, 
				pegawai_nip_lama,
				pegawai_nama, 
				pegawai_gelar_depan, 
				pegawai_gelar_belakang, 
				pegawai_nomor_ktp, 
				pegawai_nomor_npwp, 
				pegawai_tempat_lahir, 
				pegawai_tanggal_lahir, 
				pegawai_jenis_kelamin_id, 
				pegawai_perkawinan_id, 
				pegawai_agama_id, 
				pegawai_golongan_darah_id, 
				pegawai_domisili_alamat, 
				pegawai_domisili_kota_text, 
				pegawai_domisili_provinsi_text, 
				pegawai_domisili_kodepos, 
				pegawai_ktp_alamat, 
				pegawai_ktp_kota_text, 
				pegawai_ktp_provinsi_text, 
				pegawai_ktp_kodepos, 
				pegawai_domisili_negara_id, 
				pegawai_domisili_provinsi_id, 
				pegawai_status_pegawai_id, 
				pegawai_instansi_asal, 
				pegawai_cpns_fungsional_tipe, 
				pegawai_cpns_golongan_id, 
				pegawai_golongan_id, 
				pegawai_cpns_tanggal_tmt, 
				pegawai_tanggal_tmt, 
				pegawai_tahun_masa_kerja_golongan, 
				pegawai_bulan_masa_kerja_golongan, 
				pegawai_tahun_masa_kerja_berkala, 
				pegawai_bulan_masa_kerja_berkala, 
				pegawai_tanggal_tmt_berkala, 
				pegawai_jabatan_id, 
				pegawai_nama_jabatan, 
				pegawai_nama_jabatan_bkn, 
				pegawai_tanggal_awal_tmt_jabatan, 
				pegawai_tanggal_tmt_jabatan, 
				pegawai_telah_pra_jabatan, 
				pegawai_tahun_pra_jabatan, 
				pegawai_telah_sumpah_jabatan, 
				pegawai_tahun_sumpah_jabatan, 
				pegawai_telah_test_kesehatan, 
				pegawai_tahun_test_kesehatan, 
				pegawai_nomor_pendidikan, 
				pegawai_email, 
				pegawai_email_lain, 
				pegawai_handphone, 
				pegawai_nomor_karpeg, 
				pegawai_nomor_askes, 
				pegawai_nomor_karis, 
				pegawai_catatan, 
				pegawai_tanggal_tmt_jabfung, 
				pegawai_fungsional_kredit, 
				pegawai_jab_fung_bidang_1, 
				pegawai_jab_fung_bidang_2, 
				pegawai_tanggal_pembebasan_tmt_jab_fung, 
				pegawai_cpns_formasi_id, 
				pegawai_cpns_catatan, 
				pegawai_cpns_status_formasi, 
				pegawai_telah_kpe, 
				pegawai_penghargaan_slks_id, 
				pegawai_tahun_penghargaan_slks, 
				pegawai_diklat_jenis_id, 
				pegawai_diklat_tahun, 
				pegawai_diklat_nama, 
				pegawai_diklat_lemhanas, 
				pegawai_diklat_predikat, 
				pegawai_diklat_angkatan, 
				pegawai_nilai_toefl, 
				pegawai_nilai_ielt, 
				pegawai_pendidikan_ipk, 
				pegawai_diklat_lemhanas_tahun, 
				pegawai_image_path, 
				pegawai_old_id, 
				pegawai_unit_kerja_code,
				pegawai_pendidikan_terakhir_id,
				pegawai_jenis_pensiun_id,
				pegawai_status,
				pegawai_tanggal_pensiun,
				pegawai_is_pns
				) VALUES (
				'".$dsr["pegawai_nip"]."',
				'".$dsr["pegawai_nip_lama"]."',
				'".$dsr["pegawai_nama"]."',
				'".$dsr["pegawai_gelar_depan"]."',
				'".$dsr["pegawai_gelar_belakang"]."',
				'".$dsr["pegawai_nomor_ktp"]."',
				'".$dsr["pegawai_nomor_npwp"]."',
				'".$dsr["pegawai_tempat_lahir"]."',
				'".$dsr["pegawai_tanggal_lahir"]."',
				'".$dsr["pegawai_jenis_kelamin_id"]."',
				'".$dsr["pegawai_perkawinan_id"]."',
				'".$dsr["pegawai_agama_id"]."',
				'".$dsr["pegawai_golongan_darah_id"]."',
				'".$dsr["pegawai_domisili_alamat"]."',
				'".$dsr["pegawai_domisili_kota_text"]."',
				'".$dsr["pegawai_domisili_provinsi_text"]."',
				'".$dsr["pegawai_domisili_kodepos"]."',
				'".$dsr["pegawai_ktp_alamat"]."',
				'".$dsr["pegawai_ktp_kota_text"]."',
				'".$dsr["pegawai_ktp_provinsi_text"]."',
				'".$dsr["pegawai_ktp_kodepos"]."',
				'".$dsr["pegawai_domisili_negara_id"]."',
				'".$pegawai_domisili_provinsi_id["provinsi_id"]."',
				'".$dsr["pegawai_status_pegawai_id"]."',
				'".$dsr["pegawai_instansi_asal"]."',
				'".$dsr["pegawai_cpns_fungsional_tipe"]."',
				'".$dsr["pegawai_cpns_golongan_id"]."',
				'".$dsr["pegawai_golongan_id"]."',
				'".$dsr["pegawai_cpns_tanggal_tmt"]."',
				'".$dsr["pegawai_tanggal_tmt"]."',
				'".$dsr["pegawai_tahun_masa_kerja_golongan"]."',
				'".$dsr["pegawai_bulan_masa_kerja_golongan"]."',
				'".$dsr["pegawai_tahun_masa_kerja_berkala"]."',
				'".$dsr["pegawai_bulan_masa_kerja_berkala"]."',
				'".$dsr["pegawai_tanggal_tmt_berkala"]."',
				'".$dsr["pegawai_jabatan_id"]."',
				'".$dsr["pegawai_nama_jabatan"]."',
				'".$dsr["pegawai_nama_jabatan_bkn"]."',
				'".$dsr["pegawai_tanggal_awal_tmt_jabatan"]."',
				'".$dsr["pegawai_tanggal_tmt_jabatan"]."',
				'".$dsr["pegawai_telah_pra_jabatan"]."',
				'".$dsr["pegawai_tahun_pra_jabatan"]."',
				'".$dsr["pegawai_telah_sumpah_jabatan"]."',
				'".$dsr["pegawai_tahun_sumpah_jabatan"]."',
				'".$dsr["pegawai_telah_test_kesehatan"]."',
				'".$dsr["pegawai_tahun_test_kesehatan"]."',
				'".$dsr["pegawai_nomor_pendidikan"]."',
				'".$dsr["pegawai_email"]."',
				'".$dsr["pegawai_email_lain"]."',
				'".$dsr["pegawai_handphone"]."',
				'".$dsr["pegawai_nomor_karpeg"]."',
				'".$dsr["pegawai_nomor_askes"]."',
				'".$dsr["pegawai_nomor_karis"]."',
				'".$dsr["pegawai_catatan"]."',
				'".$dsr["pegawai_tanggal_tmt_jabfung"]."',
				'".$dsr["pegawai_fungsional_kredit"]."',
				'".$dsr["pegawai_jab_fung_bidang_1"]."',
				'".$dsr["pegawai_jab_fung_bidang_2"]."',
				'".$dsr["pegawai_tanggal_pembebasan_tmt_jab_fung"]."',
				'".$dsr["pegawai_cpns_formasi_id"]."',
				'".$dsr["pegawai_cpns_catatan"]."',
				'".$dsr["pegawai_cpns_status_formasi"]."',
				'".$dsr["pegawai_telah_kpe"]."',
				'".$dsr["pegawai_penghargaan_slks_id"]."',
				'".$dsr["pegawai_tahun_penghargaan_slks"]."',
				'".$dsr["pegawai_diklat_jenis_id"]."',
				'".$dsr["pegawai_diklat_tahun"]."',
				'".$dsr["pegawai_diklat_nama"]."',
				'".$dsr["pegawai_diklat_lemhanas"]."',
				'".$dsr["pegawai_diklat_predikat"]."',
				'".$dsr["pegawai_diklat_angkatan"]."',
				'".$dsr["pegawai_nilai_toefl"]."',
				'".$dsr["pegawai_nilai_ielt"]."',
				'".$dsr["pegawai_pendidikan_ipk"]."',
				'".$dsr["pegawai_diklat_lemhanas_tahun"]."',
				'".$dsr["pegawai_image_path"]."',
				'".$dsr["pegawai_old_id"]."',
				'".$dsr["pegawai_unit_kerja_code"]."',
				'".$dsr["pegawai_pendidikan_terakhir_id"]."',
				'".$pegawai_jenis_pensiun_id["jenis_pensiun_id"]."',
				'".$dsr["pegawai_status"]."',
				'".$dsr["pegawai_tanggal_pensiun"]."',
				'".$dsr["pegawai_is_pns"]."')"
			));
            /* Fixer Indent
            */
        }

        $this->db->query("UPDATE a SET a.pegawai_unit_kerja_id = b.unit_kerja_id FROM ms_pegawai a INNER JOIN ms_unit_kerja b ON a.pegawai_unit_kerja_code = b.unit_kerja_kode;");
        $getDataEpegawai = $epegawai_db->query('SELECT * FROM login_epegawai')->result_array();
        foreach ($getDataEpegawai as $epg) {
        	echo 'INSERT DATA USER: '.$epg["UserID"]."\r\n";

        	$this->db->query(str_replace("''","NULL","INSERT INTO ms_user (
        		username,
        		password,
        		user_old_id,
        		user_status,
        		user_akses_id,
        		user_default_akses_id,
        		user_old_id_login,
        		user_old_level
        		) VALUES (
        		'".$epg["UserID"]."',
        		'".$epg["UserPass"]."',
        		'".$epg["idpeg"]."',
        		'".(($epg["aktif"]=="Y")?1:0)."',
        		'3',
        		'3',
        		'".$epg["idlogin"]."',
        		'".$epg["Level"]."')"));
        }

        $this->db->query("UPDATE a SET user_nip = b.pegawai_nip FROM ms_user a INNER JOIN ms_pegawai b ON a.user_old_id = b.pegawai_old_id;");
        
        $getDataAnak = $simpeg_db->query('SELECT * FROM SEND_TO_NEW_EPEG_ANAK')->result_array();
        foreach ($getDataAnak as $ank) {
        	echo 'INSERT DATA ANAK: '.$ank["anak_pegawai_nama"]."\r\n";
        	$anak_pegawai_nip = $this->db->query("SELECT TOP 1 pegawai_id, pegawai_nip FROM ms_pegawai WHERE pegawai_nip_lama = '".$ank["anak_pegawai_nip"]."'")->row_array();
        	if($anak_pegawai_nip!=null){
        		$this->db->query(str_replace("''","NULL","INSERT INTO trx_anak (
        			anak_pegawai_id,
        			anak_pegawai_nip, 
        			anak_pegawai_nama, 
        			anak_jenis_kelamin_id, 
        			anak_tempat_lahir, 
        			anak_tanggal_lahir, 
        			anak_jenis_anak, 
        			anak_pekerjaan,
        			anak_pendidikan_id,
        			anak_masuk_kp4,
        			anak_kondisi,
        			anak_status
        			) VALUES (
        			'".$anak_pegawai_nip["pegawai_id"]."',
        			'".$anak_pegawai_nip["pegawai_nip"]."',
        			'".$ank["anak_pegawai_nama"]."',
        			'".$ank["anak_jenis_kelamin_id"]."',
        			'".$ank["anak_tempat_lahir"]."',
        			'".$ank["anak_tanggal_lahir"]."',
        			'".$ank["anak_jenis_anak"]."',
        			'".$ank["anak_pekerjaan"]."',
        			'".$ank["anak_pendidikan_id"]."',
        			'".$ank["anak_masuk_kp4"]."',
        			'".$ank["anak_kondisi"]."',
        			1
        		)"));
        	}
        }

        $getDataIstri = $simpeg_db->query('SELECT * FROM SEND_TO_NEW_EPEG_ISTRI')->result_array();
        foreach ($getDataIstri as $suis) {
        	echo 'INSERT DATA SUAMI ISTRI: '.$suis["suami_istri_nama"]."\r\n";
        	$suis_pegawai_nip = $this->db->query("SELECT TOP 1 pegawai_id, pegawai_nip, pegawai_jenis_kelamin_id FROM ms_pegawai WHERE pegawai_nip_lama = '".$suis["suami_istri_pegawai_nip"]."'")->row_array();
        	if($suis_pegawai_nip!=null){
        		$this->db->query(str_replace("''","NULL","INSERT INTO trx_suami_istri (
        			suami_istri_pegawai_id,
        			suami_istri_pegawai_nip, 
        			suami_istri_jenis, 
        			suami_istri_nama, 
        			suami_istri_no_karis_karsu, 
        			suami_istri_tanggal_lahir, 
        			suami_istri_tanggal_nikah, 
        			suami_istri_pendidikan_id,
        			suami_istri_pekerjaan,
        			suami_istri_jenis_status,
        			suami_istri_tanggal_status,
        			suami_istri_sk_cerai_wafat,
        			suami_istri_status
        			) VALUES (
        			'".$suis_pegawai_nip["pegawai_id"]."',
        			'".$suis_pegawai_nip["pegawai_nip"]."',
        			'".(($suis_pegawai_nip["pegawai_jenis_kelamin_id"]==1?1:2))."',
        			'".$suis["suami_istri_nama"]."',
        			'".$suis["suami_istri_no_karis_karsu"]."',
        			'".$suis["suami_istri_tanggal_lahir"]."',
        			'".$suis["suami_istri_tanggal_nikah"]."',
        			'".$suis["suami_istri_pendidikan_id"]."',
        			'".$suis["suami_istri_pekerjaan"]."',
        			'".$suis["suami_istri_jenis_status"]."',
        			'".$suis["suami_istri_tanggal_status"]."',
        			'".$suis["suami_istri_sk_cerai_wafat"]."',
        			1
        		)"));
        	}
        }

        die();
    }
    public function meninggal()
    {
    	$adms_db = $this->load->database('adms_db', TRUE); 
    	$simpeg_db = $this->load->database('simpeg_db', TRUE); 
    	$epegawai_db = $this->load->database('epegawai_db', TRUE); 
		//$epegawai_new_db = $this->load->database('epegawai_new_db', TRUE); 

    	$getDataKepangkatan = $simpeg_db->query('SELECT * FROM SEND_TO_NEW_EPEG_MENINGGAL')->result_array();
    	foreach ($getDataKepangkatan as $kel) {
    		echo 'INSERT DATA MENINGGAL: '.$kel["pegawai_nip"]."\r\n";
    		$kel_pegawai_nip = $this->db->query("SELECT TOP 1 pegawai_id, pegawai_nip FROM ms_pegawai WHERE pegawai_nip_lama = '".$kel["pegawai_nip"]."'")->row_array();
    		if($kel_pegawai_nip!=null){
    			$this->db->query(str_replace("''","NULL","UPDATE ms_pegawai SET pegawai_tanggal_wafat = '".$kel["pegawai_tanggal_wafat"]."', pegawai_tanggal_pensiun = '".$kel["pegawai_tanggal_wafat"]."', pegawai_telah_wafat = '".$kel["pegawai_telah_wafat"]."', pegawai_faktor_wafat = '".$kel["pegawai_faktor_wafat"]."', pegawai_jenis_pensiun_id = 2 WHERE pegawai_id = '".$kel_pegawai_nip["pegawai_id"]."'"));
    		}
    	}
    	die();
    }

    public function meninggalxx()
    {
    	$adms_db = $this->load->database('adms_db', TRUE); 
    	$simpeg_db = $this->load->database('simpeg_db', TRUE); 
    	$epegawai_db = $this->load->database('epegawai_db', TRUE); 
    	$epegawai_new_db = $this->load->database('epegawai_new_db', TRUE); 

    	$getDataKepangkatan = $simpeg_db->query('SELECT * FROM SEND_TO_NEW_EPEG_MENINGGAL')->result_array();
    	foreach ($getDataKepangkatan as $kel) {
    		echo 'INSERT DATA MENINGGAL: '.$kel["pegawai_nip"]."\r\n";
    		$kel_pegawai_nip = $this->db->query("SELECT TOP 1 pegawai_id, pegawai_nip FROM ms_pegawai WHERE pegawai_nip_lama = '".$kel["pegawai_nip"]."'")->row_array();
    		echo "SELECT TOP 1 pegawai_id, pegawai_nip FROM ms_pegawai WHERE pegawai_nip_lama = '".$kel["pegawai_nip"]."'";
    		if($kel_pegawai_nip!=null){
    			$this->db->query(str_replace("''","NULL","UPDATE ms_pegawai SET pegawai_tanggal_wafat = '".$kel["pegawai_tanggal_wafat"]."', pegawai_tanggal_pensiun = '".$kel["pegawai_tanggal_wafat"]."', pegawai_telah_wafat = '".$kel["pegawai_telah_wafat"]."', pegawai_faktor_wafat = '".$kel["pegawai_faktor_wafat"]."', pegawai_jenis_pensiun_id = 2 WHERE pegawai_id = '".$kel_pegawai_nip["pegawai_id"]."'"));
    		}
    	}
    	die();
    }
		/*SELECT
		trim_hapus_space (a.UserID) AS pegawai_nip,
		Level AS levelsss
		FROM
		login AS a
		WHERE a.aktif = 'Y' */
    //SEND_TO_NEW_EPEG_MENINGGAL
	}