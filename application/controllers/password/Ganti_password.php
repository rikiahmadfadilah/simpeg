<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ganti_password extends CI_Controller{

    function __construct(){
        parent:: __construct();
        $this->load->model('password/ganti_password_model');
        $this->load->helper('xml');
        $this->load->helper('text');
    }

    public function index(){
        $data = array();
        $this->template->display('password/ganti_password/ubah', $data);
    }

    public function ubah($pegawai_id){
        if($pegawai_id!=''){

            if($post = $this->input->post()){
                $user_id = $this->session->userdata('user_id');
                $password_lama = $post['password_lama'];
                $konfir_password_lama = md5($post['konfir_password_lama']);
                $password_baru = md5($post['password_baru']);
                $dataupdate = array(
                    'password' => $password_baru
                );

                if($password_lama == $konfir_password_lama){
                
                    $upd = $this->ganti_password_model->updatePassword($dataupdate, $user_id);
                    
                    if($upd>0){
                            $notify = array(
                            'title'     => 'Berhasil!',
                            'message'   => 'Perubahan Password Berhasil',
                            'status'    => 'success'
                            );
                        $this->session->set_flashdata('notify', $notify);

                    redirect(base_url().'password/ganti_password/ubah/'.$pegawai_id);
                    }else{
                        $notify = array(
                        'title'     => 'Gagal!',
                        'message'   => 'Perubahan Password gagal, silahkan coba lagi',
                        'status'    => 'error'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    redirect(base_url().'password/ganti_password/ubah/'.$pegawai_id);
                    }
                }else{
                    $notify = array(
                        'title'     => 'Gagal!',
                        'message'   => 'Password lama salah!, Perubahan Password gagal, silahkan coba lagi',
                        'status'    => 'error'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    redirect(base_url().'password/ganti_password/ubah/'.$pegawai_id);
                }
            }

            $data = array();
            $data['password_lama'] = $this->ganti_password_model->checkPasswordLama($pegawai_id)->row_array();
            // echo "<pre>"; print_r($data['password_lama']);die;
            $this->template->display('password/ganti_password/ubah', $data);
        }else{
            $this->access->redirect('404');
        }
    }

    // public function checkPasswordLama()
    // {
    //     $pegawai_id = $this->$this->session->userdata('pegawai_id');
    //     $password_lama = $this->ganti_password_model->checkPasswordLama($pegawai_id)->result_array();
    //     $password = md5($this->input->post('password_lama'));
    //     $res = true;
    //     if(count($password)>0){
    //             $res = false;
    //         }
    //     echo json_encode($res);
    // }
}