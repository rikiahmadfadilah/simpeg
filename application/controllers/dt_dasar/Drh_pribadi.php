<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Drh_pribadi extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('dt_dasar/dt_dasar_model','',TRUE);
	}

	public function index()
	{
		$id = $this->session->userdata('ID');
		$nip = $this->session->userdata('username');
		$data = array();
		$data["pegawai"] = $this->dt_dasar_model->getDetailPegawai($id)->row_array();
		$data['dt_pendidikan'] = $this->dt_dasar_model->getDataPendidikan($data['pegawai']['NIP'])->result_array();
		$data['dt_kepangkatan'] = $this->dt_dasar_model->getDataKepangkatan($data['pegawai']['NIP'])->result_array();
		$data['dt_jabatan'] = $this->dt_dasar_model->getDataJabatan($data['pegawai']['NIP'])->result_array();
		$data['dt_diklat'] = $this->dt_dasar_model->getDataDiklat($data['pegawai']['NIP'])->result_array();
		$data['dt_skp'] = $this->dt_dasar_model->getDataSKP($data['pegawai']['NIP'])->result_array();
		$data['dt_penghargaan'] = $this->dt_dasar_model->getDataPenghargaan($data['pegawai']['NIP'])->result_array();
		$data['dt_kgb'] = $this->dt_dasar_model->getDataKgb($data['pegawai']['NIP'])->result_array();
		$data['dt_cuti'] = $this->dt_dasar_model->getDataCuti($id)->result_array();
		$data['dt_keluarga'] = $this->dt_dasar_model->getDataKeluarga($data['pegawai']['NIP'])->result_array();
		$data['dt_assessment'] = $this->dt_dasar_model->getDataAssessment($data['pegawai']['NIP'])->result_array();
		$data['dt_hukdis'] = $this->dt_dasar_model->getDataHukdis($data['pegawai']['NIP'])->result_array();
		$data['dt_assessment'] = $this->dt_dasar_model->getDataAssessment($data['pegawai']['NIP'])->result_array();
		$this->template->display('riwayat/drh_pribadi/index', $data);
	}

	public function read($id){
		$data["pegawai"] = $this->dt_dasar_model->getDetailPegawai($id)->row_array();
		$data['dt_pendidikan'] = $this->dt_dasar_model->getDataPendidikan($data['pegawai']['NIP'])->result_array();
		$data['dt_kepangkatan'] = $this->dt_dasar_model->getDataKepangkatan($data['pegawai']['NIP'])->result_array();
		$data['dt_jabatan'] = $this->dt_dasar_model->getDataJabatan($data['pegawai']['NIP'])->result_array();
		$data['dt_diklat'] = $this->dt_dasar_model->getDataDiklat($data['pegawai']['NIP'])->result_array();
		$data['dt_skp'] = $this->dt_dasar_model->getDataSKP($data['pegawai']['NIP'])->result_array();
		$data['dt_penghargaan'] = $this->dt_dasar_model->getDataPenghargaan($data['pegawai']['NIP'])->result_array();
		$data['dt_kgb'] = $this->dt_dasar_model->getDataKgb($data['pegawai']['NIP'])->result_array();
		$data['dt_cuti'] = $this->dt_dasar_model->getDataCuti($id)->result_array();
		$data['dt_keluarga'] = $this->dt_dasar_model->getDataKeluarga($data['pegawai']['NIP'])->result_array();
		$data['dt_assessment'] = $this->dt_dasar_model->getDataAssessment($data['pegawai']['NIP'])->result_array();
		$data['dt_hukdis'] = $this->dt_dasar_model->getDataHukdis($data['pegawai']['NIP'])->result_array();
		$data['dt_assessment'] = $this->dt_dasar_model->getDataAssessment($data['pegawai']['NIP'])->result_array();
		// echo "<pre>"; print_r($data['dt_assessment']); die;
		$this->load->view('sources/laporan/daftar_riwayat_hidup/read', $data);
	}
}
