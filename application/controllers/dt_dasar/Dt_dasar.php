<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dt_dasar extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('dt_dasar/dt_dasar_model','',TRUE);
		$this->load->model('riwayat/riwayat_model','',TRUE);
	}

	public function index()
	{
		if($this->session->userdata('user_akses_id') == 3){
			redirect('dt_dasar/dt_dasar/proses/'.$this->session->userdata('ID'));
		}else{
			if($this->access->permission('read')){
				$data = array();
				$this->template->display('dt_dasar/index', $data);
			}else{
				$this->access->redirect('404');
			}
		}
		// $data = array();
		// $this->template->display('skp/kinerjapegawai/index', $data);	
	}

	public function list_data(){
		$default_order = "KODE_UNKER asc, ESELON asc";
		$limit = 10;
		$where = "STATUS = 1";
		$field_name 	= array(
			'NIP',
			'NAMA'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->dt_dasar_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->dt_dasar_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->dt_dasar_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$aaData[] = array(
				$row["NIP"],
				$row["NAMA"],
				$row["TMP_LAHIR"],
				$row["NAMA_JAB"],
				$row["UNIT_KERJA"],
				'<ul class="icons-list">
				<li><a href="'.base_url().'dt_dasar/dt_dasar/proses/'.urlencode($row["ID"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}

	public function create(){
		if($this->access->permission('read')){
			if($post = $this->input->post()){
				// echo '<pre>'; print_r($post); die;
				$nip = str_replace(' ', '', $post["NIP"]);
				$filephoto = null;
				$pathphoto = null;
				$path_documents = './upload/documents/';
				if (isset($_FILES['PHOTO'])) {
					if($_FILES['PHOTO']['name'] !=''){
						$pathfile = $nip."/photo/";		
						$fullpath = $path_documents.$pathfile;
						
						if (!file_exists($fullpath)) {
						    mkdir($fullpath, 0777, true);
						}

						$config['upload_path'] = $fullpath;
				 		$config['allowed_types'] = 'jpg|jpeg|png';
				 		$config['max_size'] = '20000'; // KB    
				 		// $new_name = 'gallery_'.time();
				 		// $config['file_name'] = $new_name;
				 		$this->load->library('upload');
						
						$this->upload->initialize($config);
						$this->upload->do_upload('PHOTO');
						$dataupdload = $this->upload->data();
						$filephoto = $dataupdload["file_name"];
						$pathphoto = $fullpath.$filephoto;
					}
				}
				echo "<pre>"; print_r($pathphoto); die;
				
				$datainsert = array(
					'IS_PNS' => isset($post['IS_PNS'])?$post['IS_PNS']:'',
					'NIP' => str_replace(' ', '', $post["NIP"]),
					'NO_KTP' => isset($post['NO_KTP'])?$post['NO_KTP']:'',
					'NAMA' => isset($post['NAMA'])?$post['NAMA']:'',
					'GELAR_DPN' => isset($post['GELAR_DPN'])?$post['GELAR_DPN']:'',
					'GELAR_BLK' => isset($post['GELAR_BLK'])?$post['GELAR_BLK']:'',
					'TMP_LAHIR' => isset($post['TMP_LAHIR'])?$post['TMP_LAHIR']:'',
					'TGL_LAHIR' => isset($post['TGL_LAHIR_submit'])?$post['TGL_LAHIR_submit']:'',
					'JENIS_KEL' => isset($post['JENIS_KEL'])?$post['JENIS_KEL']:'',
					'STAT_KAWIN' => isset($post['STAT_KAWIN'])?$post['STAT_KAWIN']:'',
					'AGAMA' => isset($post['AGAMA'])?$post['AGAMA']:'',
					'GOL_DARAH' => isset($post['GOL_DARAH'])?$post['GOL_DARAH']:'',
					'NO_BPJS' => isset($post['NO_BPJS'])?$post['NO_BPJS']:'',
					'NO_NPWP' => str_replace(' ', '', $post["NO_NPWP"]),
					'NO_KARPEG' => isset($post['NO_KARPEG'])?$post['NO_KARPEG']:'',
					'NO_KARIS' => isset($post['NO_KARIS'])?$post['NO_KARIS']:'',
					'KD_TASPEN' => isset($post['KD_TASPEN'])?$post['KD_TASPEN']:'',
					'ALMT_EMAIL' => isset($post['ALMT_EMAIL'])?$post['ALMT_EMAIL']:'',
					'TELPON' => isset($post['TELPON'])?$post['TELPON']:'',
					'NO_KARIS' => isset($post['NO_KARIS'])?$post['NO_KARIS']:'',
					'NO_HP_SMS' => isset($post['NO_HP_SMS'])?$post['NO_HP_SMS']:'',
					'KODE_PROVINSI' => isset($post['KODE_PROVINSI'])?$post['KODE_PROVINSI']:'',
					'KODE_KOTA' => isset($post['KODE_KOTA'])?$post['KODE_KOTA']:'',
					'KODE_KECAMATAN' => isset($post['KODE_KECAMATAN'])?$post['KODE_KECAMATAN']:'',
					'KODE_KELURAHAN' => isset($post['KODE_KELURAHAN'])?$post['KODE_KELURAHAN']:'',
					'ALAMAT' => isset($post['ALAMAT'])?$post['ALAMAT']:'',
					'KODE_POS' => isset($post['KODE_POS'])?$post['KODE_POS']:'',
					'KODE_PROVINSI2' => isset($post['KODE_PROVINSI2'])?$post['KODE_PROVINSI2']:'',
					'KODE_KOTA2' => isset($post['KODE_KOTA2'])?$post['KODE_KOTA2']:'',
					'KODE_KECAMATAN2' => isset($post['KODE_KECAMATAN2'])?$post['KODE_KECAMATAN2']:'',
					'KODE_KELURAHAN2' => isset($post['KODE_KELURAHAN2'])?$post['KODE_KELURAHAN2']:'',
					'ALAMAT2' => isset($post['ALAMAT2'])?$post['ALAMAT2']:'',
					'KODE_POS2' => isset($post['KODE_POS2'])?$post['KODE_POS2']:'',
					'PHOTO' => $filephoto,
					'STATUS' => 1
				);
				// echo "<pre>"; print_r($datainsert); die;
				$pegawai = $this->dt_dasar_model->insert_dt_dasar($datainsert);//
				if($pegawai > 0){
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Penambahan Pegawai Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'dt_dasar/dt_dasar');
				}else{
					$notify = array(
						'title' 	=> 'Gagal!',
						'message'	=> 'Penambahan Pegawai gagal, silahkan coba lagi',
						'status' 	=> 'error'
					);
					$this->session->set_flashdata('notify', $notify);
					redirect(base_url().'dt_dasar/dt_dasar');
				}
			}

			$data = array();
			$data['jenis_kelamin'] = $this->dt_dasar_model->getJenisKelamin()->result_array();
			$data['status_kawin'] = $this->dt_dasar_model->getStatusKawin()->result_array();
			$data['agama'] = $this->dt_dasar_model->getAgama()->result_array();
			$data['golongan_darah'] = $this->dt_dasar_model->getGolDarah()->result_array();
			$data['provinsi'] = $this->dt_dasar_model->getProvinsi()->result_array();
			$this->template->display('dt_dasar/create', $data);	

		}else{
			$this->access->redirect('404');
		}
	}
	public function get_kota(){
		$KODE_PROVINSI 	= $this->input->post('KODE_PROVINSI');
		$data["kota"] = $this->dt_dasar_model->getKota($KODE_PROVINSI)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}

	public function get_kecamatan(){
		$KODE_KOTA 	= $this->input->post('KODE_KOTA');
		$data["kecamatan"] = $this->dt_dasar_model->getKecamatan($KODE_KOTA)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}

	public function get_kelurahan(){
		$KODE_KECAMATAN 	= $this->input->post('KODE_KECAMATAN');
		$data["kelurahan"] = $this->dt_dasar_model->getKelurahan($KODE_KECAMATAN)->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}

	public function proses($id = 0){
		if($this->access->permission('update')){
			if($post = $this->input->post()){
				// echo "<pre>"; print_r($post); die;
				$format="";//format file name
	            $strf=(string)strftime($format);
	            $config['file_name'] = '';
	            $config['upload_path']      = './assets/images/pegawai';
	            $config['allowed_types']    = 'gif|jpg|png|svg|jpeg';
	            $config['max_size']         = '20000'; // KB    
	            $this->load->library('upload', $config);
	            $file_name = "";
	            if($this->upload->do_upload('PHOTOS')) {
	                $upload_data = array('uploads' =>$this->upload->data());
	                $file_name   =  $upload_data['uploads']['file_name'];
	            }else{
	            	$file_name = $post["PHOTO"];
	            }
	            // echo "<pre>"; print_r($file_name); die;
				$dataupdate = array(
					'IS_PNS' => isset($post['IS_PNS'])?$post['IS_PNS']:'',
					'NIP' => str_replace(' ', '', $post["NIP"]),
					'NO_KTP' => isset($post['NO_KTP'])?$post['NO_KTP']:'',
					'NAMA' => isset($post['NAMA'])?$post['NAMA']:'',
					'GELAR_DPN' => isset($post['GELAR_DPN'])?$post['GELAR_DPN']:'',
					'GELAR_BLK' => isset($post['GELAR_BLK'])?$post['GELAR_BLK']:'',
					'TMP_LAHIR' => isset($post['TMP_LAHIR'])?$post['TMP_LAHIR']:'',
					'TGL_LAHIR' => isset($post['TGL_LAHIR_submit'])?$post['TGL_LAHIR_submit']:'',
					'JENIS_KEL' => isset($post['JENIS_KEL'])?$post['JENIS_KEL']:'',
					'STAT_KAWIN' => isset($post['STAT_KAWIN'])?$post['STAT_KAWIN']:'',
					'AGAMA' => isset($post['AGAMA'])?$post['AGAMA']:'',
					'GOL_DARAH' => isset($post['GOL_DARAH'])?$post['GOL_DARAH']:'',
					'NO_BPJS' => isset($post['NO_BPJS'])?$post['NO_BPJS']:'',
					'NO_NPWP' => str_replace(' ', '', $post["NO_NPWP"]),
					'NO_KARPEG' => isset($post['NO_KARPEG'])?$post['NO_KARPEG']:'',
					'NO_KARIS' => isset($post['NO_KARIS'])?$post['NO_KARIS']:'',
					'KD_TASPEN' => isset($post['KD_TASPEN'])?$post['KD_TASPEN']:'',
					'ALMT_EMAIL' => isset($post['ALMT_EMAIL'])?$post['ALMT_EMAIL']:'',
					'TELPON' => isset($post['TELPON'])?$post['TELPON']:'',
					'NO_KARIS' => isset($post['NO_KARIS'])?$post['NO_KARIS']:'',
					'NO_HP_SMS' => isset($post['NO_HP_SMS'])?$post['NO_HP_SMS']:'',
					'KODE_PROVINSI' => isset($post['KODE_PROVINSI'])?$post['KODE_PROVINSI']:'',
					'KODE_KOTA' => isset($post['KODE_KOTA'])?$post['KODE_KOTA']:'',
					'KODE_KECAMATAN' => isset($post['KODE_KECAMATAN'])?$post['KODE_KECAMATAN']:'',
					'KODE_KELURAHAN' => isset($post['KODE_KELURAHAN'])?$post['KODE_KELURAHAN']:'',
					'ALAMAT' => isset($post['ALAMAT'])?$post['ALAMAT']:'',
					'KODE_POS' => isset($post['KODE_POS'])?$post['KODE_POS']:'',
					'KODE_PROVINSI2' => isset($post['KODE_PROVINSI2'])?$post['KODE_PROVINSI2']:'',
					'KODE_KOTA2' => isset($post['KODE_KOTA2'])?$post['KODE_KOTA2']:'',
					'KODE_KECAMATAN2' => isset($post['KODE_KECAMATAN2'])?$post['KODE_KECAMATAN2']:'',
					'KODE_KELURAHAN2' => isset($post['KODE_KELURAHAN2'])?$post['KODE_KELURAHAN2']:'',
					'ALAMAT2' => isset($post['ALAMAT2'])?$post['ALAMAT2']:'',
					'KODE_POS2' => isset($post['KODE_POS2'])?$post['KODE_POS2']:'',
					'PHOTO' => $file_name,
					'STATUS' => 1
				);
				// echo "<pre>"; print_r($dataupdate); die;
				$this->dt_dasar_model->update_dt_dasar($dataupdate, $post['ID']);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Perubahan Data Pegawai Berhasil',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$id);
			}

			$data = array();
			$data['pegawai'] = $this->dt_dasar_model->getPegawai($id)->row_array();
			$data['jenis_kelamin'] = $this->dt_dasar_model->getJenisKelamin()->result_array();
			$data['status_kawin'] = $this->dt_dasar_model->getStatusKawin()->result_array();
			$data['agama'] = $this->dt_dasar_model->getAgama()->result_array();
			$data['gol_darah'] = $this->dt_dasar_model->getGolDarah()->result_array();
			$data['provinsi'] = $this->dt_dasar_model->getProvinsi()->result_array();
			$data['dt_pendidikan'] = $this->dt_dasar_model->getDataPendidikan($data['pegawai']['NIP'])->result_array();
			$data['dt_kepangkatan'] = $this->dt_dasar_model->getDataKepangkatan($data['pegawai']['NIP'])->result_array();
			$data['dt_jabatan'] = $this->dt_dasar_model->getDataJabatan($data['pegawai']['NIP'])->result_array();
			$data['dt_diklat'] = $this->dt_dasar_model->getDataDiklat($data['pegawai']['NIP'])->result_array();
			$data['dt_skp'] = $this->dt_dasar_model->getDataSKP($data['pegawai']['NIP'])->result_array();
			$data['dt_penghargaan'] = $this->dt_dasar_model->getDataPenghargaan($data['pegawai']['NIP'])->result_array();
			$data['dt_kgb'] = $this->dt_dasar_model->getDataKgb($data['pegawai']['NIP'])->result_array();
			$data['dt_cuti'] = $this->dt_dasar_model->getDataCuti($id)->result_array();
			$data['dt_keluarga'] = $this->dt_dasar_model->getDataKeluarga($data['pegawai']['NIP'])->result_array();
			$data['dt_assessment'] = $this->dt_dasar_model->getDataAssessment($data['pegawai']['NIP'])->result_array();
			$data['dt_hukdis'] = $this->dt_dasar_model->getDataHukdis($data['pegawai']['NIP'])->result_array();
			// echo "<pre>"; print_r($data['dt_hukdis']); die;
			$this->template->display('dt_dasar/proses', $data);
		}else{
			$this->access->redirect('404');
		}
	}

	public function create_dt_pendidikan($id=0){
		if($this->access->permission('update')){
			if($post = $this->input->post()){
				$format="";//format file name
	            $strf=(string)strftime($format);
	            $config['file_name'] = '';
	            $config['upload_path']      = './assets/images/pendidikan';
	            $config['allowed_types']    = 'gif|jpg|png|svg|jpeg|pdf';
	            $config['max_size']         = '20000'; // KB    
	            $this->load->library('upload', $config);
	            $file_name = "";
	            if($this->upload->do_upload('E_DOC')) {
	                $upload_data = array('uploads' =>$this->upload->data());
	                $file_name   =  $upload_data['uploads']['file_name'];
	            }else{
	            	$file_name = '';
	            }
				$datainsert = array(
					'NIP' => isset($post['NIP'])?$post['NIP']:'',
					'TINGKAT' => isset($post['TINGKAT'])?$post['TINGKAT']:'',
					'NAMA_SEK_PT' => isset($post['NAMA_SEK_PT'])?$post['NAMA_SEK_PT']:'',
					'PRO_STUDI' => isset($post['PRO_STUDI'])?$post['PRO_STUDI']:'',
					'THN_MASUK' => isset($post['THN_MASUK'])?$post['THN_MASUK']:'',
					'THN_LULUS' => isset($post['THN_LULUS'])?$post['THN_LULUS']:'',
					'TMP_BLJ' => isset($post['TMP_BLJ'])?$post['TMP_BLJ']:'',
					'LOKASI' => isset($post['LOKASI'])?$post['LOKASI']:'',
					'NO_IJASAH' => isset($post['NO_IJASAH'])?$post['NO_IJASAH']:'',
					'NAMA_KEPSEK_REKTOR' => isset($post['NAMA_KEPSEK_REKTOR'])?$post['NAMA_KEPSEK_REKTOR']:'',
					'E_DOC' => $file_name,
					'STATUS' => 1
				);
				// echo '<pre>'; print_r($datainsert); die;
				$this->riwayat_model->insert_dt_pendidikan($datainsert);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Penambahan Data Pendidikan Berhasil',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$id);
			}

			$data = array();
			$data['pegawai'] = $this->dt_dasar_model->getPegawai($id)->row_array();
			$data['pendidikan'] = $this->dt_dasar_model->getPendidikan()->result_array();
			$data['kategori_lokasi'] = $this->dt_dasar_model->getKategoriLokasi()->result_array();
			$this->template->display('riwayat/dt_pendidikan/create', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function update_dt_pendidikan($id=0){
		if($this->access->permission('update')){
			if($post = $this->input->post()){
				$format="";//format file name
	            $strf=(string)strftime($format);
	            $config['file_name'] = '';
	            $config['upload_path']      = './assets/images/pendidikan';
	            $config['allowed_types']    = 'gif|jpg|png|svg|jpeg|pdf';
	            $config['max_size']         = '20000'; // KB    
	            $this->load->library('upload', $config);
	            $file_name = "";
	            if($this->upload->do_upload('E_DOCS')) {
	                $upload_data = array('uploads' =>$this->upload->data());
	                $file_name   =  $upload_data['uploads']['file_name'];
	            }else{
	            	$file_name = $post["E_DOC"];
	            }
				$dataupdate = array(
					'NIP' => isset($post['NIP'])?$post['NIP']:'',
					'TINGKAT' => isset($post['TINGKAT'])?$post['TINGKAT']:'',
					'NAMA_SEK_PT' => isset($post['NAMA_SEK_PT'])?$post['NAMA_SEK_PT']:'',
					'PRO_STUDI' => isset($post['PRO_STUDI'])?$post['PRO_STUDI']:'',
					'THN_MASUK' => isset($post['THN_MASUK'])?$post['THN_MASUK']:'',
					'THN_LULUS' => isset($post['THN_LULUS'])?$post['THN_LULUS']:'',
					'TMP_BLJ' => isset($post['TMP_BLJ'])?$post['TMP_BLJ']:'',
					'LOKASI' => isset($post['LOKASI'])?$post['LOKASI']:'',
					'NO_IJASAH' => isset($post['NO_IJASAH'])?$post['NO_IJASAH']:'',
					'NAMA_KEPSEK_REKTOR' => isset($post['NAMA_KEPSEK_REKTOR'])?$post['NAMA_KEPSEK_REKTOR']:'',
					'E_DOC' => $file_name,
					'STATUS' => 1
				);
				// echo '<pre>'; print_r($dataupdate); die;
				$this->riwayat_model->update_dt_pendidikan($dataupdate,$id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Perubahan Data Pendidikan Berhasil',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$post['ID']);
			}

			$data = array();
			$data['dt_pendidikan'] = $this->dt_dasar_model->getDtPendidikan($id)->row_array();
			$data['pegawai'] = $this->dt_dasar_model->getPegawaiByNip($data['dt_pendidikan']['NIP'])->row_array();
			$data['pendidikan'] = $this->dt_dasar_model->getPendidikan()->result_array();
			$data['kategori_lokasi'] = $this->dt_dasar_model->getKategoriLokasi()->result_array();
			// echo "<pre>"; print_r($data['dt_pendidikan']); die;
			$this->template->display('riwayat/dt_pendidikan/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function delete_dt_pendidikan($id = 0,$pegawai_id = 0){

		if($this->access->permission('delete')) {
			if($id) {

				$dataupdate = array(
					'STATUS'  => 0
				);

				$del = $this->riwayat_model->update_dt_pendidikan($dataupdate,$id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Pendidikan Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Pendidikan Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Pendidikan Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
		}
	}
	public function create_dt_pangkat($id=0){
		if($this->access->permission('update')){
			if($post = $this->input->post()){
				// echo "<pre>"; print_r($post); die;
				$format="";//format file name
	            $strf=(string)strftime($format);
	            $config['file_name'] = '';
	            $config['upload_path']      = './assets/doc/kepangkatan';
	            $config['allowed_types']    = 'gif|jpg|png|svg|jpeg|pdf';
	            $config['max_size']         = '20000'; // KB    
	            $this->load->library('upload', $config);
	            $file_name = "";
	            if($this->upload->do_upload('E_DOC')) {
	                $upload_data = array('uploads' =>$this->upload->data());
	                $file_name   =  $upload_data['uploads']['file_name'];
	            }else{
	            	$file_name = '';
	            }
				$datainsert = array(
					'NIP' => isset($post['NIP'])?$post['NIP']:'',
					'KATEGORI_SK' => isset($post['KATEGORI_SK'])?$post['KATEGORI_SK']:'',
					'GOLONGAN' => isset($post['GOLONGAN'])?$post['GOLONGAN']:'',
					'TMT_GOL' => isset($post['TMT_GOL_submit'])?$post['TMT_GOL_submit']:'',
					'PEJABAT' => isset($post['PEJABAT'])?$post['PEJABAT']:'',
					'NOMOR_SK' => isset($post['NOMOR_SK'])?$post['NOMOR_SK']:'',
					'TGL_SK' => isset($post['TGL_SK_submit'])?$post['TGL_SK_submit']:'',
					'E_DOC' => $file_name,
					'STATUS' => 1,
					'MASA_KERJA_THN' => isset($post['MASA_KERJA_THN'])?$post['MASA_KERJA_THN']:'',
					'MASA_KERJA_BLN' => isset($post['MASA_KERJA_BLN'])?$post['MASA_KERJA_BLN']:''
				);
				// echo '<pre>'; print_r($datainsert); die;
				$this->riwayat_model->insert_dt_pangkat($datainsert);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Penambahan Data Kepangkatan Berhasil',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$id);
			}

			$data = array();
			$data['pegawai'] = $this->dt_dasar_model->getPegawai($id)->row_array();
			$data['dt_kepangkatan'] = $this->dt_dasar_model->getDataKepangkatan($data['pegawai']['NIP'])->result_array();
			$data['dt_jabatan'] = $this->dt_dasar_model->getDataJabatan($data['pegawai']['NIP'])->result_array();
			$data['golongan'] = $this->dt_dasar_model->getGolongan()->result_array();
			$data['kategori_sk'] = $this->dt_dasar_model->getKategoriSk()->result_array();
			$data['jenis_peg'] = $this->dt_dasar_model->getJenisPeg()->result_array();
			$data['kategori_jab'] = $this->dt_dasar_model->getKategoriJab()->result_array();
			// echo "<pre>"; print_r($data['golongan']); die;
			$this->template->display('riwayat/dt_pangkat/create', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function update_dt_pangkat($id=0){
		if($this->access->permission('update')){
			if($post = $this->input->post()){
				// echo "<pre>"; print_r($post); die;
				$format="";//format file name
	            $strf=(string)strftime($format);
	            $config['file_name'] = '';
	            $config['upload_path']      = './assets/doc/kepangkatan';
	            $config['allowed_types']    = 'gif|jpg|png|svg|jpeg|pdf';
	            $config['max_size']         = '20000'; // KB    
	            $this->load->library('upload', $config);
	            $file_name = "";
	            if($this->upload->do_upload('E_DOCS')) {
	                $upload_data = array('uploads' =>$this->upload->data());
	                $file_name   =  $upload_data['uploads']['file_name'];
	            }else{
	            	$file_name = $post['E_DOC'];
	            }
				$dataupdate = array(
					'NIP' => isset($post['NIP'])?$post['NIP']:'',
					'KATEGORI_SK' => isset($post['KATEGORI_SK'])?$post['KATEGORI_SK']:'',
					'GOLONGAN' => isset($post['GOLONGAN'])?$post['GOLONGAN']:'',
					'TMT_GOL' => isset($post['TMT_GOL_submit'])?$post['TMT_GOL_submit']:'',
					'PEJABAT' => isset($post['PEJABAT'])?$post['PEJABAT']:'',
					'NOMOR_SK' => isset($post['NOMOR_SK'])?$post['NOMOR_SK']:'',
					'TGL_SK' => isset($post['TGL_SK_submit'])?$post['TGL_SK_submit']:'',
					'E_DOC' => $file_name,
					'STATUS' => 1,
					'MASA_KERJA_THN' => isset($post['MASA_KERJA_THN'])?$post['MASA_KERJA_THN']:'',
					'MASA_KERJA_BLN' => isset($post['MASA_KERJA_BLN'])?$post['MASA_KERJA_BLN']:''
				);
				// echo '<pre>'; print_r($dataupdate); die;
				$this->riwayat_model->update_dt_pangkat($dataupdate, $id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Perubahan Data Kepangkatan Berhasil',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$post['ID']);
			}

			$data = array();
			$data['dt_kepangkatan'] = $this->dt_dasar_model->getDtKepangkatan($id)->row_array();
			$data['pegawai'] = $this->dt_dasar_model->getPegawaiByNip($data['dt_kepangkatan']['NIP'])->row_array();
			// echo "<pre>"; print_r($data['pegawai']); die;
			$data['golongan'] = $this->dt_dasar_model->getGolongan()->result_array();
			$data['kategori_sk'] = $this->dt_dasar_model->getKategoriSk()->result_array();
			$data['jenis_peg'] = $this->dt_dasar_model->getJenisPeg()->result_array();
			$data['kategori_jab'] = $this->dt_dasar_model->getKategoriJab()->result_array();
			// echo "<pre>"; print_r($data['golongan']); die;
			$this->template->display('riwayat/dt_pangkat/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function delete_dt_pangkat($id = 0,$pegawai_id = 0){

		if($this->access->permission('delete')) {
			if($id) {

				$dataupdate = array(
					'STATUS'  => 0
				);

				$del = $this->riwayat_model->update_dt_pangkat($dataupdate,$id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Kepangkatan Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Kepangkatan Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Pendidikan Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
		}
	}
	public function create_dt_jabatan($id=0){
		if($this->access->permission('update')){
			if($post = $this->input->post()){
				$format="";//format file name
	            $strf=(string)strftime($format);
	            $config['file_name'] = '';
	            $config['upload_path']      = './assets/doc/pendidikan';
	            $config['allowed_types']    = 'gif|jpg|png|svg|jpeg|pdf';
	            $config['max_size']         = '20000'; // KB    
	            $this->load->library('upload', $config);
	            $file_name = "";
	            if($this->upload->do_upload('E_DOC')) {
	                $upload_data = array('uploads' =>$this->upload->data());
	                $file_name   =  $upload_data['uploads']['file_name'];
	            }else{
	            	$file_name = '';
	            }
				$datainsert = array(
					'NIP' => isset($post['NIP'])?$post['NIP']:'',
					'UNIT_KERJA' => isset($post['UNIT_KERJA'])?$post['UNIT_KERJA']:'',
					'KATEGORI_JAB' => isset($post['KATEGORI_JAB'])?$post['KATEGORI_JAB']:'',
					'ESELON' => isset($post['ESELON'])?$post['ESELON']:'',
					'NAMA_JAB' => isset($post['NAMA_JAB'])?$post['NAMA_JAB']:'',
					'TGL_MULAI' => isset($post['TGL_MULAI_submit'])?$post['TGL_MULAI_submit']:'',
					'TGL_SELESAI' => isset($post['TGL_MULAI_submit'])?$post['TGL_MULAI_submit']:'',
					'UNIT_KERJA_LAMA' => isset($post['UNIT_KERJA_LAMA'])?$post['UNIT_KERJA_LAMA']:'',
					'NOMOR_SK' => isset($post['NOMOR_SK'])?$post['NOMOR_SK']:'',
					'TANGGAL_SK' => isset($post['TANGGAL_SK_submit'])?$post['TANGGAL_SK_submit']:'',
					'E_DOC' => $file_name,
					'STATUS' => 1,
					'JENIS_PEGAWAI' => isset($post['JENIS_PEGAWAI'])?$post['JENIS_PEGAWAI']:'',
				);
				// echo '<pre>'; print_r($datainsert); die;
				$this->riwayat_model->insert_dt_jabatan($datainsert);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Penambahan Data Jabatan Berhasil',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$id);
			}

			$data = array();
			$data['pegawai'] = $this->dt_dasar_model->getPegawai($id)->row_array();
			$data['unit_kerja'] = $this->dt_dasar_model->getUnitKerja()->result_array();
			$data['kategori_jab'] = $this->dt_dasar_model->getKategoriJab()->result_array();
			$data['jenis_peg'] = $this->dt_dasar_model->getJenisPeg()->result_array();
			$data['eselon'] = $this->dt_dasar_model->getEselonS()->result_array();
			$data['jft'] = $this->dt_dasar_model->getDataJFT()->result_array();
			$data['jfu'] = $this->dt_dasar_model->getDataJFU()->result_array();
			$this->template->display('riwayat/dt_jabatan/create', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function update_dt_jabatan($id=0){
		if($this->access->permission('update')){
			if($post = $this->input->post()){
				$format="";//format file name
	            $strf=(string)strftime($format);
	            $config['file_name'] = '';
	            $config['upload_path']      = './assets/doc/pendidikan';
	            $config['allowed_types']    = 'gif|jpg|png|svg|jpeg|pdf';
	            $config['max_size']         = '20000'; // KB    
	            $this->load->library('upload', $config);
	            $file_name = "";
	            if($this->upload->do_upload('E_DOCS')) {
	                $upload_data = array('uploads' =>$this->upload->data());
	                $file_name   =  $upload_data['uploads']['file_name'];
	            }else{
	            	$file_name = $post['E_DOC'];
	            }
				$dataupdate = array(
					'NIP' => isset($post['NIP'])?$post['NIP']:'',
					'UNIT_KERJA' => isset($post['UNIT_KERJA'])?$post['UNIT_KERJA']:'',
					'KATEGORI_JAB' => isset($post['KATEGORI_JAB'])?$post['KATEGORI_JAB']:'',
					'ESELON' => isset($post['ESELON'])?$post['ESELON']:'',
					'NAMA_JAB' => isset($post['NAMA_JAB'])?$post['NAMA_JAB']:'',
					'TGL_MULAI' => isset($post['TGL_MULAI_submit'])?$post['TGL_MULAI_submit']:'',
					'TGL_SELESAI' => isset($post['TGL_MULAI_submit'])?$post['TGL_MULAI_submit']:'',
					'UNIT_KERJA_LAMA' => isset($post['UNIT_KERJA_LAMA'])?$post['UNIT_KERJA_LAMA']:'',
					'NOMOR_SK' => isset($post['NOMOR_SK'])?$post['NOMOR_SK']:'',
					'TANGGAL_SK' => isset($post['TANGGAL_SK_submit'])?$post['TANGGAL_SK_submit']:'',
					'E_DOC' => $file_name,
					'STATUS' => 1,
					'JENIS_PEGAWAI' => isset($post['JENIS_PEGAWAI'])?$post['JENIS_PEGAWAI']:'',
				);
				// echo '<pre>'; print_r($dataupdate); die;
				$this->riwayat_model->update_dt_jabatan($dataupdate, $id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Perubahan Data Jabatan Berhasil',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$post['ID']);
			}

			$data = array();
			$data['dt_jabatan'] = $this->dt_dasar_model->getDtJabatan($id)->row_array();
			$data['pegawai'] = $this->dt_dasar_model->getPegawaiByNip($data['dt_jabatan']['NIP'])->row_array();
			$data['unit_kerja'] = $this->dt_dasar_model->getUnitKerja()->result_array();
			$data['kategori_jab'] = $this->dt_dasar_model->getKategoriJab()->result_array();
			$data['jenis_peg'] = $this->dt_dasar_model->getJenisPeg()->result_array();
			$data['eselon'] = $this->dt_dasar_model->getEselon()->result_array();
			// echo "<pre>"; print_r($data['dt_jabatan']); die;
			$this->template->display('riwayat/dt_jabatan/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function delete_dt_jabatan($id = 0,$pegawai_id = 0){

		if($this->access->permission('delete')) {
			if($id) {

				$dataupdate = array(
					'STATUS'  => 0
				);

				$del = $this->riwayat_model->update_dt_jabatan($dataupdate,$id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Jabatan Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Kepangkatan Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Pendidikan Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
		}
	}
	public function create_dt_pelatihan($id=0){
		if($this->access->permission('update')){
			if($post = $this->input->post()){
				$format="";//format file name
	            $strf=(string)strftime($format);
	            $config['file_name'] = '';
	            $config['upload_path']      = './assets/doc/pelatihan';
	            $config['allowed_types']    = 'gif|jpg|png|svg|jpeg|pdf';
	            $config['max_size']         = '20000'; // KB    
	            $this->load->library('upload', $config);
	            $file_name = "";
	            if($this->upload->do_upload('E_DOC')) {
	                $upload_data = array('uploads' =>$this->upload->data());
	                $file_name   =  $upload_data['uploads']['file_name'];
	            }else{
	            	$file_name = '';
	            }
				$datainsert = array(
					'NIP' => isset($post['NIP'])?$post['NIP']:'',
					'KATEGORI_DIKLAT' => isset($post['KATEGORI_DIKLAT'])?$post['KATEGORI_DIKLAT']:'',
					'NAMA_DIKLAT' => isset($post['NAMA_DIKLAT'])?$post['NAMA_DIKLAT']:'',
					'ANGKATAN' => isset($post['ANGKATAN'])?$post['ANGKATAN']:'',
					'TGL_MULAI' => isset($post['TGL_MULAI_submit'])?$post['TGL_MULAI_submit']:'',
					'TGL_SELESAI' => isset($post['TGL_SELESAI_submit'])?$post['TGL_SELESAI_submit']:'',
					'PENYELENGGARA' => isset($post['PENYELENGGARA'])?$post['PENYELENGGARA']:'',
					'PREDIKAT' => isset($post['PREDIKAT'])?$post['PREDIKAT']:'',
					'LOKASI' => isset($post['LOKASI'])?$post['LOKASI']:'',
					'JUMLAH_JAM' => isset($post['JUMLAH_JAM'])?$post['JUMLAH_JAM']:'',
					'NO_SERTIFIKAT' => isset($post['NO_SERTIFIKAT'])?$post['NO_SERTIFIKAT']:'',
					'TGL_SERTIFIKAT' => isset($post['TGL_SERTIFIKAT_submit'])?$post['TGL_SERTIFIKAT_submit']:'',
					'E_DOC' => $file_name,
					'STATUS' => 1
				);
				// echo '<pre>'; print_r($datainsert); die;
				$this->riwayat_model->insert_dt_pelatihan($datainsert);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Penambahan Data Pelatihan Berhasil',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$id);
			}

			$data = array();
			$data['pegawai'] = $this->dt_dasar_model->getPegawai($id)->row_array();
			$data['kategori_pel'] = $this->dt_dasar_model->getKategoriPel()->result_array();
			$data['kategori_predikat'] = $this->dt_dasar_model->getKategoriPredikat()->result_array();
			$data['kategori_lokasi'] = $this->dt_dasar_model->getKategoriLokasi()->result_array();
			$this->template->display('riwayat/dt_pelatihan/create', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function update_dt_pelatihan($id=0){
		if($this->access->permission('update')){
			if($post = $this->input->post()){
				$format="";//format file name
	            $strf=(string)strftime($format);
	            $config['file_name'] = '';
	            $config['upload_path']      = './assets/doc/pelatihan';
	            $config['allowed_types']    = 'gif|jpg|png|svg|jpeg|pdf';
	            $config['max_size']         = '20000'; // KB    
	            $this->load->library('upload', $config);
	            $file_name = "";
	            if($this->upload->do_upload('E_DOCS')) {
	                $upload_data = array('uploads' =>$this->upload->data());
	                $file_name   =  $upload_data['uploads']['file_name'];
	            }else{
	            	$file_name = $post['E_DOC'];
	            }
				$dataupdate = array(
					'NIP' => isset($post['NIP'])?$post['NIP']:'',
					'KATEGORI_DIKLAT' => isset($post['KATEGORI_DIKLAT'])?$post['KATEGORI_DIKLAT']:'',
					'NAMA_DIKLAT' => isset($post['NAMA_DIKLAT'])?$post['NAMA_DIKLAT']:'',
					'ANGKATAN' => isset($post['ANGKATAN'])?$post['ANGKATAN']:'',
					'TGL_MULAI' => isset($post['TGL_MULAI_submit'])?$post['TGL_MULAI_submit']:'',
					'TGL_SELESAI' => isset($post['TGL_SELESAI_submit'])?$post['TGL_SELESAI_submit']:'',
					'PENYELENGGARA' => isset($post['PENYELENGGARA'])?$post['PENYELENGGARA']:'',
					'PREDIKAT' => isset($post['PREDIKAT'])?$post['PREDIKAT']:'',
					'LOKASI' => isset($post['LOKASI'])?$post['LOKASI']:'',
					'JUMLAH_JAM' => isset($post['JUMLAH_JAM'])?$post['JUMLAH_JAM']:'',
					'NO_SERTIFIKAT' => isset($post['NO_SERTIFIKAT'])?$post['NO_SERTIFIKAT']:'',
					'TGL_SERTIFIKAT' => isset($post['TGL_SERTIFIKAT_submit'])?$post['TGL_SERTIFIKAT_submit']:'',
					'E_DOC' => $file_name,
					'STATUS' => 1
				);
				// echo '<pre>'; print_r($dataupdate); die;
				$this->riwayat_model->update_dt_pelatihan($dataupdate, $id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Perubahan Data Pelatihan Berhasil',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$post['ID']);
			}

			$data = array();
			$data['dt_pelatihan'] = $this->dt_dasar_model->getDtPelatihan($id)->row_array();
			$data['pegawai'] = $this->dt_dasar_model->getPegawaiByNip($data['dt_pelatihan']['NIP'])->row_array();
			$data['kategori_pel'] = $this->dt_dasar_model->getKategoriPel()->result_array();
			$data['kategori_predikat'] = $this->dt_dasar_model->getKategoriPredikat()->result_array();
			$data['kategori_lokasi'] = $this->dt_dasar_model->getKategoriLokasi()->result_array();
			// echo "<pre>"; print_r($data['dt_jabatan']); die;
			$this->template->display('riwayat/dt_pelatihan/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function delete_dt_pelatihan($id = 0,$pegawai_id = 0){

		if($this->access->permission('delete')) {
			if($id) {

				$dataupdate = array(
					'STATUS'  => 0
				);

				$del = $this->riwayat_model->update_dt_pelatihan($dataupdate,$id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Pelatihan Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Pelatihan Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Pelatihan Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
		}
	}
	public function create_dt_skp($id=0){
		if($this->access->permission('update')){
			if($post = $this->input->post()){
				// echo "<pre>"; print_r($post); die;
				$format="";//format file name
	            $strf=(string)strftime($format);
	            $config['file_name'] = '';
	            $config['upload_path']      = './assets/doc/skp';
	            $config['allowed_types']    = 'gif|jpg|png|svg|jpeg|pdf';
	            $config['max_size']         = '20000'; // KB    
	            $this->load->library('upload', $config);
	            $file_name = "";
	            if($this->upload->do_upload('E_DOC')) {
	                $upload_data = array('uploads' =>$this->upload->data());
	                $file_name   =  $upload_data['uploads']['file_name'];
	            }else{
	            	$file_name = '';
	            }
				$datainsert = array(
					'NIP' => isset($post['NIP'])?$post['NIP']:'',
					'TH_NILAI' => isset($post['TH_NILAI'])?$post['TH_NILAI']:'',
					'SKP' => isset($post['SKP'])?$post['SKP']:'',
					'ORIENTASI_PELAYANAN' => isset($post['ORIENTASI_PELAYANAN'])?$post['ORIENTASI_PELAYANAN']:'',
					'INTEGRITAS' => isset($post['INTEGRITAS'])?$post['INTEGRITAS']:'',
					'KOMITMEN' => isset($post['KOMITMEN'])?$post['KOMITMEN']:'',
					'DISIPLIN' => isset($post['DISIPLIN'])?$post['DISIPLIN']:'',
					'KERJASAMA' => isset($post['KERJASAMA'])?$post['KERJASAMA']:'',
					'KEPEMIMPINAN' => isset($post['KEPEMIMPINAN'])?$post['KEPEMIMPINAN']:'',
					'E_DOC' => $file_name,
					'STATUS' => 1
				);
				// echo '<pre>'; print_r($datainsert); die;
				$this->riwayat_model->insert_dt_skp($datainsert);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Penambahan Data SKP Berhasil',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$id);
			}

			$data = array();
			$data['pegawai'] = $this->dt_dasar_model->getPegawai($id)->row_array();
			$data['unit_kerja'] = $this->dt_dasar_model->getUnitKerja()->result_array();
			// echo "<pre>"; print_r($data['pegawai']); die;
			$this->template->display('riwayat/dt_skp/create', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function update_dt_skp($id=0){
		if($this->access->permission('update')){
			if($post = $this->input->post()){
				$format="";//format file name
	            $strf=(string)strftime($format);
	            $config['file_name'] = '';
	            $config['upload_path']      = './assets/doc/skp';
	            $config['allowed_types']    = 'gif|jpg|png|svg|jpeg|pdf';
	            $config['max_size']         = '20000'; // KB    
	            $this->load->library('upload', $config);
	            $file_name = "";
	            if($this->upload->do_upload('E_DOCS')) {
	                $upload_data = array('uploads' =>$this->upload->data());
	                $file_name   =  $upload_data['uploads']['file_name'];
	            }else{
	            	$file_name = $post['E_DOC'];
	            }
				$dataupdate = array(
					'NIP' => isset($post['NIP'])?$post['NIP']:'',
					'TH_NILAI' => isset($post['TH_NILAI'])?$post['TH_NILAI']:'',
					'SKP' => isset($post['SKP'])?$post['SKP']:'',
					'ORIENTASI_PELAYANAN' => isset($post['ORIENTASI_PELAYANAN'])?$post['ORIENTASI_PELAYANAN']:'',
					'INTEGRITAS' => isset($post['INTEGRITAS'])?$post['INTEGRITAS']:'',
					'KOMITMEN' => isset($post['KOMITMEN'])?$post['KOMITMEN']:'',
					'DISIPLIN' => isset($post['DISIPLIN'])?$post['DISIPLIN']:'',
					'KERJASAMA' => isset($post['KERJASAMA'])?$post['KERJASAMA']:'',
					'KEPEMIMPINAN' => isset($post['KEPEMIMPINAN'])?$post['KEPEMIMPINAN']:'',
					'E_DOC' => $file_name,
					'STATUS' => 1
				);
				// echo '<pre>'; print_r($dataupdate); die;
				$this->riwayat_model->update_dt_skp($dataupdate, $id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Perubahan Data SKP Berhasil',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$post['ID']);
			}

			$data = array();
			$data['dt_skp'] = $this->dt_dasar_model->getDtSKP($id)->row_array();
			$data['pegawai'] = $this->dt_dasar_model->getPegawaiByNip($data['dt_skp']['NIP'])->row_array();
			// echo "<pre>"; print_r($data['dt_jabatan']); die;
			$this->template->display('riwayat/dt_skp/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function delete_dt_skp($id = 0,$pegawai_id = 0){

		if($this->access->permission('delete')) {
			if($id) {

				$dataupdate = array(
					'STATUS'  => 0
				);

				$del = $this->riwayat_model->update_dt_skp($dataupdate,$id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'SKP Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'SKP Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Pelatihan Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
		}
	}
	public function create_dt_penghargaan($id=0){
		if($this->access->permission('update')){
			if($post = $this->input->post()){
				$format="";//format file name
	            $strf=(string)strftime($format);
	            $config['file_name'] = '';
	            $config['upload_path']      = './assets/doc/penghargaan';
	            $config['allowed_types']    = 'gif|jpg|png|svg|jpeg|pdf';
	            $config['max_size']         = '20000'; // KB    
	            $this->load->library('upload', $config);
	            $file_name = "";
	            if($this->upload->do_upload('E_DOC')) {
	                $upload_data = array('uploads' =>$this->upload->data());
	                $file_name   =  $upload_data['uploads']['file_name'];
	            }else{
	            	$file_name = '';
	            }
				$datainsert = array(
					'NIP' => isset($post['NIP'])?$post['NIP']:'',
					'NAMA_TANDA' => isset($post['NAMA_TANDA'])?$post['NAMA_TANDA']:'',
					'JENIS_PENGHARGAAN' => isset($post['JENIS_PENGHARGAAN'])?$post['JENIS_PENGHARGAAN']:'',
					'NOMOR_SK' => isset($post['NOMOR_SK'])?$post['NOMOR_SK']:'',
					'TGL_OLEH' => isset($post['TGL_OLEH_submit'])?$post['TGL_OLEH_submit']:'',
					'THN_OLEH' => isset($post['THN_OLEH'])?$post['THN_OLEH']:'',
					'PEMBERI' => isset($post['PEMBERI'])?$post['PEMBERI']:'',
					'JABATAN' => isset($post['JABATAN'])?$post['JABATAN']:'',
					'E_DOC' => $file_name,
					'STATUS' => 1
				);
				// echo '<pre>'; print_r($datainsert); die;
				$this->riwayat_model->insert_dt_penghargaan($datainsert);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Penambahan Data Penghargaan Berhasil',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$id);
			}

			$data = array();
			$data['pegawai'] = $this->dt_dasar_model->getPegawai($id)->row_array();
			$data['kategori_slks'] = $this->dt_dasar_model->getKategoriSlks()->result_array();
			// echo "<pre>"; print_r($data['kategori_slks']); die;
			$this->template->display('riwayat/dt_penghargaan/create', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function update_dt_penghargaan($id=0){
		if($this->access->permission('update')){
			if($post = $this->input->post()){
				$format="";//format file name
	            $strf=(string)strftime($format);
	            $config['file_name'] = '';
	            $config['upload_path']      = './assets/doc/penghargaan';
	            $config['allowed_types']    = 'gif|jpg|png|svg|jpeg|pdf';
	            $config['max_size']         = '20000'; // KB    
	            $this->load->library('upload', $config);
	            $file_name = "";
	            if($this->upload->do_upload('E_DOCS')) {
	                $upload_data = array('uploads' =>$this->upload->data());
	                $file_name   =  $upload_data['uploads']['file_name'];
	            }else{
	            	$file_name = $post['E_DOC'];
	            }
				$dataupdate = array(
					'NIP' => isset($post['NIP'])?$post['NIP']:'',
					'NAMA_TANDA' => isset($post['NAMA_TANDA'])?$post['NAMA_TANDA']:'',
					'JENIS_PENGHARGAAN' => isset($post['JENIS_PENGHARGAAN'])?$post['JENIS_PENGHARGAAN']:'',
					'NOMOR_SK' => isset($post['NOMOR_SK'])?$post['NOMOR_SK']:'',
					'TGL_OLEH' => isset($post['TGL_OLEH_submit'])?$post['TGL_OLEH_submit']:'',
					'THN_OLEH' => isset($post['THN_OLEH'])?$post['THN_OLEH']:'',
					'PEMBERI' => isset($post['PEMBERI'])?$post['PEMBERI']:'',
					'JABATAN' => isset($post['JABATAN'])?$post['JABATAN']:'',
					'E_DOC' => $file_name,
					'STATUS' => 1
				);
				// echo '<pre>'; print_r($dataupdate); die;
				$this->riwayat_model->update_dt_penghargaan($dataupdate, $id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Perubahan Data Penghargaan Berhasil',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$post['ID']);
			}

			$data = array();
			$data['dt_penghargaan'] = $this->dt_dasar_model->getDtPenghargaan($id)->row_array();
			$data['pegawai'] = $this->dt_dasar_model->getPegawaiByNip($data['dt_penghargaan']['NIP'])->row_array();
			$data['kategori_slks'] = $this->dt_dasar_model->getKategoriSlks()->result_array();
			// echo "<pre>"; print_r($data['dt_penghargaan']); die;
			$this->template->display('riwayat/dt_penghargaan/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function delete_dt_penghargaan($id = 0,$pegawai_id = 0){

		if($this->access->permission('delete')) {
			if($id) {

				$dataupdate = array(
					'STATUS'  => 0
				);

				$del = $this->riwayat_model->update_dt_penghargaan($dataupdate,$id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Penghargaan Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Penghargaan Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Penghargaan Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
		}
	}
	public function create_dt_kgb($id=0){
		if($this->access->permission('update')){
			if($post = $this->input->post()){
				$format="";//format file name
	            $strf=(string)strftime($format);
	            $config['file_name'] = '';
	            $config['upload_path']      = './assets/doc/kgb';
	            $config['allowed_types']    = 'gif|jpg|png|svg|jpeg|pdf';
	            $config['max_size']         = '20000'; // KB    
	            $this->load->library('upload', $config);
	            $file_name = "";
	            if($this->upload->do_upload('E_DOC')) {
	                $upload_data = array('uploads' =>$this->upload->data());
	                $file_name   =  $upload_data['uploads']['file_name'];
	            }else{
	            	$file_name = '';
	            }
				$datainsert = array(
					'NIP' => isset($post['NIP'])?$post['NIP']:'',
					'TANGGAL_KGB' => isset($post['TANGGAL_KGB_submit'])?$post['TANGGAL_KGB_submit']:'',
					'KGB_SEBELUM' => isset($post['KGB_SEBELUM'])?$post['KGB_SEBELUM']:'',
					'KGB_SESUDAH' => isset($post['KGB_SESUDAH'])?$post['KGB_SESUDAH']:'',
					'E_DOC' => $file_name,
					'STATUS' => 1
				);
				// echo '<pre>'; print_r($datainsert); die;
				$this->riwayat_model->insert_dt_kgb($datainsert);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Penambahan Data KGB Berhasil',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$id);
			}

			$data = array();
			$data['pegawai'] = $this->dt_dasar_model->getPegawai($id)->row_array();
			// echo "<pre>"; print_r($data['kategori_slks']); die;
			$this->template->display('riwayat/dt_kgb/create', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function update_dt_kgb($id=0){
		if($this->access->permission('update')){
			if($post = $this->input->post()){
				$format="";//format file name
	            $strf=(string)strftime($format);
	            $config['file_name'] = '';
	            $config['upload_path']      = './assets/doc/penghargaan';
	            $config['allowed_types']    = 'gif|jpg|png|svg|jpeg|pdf';
	            $config['max_size']         = '20000'; // KB    
	            $this->load->library('upload', $config);
	            $file_name = "";
	            if($this->upload->do_upload('E_DOCS')) {
	                $upload_data = array('uploads' =>$this->upload->data());
	                $file_name   =  $upload_data['uploads']['file_name'];
	            }else{
	            	$file_name = $post['E_DOC'];
	            }
				$dataupdate = array(
					'NIP' => isset($post['NIP'])?$post['NIP']:'',
					'TANGGAL_KGB' => isset($post['TANGGAL_KGB_submit'])?$post['TANGGAL_KGB_submit']:'',
					'KGB_SEBELUM' => isset($post['KGB_SEBELUM'])?$post['KGB_SEBELUM']:'',
					'KGB_SESUDAH' => isset($post['KGB_SESUDAH'])?$post['KGB_SESUDAH']:'',
					'E_DOC' => $file_name,
					'STATUS' => 1
				);
				// echo '<pre>'; print_r($dataupdate); die;
				$this->riwayat_model->update_dt_kgb($dataupdate, $id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Perubahan Data KGB Berhasil',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$post['ID']);
			}

			$data = array();
			$data['dt_kgb'] = $this->dt_dasar_model->getDtKgb($id)->row_array();
			$data['pegawai'] = $this->dt_dasar_model->getPegawaiByNip($data['dt_kgb']['NIP'])->row_array();
			// echo "<pre>"; print_r($data['dt_kgb']); die;
			$this->template->display('riwayat/dt_kgb/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function delete_dt_kgb($id = 0,$pegawai_id = 0){

		if($this->access->permission('delete')) {
			if($id) {

				$dataupdate = array(
					'STATUS'  => 0
				);

				$del = $this->riwayat_model->update_dt_kgb($dataupdate,$id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'KGB Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'KGB Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'KGB Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
		}
	}
	public function create_dt_cuti($id=0){
		if($this->access->permission('update')){
			if($post = $this->input->post()){
				$datainsert = array(
					'iddatadasar' => isset($post['ID'])?$post['ID']:'',
					'jns_cuti' => isset($post['jns_cuti'])?$post['jns_cuti']:'',
					'no_surat' => isset($post['no_surat'])?$post['no_surat']:'',
					'tgl_surat' => isset($post['tgl_surat_submit'])?$post['tgl_surat_submit']:'',
					'kota_surat' => isset($post['kota_surat'])?$post['kota_surat']:'',
					'tgl_start' => isset($post['tgl_start_submit'])?$post['tgl_start_submit']:'',
					'tgl_end' => isset($post['tgl_end_submit'])?$post['tgl_end_submit']:'',
					'jml_hari' => isset($post['jml_hari'])?$post['jml_hari']:'',
					'satuan_jml' => isset($post['satuan_jml'])?$post['satuan_jml']:'',
					'iddatadasar_kepala' => isset($post['iddatadasar_kepala'])?$post['iddatadasar_kepala']:'',
					'nipp_kepala' => isset($post['nipp_kepala'])?$post['nipp_kepala']:'',
					'nama_kepala' => isset($post['nama_kepala'])?$post['nama_kepala']:'',
					'nama_unker_kepala' => isset($post['nama_unker_kepala'])?$post['nama_unker_kepala']:'',
					'created_by' => $this->session->userdata('user_id'),
					'aktif' => 1
				);
				$this->riwayat_model->insert_dt_cuti($datainsert);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Penambahan Data Cuti Berhasil',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$id);
			}

			$data = array();
			$data['pegawai'] = $this->dt_dasar_model->getPegawai($id)->row_array();
			$data['pejabat'] = $this->dt_dasar_model->getPegawaiAtasan($id)->result_array();
			$data['jenis_cuti'] = $this->dt_dasar_model->getJenisCuti()->result_array();
			$this->template->display('riwayat/dt_cuti/create', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function update_dt_cuti($id=0){
		if($this->access->permission('update')){
			if($post = $this->input->post()){
				$format="";//format file name
				$dataupdate = array(
					'iddatadasar' => isset($post['ID'])?$post['ID']:'',
					'jns_cuti' => isset($post['jns_cuti'])?$post['jns_cuti']:'',
					'no_surat' => isset($post['no_surat'])?$post['no_surat']:'',
					'tgl_surat' => isset($post['tgl_surat_submit'])?$post['tgl_surat_submit']:'',
					'kota_surat' => isset($post['kota_surat'])?$post['kota_surat']:'',
					'tgl_start' => isset($post['tgl_start_submit'])?$post['tgl_start_submit']:'',
					'tgl_end' => isset($post['tgl_end_submit'])?$post['tgl_end_submit']:'',
					'jml_hari' => isset($post['jml_hari'])?$post['jml_hari']:'',
					'satuan_jml' => isset($post['satuan_jml'])?$post['satuan_jml']:'',
					'iddatadasar_kepala' => isset($post['iddatadasar_kepala'])?$post['iddatadasar_kepala']:'',
					'nipp_kepala' => isset($post['nipp_kepala'])?$post['nipp_kepala']:'',
					'nama_kepala' => isset($post['nama_kepala'])?$post['nama_kepala']:'',
					'nama_unker_kepala' => isset($post['nama_unker_kepala'])?$post['nama_unker_kepala']:'',
					'changed_by' => $this->session->userdata('user_id'),
					'aktif' => 1
				);
				$this->riwayat_model->update_dt_cuti($dataupdate, $id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Perubahan Data Cuti Berhasil',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$post['ID']);
			}

			$data = array();
			$data['dt_cuti'] = $this->dt_dasar_model->getDtCuti($id)->row_array();
			$data['pegawai'] = $this->dt_dasar_model->getPegawai($data['dt_cuti']['iddatadasar'])->row_array();
			$data['pejabat'] = $this->dt_dasar_model->getPegawaiAtasan($id)->result_array();
			$data['jenis_cuti'] = $this->dt_dasar_model->getJenisCuti()->result_array();
			// echo "<pre>"; print_r($data['dt_cuti']); die;
			$this->template->display('riwayat/dt_cuti/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function delete_dt_cuti($id = 0,$pegawai_id = 0){

		if($this->access->permission('delete')) {
			if($id) {

				$dataupdate = array(
					'aktif'  => 0
				);

				$del = $this->riwayat_model->update_dt_cuti($dataupdate,$id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Cuti Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Cuti Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Cuti Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
		}
	}
	public function create_dt_keluarga($id=0){
		if($this->access->permission('update')){
			if($post = $this->input->post()){
				// echo "<pre>"; print_r($post); die;
				$format="";//format file name
	            $strf=(string)strftime($format);
	            $config['file_name'] = '';
	            $config['upload_path']      = './assets/doc/kgb';
	            $config['allowed_types']    = 'gif|jpg|png|svg|jpeg|pdf';
	            $config['max_size']         = '20000'; // KB    
	            $this->load->library('upload', $config);
	            $file_name = "";
	            if($this->upload->do_upload('E_DOC')) {
	                $upload_data = array('uploads' =>$this->upload->data());
	                $file_name   =  $upload_data['uploads']['file_name'];
	            }else{
	            	$file_name = '';
	            }
	            $cerai_wafat = isset($post['TGL_CERAIWAFAT'])?$post['TGL_CERAIWAFAT']:'';
	            if($cerai_wafat == ''){
	                $tgl_cerai_wafat = '0000-00-00';
	            }else{
	                $tgl_cerai_wafat = $cerai_wafat;
	            }
	            $nikah = isset($post['TGL_NIKAH_submit'])?$post['TGL_NIKAH_submit']:'';
	            if($nikah == ''){
	                $tgl_nikah = '0000-00-00';
	            }else{
	                $tgl_nikah = $nikah;
	            }
				$datainsert = array(
					'NIP' => isset($post['NIP'])?$post['NIP']:'',
					'HUBUNGAN' => isset($post['HUBUNGAN'])?$post['HUBUNGAN']:'',
					'NAMA_KEL' => isset($post['NAMA_KEL'])?$post['NAMA_KEL']:'',
					'JENIS_KEL' => isset($post['JENIS_KEL'])?$post['JENIS_KEL']:'',
					'TMP_LAHIR' => isset($post['TMP_LAHIR'])?$post['TMP_LAHIR']:'',
					'TGL_LAHIR' => isset($post['TGL_LAHIR_submit'])?$post['TGL_LAHIR_submit']:'',
					'STATUS_ANAK' => isset($post['STATUS_ANAK'])?$post['STATUS_ANAK']:'',
					'MASUK_KP4' => isset($post['MASUK_KP4'])?$post['MASUK_KP4']:'',
					'TGL_NIKAH' => $tgl_nikah,
					'PEKERJAAN' => isset($post['PEKERJAAN'])?$post['PEKERJAAN']:'',
					'STATUS_ISTRI_SUAMI' => isset($post['STATUS_ISTRI_SUAMI'])?$post['STATUS_ISTRI_SUAMI']:'',
					'TGL_CERAIWAFAT' => $tgl_cerai_wafat,
					'SK_CERAIWAFAT' => isset($post['SK_CERAIWAFAT'])?$post['SK_CERAIWAFAT']:'',
					'KONDISI' => isset($post['KONDISI'])?$post['KONDISI']:'',
					'STATUS' => 1,
					'FOTO_KELUARGA' => '',
				);
				// echo '<pre>'; print_r($datainsert); die;
				$this->riwayat_model->insert_dt_keluarga($datainsert);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Penambahan Data Keluarga Berhasil',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$id);
			}

			$data = array();
			$data['pegawai'] = $this->dt_dasar_model->getPegawai($id)->row_array();
			$data['hubungan_keluarga'] = $this->dt_dasar_model->getHubunganKeluarga()->result_array();
			$data['jenis_kelamin'] = $this->dt_dasar_model->getJenisKelamin()->result_array();
			// echo "<pre>"; print_r($data['hubungan_keluarga']); die;
			$this->template->display('riwayat/dt_keluarga/create', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function update_dt_keluarga($id=0){
		if($this->access->permission('update')){
			if($post = $this->input->post()){
				$format="";//format file name
	            // $strf=(string)strftime($format);
	            // $config['file_name'] = '';
	            // $config['upload_path']      = './assets/doc/penghargaan';
	            // $config['allowed_types']    = 'gif|jpg|png|svg|jpeg|pdf';
	            // $config['max_size']         = '20000'; // KB    
	            // $this->load->library('upload', $config);
	            // $file_name = "";
	            // if($this->upload->do_upload('E_DOCS')) {
	            //     $upload_data = array('uploads' =>$this->upload->data());
	            //     $file_name   =  $upload_data['uploads']['file_name'];
	            // }else{
	            // 	$file_name = $post['E_DOC'];
	            // }
	            $cerai_wafat = isset($post['TGL_CERAIWAFAT'])?$post['TGL_CERAIWAFAT']:'';
	            if($cerai_wafat == ''){
	                $tgl_cerai_wafat = '0000-00-00';
	            }else{
	                $tgl_cerai_wafat = $cerai_wafat;
	            }
	            $nikah = isset($post['TGL_NIKAH_submit'])?$post['TGL_NIKAH_submit']:'';
	            if($nikah == ''){
	                $tgl_nikah = '0000-00-00';
	            }else{
	                $tgl_nikah = $nikah;
	            }
				$dataupdate = array(
					'NIP' => isset($post['NIP'])?$post['NIP']:'',
					'HUBUNGAN' => isset($post['HUBUNGAN'])?$post['HUBUNGAN']:'',
					'NAMA_KEL' => isset($post['NAMA_KEL'])?$post['NAMA_KEL']:'',
					'JENIS_KEL' => isset($post['JENIS_KEL'])?$post['JENIS_KEL']:'',
					'TMP_LAHIR' => isset($post['TMP_LAHIR'])?$post['TMP_LAHIR']:'',
					'TGL_LAHIR' => isset($post['TGL_LAHIR_submit'])?$post['TGL_LAHIR_submit']:'',
					'STATUS_ANAK' => isset($post['STATUS_ANAK'])?$post['STATUS_ANAK']:'',
					'MASUK_KP4' => isset($post['MASUK_KP4'])?$post['MASUK_KP4']:'',
					'TGL_NIKAH' => $tgl_nikah,
					'PEKERJAAN' => isset($post['PEKERJAAN'])?$post['PEKERJAAN']:'',
					'STATUS_ISTRI_SUAMI' => isset($post['STATUS_ISTRI_SUAMI'])?$post['STATUS_ISTRI_SUAMI']:'',
					'TGL_CERAIWAFAT' => $tgl_cerai_wafat,
					'SK_CERAIWAFAT' => isset($post['SK_CERAIWAFAT'])?$post['SK_CERAIWAFAT']:'',
					'KONDISI' => isset($post['KONDISI'])?$post['KONDISI']:'',
					'STATUS' => 1,
					'FOTO_KELUARGA' => '',
				);
				// echo '<pre>'; print_r($dataupdate); die;
				$this->riwayat_model->update_dt_keluarga($dataupdate, $id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Perubahan Data Keluarga Berhasil',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$post['ID']);
			}

			$data = array();
			$data['dt_keluarga'] = $this->dt_dasar_model->getDtKeluarga($id)->row_array();
			$data['pegawai'] = $this->dt_dasar_model->getPegawaiByNip($data['dt_keluarga']['NIP'])->row_array();
			$data['hubungan_keluarga'] = $this->dt_dasar_model->getHubunganKeluarga()->result_array();
			$data['jenis_kelamin'] = $this->dt_dasar_model->getJenisKelamin()->result_array();
			// echo "<pre>"; print_r($data['dt_keluarga']); die;
			$this->template->display('riwayat/dt_keluarga/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function delete_dt_keluarga($id = 0,$pegawai_id = 0){

		if($this->access->permission('delete')) {
			if($id) {

				$dataupdate = array(
					'STATUS'  => 0
				);

				$del = $this->riwayat_model->update_dt_keluarga($dataupdate,$id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Keluarga Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Keluarga Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Keluarga Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
		}
	}
	public function create_dt_assessment($id=0){
		if($this->access->permission('update')){
			if($post = $this->input->post()){
				// echo "<pre>"; print_r($post); die;
				$format="";//format file name
	            $strf=(string)strftime($format);
	            $config['file_name'] = '';
	            $config['upload_path']      = './assets/doc/kgb';
	            $config['allowed_types']    = 'gif|jpg|png|svg|jpeg|pdf';
	            $config['max_size']         = '20000'; // KB    
	            $this->load->library('upload', $config);
	            $file_name = "";
	            if($this->upload->do_upload('E_DOC')) {
	                $upload_data = array('uploads' =>$this->upload->data());
	                $file_name   =  $upload_data['uploads']['file_name'];
	            }else{
	            	$file_name = '';
	            }
				$datainsert = array(
					'NIP' => isset($post['NIP'])?$post['NIP']:'',
					'TANGGAL_ASSESSMENT' => isset($post['TANGGAL_ASSESSMENT_submit'])?$post['TANGGAL_ASSESSMENT_submit']:'',
					'NILAI_JOB_FIT' => isset($post['NILAI_JOB_FIT'])?$post['NILAI_JOB_FIT']:'',
					'REKOMENDASI' => isset($post['REKOMENDASI'])?$post['REKOMENDASI']:'',
					'STATUS' => 1
				);
				// echo '<pre>'; print_r($datainsert); die;
				$this->riwayat_model->insert_dt_assessment($datainsert);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Penambahan Data Assessment Berhasil',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$id);
			}

			$data = array();
			$data['pegawai'] = $this->dt_dasar_model->getPegawai($id)->row_array();
			$data['rekomendasi'] = $this->dt_dasar_model->getRekomendasi()->result_array();
			// echo "<pre>"; print_r($data['kategori_slks']); die;
			$this->template->display('riwayat/dt_assessment/create', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function update_dt_assessment($id=0){
		if($this->access->permission('update')){
			if($post = $this->input->post()){
				$format="";//format file name
	            // $strf=(string)strftime($format);
	            // $config['file_name'] = '';
	            // $config['upload_path']      = './assets/doc/penghargaan';
	            // $config['allowed_types']    = 'gif|jpg|png|svg|jpeg|pdf';
	            // $config['max_size']         = '20000'; // KB    
	            // $this->load->library('upload', $config);
	            // $file_name = "";
	            // if($this->upload->do_upload('E_DOCS')) {
	            //     $upload_data = array('uploads' =>$this->upload->data());
	            //     $file_name   =  $upload_data['uploads']['file_name'];
	            // }else{
	            // 	$file_name = $post['E_DOC'];
	            // }
				$dataupdate = array(
					'NIP' => isset($post['NIP'])?$post['NIP']:'',
					'TANGGAL_ASSESSMENT' => isset($post['TANGGAL_ASSESSMENT_submit'])?$post['TANGGAL_ASSESSMENT_submit']:'',
					'NILAI_JOB_FIT' => isset($post['NILAI_JOB_FIT'])?$post['NILAI_JOB_FIT']:'',
					'REKOMENDASI' => isset($post['REKOMENDASI'])?$post['REKOMENDASI']:'',
					'STATUS' => 1
				);
				// echo '<pre>'; print_r($dataupdate); die;
				$this->riwayat_model->update_dt_assessment($dataupdate, $id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Perubahan Data Assessment Berhasil',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$post['ID']);
			}

			$data = array();
			$data['dt_assessment'] = $this->dt_dasar_model->getDtAssessment($id)->row_array();
			$data['pegawai'] = $this->dt_dasar_model->getPegawaiByNip($data['dt_assessment']['NIP'])->row_array();
			$data['rekomendasi'] = $this->dt_dasar_model->getRekomendasi()->result_array();
			// echo "<pre>"; print_r($data['dt_assessment']); die;
			$this->template->display('riwayat/dt_assessment/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function delete_dt_assessment($id = 0,$pegawai_id = 0){

		if($this->access->permission('delete')) {
			if($id) {

				$dataupdate = array(
					'STATUS'  => 0
				);

				$del = $this->riwayat_model->update_dt_assessment($dataupdate,$id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Assessment Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Assessment Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Assessment Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
		}
	}
	public function create_dt_hukdis($id=0){
		if($this->access->permission('update')){
			if($post = $this->input->post()){
				// echo "<pre>"; print_r($post); die;
				$format="";//format file name
	            $strf=(string)strftime($format);
	            $config['file_name'] = '';
	            $config['upload_path']      = './assets/doc/kgb';
	            $config['allowed_types']    = 'gif|jpg|png|svg|jpeg|pdf';
	            $config['max_size']         = '20000'; // KB    
	            $this->load->library('upload', $config);
	            $file_name = "";
	            if($this->upload->do_upload('E_DOC')) {
	                $upload_data = array('uploads' =>$this->upload->data());
	                $file_name   =  $upload_data['uploads']['file_name'];
	            }else{
	            	$file_name = '';
	            }
				$datainsert = array(
					'NIP' => isset($post['NIP'])?$post['NIP']:'',
					'KATEGORI_HUKDIS' => isset($post['KATEGORI_HUKDIS'])?$post['KATEGORI_HUKDIS']:'',
					'TGL_MONEV' => isset($post['TGL_MONEV_submit'])?$post['TGL_MONEV_submit']:'',
					'TGL_MULAI' => isset($post['TGL_MULAI_submit'])?$post['TGL_MULAI_submit']:'',
					'TGL_SELESAI' => isset($post['TGL_SELESAI_submit'])?$post['TGL_SELESAI_submit']:'',
					'TOTAL_AKUMULASI' => isset($post['TOTAL_AKUMULASI'])?$post['TOTAL_AKUMULASI']:'',
					'STATUS' => 1
				);
				// echo '<pre>'; print_r($datainsert); die;
				$this->riwayat_model->insert_dt_hukdis($datainsert);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Penambahan Data Hukuman Disiplin Berhasil',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$id);
			}

			$data = array();
			$data['pegawai'] = $this->dt_dasar_model->getPegawai($id)->row_array();
			$data['kategori_hukdis'] = $this->dt_dasar_model->getKategoriHukdis()->result_array();
			// echo "<pre>"; print_r($data['kategori_hukdis']); die;
			$this->template->display('riwayat/dt_hukdis/create', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function update_dt_hukdis($id=0){
		if($this->access->permission('update')){
			if($post = $this->input->post()){
				$format="";//format file name
	            // $strf=(string)strftime($format);
	            // $config['file_name'] = '';
	            // $config['upload_path']      = './assets/doc/penghargaan';
	            // $config['allowed_types']    = 'gif|jpg|png|svg|jpeg|pdf';
	            // $config['max_size']         = '20000'; // KB    
	            // $this->load->library('upload', $config);
	            // $file_name = "";
	            // if($this->upload->do_upload('E_DOCS')) {
	            //     $upload_data = array('uploads' =>$this->upload->data());
	            //     $file_name   =  $upload_data['uploads']['file_name'];
	            // }else{
	            // 	$file_name = $post['E_DOC'];
	            // }
				$dataupdate = array(
					'NIP' => isset($post['NIP'])?$post['NIP']:'',
					'KATEGORI_HUKDIS' => isset($post['KATEGORI_HUKDIS'])?$post['KATEGORI_HUKDIS']:'',
					'TGL_MONEV' => isset($post['TGL_MONEV_submit'])?$post['TGL_MONEV_submit']:'',
					'TGL_MULAI' => isset($post['TGL_MULAI_submit'])?$post['TGL_MULAI_submit']:'',
					'TGL_SELESAI' => isset($post['TGL_SELESAI_submit'])?$post['TGL_SELESAI_submit']:'',
					'TOTAL_AKUMULASI' => isset($post['TOTAL_AKUMULASI'])?$post['TOTAL_AKUMULASI']:'',
					'STATUS' => 1
				);
				// echo '<pre>'; print_r($dataupdate); die;
				$this->riwayat_model->update_dt_hukdis($dataupdate, $id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Perubahan Data Hukuman Disiplin Berhasil',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$post['ID']);
			}

			$data = array();
			$data['dt_hukdis'] = $this->dt_dasar_model->getDtHukdis($id)->row_array();
			$data['pegawai'] = $this->dt_dasar_model->getPegawaiByNip($data['dt_hukdis']['NIP'])->row_array();
			$data['kategori_hukdis'] = $this->dt_dasar_model->getKategoriHukdis()->result_array();
			// echo "<pre>"; print_r($data['dt_hukdis']); die;
			$this->template->display('riwayat/dt_hukdis/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}
	public function delete_dt_hukdis($id = 0,$pegawai_id = 0){

		if($this->access->permission('delete')) {
			if($id) {

				$dataupdate = array(
					'STATUS'  => 0
				);

				$del = $this->riwayat_model->update_dt_hukdis($dataupdate,$id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Hukuman Disiplin Berhasil Dihapus',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Hukuman Disiplin Gagal Dihapus',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Hukuman Disiplin Gagal Dihapus',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'dt_dasar/dt_dasar/proses/'.$pegawai_id);
		}
	}

}
