<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kota extends CI_Controller{

	function __construct(){
		parent:: __construct();
		$this->load->model('master/kota_model');
		//$this->load->library('access');
		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index(){
		if($this->access->permission('read')){
			$data = array();
			$this->template->display('master/kota/index', $data);
		}else{
			$this->access->redirect('404');
		}
		
	}
	public function create(){
		
		if($this->access->permission('create')){
			if($post = $this->input->post()){
				$kota = array(
					'KODE_PROVINSI' 		=> isset($post['KODE_PROVINSI'])?$post['KODE_PROVINSI']:'',
					'KODE'					=> isset($post['KODE'])?$post['KODE']:'',
					'KOTA'					=> isset($post['KOTA'])?$post['KOTA']:'',
					'STATUS' 				=> 1
				);

				$id = $this->kota_model->addkota($kota);
				if($id > 0){
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Tambah kota Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);
					
					redirect(base_url().'master/kota');
				}else{
					$notify = array(
						'title' 	=> 'Gagal!',
						'message' 	=> 'Tambah kota gagal, silahkan coba lagi',
						'status' 	=> 'error'
					);
					$this->session->set_flashdata('notify', $notify);
					
					redirect(base_url().'master/kota');
				}
			}
			
			$data = array();
			$data['provinsi']  	= $this->kota_model->getProvinsi()->result_array();
			$this->template->display('master/kota/create', $data);
		} else {
			$this->access->redirect('404');
		}
	}
	public function update($id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$id = $post['ID'];
				$dataupdate = array(
					'KODE_PROVINSI' 	=> isset($post['KODE_PROVINSI'])?$post['KODE_PROVINSI']:'',
					'KODE'				=> isset($post['KODE'])?$post['KODE']:'',
					'KOTA'				=> isset($post['KOTA'])?$post['KOTA']:'',
				);

				
				$insDb = $this->kota_model->updatekota($dataupdate, $id);

				if($insDb > 0){
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan kota Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'master/kota');
				}else{
					$notify = array(
						'title' 	=> 'Gagal!',
						'message'	=> 'Perubahan kota gagal, silahkan coba lagi',
						'status' 	=> 'error'
					);
					$this->session->set_flashdata('notify', $notify);
					redirect(base_url().'master/kota');
				}
			}

			$data = array();
			$data['provinsi']  	= $this->kota_model->getProvinsi()->result_array();
			$data['kota']  		= $this->kota_model->getKota($id)->row_array();
			$this->template->display('master/kota/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}

	
	public function read($id=0){
		if($this->access->permission('read')){
			$data['provinsi']  	= $this->kota_model->getProvinsi()->result_array();
			$data['kota']  		= $this->kota_model->getKota($id)->row_array();
			$this->template->display('master/kota/read', $data);
		}else{
			$this->access->redirect('404');
		}
	}

	public function list_data(){
		$default_order = "ID DESC";
		$limit = 10;
		$where = "";
		$field_name 	= array(
			'KOTA',
			'PROVINSI'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->kota_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->kota_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->kota_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$tombol_aktif = '';
			if($row["STATUS"]==1){
				$tombol_aktif = '<li><a onclick="set_non_aktif('.$row["ID"].')" class="lihat_data" data-popup="tooltip" title="Non Aktifkan" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>';
			}else{
				$tombol_aktif = '<li><a onclick="set_aktif('.$row["ID"].')" class="lihat_data" data-popup="tooltip" title="Aktifkan" data-placement="bottom"><i class="icon-file-check" style="font-size: 13px;"></i></a></li>';
			}
			$aaData[] = array(
				$no,
				$row["KODE"],
				$row["KOTA"],
				$row["PROVINSI"],
				(($row["STATUS"]==1)?'<span class="label label-success">'."AKTIF".'</span>':'<span class="label label-danger">'."TIDAK AKTIF".'</span>'),
				'<ul class="icons-list">
				<li><a href="'.base_url().'master/kota/update/'.urlencode($row["ID"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				<li><a href="'.base_url().'master/kota/read/'.urlencode($row["ID"]).'" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>'.
				$tombol_aktif.'
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}

	public function non_aktif($id = 0){

		$idFilter = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
		if($this->access->permission('delete')) {
			if($id==$idFilter) {

				$dataupdate = array(
					'STATUS' 				=> 0
				);

				$del = $this->kota_model->updatekota($dataupdate,$id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'kota dinonaktifkan',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'master/kota');
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Kota gagal dinonaktifkan',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'master/kota');
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Kota gagal dinonaktifkan',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'master/kota');
		}
	}

	public function aktif($id = 0){

		$idFilter = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
		if($this->access->permission('update')) {
			if($id==$idFilter) {

				$dataupdate = array(
					'STATUS' 				=> 1
				);

				$del = $this->kota_model->updatekota($dataupdate,$id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'Kota Berhasil diaktifkan',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'master/kota');
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'Kota Gagal diaktifkan',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'master/kota');
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'Kota Gagal diaktifkan',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'master/kota');
		}
	}
	public function checkKodeKota()
	{
		$kode = $this->input->post('KODE');
		$kota = $this->kota_model->checkKodeKota($kode)->result_array();
		$res = true;
		if(count($kota)>0){
			$res = false;
		}
		echo json_encode($res);
	}
	public function checkNamaKota()
	{
		$kota = $this->input->post('KOTA');
		$kota = $this->kota_model->checkNamaKota($kota)->result_array();
		$res = true;
		if(count($kota)>0){
			$res = false;
		}
		echo json_encode($res);
	}
}