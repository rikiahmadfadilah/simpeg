<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Config extends CI_Controller{

    function __construct(){
        parent:: __construct();
        $this->load->model('master/config_model');
       //$this->load->library('access');
        $this->load->helper('xml');
        $this->load->helper('text');
    }

    public function index(){
        if($this->access->permission('read')){
            $data = array();
            $this->template->display('master/config/index', $data);
        }else{
            $this->access->redirect('404');
        }
        
    }
    public function update($id = 0){
        if($this->access->permission('update')){

		    $jabatan_unit_kerja_kode = '';
            if($post = $this->input->post()){
                $format="";//format file name
                $strf=(string)strftime($format);
                $config['file_name'] = '';
                $config['upload_path']      = './assets/images/config';
                $config['allowed_types']    = 'gif|jpg|png|svg|jpeg';
                $config['max_size']         = '20000'; // KB    
                $this->load->library('upload', $config);
                $file_name = "";
                if($this->upload->do_upload('PATH')) {
                    $upload_data = array('uploads' =>$this->upload->data());
                    $file_name   =  $upload_data['uploads']['file_name'];
                }else{
                    $file_name = $post["PATH_OLD"];
                }
                if($post['CONFIG_TYPE'] == 0){
                    $cv = isset($post['CONFIG_VALUE'])?$post['CONFIG_VALUE']:'';
                }else{
                    $cv = 'assets/images/config/'.$file_name;
                }
               $dataupdate = array(
				    'CONFIG_CODE'         => isset($post['CONFIG_CODE'])?$post['CONFIG_CODE']:'',
                    'CONFIG_NAME'         => isset($post['CONFIG_NAME'])?$post['CONFIG_NAME']:'',
                    'CONFIG_VALUE'        => $cv,
                    'CONFIG_DESCRIPTION'  => isset($post['CONFIG_DESCRIPTION'])?$post['CONFIG_DESCRIPTION']:'',
                    'CONFIG_UPDATE_BY'    => $this->session->userdata('user_id'),
                    'CONFIG_UPDATE_DATE'  => date('Y-m-d H:i:s'),
                    'CONFIG_STATUS'       => 1
                );
		        // echo "<pre>";print_r($post);print_r($dataupdate);die;
                $insDb = $this->config_model->updateconfig($dataupdate, $id);

                if($insDb > 0){
                    $notify = array(
                        'title'     => 'Berhasil!',
                        'message'   => 'Perubahan konfigurasi Berhasil',
                        'status'    => 'success'
                    );
                    $this->session->set_flashdata('notify', $notify);

                    redirect(base_url().'master/config');
                }else{
                    $notify = array(
                        'title'     => 'Gagal!',
                        'message'   => 'Perubahan jabatan gagal, silahkan coba lagi',
                        'status'    => 'error'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    redirect(base_url().'master/config');
                }
            }

            $data = array();
            $data['config'] = $this->config_model->getconfig($id)->row_array();
            $this->template->display('master/config/update', $data);
        }else{
            $this->access->redirect('404');
        }
    }

    
    public function read($id=0){
        if($this->access->permission('read')){
            $data['config'] = $this->config_model->getconfig($id)->row_array();
            $this->template->display('master/config/read', $data);
        }else{
            $this->access->redirect('404');
        }
    }

    public function list_data_aktif(){
        $default_order = "ID asc";
        $limit = 10;
        $where = "";
        $field_name     = array(
            'CONFIG_CODE',
            'CONFIG_NAME',
            'CONFIG_VALUE'
        );
        $iSortingCols   = ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
        $ordertextarr = array();
        for ($i = 0;$i<$iSortingCols;$i++){
            $iSortCol   = ($this->input->get('iSortCol_'.$i));
            $sSortDir   = (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
            $ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
        }
        
        $ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
        $search     = (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
        $limit      = (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
        $start      = (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
        $data['sEcho'] = $this->input->get('sEcho');
        $data['iTotalRecords'][] = $this->config_model->get_count_all_data ($search,$field_name, $where);
        $data['iTotalDisplayRecords'][] = $this->config_model->get_count_all_data  ($search,$field_name, $where);


        $aaData = array();
        $getData    = $this->config_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
        $no = (($start == 0) ? 1 : $start + 1);
        foreach ($getData as $row) {
            $tombol_aktif = '';
            // if($row["CONFIG_STATUS"]=='1'){
            //     $tombol_aktif = '<li><a onclick="set_non_aktif('.$row["ID"].')" class="lihat_data" data-popup="tooltip" title="Non Aktifkan" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>';
            // }else{
            //     $tombol_aktif = '<li><a onclick="set_aktif('.$row["ID"].')" class="lihat_data" data-popup="tooltip" title="Aktifkan" data-placement="bottom"><i class="icon-file-check" style="font-size: 13px;"></i></a></li>';
            // }
            $aaData[] = array(
                $row["CONFIG_CODE"],
                $row["CONFIG_NAME"],
                $row["CONFIG_VALUE"],
                (($row["CONFIG_STATUS"]=='1')?'<span class="label label-success">'.'AKTIF'.'</span>':'<span class="label label-danger">'.'TIDAK AKTIF'.'</span>'),
                '<ul class="icons-list">
                <li><a href="'.base_url().'master/config/update/'.urlencode($row["ID"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
                <li><a href="'.base_url().'master/config/read/'.urlencode($row["ID"]).'" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>'.
                $tombol_aktif.'
                </ul>'
            );
            $no++;
        }
        $data['aaData'] = $aaData;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));

    }
}