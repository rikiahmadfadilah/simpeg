<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jabatan extends CI_Controller{

    function __construct(){
        parent:: __construct();
        $this->load->model('master/jabatan_model');
		$this->load->model('kepegawaian/pns/pns_model','',TRUE);
       //$this->load->library('access');
        $this->load->helper('xml');
        $this->load->helper('text');
    }

    public function index(){
        if($this->access->permission('read')){
            $data = array();
            $this->template->display('master/jabatan/index', $data);
        }else{
            $this->access->redirect('404');
        }
        
    }
    public function create(){
        
        if($this->access->permission('create')){
            if($post = $this->input->post()){
               $jabatan = array(
				    'kode_fung1'                 => isset($post['jabatan_kode'])?$post['jabatan_kode']:'',
                    'nama_fungsional'                 => isset($post['jabatan_nama'])?$post['jabatan_nama']:'',
				    'tipe_jabatan'                 => isset($post['jabatan_tipe'])?$post['jabatan_tipe']:'',
                    'created_by'            => $this->session->userdata('user_id'),
                    'created_date'          => date('Y-m-d H:i:s'),
                    'aktif'               => 'Y'
                );
		        // echo "<pre>";print_r($jabatan);die;
				
                $jabatan_id = $this->jabatan_model->addjabatan($jabatan);
                if($jabatan_id > 0){
                    $notify = array(
                        'title'     => 'Berhasil!',
                        'message'   => 'Tambah jabatan Berhasil',
                        'status'    => 'success'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    
                    redirect(base_url().'master/jabatan');
                }else{
                    $notify = array(
                        'title'     => 'Gagal!',
                        'message'   => 'Tambah jabatan gagal, silahkan coba lagi',
                        'status'    => 'error'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    
                    redirect(base_url().'master/jabatan');
                }
            }
            
            $data = array();
            $data['jabatan']   = $this->jabatan_model->getjabatan()->result_array();
            $this->template->display('master/jabatan/create', $data);
        } else {
            $this->access->redirect('404');
        }
    }
    public function update($jabatan_id = 0){
        if($this->access->permission('update')){

		    $jabatan_unit_kerja_kode = '';
            if($post = $this->input->post()){
                $jabatan_id = $post['jabatan_id'];
				
               $dataupdate = array(
				    'kode_fung1'          => isset($post['jabatan_kode'])?$post['jabatan_kode']:'',
                    'nama_fungsional'     => isset($post['jabatan_nama'])?$post['jabatan_nama']:'',
                    'tipe_jabatan'        => isset($post['jabatan_tipe'])?$post['jabatan_tipe']:'',
                    'created_by'          => $this->session->userdata('user_id'),
                    'created_date'        => date('Y-m-d H:i:s'),
                    'aktif'               => 'Y'
                );

		        //echo "<pre>";print_r($post);print_r($dataupdate);die;
                $insDb = $this->jabatan_model->updatejabatan($dataupdate, $jabatan_id);

                if($insDb > 0){
                    $notify = array(
                        'title'     => 'Berhasil!',
                        'message'   => 'Perubahan jabatan Berhasil',
                        'status'    => 'success'
                    );
                    $this->session->set_flashdata('notify', $notify);

                    redirect(base_url().'master/jabatan');
                }else{
                    $notify = array(
                        'title'     => 'Gagal!',
                        'message'   => 'Perubahan jabatan gagal, silahkan coba lagi',
                        'status'    => 'error'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    redirect(base_url().'master/jabatan');
                }
            }

            $data = array();
            $data['jabatan'] = $this->jabatan_model->getjabatan($jabatan_id)->row_array();
            $this->template->display('master/jabatan/update', $data);
        }else{
            $this->access->redirect('404');
        }
    }

    
    public function read($jabatan_id=0){
        if($this->access->permission('read')){
            $data['jabatan'] = $this->jabatan_model->getjabatan($jabatan_id)->row_array();
            $this->template->display('master/jabatan/read', $data);
        }else{
            $this->access->redirect('404');
        }
    }

    public function list_data_aktif(){
        $default_order = "tipe_jabatan asc";
        $limit = 10;
        $where = "";
        $field_name     = array(
            'nama_fungsional'
        );
        $iSortingCols   = ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
        $ordertextarr = array();
        for ($i = 0;$i<$iSortingCols;$i++){
            $iSortCol   = ($this->input->get('iSortCol_'.$i));
            $sSortDir   = (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
            $ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
        }
        
        $ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
        $search     = (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
        $limit      = (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
        $start      = (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
        $data['sEcho'] = $this->input->get('sEcho');
        $data['iTotalRecords'][] = $this->jabatan_model->get_count_all_data ($search,$field_name, $where);
        $data['iTotalDisplayRecords'][] = $this->jabatan_model->get_count_all_data  ($search,$field_name, $where);


        $aaData = array();
        $getData    = $this->jabatan_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
        $no = (($start == 0) ? 1 : $start + 1);
        foreach ($getData as $row) {
            $tombol_aktif = '';
            if($row["aktif"]=='Y'){
                $tombol_aktif = '<li><a onclick="set_non_aktif('.$row["idfungsional"].')" class="lihat_data" data-popup="tooltip" title="Non Aktifkan" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>';
            }else{
                $tombol_aktif = '<li><a onclick="set_aktif('.$row["idfungsional"].')" class="lihat_data" data-popup="tooltip" title="Aktifkan" data-placement="bottom"><i class="icon-file-check" style="font-size: 13px;"></i></a></li>';
            }
            if($row["tipe_jabatan"] == 1) {
                $tipe_jabatan = "Fungsional Tertentu";
            }else if($row["tipe_jabatan"] == 2){
                $tipe_jabatan = "Fungsional Umum";
            }else if($row["tipe_jabatan"] == 3){
                $tipe_jabatan = "Struktural";
            }
            $aaData[] = array(
                $row["nama_fungsional"],
                $tipe_jabatan,
                (($row["aktif"]=='Y')?'<span class="label label-success">'.'AKTIF'.'</span>':'<span class="label label-danger">'.'TIDAK AKTIF'.'</span>'),
                '<ul class="icons-list">
                <li><a href="'.base_url().'master/jabatan/update/'.urlencode($row["idfungsional"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
                <li><a href="'.base_url().'master/jabatan/read/'.urlencode($row["idfungsional"]).'" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>'.
                $tombol_aktif.'
                </ul>'
            );
            $no++;
        }
        $data['aaData'] = $aaData;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));

    }

    public function non_aktif($jabatan_id = 0){

        $jabatan_idFilter = filter_var($jabatan_id, FILTER_SANITIZE_NUMBER_INT);
        if($this->access->permission('delete')) {
            if($jabatan_id==$jabatan_idFilter) {

                $dataupdate = array(
                    'aktif' => 'T'
                );

                $del = $this->jabatan_model->updatejabatan($dataupdate,$jabatan_id);
                $notify = array(
                    'title'     => 'Berhasil!',
                    'message'   => 'jabatan dinonaktifkan',
                    'status'    => 'success'
                );
                $this->session->set_flashdata('notify', $notify);

                redirect(base_url().'master/jabatan');
            } else {
                $notify = array(
                    'title'     => 'Gagal!',
                    'message'   => 'jabatan gagal dinonaktifkan',
                    'status'    => 'error'
                );
                $this->session->set_flashdata('notify', $notify);
                redirect(base_url().'master/jabatan');
            }
        } else {
            $notify = array(
                'title'     => 'Gagal!',
                'message'   => 'jabatan gagal dinonaktifkan',
                'status'    => 'error'
            );
            $this->session->set_flashdata('notify', $notify);
            redirect(base_url().'master/jabatan');
        }
    }

    public function aktif($jabatan_id = 0){

        $jabatan_idFilter = filter_var($jabatan_id, FILTER_SANITIZE_NUMBER_INT);
        if($this->access->permission('update')) {
            if($jabatan_id==$jabatan_idFilter) {

                $dataupdate = array(
                    'aktif' => 'Y'
                );

                $del = $this->jabatan_model->updatejabatan($dataupdate,$jabatan_id);
                $notify = array(
                    'title'     => 'Berhasil!',
                    'message'   => 'jabatan Berhasil diaktifkan',
                    'status'    => 'success'
                );
                $this->session->set_flashdata('notify', $notify);

                redirect(base_url().'master/jabatan');
            } else {
                $notify = array(
                    'title'     => 'Gagal!',
                    'message'   => 'jabatan Gagal diaktifkan',
                    'status'    => 'error'
                );
                $this->session->set_flashdata('notify', $notify);
                redirect(base_url().'master/jabatan');
            }
        } else {
            $notify = array(
                'title'     => 'Gagal!',
                'message'   => 'jabatan Gagal diaktifkan',
                'status'    => 'error'
            );
            $this->session->set_flashdata('notify', $notify);
            redirect(base_url().'master/jabatan');
        }
    }
    public function checkKodeJabatan()
    {
        $jabatan_kode = $this->input->post('jabatan_kode');
        $jabatan      = $this->jabatan_model->checkKodeJabatan($jabatan_kode)->result_array();
        $res = true;
        if(count($jabatan)>0){
            $res = false;
        }
        echo json_encode($res);
    }
    public function checkNamaJabatan()
    {
        $jabatan_nama = $this->input->post('jabatan_nama');
        $jabatan = $this->jabatan_model->checkNamaJabatan($jabatan_nama)->result_array();
        $res = true;
        if(count($jabatan)>0){
            $res = false;
        }
        echo json_encode($res);
    }
	
	
    // muklis
    public function check_jabatan_kode()
    {
        $jabatan_kode = $this->input->post('jabatan_kode');
        $jabatan      = $this->jabatan_model->check_jabatan_kode($jabatan_kode)->result_array();
        $res = true;
        if(count($jabatan)>0){
            $res = false;
        }
        echo json_encode($res);
    }	
    public function check_jabatan_nama()
    {
        $jabatan_nama = $this->input->post('jabatan_nama');
        $jabatan = $this->jabatan_model->check_jabatan_nama($jabatan_nama)->result_array();
        $res = true;
        if(count($jabatan)>0){
            $res = false;
        }
        echo json_encode($res);
    }	
}