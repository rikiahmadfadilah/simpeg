<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Provinsi extends CI_Controller{

    function __construct(){
        parent:: __construct();
        $this->load->model('master/provinsi_model');
        //$this->load->library('access');
        $this->load->helper('xml');
        $this->load->helper('text');
    }

    public function index(){
        if($this->access->permission('read')){
            $data = array();
            $this->template->display('master/provinsi/index', $data);
        }else{
            $this->access->redirect('404');
        }
        
    }
    public function create(){
        
        if($this->access->permission('create')){
            if($post = $this->input->post()){
                $provinsi = array(
                    'KODE'                 => isset($post['KODE'])?$post['KODE']:'',
                    'PROVINSI'             => isset($post['PROVINSI'])?$post['PROVINSI']:'',
                    'STATUS'               => 1
                );

                $id = $this->provinsi_model->addprovinsi($provinsi);
                if($id > 0){
                    $notify = array(
                        'title'     => 'Berhasil!',
                        'message'   => 'Tambah provinsi Berhasil',
                        'status'    => 'success'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    
                    redirect(base_url().'master/provinsi');
                }else{
                    $notify = array(
                        'title'     => 'Gagal!',
                        'message'   => 'Tambah provinsi gagal, silahkan coba lagi',
                        'status'    => 'error'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    
                    redirect(base_url().'master/provinsi');
                }
            }
            
            $data = array();
            $data['provinsi']   = $this->provinsi_model->getProvinsi()->result_array();
            $this->template->display('master/provinsi/create', $data);
        } else {
            $this->access->redirect('404');
        }
    }
    public function update($id = 0){
        if($this->access->permission('update')){

            if($post = $this->input->post()){
                $id = $post['ID'];
                $dataupdate = array(
                    'KODE'             => isset($post['KODE'])?$post['KODE']:'',
                    'PROVINSI'         => isset($post['PROVINSI'])?$post['PROVINSI']:'',
                );

                
                $insDb = $this->provinsi_model->updateprovinsi($dataupdate, $id);

                if($insDb > 0){
                    $notify = array(
                        'title'     => 'Berhasil!',
                        'message'   => 'Perubahan provinsi Berhasil',
                        'status'    => 'success'
                    );
                    $this->session->set_flashdata('notify', $notify);

                    redirect(base_url().'master/provinsi');
                }else{
                    $notify = array(
                        'title'     => 'Gagal!',
                        'message'   => 'Perubahan provinsi gagal, silahkan coba lagi',
                        'status'    => 'error'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    redirect(base_url().'master/provinsi');
                }
            }

            $data = array();
            $data['provinsi'] = $this->provinsi_model->getprovinsi($id)->row_array();
            $this->template->display('master/provinsi/update', $data);
        }else{
            $this->access->redirect('404');
        }
    }

    
    public function read($id=0){
        if($this->access->permission('read')){
            $data['provinsi']       = $this->provinsi_model->getprovinsi($id)->row_array();
            $this->template->display('master/provinsi/read', $data);
        }else{
            $this->access->redirect('404');
        }
    }

    public function list_data(){
        $default_order = "PROVINSI ASC";
        $limit = 10;
        $where = "";
        $field_name     = array(
            'PROVINSI'
        );
        $iSortingCols   = ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
        $ordertextarr = array();
        for ($i = 0;$i<$iSortingCols;$i++){
            $iSortCol   = ($this->input->get('iSortCol_'.$i));
            $sSortDir   = (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
            $ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
        }
        
        $ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
        $search     = (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
        $limit      = (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
        $start      = (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
        $data['sEcho'] = $this->input->get('sEcho');
        $data['iTotalRecords'][] = $this->provinsi_model->get_count_all_data($search,$field_name, $where);
        $data['iTotalDisplayRecords'][] = $this->provinsi_model->get_count_all_data($search,$field_name, $where);


        $aaData = array();
        $getData    = $this->provinsi_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
        $no = (($start == 0) ? 1 : $start + 1);
        foreach ($getData as $row) {
            $tombol_aktif = '';
            if($row["STATUS"]==1){
                $tombol_aktif = '<li><a onclick="set_non_aktif('.$row["ID"].')" class="lihat_data" data-popup="tooltip" title="Non Aktifkan" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>';
            }else{
                $tombol_aktif = '<li><a onclick="set_aktif('.$row["ID"].')" class="lihat_data" data-popup="tooltip" title="Aktifkan" data-placement="bottom"><i class="icon-file-check" style="font-size: 13px;"></i></a></li>';
            }
            $aaData[] = array(
                $no,
                $row["PROVINSI"],
                (($row["STATUS"]==1)?'<span class="label label-success">'.'AKTIF'.'</span>':'<span class="label label-danger">'.'TIDAK AKTIF.'.'</span>'),
                '<ul class="icons-list">
                <li><a href="'.base_url().'master/provinsi/update/'.urlencode($row["ID"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
                <li><a href="'.base_url().'master/provinsi/read/'.urlencode($row["ID"]).'" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>'.
                $tombol_aktif.'
                </ul>'
            );
            $no++;
        }
        $data['aaData'] = $aaData;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));

    }

    public function non_aktif($id = 0){

        $idFilter = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
        if($this->access->permission('delete')) {
            if($id==$idFilter) {

                $dataupdate = array(
                    'STATUS'               => 0
                );

                $del = $this->provinsi_model->updateprovinsi($dataupdate,$id);
                $notify = array(
                    'title'     => 'Berhasil!',
                    'message'   => 'provinsi dinonaktifkan',
                    'status'    => 'success'
                );
                $this->session->set_flashdata('notify', $notify);

                redirect(base_url().'master/provinsi');
            } else {
                $notify = array(
                    'title'     => 'Gagal!',
                    'message'   => 'provinsi gagal dinonaktifkan',
                    'status'    => 'error'
                );
                $this->session->set_flashdata('notify', $notify);
                redirect(base_url().'master/provinsi');
            }
        } else {
            $notify = array(
                'title'     => 'Gagal!',
                'message'   => 'provinsi gagal dinonaktifkan',
                'status'    => 'error'
            );
            $this->session->set_flashdata('notify', $notify);
            redirect(base_url().'master/provinsi');
        }
    }

    public function aktif($id = 0){

        $idFilter = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
        if($this->access->permission('update')) {
            if($id==$idFilter) {

                $dataupdate = array(
                    'STATUS'               => 1
                );

                $del = $this->provinsi_model->updateprovinsi($dataupdate,$id);
                $notify = array(
                    'title'     => 'Berhasil!',
                    'message'   => 'provinsi Berhasil diaktifkan',
                    'status'    => 'success'
                );
                $this->session->set_flashdata('notify', $notify);

                redirect(base_url().'master/provinsi');
            } else {
                $notify = array(
                    'title'     => 'Gagal!',
                    'message'   => 'provinsi Gagal diaktifkan',
                    'status'    => 'error'
                );
                $this->session->set_flashdata('notify', $notify);
                redirect(base_url().'master/provinsi');
            }
        } else {
            $notify = array(
                'title'     => 'Gagal!',
                'message'   => 'provinsi Gagal diaktifkan',
                'status'    => 'error'
            );
            $this->session->set_flashdata('notify', $notify);
            redirect(base_url().'master/provinsi');
        }
    }

    public function checkKodeProvinsi()
    {
        $KODE = $this->input->post('KODE');
        $provinsi = $this->provinsi_model->checkKodeProvinsi($KODE)->result_array();
        $res = true;
        if(count($provinsi)>0){
            $res = false;
        }
        echo json_encode($res);
    }
    public function checkNamaProvinsi()
    {
        $PROVINSI = $this->input->post('PROVINSI');
        $provinsi = $this->provinsi_model->checkNamaProvinsi($PROVINSI)->result_array();
        $res = true;
        if(count($provinsi)>0){
            $res = false;
        }
        echo json_encode($res);
    }
}