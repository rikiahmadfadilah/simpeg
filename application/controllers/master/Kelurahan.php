<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kelurahan extends CI_Controller{

	function __construct(){
		parent:: __construct();
		$this->load->model('master/kelurahan_model');
		//$this->load->library('access');
		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index(){
		if($this->access->permission('read')){
			$data = array();
			$this->template->display('master/kelurahan/index', $data);
		}else{
			$this->access->redirect('404');
		}
		
	}
	public function create(){
		
		if($this->access->permission('create')){
			if($post = $this->input->post()){
				$kelurahan = array(
					'kelurahan_kecamatan_kode' 		=> isset($post['kelurahan_kecamatan_kode'])?$post['kelurahan_kecamatan_kode']:'',
					'kelurahan_kode'					=> isset($post['kelurahan_kode'])?$post['kelurahan_kode']:'',
					'kelurahan_nama'					=> isset($post['kelurahan_nama'])?$post['kelurahan_nama']:'',
					'kelurahan_create_by'			=> $this->session->userdata('user_id'),
					'kelurahan_create_date'			=> date('Y-m-d H:i:s'),
					'kelurahan_status' 				=> 1
				);

				$kelurahan_id = $this->kelurahan_model->addkelurahan($kelurahan);
				if($kelurahan_id > 0){
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Tambah kelurahan Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);
					
					redirect(base_url().'master/kelurahan');
				}else{
					$notify = array(
						'title' 	=> 'Gagal!',
						'message' 	=> 'Tambah kelurahan gagal, silahkan coba lagi',
						'status' 	=> 'error'
					);
					$this->session->set_flashdata('notify', $notify);
					
					redirect(base_url().'master/kelurahan');
				}
			}
			
			$data = array();
			// $data['kecamatan']  	= $this->kelurahan_model->getkecamatan()->result_array();
			$this->template->display('master/kelurahan/create', $data);
		} else {
			$this->access->redirect('404');
		}
	}
	public function update($kelurahan_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$kelurahan_id = $post['kelurahan_id'];
				$dataupdate = array(
					'kelurahan_kecamatan_kode' 	=> isset($post['kelurahan_kecamatan_kode'])?$post['kelurahan_kecamatan_kode']:'',
					'kelurahan_kode'				=> isset($post['kelurahan_kode'])?$post['kelurahan_kode']:'',
					'kelurahan_nama'				=> isset($post['kelurahan_nama'])?$post['kelurahan_nama']:'',
				);

				
				$insDb = $this->kelurahan_model->updatekelurahan($dataupdate, $kelurahan_id);

				if($insDb > 0){
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan kelurahan Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'master/kelurahan');
				}else{
					$notify = array(
						'title' 	=> 'Gagal!',
						'message'	=> 'Perubahan kelurahan gagal, silahkan coba lagi',
						'status' 	=> 'error'
					);
					$this->session->set_flashdata('notify', $notify);
					redirect(base_url().'master/kelurahan');
				}
			}

			$data = array();
			$data['kecamatan']  	= $this->kelurahan_model->getkecamatan()->result_array();
			$data['kelurahan']  		= $this->kelurahan_model->getkelurahan($kelurahan_id)->row_array();
			$this->template->display('master/kelurahan/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}

	
	public function read($kelurahan_id=0){
		if($this->access->permission('read')){
			$data['kecamatan']  	= $this->kelurahan_model->getkecamatan()->result_array();
			$data['kelurahan']  		= $this->kelurahan_model->getkelurahan($kelurahan_id)->row_array();
			$this->template->display('master/kelurahan/read', $data);
		}else{
			$this->access->redirect('404');
		}
	}

	public function list_data_aktif(){
		$default_order = "ID DESC";
		$limit = 10;
		$where = "";
		$field_name 	= array(
			'KELURAHAN'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->kelurahan_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->kelurahan_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->kelurahan_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$tombol_aktif = '';
			if($row["STATUS"]==1){
				$tombol_aktif = '<li><a onclick="set_non_aktif('.$row["ID"].')" class="lihat_data" data-popup="tooltip" title="Non Aktifkan" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>';
			}else{
				$tombol_aktif = '<li><a onclick="set_aktif('.$row["ID"].')" class="lihat_data" data-popup="tooltip" title="Aktifkan" data-placement="bottom"><i class="icon-file-check" style="font-size: 13px;"></i></a></li>';
			}
			$aaData[] = array(
				$row["KELURAHAN"],
				$row["KECAMATAN"],
				(($row["STATUS"]==1)?'<span class="label label-success">'.'AKTIF'.'</span>':'<span class="label label-danger">'.'NON AKTIF'.'</span>'),
				'<ul class="icons-list">
				<li><a href="'.base_url().'master/kelurahan/update/'.urlencode($row["ID"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				<li><a href="'.base_url().'master/kelurahan/read/'.urlencode($row["ID"]).'" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>'.
				$tombol_aktif.'
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}

	public function non_aktif($kelurahan_id = 0){

		$kelurahan_idFilter = filter_var($kelurahan_id, FILTER_SANITIZE_NUMBER_INT);
		if($this->access->permission('delete')) {
			if($kelurahan_id==$kelurahan_idFilter) {

				$dataupdate = array(
					'kelurahan_status' 				=> 0,
					'kelurahan_create_by' 			=> $this->session->userdata('user_id'),
					'kelurahan_create_date' 			=> date('Y-m-d H:i:s')
				);

				$del = $this->kelurahan_model->updatekelurahan($dataupdate,$kelurahan_id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'kelurahan dinonaktifkan',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'master/kelurahan');
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'kelurahan gagal dinonaktifkan',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'master/kelurahan');
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'kelurahan gagal dinonaktifkan',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'master/kelurahan');
		}
	}

	public function aktif($kelurahan_id = 0){

		$kelurahan_idFilter = filter_var($kelurahan_id, FILTER_SANITIZE_NUMBER_INT);
		if($this->access->permission('update')) {
			if($kelurahan_id==$kelurahan_idFilter) {

				$dataupdate = array(
					'kelurahan_status' 				=> 1,
					'kelurahan_create_by' 			=> $this->session->userdata('user_id'),
					'kelurahan_create_date' 			=> date('Y-m-d H:i:s')
				);

				$del = $this->kelurahan_model->updatekelurahan($dataupdate,$kelurahan_id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'kelurahan Berhasil diaktifkan',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'master/kelurahan');
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'kelurahan Gagal diaktifkan',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'master/kelurahan');
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'kelurahan Gagal diaktifkan',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'master/kelurahan');
		}
	}
	public function checkKodekelurahan()
	{
		$kelurahan_kode = $this->input->post('kelurahan_kode');
		$kelurahan = $this->kelurahan_model->checkKodekelurahan($kelurahan_kode)->result_array();
		$res = true;
		if(count($kelurahan)>0){
			$res = false;
		}
		echo json_encode($res);
	}
	public function checkNamakelurahan()
	{
		$kelurahan_nama = $this->input->post('kelurahan_nama');
		$kelurahan = $this->kelurahan_model->checkNamakelurahan($kelurahan_nama)->result_array();
		$res = true;
		if(count($kelurahan)>0){
			$res = false;
		}
		echo json_encode($res);
	}
	 public function getDataListKecamatan($page=0)
    {
        $search = $this->input->get('q');
        $page = $this->input->get('page');
        
        $data = array();
        $data['total_count'] = count($this->kelurahan_model->getDataListKecamatanCount($search)->result_array());
        $data['incomplete_results'] = false;
        $getDataListKecamatan = $this->kelurahan_model->getDataListKecamatan($search,$page)->result_array();
        $data['items'] = array();
        $index = 0;
        foreach ($getDataListKecamatan as $dlp) {
            $data['items'][$index]["id"] = $dlp["kecamatan_kode"];
            $data['items'][$index]["kecamatan_id"] = $dlp["kecamatan_id"];
            $data['items'][$index]["kecamatan_kode"] = $dlp["kecamatan_kode"];
            $data['items'][$index]["kecamatan_nama"] = $dlp["kecamatan_nama"];
            $data['items'][$index]["kota_nama"] = $dlp["kota_nama"];
            $index++;
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }
    public function getDataKecamatan()
	{
		$kecamatan_kode = $this->input->post('kecamatan_kode');
		$data = array();
		$data['kecamatan'] = $this->kelurahan_model->getDataKecamatan($kecamatan_kode)->row_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
}