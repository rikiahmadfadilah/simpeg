<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Unit_kerja extends CI_Controller{

    function __construct(){
        parent:: __construct();
        $this->load->model('master/unit_kerja_model');
        //$this->load->library('access');
        $this->load->helper('xml');
        $this->load->helper('text');
    }

    public function index(){
        if($this->access->permission('read')){
            $data = array();
            $this->template->display('master/unit_kerja/index', $data);
        }else{
            $this->access->redirect('404');
        }
        
    }
    public function create(){
        
        if($this->access->permission('create')){
            if($post = $this->input->post()){
                // $provinsi = $post['unit_kerja_provinsi_id'];
                // $provinsi_id = $this->db->query('select provinsi_id from ms_provinsi where provinsi_kode ='.$provinsi)->row_array();
                // $kota = isset($post['unit_kerja_kota_id'])?$post['unit_kerja_kota_id']:'';
                // $kecamatan = isset($post['unit_kerja_kec_id'])?$post['unit_kerja_kec_id']:'';
                // $kelurahan = isset($post['unit_kerja_kel_id'])?$post['unit_kerja_kel_id']:'';
                // $kelurahan_id = $this->db->query('select kelurahan_id from ms_kelurahan where kelurahan_kode ='.$kelurahan)->row_array();
                $unit_kerja = array(
                    'idparent'        => isset($post['idparent'])?$post['idparent']:'',
                    'kode_unker'      => isset($post['kode_unker'])?$post['kode_unker']:'',
                    'nama_unker'      => isset($post['nama_unker'])?$post['nama_unker']:'',
                    'eselon_unker'    => isset($post['eselon_unker'])?$post['eselon_unker']:'',
                    'nama_jab_struk'  => isset($post['nama_jab_struk'])?$post['nama_jab_struk']:'',
                    'aktif'           => 1
                );
                // echo "<pre>";
                // print_r($unit_kerja); die;

                $data_unker = $this->unit_kerja_model->create_unit_kerja($unit_kerja);
                if($data_unker > 0){
                    $notify = array(
                        'title'     => 'Berhasil!',
                        'message'   => 'Tambah Unit Kerja Berhasil',
                        'status'    => 'success'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    
                    redirect(base_url().'master/unit_kerja');
                }else{
                    $notify = array(
                        'title'     => 'Gagal!',
                        'message'   => 'Tambah Unit Kerja gagal, silahkan coba lagi',
                        'status'    => 'error'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    
                    redirect(base_url().'master/unit_kerja');
                }
            }
            
            $data = array();
            $data["unit_kerja"] = $this->unit_kerja_model->getUnitKerja()->result_array();
            $data["satker"] = $this->unit_kerja_model->getUnitKerjaSatker()->result_array();
            $data['eselon']   = $this->unit_kerja_model->geteselon()->result_array();
            $data["provinsi"] = $this->unit_kerja_model->getProvinsi()->result_array();
            $this->template->display('master/unit_kerja/create', $data);
        } else {
            $this->access->redirect('404');
        }
    }
    public function update($idunker = 0){
        if($this->access->permission('update')){

            if($post = $this->input->post()){
                $idunker = $post['idunker'];
                // $unit_kerja_kode_old = $post['unit_kerja_kode_old'];
                // $unit_kerja_kode = $post['unit_kerja_kode'];
                // $jenis_input = $post['jenis_input'];
                // $unit_kerja_parent_id_kode = "";
                // foreach($post["unit_kerja_parent_id_kode"] as $field => $value) {
                //     if($value!=""){
                //         $unit_kerja_parent_id_kode = $value;
                //     }
                // }
                $dataupdate = array(
                    'idparent'        => isset($post['idparent'])?$post['idparent']:'',
                    'kode_unker'      => isset($post['kode_unker'])?$post['kode_unker']:'',
                    'nama_unker'      => isset($post['nama_unker'])?$post['nama_unker']:'',
                    'eselon_unker'    => isset($post['eselon_unker'])?$post['eselon_unker']:'',
                    'nama_jab_struk'  => isset($post['nama_jab_struk'])?$post['nama_jab_struk']:'',
                    'aktif'           => 1
                );

                $data_unit = $this->unit_kerja_model->update_unit_kerja($dataupdate,$idunker);

                if($data_unit > 0){
                    $notify = array(
                        'title'     => 'Berhasil!',
                        'message'   => 'Perubahan Unit Keja Berhasil',
                        'status'    => 'success'
                    );
                    $this->session->set_flashdata('notify', $notify);

                    redirect(base_url().'master                                                                                                                  /unit_kerja');
                }else{
                    $notify = array(
                        'title'     => 'Gagal!',
                        'message'   => 'Perubahan Unit Kerja gagal, silahkan coba lagi',
                        'status'    => 'error'
                    );
                    $this->session->set_flashdata('notify', $notify);
                    redirect(base_url().'master/unit_kerja');
                }
            }

            $data = array();
            $data["unit_kerja"] = $this->unit_kerja_model->getUnitKerja(1,2)->result_array();
            $data["satker"] = $this->unit_kerja_model->getUnitKerjaSatker()->result_array();
            $data['data'] = $this->unit_kerja_model->get_detail_unit_kerja($idunker)->row_array();
            $data['eselon']   = $this->unit_kerja_model->geteselon()->result_array();
            $data["provinsi"] = $this->unit_kerja_model->getProvinsi()->result_array();
            $this->template->display('master/unit_kerja/update', $data);
        }else{
            $this->access->redirect('404');
        }
    }

    
    public function read($unit_kerja_id=0){
        if($this->access->permission('read')){
            $data['unit_kerja']       = $this->unit_kerja_model->get_detail_unit_kerja($unit_kerja_id)->row_array();
            $this->template->display('master/unit_kerja/read', $data);
        }else{
            $this->access->redirect('404');
        }
    }

    public function list_data_aktif(){
        $default_order = "kode_unker ASC";
        $limit = 10;
        $where = "";
        $field_name     = array(
            'kode_unker',
            'nama_unker'
        );
        $iSortingCols   = ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
        $ordertextarr = array();
        for ($i = 0;$i<$iSortingCols;$i++){
            $iSortCol   = ($this->input->get('iSortCol_'.$i));
            $sSortDir   = (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
            $ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
        }
        
        $ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
        $search     = (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
        $limit      = (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
        $start      = (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
        $data['sEcho'] = $this->input->get('sEcho');
        $data['iTotalRecords'][] = $this->unit_kerja_model->get_count_all_data($search,$field_name, $where);
        $data['iTotalDisplayRecords'][] = $this->unit_kerja_model->get_count_all_data($search,$field_name, $where);


        $aaData = array();
        $getData    = $this->unit_kerja_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
        $no = (($start == 0) ? 1 : $start + 1);
        foreach ($getData as $row) {
            $tombol_aktif = '';
            if($row["aktif"]==1){
                $tombol_aktif = '<li><a onclick="set_non_aktif('.$row["idunker"].')" class="lihat_data" data-popup="tooltip" title="Non Aktifkan" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>';
            }else{
                $tombol_aktif = '<li><a onclick="set_aktif('.$row["idunker"].')" class="lihat_data" data-popup="tooltip" title="Aktifkan" data-placement="bottom"><i class="icon-file-check" style="font-size: 13px;"></i></a></li>';
            }
            $aaData[] = array(
                $row["kode_unker"],
                $row["nama_unker"],
                $row["nama_unker_induk"],
                $row["nama_jab_struk"],
                (($row["aktif"]==1)?'<span class="label label-success">'.'AKTIF'.'</span>':'<span class="label label-danger">'.'NON AKTIF'.'</span>'),
                '<ul class="icons-list">
                <li><a href="'.base_url().'master/unit_kerja/update/'.urlencode($row["idunker"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
                <li><a href="'.base_url().'master/unit_kerja/read/'.urlencode($row["idunker"]).'" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>'.
                $tombol_aktif.'
                </ul>'
            );
            $no++;
        }
        $data['aaData'] = $aaData;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));

    }

    public function non_aktif($idunker = 0){

        $idunkerFilter = filter_var($idunker, FILTER_SANITIZE_NUMBER_INT);
        if($this->access->permission('delete')) {
            if($idunker==$idunkerFilter) {

                $dataupdate = array(
                    'aktif'               => 0
                );

                $del = $this->unit_kerja_model->update_unit_kerja($dataupdate,$idunker);
                $notify = array(
                    'title'     => 'Berhasil!',
                    'message'   => 'Unit Keja dinonaktifkan',
                    'status'    => 'success'
                );
                $this->session->set_flashdata('notify', $notify);

                redirect(base_url().'master/unit_kerja');
            } else {
                $notify = array(
                    'title'     => 'Gagal!',
                    'message'   => 'Unit Kerja gagal dinonaktifkan',
                    'status'    => 'error'
                );
                $this->session->set_flashdata('notify', $notify);
                redirect(base_url().'master/unit_kerja');
            }
        } else {
            $notify = array(
                'title'     => 'Gagal!',
                'message'   => 'Unit Kerja gagal dinonaktifkan',
                'status'    => 'error'
            );
            $this->session->set_flashdata('notify', $notify);
            redirect(base_url().'master/unit_kerja');
        }
    }

    public function aktif($idunker = 0){

        $idunkerFilter = filter_var($idunker, FILTER_SANITIZE_NUMBER_INT);
        if($this->access->permission('update')) {
            if($idunker==$idunkerFilter) {

                $dataupdate = array(
                    'aktif'               => 1
                );

                $del = $this->unit_kerja_model->update_unit_kerja($dataupdate,$idunker);
                $notify = array(
                    'title'     => 'Berhasil!',
                    'message'   => 'Unit Kerja Berhasil diaktifkan',
                    'status'    => 'success'
                );
                $this->session->set_flashdata('notify', $notify);

                redirect(base_url().'master/unit_kerja');
            } else {
                $notify = array(
                    'title'     => 'Gagal!',
                    'message'   => 'Unit Kerja Gagal diaktifkan',
                    'status'    => 'error'
                );
                $this->session->set_flashdata('notify', $notify);
                redirect(base_url().'master/unit_kerja');
            }
        } else {
            $notify = array(
                'title'     => 'Gagal!',
                'message'   => 'Unit Kerja Gagal diaktifkan',
                'status'    => 'error'
            );
            $this->session->set_flashdata('notify', $notify);
            redirect(base_url().'master/unit_kerja');
        }
    }
    public function checkKodeUnitKerja()
    {
        $kode_unker = $this->input->post('kode_unker');
        $unit_kerja = $this->unit_kerja_model->checkKodeUnitKerja($kode_unker)->result_array();
        $res = true;
        if(count($unit_kerja)>0){
            $res = false;
        }
        echo json_encode($res);
    }
    public function checkNamaUnitKerja()
    {
        $nama_unker = $this->input->post('nama_unker');
        $unit_kerja = $this->unit_kerja_model->checkNamaUnitKerja($nama_unker)->result_array();
        $res = true;
        if(count($unit_kerja)>0){
            $res = false;
        }
        echo json_encode($res);
    }
}