<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kecamatan extends CI_Controller{

	function __construct(){
		parent:: __construct();
		$this->load->model('master/kecamatan_model');
		//$this->load->library('access');
		$this->load->helper('xml');
		$this->load->helper('text');
	}

	public function index(){
		if($this->access->permission('read')){
			$data = array();
			$this->template->display('master/kecamatan/index', $data);
		}else{
			$this->access->redirect('404');
		}
		
	}
	public function create(){
		
		if($this->access->permission('create')){
			if($post = $this->input->post()){
				$kecamatan = array(
					'KODE_KOTA' 		=> isset($post['KODE_KOTA'])?$post['KODE_KOTA']:'',
					'KODE'				=> isset($post['KODE'])?$post['KODE']:'',
					'KECAMATAN'			=> isset($post['KECAMATAN'])?$post['KECAMATAN']:'',
					'STATUS' 			=> 1
				);

				$kecamatan_id = $this->kecamatan_model->addkecamatan($kecamatan);
				if($kecamatan_id > 0){
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Tambah kecamatan Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);
					
					redirect(base_url().'master/kecamatan');
				}else{
					$notify = array(
						'title' 	=> 'Gagal!',
						'message' 	=> 'Tambah kecamatan gagal, silahkan coba lagi',
						'status' 	=> 'error'
					);
					$this->session->set_flashdata('notify', $notify);
					
					redirect(base_url().'master/kecamatan');
				}
			}
			
			$data = array();
			$data['kota']  	= $this->kecamatan_model->getkota()->result_array();
			$this->template->display('master/kecamatan/create', $data);
		} else {
			$this->access->redirect('404');
		}
	}
	public function update($kecamatan_id = 0){
		if($this->access->permission('update')){

			if($post = $this->input->post()){
				$kecamatan_id = $post['ID'];
				$dataupdate = array(
					'KODE_KOTA' 	=> isset($post['KODE_KOTA'])?$post['KODE_KOTA']:'',
					'KODE'			=> isset($post['KODE'])?$post['KODE']:'',
					'KECAMATAN'		=> isset($post['KECAMATAN'])?$post['KECAMATAN']:'',
				);

				
				$insDb = $this->kecamatan_model->updatekecamatan($dataupdate, $kecamatan_id);

				if($insDb > 0){
					$notify = array(
						'title' 	=> 'Berhasil!',
						'message' 	=> 'Perubahan kecamatan Berhasil',
						'status' 	=> 'success'
					);
					$this->session->set_flashdata('notify', $notify);

					redirect(base_url().'master/kecamatan');
				}else{
					$notify = array(
						'title' 	=> 'Gagal!',
						'message'	=> 'Perubahan kecamatan gagal, silahkan coba lagi',
						'status' 	=> 'error'
					);
					$this->session->set_flashdata('notify', $notify);
					redirect(base_url().'master/kecamatan');
				}
			}

			$data = array();
			$data['kota']  	= $this->kecamatan_model->getkota()->result_array();
			$data['kecamatan']  		= $this->kecamatan_model->getkecamatan($kecamatan_id)->row_array();
			$this->template->display('master/kecamatan/update', $data);
		}else{
			$this->access->redirect('404');
		}
	}

	
	public function read($kecamatan_id=0){
		if($this->access->permission('read')){
			$data['kota']  	= $this->kecamatan_model->getkota()->result_array();
			$data['kecamatan']  		= $this->kecamatan_model->getkecamatan($kecamatan_id)->row_array();
			$this->template->display('master/kecamatan/read', $data);
		}else{
			$this->access->redirect('404');
		}
	}

	public function list_data(){
		$default_order = "ID DESC";
		$limit = 10;
		$where = "";
		$field_name 	= array(
			'KECAMATAN',
			'KOTA'
		);
		$iSortingCols 	= ($this->input->get('iSortingCols')=="0")?"0":$this->input->get('iSortingCols');
		$ordertextarr = array();
		for ($i = 0;$i<$iSortingCols;$i++){
			$iSortCol 	= ($this->input->get('iSortCol_'.$i));
			$sSortDir 	= (!$this->input->get('sSortDir_'.$i))?'':$this->input->get('sSortDir_'.$i);
			$ordertextarr[] = $field_name[$iSortCol]." ".$sSortDir;
		}
		
		$ordertext = ((implode(", ",$ordertextarr)=="")?$default_order:(implode(", ",$ordertextarr)==""));
		$search 	= (!$this->input->get('sSearch'))?'':strtoupper($this->input->get('sSearch'));
		$limit 		= (!$this->input->get('iDisplayLength'))?$limit:$this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))?0:$this->input->get('iDisplayStart');
		$data['sEcho'] = $this->input->get('sEcho');
		$data['iTotalRecords'][] = $this->kecamatan_model->get_count_all_data($search,$field_name, $where);
		$data['iTotalDisplayRecords'][] = $this->kecamatan_model->get_count_all_data($search,$field_name, $where);


		$aaData = array();
		$getData 	= $this->kecamatan_model->get_list_data($limit, $start, $ordertext, $search, $field_name, $default_order, $where)->result_array();
		$no = (($start == 0) ? 1 : $start + 1);
		foreach ($getData as $row) {
			$tombol_aktif = '';
			if($row["STATUS"]==1){
				$tombol_aktif = '<li><a onclick="set_non_aktif('.$row["ID"].')" class="lihat_data" data-popup="tooltip" title="Non Aktifkan" data-placement="bottom"><i class="icon-cancel-square" style="font-size: 13px;"></i></a></li>';
			}else{
				$tombol_aktif = '<li><a onclick="set_aktif('.$row["ID"].')" class="lihat_data" data-popup="tooltip" title="Aktifkan" data-placement="bottom"><i class="icon-file-check" style="font-size: 13px;"></i></a></li>';
			}
			$aaData[] = array(
				$no,
				$row["KECAMATAN"],
				$row["KOTA"],
				(($row["STATUS"]==1)?'<span class="label label-success">'.'AKTIF'.'</span>':'<span class="label label-danger">'.'TIDAK AKTIF'.'</span>'),
				'<ul class="icons-list">
				<li><a href="'.base_url().'master/kecamatan/update/'.urlencode($row["ID"]).'" class="update_data" data-popup="tooltip" title="Ubah" data-placement="bottom"><i class="icon-pencil7" style="font-size: 13px;"></i></a></li>
				<li><a href="'.base_url().'master/kecamatan/read/'.urlencode($row["ID"]).'" class="lihat_data" data-popup="tooltip" title="Lihat" data-placement="bottom"><i class="icon-file-text" style="font-size: 13px;"></i></a></li>'.
				$tombol_aktif.'
				</ul>'
			);
			$no++;
		}
		$data['aaData'] = $aaData;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));

	}

	public function non_aktif($kecamatan_id = 0){

		$kecamatan_idFilter = filter_var($kecamatan_id, FILTER_SANITIZE_NUMBER_INT);
		if($this->access->permission('delete')) {
			if($kecamatan_id==$kecamatan_idFilter) {

				$dataupdate = array(
					'STATUS' 				=> 0
				);

				$del = $this->kecamatan_model->updatekecamatan($dataupdate,$kecamatan_id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'kecamatan dinonaktifkan',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'master/kecamatan');
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'kecamatan gagal dinonaktifkan',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'master/kecamatan');
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'kecamatan gagal dinonaktifkan',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'master/kecamatan');
		}
	}

	public function aktif($kecamatan_id = 0){

		$kecamatan_idFilter = filter_var($kecamatan_id, FILTER_SANITIZE_NUMBER_INT);
		if($this->access->permission('update')) {
			if($kecamatan_id==$kecamatan_idFilter) {

				$dataupdate = array(
					'STATUS' 				=> 1
				);

				$del = $this->kecamatan_model->updatekecamatan($dataupdate,$kecamatan_id);
				$notify = array(
					'title' 	=> 'Berhasil!',
					'message' 	=> 'kecamatan Berhasil diaktifkan',
					'status' 	=> 'success'
				);
				$this->session->set_flashdata('notify', $notify);

				redirect(base_url().'master/kecamatan');
			} else {
				$notify = array(
					'title' 	=> 'Gagal!',
					'message' 	=> 'kecamatan Gagal diaktifkan',
					'status' 	=> 'error'
				);
				$this->session->set_flashdata('notify', $notify);
				redirect(base_url().'master/kecamatan');
			}
		} else {
			$notify = array(
				'title' 	=> 'Gagal!',
				'message' 	=> 'kecamatan Gagal diaktifkan',
				'status' 	=> 'error'
			);
			$this->session->set_flashdata('notify', $notify);
			redirect(base_url().'master/kecamatan');
		}
	}
	public function checkKodekecamatan()
	{
		$KODE = $this->input->post('KODE');
		$kecamatan = $this->kecamatan_model->checkKodekecamatan($KODE)->result_array();
		$res = true;
		if(count($kecamatan)>0){
			$res = false;
		}
		echo json_encode($res);
	}
	public function checkNamakecamatan()
	{
		$KECAMATAN = $this->input->post('KECAMATAN');
		$kecamatan = $this->kecamatan_model->checkNamakecamatan($KECAMATAN)->result_array();
		$res = true;
		if(count($kecamatan)>0){
			$res = false;
		}
		echo json_encode($res);
	}
}