<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('dashboard/dashboard_model');
	}

	public function index()
	{
		$data = array();
		$array_jk = array('Grafik Prosentase Pegawai Berdasarkan GolonganL');
		$array_jml_jk = array(array('name'=>'Grafik Prosentase Pegawai Berdasarkan Golongan', 'data'=>array()));
		$array_datas = array();
		$data_gol = $this->dashboard_model->getDataGrafik1()->result_array();
		 
		$i=0;
		while($i < count($data_gol)){
		$array_datas[$data_gol[$i]['JENIS_KELAMIN']] = intval($data_gol[$i]['JML']);
		$i++;
		}
		 
		// set value per data grafik
		foreach($array_datas as $key=>$val){
		array_push($array_jml_jk[0]['data'], array((string)$key, $val));
		}
		 
		$data['array_jk'] = json_encode($array_jk);
		$data['array_jml_jk'] = json_encode($array_jml_jk);

		$data['grafik2'] = $this->dashboard_model->getDataGrafik2()->result();
		$data['grafik3'] = $this->dashboard_model->getDataGrafik3()->result();
		$data['grafik4'] = $this->dashboard_model->getDataGrafik4()->result();
		// echo"<pre>"; print_r($data['grafik4']); die;

		$this->template->display('home/dashboard', $data);	
	}

	public function getDataGrafik(){
		$data = array();

		// $grafik1 = $this->dashboard_model->getDataGrafik1()->result_array();
		// foreach ($grafik1 as $k => $v) {
		// 	$description = strip_tags($v['JENIS_KELAMIN']);
		// 	$short = explode(",", $description);
		// 	$short[0] = ucwords(strtolower($short[0]));
		// 	$data['grafik1'][$k]['description'] = $short[0];
		// 	$data['grafik1'][$k]['name'] = $v['JENIS_KELAMIN'];
		// 	$data['grafik1'][$k]['jml'] = $v['JML'];
		// }

		// echo json_encode($data); 
	}
}
