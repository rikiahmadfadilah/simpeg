<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('auth/auth_model','',TRUE);
	}

	public function login($force=0)
	{
		if($force==0) {
			$data = array();
			$data["message"] = $this->session->flashdata('message');
			if($this->session->userdata('pegawai_nama')) {
				redirect(base_url().'home/auth/lock');
			} else {
				$this->template->single('login', $data);
			}
		} else {
			$this->session->sess_destroy();
			$this->access->redirect('login');
		}
	}

	
	public function logout()
	{
		$data = array();
		$this->session->sess_destroy();
		$this->access->redirect('login');
	}

	public function lock()
	{
		if($this->session->userdata('pegawai_nama')) {
			$data = array();
			$this->session->set_userdata(array('logged_in'=>false));
			$this->template->single('lockscreen', $data);
		} else {
			redirect(base_url().'home/auth/login');
		}
	}
	public function changeroles() {
		$newRoles = $this->input->post('newRoles');
		$newRolesName = $this->input->post('newRolesName');


		$newdata = array(
			'user_akses_id'			=> $newRoles,
			'user_akses_nama'		=> $newRolesName,
		);
		$this->session->set_userdata($newdata);
		$getAkses = $this->db->query("SELECT * FROM view_ms_akses WHERE akses_id = '".$newRoles."'")->row_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($getAkses));;
	}
	public function checklogin() {
		$feedback = array('status'=>'','message'=>'','redirect_url'=>'');
		$data = array();
		$data["status"] = "";
		$data["message"] = "";
		if($post = $this->input->post()) {

			$username = $post['username'];
			$password = $post['password'];
			$cekuser = $this->auth_model->checkUsername($username)->row_array();

			if(count($cekuser)>0){
				if($cekuser['user_status']==1) {
					$authenticated = false;
					$authenticated = ($this->auth_model->getAuth($username, passwordEncoder($password))>0)?true:false;
					if($authenticated) {
						$userdata = $this->auth_model->getUser($username)->row_array();
						
						if(count($userdata)>0){
							$session = array(
								'user_id'				=> $userdata["user_id"],
								'username'				=> $userdata["username"],
								'NIP'					=> $userdata["NIP"],
								'NAMA'					=> $userdata["NAMA"],
								'user_ip_address'		=> $userdata["user_ip_address"],
								'user_last_online'		=> $userdata["user_last_online"],
								'user_akses_id'			=> $userdata["user_default_akses_id"],
								'user_akses_nama'		=> $userdata["user_default_akses_nama"],
								'UNIT_KERJA'	=> $userdata["UNIT_KERJA"],
								'NAMA_JAB'	=> $userdata["NAMA_JAB"],
								'ID'			=> $userdata["ID"],
								'ID_UNKER'						=> $userdata["ID_UNKER"],
								'KODE_UNKER'				=> $userdata["KODE_UNKER"],
								'ID_PARENT_UNKER'			=> $userdata["ID_PARENT_UNKER"],
								// 'unit_kerja_nama'					=> $userdata["unit_kerja_nama"],
								// 'unit_kerja_kode'					=> $userdata["unit_kerja_kode"],
								// 'parent_unit_kerja_id'				=> $userdata["parent_unit_kerja_id"],
								// 'parent_unit_kerja_id_kode'			=> $userdata["parent_unit_kerja_id_kode"],
								// 'parent_unit_kerja_parent_id_kode'	=> $userdata["parent_unit_kerja_parent_id_kode"],
								// 'parent_unit_kerja_nama'			=> $userdata["parent_unit_kerja_nama"],
								// 'parent_unit_kerja_kode'			=> $userdata["parent_unit_kerja_kode"],
								// 'atasan_parent_unit_kerja_id'				=> $userdata["atasan_parent_unit_kerja_id"],
								// 'atasan_parent_unit_kerja_id_kode'			=> $userdata["atasan_parent_unit_kerja_id_kode"],
								// 'atasan_parent_unit_kerja_parent_id_kode'	=> $userdata["atasan_parent_unit_kerja_parent_id_kode"],
								// 'atasan_parent_unit_kerja_nama'			=> $userdata["atasan_parent_unit_kerja_nama"],
								// 'atasan_parent_unit_kerja_kode'			=> $userdata["atasan_parent_unit_kerja_kode"],
								// 'user_operator_unit_kerja_id'			=> $userdata["user_operator_unit_kerja_id"],
								// 'user_operator_unit_kerja_kode'			=> $userdata["user_operator_unit_kerja_kode"],
								// 'pegawai_old_id'			=> $userdata["pegawai_old_id"],
								
								'PHOTO'			=> $userdata["PHOTO"],
								'logged_in' 			=> true
							);

							$this->session->set_userdata($session);
							$this->auth_model->setLastLogin($userdata["user_id"]);
							$feedback['status'] = 1;
							$feedback['message'] = 'OK';
							if($this->session->userdata('prev_url')!='') {
								$feedback['redirect_url'] = str_replace('/index.php','',$this->session->userdata('prev_url'));
							} else {	
								
								$defMenu = $this->auth_model->getUserDefaultMenu($username)->row_array();
								/*echo "<pre>";
								print_r($defMenu);
								die();*/
								if(count($defMenu)>0){
									$feedback['redirect_url'] = base_url().$defMenu["menu_url"];
								}else{
									$feedback['redirect_url'] = base_url().'home/dashboard';
								}
								//$feedback['redirect_url'] = base_url().'home/dashboard';
							}

						} else {
							$feedback['status'] = 0;
							$feedback['message'] = 'Username tidak Terdaftar';
							$feedback['redirect_url'] = '';
						}

					} else {
						$feedback['status'] = 0;
						$feedback['message'] = 'Username dan Password tidak Cocok';
						$feedback['redirect_url'] = '';
					}
				} else {
					$feedback['status'] = 0;
					$feedback['message'] = 'User Sudah tidak Aktif';
					$feedback['redirect_url'] = '';
				}
			} else {
				$feedback['status'] = 0;
				$feedback['message'] = 'Username tidak Terdaftar';
				$feedback['redirect_url'] = '';
			}
		} else {
			$feedback['status'] = 'error';
			$feedback['message'] = 'Sistem sedang gangguan';
			$feedback['redirect_url'] = '';
		}

		if($feedback['status']==1){
			redirect($feedback['redirect_url']);
		}else{
			$this->session->set_userdata(array('logged_in'=>false));
			$this->session->set_flashdata(array('status' => $feedback['status'],'message' => $feedback['message']));
			redirect(base_url().'home/auth/login');
		}
		//echo json_encode($feedback);
	}

	private function LDAPChecking() {
		$ldap = @ldap_connect($this->config->config['LDAP_server'], $this->config->config['LDAP_port']);
		@ldap_close($ldap);
		return $ldap?true:false;
	}

	private function LDAPAuth($username, $password) {
		$ldap = @ldap_connect($this->config->config['LDAP_server']);
		$bind = @ldap_bind($ldap, $this->config->config['LDAP_domain']. "\\" . $username, $password);
		@ldap_close($ldap);
		return $bind?true:false;
	}
}
