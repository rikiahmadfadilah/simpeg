<?php
class Template {

	protected $_ci;

	function __construct(){
		$this->_ci = &get_instance();
		$this->_ci->load->model('global_model','',TRUE);
		$this->_ci->load->model('auth/auth_model','',TRUE);
		$this->_ci->load->helper('global_helper','',TRUE);
	}
	function ordered_menu($array,$parent_id = 0)
	{
		$temp_array = array();
		foreach($array as $element)
		{
			if($element['menu_parent_id']==$parent_id)
			{
				$element['subs'] = $this->ordered_menu($array,$element['menu_id']);
				$temp_array[] = $element;
			}
		}
		return $temp_array;
	}
	function get_html_sub_menu($array,$parent_id = 0)
	{
		//if($parent_id==0)

		$menu_html = '<ul class="">';
		
		
		foreach($array as $element)
		{
			if($element['menu_parent_id']==$parent_id)
			{

				$getMenuIsParent = $this->_ci->global_model->getMenuIsParent($element['menu_id']);
				$datamenu =  $this->_ci->global_model->getThisMenu($this->_ci->uri->segment(1)."/".$this->_ci->uri->segment(2))->row_array();
				if(count($datamenu)>0){
					$menu_html .= '<li class="'.(($datamenu["menu_id"]==$element["menu_id"])?"active":"").'"><a href="'.(($element['menu_url']!="javascript:void(0);")?base_url().$element['menu_url']:"javascript:void(0);").'">'.(($element['menu_icon']!=null|| $element['menu_icon']!="")?'<i class="'.$element['menu_icon'].'"></i>':'').' <span>'.$element['menu_nama'].'</span>'.(($getMenuIsParent>0)?'':"").'</a>';
					$menu_html .= $this->get_html_sub_menu($array,$element['menu_id']);

					$menu_html .= '</li>';
				}else{
					$menu_html .= '<li class=""><a href="'.(($element['menu_url']!="javascript:void(0);")?base_url().$element['menu_url']:"javascript:void(0);").'">'.(($element['menu_icon']!=null|| $element['menu_icon']!="")?'<i class="'.$element['menu_icon'].'"></i>':'').' <span> '.$element['menu_nama'].' </span>'.(($getMenuIsParent>0)?'':"").'</a>';
					$menu_html .= $this->get_html_sub_menu($array,$element['menu_id']);

					$menu_html .= '</li>';
				}
				
			}
		}
		$menu_html .= '</ul>';
		
		return $menu_html;
	}
	function get_html_menu($array,$parent_id = 0)
	{
		
		$menu_html = '<ul class="navigation navigation-main navigation-accordion navigation-xs navigation-bordered menu_epgawai">';	
		
		foreach($array as $element)
		{
			if($element['menu_parent_id']==$parent_id)
			{

				$getMenuIsParent = $this->_ci->global_model->getMenuIsParent($element['menu_id']);
				$datamenu =  $this->_ci->global_model->getThisMenu($this->_ci->uri->segment(1)."/".$this->_ci->uri->segment(2))->row_array();
				if(count($datamenu)>0){
					$menu_html .= '<li class="'.(($datamenu["menu_id"]==$element["menu_id"] || $datamenu["menu_parent_id"]==$element["menu_id"])?"active":"").'"><a href="'.(($element['menu_url']!="javascript:void(0);")?base_url().$element['menu_url']:"javascript:void(0);").'">'.(($element['menu_icon']!=null|| $element['menu_icon']!="")?'<i class="'.$element['menu_icon'].'"></i>':'').' <span>'.$element['menu_nama'].'</span>'.(($getMenuIsParent>0)?'':"").'</a>';
					if($getMenuIsParent>0){
						$menu_html .= $this->get_html_sub_menu($array,$element['menu_id']);	

					}
					$menu_html .= '</li>';
				}else{
					$menu_html .= '<li class=""><a href="'.(($element['menu_url']!="javascript:void(0);")?base_url().$element['menu_url']:"javascript:void(0);").'">'.(($element['menu_icon']!=null|| $element['menu_icon']!="")?'<i class="'.$element['menu_icon'].'"></i>':'').' <span> '.$element['menu_nama'].' </span>'.(($getMenuIsParent>0)?'':"").'</a>';
					if($getMenuIsParent>0){
						$menu_html .= $this->get_html_sub_menu($array,$element['menu_id']);	
					}
					$menu_html .= '</li>';
				}
			}
		}
		$menu_html .= '</ul>';

						//echo "<pre>";
		$menu_html = str_replace('<ul class=""></ul>',"",$menu_html);
		
		return $menu_html;
	}
	function get_html_notifikasi($array)
	{
		$menu_html = '<a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
		<i class="clip-notification-2"></i>
		<span class="badge"> '.count($array).'</span>
		</a>';
		$menu_html .= '<ul class="dropdown-menu notifications">';	
		$menu_html .='<li><span class="dropdown-menu-title"> Anda Memiliki '.count($array).' notifikasi</span></li>';
		$menu_html .='<li><div class="drop-down-wrapper"><ul>';
		if(count($array)>0){
			foreach($array as $element)
			{
				$menu_html .='<li><a href="'.base_url().$element["notif_link"].'">
				<span class="message"> '.$element["notif_message"].'</span>
				<span class="time"> '.timeToTextNotif($element["notif_date"]).'</span>
				</a>
				</li>';
			}

		}

		$menu_html .='</ul></div></li>';
		$menu_html .='<li class="view-all">
		<a href="'.base_url().'home/notifikasi'.'">
		Lihat Semua Notifikasi <i class="fa fa-arrow-circle-o-right"></i>
		</a>
		</li></ul>';


		return $menu_html;
	}
	function display($template, $data=null, $ignore=null){
		$accessid = $this->_ci->session->userdata('user_akses_id');
		$data_user = $userdata = $this->_ci->auth_model->getUser($this->_ci->session->userdata('username'))->row_array();
		$data["photo"] = (($data_user["PHOTO"]==null || $data_user["PHOTO"] == "")?"defaultphoto.png":$data_user["PHOTO"]);
		$logo_sidebar = $this->_ci->global_model->getGlobalConfig(['CONFIG_CODE' => 'logo_sidebar']);
		$data["logo_sidebar"] = $logo_sidebar->CONFIG_VALUE;

		$menu = $this->_ci->global_model->getMenus($accessid)->result_array();
		$html_menu = $this->get_html_menu($menu,0);
		$data["menu"] = $html_menu;
		$datamenu =  $this->_ci->global_model->getThisMenu($this->_ci->uri->segment(1)."/".$this->_ci->uri->segment(2))->row_array();

		$data["breadcrumb"] = "";
		$data["statusproses"] = $this->_ci->session->flashdata('statusproses');
		$data["message"] = $this->_ci->session->flashdata('message');
		if(count($datamenu)>0){
			$data["currentmenu"] = $datamenu["menu_nama"];
			$data["currentmenuparent"] = ($datamenu["menu_parent_id"]==0)?$datamenu["menu_id"]:$datamenu["menu_parent_id"];
		}
		$data["data_Akses"] = $this->_ci->db->query("SELECT * FROM view_ms_roles WHERE role_user_id = '".$data_user["user_id"]."' AND role_status = 1 ORDER BY akses_nama ASC")->result_array();
		$data["current_akses_id"] = $accessid;
		$data['_content'] = $this->_ci->load->view('/sources/'.$template, $data, TRUE);

		$this->_ci->load->view('/template_render', $data);
	}

	function single($template, $data=null){
		$bg_login = $this->_ci->global_model->getGlobalConfig(['CONFIG_CODE' => 'bg_login']);
		$app_name = $this->_ci->global_model->getGlobalConfig(['CONFIG_CODE' => 'app_name']);
		$company_name = $this->_ci->global_model->getGlobalConfig(['CONFIG_CODE' => 'company_name']);
		$company_name = $this->_ci->global_model->getGlobalConfig(['CONFIG_CODE' => 'company_name']);
		$logo_login = $this->_ci->global_model->getGlobalConfig(['CONFIG_CODE' => 'logo_login']);
		$data = [
			'bg_login' => $bg_login->CONFIG_VALUE,
			'app_name' => $app_name->CONFIG_VALUE,
			'company_name' => $company_name->CONFIG_VALUE,
			'logo_login' => $logo_login->CONFIG_VALUE
		];
		// echo "<pre>"; print_r($data); die;
		$this->_ci->load->view('/structures/single/'.$template, $data);
	}

	function printout($data=null){
		$this->_ci->load->view('/structures/print_header', $data);
		$this->_ci->load->view('/structures/print_footer');
	}
}

?>
