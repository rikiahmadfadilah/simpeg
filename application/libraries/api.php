<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller {

    public      $access_idle = 3600;
    public      $status = array(0, 1, 2);
    protected   $supported_output = array('json', 'xml', 'jsonp', 'serialize');
    protected   $another_output = array('index', 'notify', 'doc', '');
    public      $helper = array('init_api',
                    'auth_api',
                    'post_api',
                    'get_api',
                );
    public      $withoutuser = array(
                    'getAccessToken',
                    'getAnnouncement',
                    'getAuth',
                    'getProducts',
                    'getTOCRegistration',
                    'checkNIK',
                    'checkEmailAddress',
                    'checkPhoneNumber',
                    'uploadTemp',
                    'postRegistration',
                    'doConfirmRegistration',
                    'resendConfirmRegistration',
                    'forgotPassword',
                    'barcodeGetData',
                    'checkVersion',
                    'getAbout',
                    'testFCM',
                    'getDataPegawai',
                    'getDataUnitKerja'
                );
    public      $timestart = '';
    public      $param = array();

    public function __construct() {
        parent::__construct();
    }

    public function _remap()
    {
        if(!in_array($this->uri->segment(2), $this->another_output)) {
            $this->initializing();
        }
    }

    /* ------------------------------------------------------------------------ */
    /**
     * Initializing API engine
     */

    public function index() {
        redirect('API/doc');
    }

    public function doc() {
        $this->load->view('apidoc', null);
    }

    private function initializing() {
        //set time start for generator time
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $start = $time;
        $this->timestart = $start;

        //load helper
        $this->load->helper($this->helper);

        // we need it to return data who was user wont
        // get setted parameter
        $param = $this->input->get();

        // get setted output (required) json, xml or jsonp available now
        $output_type = $this->uri->segment(2);

        // get setted method
        $method = $this->uri->segment(3);

        //checking output type
        
        if($output_type=='') {
            $result_api = array('status'=>$this->status[0],'code'=>204,'message'=>'Please set your output type!','generated'=>generated_time($this->timestart),'serverTime'=>time());
            echo $this->render_output('json', $result_api);
            die();
        }

        //checking sopported type
        if(!in_array($output_type, $this->supported_output)) {
            $result_api = array('status'=>$this->status[0],'code'=>204,'message'=>'Type name ('.$output_type.') isn\'t available, please try json, jsonp, serialize or xml.','generated'=>generated_time($this->timestart),'serverTime'=>time());
            echo $this->render_output('json', $result_api); exit();
        } else {

            if($method=='getAccessToken') {
                if(empty($param['app_code'])) {
                    $result_api = array('status'=>$this->status[0],'code'=>403,'message'=>'What\'s your application code?','generated'=>generated_time($this->timestart),'serverTime'=>time());
                    echo $this->render_output($output_type, $result_api);
                    die();
                }
            } elseif($method=='uploadTemp') {
                echo $this->output($output_type, $method, $param);
                die();
            } else {
                if(empty($param['access_token'])) {
                    $result_api = array('status'=>$this->status[0],'code'=>403,'message'=>'Can you tell me about your access token?','generated'=>generated_time($this->timestart),'serverTime'=>time());
                    echo $this->render_output($output_type, $result_api);
                    die();
                } else {
                    $check_token = check_access_token($this, $param);
                    if(!$check_token) {
                        $result_api = array('status'=>$this->status[2],'code'=>403,'message'=>'Your access token is unusable!','generated'=>generated_time($this->timestart),'serverTime'=>time());
                        echo $this->render_output($output_type, $result_api);
                        die();
                    }
                }
            }
            // generate now as output type and setted method
            echo $this->output($output_type, $method, $param);
        }

        die();
    }

    /* ------------------------------------------------------------------------ */

    /**
     * Generate data request
     * 
     * @access  public
     * @param   methode API
     * @return  json, jsonp, xml
     */

    private function output($output_type, $method, $param) {
        // by default, set error
        $result_api = array('status'=>$this->status[0],'message'=>'Where are your method?','serverTime'=>time());

        // execute if method is setted
        if($method!='') {

            // set error by default before checking existing function
            $result_api = array('status'=>$this->status[0],'message'=>'Your method ('.$method.') isn\'t supported.','serverTime'=>time());
            // check existing function in loaded helper
            if (function_exists($method))
            {
                //yeah, get result now from function by method
                if(in_array($method, $this->withoutuser)) {
                    $result_api = $method($this, $param);
                } else {
                    $result_api = array('status'=>$this->status[0],'message'=>'User ID is required','serverTime'=>time());
                    if(isset($param['user_id'])) {
                        $result_api = array('status'=>$this->status[2],'message'=>'Expired Session, please relogin','serverTime'=>time());
                        $getUser = explode(':', base64_decode($param['access_token']));
                        if($param['user_id']==$getUser[0]) {
                            $result_api = $method($this, $param);
                        }
                    }
                }
            }
        }

        $result_api['generated'] = generated_time($this->timestart);
        $result_api['serverTime'] = time();

        return $this->render_output($output_type, $result_api);
    }

    /* ------------------------------------------------------------------------ */

    /**
     * Generate data request as xml
     * 
     * @access  public
     * @param   methode API
     * @return  xml
     */

    private function render_output($output_type, $result_api) {
        // if you don't understand about source below, you must studying php from basic again 
        switch ($output_type) {
            case 'json':
            header('Content-Type: application/json');
            echo json_encode($result_api);
            break;
            
            case 'xml':
            $this->load->library('array2xml');
            header("Content-Type: application/xml; charset=utf-8");
            $xml = $this->array2xml->createXML('mki-kkp-epegawai', $result_api);
            echo $xml->saveXML();
            break;

            case 'jsonp':
            header('Content-Type: application/javascript');
            echo json_encode($result_api);
            break;

            case 'serialize':
            header('Content-type: text/plain');
            echo serialize($result_api);
            break;

            default:
            header('Content-Type: application/json');
            echo json_encode($result_api);
            break;
        }

    }

}