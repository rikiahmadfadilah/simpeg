<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

function sendSurvey($_this, $param){
	$_this->load->model('responden_model', '', TRUE);
	$_this->load->model('auth_api_model','',TRUE);

	$data 				  = array();
	$resdata 	  		  = array();
	$datakuisoner 		  = array();
	

	if(count($param)>2){
			// echo '<pre>';print_r($param);die;

 			$ansadd	 	= isset($param['ansadd'])?$param['ansadd']:'';
 			$qrcode	 	= isset($param['qrcode'])?$param['qrcode']:'';
			$nik 	 	= isset($param['nik'])?$param['nik']:'';
			$nama 	 	= isset($param['nama'])?$param['nama']:'';
			$alamat  	= isset($param['alamat'])?$param['alamat']:'';
			$kabkota 	= isset($param['kabkota'])?$param['kabkota']:'';
			$kecamatan 	= isset($param['kecamatan'])?$param['kecamatan']:'';
			$kelurahan 	= isset($param['kelurahan'])?$param['kelurahan']:'';
			$rt 	 	= isset($param['rt'])?$param['rt']:'';
			$rw 	 	= isset($param['rw'])?$param['rw']:'';
			$loc 	 	= explode('|', (isset($param['loc'])?$param['loc']:'|'));
			
			$imb   			= (isset($param['ansadd']['35']) && $param['ansadd']['35']=='161_')?'1':'2';
			$noimb 			= (isset($param['ansadd']['35']) && $param['ansadd']['35']=='161_') && isset($param['noimb'])?$param['noimb']:'';
			$luastanah  	= isset($param['luastanah'])?$param['luastanah']:'';
			$luasbangunan  	= isset($param['luasbangunan'])?$param['luasbangunan']:'';
			$nop 			= (isset($param['nop']) && $param['nop']!='')?$param['nop']:0;
			$njop 			= (isset($param['njop']) && $param['njop']!='')?$param['njop']:0;
			$thnbangun		= (isset($param['thnbangun']) && $param['thnbangun']!='')?$param['thnbangun']:0;
			$thnrenov		= (isset($param['thnrenov']) && $param['thnrenov']!='')?$param['thnrenov']:0;

			$kepemilikan = null;
            $statuskepemilikan = null;
            $fungsibangunan = null;
            $tingkatbangunan = null;
            $klasifikasikompleksitas = null;
            $resikokebakaran = null;
            $zonasigempa = null;
            $kepadatanbangunan = null;
            $waktupenggunaan = null;
            $konspembatas = null;
            $konspenanda = null;
            $konspenghubung = null;
            $konskolam = null;
            $konsmenara = null;
            $konsmonumen = null;
            $konsinstalasi = null;
            $konsreklame = null;
            $perkerasan = null;
            $turap = null;

			//kepemilikan
			switch (isset($param['ansadd']['3'])?$param['ansadd']['3']:'') {
				case '3_':
				$kepemilikan = 1;
				break;
				case '4_':
				$kepemilikan = 2;
				break;
				case '5_':
				$kepemilikan = 3;
				break;
			}

			//Status Kepemilikan
			switch (isset($param['ansadd']['4'])?$param['ansadd']['4']:'') {
				case '6_':
				$statuskepemilikan = 1;
				break;
				case '7_':
				$statuskepemilikan = 2;
				break;
				case '8_':
				$statuskepemilikan = 3;
				break;
				case '9_':
				$statuskepemilikan = 4;
				break;
				case '10_':
				$statuskepemilikan = 5;
				break;
				case '11_':
				$statuskepemilikan = 6;
				break;
			}

			 //Fungsi Bangunan
			switch (isset($param['ansadd']['5'])?$param['ansadd']['5']:'') {
				case '12_':
				$fungsibangunan = 1;
				break;
				case '13_':
				$fungsibangunan = 2;
				break;
				case '14_':
				$fungsibangunan = 3;
				break;
				case '15_':
				$fungsibangunan = 4;
				break;
				case '16_':
				$fungsibangunan = 5;
				break;
				case '17_':
				$fungsibangunan = 6;
				break;
				case '18_':
				$fungsibangunan = 7;
				break;
				case '19_':
				$fungsibangunan = 8;
				break;
				case '20_':
				$fungsibangunan = 9;
				break;
				case '21_':
				$fungsibangunan = 10;
				break;
				case '22_':
				$fungsibangunan = 11;
				break;
				case '23_':
				$fungsibangunan = 12;
				break;
				case '24_':
				$fungsibangunan = 13;
				break;
				case '25_':
				$fungsibangunan = 14;
				break;
			}

			//Klasifikasi Kompleksitas
			switch (isset($param['ansadd']['6'])?$param['ansadd']['6']:'') {
				case '26_':
				$klasifikasikompleksitas = 1;
				break;
				case '27_':
				$klasifikasikompleksitas = 2;
				break;
				case '28_':
				$klasifikasikompleksitas = 3;
				break;
			}
			
			//Risiko Kebakaran
			switch (isset($param['ansadd']['7'])?$param['ansadd']['7']:'') {
				case '29_':
				$resikokebakaran = 1;
				break;
				case '30_':
				$resikokebakaran = 2;
				break;
				case '31_':
				$resikokebakaran = 3;
				break;
			}

			//Zonasi Gempa
			switch (isset($param['ansadd']['8'])?$param['ansadd']['8']:'') {
				case '32_':
				$zonasigempa = 1;
				break;
				case '33_':
				$zonasigempa = 2;
				break;
				case '34_':
				$zonasigempa = 3;
				break;
				case '35_':
				$zonasigempa = 4;
				break;
				case '36_':
				$zonasigempa = 5;
				break;
				case '37_':
				$zonasigempa = 6;
				break;
			}

			// Lokasi (Kepadatan Bangunan Gedung)
			switch (isset($param['ansadd']['9'])?$param['ansadd']['9']:'') {
				case '38_':
				$kepadatanbangunan = 1;
				break;
				case '39_':
				$kepadatanbangunan = 2;
				break;
				case '40_':
				$kepadatanbangunan = 3;
				break;
			}

			//Waktu Penggunaan Bangunan
			switch (isset($param['ansadd']['10'])?$param['ansadd']['10']:'') {
				case '41_':
				$waktupenggunaan = 1;
				break;
				case '42_':
				$waktupenggunaan = 2;
				break;
				case '43_':
				$waktupenggunaan = 3;
				break;
			}

			//Tingkat Bangunan
			// $tingkatbangunan = 0;
			switch (isset($param['ansadd']['11'])?$param['ansadd']['11']:'') {
				case '44_':
				$tingkatbangunan = 1;
				break;
				case '45_':
				$tingkatbangunan = 2;
				break;
				case '46_':
				$tingkatbangunan = 3;
				break;
				case '47_':
				$tingkatbangunan = 4;
				break;
				case '48_':
				$tingkatbangunan = 5;
				break;
				case '49_':
				$tingkatbangunan = 6;
				break;
				case '50_':
				$tingkatbangunan = 7;
				break;
				case '51_':
				$tingkatbangunan = 8;
				break;
				case '52_':
				$tingkatbangunan = 9;
				break;
			}

			//Kontruksi Pembatas/Penahan/Pengaman
			switch (isset($param['ansadd']['12'])?$param['ansadd']['12']:'') {
				case '53_':
				$konspembatas = 1;
				break;
				case '54_':
				$konspembatas = 2;
				break;
				case '55_':
				$konspembatas = 3;
				break;
				case '56_':
				$konspembatas = 4;
				break;
			}

			//Konstruksi Penanda Masuk
			switch (isset($param['ansadd']['14'])?$param['ansadd']['14']:'') {
				case '57_':
				$konspenanda = 1;
				break;
				case '58_':
				$konspenanda = 2;
				break;
				case '59_':
				$konspenanda = 3;
				break;
			}

			//Konstruksi Penghubung
			switch (isset($param['ansadd']['15'])?$param['ansadd']['15']:'') {
				case '60_':
				$konspenghubung = 1;
				break;
				case '61_':
				$konspenghubung = 2;
				break;
				case '62_':
				$konspenghubung = 3;
				break;
			}

			//Konstruksi Kolam/ Reservoir Bawah Tanah
			switch (isset($param['ansadd']['16'])?$param['ansadd']['16']:'') {
				case '63_':
				$konskolam = 1;
				break;
				case '64_':
				$konskolam = 2;
				break;
				case '65_':
				$konskolam = 3;
				break;
				case '66_':
				$konskolam = 4;
				break;
			}

			//Konstruksi Menara
			switch (isset($param['ansadd']['17'])?$param['ansadd']['17']:'') {
				case '67_':
				$konsmenara = 1;
				break;
				case '68_':
				$konsmenara = 2;
				break;
				case '69_':
				$konsmenara = 3;
				break;
				case '70_':
				$konsmenara = 4;
				break;
			}

			//Konstruksi Monumen
			switch (isset($param['ansadd']['18'])?$param['ansadd']['18']:'') {
				case '71_':
				$konsmonumen = 1;
				break;
				case '72_':
				$konsmonumen = 2;
				break;
				case '73_':
				$konsmonumen = 3;
				break;
			}

			//Konstruksi Instalasi
			switch (isset($param['ansadd']['22'])?$param['ansadd']['22']:'') {
				case '83_':
				$konsinstalasi = 1;
				break;
				case '84_':
				$konsinstalasi = 2;
				break;
				case '85_':
				$konsinstalasi = 3;
				break;
				case '86_':
				$konsinstalasi = 4;
				break;
			}

			//Konstruksi Reklame/Papan Nama
			switch (isset($param['ansadd']['23'])?$param['ansadd']['23']:'') {
				case '87_':
				$konsreklame = 1;
				break;
				case '88_':
				$konsreklame = 2;
				break;
				case '89_':
				$konsreklame = 3;
				break;
				case '90_':
				$konsreklame = 4;
				break;
			}

			//Sumber Air
			$sumberair = '';
			$arrsa  = array( 1 => '91_', 2 => '92_', 3 =>'93_', 4 => '94_', 5 => '163_', 6 => '164_', 7 => '165_', 8 => '166_', 9 => '167_', 10 => '168_');
			
			$explsa = isset($param['ansadd']['24'])?explode('_', $param['ansadd']['24']):'';

			if(count($explsa)>1){
				$key1 = array_search($explsa[0], str_replace('_', '', array_values($arrsa)));
				$key2 = array_search($explsa[1], str_replace('_', '', array_values($arrsa)));
				$keys2 = ($key2 !="")?$key2+1:0;
				$sumberair = ($key1+1).','.$keys2;

			}else{
				switch (isset($param['ansadd']['24'])?$param['ansadd']['24']:'') {
					case '91_':
					$sumberair = 1;
					break;
					case '92_':
					$sumberair = 2;
					break;
					case '93_':
					$sumberair = 3;
					break;
					case '94_':
					$sumberair = 4;
					break;
					case '163_':
					$sumberair = 5;
					break;
					case '164_':
					$sumberair = 6;
					break;
					case '165_':
					$sumberair = 7;
					break;
					case '166_':
					$sumberair = 8;
					break;
					case '167_':
					$sumberair = 9;
					break;
					case '168_':
					$sumberair = 10;
					break;
				}
			}

			// //Perkerasan
			switch (isset($param['ansadd']['29'])?$param['ansadd']['29']:'') {
				case '103_':
				$perkerasan = 1;
				break;
				case '104_':
				$perkerasan = 2;
				break;
				case '105_':
				$perkerasan = 3;
				break;
				case '106_':
				$perkerasan = 4;
				break;
				case '107_':
				$perkerasan = 5;
				break;
				case '108_':
				$perkerasan = 6;
				break;
				case '109_':
				$perkerasan = 7;
				break;
			}

			// //Turap
			// $turap = 0;
			switch (isset($param['ansadd']['30'])?$param['ansadd']['30']:'') {
				case '110_':
				$turap = 1;
				break;
				case '111_':
				$turap = 2;
				break;
				case '112_':
				$turap = 3;
				break;
				case '113_':
				$turap = 4;
				break;
			}

			// //Bangunan Gedung
			// $bangunangedung = '';
			$arrbg  = array( 1 => '116_', 2 => '121_', 3 =>'126_', 4 => '131_', 5 => '134_', 6 => '117_', 7 => '118_', 8 => '119_', 9 =>'120_', 10 => '122_', 11 => '123_', 12 => '124_', 13 => '125_', 14 => '127_', 15 => '128_', 16 => '129_', 17 => '130_', 18 => '132_', 19 => '133_', 20 => '135_', 21 => '136_');
			
			$explbg = isset($param['ansadd']['32'])?explode('_', $param['ansadd']['32']):'';

			if(count($explbg)>1){
				$keybg1 = array_search($explbg[0], str_replace('_', '', array_values($arrbg)));
				$keybg2 = array_search($explbg[1], str_replace('_', '', array_values($arrbg)));
				$keysbg2 = ($keybg2 !="")?$keybg2+1:0;
				$bangunangedung = ($keybg1+1).','.$keysbg2;

			}else{
				switch (isset($param['ansadd']['32'])?$param['ansadd']['32']:'') {
					case '116_':
					$bangunangedung = 1;
					break;
					case '121_':
					$bangunangedung = 2;
					break;
					case '126_':
					$bangunangedung = 3;
					break;
					case '131_':
					$bangunangedung = 4;
					break;
					case '134_':
					$bangunangedung = 5;
					break;
					case '117_':
					$bangunangedung = 6;
					break;
					case '118_':
					$bangunangedung = 7;
					break;
					case '119_':
					$bangunangedung = 8;
					break;
					case '120_':
					$bangunangedung = 9;
					break;
					case '122_':
					$bangunangedung = 10;
					break;
					case '123_':
					$bangunangedung = 11;
					break;
					case '124_':
					$bangunangedung = 12;
					break;
					case '125_':
					$bangunangedung = 13;
					break;
					case '127_':
					$bangunangedung = 14;
					break;
					case '128_':
					$bangunangedung = 15;
					break;
					case '129_':
					$bangunangedung = 16;
					break;
					case '130_':
					$bangunangedung = 17;
					break;
					case '132_':
					$bangunangedung = 18;
					break;
					case '133_':
					$bangunangedung = 19;
					break;
					case '135_':
					$bangunangedung = 20;
					break;
					case '136_':
					$bangunangedung = 21;
					break;
				}
			}

			//Bangunan Pagar
			$arrbp  = array( 1 => '137_', 2 => '143_', 3 =>'138_', 4 => '139_', 5 => '140_', 6 => '141_', 7 => '142_', 8 => '144_', 9 =>'145_', 10 => '146_', 11 => '147_', 12 => '148_');
			
			$explbp = isset($param['ansadd']['33'])?explode('_', $param['ansadd']['33']):'';

			if(count($explbp)>1){
				$keybp1 = array_search($explbp[0], str_replace('_', '', array_values($arrbp)));
				$keybp2 = array_search($explbp[1], str_replace('_', '', array_values($arrbp)));
				$keysbp2 = ($keybp2 !="")?$keybp2+1:0;
				$bangunanpagar = ($keybp1+1).','.$keysbp2;

			}else{
				switch (isset($param['ansadd']['33'])?$param['ansadd']['33']:'') {
					case '137_':
					$bangunanpagar = 1;
					break;
					case '143_':
					$bangunanpagar = 2;
					break;
					case '138_':
					$bangunanpagar = 3;
					break;
					case '139_':
					$bangunanpagar = 4;
					break;
					case '140_':
					$bangunanpagar = 5;
					break;
					case '141_':
					$bangunanpagar = 6;
					break;
					case '142_':
					$bangunanpagar = 7;
					break;
					case '144_':
					$bangunanpagar = 8;
					break;
					case '145_':
					$bangunanpagar = 9;
					break;
					case '146_':
					$bangunanpagar = 10;
					break;
					case '147_':
					$bangunanpagar = 11;
					break;
					case '148_':
					$bangunanpagar = 12;
					break;
				}
			}

			//Bangunan Pabrik
			$arrbpr  = array( 1 => '149_', 2 => '153_', 3 =>'157_', 4 => '150_', 5 => '151_', 6 => '152_', 7 => '154_', 8 => '155_', 9 =>'156_', 10 => '158_', 11 => '159_', 12 => '160_');
			
			$explbpr = isset($param['ansadd']['34'])?explode('_', $param['ansadd']['34']):'';

			if(count($explbpr)>1){
				$keybpr1 = array_search($explbpr[0], str_replace('_', '', array_values($arrbpr)));
				$keybpr2 = array_search($explbpr[1], str_replace('_', '', array_values($arrbpr)));
				$keysbr2 = ($keybpr2 !="")?$keybpr2+1:0;
				$bangunanpabrik = ($keybpr1+1).','.$keysbr2;

			}else{
				switch (isset($param['ansadd']['34'])?$param['ansadd']['34']:'') {
					case '149_':
					$bangunanpabrik = 1;
					break;
					case '153_':
					$bangunanpabrik = 2;
					break;
					case '157_':
					$bangunanpabrik = 3;
					break;
					case '150_':
					$bangunanpabrik = 4;
					break;
					case '151_':
					$bangunanpabrik = 5;
					break;
					case '152_':
					$bangunanpabrik = 6;
					break;
					case '154_':
					$bangunanpabrik = 7;
					break;
					case '155_':
					$bangunanpabrik = 8;
					break;
					case '156_':
					$bangunanpabrik = 9;
					break;
					case '158_':
					$bangunanpabrik = 10;
					break;
					case '159_':
					$bangunanpabrik = 11;
					break;
					case '160_':
					$bangunanpabrik = 12;
					break;
				}
			}

		
		$data['status'] 	= $_this->status[1];
		$kec_id = $kecamatan;
		$kel_id = $kelurahan;
		$newnum = "";
		$getlastidentity = $_this->responden_model->get_last_identity($kec_id,$kel_id)->row_array();
		if(count($getlastidentity)>0){
			$lastnum = $getlastidentity['data_nomor_identitas'];
			$expl = explode('.', $lastnum);
			$newnum = str_replace($expl[4], str_pad(($expl[4]+1), 8, "0", STR_PAD_LEFT), $lastnum);
		}else{
			$lastnum = '32.'.$kabkota.'.'.$kecamatan.'.'.$kelurahan.'.00000000';
			$expl = explode('.', $lastnum);
			$newnum = str_replace($expl[4], str_pad(($expl[4]+1), 8, "0", STR_PAD_LEFT), $lastnum);
		}
		
		$resdata = array(
			'data_nomor_identitas' => $newnum,
			'data_qrcode' => $qrcode,
			'data_map_latitude' => $loc[0],
			'data_map_longitude' => $loc[1],
			'data_responden_nik' => $nik,
			'data_responden_nama' => $nama,
			'data_responden_alamat' => $alamat,
			'data_responden_rt' => $rt,
			'data_responden_rw' => $rw,
			'data_responden_kabkota_id' => $kabkota,
			'data_responden_kecamatan_id' => $kecamatan,
			'data_responden_kelurahan_id' => $kelurahan,
			'data_fungsi_luas_tanah' => $luastanah,
			'data_fungsi_luas_bangunan' => $luasbangunan,
			'data_fungsi_kepemilikan' =>$kepemilikan,
			'data_fungsi_status_kepemilikan' =>$statuskepemilikan,
			'data_fungsi_kelompok_fungsi_bangunan' =>$fungsibangunan,
			'data_fungsi_tingkat_bangunan' =>$tingkatbangunan,
			'data_fungsi_klasifikasi_kompleksitas' =>$klasifikasikompleksitas,
			'data_fungsi_resiko_kebakaran' =>$resikokebakaran,
			'data_fungsi_zonasi_gempa' =>$zonasigempa,
			'data_fungsi_kepadatan_bangunan' =>$kepadatanbangunan,
			'data_fungsi_waktu_penggunaan_bangunan' => $waktupenggunaan,
			'data_fasilitas_konstruksi_pembatas' =>$konspembatas,
			'data_fasilitas_penanda_masuk' =>$konspenanda,
			'data_fasilitas_penghubung' =>$konspenghubung,
			'data_fasilitas_konstruksi_kolam' =>$konskolam,
			'data_fasilitas_konstruksi_menara' =>$konsmenara,
			'data_fasilitas_konstruksi_monumen' =>$konsmonumen,
			'data_fasilitas_konstruksi_instalasi' =>$konsinstalasi,
			'data_fasilitas_konstruksi_reklame' =>$konsreklame,
			'data_fasilitas_daya_listrik' =>null,
			'data_fasilitas_telepon' =>null,
			'data_fasilitas_sumber_air' =>$sumberair,
			'data_fasilitas_septictank' =>($param['ansadd']['25']=='95_')?1:2,
			'data_fasilitas_bangunan_carport' =>($param['ansadd']['26']=='97_')?1:2,
			'data_fasilitas_teras' =>($param['ansadd']['27']=='99_')?1:2,
			'data_fasilitas_saluran_air' =>($param['ansadd']['28']=='101_')?1:2,
			'data_fasilitas_bangunan_kolam_renang' =>null,
			'data_fasilitas_bangunan_kolam_ikan' =>null,
			'data_fasilitas_konstruksi_perkerasan' =>$perkerasan,
			'data_fasilitas_bangunan_turap' =>$turap,
			'data_fasilitas_bangunan_water_turn' =>null,
			'data_fasilitas_bangunan_tangki_cairan' =>($param['ansadd']['31']=='114_')?1:2,
			'data_klasifikasi_bangunan_gedung' =>$bangunangedung,
			'data_klasifikasi_bangunan_pabrik' =>$bangunanpabrik,
			'data_klasifikasi_bangunan_pagar' =>$bangunanpagar,
			'data_adm_imb' => $imb,
			'data_adm_noimb' => $noimb,
			'data_adm_nop' => $nop,
			'data_adm_njop' => $njop,
			'data_adm_thn_berdiri_bangunan' => $thnbangun,
			'data_adm_thn_renovasi_bangunan' => $thnrenov,
			'data_user_input' => $param['user_id'],
			'data_dateinput' => date('Y-m-d H:i:s'),
			'data_tahun_input' => date('Y'),
			'data_status' => 1
			);
		
		$jmlqrcode = $_this->responden_model->getCountQRCode($param['qrcode'])->row_array();
		if($jmlqrcode['jmlqrcode']==0){
			$html = $_this->responden_model->insert_responden($resdata);
			foreach($ansadd as $kuisoner_id => $pilihan_id_ext) {
			$pil_id = explode('_', $pilihan_id_ext);
			// echo count($pil_id);print_r();
			$pilihan_id = '';
			if(count($pil_id)>1 && $pil_id[1] !=''){
				$pilihan_id = $pil_id[0].','.$pil_id[1];
			}else{
				$pilihan_id = $pil_id[0];
			}
			// echo '<pre>';print_r($pil_id);die;
			$pilihan_other = str_replace($pilihan_id.'_', '', $pilihan_id_ext);
			$datakuisoner = array(
				'data_kuisoner_data_id'				=> $html,
				'data_kuisoner_kuisoner_id'			=> $kuisoner_id,
				'data_kuisoner_pilihan_id'			=> $pilihan_id, //(count(explode(',', $pilihan_id))>1)?$pilihan_id:str_replace(',', '', $pilihan_id),
				'data_kuisoner_jawaban_esay'		=> $pilihan_id,
				'data_kuisoner_pilihan_tambahan'	=> $pilihan_other,
				'data_kuisoner_status'				=> 1
				);
			
			// echo '<pre>';print_r($datakuisoner);
			$_this->responden_model->insert_kuisoner_data($datakuisoner);
		}

		$arrEssay = array('0' => $luastanah,'1' => $luasbangunan);
		//$ltlb = array(0,1);
		//for ($x=0; $x < 2; $x++) { 
		$ltlb = array(0,1,2);
		for ($x=0; $x < count($ltlb)-1; $x++) { 
			$_this->responden_model->updateJawabankuisioner($html, $ltlb[($x+1)],$arrEssay[$x]);
		}
		$data['status'] 	  = $_this->status[1];
		$data['message'] 	  = 'Data Responden atas Nama '.$nama.' dengan QRCODE '.$param['qrcode'].' berhasil';
		//$data['resdata'] 	  = $resdata;
		//$data['datakuisoner'] = $datakuisoner;
		}else{
			$data = array('status'=>$_this->status[0],'message'=>'Data Responden dengan QRCODE '.$param['qrcode'].'  sudah ada, silahkan coba lagi!.');
		}
	}else{
		$data = array('status'=>$_this->status[0],'message'=>'Data Responden Kosong, silahkan coba lagi!.');
	}

	return $data;
}

function saveImageResponden($_this, $param){
	$_this->load->model('responden_model', '', TRUE);
	$_this->load->model('auth_api_model','',TRUE);
	$data 				= array();
 	$dataphoto          = array();

 	$filename = '';
 	// $data = array('status'=>0,'message'=>'Error, para');

 	if(count($param)>2){
	 	if(isset($_FILES['uploadedfile'])){
	 		$getNoId 	= $_this->responden_model->getNoIdentitas($param['respondenId'])->row_array();
	 		$nomor   	= isset($getNoId['data_nomor_identitas'])?$getNoId['data_nomor_identitas']."/":"";
	 		$targetpath = "./assets/default/img/responden/".$nomor;
	 		
	 		if(!file_exists($targetpath)){
				mkdir("./assets/default/img/responden/".$nomor, 0777, true);
	 			// $target_dir  = "./assets/default/img/responden/".$nomor;
	 		}

			$target_file = $targetpath . basename($_FILES["uploadedfile"]["name"]);
			if (move_uploaded_file($_FILES["uploadedfile"]["tmp_name"], $target_file)) {

	 			// $count = count(basename($_FILES["uploadedfile"]["name"]));
	 			$dataphoto = array(
					'photo_data_id' => $param['respondenId'],
					'photo_image'   => basename($_FILES["uploadedfile"]["name"]),
					'photo_status'  => 1
				);
	 			$_this->responden_model->tambahphotosingle($dataphoto);

				$data = array('status'=>1, 'message'=>'Success, Gambar Responden berhasil diupload');
			}
		}else{
			$data = array('status'=>0,'message'=>'Error, Gambar Responden tidak bisa diupload, silahkan coba lagi!');
		}
	}else{
		$data = array('status'=>0,'message'=>'Error, parameter ada yang kurang');
	}

	return $data;
}
