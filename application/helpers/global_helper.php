<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

function dateEnToId($string='', $format='') { 
	$LongMonth = array(
		'en' => array('January','February','March','April','May','June','July','August','September','October','November','December'),
		'id' => array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember')
	);

	$shortMonthId = array(
		'en' => array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'),
		'id' => array('Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agu','Sept','Okt','Nov','Des')
	);
	$newDate = strtotime($string);
	$newDate = date($format, $newDate);
	$newDate = str_replace($LongMonth['en'], $LongMonth['id'], $newDate);
	$newDate = str_replace($shortMonthId['en'], $shortMonthId['id'], $newDate);

	return formatDate($newDate);
}
function dateEnToIdFullDay($string='', $format='') { 
	
	$LongDay = array(
		'en' => array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'),
		'id' => array('Senin','Selasa','Rabu','Kamis','Jum\'at','Sabtu','Minggu')
	);

	$shortDayId = array(
		'en' => array('Mon','Tue','Wed','Thu','Fri','Sat','Sun'),
		'id' => array('Sen','Sel','Rab','Kam','Jum','Sab','Min')
	);

	$LongMonth = array(
		'en' => array('January','February','March','April','May','June','July','August','September','October','November','December'),
		'id' => array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember')
	);

	$shortMonthId = array(
		'en' => array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'),
		'id' => array('Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agu','Sept','Okt','Nov','Des')
	);
	$newDate = strtotime($string);
	$newDate = date($format, $newDate);
	$newDate = str_replace($LongMonth['en'], $LongMonth['id'], $newDate);
	$newDate = str_replace($shortMonthId['en'], $shortMonthId['id'], $newDate);
	$newDate = str_replace($LongDay['en'], $LongDay['id'], $newDate);
	$newDate = str_replace($shortDayId['en'], $shortDayId['id'], $newDate);

	return $newDate;
}
function make_url($string){
	return str_replace(' ', '-', preg_replace('/\s\s+/', ' ',strtolower(preg_replace('/[^a-z_\-0-9 ]/i', '', preg_replace('/\s\s+/', ' ', strip_tags($string))))));
}
function parseCurrency($value) {
	if ( intval($value) == $value ) {
		$return = number_format($value, 0, ",", ".");
	}
	else {
		$return = number_format($value, 2, ",", ".");
		$return = rtrim($return, 0);
	}

	return $return;
}
function toNum($num) {
	$list=array('A' => 0,
		'B' => 1,
		'C' => 2,
		'D' => 3,
		'E' => 4,
		'F' => 5,
		'G' => 6,
		'H' => 7,
		'I' => 8,
		'J' => 9);
	$temp='';
	$arr_num=str_split ($num);
	foreach($arr_num as $data)
	{
		$temp.=array_search($data,$list);
	}
	$num=$temp;
	return $num;
}
function timeToText($string) {
	$result = '';
	$newDate = strtotime($string);
	if(date('Y-m-d', time())==date('Y-m-d', $newDate)) {
		$rem = time()-$newDate;
		if($rem<300) {
			$result = 'Beberapa menit yang lalu';
		} else {
			$hasil = '';
			$time = floor($rem/60);
			$jam = floor($time/60);
			$menit = $time-($jam*60);
			if($jam>0) {
				$hasil = $jam.' jam';
			}
			if($menit>0) {
				$hasil .= ($hasil!=''?' ':'').$menit.' menit';
			}
			if($hasil!='') {
				$result = $hasil.' yang lalu';
			} else {
				$result = '---';
			}
		}
	} else {
		if(date('Y', time())==date('Y', $newDate)) {
			$result = dateEnToId($string, 'd F').' jam '.dateEnToId($string, 'H:i');
		} else {
			$result = dateEnToId($string, 'd F Y').' jam '.dateEnToId($string, 'H:i');
		}
	}
	return $result;
}
function timeToTextNotif($string) {
	$result = '';
	$newDate = strtotime($string);
	if(date('Y-m-d', time())==date('Y-m-d', $newDate)) {
		$rem = time()-$newDate;
		if($rem<300) {
			$result = '0 Menit';
		} else {
			$hasil = '';
			$time = floor($rem/60);
			$jam = floor($time/60);
			$menit = $time-($jam*60);
			if($jam>0) {
				$hasil = $jam.' Jam';
			}
			if($menit>0) {
				$hasil .= ($hasil!=''?' ':''.$menit.' Menit');
			}
			if($hasil!='') {
				$result = $hasil.'';
			} else {
				$result = '---';
			}
		}
	} else {
		if(date('Y', time())==date('Y', $newDate)) {
			$result = dateEnToId($string, 'd F').'';
		} else {
			$result = dateEnToId($string, 'd F Y').'';
		}
	}
	return $result;
}
function konv_bulan($bulan){
		if($bulan>=12) $hsl="DESEMBER";
	    elseif($bulan>=11) $hsl="NOVEMBER";
	    elseif($bulan>=10) $hsl="OKTOBER";
	    elseif($bulan>=9) $hsl="SEPTEMBER";
	    elseif($bulan>=8) $hsl="AGUSTUS";
	    elseif($bulan>=7) $hsl="JULI";
	    elseif($bulan>=6) $hsl="JUNI";
	    elseif($bulan>=5) $hsl="MEI";
	    elseif($bulan>=4) $hsl="APRIL";
	    elseif($bulan>=3) $hsl="MARET";
	    elseif($bulan>=2) $hsl="FEBRUARI";
	    elseif($bulan>=1) $hsl="JANUARI";
	    else $hsl="Tidak ada Target";
	    return $hsl;
	}
function konv_bulan_cap($bulan){
	if($bulan>=12) $hsl="Desember";
    elseif($bulan>=11) $hsl="November";
    elseif($bulan>=10) $hsl="Oktober";
    elseif($bulan>=9) $hsl="September";
    elseif($bulan>=8) $hsl="Agustus";
    elseif($bulan>=7) $hsl="Juli";
    elseif($bulan>=6) $hsl="Juni";
    elseif($bulan>=5) $hsl="Mei";
    elseif($bulan>=4) $hsl="April";
    elseif($bulan>=3) $hsl="Maret";
    elseif($bulan>=2) $hsl="Februari";
    elseif($bulan>=1) $hsl="Januari";
    else $hsl="Tidak ada Target";
    return $hsl;
}
function passwordEncoder($text) {
	$first = md5($text);
	$second = '';
	for($i=0;$i<strlen($first);++$i) {
		$second .= ($i%2==0)?substr($first, $i,1):'';
	}
	$third = base64_encode($second);
	return $first;
}
function converToRupiahWithDot($rupiah) {
	$newRupiah = $rupiah*1;
	$jumlahDecimal = strlen(substr(strrchr($newRupiah, "."), 1));
	$result = number_format($newRupiah,$jumlahDecimal,'.',',');
	return $result;
}
function hitungcuti($tglawal,$tglakhir,$delimiter) {
        $ci =& get_instance();
        $tgl_awal = $tgl_akhir = $minggu = $sabtu = $koreksi = $libur = 0;

        $libur = $ci->db->query("SELECT hari_libur_tanggal,hari_libur_jumlah_hari FROM ms_hari_libur WHERE hari_libur_status = 1 AND hari_libur_tanggal >= '".dateEnToId($tglawal, 'Y-m-d')."' AND hari_libur_tanggal <= '".dateEnToId($tglakhir,'Y-m-d')."'")->result_array();
            foreach ($libur as $l) {
                $libur_n[] = ($l['hari_libur_jumlah_hari']);
            }
        if(count($libur)>0){
            $liburnasional = array_sum($libur_n);
        }else{
            $liburnasional = 0;
        }
        $pecah_tglawal = explode($delimiter, $tglawal);
        $pecah_tglakhir = explode($delimiter, $tglakhir);
        $tgl_awal = gregoriantojd($pecah_tglawal[1], $pecah_tglawal[0], $pecah_tglawal[2]);
        $tgl_akhir = gregoriantojd($pecah_tglakhir[1], $pecah_tglakhir[0], $pecah_tglakhir[2]);
        $jmldetik = 24*3600;
        $a = strtotime($tglawal);
        $b = strtotime($tglakhir);
        for($i=$a; $i<$b; $i+=$jmldetik){
            if(date("w",$i)=="0"){
                $minggu++;
            }
        }
        for($i=$a; $i<$b; $i+=$jmldetik){
            if(date("w",$i)=="6"){
                $sabtu++;
            }
        }
        if(date("w",$b)=="0" || date("w",$b)=="6"){
            $koreksi = 1;
        }
        $hitungcuti =  (float)$tgl_akhir - (float)$tgl_awal - (float)$liburnasional - (float)$minggu - (float)$sabtu - (float)$koreksi + 1;
        return $hitungcuti;
    }

    function konv_nilai_hasil_format($nilai){
    	if($nilai>=91) $hsl="<span class='label label-sm label-success'>Sangat baik</span>";
    	elseif($nilai>=76) $hsl="<span class='label label-sm label-success'>Baik</span>";
    	elseif($nilai>=61) $hsl="<span class='label label-sm label-warning'>Cukup</span>";
    	elseif($nilai>=51) $hsl="<span class='label label-sm label-danger'>Kurang</span>";
    	else $hsl="<span class='label label-sm label-danger'>Buruk</span>";
    	return $hsl;
    }

    function new_pangkat($old_pangkat)
		{
		  if ($old_pangkat=="1A") $hsl="1B";
		  elseif ($old_pangkat=="1B") $hsl="1C";
		  elseif ($old_pangkat=="1C") $hsl="1D";
		  elseif ($old_pangkat=="1D") $hsl="2A";
		  elseif ($old_pangkat=="2A") $hsl="2B";
		  elseif ($old_pangkat=="2B") $hsl="2C";
		  elseif ($old_pangkat=="2C") $hsl="2D";
		  elseif ($old_pangkat=="2D") $hsl="3A";
		  elseif ($old_pangkat=="3A") $hsl="3B";
		  elseif ($old_pangkat=="3B") $hsl="3C";
		  elseif ($old_pangkat=="3C") $hsl="3D";
		  elseif ($old_pangkat=="3D") $hsl="4A";
		  elseif ($old_pangkat=="4A") $hsl="4B";
		  elseif ($old_pangkat=="4B") $hsl="4C";
		  elseif ($old_pangkat=="4C") $hsl="4D";
		  elseif ($old_pangkat=="4D") $hsl="4E";
		  else $hsl="-";
		  return $hsl;
		}
	function konversi_gol($gol)
		{
		   if ($gol=='1A') {$hsl='I/a'; }
		   elseif ($gol=='1B') { $hsl='I/b'; }
		   elseif ($gol=='1C') { $hsl='I/c'; }
		   elseif ($gol=='1D') { $hsl='I/d'; }
		   elseif ($gol=='2A') { $hsl='II/a'; }
		   elseif ($gol=='2B') { $hsl='II/b'; }
		   elseif ($gol=='2C') { $hsl='II/c'; }
		   elseif ($gol=='2D') { $hsl='II/d'; }
		   elseif ($gol=='3A') { $hsl='III/a'; }
		   elseif ($gol=='3B') { $hsl='III/b'; }
		   elseif ($gol=='3C') { $hsl='III/c'; }
		   elseif ($gol=='3D') { $hsl='III/d'; }
		   elseif ($gol=='4A') { $hsl='IV/a'; }
		   elseif ($gol=='4B') { $hsl='IV/b'; }
		   elseif ($gol=='4C') { $hsl='IV/c'; }
		   elseif ($gol=='4D') { $hsl='IV/d'; }
		   elseif ($gol=='4E') { $hsl='IV/e'; }
		   else { $hsl='-'; }
		  return $hsl;
		}
		function konversi_gol_lengkap($dt) {
			$ci = get_instance();
		   	$hsl="";
		   	$qryf="SELECT * from ms_golongan WHERE golongan_kode='".$dt."'";
		   	$ohslf=$ci->db->query($qryf);
		   	foreach ($ohslf->result_array() as $dhslf) {
	         	$hsl=strtoupper($dhslf['golongan_pangkat']).' - '.$dhslf['golongan_nama'];
		   	}
		   	return $hsl;
		}
		function konversi_esel($dt) {
			$ci = get_instance();
		   	$hsl="";
		   	$qryf="SELECT * from ms_eselon WHERE eselon_kode='".$dt."'";
		   	$ohslf=$ci->db->query($qryf);
		   	foreach ($ohslf->result_array() as $dhslf) {
	         	$hsl=strtoupper($dhslf['eselon_nama']);
		   	}
		   	return $hsl;
		}
		function encrypt_url($string) {
			$tgl = date("Ydm");
			$key = "aplabsensi" . $tgl; //key to encrypt and decrypts.
			$result = '';
		//    $test = "";
			for ($i = 0; $i < strlen($string); $i++) {
				$char = substr($string, $i, 1);
				$keychar = substr($key, ($i % strlen($key)) - 1, 1);
				$char = chr(ord($char) + ord($keychar));
		
		//        $test[$char] = ord($char) + ord($keychar);
				$result .= $char;
			}
		
			return urlencode(base64_encode($result));
		}

	function hitung_wkt_biaya_nilai($dtt,$dtr,$hit){
		$nil_ef=100-($dtr/$dtt*100);
	  	if($nil_ef>24)$nil = 76-($hit-100);
	        else $nil = $hit;

	        $hsl = $nil;
	        return $hsl;
	}

	function hitung_skp_baris($ary_dt)
	{
	    $hsl = array();

	    $nil1 = ($ary_dt['t_ko'] == 0) ? 0 : ($ary_dt['r_ko'] / $ary_dt['t_ko']) * 100;
	    $nil2 = ($ary_dt['t_km'] == 0) ? 0 : ($ary_dt['r_km'] / $ary_dt['t_km']) * 100;

	    if ($ary_dt['r_wkt'] > 0) {
	        $nil3 = $ary_dt['t_wkt'] == 0 ? 0 : (((1.76 * $ary_dt['t_wkt']) - ($ary_dt['r_wkt'])) / $ary_dt['t_wkt']) * 100;
	        $nil3 = hitung_wkt_biaya_nilai($ary_dt['t_wkt'], $ary_dt['r_wkt'], $nil3);
	    } else $nil3 = 0;

	    if ($ary_dt['t_biaya']) {
	        $pembagi = 4;
	        if ($ary_dt['r_wkt'] > 0) {
	            $nil4 = ($ary_dt['t_biaya'] == 0) ? 0 : (((1.76 * $ary_dt['t_biaya']) - ($ary_dt['r_biaya'])) / $ary_dt['t_biaya']) * 100;
	            $nil4 = hitung_wkt_biaya_nilai($ary_dt['t_biaya'], $ary_dt['r_biaya'], $nil4);
	        } else $nil4 = 0;
	    } else {
	        $nil4 = 0;
	        $pembagi = 3;
	    }
	    $hsl['tot_nilai'] = $nil1 + $nil2 + $nil3 + $nil4;
	    $hsl['rata_nilai'] = $hsl['tot_nilai'] / $pembagi;
	    return $hsl;
	}

	function konv_nilai_hasil($nilai){
	    if($nilai>=91) $hsl="Sangat baik";
	    elseif($nilai>=76) $hsl="Baik";
	    elseif($nilai>=61) $hsl="Cukup";
	    elseif($nilai>=51) $hsl="Kurang";
	    elseif($nilai>=0) $hsl="Buruk";
	    else $hsl="Tidak ada Target";
	    return $hsl;
	}

	function banding_nilai_tambahan($dt)
	{
	    if ($dt >= 7) $hsl = 3;
	    elseif ($dt >= 4) $hsl = 2;
	    elseif ($dt >= 1) $hsl = 1;
	    else $hsl = 0;
	    return $hsl;
	}


	function get_nilai_tambahan($dt)
	{
	   	$ci =& get_instance();

	    $hsl = 0;
	    $qryf = "SELECT COUNT ( * ) jml FROM trx_skp_tugas_tambahan a 
				WHERE
				a.skp_tugas_tambahan_status = 1
				AND a.skp_tugas_tambahan_skp_header_id = ".$dt."";
	    $ohslf = $ci->db->query($qryf);
	    $jml =array();
	    foreach ($ohslf->result_array() as $dhslf) {
	        $jml = $dhslf['jml'];
	    }
	    $hsl = banding_nilai_tambahan($jml);
	    return $hsl;
	}

	function get_nilai_kreativitas($dt)
	{
	  	 $ci =& get_instance();
	    $hsl = 0;
	    $qryf = "SELECT SUM ( a.skp_kreativitas_nilai ) jml FROM trx_skp_kreativitas a 
				WHERE
				a.skp_kreativitas_skp_header_id=".$dt."";
	    $ohslf = $ci->db->query($qryf);
	    $jml =array();
	    foreach ($ohslf->result_array() as $dhslf) {
	        $jml = $dhslf['jml'];
	    }
	    $hsl = $jml;
	    return $hsl;
		}

	function get_semester_perilaku360($vbln)
	{
	    if ($vbln < 7) $nhsl = 2;
	    else $nhsl = 1;
	    return $nhsl;
	}

	function formatDate($format='') { 
		if($format == '01-01-1970' || $format == '01-01-1900'){
			$date = '-';
		}else{
			$date = $format;
		}
		return $date;
	}

	function count_cuti_tahunan($pegawaiid,$tahun) {
		$ci = get_instance();
	   	$hsl="";
	   	$qryf="SELECT COUNT(0) AS jml FROM dt_cuti WHERE iddatadasar = '".$pegawaiid."' AND YEAR(tgl_start) = '".$tahun."' AND jns_cuti = 1";
	   	$hsl=$ci->db->query($qryf)->row_array();
	   	return $hsl['jml'];
	}

	function count_cuti_besar($pegawaiid,$tahun) {
		$ci = get_instance();
	   	$hsl="";
	   	$qryf="SELECT COUNT(0) AS jml FROM dt_cuti WHERE iddatadasar = '".$pegawaiid."' AND YEAR(tgl_start) = '".$tahun."' AND jns_cuti = 2";
	   	$hsl=$ci->db->query($qryf)->row_array();
	   	return $hsl['jml'];
	}

	function count_cuti_sakit($pegawaiid,$tahun) {
		$ci = get_instance();
	   	$hsl="";
	   	$qryf="SELECT COUNT(0) AS jml FROM dt_cuti WHERE iddatadasar = '".$pegawaiid."' AND YEAR(tgl_start) = '".$tahun."' AND jns_cuti = 3";
	   	$hsl=$ci->db->query($qryf)->row_array();
	   	return $hsl['jml'];
	}	

	function count_cuti_ap($pegawaiid,$tahun) {
		$ci = get_instance();
	   	$hsl="";
	   	$qryf="SELECT COUNT(0) AS jml FROM dt_cuti WHERE iddatadasar = '".$pegawaiid."' AND YEAR(tgl_start) = '".$tahun."' AND jns_cuti = 4";
	   	$hsl=$ci->db->query($qryf)->row_array();
	   	return $hsl['jml'];
	}

	function count_cuti_bersalin($pegawaiid,$tahun) {
		$ci = get_instance();
	   	$hsl="";
	   	$qryf="SELECT COUNT(0) AS jml FROM dt_cuti WHERE iddatadasar = '".$pegawaiid."' AND YEAR(tgl_start) = '".$tahun."' AND jns_cuti = 5";
	   	$hsl=$ci->db->query($qryf)->row_array();
	   	return $hsl['jml'];
	}		

	function count_cuti_all($pegawaiid,$tahun) {
		$ci = get_instance();
	   	$hsl="";
	   	$qryf="SELECT COUNT(0) AS jml FROM dt_cuti WHERE iddatadasar = '".$pegawaiid."' AND YEAR(tgl_start) = '".$tahun."'";
	   	$hsl=$ci->db->query($qryf)->row_array();
	   	return $hsl['jml'];
	}		

?>