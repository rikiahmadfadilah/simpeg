<?php
class Ganti_password_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function getPassword($bahasa_id = '') {
		$this->db->select('*');
		if($bahasa_id!=""){
			$this->db->where('bahasa_id', $bahasa_id);
		}
		return $this->db->get('ms_user');
	}
	public function checkPasswordLama($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('ID', $pegawai_id);
		return $this->db->get('view_sys_users');
	}
	public function updatePassword($dataupdate, $user_id){
		$this->db->where('user_id', $user_id);
		return $this->db->update('ms_user', $dataupdate);
	}
}