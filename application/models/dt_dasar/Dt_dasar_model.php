<?php
class Dt_dasar_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	
	function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}

		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('view_dt_dasar',$limit,$offset);
		}else{
			return $this->db->get('view_dt_dasar');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}

		// if($status ='pegawai_status = 1 AND pegawai_is_non_pns = 1 AND pegawai_is_pns = 1'){
		// 	$this->db->where($status);
		// }
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('view_dt_dasar');
		return $this->db->count_all_results(); 
	}
	function getPegawai($id){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		$this->db->where('ID', $id);
		return $this->db->get('dt_dasar');
	}
	function getPegawaiAtasan($id){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		$this->db->where('IS_PNS', 1);
		return $this->db->get('view_dt_dasar');
	}
	function getDetailPegawai($id){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		$this->db->where('ID', $id);
		return $this->db->get('view_dt_dasar');
	}
	function getPegawaiByNip($id){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		$this->db->where('NIP', $id);
		return $this->db->get('dt_dasar');
	}
	public function insert_dt_dasar($datacreate){
		$this->db->insert('dt_dasar', $datacreate);
		return $this->db->insert_id();
	}
	function update_dt_dasar($dataupdate, $id){
		$this->db->where('ID', $id);
		return $this->db->update('dt_dasar', $dataupdate);
	}
	function getJenisKelamin(){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		return $this->db->get('dt_jenis_kelamin');
	}
	function getStatusKawin(){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		return $this->db->get('tabel_status_kawin');
	}
	function getAgama(){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		return $this->db->get('dt_agama');
	}
	function getGolDarah(){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		return $this->db->get('dt_gol_darah');
	}
	function getProvinsi(){
		$this->db->select('*');
		$this->db->order_by('PROVINSI', 'asc');
		$this->db->where('STATUS', 1);
		return $this->db->get('tabel_provinsi');
	}
	public function getKota($KODE_PROVINSI) {
		$this->db->select('*');
		$this->db->order_by('KOTA', 'asc');
		$this->db->where('KODE_PROVINSI', $KODE_PROVINSI);
		$this->db->where('STATUS', 1);
		return $this->db->get('tabel_kota');
	}
	public function getKecamatan($KODE_KOTA) {
		$this->db->select('*');
		$this->db->order_by('KECAMATAN', 'asc');
		$this->db->where('KODE_KOTA', $KODE_KOTA);
		$this->db->where('STATUS', 1);
		return $this->db->get('tabel_kecamatan');
	}
	public function getKelurahan($KODE_KECAMATAN) {
		$this->db->select('*');
		$this->db->order_by('KELURAHAN', 'asc');
		$this->db->where('KODE_KECAMATAN', $KODE_KECAMATAN);
		$this->db->where('STATUS', 1);
		return $this->db->get('tabel_kelurahan');
	}
	function getGolongan(){
		$this->db->select('*');
		// $this->db->where('STATUS', 1);
		return $this->db->get('tabel_gol');
	}
	function getKategoriSk(){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		return $this->db->get('tabel_kategori_sk');
	}
	function getJenisPeg(){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		return $this->db->get('tabel_jenis_peg');
	}
	function getKategoriHukdis(){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		return $this->db->get('tabel_kategori_hukdis');
	}
	function getPendidikan(){
		$this->db->select('*');
		// $this->db->where('STATUS', 1);
		return $this->db->get('tabel_pendidikan');
	}
	function getUnitKerja(){
		$this->db->select('*');
		$this->db->where('aktif', 1);
		$this->db->order_by('kode_unker', 'asc');
		return $this->db->get('unker');
	}
	function getHubunganKeluarga(){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		return $this->db->get('tabel_hubungan_keluarga');
	}
	function getKategoriJab(){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		return $this->db->get('tabel_kategori_jab');
	}
	function getKategoriPel(){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		return $this->db->get('tabel_kategori_pel');
	}
	function getKategoriPredikat(){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		return $this->db->get('tabel_kategori_predikat');
	}
	function getKategoriLokasi(){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		return $this->db->get('tabel_kategori_lokasi');
	}
	function getKategoriSlks(){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		return $this->db->get('tabel_kategori_slks');
	}
	function getJenisCuti(){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		return $this->db->get('tabel_jenis_cuti');
	}
	function getEselon(){
		$this->db->select('*');
		// $this->db->where('STATUS', 1);
		return $this->db->get('tabel_eselon');
	}
	function getEselonS(){
		$this->db->select('*');
		$this->db->where_not_in('KODE_ESL', 99);
		$this->db->where_not_in('KODE_ESL', 88);
		$this->db->order_by('KODE_ESL ASC');
		// $this->db->where('STATUS', 1);
		return $this->db->get('tabel_eselon');
	}
	function getRekomendasi(){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		return $this->db->get('tabel_rekomendasi');
	}
	function getDataPendidikan($nip){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		$this->db->where('NIP', $nip);
		return $this->db->get('view_dt_pendidikan');
	}
	function getDtPendidikan($id){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		$this->db->where('ID', $id);
		return $this->db->get('view_dt_pendidikan');
	}
	function getDataKepangkatan($nip){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		$this->db->where('NIP', $nip);
		return $this->db->get('view_dt_kepangkatan');
	}
	function getDtKepangkatan($id){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		$this->db->where('ID', $id);
		return $this->db->get('view_dt_kepangkatan');
	}
	function getDataJabatan($nip){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		$this->db->where('NIP', $nip);
		return $this->db->get('view_dt_jabatan');
	}
	function getDtJabatan($id){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		$this->db->where('ID', $id);
		return $this->db->get('view_dt_jabatan');
	}
	function getDtPelatihan($id){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		$this->db->where('ID', $id);
		return $this->db->get('view_dt_diklat');
	}
	function getDtSKP($id){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		$this->db->where('ID', $id);
		return $this->db->get('view_dt_skp');
	}
	function getDtPenghargaan($id){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		$this->db->where('ID', $id);
		return $this->db->get('view_dt_penghargaan');
	}
	function getDtKgb($id){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		$this->db->where('ID', $id);
		return $this->db->get('view_dt_kgb');
	}
	function getDtCuti($id){
		$this->db->select('*');
		$this->db->where('aktif', 1);
		$this->db->where('idcuti', $id);
		return $this->db->get('view_dt_cuti');
	}
	function getDtKeluarga($id){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		$this->db->where('ID', $id);
		return $this->db->get('view_dt_keluarga');
	}
	function getDtAssessment($id){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		$this->db->where('ID', $id);
		return $this->db->get('view_dt_assessment');
	}
	function getDtHukdis($id){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		$this->db->where('ID', $id);
		return $this->db->get('view_dt_hukdis');
	}
	function getDataDiklat($nip){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		$this->db->where('NIP', $nip);
		return $this->db->get('view_dt_diklat');
	}
	function getDataSKP($nip){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		$this->db->where('NIP', $nip);
		return $this->db->get('view_dt_skp');
	}
	function getDataPenghargaan($nip){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		$this->db->where('NIP', $nip);
		return $this->db->get('view_dt_penghargaan');
	}
	function getDataKgb($nip){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		$this->db->where('NIP', $nip);
		return $this->db->get('view_dt_kgb');
	}
	function getDataCuti($id){
		$this->db->select('*');
		$this->db->where('aktif', 1);
		$this->db->where('iddatadasar', $id);
		return $this->db->get('view_dt_cuti');
	}
	function getDataKeluarga($nip){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		$this->db->where('NIP', $nip);
		return $this->db->get('view_dt_keluarga');
	}
	function getDataAssessment($nip){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		$this->db->where('NIP', $nip);
		return $this->db->get('view_dt_assessment');
	}
	function getDataHukdis($nip){
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		$this->db->where('NIP', $nip);
		return $this->db->get('view_dt_hukdis');
	}
	function getDataJFT(){
		$this->db->select('*');
		$this->db->where('tipe_jabatan', 1);
		return $this->db->get('fungsional');
	}
	function getDataJFU(){
		$this->db->select('*');
		$this->db->where('tipe_jabatan', 2);
		return $this->db->get('fungsional');
	}
}