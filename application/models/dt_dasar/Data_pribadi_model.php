<?php
class Data_pribadi_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	
	public function getDataPribadi($id = '') {
		$this->db->select('*');
		$this->db->where('ID', $id);
		return $this->db->get('view_dt_dasar');
	}

	public function updatedatapribadi($dataupdate, $id){
		$this->db->where('ID', $id);
		return $this->db->update('dt_dasar', $dataupdate);
	}
}