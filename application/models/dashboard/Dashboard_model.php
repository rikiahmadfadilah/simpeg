<?php
class Dashboard_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}

	function getDataGrafik1(){
		$query = "SELECT
						a.JENIS_KELAMIN,
						( SELECT COUNT( 1 ) FROM view_dt_dasar x WHERE x.JENIS_KEL = a.ID ) AS JML 
					FROM
						dt_jenis_kelamin a";
		return $this->db->query($query);
	}

	function getDataGrafik2(){
		$query = "SELECT
						CONCAT('GOL ',REPLACE(a.GOL,'/a','')) AS GOL,
						( SELECT COUNT( 1 ) FROM view_dt_dasar x WHERE LEFT(x.GOLONGAN,1) = LEFT(a.KODE_GOL,1) ) AS JML 
					FROM
						tabel_gol a
						GROUP BY LEFT(a.KODE_GOL,1)";
		return $this->db->query($query);
	}

	function getDataGrafik3(){
		$query = "SELECT
						a.ESL,
						( SELECT COUNT( 1 ) FROM view_dt_dasar x WHERE LEFT(x.ESELON,1) = LEFT(a.KODE_ESL,1) ) AS JML 
					FROM
						tabel_eselon a
						GROUP BY LEFT(a.KODE_ESL,1)";
		return $this->db->query($query);
	}

	function getDataGrafik4(){
		$query = "SELECT
						a.PENDIDIKAN,
						( SELECT COUNT( 1 ) FROM view_dt_dasar x WHERE x.NAMA_PENDIDIKAN = a.PENDIDIKAN ) AS JML 
					FROM
						tabel_pendidikan a";
		return $this->db->query($query);
	}
}