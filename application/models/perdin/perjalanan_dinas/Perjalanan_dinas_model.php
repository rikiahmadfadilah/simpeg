<?php
class Perjalanan_dinas_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function getPegawai() {
		$this->db->select('*');
		$this->db->order_by('pegawai_nip', 'asc');
		$this->db->where('pegawai_status_aktif', 1);
		return $this->db->get('dbo.ms_pegawai');
	}
	public function getPangkatGolongan() {
		$this->db->select('*');
		$this->db->order_by('golongan_name', 'asc');
		$this->db->where('golongan_status', 1);
		return $this->db->get('dbo.ms_golongan');
	}
	public function getJabatanInstansi() {
		$this->db->select('*');
		$this->db->order_by('jabatan_name', 'asc');
		$this->db->where('jabatan_status', 1);
		return $this->db->get('dbo.ms_jabatan');
	}
	public function getPerdin($where,$table){		
		return $this->db->get_where($table,$where);
	}
	public function insertPerdin($data){
		$this->db->set('perdin_create_date', "('".date('Y-m-d h:i:s', time())."')", FALSE);
        $this->db->insert('dbo.trx_perdin', $data);
        return $this->db->insert_id();
    }
    public function insertPerdindetail($data){
        $this->db->insert('dbo.trx_perdin_detail', $data);
        return $this->db->insert_id();
    }
    public function detailPerdin($where,$table){		
		return $this->db->get_where($table,$where);
	}
	public function updatePerdin($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
	public function updateSPerdin($data,$id){
		$this->db->where('perdin_id', $id);
		return $this->db->update('dbo.trx_perdin', $data);
	}
	public function statusPerdin($id){
		$this->db->where('perdin_id', $id);
		$this->db->select('perdin_status');
		return $this->db->get('dbo.trx_perdin');
	}

	function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('dbo.view_perjalanan_dinas',$limit,$offset);
		}else{
			return $this->db->get('dbo.view_perjalanan_dinas');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('dbo.view_perjalanan_dinas');
		return $this->db->count_all_results(); 
	}
}