<?php
class Rekap_perdin_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function getPegawai($perdin_id) {
		$this->db->select('pegawai_id,pegawai_nip,pegawai_nama,pegawai_nama_jabatan,unit_kerja_hirarki_name_full');
		$this->db->where('pegawai_nip', $perdin_id);
		return $this->db->get('dbo.view_ms_pegawai');
	}
	public function getPerdin($perdin_id = '') {
		$this->db->select('*');
		if($perdin_id!=""){
			$this->db->where('perdin_id', $perdin_id);
		}
		return $this->db->get('trx_perdin');
	}
	function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		$this->db->select('pegawai_id,pegawai_nip,pegawai_nama,pegawai_nama_lengkap,pegawai_nama_jabatan,unit_kerja_hirarki_name_full');
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('dbo.view_ms_pegawai',$limit,$offset);
		}else{
			return $this->db->get('dbo.view_ms_pegawai');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		$this->db->select('pegawai_id,pegawai_nip,pegawai_nama,pegawai_nama_lengkap,pegawai_nama_jabatan,unit_kerja_hirarki_name_full');
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('dbo.view_ms_pegawai');
		return $this->db->count_all_results(); 
	}
	public function getNegara() {
		$this->db->select('*');
		$this->db->order_by('negara_nama', 'asc');
		$this->db->where('negara_status', 1);
		return $this->db->get('dbo.ms_negara');
	}
	public function getProvinsi() {
		$this->db->select('*');
		$this->db->order_by('provinsi_nama', 'asc');
		$this->db->where('provinsi_status', 1);
		return $this->db->get('dbo.ms_provinsi');
	}
	public function getKota($provinsi_kode) {
		$this->db->select('*');
		$this->db->order_by('kota_nama', 'asc');
		$this->db->where('kota_provinsi_kode', $provinsi_kode);
		$this->db->where('kota_status', 1);
		return $this->db->get('ms_kota');
	}
	public function getKecamatan($kota_kode) {
		$this->db->select('*');
		$this->db->order_by('kecamatan_nama', 'asc');
		$this->db->where('kecamatan_kota_kode', $kota_kode);
		$this->db->where('kecamatan_status', 1);
		return $this->db->get('ms_kecamatan');
	}
	public function readPerdin($perdin_id = '') {
		$this->db->select('*');
		if($perdin_id!=""){
			$this->db->where('perdin_id', $perdin_id);
		}
		return $this->db->get('view_trx_perdin');
	}
}
