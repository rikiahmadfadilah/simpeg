<?php
class Perdin_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function getPegawai() {
		$this->db->select('ID,NIP,NAMA');
		// $this->db->order_by('NIP', 'asc');
		// $where = "STATUS = 1 and IS_PNS = 1";
		// $this->db->where($where);
		return $this->db->get('dt_dasar');
	}
	public function getPangkatGolongan() {
		$this->db->select('*');
		$this->db->order_by('golongan_nama', 'asc');
		$this->db->where('golongan_status', 1);
		return $this->db->get('ms_golongan');
	}
	public function getJabatanInstansi() {
		$this->db->select('*');
		$this->db->order_by('jabatan_nama', 'asc');
		$this->db->where('jabatan_status', 1);
		return $this->db->get('ms_jabatan');
	}
	public function getPerdin($perdin_id = '') {
		$this->db->select('*');
		if($perdin_id!=""){
			$this->db->where('perdin_id', $perdin_id);
		}
		return $this->db->get('trx_perdin');
	}
	public function addPerdin($data){
        $this->db->insert('trx_perdin', $data);
        return $this->db->insert_id();
    }
    public function addPerdinDetail($data){
        $this->db->insert('trx_perdin_detail', $data);
        return $this->db->insert_id();
    }
	public function updatePerdin($data,$id){
		$this->db->where('perdin_id', $id);
		return $this->db->update('trx_perdin', $data);
	}

	function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('view_trx_perdin',$limit,$offset);
		}else{
			return $this->db->get('view_trx_perdin');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('view_trx_perdin');
		return $this->db->count_all_results(); 
	}

	public function getPengikut($perdin_id) {
		$this->db->select('*');
		$where = "pegawai_status = 1 and pegawai_is_pns = 1 and (pegawai_jenis_pensiun_id is null or pegawai_jenis_pensiun_id = 0) and (perdin_detail_perdin_id = $perdin_id)";
		$this->db->where($where);
		return $this->db->get('view_trx_perdin_detail');
	}
	public function getNegara() {
		$this->db->select('*');
		$this->db->order_by('negara_nama', 'asc');
		$this->db->where('negara_status', 1);
		return $this->db->get('ms_negara');
	}
	public function getProvinsi() {
		$this->db->select('*');
		$this->db->order_by('provinsi_nama', 'asc');
		$this->db->where('provinsi_status', 1);
		return $this->db->get('ms_provinsi');
	}
	public function getKota($provinsi_kode) {
		$this->db->select('*');
		$this->db->order_by('kota_nama', 'asc');
		$this->db->where('kota_provinsi_kode', $provinsi_kode);
		$this->db->where('kota_status', 1);
		return $this->db->get('ms_kota');
	}
	public function getKecamatan($kota_kode) {
		$this->db->select('*');
		$this->db->order_by('kecamatan_nama', 'asc');
		$this->db->where('kecamatan_kota_kode', $kota_kode);
		$this->db->where('kecamatan_status', 1);
		return $this->db->get('ms_kecamatan');
	}
	public function readPerdin($perdin_id = '') {
		$this->db->select('*');
		if($perdin_id!=""){
			$this->db->where('perdin_id', $perdin_id);
		}
		return $this->db->get('view_trx_perdin');
	}
}
