<?php
class Elogbook_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	
	public function getPegawai($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('ID', $pegawai_id);
		return $this->db->get('view_dt_dasar');
	}
	public function getListPendidikan($pegawai_id = '') {
		$this->db->select('*');
				$this->db->where('pendidikan_formal_status', 1);
		$this->db->where('pendidikan_formal_pegawai_id', $pegawai_id);
		return $this->db->get('view_trx_pendidikan_formal');
	}
	public function getJenisDiklat(){
		$this->db->select('*');
		$this->db->where('jenis_diklat_status', 1);
		return $this->db->get('ms_jenis_diklat');		
	}
	public function getPendidikan() {
		$this->db->select('*');
		$this->db->where('pendidikan_status', 1);
		return $this->db->get('ms_pendidikan');
	}
	public function getTingkatPendidikan($pendidikan_kode) {
		$this->db->select('jurusan_grup,jurusan_kode');
		$this->db->order_by('jurusan_kode', 'asc');
		$this->db->group_by('jurusan_grup,jurusan_kode');
		$this->db->where('jurusan_pendidikan_kode', $pendidikan_kode);
		$this->db->where("right(jurusan_kode,5) = '00000'");
		$this->db->where('jurusan_status', 1);
		return $this->db->get('ms_jurusan');
	}
	
	public function getDiklatPerjenjangan($pegawai_id){
		$this->db->select('*');
		$this->db->where('diklat_pegawai_id', $pegawai_id);
		$this->db->where('diklat_status', 1);
		return $this->db->get('view_trx_diklatpenyesuaianijazah');		
	}
	function getDataPegawaiByUnitKerja($pegawai_unit_kerja_id)
	{
		$this->db->where('ID_UNKER', $pegawai_unit_kerja_id);
		$this->db->order_by('GOLONGAN', 'desc');
		return $this->db->get('view_dt_dasar');
	}

	public function GetGolongan(){
		$this->db->select('*');
		$this->db->where('golongan_status', 1);
		return $this->db->get('ms_golongan');		
	}

	public function getUnitKerja($parent=0,$lvl=3) {
		$this->db->select('*');
		$this->db->order_by('unit_kerja_hirarki', 'asc');
		$this->db->where('unit_kerja_level', $lvl);
		$this->db->where('unit_kerja_parent_id_kode', $parent);
		return $this->db->get('view_ms_unit_kerja');
	}

	public function getJabatan() {
		$this->db->select('*');
		$this->db->where('jabatan_status', 1);
		return $this->db->get('ms_jabatan');
	}

	public function getJenjangJabatan($pegawai_id){
		$this->db->select('*');
		$this->db->where('jenjang_jabatan_pegawai_id', $pegawai_id);
		$this->db->where('jenjang_jabatan_status', 1);
		return $this->db->get('view_trx_jenjang_jabatan');		
	}

	public function getkegiatanHarian($jadwal_kegiatan_id){
		$this->db->select('*');
		$this->db->where('kegiatan_jadwal_kegiatan_id', $jadwal_kegiatan_id);
		$this->db->where('kegiatan_status', 1);
		return $this->db->get('view_trx_kegiatan');
	}

	public function getSKP(){
		$this->db->select('*');
		$this->db->where('jenis_skp_status', 1);
		return $this->db->get('ms_jenis_skp');	
	}

	public function create_jadwal($datacreate){
		$this->db->insert('trx_jadwal_kegiatan', $datacreate);
		return $this->db->insert_id();
	}

	public function update_jadwal($dataupdate, $id){
		$this->db->where('jadwal_kegiatan_id', $id);
		return $this->db->update('trx_jadwal_kegiatan', $dataupdate);
	}

	public function create_kegiatan($datacreate){
		$this->db->insert('trx_kegiatan', $datacreate);
		return $this->db->insert_id();
	}

	public function update_kegiatan($dataupdate, $id){
		$this->db->where('kegiatan_id', $id);
		return $this->db->update('trx_kegiatan', $dataupdate);
	}
	public function update_jadwal_kegiatan($dataupdate, $jadwal_kegiatan_id){
		$this->db->where('jadwal_kegiatan_id', $jadwal_kegiatan_id);
		return $this->db->update('trx_jadwal_kegiatan', $dataupdate);
	}
	
	public function create_jenjang_jabatan($datacreate){
		$this->db->insert('trx_jenjang_jabatan', $datacreate);
		return $this->db->insert_id();
	}

	public function update_jenjang_jabatan($dataupdate, $jenjang_jabatan_id){
		$this->db->where('jenjang_jabatan_id', $jenjang_jabatan_id);
		return $this->db->update('trx_jenjang_jabatan', $dataupdate);
	}
	
	public function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('view_trx_jadwal_kegiatan',$limit,$offset);
		}else{
			return $this->db->get('view_trx_jadwal_kegiatan');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('view_trx_jadwal_kegiatan');
		return $this->db->count_all_results(); 
	}

	function getkegiatan($id){
		$this->db->where('kegiatan_id', $id);
		return $this->db->get('dbo.trx_kegiatan');
	}
	
}