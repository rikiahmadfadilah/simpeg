<?php
class Pengumuman_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	

	public function getPengumuman($pengumuman_id = '') {
		$this->db->select('*');
		if($pengumuman_id!=""){
			$this->db->where('pengumuman_id', $pengumuman_id);
		}
		return $this->db->get('view_trx_pengumuman');
	}
	public function getListPegawai() {
		$this->db->select('*');
		$this->db->where('pegawai_firebase_id IS NOT NULL');
		return $this->db->get('view_ms_pegawai');
	}
	public function addPengumuman($datacreate){
		$this->db->insert('trx_pengumuman', $datacreate);
		return $this->db->insert_id();
	}

	public function updatepengumuman($dataupdate, $pengumuman_id){
		$this->db->where('pengumuman_id', $pengumuman_id);
		return $this->db->update('trx_pengumuman', $dataupdate);
	}
	
	public function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('view_trx_pengumuman',$limit,$offset);
		}else{
			return $this->db->get('view_trx_pengumuman');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('view_trx_pengumuman');
		return $this->db->count_all_results(); 
	}
}