<?php
class Global_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	function DataTable($TableName, $form, $FieldName, $DEFAULT_ORDER, $WHERE_DATA) {
		$LIMIT = 10;
		$iDisplayLength = (($form["iDisplayLength"]) ? $form["iDisplayLength"] : 0);
		$sEcho = (($form["sEcho"]) ? $form["sEcho"] : 0);
		$iDisplayStart = (($form["iDisplayStart"]) ? $form["iDisplayStart"] : 0);
		$LIMIT = ($iDisplayLength == 0) ? $LIMIT : $iDisplayLength;
		$START = ($iDisplayStart == 0) ? 0 : $iDisplayStart;

		$sSearch = (($form["sSearch"]) ? $form["sSearch"] : "");

		$ORDER_BY_DATA = "";
		$SEARCH_DATA = "";

		$JumlahKolom = (($form["iColumns"]) ? $form["iColumns"] : 0);
		$JumlahSorting = (($form["iSortingCols"]) ? $form["iSortingCols"] : 0);

		if ($JumlahSorting > 0)
		{
			for ($index_sorting = 0; $index_sorting < $JumlahSorting; $index_sorting++)
			{
				$iSortCol = (($form["iSortCol_".$index_sorting]) ? $form["iSortCol_".$index_sorting] : 0);
				$sSortDir = (($form["sSortDir_".$index_sorting]) ? $form["sSortDir_".$index_sorting] : "DESC");
				$ORDER_BY_DATA .= $FieldName[$iSortCol] ." ".$sSortDir;
				if (($index_sorting + 1) < $JumlahSorting)
				{
					$ORDER_BY_DATA .= " , ";
				}
			}
		}
		else
		{
			$ORDER_BY_DATA = $DEFAULT_ORDER;
		}
		if ($sSearch != "")
		{
			for ($index_field = 0; $index_field < $FieldName.Count(); $index_field++)
			{
				$SEARCH_DATA .= $FieldName[$index_field]. "  LIKE '%".$sSearch."%'";
				if (($index_field + 1) < $FieldName.Count())
				{
					$SEARCH_DATA .= " OR ";
				}
			}
			$SEARCH_DATA = "(".$SEARCH_DATA .")";
		}


		$inject_clause_count = "";
		$inject_clause_select = "";
		if ($WHERE_DATA != "" && $SEARCH_DATA != "")
		{
			$WHERE_DATA .= " AND ";
		}
		if ($WHERE_DATA != "" || $SEARCH_DATA != "")
		{
			$inject_clause_count = "WHERE " . $WHERE_DATA . " " . $SEARCH_DATA;
			$inject_clause_select = "WHERE " . $WHERE_DATA . " " . $SEARCH_DATA;
		}

		$inject_clause_select .= " ORDER BY " . $ORDER_BY_DATA . "  LIMIT " . $START . " , " . $LIMIT . " ";
		$QueryCount = 'SELECT COUNT(*) AS Jumlah FROM '.$TableName.' '.$inject_clause_count;
		$QueryList = 'SELECT * FROM '.$TableName.' '.$inject_clause_select;
		$DataCount = $this->db->query($QueryCount)->row_array();
		$DataList = $this->db->query($QueryList)->result_array();
		$RETURNDATA 	= array();
		$RETURNDATA["iTotalRecords"] = $DataCount["Jumlah"];
		$RETURNDATA["iTotalDisplayRecords"] = $DataCount["Jumlah"];
		$RETURNDATA["aaData"] = $DataList;
		$RETURNDATA["sEcho"] = $sEcho;
		$RETURNDATA["START"] = $START;
		$RETURNDATA["query"] = $QueryList;
		return $RETURNDATA;
	}
	function get_all_menu($akses_id) {
		$query = 'SELECT
		MP.MENU_ID,
		MP.MENU_PARENT_ID,
		MP.MENU_NAME,
		(
		SELECT
		COUNT (MX.MENU_ID)
		FROM
		menu MX
		WHERE
		MX.MENU_PARENT_ID = MP.MENU_ID
		) AS MENU_ANAK,
		LEVEL as MENU_LEVEL
		FROM
		ms_menu MP
		START WITH
		MP.MENU_PARENT_ID = 0
		CONNECT BY
		PRIOR MP.MENU_ID=MP.MENU_PARENT_ID
		ORDER SIBLINGS by MP.MENU_SORT ASC
		';

		return $this->db->query($query); 
	}
	public function create_log($table_name, $table_id) {
		$queryGetColumnName = "SELECT getColumnName('".$table_name."') AS ColumnName";
		$ColumnName = $this->db->query($queryGetColumnName)->row_array();
		$queryGetPrimaryKey = "SELECT getPrimaryKey('".$table_name."') AS PrimaryKey";
		$PrimaryKey = $this->db->query($queryGetPrimaryKey)->row_array();
		$queryGetColumnNameLogId = "SELECT getColumnNameLogId('".$table_name."') AS ColumnNameLogId";
		$ColumnNameLogId = $this->db->query($queryGetColumnNameLogId)->row_array();



		$query = 'INSERT INTO '.$table_name.' ('.$ColumnName["ColumnName"].','.$ColumnNameLogId["ColumnNameLogId"].') SELECT '.$ColumnName["ColumnName"].','.$table_id.' FROM '.$table_name.' WHERE '.$PrimaryKey["PrimaryKey"].' = '.$table_id.'';
		$this->db->query($query); 
		return $this->db->insert_id();
	}
	public function get_notifikasi() {
		$this->db->where('notif_user_id',  $this->session->userdata('user_id'));
		//$this->db->where('notif_type', 1);
		$this->db->where('notif_is_count', 1);
		$this->db->order_by('notif_date', 'DESC');
		return $this->db->get('view_sys_notif');
	}
	public function get_history($table_name,$table_id) {
		$this->db->where('history_table_name', $table_name);
		$this->db->where('history_table_id', $table_id);
		$this->db->order_by('history_id', 'ASC');
		return $this->db->get('view_sys_histories');
	}
	public function getMenus($access) {
		$this->db->select('ms_menu.*');
		$this->db->where('menu_id IN', '(select akses_detail_menu_id from ms_akses_detail where akses_detail_akses_id = '.$access.')', false);
		$this->db->where('menu_status', 1);
		$this->db->order_by('menu_sort', 'ASC');
		return $this->db->get('ms_menu');
	}
	public function getbreadcrumb($menu_id) {
		$query = "CALL getbreadcrumb(".$menu_id.")";
		return $this->db->query($query); 
	}
	public function getMenuIsParent($menu_id) {
		$this->db->where('menu_parent_id', $menu_id , false);
		$this->db->where('menu_status', 1);
		$this->db->from('ms_menu');
		return $this->db->count_all_results(); 
	}
	function getThisMenu($menu_segment) {
		$this->db->select('menu_id,menu_parent_id,menu_nama');
		$this->db->where('menu_url', $menu_segment);
		$this->db->where('menu_status', 1);
		return $this->db->get('ms_menu');
	}

	public function getGlobalConfig($where)
	{
		return $this->db->get_where('tabel_config', $where)->row();
	}


}