<?php
class Rekap_cuti_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function getCutiDetail($cuti_detail_id = '') {
		$this->db->select('*');
		if($cuti_detail_id!=""){
			$this->db->where('cuti_detail_id', $cuti_detail_id);
		}
		return $this->db->get('view_cuti_detail');
	}
	public function getPegawai() {
		$this->db->select('*');
		$this->db->where('pegawai_status', 1);
		$this->db->limit(100);
		return $this->db->get('view_ms_pegawai');
	}
	public function getJenisCuti() {
		$this->db->select('*');
		$this->db->where('jenis_cuti_status', 1);
		return $this->db->get('ms_jenis_cuti');
	}
	public function addCuti($datacreate){
		$this->db->insert('trx_cuti', $datacreate);
		return $this->db->insert_id();
	}
	public function addCutiDetail($datacreate){
		$this->db->insert('trx_cuti_detail', $datacreate);
		return $this->db->insert_id();
	}

	public function updateCutiDetail($dataupdate, $cuti_detail_id){
		$this->db->where('cuti_detail_id', $cuti_detail_id);
		return $this->db->update('trx_cuti_detail', $dataupdate);
	}
	
	public function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('view_cuti_detail',$limit,$offset);
		}else{
			return $this->db->get('view_cuti_detail');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('view_cuti_detail');
		return $this->db->count_all_results(); 
	}
}