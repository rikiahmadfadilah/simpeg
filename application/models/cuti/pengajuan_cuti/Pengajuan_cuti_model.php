<?php
class Pengajuan_cuti_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function getCutiDetail($cuti_detail_id = '') {
		$this->db->select('*');
		if($cuti_detail_id!=""){
			$this->db->where('cuti_detail_id', $cuti_detail_id);
		}
		return $this->db->get('view_cuti_detail');
	}
	// public function getPegawai($operator_unit_kerja_kode) {
	// 	$this->db->select('pegawai_id,pegawai_nip,pegawai_nama,unit_kerja_parent_id_kode,unit_kerja_kode_hirarki');
	// 	$where = "pegawai_status = 1 and pegawai_is_pns = 1 and (pegawai_jenis_pensiun_id is null or pegawai_jenis_pensiun_id = 0)";
 //        if($operator_unit_kerja_kode)$where .= " and (unit_kerja_kode_hirarki LIKE '%". $operator_unit_kerja_kode."%')";
	// 	$this->db->where($where);
	// 	// $this->db->limit(100);
	// 	if($this->session->userdata('filter_where') != ""){
	// 		$this->db->where($this->session->userdata('filter_where'));
	// 	}
	// 	return $this->db->get('view_ms_pegawai');
	// }
	public function getPegawai() {
		$this->db->select('ID,NIP,NAMA,UNIT_KERJA');
		$where = "STATUS = 1 and IS_PNS = 1";
		$this->db->where($where);
		return $this->db->get('view_dt_dasar');
	}
	public function get_atasan($atasan) {
		$this->db->select('*');
		$this->db->where('pegawai_status', 1);
		$this->db->where('unit_kerja_id_kode', $atasan);
		// $this->db->limit(50);
		return $this->db->get('view_ms_pegawai');
	}
	public function getJenisCuti() {
		$this->db->select('*');
		$this->db->where('jenis_cuti_status', 1);
		return $this->db->get('ms_jenis_cuti');
	}
	public function addCuti($datacreate){
		$this->db->insert('trx_cuti', $datacreate);
		return $this->db->insert_id();
	}
	public function addCutiDetail($datacreate){
		$this->db->insert('trx_cuti_detail', $datacreate);
		return $this->db->insert_id();
	}

	public function updateCutiDetail($dataupdate, $cuti_detail_id){
		$this->db->where('cuti_detail_id', $cuti_detail_id);
		return $this->db->update('trx_cuti_detail', $dataupdate);
	}
	
	public function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('view_cuti_detail',$limit,$offset);
		}else{
			return $this->db->get('view_cuti_detail');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('view_cuti_detail');
		return $this->db->count_all_results(); 
	}
	function getDataListPegawai($unit_kerja_parent_id,$search,$page,$pegawai_id_selected)
	{	
		//$this->db->where('skp_header_pegawai_id', $id);
		//$this->db->where('skp_header_status', 1);

		$pegawai_id = $pegawai_id_selected;
		$this->db->where('ID <>', $pegawai_id);
		$limit = 5;
		$offset = $page*$limit;
		//$this->db->where('pegawai_unit_kerja_id',$unit_kerja_parent_id);
		$likeclause ="(";
		$likeclause .= "UPPER(NIP) LIKE '%".strtoupper($search)."%' OR ";
		$likeclause .= "UPPER(NAMA) LIKE '%".strtoupper($search)."%'";
		$likeclause .=")";
		$this->db->where($likeclause);
		
		return $this->db->get('view_dt_dasar',$limit,$offset);
	}
	function getDataListPegawaiCount($unit_kerja_parent_id,$search,$pegawai_id_selected)
	{	
		//$this->db->where('skp_header_pegawai_id', $id);
		//$this->db->where('skp_header_status', 1);
		$pegawai_id = $pegawai_id_selected;
		$this->db->where('ID <>', $pegawai_id);
		$likeclause ="";
		//$this->db->where('pegawai_unit_kerja_id',$unit_kerja_parent_id);
		$likeclause ="(";
		$likeclause .= "UPPER(NIP) LIKE '%".strtoupper($search)."%' OR ";
		$likeclause .= "UPPER(NAMA) LIKE '%".strtoupper($search)."%'";
		$likeclause .=")";
		$this->db->where($likeclause);
		
		return $this->db->get('view_dt_dasar');
	}

	function getDataPegawai($pegawai_id = '',$pegawai_nip = '')
	{	
		$this->db->select('ID,NIP,NAMA,TMP_LAHIR,GOLONGAN_NAMA,PANGKAT,ESELON,NAMA_JAB,UNIT_KERJA');
		if($pegawai_id!=''){
			$this->db->where('ID', $pegawai_id);
		}
		if($pegawai_nip!=''){
			$this->db->where('NIP', $pegawai_nip);
		}
		
		return $this->db->get('view_dt_dasar');
	}
}
