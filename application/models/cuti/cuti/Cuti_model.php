<?php
class Cuti_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function getPegawai() {
		$this->db->select('*');
		//$this->db->order_by('pegawai_nama', 'asc');
		//$this->db->where('pegawai_status_aktif', 1);
		//return $this->db->get('dbo.ms_pegawai');
		return $this->db->get('Master.view_master_pegawai',10,0);
	}
	public function getPegawaidetail($id){
		$this->db->where('pegawai_id',$id);
		return $this->db->get('dbo.ms_pegawai');
	}
	public function getCutiPegawai() {
		$this->db->select('*');
		// $this->db->order_by('cuti_pegawai_nip', 'asc');
		// $this->db->where('cuti_id');
		return $this->db->get('dbo.trx_cuti');
	}
	public function getJenisCuti() {
		$this->db->select('*');
		$this->db->order_by('jenis_cuti_name', 'asc');
		$this->db->where('jenis_cuti_status', 1);
		return $this->db->get('dbo.ms_jenis_cuti');
	}
	public function getCuti($where,$table){		
		return $this->db->get_where($table,$where);
	}
	public function getCutidetail($where,$table){		
		return $this->db->get_where($table,$where);
	}
	public function insertCuti($data){
        $this->db->insert('dbo.trx_cuti', $data);
        return $this->db->insert_id();
    }
    public function insertCutidetail($data){
    	$this->db->set('cuti_detail_creete_date', "('".date('Y-m-d h:i:s', time())."')", FALSE);
        $this->db->insert('dbo.trx_cuti_detail', $data);
        return $this->db->insert_id();
    }
	public function updateCuti($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
	public function updateCutidetail($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
    public function detailCuti($where,$table){		
		return $this->db->get_where($table,$where);
	}
	public function updateCutis($data,$id){
		$this->db->where('cuti_detail_id', $id);
		return $this->db->update('dbo.trx_cuti_detail', $data);
	}
	public function statusCuti($id){
		$this->db->where('cuti_detail_id', $id);
		$this->db->select('cuti_detail_status');
		return $this->db->get('dbo.trx_cuti_detail');
	}

	function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('dbo.view_cuti',$limit,$offset);
		}else{
			return $this->db->get('dbo.view_cuti');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('dbo.view_cuti');
		return $this->db->count_all_results(); 
	}
}