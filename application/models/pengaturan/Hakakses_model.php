<?php
class hakakses_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function get_hakakses() {
		$this->db->select('*');		
		$this->db->where('akses_status', 1);
		return $this->db->get('dbo.ms_akses');
	}
	public function get_hakakses_nama($akses_id,$akses_status=1) {
		$this->db->select(array('akses_nama','akses_id'));		
		$this->db->where(array('dbo.ms_akses.akses_id'=>$akses_id,'dbo.ms_akses.akses_status'=> $akses_status));
		return $this->db->get('dbo.ms_akses');
	}
	public function get_hakakses_detail($akses_id,$akses_status=1) {
		$this->db->select('*');		
		$this->db->where(array('dbo.view_ms_akses_detail.akses_id'=>$akses_id,'dbo.view_ms_akses_detail.akses_status'=> $akses_status));
		return $this->db->get('dbo.view_ms_akses_detail');
	}
	
	public function get_menu() {
		$this->db->select('*');		
		$this->db->where('menu_status', 1);
		$this->db->order_by('MenuHirarki', 'asc');
		return $this->db->get('dbo.View_ms_menu');
	}

	function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = ''){
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!=''){
			$likeclause = '(';
			$i=0;
			foreach($fields as $field){
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext)){
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('dbo.view_ms_akses',$limit,$offset);
		}else{
			return $this->db->get('dbo.view_ms_akses');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = ''){	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!=''){
			$likeclause = '(';
			$i=0;
			foreach($fields as $field){
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('dbo.view_ms_akses');
		
		return $this->db->count_all_results(); 
	}


	function add_hakakses($data){
		$this->db->insert('dbo.ms_akses', $data);
		return $this->db->insert_id();
	}

	function add_hakakses_detail($data){
		$this->db->insert('dbo.ms_akses_detail', $data);
		return $this->db->insert_id();
	}

	function update_hakakses($data, $data_id){
		$this->db->where('akses_id', $data_id);
		return $this->db->update('dbo.ms_akses', $data);
	}	
	
	function update_hakakses_detail($data, $data_id){
		$this->db->where('akses_detail_id', $data_id);
		return $this->db->update('dbo.ms_akses_detail', $data);
	}		

	public function check_nama_hakakses($akses_nama = '') {
		$this->db->select('*');
		$this->db->where('akses_nama', $akses_nama);
		return $this->db->get('dbo.ms_akses');
	}
	public function check_data_hakakses($akses_id = '') {
		$this->db->select('*');
		$this->db->where('akses_id', $akses_id);
		return $this->db->get('dbo.view_ms_akses_detail');
	}	
}
