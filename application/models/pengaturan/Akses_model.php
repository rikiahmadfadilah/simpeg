<?php
class Akses_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function getakses($akses_id = '') {
		$this->db->select('*');
		if($akses_id!=""){
			$this->db->where('akses_id', $akses_id);
		}
		return $this->db->get('view_ms_akses');
	}
	public function checkKodeakses($akses_kode = '') {
		$this->db->select('*');
		$this->db->where('akses_kode', $akses_kode);
		return $this->db->get('ms_akses');
	}
	public function checkNamaakses($akses_nama = '') {
		$this->db->select('*');
		$this->db->where('akses_nama', $akses_nama);
		return $this->db->get('ms_akses');
	}
	
	public function addakses($datacreate){
		$this->db->insert('ms_akses', $datacreate);
		return $this->db->insert_id();
	}
	function addDetail($akses_detail){
		$this->db->insert('ms_akses_detail', $akses_detail);
	}
	function updateDetail($menuid,$aksesid,$dataupdate){
		$this->db->where('akses_detail_menu_id',$menuid);
		$this->db->where('akses_detail_akses_id',$aksesid);
		$this->db->update('ms_akses_detail', $dataupdate);
	}

	public function updateakses($dataupdate,$akses_id){
		$this->db->where('akses_id', $akses_id);
		return $this->db->update('ms_akses', $dataupdate);
	}
	function delete_detail($idakses){
		$this->db->where('akses_detail_akses_id',$idakses);
		return $this->db->delete('ms_akses_detail');
	}

	public function get_menu() {
		$this->db->select('*');
		$this->db->where('menu_status', 1);
		$this->db->order_by('MenuHirarki', 'asc');
		return $this->db->get('dbo.View_ms_menu');
	}

	function get_listmenu_induk($id)
	{
		$query = "SELECT
			menu_id,
			menu_parent_id,
			menu_url,
			menu_nama,
			menu_icon,
			menu_sort,
			menu_status
		FROM
			ms_menu
		WHERE
		menu_parent_id = '".$id."' ORDER BY menu_sort ASC";
		return $this->db->query($query);
	}
	function get_listmenu_anak($id, $sort)
	{
		$query = "SELECT
			menu_id,
			menu_parent_id,
			menu_url,
			menu_nama,
			menu_icon,
			menu_sort,
			menu_status
		FROM
			ms_menu
		WHERE
		menu_parent_id = '".$id."'";
		return $this->db->query($query);
	}
	function get_listmenu_anakk($id, $sort)
	{
		$query = "SELECT
			menu_id,
			menu_parent_id,
			menu_url,
			menu_nama,
			menu_icon,
			menu_sort,
			menu_status
		FROM
			ms_menu
		WHERE
		menu_parent_id = '".$id."'";
		return $this->db->query($query);
	}
	function getDetailakses($idakses)
	{
		$this->db->where('akses_detail_akses_id',$idakses);
		return $this->db->get('ms_akses_detail');
	}

	function getDetailhakakses($idakses)
	{
		$this->db->where('akses_id',$idakses);
		return $this->db->get('ms_akses');
	}
	function getListhakakses(){
		return $this->db->get_where('ms_akses', array('akses_status' => 1));
	}
	
	public function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('view_ms_akses',$limit,$offset);
		}else{
			return $this->db->get('view_ms_akses');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('view_ms_akses');
		return $this->db->count_all_results(); 
	}
}
