<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aplikasi_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
	function getConfig()
	{
		$this->db->where('config_status', 1);
		return $this->db->get('ms_config');
	}
	function updateConfig($kode, $value)
	{
		$this->db->set('config_value', $value);
		$this->db->where('config_code', $kode);
		return $this->db->update('ms_config');
	}

}
