<?php
class User_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	// public function getPegawai($pegawai_id = '') {
	// 	$this->db->select('*');
	// 	if($pegawai_id!=""){
	// 		$this->db->where('pegawai_id', $pegawai_id);
	// 	}
	// 	$this->db->limit(100);
	// 	return $this->db->get('dbo.pegawai');
	// }
	public function getPegawai($pegawai_id = '') {
		$query = $this->db->query("SELECT * FROM dt_dasar a LEFT JOIN ms_user b ON a.NIP = b.user_nip WHERE b.user_nip IS NULL");
		return $query;
	}
	public function getUser($user_id = '') {
		$this->db->select('*');
		if($user_id!=""){
			$this->db->where('user_id', $user_id);
		}
		return $this->db->get('view_ms_user');
	}
	// public function getUserNull() {
	// 	$this->db->select('*');
	// 	$this->db->where('user_akses_id IS NULL');

	// 	return $this->db->get('view_user');
	// }
	public function getAkses($akses_id = '') {
		$this->db->select('*');
		if($akses_id!=""){
			$this->db->where('akses_id', $akses_id);
		}
		return $this->db->get('ms_akses');
	}
	public function getRoles($user_id = '') {
		$this->db->select('*');
		$this->db->where('role_user_id', $user_id);
		$this->db->where('role_status', 1);
		return $this->db->get('ms_roles');
	}
	public function getDetailUser($user_id = '') {
		$this->db->select('*');
		if($user_id!=""){
			$this->db->where('user_id', $user_id);
		}
		return $this->db->get('view_ms_user');
	}
	public function checkKodeuser($user_kode = '') {
		$this->db->select('*');
		$this->db->where('user_kode', $user_kode);
		return $this->db->get('dbo.user');
	}
	public function checkNamauser($user_nama = '') {
		$this->db->select('*');
		$this->db->where('user_nama', $user_nama);
		return $this->db->get('dbo.user');
	}
	
	public function addUser($datacreate){
		$this->db->insert('ms_user', $datacreate);
		return $this->db->insert_id();
	}

	public function updateUser($dataupdate, $user_id){
		$this->db->where('user_id', $user_id);
		return $this->db->update('ms_user', $dataupdate);
	}
	
	public function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('view_ms_user',$limit,$offset);
		}else{
			return $this->db->get('view_ms_user');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('view_ms_user');
		return $this->db->count_all_results(); 
	}

	public function GetUnitKerjaAdmin(){
		$this->db->order_by('kode_unker', "asc");
		return $this->db->get('view_unker');
	}

	public function GetUnitKerja($operator_unit_kerja_kode){
		$this->db->order_by('kode_unker', "asc");
		return $this->db->get('view_unker');
	}
}