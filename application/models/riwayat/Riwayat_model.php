<?php
class Riwayat_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	
	public function insert_dt_pendidikan($datacreate){
		$this->db->insert('dt_pendidikan', $datacreate);
		return $this->db->insert_id();
	}
	public function update_dt_pendidikan($dataupdate, $id){
		$this->db->where('ID', $id);
		return $this->db->update('dt_pendidikan', $dataupdate);
	}
	public function insert_dt_pangkat($datacreate){
		$this->db->insert('dt_kepangkatan', $datacreate);
		return $this->db->insert_id();
	}
	public function update_dt_pangkat($dataupdate, $id){
		$this->db->where('ID', $id);
		return $this->db->update('dt_kepangkatan', $dataupdate);
	}
	public function insert_dt_jabatan($datacreate){
		$this->db->insert('dt_jabatan', $datacreate);
		return $this->db->insert_id();
	}
	public function update_dt_jabatan($dataupdate, $id){
		$this->db->where('ID', $id);
		return $this->db->update('dt_jabatan', $dataupdate);
	}
	public function insert_dt_pelatihan($datacreate){
		$this->db->insert('dt_diklat', $datacreate);
		return $this->db->insert_id();
	}
	public function update_dt_pelatihan($dataupdate, $id){
		$this->db->where('ID', $id);
		return $this->db->update('dt_diklat', $dataupdate);
	}
	public function insert_dt_skp($datacreate){
		$this->db->insert('dt_skp', $datacreate);
		return $this->db->insert_id();
	}
	public function update_dt_skp($dataupdate, $id){
		$this->db->where('ID', $id);
		return $this->db->update('dt_skp', $dataupdate);
	}
	public function insert_dt_penghargaan($datacreate){
		$this->db->insert('dt_penghargaan', $datacreate);
		return $this->db->insert_id();
	}
	public function update_dt_penghargaan($dataupdate, $id){
		$this->db->where('ID', $id);
		return $this->db->update('dt_penghargaan', $dataupdate);
	}
	public function insert_dt_kgb($datacreate){
		$this->db->insert('dt_kgb', $datacreate);
		return $this->db->insert_id();
	}
	public function update_dt_kgb($dataupdate, $id){
		$this->db->where('ID', $id);
		return $this->db->update('dt_kgb', $dataupdate);
	}
	public function update_dt_cuti($dataupdate, $id){
		$this->db->where('idcuti', $id);
		return $this->db->update('dt_cuti', $dataupdate);
	}
	public function update_dt_keluarga($dataupdate, $id){
		$this->db->where('ID', $id);
		return $this->db->update('dt_keluarga', $dataupdate);
	}
	public function update_dt_assessment($dataupdate, $id){
		$this->db->where('ID', $id);
		return $this->db->update('dt_assessment', $dataupdate);
	}
	public function update_dt_hukdis($dataupdate, $id){
		$this->db->where('ID', $id);
		return $this->db->update('dt_hukdis', $dataupdate);
	}
	public function insert_dt_keluarga($datacreate){
		$this->db->insert('dt_keluarga', $datacreate);
		return $this->db->insert_id();
	}
	public function insert_dt_cuti($datacreate){
		$this->db->insert('dt_cuti', $datacreate);
		return $this->db->insert_id();
	}
	public function insert_dt_assessment($datacreate){
		$this->db->insert('dt_assessment', $datacreate);
		return $this->db->insert_id();
	}
	public function insert_dt_hukdis($datacreate){
		$this->db->insert('dt_hukdis', $datacreate);
		return $this->db->insert_id();
	}
}