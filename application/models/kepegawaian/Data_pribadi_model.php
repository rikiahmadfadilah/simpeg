<?php
class Data_pribadi_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	
	public function getDataPribadi($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('pegawai_id', $pegawai_id);
		return $this->db->get('view_ms_pegawai');
	}

	public function updatedatapribadi($dataupdate, $pegawai_id){
		$this->db->where('pegawai_id', $pegawai_id);
		return $this->db->update('ms_pegawai', $dataupdate);
	}
}