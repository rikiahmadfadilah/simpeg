<?php
class Anak_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	
	public function getPegawai($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('pegawai_id', $pegawai_id);
		return $this->db->get('view_ms_pegawai');
	}
	public function getListPendidikan($pegawai_id = '') {
		$this->db->select('*');
				$this->db->where('pendidikan_formal_status', 1);
		$this->db->where('pendidikan_formal_pegawai_id', $pegawai_id);
		return $this->db->get('view_trx_pendidikan_formal');
	}
	public function getPendidikan() {
		$this->db->select('*');
		$this->db->where('pendidikan_status', 1);
		return $this->db->get('ms_pendidikan');
	}	
	public function GetJenisKelamin(){
		$this->db->select('*');
		$this->db->where('jenis_kelamin_status', 1);
		return $this->db->get('ms_jenis_kelamin');		
	}

	public function getAnak($pegawai_id){
		$this->db->select('*');
		$this->db->where('anak_pegawai_id', $pegawai_id);
		$this->db->where('anak_status', 1);
		return $this->db->get('view_trx_anak');		
	}
	
	public function create_anak($datacreate){
		$this->db->insert('trx_anak', $datacreate);
		return $this->db->insert_id();
	}

	public function update_anak($dataupdate, $anak_id){
		$this->db->where('anak_id', $anak_id);
		return $this->db->update('trx_anak', $dataupdate);
	}
	
	public function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('view_ms_pegawai',$limit,$offset);
		}else{
			return $this->db->get('view_ms_pegawai');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('view_ms_pegawai');
		return $this->db->count_all_results(); 
	}

	public function check_hubungan_keluarga($data_id = '',$pegawai_id= '') {
		$this->db->select('*');
		$this->db->where(array('dbo.trx_keluarga.keluarga_hubungan_keluarga_id' => $data_id,
		                       'dbo.trx_keluarga.keluarga_pegawai_id' => $pegawai_id));
		return $this->db->get('dbo.trx_keluarga');
	}
	
}