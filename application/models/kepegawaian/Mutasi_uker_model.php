<?php
class Mutasi_uker_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function getPegawai($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('pegawai_id', $pegawai_id);
		return $this->db->get('view_ms_pegawai');
	}
	// public function getUnitKerja() {
	// 	$this->db->select('*');
	// 	$this->db->where('unit_kerja_status', 1);
	// 	return $this->db->get('ms_unit_kerja');
	// }
	public function getUnitKerja($parent=0,$lvl=3) {
		$this->db->select('*');
		$this->db->order_by('unit_kerja_hirarki', 'asc');
		$this->db->where('unit_kerja_level', $lvl);
		$this->db->where('unit_kerja_parent_id_kode', $parent);
		return $this->db->get('view_ms_unit_kerja');
	}
	public function getListmutasi_uker($pegawai_id = '') {
		$this->db->select('*');
				$this->db->where('mutasi_uker_status', 1);
		$this->db->where('mutasi_uker_pegawai_id', $pegawai_id);
		return $this->db->get('view_trx_mutasi_uker');
	}
	public function create_mutasi_uker($datacreate){
		$this->db->insert('trx_mutasi_unit_kerja', $datacreate);
		return $this->db->insert_id();
	}

	public function update_mutasi_uker($dataupdate, $mutasi_uker_id){
		$this->db->where('mutasi_uker_id', $mutasi_uker_id);
		return $this->db->update('trx_mutasi_unit_kerja', $dataupdate);
	}
	public function getJabatan() {
		$this->db->select('*');
		$this->db->where('jabatan_status', 1);
		return $this->db->get('ms_jabatan');
	}
	public function update_pegawai($updatepegawai, $mutasi_uker_id){
		$this->db->where('pegawai_id', $mutasi_uker_id);
		return $this->db->update('ms_pegawai', $updatepegawai);
	}
	
	public function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('view_ms_pegawai',$limit,$offset);
		}else{
			return $this->db->get('view_ms_pegawai');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('view_ms_pegawai');
		return $this->db->count_all_results(); 
	}
}