<?php
class Dp3_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function getPegawai($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('pegawai_id', $pegawai_id);
		$this->db->limit(20);
		return $this->db->get('view_ms_pegawai');
	}
	public function getAtasan() {
		$this->db->select('*');
		$this->db->where('pegawai_id IS NOT NULL');
		$this->db->limit(100);
		return $this->db->get('view_ms_pegawai');
	}
	public function getListdp3($pegawai_id = '') {
		$this->db->select('*');
				$this->db->where('dp3_status', 1);
		$this->db->where('dp3_pegawai_id', $pegawai_id);
		$this->db->order_by('dp3_tahun_penilaian', 'desc');
		return $this->db->get('view_trx_dp3');
	}
	public function getdp3Data($dp3_id) {
		$this->db->select('*');
		$this->db->where('dp3_id', $dp3_id);
		return $this->db->get('ms_dp3');
	}
	
	public function create_dp3($datacreate){
		$this->db->insert('trx_dp3', $datacreate);
		return $this->db->insert_id();
	}

	public function update_dp3($dataupdate, $dp3_id){
		$this->db->where('dp3_id', $dp3_id);
		return $this->db->update('trx_dp3', $dataupdate);
	}
	
	public function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('view_ms_pegawai',$limit,$offset);
		}else{
			return $this->db->get('view_ms_pegawai');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('view_ms_pegawai');
		return $this->db->count_all_results(); 
	}

	function getDataPegawai($pegawai_id = '',$pegawai_nip = '')
	{	
		if($pegawai_id!=''){
			$this->db->where('pegawai_id', $pegawai_id);
		}
		if($pegawai_nip!=''){
			$this->db->where('pegawai_nip', $pegawai_nip);
		}
		$this->db->limit(500);
		return $this->db->get('view_ms_pegawai');
	}
	function getDataListPegawai($search,$page,$pegawai_id_selected)
	{	
		$pegawai_id = $pegawai_id_selected;
		$this->db->where('pegawai_id <>', $pegawai_id);
		$limit = 5;
		$offset = $page*$limit;
		$likeclause ="(";
		$likeclause .= "UPPER(pegawai_nip) LIKE '%".strtoupper($search)."%' OR ";
		$likeclause .= "UPPER(pegawai_nama_lengkap) LIKE '%".strtoupper($search)."%'";
		$likeclause .=")";
		$this->db->where($likeclause);
		$this->db->limit(500);

		return $this->db->get('view_ms_pegawai',$limit,$offset);
	}
	function getDataListPegawaiCount($search,$pegawai_id_selected)
	{	
		$pegawai_id = $pegawai_id_selected;
		$this->db->where('pegawai_id <>', $pegawai_id);
		$likeclause ="";
		$likeclause ="(";
		$likeclause .= "UPPER(pegawai_nip) LIKE '%".strtoupper($search)."%' OR ";
		$likeclause .= "UPPER(pegawai_nama_lengkap) LIKE '%".strtoupper($search)."%'";
		$likeclause .=")";
		$this->db->where($likeclause);
		$this->db->limit(500);
		
		return $this->db->get('view_ms_pegawai');
	}
}