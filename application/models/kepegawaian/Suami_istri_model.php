<?php
class Suami_istri_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	
	public function getPegawai($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('pegawai_id', $pegawai_id);
		return $this->db->get('view_ms_pegawai');
	}
	public function getListPendidikan($pegawai_id = '') {
		$this->db->select('*');
				$this->db->where('pendidikan_formal_status', 1);
		$this->db->where('pendidikan_formal_pegawai_id', $pegawai_id);
		return $this->db->get('view_trx_pendidikan_formal');
	}
	public function getJenisDiklat(){
		$this->db->select('*');
		$this->db->where('jenis_diklat_status', 1);
		return $this->db->get('ms_jenis_diklat');		
	}
	public function getPendidikan() {
		$this->db->select('*');
		$this->db->where('pendidikan_status', 1);
		return $this->db->get('ms_pendidikan');
	}
	public function getTingkatPendidikan($pendidikan_kode) {
		$this->db->select('jurusan_grup,jurusan_kode');
		$this->db->order_by('jurusan_kode', 'asc');
		$this->db->group_by('jurusan_grup,jurusan_kode');
		$this->db->where('jurusan_pendidikan_kode', $pendidikan_kode);
		$this->db->where("right(jurusan_kode,5) = '00000'");
		$this->db->where('jurusan_status', 1);
		return $this->db->get('ms_jurusan');
	}
	
	public function getDiklatPerjenjangan($pegawai_id){
		$this->db->select('*');
		$this->db->where('diklat_pegawai_id', $pegawai_id);
		$this->db->where('diklat_status', 1);
		return $this->db->get('view_trx_diklatpenyesuaianijazah');		
	}

	public function GetGolongan(){
		$this->db->select('*');
		$this->db->where('golongan_status', 1);
		return $this->db->get('ms_golongan');		
	}

	public function getSuamiIstri($pegawai_id){
		$this->db->select('*');
		$this->db->where('suami_istri_pegawai_id', $pegawai_id);
		$this->db->where('suami_istri_status', 1);
		return $this->db->get('view_trx_suami_istri');		
	}
	
	public function create_suami_istri($datacreate){
		$this->db->insert('trx_suami_istri', $datacreate);
		return $this->db->insert_id();
	}

	public function update_suami_istri($dataupdate, $suami_istri_id){
		$this->db->where('suami_istri_id', $suami_istri_id);
		return $this->db->update('trx_suami_istri', $dataupdate);
	}
	
	public function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('view_ms_pegawai',$limit,$offset);
		}else{
			return $this->db->get('view_ms_pegawai');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('view_ms_pegawai');
		return $this->db->count_all_results(); 
	}
	
}