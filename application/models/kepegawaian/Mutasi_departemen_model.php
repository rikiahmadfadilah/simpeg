<?php
class Mutasi_departemen_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function getPegawai($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('pegawai_id', $pegawai_id);
		return $this->db->get('view_ms_pegawai');
	}
	public function getListmutasi_departemen($pegawai_id = '') {
		$this->db->select('*');
				$this->db->where('mutasi_departemen_status', 1);
		$this->db->where('mutasi_departemen_pegawai_id', $pegawai_id);
		return $this->db->get('view_trx_mutasi_departemen');
	}
	public function create_mutasi_departemen($datacreate){
		$this->db->insert('trx_mutasi_departemen', $datacreate);
		return $this->db->insert_id();
	}

	public function update_mutasi_departemen($dataupdate, $mutasi_departemen_id){
		$this->db->where('mutasi_departemen_id', $mutasi_departemen_id);
		return $this->db->update('trx_mutasi_departemen', $dataupdate);
	}

	public function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('dbo.view_ms_pegawai_less',$limit,$offset);
		}else{
			return $this->db->get('dbo.view_ms_pegawai_less');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('dbo.view_ms_pegawai_less');
		return $this->db->count_all_results(); 
	}
}
