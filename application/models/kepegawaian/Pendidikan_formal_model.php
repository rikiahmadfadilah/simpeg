<?php
class Pendidikan_formal_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function getPegawai($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('pegawai_id', $pegawai_id);
		return $this->db->get('view_ms_pegawai');
	}
	public function getListPendidikan($pegawai_id = '') {
		$this->db->select('*');
				$this->db->where('pendidikan_formal_status', 1);
		$this->db->where('pendidikan_formal_pegawai_id', $pegawai_id);
		$this->db->order_by('pendidikan_formal_pendidikan_id asc','pendidikan_formal_thn_lulus desc');
		return $this->db->get('view_trx_pendidikan_formal');
	}
	public function getPendidikan() {
		$this->db->select('*');
		$this->db->where('pendidikan_status', 1);
		return $this->db->get('ms_pendidikan');
	}
	public function getSekolahSD() {
		$this->db->select('*');
		$this->db->where('sekolah_sd_status', 1);
		$this->db->limit(100);
		return $this->db->get('ms_sekolah_sd');
	}
	public function getSekolahSMP() {
		$this->db->select('*');
		$this->db->where('sekolah_smp_status', 1);
		$this->db->limit(100);
		return $this->db->get('ms_sekolah_smp');
	}
	public function getSekolahSMA() {
		$this->db->select('*');
		$this->db->where('sekolah_sma_status', 1);
		$this->db->limit(100);
		return $this->db->get('ms_sekolah_sma');
	}
	public function getProdi() {
		$this->db->select('*');
		$this->db->where('prodi_status', 1);
		$this->db->limit(100);
		return $this->db->get('ms_prodi');
	}
	public function get_prodi($univ_id,$jenjang) {
		$this->db->select('*');
		$this->db->order_by('prodi_jenjang', 'asc');
		$this->db->order_by('prodi_nama', 'asc');
		$this->db->where('prodi_univ_id', $univ_id);
		// $this->db->like('prodi_jenjang',"%$jenjang%");
		$this->db->where('prodi_jenjang', "$jenjang");
		$this->db->where('prodi_status', 1);
		return $this->db->get('ms_prodi');
	}
	public function getUniv() {
		$this->db->select('*');
		// $this->db->where('univ_status', 1);
		$this->db->limit(100);
		return $this->db->get('view_pencarian_univ');
	}
	public function getTingkatPendidikan($pendidikan_kode) {
		$this->db->select('jurusan_grup,jurusan_kode,jurusan_jenjang');
		$this->db->order_by('jurusan_kode', 'asc');
		$this->db->group_by('jurusan_grup,jurusan_kode,jurusan_jenjang');
		$this->db->where('jurusan_pendidikan_kode', $pendidikan_kode);
		$this->db->where("right(jurusan_kode,5) = '00000'");
		$this->db->where('jurusan_status', 1);
		return $this->db->get('ms_jurusan');
	}
	
	public function getFakultas($jurusan_kode) {
		$this->db->select('*');
		$this->db->order_by('jurusan_nama', 'asc');
		$this->db->where('left(jurusan_kode,2)', substr($jurusan_kode,0,2));
		$this->db->where("right(jurusan_kode,5) <> '00000'");
		$this->db->where("right(jurusan_kode,3) = '000'");
		$this->db->where("jurusan_grup = jurusan_nama");
		$this->db->where('jurusan_status', 1);
		return $this->db->get('ms_jurusan');
	}
	public function getJurusan($jurusan_kode) {
		$this->db->select('*');
		$this->db->order_by('jurusan_kode', 'asc');
		$this->db->where('left(jurusan_kode,4)', substr($jurusan_kode,0,4));
		$this->db->where("right(jurusan_kode,5) <> '00000'");
		$this->db->where("jurusan_grup <> jurusan_nama");
		$this->db->where('jurusan_status', 1);
		return $this->db->get('ms_jurusan');
	}
	public function getProdiUniv($univ_id) {
		$this->db->select('*');
		$this->db->order_by('prodi_univ_id', 'asc');
		// $this->db->group_by('jurusan_grup,jurusan_kode');
		$this->db->where('prodi_univ_id', $univ_id);
		// $this->db->where("right(jurusan_kode,5) = '00000'");
		// $this->db->where('prodi_status', 1);
		return $this->db->get('ms_prodi');
	}
	public function getJurusanData($jurusan_kode) {
		$this->db->select('*');
		$this->db->where('jurusan_kode', $jurusan_kode);
		return $this->db->get('ms_jurusan');
	}
	public function getNamaJurusan($jurusan_id) {
		$this->db->select('*');
		$this->db->where('jurusan_id', $jurusan_id);
		return $this->db->get('ms_jurusan');
	}
	public function getPendidikanData($pendidikan_id) {
		$this->db->select('*');
		$this->db->where('pendidikan_id', $pendidikan_id);
		return $this->db->get('ms_pendidikan');
	}
	
	public function create_pendidikan($datacreate){
		$this->db->insert('trx_pendidikan_formal', $datacreate);
		return $this->db->insert_id();
	}

	public function update_pendidikan($dataupdate, $pendidikan_formal_id){
		$this->db->where('pendidikan_formal_id', $pendidikan_formal_id);
		return $this->db->update('trx_pendidikan_formal', $dataupdate);
	}
	
	public function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('view_ms_pegawai',$limit,$offset);
		}else{
			return $this->db->get('view_ms_pegawai');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('view_ms_pegawai');
		return $this->db->count_all_results(); 
	}
	
	// tambahan muklis
	public function check_tingat_pendidikan($data_id = '',$pegawai_id= '') {
		$this->db->select('*');
		$this->db->where(array('dbo.trx_pendidikan_formal.pendidikan_formal_pendidikan_id' => $data_id,
		                       'dbo.trx_pendidikan_formal.pendidikan_formal_pegawai_id' => $pegawai_id));
		$this->db->where('pendidikan_formal_status', 1);
		return $this->db->get('dbo.trx_pendidikan_formal');
	}
	public function get_short_id($pendidikan_formal_pendidikan_id,$pendidikan_status=1) {
		$this->db->select('pendidikan_sort');		
		$this->db->where(array('dbo.ms_pendidikan.pendidikan_id' => $pendidikan_formal_pendidikan_id,
		                       'dbo.ms_pendidikan.pendidikan_status' => $pendidikan_status));
		return $this->db->get('dbo.ms_pendidikan');
	}	
	public function get_last_pendidikan($pendidikan_formal_pegawai_id) {
		$this->db->select(array('pendidikan_formal_pegawai_id','pendidikan_formal_pendidikan_id','pendidikan_formal_thn_lulus','pendidikan_nama'));		
		$this->db->where(array('dbo.view_trx_pendidikan_formal.pendidikan_formal_pegawai_id'=>$pendidikan_formal_pegawai_id));
		$this->db->order_by('pendidikan_formal_thn_lulus desc','pendidikan_formal_pendidikan_id desc');
		$this->db->limit(1, 0);
		return $this->db->get('dbo.view_trx_pendidikan_formal');
   }	
	function update_pendidikan_last($data, $data_id){
		$this->db->where('pegawai_id', $data_id);
		return $this->db->update('dbo.ms_pegawai', $data);
	}	
	
	function get_sekolah_sd(){
		$this->db->where('sekolah_sd_status', 1);
		return $this->db->get('dbo.ms_sekolah_sd');
	}
	function getDataSD($sekolah_sd_id = '',$sekolah_sd_npsn = '')
	{	
		if($sekolah_sd_id!=''){
			$this->db->where('sekolah_sd_id', $sekolah_sd_id);
		}
		if($sekolah_sd_npsn!=''){
			$this->db->where('sekolah_sd_npsn', $sekolah_sd_npsn);
		}
		$this->db->limit(500);
		return $this->db->get('view_ms_sekolah_sd');
	}
	function getDataListSD($search,$page,$sekolah_sd_id_selected)
	{	
		$sekolah_sd_id = $sekolah_sd_id_selected;
		$this->db->where('sekolah_sd_id <>', $sekolah_sd_id);
		$limit = 5;
		$offset = $page*$limit;
		$likeclause ="(";
		$likeclause .= "UPPER(sekolah_sd_npsn) = '".strtoupper($search)."' OR ";
		$likeclause .= "UPPER(sekolah_sd_nama) LIKE '%".strtoupper($search)."%'";
		$likeclause .=")";
		$this->db->where($likeclause);
		$this->db->limit(500);

		return $this->db->get('view_ms_sekolah_sd',$limit,$offset);
	}
	function getDataListSDCount($search,$sekolah_sd_id_selected)
	{	
		$sekolah_sd_id = $sekolah_sd_id_selected;
		$this->db->where('sekolah_sd_id <>', $sekolah_sd_id);
		$likeclause ="";
		$likeclause ="(";
		$likeclause .= "UPPER(sekolah_sd_npsn) = '".strtoupper($search)."' OR ";
		$likeclause .= "UPPER(sekolah_sd_nama) LIKE '%".strtoupper($search)."%'";
		$likeclause .=")";
		$this->db->where($likeclause);
		$this->db->limit(500);

		return $this->db->get('view_ms_sekolah_sd');
	}
	function getDataSMP($sekolah_smp_id = '',$sekolah_smp_npsn = '')
	{	
		if($sekolah_smp_id!=''){
			$this->db->where('sekolah_smp_id', $sekolah_smp_id);
		}
		if($sekolah_smp_npsn!=''){
			$this->db->where('sekolah_smp_npsn', $sekolah_smp_npsn);
		}
		$this->db->limit(500);
		return $this->db->get('view_ms_sekolah_smp');
	}
	function getDataListSMP($search,$page,$sekolah_smp_id_selected)
	{	
		$sekolah_smp_id = $sekolah_smp_id_selected;
		$this->db->where('sekolah_smp_id <>', $sekolah_smp_id);
		$limit = 50;
		$offset = $page*$limit;
		$likeclause ="(";
		$likeclause .= "UPPER(sekolah_smp_npsn) = '".strtoupper($search)."' OR ";
		$likeclause .= "UPPER(sekolah_smp_nama) LIKE '%".strtoupper($search)."%'";
		$likeclause .=")";
		$this->db->where($likeclause);
		$this->db->limit(500);

		return $this->db->get('view_ms_sekolah_smp',$limit,$offset);
	}
	function getDataListSMPCount($search,$sekolah_smp_id_selected)
	{	
		$sekolah_smp_id = $sekolah_smp_id_selected;
		$this->db->where('sekolah_smp_id <>', $sekolah_smp_id);
		$likeclause ="";
		$likeclause ="(";
		$likeclause .= "UPPER(sekolah_smp_npsn) = '".strtoupper($search)."' OR ";
		$likeclause .= "UPPER(sekolah_smp_nama) LIKE '%".strtoupper($search)."%'";
		$likeclause .=")";
		$this->db->where($likeclause);
		$this->db->limit(500);
		
		return $this->db->get('view_ms_sekolah_smp');
	}
	function getDataSMA($sekolah_sma_id = '',$sekolah_sma_npsn = '')
	{	
		if($sekolah_sma_id!=''){
			$this->db->where('sekolah_sma_id', $sekolah_sma_id);
		}
		if($sekolah_sma_npsn!=''){
			$this->db->where('sekolah_sma_npsn', $sekolah_sma_npsn);
		}
		$this->db->limit(500);
		return $this->db->get('view_ms_sekolah_sma');
	}
	function getDataListSMA($search,$page,$sekolah_sma_id_selected)
	{	
		$sekolah_sma_id = $sekolah_sma_id_selected;
		$this->db->where('sekolah_sma_id <>', $sekolah_sma_id);
		$limit = 50;
		$offset = $page*$limit;
		$likeclause ="(";
		$likeclause .= "UPPER(sekolah_sma_npsn) = '".strtoupper($search)."' OR ";
		$likeclause .= "UPPER(sekolah_sma_nama) LIKE '%".strtoupper($search)."%'";
		$likeclause .=")";
		$this->db->where($likeclause);
		$this->db->limit(500);

		return $this->db->get('view_ms_sekolah_sma',$limit,$offset);
	}
	function getDataListSMACount($search,$sekolah_sma_id_selected)
	{	
		$sekolah_sma_id = $sekolah_sma_id_selected;
		$this->db->where('sekolah_sma_id <>', $sekolah_sma_id);
		$likeclause ="";
		$likeclause ="(";
		$likeclause .= "UPPER(sekolah_sma_npsn) = '".strtoupper($search)."' OR ";
		$likeclause .= "UPPER(sekolah_sma_nama) LIKE '%".strtoupper($search)."%'";
		$likeclause .=")";
		$this->db->where($likeclause);
		$this->db->limit(500);
		
		return $this->db->get('view_ms_sekolah_sma');
	}
	function getDataUNIV($univ_id = '',$univ_npsn = '')
	{	
		if($univ_id!=''){
			$this->db->where('univ_id', $univ_id);
		}
		if($univ_npsn!=''){
			$this->db->where('univ_npsn', $univ_npsn);
		}
		$this->db->limit(500);
		return $this->db->get('view_ms_univ');
	}
	function getDataListUNIV($search,$page,$univ_id_selected)
	{	
		$univ_id = $univ_id_selected;
		$this->db->where('univ_id <>', $univ_id);
		$limit = 5;
		$offset = $page*$limit;
		$likeclause ="(";
		$likeclause .= "UPPER(univ_npsn) = '".strtoupper($search)."' OR ";
		$likeclause .= "UPPER(univ_nama) LIKE '%".strtoupper($search)."%'";
		$likeclause .=")";
		$this->db->where($likeclause);
		$this->db->limit(500);

		return $this->db->get('view_ms_univ',$limit,$offset);
	}
	function getDataListUNIVCount($search,$univ_id_selected)
	{	
		$univ_id = $univ_id_selected;
		$this->db->where('univ_id <>', $univ_id);
		$likeclause ="";
		$likeclause ="(";
		$likeclause .= "UPPER(univ_npsn) = '".strtoupper($search)."' OR ";
		$likeclause .= "UPPER(univ_nama) LIKE '%".strtoupper($search)."%'";
		$likeclause .=")";
		$this->db->where($likeclause);
		$this->db->limit(500);
		
		return $this->db->get('view_ms_univ');
	}
}