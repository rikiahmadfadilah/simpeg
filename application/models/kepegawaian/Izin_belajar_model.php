<?php
class Izin_belajar_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function getPegawai($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('pegawai_id', $pegawai_id);
		return $this->db->get('view_ms_pegawai');
	}
	public function getListIzin_belajar($pegawai_id = '') {
		$this->db->select('*');
				$this->db->where('izin_belajar_status', 1);
		$this->db->where('izin_belajar_pegawai_id', $pegawai_id);
		return $this->db->get('view_trx_izin_belajar');
	}
	public function getTingkat() {
		$this->db->select('*');
		$this->db->where('pendidikan_status', 1);
		return $this->db->get('ms_pendidikan');
	}
	public function getNegara() {
		$this->db->select('*');
		$this->db->where('negara_status', 1);
		return $this->db->get('ms_negara');
	}
	public function getTingkatPendidikan($pendidikan_kode) {
		$this->db->select('jurusan_grup,jurusan_kode');
		$this->db->order_by('jurusan_kode', 'asc');
		$this->db->group_by('jurusan_grup,jurusan_kode');
		$this->db->where('jurusan_pendidikan_kode', $pendidikan_kode);
		$this->db->where("right(jurusan_kode,5) = '00000'");
		$this->db->where('jurusan_status', 1);
		return $this->db->get('ms_jurusan');
	}
	
	public function getFakultas($jurusan_kode) {
		$this->db->select('*');
		$this->db->order_by('jurusan_nama', 'asc');
		$this->db->where('left(jurusan_kode,2)', substr($jurusan_kode,0,2));
		$this->db->where("right(jurusan_kode,5) <> '00000'");
		$this->db->where("right(jurusan_kode,3) = '000'");
		$this->db->where("jurusan_grup = jurusan_nama");
		$this->db->where('jurusan_status', 1);
		return $this->db->get('ms_jurusan');
	}
	public function getJurusan($jurusan_kode) {
		$this->db->select('*');
		$this->db->order_by('jurusan_kode', 'asc');
		$this->db->where('left(jurusan_kode,4)', substr($jurusan_kode,0,4));
		$this->db->where("right(jurusan_kode,5) <> '00000'");
		$this->db->where("jurusan_grup <> jurusan_nama");
		$this->db->where('jurusan_status', 1);
		return $this->db->get('ms_jurusan');
	}
	public function getJurusanData($jurusan_kode) {
		$this->db->select('*');
		$this->db->where('jurusan_kode', $jurusan_kode);
		return $this->db->get('ms_jurusan');
	}
	public function getNamaJurusan($jurusan_id) {
		$this->db->select('*');
		$this->db->where('jurusan_id', $jurusan_id);
		return $this->db->get('ms_jurusan');
	}
	
	public function create_izin_belajar($datacreate){
		$this->db->insert('trx_izin_belajar', $datacreate);
		return $this->db->insert_id();
	}

	public function update_izin_belajar($dataupdate, $izin_belajar_id){
		$this->db->where('izin_belajar_id', $izin_belajar_id);
		return $this->db->update('trx_izin_belajar', $dataupdate);
	}
	
	public function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('view_ms_pegawai',$limit,$offset);
		}else{
			return $this->db->get('view_ms_pegawai');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('view_ms_pegawai');
		return $this->db->count_all_results(); 
	}
}