<?php
class Pns_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function getDiklat() {
		$this->db->select('*');
		$this->db->where('diklat_jabatan_status', 1);
		return $this->db->get('ms_diklat_jabatan');
	}
	function createpegawai($datacreate){
		$this->db->insert('ms_pegawai', $datacreate);
		return $this->db->insert_id();
	}
	function updatepegawai($dataupdate, $pegawai_id){
		$this->db->where('pegawai_id', $pegawai_id);
		return $this->db->update('ms_pegawai', $dataupdate);
	}
	public function getJenisDiklat(){
		$this->db->select('*');
		$this->db->where('jenis_diklat_status', 1);
		return $this->db->get('ms_jenis_diklat');		
	}
	public function checkNipExist($pegawai_nip){
		$this->db->select('*');
		$this->db->where('pegawai_nip', str_replace(' ','',$pegawai_nip));
		$this->db->where('pegawai_status', 1);
		return $this->db->get('ms_pegawai');	
	}
	public function checkKTPExist($pegawai_nomor_ktp){
		$this->db->select('*');
		$this->db->where('pegawai_nomor_ktp', str_replace(' ','',$pegawai_nomor_ktp));
		$this->db->where('pegawai_status', 1);
		return $this->db->get('ms_pegawai');	
	}
	public function checkNPWPExist($pegawai_nomor_npwp){
		$this->db->select('*');
		$this->db->where('pegawai_nomor_npwp', str_replace(' ','',$pegawai_nomor_npwp));
		$this->db->where('pegawai_status', 1);
		return $this->db->get('ms_pegawai');	
	}
	public function getPendidikan() {
		$this->db->select('*');
		$this->db->order_by('pendidikan_kode', 'asc');
		$this->db->where('pendidikan_status', 1);
		return $this->db->get('ms_pendidikan');
	}
	public function getProvinsi() {
		$this->db->select('*');
		$this->db->order_by('provinsi_nama', 'asc');
		$this->db->where('provinsi_status', 1);
		return $this->db->get('ms_provinsi');
	}
	public function getKota($provinsi_kode) {
		$this->db->select('*');
		$this->db->order_by('kota_nama', 'asc');
		$this->db->where('kota_provinsi_kode', $provinsi_kode);
		$this->db->where('kota_status', 1);
		return $this->db->get('ms_kota');
	}
	public function getKecamatan($kota_kode) {
		$this->db->select('*');
		$this->db->order_by('kecamatan_nama', 'asc');
		$this->db->where('kecamatan_kota_kode', $kota_kode);
		$this->db->where('kecamatan_status', 1);
		return $this->db->get('ms_kecamatan');
	}
	public function getKelurahan($kecamatan_kode) {
		$this->db->select('*');
		$this->db->order_by('kelurahan_nama', 'asc');
		$this->db->where('kelurahan_kecamatan_kode', $kecamatan_kode);
		$this->db->where('kelurahan_status', 1);
		return $this->db->get('ms_kelurahan');
	}
	public function getJurusan($jurusan_kode) {
		$this->db->select('*');
		$this->db->order_by('jurusan_kode', 'asc');
		$this->db->where('left(jurusan_kode,2)', substr($jurusan_kode,0,2));
		$this->db->where("right(jurusan_kode,5) <> '00000'");
		$this->db->where('jurusan_status', 1);
		return $this->db->get('ms_jurusan');
	}
	public function getUnitKerja($parent=0,$lvl=3) {
		$this->db->select('*');
		$this->db->order_by('unit_kerja_kode', 'asc');
		$this->db->where('unit_kerja_level', $lvl);
		$this->db->where('unit_kerja_parent_id_kode', $parent);
		return $this->db->get('view_ms_unit_kerja');
	}
	public function getStatusPegawai() {
		$this->db->select('*');
		$this->db->order_by('status_pegawai_kode', 'asc');
		$this->db->where('status_pegawai_status', 1);
		return $this->db->get('ms_status_pegawai');
	}
	public function getFungsionalTertentu() {
		$this->db->select('*');
		$this->db->order_by('fungsional_kode', 'asc');
		$this->db->where('fungsional_status', 1);
		$this->db->where("right(fungsional_kode,2)='00'");
		
		return $this->db->get('ms_fungsional');
	}
	public function getFormasiFungsionalTertentu() {
		$this->db->select('*');
		$this->db->where('formasi_fung_cpns_status', 1);
		return $this->db->get('ms_formasi_fung_cpns');
	}
	public function getGolongan() {
		$this->db->select('*');
		$this->db->where('golongan_status', 1);
		return $this->db->get('ms_golongan');
	}
	public function getJabatan() {
		$this->db->select('*');
		$this->db->where('jabatan_status', 1);
		return $this->db->get('ms_jabatan');
	}
	public function getTingkatPendidikan($pendidikan_kode) {
		$this->db->select('jurusan_grup,jurusan_kode');
		$this->db->order_by('jurusan_kode', 'asc');
		$this->db->group_by('jurusan_grup,jurusan_kode');
		$this->db->where('jurusan_pendidikan_kode', $pendidikan_kode);
		$this->db->where("right(jurusan_kode,5) = '00000'");
		$this->db->where('jurusan_status', 1);
		return $this->db->get('ms_jurusan');
	}
	public function getJenisKelamin() {
		$this->db->select('*');
		$this->db->where('jenis_kelamin_status', 1);
		return $this->db->get('ms_jenis_kelamin');
	}
	public function getStatusKawin() {
		$this->db->select('*');
		$this->db->where('perkawinan_status', 1);
		return $this->db->get('ms_status_perkawinan');
	}
	public function getNegara() {
		$this->db->select('*');
		$this->db->where('negara_status', 1);
		return $this->db->get('ms_negara');
	}
	public function getAgama() {
		$this->db->select('*');
		$this->db->where('agama_status', 1);
		return $this->db->get('ms_agama');
	}
	public function getGolonganDarah() {
		$this->db->select('*');
		$this->db->where('golongan_darah_status', 1);
		return $this->db->get('ms_golongan_darah');
	}
	public function getPegawai($pegawai_id) {
		$this->db->select('*');
		$this->db->where('pegawai_id', $pegawai_id);
		$this->db->join('view_ms_pegawai_unit_kerja', 'pegawai_id = unit_kerja_pegawai_id');
		return $this->db->get('view_ms_pegawai');
	}
	
	function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}

		// if($status ='pegawai_status = 1 AND pegawai_is_non_pns = 1 AND pegawai_is_pns = 1'){
		// 	$this->db->where($status);
		// }
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('view_ms_pegawai',$limit,$offset);
		}else{
			return $this->db->get('view_ms_pegawai');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}

		// if($status ='pegawai_status = 1 AND pegawai_is_non_pns = 1 AND pegawai_is_pns = 1'){
		// 	$this->db->where($status);
		// }
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('view_ms_pegawai');
		return $this->db->count_all_results(); 
	}
}