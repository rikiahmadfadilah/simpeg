<?php
class Hukuman_disiplin_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function getPegawai($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('pegawai_id', $pegawai_id);
		return $this->db->get('view_ms_pegawai');
	}
	public function getListHukuman_disiplin($pegawai_id = '') {
		$this->db->select('*');
				$this->db->where('hukuman_disiplin_status', 1);
		$this->db->where('hukuman_disiplin_pegawai_id', $pegawai_id);
		return $this->db->get('view_trx_hukuman_disiplin');
	}
	public function getPelanggaranPP() {
		$this->db->select('*');
		$this->db->where('pp_pelanggaran_status', 1);
		return $this->db->get('ms_pp_pelanggaran');
	}
	public function getJenisHukuman() {
		$this->db->select('*');
		$this->db->where('jenis_hukuman_status', 1);
		return $this->db->get('ms_jenis_hukuman');
	}
	public function getJenisPejabat() {
		$this->db->select('*');
		$this->db->where('jenis_pejabat_berwenang_status', 1);
		return $this->db->get('ms_jenis_pejabat_berwenang');
	}
	public function getJenisPenyKeputusan() {
		$this->db->select('*');
		$this->db->where('peny_keputusan_hukuman_status', 1);
		return $this->db->get('ms_jenis_penyampaian_keputusan_hukuman');
	}
	public function getUpayaAdm() {
		$this->db->select('*');
		$this->db->where('upaya_adm_hukuman_status', 1);
		return $this->db->get('ms_upaya_adm_hukuman');
	}
	public function getStatusHukuman() {
		$this->db->select('*');
		$this->db->where('status_hukuman_status', 1);
		return $this->db->get('ms_status_hukuman');
	}
	public function create_hukuman_disiplin($datacreate){
		$this->db->insert('trx_hukuman_disiplin', $datacreate);
		return $this->db->insert_id();
	}

	public function update_hukuman_disiplin($dataupdate, $hukuman_disiplin_id){
		$this->db->where('hukuman_disiplin_id', $hukuman_disiplin_id);
		return $this->db->update('trx_hukuman_disiplin', $dataupdate);
	}
	
	public function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('view_ms_pegawai',$limit,$offset);
		}else{
			return $this->db->get('view_ms_pegawai');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('view_ms_pegawai');
		return $this->db->count_all_results(); 
	}
}