<?php
class Kemampuan_bahasa_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function getPegawai($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('pegawai_id', $pegawai_id);
		return $this->db->get('view_ms_pegawai');
	}
	public function getListKemampuan_bahasa($pegawai_id = '') {
		$this->db->select('*');
				$this->db->where('kemampuan_bahasa_status', 1);
		$this->db->where('kemampuan_bahasa_pegawai_id', $pegawai_id);
		return $this->db->get('view_trx_kemampuan_bahasa');
	}
	public function getBahasa() {
		$this->db->select('*');
		$this->db->where('bahasa_status', 1);
		return $this->db->get('ms_bahasa');
	}
	public function create_kemampuan_bahasa($datacreate){
		$this->db->insert('trx_kemampuan_bahasa', $datacreate);
		return $this->db->insert_id();
	}

	public function update_kemampuan_bahasa($dataupdate, $kemampuan_bahasa_id){
		$this->db->where('kemampuan_bahasa_id', $kemampuan_bahasa_id);
		return $this->db->update('trx_kemampuan_bahasa', $dataupdate);
	}
	
	public function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('view_ms_pegawai',$limit,$offset);
		}else{
			return $this->db->get('view_ms_pegawai');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('view_ms_pegawai');
		return $this->db->count_all_results(); 
	}
}