<?php
class Jabatan_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function getPendidikan() {
		$this->db->select('*');
		$this->db->order_by('pendidikan_kode', 'asc');
		$this->db->where('pendidikan_status', 1);
		return $this->db->get('ms_pendidikan');
	}
	public function getProvinsi() {
		$this->db->select('*');
		$this->db->order_by('provinsi_nama', 'asc');
		$this->db->where('provinsi_status', 1);
		return $this->db->get('Master.provinsi');
	}
	public function getKota($provinsi_kode) {
		$this->db->select('*');
		$this->db->order_by('kota_nama', 'asc');
		$this->db->where('kota_provinsi_kode', $provinsi_kode);
		$this->db->where('kota_status', 1);
		return $this->db->get('ms_kota');
	}
	public function getJurusan($jurusan_kode) {
		$this->db->select('*');
		$this->db->order_by('jurusan_kode', 'asc');
		$this->db->where('left(jurusan_kode,2)', substr($jurusan_kode,0,2));
		$this->db->where("right(jurusan_kode,5) <> '00000'");
		$this->db->where('jurusan_status', 1);
		return $this->db->get('ms_jurusan');
	}
	public function getUnitKerja($parent,$lvl) {
		$this->db->select('*');
		$this->db->order_by('unit_kerja_hirarki', 'asc');
		//$this->db->where('unit_kerja_level', 2);
		return $this->db->get('view_ms_unit_kerja');
	}
	public function GetJabatan() {
		$this->db->select('*');		
		$this->db->where('jabatan_status', 1);
		return $this->db->get('ms_jabatan');
	}

	public function GetEselon() {
		$this->db->select('*');		
		$this->db->where('eselon_status', 1);
		return $this->db->get('ms_eselon');
	}
	public function getTingkatPendidikan($pendidikan_kode) {
		$this->db->select('jurusan_grup,jurusan_kode');
		$this->db->order_by('jurusan_kode', 'asc');
		$this->db->group_by('jurusan_grup,jurusan_kode');
		$this->db->where('jurusan_pendidikan_kode', $pendidikan_kode);
		$this->db->where("right(jurusan_kode,5) = '00000'");
		$this->db->where('jurusan_status', 1);
		return $this->db->get('ms_jurusan');
	}
	public function getJenisKelamin() {
		$this->db->select('*');
		$this->db->where('jenis_kelamin_status', 1);
		return $this->db->get('ms_jenis_kelamin');
	}
	public function getStatusKawin() {
		$this->db->select('*');
		$this->db->where('perkawinan_status', 1);
		return $this->db->get('ms_status_perkawinan');
	}
	public function getNegara() {
		$this->db->select('*');
		$this->db->where('negara_status', 1);
		return $this->db->get('ms_negara');
	}
	public function getAgama() {
		$this->db->select('*');
		$this->db->where('agama_status', 1);
		return $this->db->get('ms_agama');
	}
	public function getGolonganDarah() {
		$this->db->select('*');
		$this->db->where('golongan_darah_status', 1);
		return $this->db->get('ms_golongan_darah');
	}
	function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('dbo.view_ms_pegawai_less',$limit,$offset);
		}else{
			return $this->db->get('dbo.view_ms_pegawai_less');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('dbo.view_ms_pegawai_less');
		return $this->db->count_all_results(); 
	}

	function get_list_data_jabatan($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('trx_jenjang_jabatan',$limit,$offset);
		}else{
			return $this->db->get('trx_jenjang_jabatan');
		}
		
	}

	function get_count_all_data_jabatan($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('trx_jenjang_jabatan');
		return $this->db->count_all_results(); 
	}

	function AddJabatan($data_jabatan){
		$this->db->insert('trx_jenjang_jabatan', $data_jabatan);
		return $this->db->insert_id();
	}

	function GetJabatanlist($param2){
		$this->db->select('*');
		$this->db->where('jenjang_jabatan_pegawai_nip', $param2);
		return $this->db->get('trx_jenjang_jabatan');
	}

}
