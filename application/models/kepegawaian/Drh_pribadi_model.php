<?php class Drh_pribadi_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function getPegawai($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('pegawai_id', $pegawai_id);
		return $this->db->get('view_detail_ms_pegawai');
	}
	public function getDetailPegawai($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('pegawai_id', $pegawai_id);
		return $this->db->get('view_detail_ms_pegawai');
	}
	public function getPendidikanFormal($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('pendidikan_formal_pegawai_id', $pegawai_id);
		$this->db->where('pendidikan_formal_status', 1);
		return $this->db->get('view_trx_pendidikan_formal');
	}
	public function getPendidikanNonFormal($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('pendidikan_nonformal_pegawai_id', $pegawai_id);
		$this->db->where('pendidikan_nonformal_status', 1);
		return $this->db->get('view_trx_pendidikan_nonformal');
	}
	public function getKepangkatan($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('kepangkatan_pegawai_id', $pegawai_id);
		$this->db->where('kepangkatan_status', 1);
		$this->db->order_by('kepangkatan_tanggal_sk', 'desc');
		return $this->db->get('view_trx_kepangkatan');
	}
	public function getJabatan($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('jenjang_jabatan_pegawai_id', $pegawai_id);
		$this->db->where('jenjang_jabatan_status', 1);
		$this->db->order_by('jenjang_jabatan_tanggal_mulai', 'desc');
		return $this->db->get('view_trx_jenjang_jabatan');
	}
	public function getDiklatP($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('diklat_pegawai_id', $pegawai_id);
		$this->db->where('diklat_status', 1);
		return $this->db->get('view_trx_diklat_perjenjangan');
	}
	public function getSuamiIstri($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('suami_istri_pegawai_id', $pegawai_id);
		$this->db->where('suami_istri_status', 1);
		return $this->db->get('view_trx_suami_istri');
	}
	public function getAnak($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('anak_pegawai_id', $pegawai_id);
		$this->db->where('anak_status', 1);
		return $this->db->get('view_trx_anak');
	}
	public function getKeluarga($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('keluarga_pegawai_id', $pegawai_id);
		$this->db->where('keluarga_status', 1);
		return $this->db->get('view_trx_keluarga');
	}
	public function getSeminar($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('seminar_pegawai_id', $pegawai_id);
		$this->db->where('seminar_status', 1);
		return $this->db->get('view_trx_seminar');
	}
	public function getPenghargaan($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('penghargaan_pegawai_id', $pegawai_id);
		$this->db->where('penghargaan_status', 1);
		return $this->db->get('view_trx_penghargaan');
	}
	public function getHukumanDisiplin($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('hukuman_disiplin_pegawai_id', $pegawai_id);
		$this->db->where('hukuman_disiplin_status', 1);
		return $this->db->get('view_trx_hukuman_disiplin');
	}
	public function getDp3($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('dp3_pegawai_id', $pegawai_id);
		$this->db->where('dp3_status', 1);
		return $this->db->get('view_trx_dp3');
	}
	public function getPerilaku($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('skp_perilaku_penilai_pegawai_id', $pegawai_id);
		return $this->db->get('view_trx_skp_perilaku');
	}
	public function getUrut($pegawai_nip = '') {
		return $this->db->query("EXEC no_urut @nip = '$pegawai_nip'");
	}
}