<?php
class Init_api_model extends CI_Model{

	public function __construct(){
		parent:: __construct();
	}

	public function check_app_code($app_code) {
		$this->db->where('api_device_app_code', $app_code);
		return $this->db->get('ms_api_device');
	}

	public function update_api_device_date($api_id) {
		$this->db->where('api_device_id', $api_id);
		//$this->db->set('api_device_last_access_date', "to_date('".date('Y-m-d H:i:s', time())."','yyyy-mm-dd hh24:mi:ss')", false);
		$this->db->set('api_device_last_access_date', date('Y-m-d H:i:s', time()));
		return $this->db->update('ms_api_device');
	}

	public function get_access_token_last_id() {
		$this->db->select('api_token_id');
		$this->db->order_by('api_token_id', 'DESC');
		return $this->db->get('trx_api_token');
	}

	public function add_access_token($app_id, $api_token, $plustime) {
		//$this->db->set('api_token_id', 'SEQ_trx_api_token.NEXTVAL', false);
		$this->db->set('api_token_device_id', $app_id);
		$this->db->set('api_token_code', $api_token);
		//$this->db->set('api_token_valid_date', "to_date('".date('Y-m-d H:i:s', strtotime('+1 years'))."','yyyy-mm-dd hh24:mi:ss')", false);
		$this->db->set('api_token_valid_date', date('Y-m-d H:i:s', strtotime('+1 years')));
		$this->db->set('api_token_ip_address', $_SERVER['REMOTE_ADDR']);
		return $this->db->insert('trx_api_token');
	}

	public function update_valid_token($access_token, $plustime) {
		$this->db->where('api_token_code', $access_token);
		//$this->db->set('api_token_valid_date', "to_date('".date('Y-m-d H:i:s', strtotime('+1 years'))."','yyyy-mm-dd hh24:mi:ss')", false);
		$this->db->set('api_token_valid_date', date('Y-m-d H:i:s', strtotime('+1 years')));
		return $this->db->update('trx_api_token');
	}

	public function update_valid_token_stay($access_token, $plustime) {
		$this->db->where('api_token_code', $access_token);
		//$this->db->set('api_token_valid_date', "to_date('2020-12-31 23:59:59','yyyy-mm-dd hh24:mi:ss')", false);
		$this->db->set('api_token_valid_date', date('Y-m-d H:i:s', strtotime('+1 years')));
		return $this->db->update('trx_api_token');
	}

	public function check_token($access_token) {
		//$this->db->select("to_char(api_token_valid_date, 'yyyy-mm-dd hh24:mi:ss') as \"date\"", false);
		$this->db->select("api_token_valid_date as \"date\"", false);
		$this->db->where('api_token_code', $access_token);
		$this->db->where('api_token_ip_address', $_SERVER['REMOTE_ADDR']);
		return $this->db->get('trx_api_token');
	}
	public function check_version($access_token) {
		//$this->db->select("to_char(api_token_valid_date, 'yyyy-mm-dd hh24:mi:ss') as \"date\"", false);
		$this->db->select("api_token_valid_date as \"date\"", false);
		$this->db->where('api_token_code', $access_token);
		$this->db->where('api_token_ip_address', $_SERVER['REMOTE_ADDR']);
		return $this->db->get('trx_api_token');
	}
	
}