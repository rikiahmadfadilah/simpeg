<?php
class Rekap_usia_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	
	function getRekapUsia(){
		$qry = ("SELECT
					a.nama_unker,
					SUBSTRING( a.kode_unker, 1, 4 ) AS substr_kode_unker,
					a.kode_unker,
					a.eselon_unker,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND USIA > 56) AS lk_56,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND USIA >= 46 AND USIA <= 55) AS lk_46_55,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND USIA >= 36 AND USIA <= 45) AS lk_36_45,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND USIA >= 26 AND USIA <= 35) AS lk_26_35,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND USIA < 25) AS lk_25,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )) AS lk_jml,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND USIA > 56) AS pr_56,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND USIA >= 46 AND USIA <= 55) AS pr_46_55,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND USIA >= 36 AND USIA <= 45) AS pr_36_45,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND USIA >= 26 AND USIA <= 35) AS pr_26_35,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND USIA < 25) AS pr_25,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )) AS pr_jml,
					(SELECT count(*) FROM view_dt_dasar WHERE SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND USIA > 56) AS jml_56,
					(SELECT count(*) FROM view_dt_dasar WHERE SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND USIA >= 46 AND USIA <= 55) AS jml_46_55,
					(SELECT count(*) FROM view_dt_dasar WHERE SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND USIA >= 36 AND USIA <= 45) AS jml_36_45,
					(SELECT count(*) FROM view_dt_dasar WHERE SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND USIA >= 26 AND USIA <= 35) AS jml_26_35,
					(SELECT count(*) FROM view_dt_dasar WHERE SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND USIA < 25) AS jml_25,
					(SELECT count(*) FROM view_dt_dasar WHERE SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )) AS jml
					
				FROM
					unker AS a 
				WHERE
					a.kode_unker <> '0000000000' 
					AND a.eselon_unker <> '00' 
					AND a.eselon_unker < '3A'
				ORDER BY
					a.kode_unker ASC
		");
		$result = $this->db->query($qry);
		return $result;
	}
}