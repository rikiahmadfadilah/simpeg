<?php
class Daftar_pejabat_fungsional_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function get_unit_kerja($unit_kerja_kode = '') {
		$this->db->select('*');
		if($unit_kerja_kode!=""){
			$this->db->where('unit_kerja_kode', $unit_kerja_kode);
		}
		$this->db->order_by('unit_kerja_kode ASC');
		return $this->db->get('ms_unit_kerja');
	}	
	public function getPegawai($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('pegawai_id', $pegawai_id);
		return $this->db->get('view_detail_ms_pegawai');
	}
	public function getDetailPegawai($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('pegawai_id', $pegawai_id);
		return $this->db->get('view_detail_ms_pegawai');
	}
	public function getPendidikanFormal($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('pendidikan_formal_pegawai_id', $pegawai_id);
		return $this->db->get('view_trx_pendidikan_formal');
	}
	public function getPendidikanNonFormal($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('pendidikan_nonformal_pegawai_id', $pegawai_id);
		return $this->db->get('view_trx_pendidikan_nonformal');
	}
	public function getKepangkatans($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('kepangkatan_pegawai_id', $pegawai_id);
		return $this->db->get('view_trx_kepangkatan');
	}
	public function getJabatan($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('jenjang_jabatan_pegawai_id', $pegawai_id);
		return $this->db->get('view_trx_jenjang_jabatan');
	}
	public function getDiklatP($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('diklat_pegawai_id', $pegawai_id);
		return $this->db->get('view_trx_diklat_perjenjangan');
	}
	public function getSuamiIstri($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('suami_istri_pegawai_id', $pegawai_id);
		return $this->db->get('view_trx_suami_istri');
	}
	public function getAnak($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('anak_pegawai_id', $pegawai_id);
		return $this->db->get('view_trx_anak');
	}
	public function getKeluarga($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('keluarga_pegawai_id', $pegawai_id);
		return $this->db->get('view_trx_keluarga');
	}
	public function getSeminar($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('seminar_pegawai_id', $pegawai_id);
		return $this->db->get('view_trx_seminar');
	}
	public function getPenghargaan($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('penghargaan_pegawai_id', $pegawai_id);
		return $this->db->get('view_trx_penghargaan');
	}
	public function getHukumanDisiplin($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('hukuman_disiplin_pegawai_id', $pegawai_id);
		return $this->db->get('view_trx_hukuman_disiplin');
	}
	public function getDp3($pegawai_id = '') {
		$this->db->select('*');
		$this->db->where('dp3_pegawai_id', $pegawai_id);
		return $this->db->get('view_trx_dp3');
	}
	public function getEselon() {
		$this->db->select('*');
		$this->db->where('eselon_status',1);
		return $this->db->get('view_ms_eselon');
	}
	public function getGolongan() {
		$this->db->select('*');
		$this->db->where('golongan_status',1);
		return $this->db->get('ms_golongan');
	}
	public function getStatusPegawai() {
		$this->db->select('*');
		$this->db->where('status_pegawai_status',1);
		return $this->db->get('ms_status_pegawai');
	}
	public function getUrutKepangkatan($unit_1 = '',$pegawai_golongan_id = '',$pegawai_golongan_id2 = '',$eselon_id = '',$eselon_id2 = '',$pegawai_status_pegawai_id = '',$umur = '',$umur2 = '',$search='') {
		$this->db->select('*');
		$where = "pegawai_status = 1";
        if($unit_1)$where .= " and pegawai_status = 1 and pegawai_is_pns = 1 and (pegawai_jenis_pensiun_id is null or pegawai_jenis_pensiun_id = 0) and (unit_kerja_kode_hirarki LIKE '%". $unit_1."%')";
		if($pegawai_golongan_id)$where .= " and pegawai_golongan_id >= '".$pegawai_golongan_id."'";
		if($pegawai_golongan_id2)$where .= " and pegawai_golongan_id <= '".$pegawai_golongan_id2."'";
		if($eselon_id)$where .= " and pegawai_jabatan_id >= '".$eselon_id."'";
		if($eselon_id2)$where .= " and pegawai_jabatan_id <= '".$eselon_id2."'";
		if($pegawai_status_pegawai_id)$where .= " and pegawai_status_pegawai_id = '".$pegawai_status_pegawai_id."'";
		if($umur)$where .= " and umur >= '".$umur."'";
		if($umur2)$where .= " and umur <= '".$umur2."'";

		$this->db->where($where);
		$this->db->order_by('golongan_kode desc, pegawai_tanggal_tmt asc, jabatan_kode asc, pegawai_tanggal_awal_tmt_jabatan asc, pegawai_tanggal_tmt_jabatan asc, pegawai_tahun_masa_kerja_golongan desc, pegawai_diklat_jenis_id asc, pendidikan_kode desc, pegawai_tanggal_lahir asc');
		
		return $this->db->get('view_detail_ms_pegawai');
	}	
	public function getDetailKepangkatan($unit_1 = '',$search='') {
		$this->db->select('*');
		$where = "pegawai_status = 1";
		if($unit_1)$where .= " and unit_kerja_kode = '".$unit_1."'";
		
		$this->db->where($where);
		
		
		return $this->db->get('view_detail_ms_pegawai');
	}		
	public function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('view_detail_ms_pegawai',$limit,$offset);
		}else{
			return $this->db->get('view_detail_ms_pegawai');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('view_detail_ms_pegawai');
		return $this->db->count_all_results(); 
	}
	public function getUnitKerja(){
		$this->db->where('unit_kerja_level > 1');
		$this->db->where('unit_kerja_level <= 3');
		$this->db->where_not_in('unit_kerja_kode', array('0100000100'));
		$this->db->order_by('unit_kerja_kode', "asc");
		$this->db->order_by('unit_kerja_hirarki', "asc");
		return $this->db->get('dbo.view_ms_unit_kerja');
	}
	public function getjabatanfungsional(){
		$query = "select *, LEFT(jabatan_kode,2) AS kode from ms_jabatan_fix WHERE jabatan_tipe = 2 AND RIGHT(jabatan_kode,2)='00' order by jabatan_kode ASC;";

		return $this->db->query($query);
	}

	public function getjabatanfungsionalLevel(){
		$query = "select *, LEFT(jabatan_kode,2) AS kode from ms_jabatan_fix WHERE jabatan_tipe = 2 AND RIGHT(jabatan_kode,2)<>'00' order by jabatan_kode ASC";
		return $this->db->query($query);
	}
}