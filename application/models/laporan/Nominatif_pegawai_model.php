<?php
class Nominatif_pegawai_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('view_dt_dasar',$limit,$offset);
		}else{
			return $this->db->get('view_dt_dasar');
		}
		
	}
	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('view_dt_dasar');
		return $this->db->count_all_results(); 
	}

	public function preview($where,$default_order) {
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($default_order);
		return $this->db->get('view_dt_dasar');
	}	

	// public function getKepangkatan($unit_1 = '',$pegawai_golongan_id = '',$pegawai_golongan_id2 = '',$eselon_id = '',$eselon_id2 = '',$pegawai_status_pegawai_id = '',$pegawai_cpns_tanggal_tmt = '',$search='') {
	// 	$this->db->select('*');
	// 	$where = "pegawai_status = 1";
 //        if($unit_1)$where .= " and pegawai_is_pns = 1 and (pegawai_jenis_pensiun_id is null or pegawai_jenis_pensiun_id = 0) and (unit_kerja_kode_hirarki LIKE '%". $unit_1."%')";
	// 	// if($unit_2)$where .= " and unit_kerja_kode <= '".$unit_2."'";
	// 	if($pegawai_golongan_id)$where .= " and pegawai_golongan_id >= '".$pegawai_golongan_id."'";
	// 	if($pegawai_golongan_id2)$where .= " and pegawai_golongan_id <= '".$pegawai_golongan_id2."'";
	// 	if($eselon_id)$where .= " and pegawai_jabatan_id >= '".$eselon_id."'";
	// 	if($eselon_id2)$where .= " and pegawai_jabatan_id <= '".$eselon_id2."'";
	// 	if($pegawai_status_pegawai_id)$where .= " and pegawai_status_pegawai_id = '".$pegawai_status_pegawai_id."'";
	// 	if($pegawai_cpns_tanggal_tmt)$where .= " and pegawai_cpns_tanggal_tmt like '". $pegawai_cpns_tanggal_tmt."%'";
	// 	$this->db->where($where);
	// 	$this->db->order_by('unit_kerja_id asc, jabatan_kode asc, golongan_kode desc, pegawai_tanggal_tmt asc, pegawai_status_pegawai_id desc');
	// 	return $this->db->get('view_dt_dasar');
	// }	
}