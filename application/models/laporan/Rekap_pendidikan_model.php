<?php
class Rekap_pendidikan_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	
	function getRekapPendidikan(){
		$qry = ("SELECT  a.nama_unker,
	        		SUBSTRING(a.kode_unker,1,4) as substr_kode_unker,
					a.kode_unker,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'S3' AND STATUS = 1 AND IS_PNS = 1) AS lk_s3,

					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'S2' AND STATUS = 1 AND IS_PNS = 1) AS lk_s2,

					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'S1' AND STATUS = 1 AND IS_PNS = 1) AS lk_s1,

					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'D4' AND STATUS = 1 AND IS_PNS = 1) AS lk_d4,

					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'SM' AND STATUS = 1 AND IS_PNS = 1) AS lk_sm,

					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'D3' AND STATUS = 1 AND IS_PNS = 1) AS lk_d3,

					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'D2' AND STATUS = 1 AND IS_PNS = 1) AS lk_d2,

					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'D1' AND STATUS = 1 AND IS_PNS = 1) AS lk_d1,

					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'SLTA' AND STATUS = 1 AND IS_PNS = 1) AS lk_slta,

					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'SLTP' AND STATUS = 1 AND IS_PNS = 1) AS lk_sltp,

					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'SD' AND STATUS = 1 AND IS_PNS = 1) AS lk_sd,

					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN <> '' AND STATUS = 1 AND IS_PNS = 1) AS lk_jml,

					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'S3' AND STATUS = 1 AND IS_PNS = 1) AS pr_s3,

					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'S2' AND STATUS = 1 AND IS_PNS = 1) AS pr_s2,

					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'S1' AND STATUS = 1 AND IS_PNS = 1) AS pr_s1,

					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'D4' AND STATUS = 1 AND IS_PNS = 1) AS pr_d4,

					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'SM' AND STATUS = 1 AND IS_PNS = 1) AS pr_sm,

					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'D3' AND STATUS = 1 AND IS_PNS = 1) AS pr_d3,

					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'D2' AND STATUS = 1 AND IS_PNS = 1) AS pr_d2,

					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'D1' AND STATUS = 1 AND IS_PNS = 1) AS pr_d1,

					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'SLTA' AND STATUS = 1 AND IS_PNS = 1) AS pr_slta,

					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'SLTP' AND STATUS = 1 AND IS_PNS = 1) AS pr_sltp,

					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'SD' AND STATUS = 1 AND IS_PNS = 1) AS pr_sd,

					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN <> '' AND STATUS = 1 AND IS_PNS = 1) AS pr_jml,

					(SELECT count(*) FROM view_dt_dasar WHERE  SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'S3' AND STATUS = 1 AND IS_PNS = 1) AS jml_s3,

					(SELECT count(*) FROM view_dt_dasar WHERE  SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'S2' AND STATUS = 1 AND IS_PNS = 1) AS jml_s2,

					(SELECT count(*) FROM view_dt_dasar WHERE  SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'S1' AND STATUS = 1 AND IS_PNS = 1) AS jml_s1,

					(SELECT count(*) FROM view_dt_dasar WHERE  SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'D4' AND STATUS = 1 AND IS_PNS = 1) AS jml_d4,

					(SELECT count(*) FROM view_dt_dasar WHERE  SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'SM' AND STATUS = 1 AND IS_PNS = 1) AS jml_sm,

					(SELECT count(*) FROM view_dt_dasar WHERE  SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'D3' AND STATUS = 1 AND IS_PNS = 1) AS jml_d3,

					(SELECT count(*) FROM view_dt_dasar WHERE  SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'D2' AND STATUS = 1 AND IS_PNS = 1) AS jml_d2,

					(SELECT count(*) FROM view_dt_dasar WHERE  SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'D1' AND STATUS = 1 AND IS_PNS = 1) AS jml_d1,

					(SELECT count(*) FROM view_dt_dasar WHERE  SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'SLTA' AND STATUS = 1 AND IS_PNS = 1) AS jml_slta,

					(SELECT count(*) FROM view_dt_dasar WHERE  SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'SLTP' AND STATUS = 1 AND IS_PNS = 1) AS jml_sltp,

					(SELECT count(*) FROM view_dt_dasar WHERE  SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN = 'SD' AND STATUS = 1 AND IS_PNS = 1) AS jml_sd,

					(SELECT count(*) FROM view_dt_dasar WHERE  SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 )  
					AND NAMA_PENDIDIKAN <> '' AND STATUS = 1 AND IS_PNS = 1) AS jml


					FROM unker AS a
					WHERE a.kode_unker <> '0000000000' AND a.eselon_unker <> '00' AND a.eselon_unker < '3A' ORDER BY a.kode_unker ASC
		");
		$result = $this->db->query($qry);
		return $result;
	}
}