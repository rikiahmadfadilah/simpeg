<?php
class Rekap_gol_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	
	function getRekapGol(){
		$qry = ("SELECT
					a.nama_unker,
					SUBSTRING( a.kode_unker, 1, 4 ) AS substr_kode_unker,
					a.kode_unker,
					a.eselon_unker,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND 		GOLONGAN <= '1D' AND STATUS = 1 AND IS_PNS = 1) AS lk_gol_1,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND 		GOLONGAN > '1D' AND GOLONGAN < '3A' AND STATUS = 1 AND IS_PNS = 1) AS lk_gol_2,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND 		GOLONGAN > '2D' AND GOLONGAN < '4A' AND STATUS = 1 AND IS_PNS = 1) AS lk_gol_3,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND 		GOLONGAN >= '4A' AND STATUS = 1 AND IS_PNS = 1) AS lk_gol_4,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND STATUS = 1 AND IS_PNS = 1) AS lk_gol_jml,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND 		GOLONGAN <= '1D' AND STATUS = 1 AND IS_PNS = 1) AS pr_gol_1,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND 		GOLONGAN > '1D' AND GOLONGAN < '3A' AND STATUS = 1 AND IS_PNS = 1) AS pr_gol_2,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND 		GOLONGAN > '2D' AND GOLONGAN < '4A' AND STATUS = 1 AND IS_PNS = 1) AS pr_gol_3,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND 		GOLONGAN >= '4A' AND STATUS = 1 AND IS_PNS = 1) AS pr_gol_4,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND STATUS = 1 AND IS_PNS = 1) AS pr_gol_jml,
					(SELECT count(*) FROM view_dt_dasar WHERE SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND 		GOLONGAN <= '1D' AND STATUS = 1 AND IS_PNS = 1) AS jml_gol_1,
					(SELECT count(*) FROM view_dt_dasar WHERE SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND 		GOLONGAN > '1D' AND GOLONGAN < '3A' AND STATUS = 1 AND IS_PNS = 1) AS jml_gol_2,
					(SELECT count(*) FROM view_dt_dasar WHERE SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND 		GOLONGAN > '2D' AND GOLONGAN < '4A' AND STATUS = 1 AND IS_PNS = 1) AS jml_gol_3,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND 		GOLONGAN >= '4A' AND STATUS = 1 AND IS_PNS = 1) AS jml_gol_4,
					(SELECT count(*) FROM view_dt_dasar WHERE SUBSTRING( KODE_UNKER, 1, 4 ) = SUBSTRING( a.kode_unker, 1, 4 ) AND STATUS = 1 AND IS_PNS = 1) AS jml_gol
				FROM
					unker AS a 
				WHERE a.kode_unker <> '0000000000' AND a.eselon_unker <> '00' AND a.eselon_unker < '3A' ORDER BY a.kode_unker ASC
		");
		$result = $this->db->query($qry);
		return $result;
	}
}