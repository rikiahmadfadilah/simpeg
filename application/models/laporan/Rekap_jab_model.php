<?php
class Rekap_jab_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	
	function getRekapJab(){
		$qry = ("SELECT
					a.nama_unker,
					SUBSTRING( a.kode_unker, 1, 4 ) AS substr_kode_unker,
					a.kode_unker,
					a.eselon_unker,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND ESELON = '1A' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_esel_1a,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND ESELON = '1B' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_esel_1b,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND ESELON = '2A' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_esel_2a,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND ESELON = '2B' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_esel_2b,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND ESELON = '3A' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_esel_3a,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND ESELON = '3B' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_esel_3b,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND ESELON = '4A' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_esel_4a,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND ESELON = '4B' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_esel_4b,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND ESELON = '88' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_esel_v,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND ESELON = '88' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_jabfunc,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND ESELON = '99' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_pelaksana,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND ESELON <> '' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_lk,

					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND ESELON = '1A' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_esel_1a,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND ESELON = '1B' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_esel_1b,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND ESELON = '2A' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_esel_2a,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND ESELON = '2B' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_esel_2b,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND ESELON = '3A' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_esel_3a,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND ESELON = '3B' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_esel_3b,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND ESELON = '4A' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_esel_4a,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND ESELON = '4B' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_esel_4b,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND ESELON = '88' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_esel_v,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND ESELON = '88' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_jabfunc,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND ESELON = '99' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_pelaksana,
					(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND ESELON <> '' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_pr,
					
					(SELECT count(*) FROM view_dt_dasar WHERE ESELON = '1A' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_esel_1a,
					(SELECT count(*) FROM view_dt_dasar WHERE ESELON = '1B' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_esel_1b,
					(SELECT count(*) FROM view_dt_dasar WHERE ESELON = '2A' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_esel_2a,
					(SELECT count(*) FROM view_dt_dasar WHERE ESELON = '2B' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_esel_2b,
					(SELECT count(*) FROM view_dt_dasar WHERE ESELON = '3A' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_esel_3a,
					(SELECT count(*) FROM view_dt_dasar WHERE ESELON = '3B' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_esel_3b,
					(SELECT count(*) FROM view_dt_dasar WHERE ESELON = '4A' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_esel_4a,
					(SELECT count(*) FROM view_dt_dasar WHERE ESELON = '4B' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_esel_4b,
					(SELECT count(*) FROM view_dt_dasar WHERE ESELON = '88' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_esel_v,
					(SELECT count(*) FROM view_dt_dasar WHERE ESELON = '88' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_jabfunc,
					(SELECT count(*) FROM view_dt_dasar WHERE ESELON = '99' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_pelaksana,
					(SELECT count(*) FROM view_dt_dasar WHERE ESELON <> '' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml
					
					FROM unker AS a 
					WHERE a.kode_unker <> '0000000000' AND a.eselon_unker <> '00' ORDER BY a.kode_unker ASC
		");
		$result = $this->db->query($qry);
		return $result;
	}
	// function getRekapJab(){
	// 	$qry = ("SELECT
	// 				a.nama_unker,
	// 				SUBSTRING( a.kode_unker, 1, 4 ) AS substr_kode_unker,
	// 				a.kode_unker,
	// 				a.eselon_unker,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND GOLONGAN = '1A' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_gol_1a,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND GOLONGAN = '1B' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_gol_1b,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND GOLONGAN = '1C' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_gol_1c,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND GOLONGAN = '1D' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_gol_1d,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND GOLONGAN = '2A' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_gol_2a,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND GOLONGAN = '2B' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_gol_2b,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND GOLONGAN = '2C' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_gol_2c,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND GOLONGAN = '2D' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_gol_2d,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND GOLONGAN = '3A' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_gol_3a,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND GOLONGAN = '3B' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_gol_3b,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND GOLONGAN = '3C' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_gol_3c,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND GOLONGAN = '3D' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_gol_3d,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND GOLONGAN = '4A' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_gol_4a,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND GOLONGAN = '4B' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_gol_4b,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND GOLONGAN = '4C' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_gol_4c,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND GOLONGAN = '4D' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_gol_4d,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 1 AND GOLONGAN = '4E' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS lk_gol_4e,

	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND GOLONGAN = '1A' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_gol_1a,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND GOLONGAN = '1B' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_gol_1b,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND GOLONGAN = '1C' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_gol_1c,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND GOLONGAN = '1D' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_gol_1d,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND GOLONGAN = '2A' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_gol_2a,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND GOLONGAN = '2B' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_gol_2b,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND GOLONGAN = '2C' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_gol_2c,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND GOLONGAN = '2D' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_gol_2d,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND GOLONGAN = '3A' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_gol_3a,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND GOLONGAN = '3B' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_gol_3b,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND GOLONGAN = '3C' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_gol_3c,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND GOLONGAN = '3D' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_gol_3d,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND GOLONGAN = '4A' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_gol_4a,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND GOLONGAN = '4B' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_gol_4b,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND GOLONGAN = '4C' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_gol_4c,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND GOLONGAN = '4D' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_gol_4d,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE JENIS_KEL = 2 AND GOLONGAN = '4E' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS pr_gol_4e,
					
	// 				(SELECT count(*) FROM view_dt_dasar WHERE GOLONGAN = '1A' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_1a,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE GOLONGAN = '1B' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_1b,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE GOLONGAN = '1C' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_1c,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE GOLONGAN = '1D' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_1d,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE GOLONGAN = '2A' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_2a,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE GOLONGAN = '2B' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_2b,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE GOLONGAN = '2C' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_2c,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE GOLONGAN = '2D' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_2d,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE GOLONGAN = '3A' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_3a,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE GOLONGAN = '3B' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_3b,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE GOLONGAN = '3C' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_3c,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE GOLONGAN = '3D' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_3d,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE GOLONGAN = '4A' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_4a,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE GOLONGAN = '4B' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_4b,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE GOLONGAN = '4C' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_4c,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE GOLONGAN = '4D' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_4d,
	// 				(SELECT count(*) FROM view_dt_dasar WHERE GOLONGAN = '4E' AND KODE_UNKER = a.kode_unker AND STATUS = 1 AND IS_PNS = 1) AS jml_4e
	// 				FROM unker AS a 
	// 				WHERE a.kode_unker <> '0000000000' AND a.eselon_unker <> '00' ORDER BY a.kode_unker ASC
	// 	");
	// 	$result = $this->db->query($qry);
	// 	return $result;
	// }
}