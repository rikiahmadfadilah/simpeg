<?php
class Kelurahan_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function getkecamatan($kecamatan_id = '') {
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		if($kecamatan_id!=""){
			$this->db->where('KODE', $kecamatan_id);
		}
		return $this->db->get('tabel_kecamatan');
	}
	public function checkKodekelurahan($kelurahan_kode = '') {
		$this->db->select('*');
		$this->db->where('kelurahan_kode', $kelurahan_kode);
		return $this->db->get('ms_kelurahan');
	}
	public function checkNamakelurahan($kelurahan_nama = '') {
		$this->db->select('*');
		$this->db->where('kelurahan_nama', $kelurahan_nama);
		return $this->db->get('ms_kelurahan');
	}
	public function getkelurahan($kelurahan_id = '') {
		$this->db->select('*');
		if($kelurahan_id!=""){
			$this->db->where('kelurahan_id', $kelurahan_id);
		}
		return $this->db->get('view_tabel_kelurahan');
	}
	
	public function addkelurahan($datacreate){
		$this->db->insert('ms_kelurahan', $datacreate);
		return $this->db->insert_id();
	}

	public function updatekelurahan($dataupdate, $kelurahan_id){
		$this->db->where('kelurahan_id', $kelurahan_id);
		return $this->db->update('ms_kelurahan', $dataupdate);
	}
	
	public function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('view_tabel_kelurahan',$limit,$offset);
		}else{
			return $this->db->get('view_tabel_kelurahan');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('view_tabel_kelurahan');
		return $this->db->count_all_results(); 
	}
	function getDataListKecamatan($search,$page)
	{	
		$this->db->where('STATUS = 1');
		$limit = 5;
		$offset = $page*$limit;
		//$this->db->where('pegawai_unit_kerja_id',$unit_kerja_parent_id);
		$likeclause ="(";
		$likeclause .= "UPPER(kecamatan_kode) LIKE '%".strtoupper($search)."%' OR ";
		$likeclause .= "UPPER(kecamatan_nama) LIKE '%".strtoupper($search)."%'";
		$likeclause .=")";
		$this->db->where($likeclause);
		
		return $this->db->get('view_ms_kecamatan',$limit,$offset);
	}
	function getDataListKecamatanCount($search)
	{	
		$this->db->where('STATUS = 1');
		$likeclause ="";
		$likeclause ="(";
		$likeclause .= "UPPER(kecamatan_kode) LIKE '%".strtoupper($search)."%' OR ";
		$likeclause .= "UPPER(kecamatan_nama) LIKE '%".strtoupper($search)."%'";
		$likeclause .=")";
		$this->db->where($likeclause);
		
		return $this->db->get('view_ms_kecamatan');
	}
	function getDataKecamatan($kecamatan_kode)
	{	
		$this->db->where('kecamatan_kode', $kecamatan_kode);
		return $this->db->get('view_ms_kecamatan');
	}
}