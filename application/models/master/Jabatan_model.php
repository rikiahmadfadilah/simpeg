<?php
class Jabatan_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function getjabatan($jabatan_id = '') {
		$this->db->select('*');
		if($jabatan_id!=""){
			$this->db->where('idfungsional', $jabatan_id);
		}
		return $this->db->get('fungsional');
	}
	public function getjabatan_fix($jabatan_id = '') {
		$this->db->select('*');
		if($jabatan_id!=""){
			$this->db->where('jabatan_id', $jabatan_id);
		}
		return $this->db->get('view_ms_jabatan_fix');
	}	
	public function checkKodeJabatan($jabatan_kode = '') {
		$this->db->select('*');
		$this->db->where('jabatan_kode', $jabatan_kode);
		return $this->db->get('ms_jabatan');
	}
	public function checkNamaJabatan($jabatan_nama = '') {
		$this->db->select('*');
		$this->db->where('jabatan_nama', $jabatan_nama);
		return $this->db->get('ms_jabatan');
	}
	
	public function addjabatan($datacreate){
		$this->db->insert('fungsional', $datacreate);
		return $this->db->insert_id();
	}
	public function addjabatan_fix($datacreate){
		$this->db->insert('ms_jabatan_fix', $datacreate);
		return $this->db->insert_id();
	}
	public function updatejabatan($dataupdate, $jabatan_id){
		$this->db->where('idfungsional', $jabatan_id);
		return $this->db->update('fungsional', $dataupdate);
	}
	public function updatejabatan_fix($dataupdate, $jabatan_id){
		$this->db->where('jabatan_id', $jabatan_id);
		return $this->db->update('ms_jabatan_fix', $dataupdate);
	}
    function selectData($where,$kolom,$table){   
	    $data = "";
        $this->db->select($kolom);
        $this->db->where($where);
        $query = $this->db->get($table);
        foreach ($query->result() as $row) {
			$data = $row->$kolom;
		}	
        return $data;
    }				
	public function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('fungsional',$limit,$offset);
		}else{
			return $this->db->get('fungsional');
		}
		
	}
	
	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('fungsional');
		return $this->db->count_all_results(); 
	}

	public function check_jabatan_kode($jabatan_kode = '') {
		$this->db->select('*');
		$this->db->where('jabatan_kode', $jabatan_kode);
		return $this->db->get('ms_jabatan_fix');
	}
	public function check_jabatan_nama($jabatan_nama = '') {
		$this->db->select('*');
		$this->db->where('jabatan_nama', $jabatan_nama);
		return $this->db->get('ms_jabatan_fix');
	}
	public function getIndukJFT() {
		$this->db->select('*');
		$this->db->order_by('jabatan_kode', 'asc');
		$this->db->where('jabatan_tipe', 2);
		$this->db->where('jabatan_status', 1);
		$this->db->where("right(jabatan_kode,2)='00'");
		
		return $this->db->get('ms_jabatan_fix');
	}	
}