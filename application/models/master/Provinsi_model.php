<?php
class Provinsi_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function getprovinsi($id = '') {
		$this->db->select('*');
		if($id!=""){
			$this->db->where('ID', $id);
		}
		return $this->db->get('tabel_provinsi');
	}
	public function checkKodeProvinsi($kode = '') {
		$this->db->select('*');
		$this->db->where('kode', $kode);
		return $this->db->get('tabel_provinsi');
	}
	public function checkNamaProvinsi($provinsi = '') {
		$this->db->select('*');
		$this->db->where('PROVINSI', $provinsi);
		return $this->db->get('tabel_provinsi');
	}
	
	public function addprovinsi($datacreate){
		$this->db->insert('tabel_provinsi', $datacreate);
		return $this->db->insert_id();
	}

	public function updateprovinsi($dataupdate, $id){
		$this->db->where('ID', $id);
		return $this->db->update('tabel_provinsi', $dataupdate);
	}
	
	public function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('tabel_provinsi',$limit,$offset);
		}else{
			return $this->db->get('tabel_provinsi');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('tabel_provinsi');
		return $this->db->count_all_results(); 
	}
}