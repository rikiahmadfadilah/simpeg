<?php
class Kota_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function getProvinsi($id = '') {
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		if($id!=""){
			$this->db->where('ID', $id);
		}
		return $this->db->get('tabel_provinsi');
	}
	public function checkKodeKota($kode = '') {
		$this->db->select('*');
		$this->db->where('KODE', $kode);
		return $this->db->get('tabel_kota');
	}
	public function checkNamaKota($kota_nama = '') {
		$this->db->select('*');
		$this->db->where('KOTA', $kota_nama);
		return $this->db->get('tabel_kota');
	}
	public function getKota($id = '') {
		$this->db->select('*');
		if($id!=""){
			$this->db->where('ID', $id);
		}
		return $this->db->get('view_tabel_kota');
	}
	
	public function addkota($datacreate){
		$this->db->insert('tabel_kota', $datacreate);
		return $this->db->insert_id();
	}

	public function updatekota($dataupdate, $id){
		$this->db->where('ID', $id);
		return $this->db->update('tabel_kota', $dataupdate);
	}
	
	public function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('view_tabel_kota',$limit,$offset);
		}else{
			return $this->db->get('view_tabel_kota');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('view_tabel_kota');
		return $this->db->count_all_results(); 
	}
}