<?php
class Config_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function getconfig($id = '') {
		$this->db->select('*');
		if($id!=""){
			$this->db->where('ID', $id);
		}
		return $this->db->get('tabel_config');
	}
	public function addconfig($datacreate){
		$this->db->insert('tabel_config', $datacreate);
		return $this->db->insert_id();
	}
	public function updateconfig($dataupdate, $id){
		$this->db->where('ID', $id);
		return $this->db->update('tabel_config', $dataupdate);
	}
	public function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('tabel_config',$limit,$offset);
		}else{
			return $this->db->get('tabel_config');
		}
		
	}
	
	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('tabel_config');
		return $this->db->count_all_results(); 
	}
}