<?php
class Unit_kerja_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function getUnitKerja() {
		$this->db->select('*');
		$this->db->order_by('kode_unker', 'asc');
		$this->db->where('aktif', 1);
		return $this->db->get('view_unker');
	}
	public function getUnitKerjaSatker(){
		$this->db->order_by('kode_unker', "asc");
		return $this->db->get('view_unker');
	}
	public function getProvinsi() {
		$this->db->select('*');
		$this->db->order_by('ID', 'asc');
		$this->db->where('STATUS', 1);
		return $this->db->get('view_tabel_provinsi');
	}
	public function getkerja() {
		$this->db->select('*');
		$this->db->order_by('unit_kerja_id', 'asc');
		$this->db->where('unit_kerja_status', 1);
		return $this->db->get('dbo.view_unker');
	}
	public function geteselon($eselon_id = '') {
		$this->db->select('*');
		if($eselon_id!=""){
			$this->db->where('ID', $eselon_id);
		}
		$this->db->order_by('KODE_ESL','asc');

		return $this->db->get('view_tabel_eselon');
	}
	public function getjabatan($jabatan_id = '') {
		$this->db->select('*');
		if($jabatan_id!=""){
			$this->db->where('jabatan_id', $jabatan_id);
		}
		return $this->db->get('ms_jabatan');
	}
	public function get_detail_unit_kerja($idunker = '') {
		$this->db->select('*');
		if($idunker!=""){
			$this->db->where('idunker', $idunker);
		}
		return $this->db->get('view_unker');
	}
	public function checkKodeUnitKerja($kode_unker = '') {
		$this->db->select('*');
		$this->db->where('kode_unker', $kode_unker);
		return $this->db->get('unker');
	}
	public function checkNamaUnitKerja($nama_unker = '') {
		$this->db->select('*');
		$this->db->where('nama_unker', $nama_unker);
		return $this->db->get('unker');
	}
	public function create_unit_kerja($datacreate){
		$this->db->insert('unker', $datacreate);
		return $this->db->insert_id();
	}

	public function update_unit_kerja($dataupdate, $idunker){
		$this->db->where('idunker', $idunker);
		return $this->db->update('unker', $dataupdate);
	}

	public function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('view_unker',$limit,$offset);
		}else{
			return $this->db->get('view_unker');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('view_unker');
		return $this->db->count_all_results(); 
	}
}