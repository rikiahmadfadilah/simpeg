<?php
class Kecamatan_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}
	public function getkota($kota_id = '') {
		$this->db->select('*');
		$this->db->where('STATUS', 1);
		if($kota_id!=""){
			$this->db->where('ID', $kota_id);
		}
		return $this->db->get('tabel_kota');
	}
	public function checkKodekecamatan($kode = '') {
		$this->db->select('*');
		$this->db->where('KODE', $kode);
		return $this->db->get('tabel_kecamatan');
	}
	public function checkNamakecamatan($kecamatan_nama = '') {
		$this->db->select('*');
		$this->db->where('KECAMATAN', $kecamatan_nama);
		return $this->db->get('tabel_kecamatan');
	}
	public function getkecamatan($kecamatan_id = '') {
		$this->db->select('*');
		if($kecamatan_id!=""){
			$this->db->where('ID', $kecamatan_id);
		}
		return $this->db->get('view_tabel_kecamatan');
	}
	
	public function addkecamatan($datacreate){
		$this->db->insert('tabel_kecamatan', $datacreate);
		return $this->db->insert_id();
	}

	public function updatekecamatan($dataupdate, $kecamatan_id){
		$this->db->where('ID', $kecamatan_id);
		return $this->db->update('tabel_kecamatan', $dataupdate);
	}
	
	public function get_list_data($limit=10, $offset=0, $ordertext = '', $search='', $fields='', $default_order='',$where = '')
	{
		if($where!=''){
			$this->db->where($where);
		}
		
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($ordertext) || empty($ordertext))
		{
			$this->db->order_by($default_order);
		} else {
			$this->db->order_by($ordertext);
		}
		if($limit>0){
			return $this->db->get('view_tabel_kecamatan',$limit,$offset);
		}else{
			return $this->db->get('view_tabel_kecamatan');
		}
		
	}

	function get_count_all_data($search='', $fields='',$where = '')
	{	
		if($where!=''){
			$this->db->where($where);
		}
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}
		$this->db->from('view_tabel_kecamatan');
		return $this->db->count_all_results(); 
	}
}