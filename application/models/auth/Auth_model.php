<?php
class Auth_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}

	public function checkUsername($username) {
		$this->db->select('username, user_status');
		$this->db->where('username', $username);
		$this->db->where('user_status', 1);
		return $this->db->get('view_sys_users');
	}

	public function getAuth($username, $password) {
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		$this->db->where('user_status', 1);
		$this->db->from('view_sys_users');
		return $this->db->count_all_results(); 
	}
	function getUser($username) {
		$this->db->select('*');
		$this->db->where('username', $username);
		$this->db->where('user_status', 1);
		return $this->db->get('view_sys_users');
	}
	
	function getUserDefaultMenu($username) {
		$this->db->select('menu_url,menu_nama,menu_icon');
		$this->db->join('ms_akses', 'akses_id = user_akses_id and akses_status = 1', 'inner');
		$this->db->join('ms_menu', 'menu_id = akses_default_menu_id and menu_status = 1', 'inner');
		$this->db->where('username', $username);
		$this->db->where('user_status', 1);
		return $this->db->get("ms_user");
	}
	function setLastLogin($userId){
		$ipaddress = $this->input->ip_address();
		$ipaddress = (($ipaddress=='::1')?'127.0.0.1':$ipaddress);
		$this->db->set('user_last_online', date('Y-m-d H:i:s', time()));
		$this->db->set("user_is_online", 1);
		$this->db->set("user_ip_address", $ipaddress);
		$this->db->where("user_id", $userId);
		$this->db->update("ms_user");
	}
}
