<?php
class Auth_api_model extends CI_Model{

	function __construct(){
		parent:: __construct();
	}

	public function checkUsername($username) {
		$this->db->select('username, user_status');
		$this->db->where('username', $username);
		$this->db->where('user_status', 1);
		return $this->db->get('Master.view_sys_users');
	}

	public function getAuth($username, $password) {
		$this->db->where('username', $username);
		$this->db->where('password', MD5($password));
		$this->db->where('user_status', 1);
		return $this->db->get('ms_user');
	}
	public function updateToken($access_token, $new_token) {
		$this->db->set('api_token_code', $new_token);
		$this->db->where('api_token_code', $access_token);
		$this->db->update("trx_api_token");
	}
	function getUser($username) {
		$this->db->select('*');
		// $this->db->join('master_employees', 'employee_user_id = user_id and employee_status = 1', 'inner');
		// $this->db->join('sys_access', 'akses_id = user_akses_id and akses_status = 1', 'inner');
		$this->db->where('username', $username);
		$this->db->where('user_status', 1);
		return $this->db->get('view_sys_users');
	}
	
	function getUserDefaultMenu($username) {
		$this->db->select('menu_url,menu_nama,menu_icon');
		$this->db->join('Master.akses', 'akses_id = user_akses_id and akses_status = 1', 'inner');
		$this->db->join('Master.menu', 'menu_id = akses_default_menu_id and menu_status = 1', 'inner');
		$this->db->where('username', $username);
		$this->db->where('user_status', 1);
		return $this->db->get("Master.user");
	}
	function setLastLogin($userId){
		$ipaddress = $this->input->ip_address();
		$ipaddress = (($ipaddress=='::1')?'127.0.0.1':$ipaddress);
		$this->db->set('user_last_online', date('Y-m-d H:i:s', time()));
		$this->db->set("user_is_online", 1);
		$this->db->set("user_ip_address", $ipaddress);
		$this->db->where("user_id", $userId);
		$this->db->update("Master.user");
	}
	
}